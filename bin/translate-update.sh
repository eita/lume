#!/bin/sh

if [ -d "venv/" ]; then
    ENV="venv/"
else
    ENV="env/"
fi

BASEDIR=$(dirname $0)/../
cd $BASEDIR

echo ""
echo "obter traduções transifex ..."
echo ""
# $ENV/bin/tx pull --all
tx pull --all

echo ""
echo "gerar frases a partir da base de dados ..."
echo ""
$ENV/bin/python manage.py generate_messages_from_db

echo ""
echo "atualizar frases para tradução ..."
echo ""
$ENV/bin/django-admin makemessages --all

echo ""
echo "compilar arquivos traduzidos ..."
echo ""
$ENV/bin/django-admin compilemessages

echo ""
echo "enviar frases novas para transifex ..."
echo ""
# $ENV/bin/tx push -s --skip
tx push -s --skip

echo ""
echo "Tradução sincronizada com sucesso!"
echo ""
echo "Necessita reiniciar o serviço. Execute:"
echo "sudo supervisorctl restart lume"
echo ""
