.. Plataforma LUME documentation master file, created by
   sphinx-quickstart on Sun Jun  6 21:41:45 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentação da Plataforma LUME
================================================

.. toctree::
   :maxdepth: 1
   :caption: Sumário:

   Informações sobre instalação <README.md>
   Módulo principal <main>
   Modelo de dados do módulo principal <model_main>
   Módulo Caderno de Campo <cadernodecampo>
   Modelo de dados do módulo Caderno de Campo <model_cadernodecampo>
   Módulo de Análise Econômica <economica>
   Modelo de dados do módulo de Análise Econômica <model_economica>
   Módulo de Análise Qualitativa <qualitativa>
   Modelo de dados do módulo de Análise Qualitativa <model_qualitativa>
   Módulo da Linha do Tempo <linhadotempo>
   Modelo de dados do módulo da Linha do Tempo <model_linhadotempo>
   Módulo de Relatórios <relatorios>
   Modelo de dados do módulo de relatórios <model_relatorios>


Índices e tabelas
==================

* :ref:`genindex`
* :ref:`modindex`