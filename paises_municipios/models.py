from django.db import models
from django.utils.translation import gettext_lazy as _lazy

from lume.models import LumeModel
from municipios.models import UF


class Pais(LumeModel):
    nome = models.CharField(max_length=200, verbose_name=_lazy("nome"))
    nome_i18n = models.CharField(
        max_length=200, verbose_name=_lazy("nome internacionalizado")
    )
    sigla = models.CharField(max_length=10, verbose_name=_lazy("sigla"))
    continente = models.CharField(
        max_length=10, verbose_name=_lazy("continente"), null=True, blank=True
    )
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("última alteração")
    )

    class Meta:
        ordering = ("nome",)
        unique_together = ["sigla", "nome"]

    def __str__(self):
        return self.nome


class PaisUF(LumeModel):
    pais = models.ForeignKey(
        Pais, on_delete=models.CASCADE, related_name="ufs_relacionadas"
    )
    uf = models.ForeignKey(
        UF, on_delete=models.CASCADE, related_name="pais_relacionado"
    )

    class Meta:
        unique_together = ["pais", "uf"]
