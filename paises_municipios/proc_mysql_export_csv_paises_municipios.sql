DELIMITER $$                                                                                                                                                                    

-- DROP PROCEDURE IF EXISTS proc_mysql_export_csv_paises_municipios $$                                                                                                                       

CREATE PROCEDURE proc_mysql_export_csv_paises_municipios(
        IN export_csv_to VARCHAR(300)
)
BEGIN                                                                                                                                                                           
        DECLARE table_name_pais VARCHAR(255);                                                                                                                                        
        DECLARE end_of_tables INT DEFAULT 0;                                                                                                                                    

        DECLARE cur CURSOR FOR                                                                                                                                                  
            SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES 
            WHERE TABLE_NAME regexp '^_[A-Z]{2}$' AND TABLE_NAME != '_BR'
            ORDER BY TABLE_NAME;                                                                                                    
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET end_of_tables = 1;                                                                                                           

        OPEN cur;                                                                                                                                                               

        tables_loop: LOOP                                                                                                                                                       
            FETCH cur INTO table_name_pais;                                                                                                                                          

            IF end_of_tables = 1 THEN                                                                                                                                           
                LEAVE tables_loop;                                                                                                                                              
            END IF;                                                                                                                                                             

            SET @s = concat(
            "SELECT pais.continente, mun.pais as pais_sigla, pais.nome as pais,
                 prov.nome as provincia, mun.id as municipio_id, mun.nome as municipio,
                 mun.lat, mun.lng, mun.altitude, mun.populacao, mun.cidade_bool
			 FROM ", table_name_pais," as mun
             INNER JOIN ", table_name_pais,"_maes as prov
                 on mun.id_mae=prov.id
			 INNER JOIN __paises as pais
                 on pais.id=mun.pais
             INTO OUTFILE '", export_csv_to, table_name_pais,".csv'
             FIELDS TERMINATED BY ','
             ENCLOSED BY '\"'
             LINES TERMINATED BY '\n';");

            PREPARE stmt FROM @s;                                                                                                                                               
            EXECUTE stmt; 
            DEALLOCATE PREPARE stmt;

        END LOOP;                                                                                                                                                               

        CLOSE cur;                                                                                                                                                              
    END $$                                                                                                                                                                      

DELIMITER ;

-- call proc_mysql_export_csv_paises_municipios('/var/www/paises_municipios/data/');