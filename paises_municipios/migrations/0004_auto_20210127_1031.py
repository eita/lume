# Generated by Django 2.1.5 on 2021-01-27 13:31

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('paises_municipios', '0003_auto_20210125_1118'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='pais',
            unique_together={('sigla', 'nome')},
        ),
    ]
