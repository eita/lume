# Generated by Django 2.1.5 on 2021-01-25 03:51

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Pais',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(blank=True, editable=False, null=True)),
                ('nome', models.CharField(max_length=200, verbose_name='nome')),
                ('nome_i18n', models.CharField(max_length=200, verbose_name='nome internacionalizado')),
                ('sigla', models.CharField(max_length=10, verbose_name='sigla')),
                ('continente', models.CharField(max_length=10, verbose_name='continente')),
                ('criacao', models.DateTimeField(auto_now_add=True, verbose_name='criação')),
                ('ultima_alteracao', models.DateTimeField(auto_now=True, verbose_name='última alteração')),
            ],
            options={
                'ordering': ('nome',),
            },
        ),
        migrations.AlterUniqueTogether(
            name='pais',
            unique_together={('sigla', 'continente')},
        ),
    ]
