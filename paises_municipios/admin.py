from django.contrib import admin
from .models import Pais, PaisUF

# Register your models here.

admin.site.register(Pais)
admin.site.register(PaisUF)
