# Países e municípios

Aplicação que supre a necessidade de suporte a países além do Brasil no Lume.

## Contexto

Esta aplicação trabalha em conjunto com a aplicação [municipios](https://gitlab.com/eita/django/django-municipios-br)
que dá suporte aos Estados e municípios do Brasil utilizando a fonte de dados do ibge2017.

## Países e municípios

Esta aplicação utiliza a fonte de dados de países e municípios do [Agroecologia em rede](https://agroecologiaemrede.org.br/)
que está estruturada em MySQL.

## Importação dos países e municípios

### Importação por país

Para realizar a importação dos dados de um país considere os seguintes passos:

* Extração dos dados de um país/municípios para um arquivo CSV. Para isso foi utilizada a consulta:
    ```sql
    SELECT pais.continente, pais.nome as pais, mun.pais as pais_sigla,
    prov.nome as provincia, mun.id as municipio_id, mun.nome as municipio,
    mun.lat, mun.lng, mun.altitude, mun.populacao, mun.cidade_bool
    FROM _AR as mun
    INNER JOIN _AR_maes as prov
    on mun.id_mae=prov.id
    INNER JOIN __paises as pais
    on pais.id=mun.pais

    INTO OUTFILE '/var/www/lume/paises_municipios/static/paises_municipios/data/AR.csv'
    FIELDS TERMINATED BY ','
    ENCLOSED BY '\"'
    LINES TERMINATED BY '\n';
    ```

* Executar script que importa os dados CSV para a base de dados do Lume:
    ```sh
    (venv)$ python manage.py import_municipios_from_csv paises_municipios/static/paises_municipios/data/AR.csv
    ```

### Importação de todos países do MySQL

Para realizar a importação de todos os países da base considere os seguintes passos:

* Crie a procedure no MySQL que exportará os países e muncípios separados por arquivos CSV. Utilize o script `proc_mysql_export_csv_paises_municipios.sql`

* Execute a procedure, passando o diretório destino dos arquivos CSV como parâmetro: 
    ```sql
    call proc_mysql_export_csv_paises_municipios('/var/www/paises_municipios/data/');
    ```

* Execute a importação de países no Lume, passando o diretório de dados como parâmetro:
    ```sh
    (venv)$ python manage.py import_municipios_from_csv /var/www/paises_municipios/data/
    ```
