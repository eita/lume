import os
import re
import csv

from django.core.management.base import BaseCommand, CommandError
from django.utils.text import slugify

from municipios.models import Municipio, UF
from paises_municipios.models import Pais, PaisUF


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument(
            'csv_path', nargs=1,
            help="Arquivo CSV ou diretório para importação dos municípios.")

    def handle(self, *args, **options):
        csv_path = options['csv_path'][0]
        PrepareImport(csv_path)

        self.stdout.write(self.style.SUCCESS(
            "\nMunicípios importados com sucesso!\n"))


class PrepareImport():

    def __init__(self, csv_path):
        self.csv_path = csv_path
        if os.path.isdir(csv_path):
            self.load_dir_to_import()
        else:
            ImportMunicipios(csv_path)

    def load_dir_to_import(self):

        files = sorted(os.listdir(self.csv_path))
        for file in files:
            if 'csv' not in file[-3:].lower():
                continue

            print(f"\nImportando dados de {file} ...")
            csv_path = f"{self.csv_path}{file}"
            ImportMunicipios(csv_path)
            print("OK!\n")


class ImportMunicipios():

    def __init__(self, csv_file):

        self.pais = None

        with open(csv_file) as csvfile:
            csv_reader = csv.reader(csvfile, delimiter=",")
            for row in csv_reader:
                self.row = row

                # verifica se tem header das colunas e ignora
                if len(self.get_pais_sigla()) > 3:
                    continue

                self.get_pais()
                self.get_municipio()
                self.get_pais_uf_related()

    def get_pais(self):
        if self.pais: return self.pais

        pais, created = Pais.objects.get_or_create(
            nome=self.get_pais_nome(),
            nome_i18n=self.get_pais_nome_i18n(),
            sigla=self.get_pais_sigla(),
            continente=self.get_continente(),
        )

        self.pais = pais
        return self.pais

    def get_pais_nome_i18n(self):
        return self.row[2]

    def get_pais_nome(self):

        pais_i18n = self.get_pais_nome_i18n()

        try:
            return re.search(r'pt:([\w\s]+)\|', pais_i18n).group(1)
        except:
            return pais_i18n

    def get_pais_sigla(self):
        return self.row[1]

    def get_continente(self):
        return self.row[0]

    def get_municipio(self):
        municipio, created = Municipio.objects.get_or_create(
            nome = self.get_municipio_nome(),
            uf = self.get_uf(),
            # regiao_imediata = models.ForeignKey(RegiaoImediata, on_delete=models.SET_NULL, null=True)
            # diocese = models.ForeignKey(Diocese, on_delete=models.SET_NULL, null=True)
        )

        return municipio

    def get_municipio_nome(self):
        return self.row[5]

    def get_uf(self):
        uf, created = UF.objects.get_or_create(
            nome = self.get_uf_nome(), # models.CharField(max_length=500)
            sigla = self.get_uf_sigla(), # self.get_models.CharField(max_length=500)
            # regiao = models.ForeignKey(Regiao, on_delete=models.SET_NULL, null=True)
        )

        return uf

    def get_uf_nome(self):
        return self.row[3]

    def get_uf_sigla(self):
        return self.row[3]

    def get_pais_uf_related(self):
        pais_uf, created = PaisUF.objects.get_or_create(
            pais = self.get_pais(),
            uf = self.get_uf(),
        )

        return pais_uf

