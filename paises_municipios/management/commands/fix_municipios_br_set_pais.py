import re
import csv

from django.core.management.base import BaseCommand, CommandError
from django.utils.text import slugify

from municipios.models import Municipio, UF
from paises_municipios.models import Pais, PaisUF
from main.models import Comunidade


class Command(BaseCommand):

    def handle(self, *args, **options):
        self.get_brasil()
        self.set_comunidades_brasil()
        self.set_ufs_brasil()

    def get_brasil(self):
        try:
            return self.pais
        except:
            pass

        pais, created = Pais.objects.get_or_create(
            nome='Brasil',
            nome_i18n='de:Brasilien|en:Brazil|fr:Brésil|pt:Brasil|it:Brasile|es:Brasil',
            sigla='BR',
            continente='ALC',
        )

        self.pais = pais
        return self.pais

    def set_comunidades_brasil(self):
        Comunidade.objects.filter(pais__isnull=True).update(pais=self.get_brasil())

    def set_ufs_brasil(self):
        ufs = UF.objects.filter(pais_relacionado__pais__isnull=True)
        for uf in ufs:
            PaisUF.objects.get_or_create(
                uf = uf,
                pais = self.get_brasil(),
            )
