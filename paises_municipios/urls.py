from django.urls import path

from .views import (PaisAutocomplete,
                    UFAutocomplete,
                    MunicipioAutocomplete)

app_name = 'paises_municipios'
urlpatterns = [
    path(
        'pais-autocomplete/',
        PaisAutocomplete.as_view(),
        name='pais-autocomplete',
    ),
    path(
        'uf-autocomplete/',
        UFAutocomplete.as_view(),
        name='uf-autocomplete',
    ),
    path(
        'municipio-autocomplete/',
        MunicipioAutocomplete.as_view(),
        name='municipio-autocomplete',
    ),
]
