from dal import autocomplete

from .models import Pais
from municipios.models import UF, Municipio


class PaisAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Pais.objects.none()

        qs = Pais.objects.all()

        if self.q:
            qs = qs.filter(nome__istartswith=self.q)

        return qs


class UFAutocomplete(autocomplete.Select2QuerySetView):
    def get_result_label(self, item):
        return item.nome

    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return UF.objects.none()

        qs = UF.objects.all().order_by('nome')

        pais = self.forwarded.get('pais', None)

        if pais:
            qs = qs.filter(pais_relacionado__pais_id=pais)

        if self.q:
            qs = qs.filter(nome__istartswith=self.q)

        return qs


class MunicipioAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Municipio.objects.none()

        qs = Municipio.objects.all().order_by('nome')

        uf = self.forwarded.get('uf', None)

        if uf:
            qs = qs.filter(uf_id=uf)

        if self.q:
            qs = qs.filter(nome__istartswith=self.q)

        return qs
