from django.apps import AppConfig


class PaisesMunicipiosConfig(AppConfig):
    name = 'paises_municipios'
