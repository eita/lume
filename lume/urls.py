import debug_toolbar
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.i18n import JavaScriptCatalog

urlpatterns = [
    url(r'^accounts/', include('allauth.urls')),
    # path('accounts/', include('django.contrib.auth.urls')),
    path('markdown/', include('django_markdown.urls')),
    path('', include('main.urls')),
    path('admin/', admin.site.urls),
    # path('anexos/', include('anexos.urls')),
    url(r'^municipios_app/', include('municipios.urls')),
    path('paises_municipios/', include('paises_municipios.urls')),
    path('summernote/', include('django_summernote.urls')),
    path('analiseagregada/', include('analiseagregada.urls')),
    path('linhaterritorial/', include('linhaterritorial.urls')),
    path('conjunto_organizacoes/', include('conjunto_organizacoes.urls')),
    path('i18n/', include('django.conf.urls.i18n'))
]

urlpatterns += [
    path('agroecossistema/', include('agroecossistema.urls_main')),
    path('agroecossistema/<int:agroecossistema_id>/',
         include('agroecossistema.urls')),
    path('<int:agroecossistema_id>/car/<int:ano_car>/economica/',
         include('economica.urls', namespace='economica')),
    path('<int:agroecossistema_id>/car/<int:ano_car>/simulacao/<str:sim_slug>/economica/',
         include('economica.urls', namespace='ecosim')),
    path('<int:agroecossistema_id>/linhadotempo/', include('linhadotempo.urls')),
    path('<int:agroecossistema_id>/cadernodecampo/',
         include('cadernodecampo.urls')),
    path('<int:agroecossistema_id>/qualitativa/', include('qualitativa.urls')),
    path('<int:agroecossistema_id>/anexos/', include('anexos.urls')),
]

if settings.DEBUG:
    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls)),
    ]

urlpatterns += [
    path('jsi18n/', JavaScriptCatalog.as_view(), name='javascript-catalog')
]

urlpatterns += staticfiles_urlpatterns()
if settings.LOCAL:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
