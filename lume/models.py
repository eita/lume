from django.contrib.gis.db import models

from django_archive_mixin.mixins import ArchiveMixin


class LumeModel(ArchiveMixin, models.Model):
    class Meta:
        abstract = True
