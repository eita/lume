from django.urls import path

from . import views

app_name = 'conjunto_organizacoes'

urlpatterns = (

    # administrar conjuntos organizacao
    path('listar/',
         views.ConjuntoOrganizacoesListView.as_view(),
         name="listar"),
    path('criar/',
         views.ConjuntoOrganizacoesCreateView.as_view(),
         name="criar"),
    path('<int:pk>/alterar/',
         views.ConjuntoOrganizacoesUpdateView.as_view(),
         name="alterar"),
    path('<int:pk>/deletar/',
         views.ConjuntoOrganizacoesDeleteView.as_view(),
         name="deletar"),

    # gerenciar analises
    path('<int:pk>/eventos-gatilho/',
         views.ConjuntoOrganizacoesEventosGatilhoView.as_view(),
         name="eventos-gatilho"),

    # visualizar analises
    path('<int:pk>/',
         views.ConjuntoOrganizacoesAnalisesView.as_view(),
         name="analises"),
)
