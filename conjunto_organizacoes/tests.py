from django.conf import settings
from django.test import TestCase, RequestFactory, Client
from django.urls import reverse

from model_bakery import baker

from . import views, models


class ConjuntoOrganizacoesBaseTestes(TestCase):

    def setUp(self):
        self.conjunto_orgs = baker.make('ConjuntoOrganizacoes')

        membro_conjunto = self.conjunto_orgs.adicionar_membro(baker.make('Usuaria'))
        admin_conjunto = self.conjunto_orgs.adicionar_admin(baker.make('Usuaria'))
        admin_geral = self.adicionar_admin_geral()

        self.anonima = Client()
        self.client_membro_conjunto = self.client_by_user(membro_conjunto)
        self.client_admin_conjunto = self.client_by_user(admin_conjunto)
        self.client_admin_geral = self.client_by_user(admin_geral)

    def adicionar_admin_geral(self):
        user = baker.make('Usuaria',
                               is_superuser=False,
                               is_active=True)
        grupo = baker.make('Group', name='admin')
        user.groups.add(grupo)
        user.save()
        return user

    def client_by_user(self, user):
        c = Client()
        p = 'password'
        user.set_password(p)
        user.save()
        c.login(**{'username': user.username,
                   'password': p})
        return c


class ConjuntoOrganizacoesAdministracaoTests(ConjuntoOrganizacoesBaseTestes):

    # Administrar conjunto de organizacoes (superuser / admin geral)

    def test_criacao_conjunto_organizacoes(self):
        url = reverse('conjunto_organizacoes:criar')
        membro_conjunto = self.client_membro_conjunto.get(url)
        admin_conjunto = self.client_admin_conjunto.get(url)
        admin_geral = self.client_admin_geral.get(url)

        # teste de acesso
        self.assertEqual(membro_conjunto.status_code, 403)
        self.assertEqual(admin_conjunto.status_code, 403)
        self.assertEqual(admin_geral.status_code, 200)

        # teste template
        self.assertContains(admin_geral, 'Conjunto de Organizações')

        # teste form
        form_kwargs = {'nome': 'teste', 'descricao': 'teste'}
        admin_geral_post = self.client_admin_geral.post(url, **form_kwargs)
        self.assertEqual(admin_geral_post.status_code, 200)
        membro_conjunto_post = self.client_membro_conjunto.post(url, **form_kwargs)
        self.assertEqual(membro_conjunto_post.status_code, 403)

        # teste persistencia

    def test_alteracao_conjunto_organizacoes(self):
        url = reverse('conjunto_organizacoes:alterar',
                      args=[self.conjunto_orgs.id])
        membro_conjunto = self.client_membro_conjunto.get(url)
        admin_conjunto = self.client_admin_conjunto.get(url)
        admin_geral = self.client_admin_geral.get(url)

        # teste de acesso
        self.assertEqual(membro_conjunto.status_code, 403)
        self.assertEqual(admin_conjunto.status_code, 403)
        self.assertEqual(admin_geral.status_code, 200)

        # teste template
        self.assertContains(admin_geral, 'Conjunto de Organizações')

        # teste form

    def test_deletar_conjunto_organizacoes(self):
        url = reverse('conjunto_organizacoes:deletar',
                      args=[self.conjunto_orgs.id])
        membro_conjunto = self.client_membro_conjunto.get(url)
        admin_conjunto = self.client_admin_conjunto.get(url)
        admin_geral = self.client_admin_geral.get(url)

        # teste de acesso
        self.assertEqual(membro_conjunto.status_code, 403)
        self.assertEqual(admin_conjunto.status_code, 403)
        self.assertEqual(admin_geral.status_code, 200)


    # Gerenciar composição do conjunto de organizações (superuser / admin geral)

    def test_alteracao_da_composicao_do_conjunto_organizacoes(self):
        organizacao = baker.make('Organizacao')
        conjunto_organizacoes = baker.make('ConjuntoOrganizacoes')
        conjunto_organizacoes.composicao.add(organizacao)
        conjunto_organizacoes.save()

        # teste de acesso
        # self.assertEqual(response.status_code, 200)

        # teste persistencia
        # self.assertTrue(self.analiseagregada_a.organizacao != self.analiseagregada_b.organizacao)

        # teste permissoes

    def test_alteracao_de_usuarias_do_conjunto_organizacoes(self):
        pass


class ConjuntoOrganizacoesGerenciamentoTests(ConjuntoOrganizacoesBaseTestes):
    # Gerenciar analises e eventos do conjunto de organizações (" + admin_conjunto_organizacoes)
    pass

    ## criar_analise_agregada_do_conjunto_de_organizacoes

    ## editar_analise_agregada_do_conjunto_de_organizacoes

    ## deletar_analise_agregada_do_conjunto_de_organizacoes

    ## criar_evento_gatilho_do_conjunto_de_organizacoes

    ## editar_evento_gatilho_do_conjunto_de_organizacoes

    ## deletar_evento_gatilho_do_conjunto_de_organizacoes


class ConjuntoOrganizacoesAnalisesTests(ConjuntoOrganizacoesBaseTestes):

    # Dashboard do conjunto de organizações com as análises (" + user_conjunto_organizacoes)

    def test_listar_conjunto_organizacoes(self):
        url = reverse('conjunto_organizacoes:listar')
        membro_conjunto = self.client_membro_conjunto.get(url)
        admin_conjunto = self.client_admin_conjunto.get(url)
        admin_geral = self.client_admin_geral.get(url)

        # teste de acesso
        self.assertEqual(membro_conjunto.status_code, 200)
        self.assertEqual(admin_conjunto.status_code, 200)
        self.assertEqual(admin_geral.status_code, 200)

    def test_acesso_dashboard_de_analises_do_conjunto_organizacoes(self):

        url = reverse('conjunto_organizacoes:analises',
                      args=[self.conjunto_orgs.id])
        membro_conjunto = self.client_membro_conjunto.get(url)
        admin_conjunto = self.client_admin_conjunto.get(url)

        # teste de acesso
        self.assertEqual(membro_conjunto.status_code, 200)
        self.assertEqual(admin_conjunto.status_code, 200)

        # teste template
        self.assertTemplateUsed(membro_conjunto, views.ConjuntoOrganizacoesAnalisesView.template_name)
        self.assertContains(membro_conjunto, 'Análise')

        # teste permissao

    ## acessar_analise_do_conjunto_de_organizacoes

