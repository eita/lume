from django.apps import AppConfig


class ConjuntoOrganizacoesConfig(AppConfig):
    name = 'conjunto_organizacoes'
