from django.urls import reverse_lazy
from django.views.generic import (DetailView, CreateView,
                                  ListView, UpdateView,
                                  DeleteView)
from rules.contrib.views import PermissionRequiredMixin

from formsetview.views.mixins import FormSetViewMixin
from .models import ConjuntoOrganizacoes
from . import forms


# Administrar Conjunto Organizações

class ConjuntoOrganizacoesBaseFormView(PermissionRequiredMixin, FormSetViewMixin):
    model = ConjuntoOrganizacoes
    permission_required = 'conjunto_organizacoes.administrar'
    form_class = forms.ConjuntoOrganizacoesModelForm
    success_url = reverse_lazy('conjunto_organizacoes:listar')
    formsets = [(forms.usuarias_conjunto_form_set, {'title': ' ', 'is_modal': False}),]


class ConjuntoOrganizacoesCreateView(ConjuntoOrganizacoesBaseFormView, CreateView):
    pass


class ConjuntoOrganizacoesUpdateView(ConjuntoOrganizacoesBaseFormView, UpdateView):
    pass


class ConjuntoOrganizacoesDeleteView(PermissionRequiredMixin, DeleteView):
    model = ConjuntoOrganizacoes
    permission_required = 'conjunto_organizacoes.administrar'
    success_url = reverse_lazy('conjunto_organizacoes:listar')


# Gerenciar Analises do Conjunto Organizações

# listar, criar, alterar, deletar evento gatilho de nível elevado (admin do conjunto)
from linhadotempo.models import EventoGatilho

class ConjuntoOrganizacoesEventosGatilhoView(PermissionRequiredMixin, DetailView):
    model = ConjuntoOrganizacoes
    template_name = 'conjunto_organizacoes/dashboard_eventos_gatilho.html'
    permission_required = 'conjunto_organizacoes.gerenciar_conjunto'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['eventos_gatilho'] = self.get_conjunto_eventos_gatilho()
        context['modulo'] = 'conjunto_organizacoes'
        context['submodulo'] = 'eventos-gatilho'
        return context

    def get_conjunto_eventos_gatilho(self):
        # TODO corrigir a seguinte queryset
        return EventoGatilho.objects.all()


# criar, alterar, deletar analise agregada de nível elevado (admin do conjunto)


# Acessar Analises do Conjunto Organizações

class ConjuntoOrganizacoesListView(PermissionRequiredMixin, ListView):
    model = ConjuntoOrganizacoes
    fields = ('nome', 'descricao')
    permission_required = 'conjunto_organizacoes.listagem'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['modulo'] = 'conjunto_organizacoes'
        context['own_conjuntoorganizacoes'] = self.get_own_conjuntoorganizacoes()
        return context

    def get_queryset(self):
        if self.request.user.has_perm('conjunto_organizacoes.administrar'):
            return ConjuntoOrganizacoes.objects.all()
        elif self.request.user.is_anonymous:
            return ConjuntoOrganizacoes.objects.none()
        else:
            return ConjuntoOrganizacoes.objects.filter(
                usuarias__usuaria=self.request.user.id)

    def get_own_conjuntoorganizacoes(self):
        if self.request.user.is_anonymous:
            return None
        return ConjuntoOrganizacoes.objects.filter(autoria=self.request.user)


# listar analise agregada de nível elevado (membro do conjunto)

from analiseagregada.models import Analiseagregada

class ConjuntoOrganizacoesAnalisesView(PermissionRequiredMixin, DetailView):
    model = ConjuntoOrganizacoes
    template_name = 'conjunto_organizacoes/dashboard_analises.html'
    permission_required = 'conjunto_organizacoes.acessar_analises'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['analises'] = self.get_conjunto_analises()
        context['modulo'] = 'conjunto_organizacoes'
        context['submodulo'] = 'analises'
        return context

    def get_conjunto_analises(self):
        # TODO corrigir a seguinte queryset
        return Analiseagregada.objects.all()
