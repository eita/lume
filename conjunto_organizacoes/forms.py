from django import forms

from dal import autocomplete

from main.forms import LumeModelForm, LumeForm
from .models import ConjuntoOrganizacoes, ConjuntoOrganizacoesUsuaria


class ConjuntoOrganizacoesModelForm(forms.ModelForm):

    class Meta:
        model = ConjuntoOrganizacoes
        fields = ('nome', 'descricao', 'composicao')
        widgets = {
            'descricao': forms.Textarea(attrs={'cols': 80, 'rows': 2}),
            'composicao': autocomplete.ModelSelect2Multiple(url='organizacao_autocomplete')
        }


class ConjuntoOrganizacoesUsuariaModelForm(forms.ModelForm):

    class Meta:
        model = ConjuntoOrganizacoesUsuaria
        exclude = ('conjunto_organizacao',)


def usuarias_conjunto_form_set(model):

    return forms.inlineformset_factory(
        model,
        ConjuntoOrganizacoesUsuaria,
        form=ConjuntoOrganizacoesUsuariaModelForm, extra=1)

