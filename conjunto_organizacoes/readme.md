# Conjunto de organizações

o novo tipo de usuário integrante do "Conjunto de Organizações"
é um usuário que tão somente faz consultas de análises
agregadas inter-organizações, e cria eventos-gatilho de
nível mais elevado que passa a ser compartilhado entre organizações

Existe o admin deste tipo de objeto, "conjunto de organizações".
O usuário que faz parte do conjunto de organizações, pode ver somente 
análises agregadas geradas pela agregação de organizações.
O admin do conjunto de organizações pode adicionar usuários a este conjunto, 
pode CRIAR análises agregadas e pode CRIAR evento-gatilho deste nível. O evento gatilho 
do conjunto de organizações é acessível para qualquer dos agroecossistemas das 
organizações que fazem parte. É um evento gatilho que vai além da organização.

* Entidade ConjuntoOrganizacoes que terá uma relação m2m com Organizacao. 
* Esta entidade também terá nome e descricao, além de autor.
* Sobre permissão para criação de ConjuntoOrganizacoes fica para superuser e integrante do grupo admin.
* Para visualização desses conjuntos também fica com esses tipos de usuárias;
* Para as ações que essas usuárias podem fazer:
    * faz consultas de análises agregadas inter-organizações
    * cria análises agregadas inter-organizações
    * cria eventos-gatilho de nível mais elevado que passa a ser compartilhado entre organizações

