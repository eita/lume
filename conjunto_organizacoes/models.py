from django.conf import settings
from django.db import models
from django.urls import reverse

from lume.models import LumeModel
from main.models import Organizacao


class ConjuntoOrganizacoes(LumeModel):
    nome = models.CharField(max_length=80)
    descricao = models.TextField('Descrição', null=True, blank=True)
    autoria = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        null=True, blank=True,
        related_name="conjunto_organizacoes")
    composicao = models.ManyToManyField(Organizacao, verbose_name='Composição')

    def __str__(self):
        return f"{self.nome}"

    def get_absolute_url(self):
        return reverse('conjunto_organizacoes:analises', args=(self.pk,))

    def get_update_url(self):
        return reverse('conjunto_organizacoes:alterar', args=(self.pk,))

    def get_delete_url(self):
        return reverse('conjunto_organizacoes:deletar', args=(self.pk,))

    def adicionar_membro(self, usuaria):
        return self.adicionar_usuaria(usuaria)

    def adicionar_admin(self, usuaria):
        return self.adicionar_usuaria(usuaria, is_admin=True)

    def adicionar_usuaria(self, usuaria, is_admin=False):
        conjunto_usuarias = ConjuntoOrganizacoesUsuaria(
            usuaria=usuaria,
            conjunto_organizacao=self,
            is_admin=is_admin)
        conjunto_usuarias.save()
        self.usuarias.add(conjunto_usuarias)

        return usuaria


class ConjuntoOrganizacoesUsuaria(LumeModel):
    conjunto_organizacao = models.ForeignKey(
        ConjuntoOrganizacoes,
        on_delete=models.CASCADE,
        related_name="usuarias")
    usuaria = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name='usuária',
        on_delete=models.CASCADE,
        related_name="conjunto_orgs")
    is_admin = models.BooleanField('administra o conjunto', default=False)

