from __future__ import absolute_import

import rules
from . import models


eh_admin_geral = rules.is_group_member('admin')

@rules.predicate
def eh_superusuario(usuaria):
    return usuaria.is_superuser

@rules.predicate
def eh_membro_do_conjunto_orgs(usuaria, conjunto_organizacoes):
    return conjunto_organizacoes.usuarias.filter(usuaria=usuaria)

@rules.predicate
def eh_membro_algum_conjunto_orgs(usuaria):
    try:
        return models.ConjuntoOrganizacoes.objects.filter(usuarias__usuaria=usuaria)
    except:
        return False

@rules.predicate
def eh_admin_do_conjunto_orgs(usuaria, conjunto_organizacoes):
    return conjunto_organizacoes.usuarias.filter(usuaria=usuaria,
                                                 is_admin=True)

rules.add_perm(
    'conjunto_organizacoes.listagem',
    eh_superusuario | eh_admin_geral | eh_membro_algum_conjunto_orgs)

rules.add_perm(
    'conjunto_organizacoes.acessar_analises',
    eh_superusuario | eh_admin_geral | eh_membro_do_conjunto_orgs)

rules.add_perm(
    'conjunto_organizacoes.gerenciar_conjunto',
    eh_admin_do_conjunto_orgs)

rules.add_perm(
    'conjunto_organizacoes.administrar',
    eh_superusuario | eh_admin_geral)

