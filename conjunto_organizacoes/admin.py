from django.contrib import admin

from .models import *


class ConjuntoUsuariasInline(admin.TabularInline):
    model = ConjuntoOrganizacoesUsuaria
    can_delete = True
    extra=3
    fields = ('usuaria', 'is_admin')
    autocomplete_fields = ('usuaria',)


class ConjuntoOrganizacoesAdmin(admin.ModelAdmin):
    excludes = ()
    inlines = (ConjuntoUsuariasInline,)
    autocomplete_fields = ('autoria',)
    filter_horizontal = ('composicao',)

admin.site.register(ConjuntoOrganizacoes, ConjuntoOrganizacoesAdmin)

