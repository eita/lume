# debug NoReverseMatch:
# https://overiq.com/django-1-11/creating-urls-and-custom-response/

from django.urls import path
from . import views
from anexos import views as anexosViews

app_name = 'linhadotempo'
urlpatterns = (
    # urls for Evento
    path('', views.EventoListView.as_view(), name='evento_list'),

    # painel agroecossistema
    path('painel/linhadotempo/',
         views.PainelAgroecossistemaEventoListView.as_view(),
         name='painel_evento_list'),
    path('painel/evento/create/',
         views.PainelAgroecossistemaEventoCreateView.as_view(),
         name="painel_evento_create"),

    path('eventos/detail/<int:pk>/', views.EventoDetailView.as_view(),
         name='evento_detail'),
    path('eventos/delete/<int:pk>/', views.EventoDeleteView.as_view(),
         name='evento_delete'),
    path('eventos/anexos/<int:pk>/', anexosViews.AnexoListView.as_view(), {'related_type': 'linhadotempo', 'related_model': 'Evento'}, name='evento_anexos'),
    path('eventos/rest/update', views.EventoRestUpdateView.as_view(), name="evento_rest_update"),
    path('eventos/rest/delete', views.EventoRestDeleteView.as_view(), name="evento_rest_delete"),
)

urlpatterns += (
    # urls for EventoGrupo
    path('eventogrupo/', views.EventoGrupoListView.as_view(), name='eventogrupo_list'),
    path('eventogrupo/create/', views.EventoGrupoCreateView.as_view(), name='eventogrupo_create'),
    path('eventogrupo/detail/<int:pk>/', views.EventoGrupoDetailView.as_view(), name='eventogrupo_detail'),
    path('eventogrupo/update/<int:pk>/', views.EventoGrupoUpdateView.as_view(), name='eventogrupo_update'),
)
