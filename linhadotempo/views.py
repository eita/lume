import json
import logging

from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.utils.text import slugify
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, CreateView, DetailView, UpdateView
from django_markdown.utils import markdown
from django.utils.translation import gettext as _
from rules.contrib.views import PermissionRequiredMixin

from linhadotempo.forms import EventoForm, EventoGrupoForm, EventoGatilhoForm
from linhadotempo.models import Evento, TipoDimensao, Dimensao, EventoGrupo, EventoGatilho
from linhadotempo.mixins import AjaxableResponseMixin
from agroecossistema.models import Agroecossistema
from main.views import LumeDeleteView
from main.models import Organizacao, OrganizacaoUsuaria
from main.utils import getHelperI18n


class EventoGatilhoListView(PermissionRequiredMixin, ListView):
    model = EventoGatilho
    permission_required = 'eventogatilho.visualizar'
    titulo = _('Eventos-gatilho')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['modulo'] = 'linhadotempo'
        context['enable_datatable_export'] = True
        context['titulo'] = self.titulo
        return context

    def get_queryset(self):
        user = self.request.user
        eh_admin_geral = user.groups.filter(name='admin')
        if eh_admin_geral or user.is_superuser:
            return EventoGatilho.objects.all()

        user_organizacoes = OrganizacaoUsuaria.objects.filter(
            usuaria_id=user.id, is_admin=True
        ).values_list('organizacao__id', flat=True)

        return EventoGatilho.objects.filter(organizacao__in=user_organizacoes)


class EventoGatilhoDetailView(PermissionRequiredMixin, DetailView):
    model = EventoGatilho
    permission_required = 'eventogatilho.visualizar'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['modulo'] = 'linhadotempo'
        return context


class EventoGatilhoUpdateView(PermissionRequiredMixin, UpdateView):
    model = EventoGatilho
    form_class = EventoGatilhoForm
    titulo = _('Editar evento-gatilho')
    permission_required = 'eventogatilho.visualizar'
    success_url = reverse_lazy('eventogatilho_list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = self.titulo
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'prefix': {'user': self.request.user}})
        kwargs.update({'can_delete': self.request.user.has_perm(
            'eventogatilho.visualizar', self.get_object())})
        return kwargs


class EventoGatilhoCreateView(PermissionRequiredMixin, CreateView):
    model = EventoGatilho
    form_class = EventoGatilhoForm
    titulo = _('Novo evento-gatilho')
    permission_required = 'eventogatilho.visualizar'
    success_url = reverse_lazy('eventogatilho_list')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'prefix': {'user': self.request.user}})
        return kwargs


class EventoGatilhoDeleteView(PermissionRequiredMixin, LumeDeleteView):
    model = EventoGatilho
    permission_required = 'eventogatilho.visualizar'

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class EventoListView(PermissionRequiredMixin, ListView):
    model = Evento
    form_class = EventoForm
    agroecossistema = None
    grupos = None
    skipped_years = None
    permission_required = 'agroecossistema.visualizar'

    def get_permission_object(self):
        agroecossistema = get_object_or_404(
            Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    def get_queryset(self):
        agroecossistema = get_object_or_404(
            Agroecossistema, id=self.kwargs['agroecossistema_id'])
        # return Evento.objects.select_related('evento_gatilho').filter(agroecossistema=agroecossistema)
        return Evento.objects.filter(agroecossistema=agroecossistema).prefetch_related('anexos').select_related('dimensao', 'evento_gatilho', 'criado_por', 'agroecossistema')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['agroecossistema'] = Agroecossistema.objects.get(
            id=self.kwargs['agroecossistema_id'])
        context['grupos'] = json.dumps(
            self.get_grupos(), cls=DjangoJSONEncoder)
        context['skipped_years'] = json.dumps(
            self.get_skipped_years(), cls=DjangoJSONEncoder)
        context['modulo'] = 'linhadotempo'
        context['form'] = EventoForm(
            prefix=self.kwargs['agroecossistema_id'], instance=None)
        context['titulo'] = context['agroecossistema'].__str__()
        return context

    def get_grupos(self):
        tipos_dimensao = TipoDimensao.objects.all()
        grupos = []
        for tipo_dimensao in tipos_dimensao:
            nested_groups = []
            for dimensao in tipo_dimensao.dimensoes.all():
                nested_groups.append(dimensao.id)
                tipo_dimensao_slug = slugify(tipo_dimensao.nome)
                helperI18n = getHelperI18n(
                    'linhadotempo_helpers', dimensao, dimensao.nome)
                contentI18n = f"{helperI18n['nome']} <span></span>" if helperI18n['nome'] else helperI18n['ajuda']
                try:
                    help_md = markdown(helperI18n['ajuda'])
                except:
                    help_md = ''
                grupos.append({
                    'id': dimensao.id,
                    'order': dimensao.ordem,
                    'content': contentI18n,
                    'help': help_md,
                    'className': f"{slugify(tipo_dimensao_slug)} group-{dimensao.id}"
                })

            helperI18n = getHelperI18n(
                'linhadotempo_helpers', tipo_dimensao, tipo_dimensao.nome)
            try:
                content = helperI18n['nome']
            except:
                content = ''
            grupos.append({
                'id': 100 + tipo_dimensao.id,
                'content': content,
                'nestedGroups': nested_groups,
                'className': slugify(tipo_dimensao.nome)
            })
        return grupos

    def get_skipped_years(self):
        years = {}
        for object in self.object_list:
            if object.data_fim is None:
                years[object.data_inicio] = True
            else:
                for year in range(object.data_inicio, object.data_fim + 1):
                    years[year] = True

        skipped_years = []
        last_year = None
        for year in sorted(years):
            if last_year is not None and year != last_year + 1:
                skipped_years.append(
                    {'start': str(last_year + 1) + '-01-01', 'end': str(year) + '-01-01'})
            last_year = year
        return skipped_years


class EventoDetailView(PermissionRequiredMixin, DetailView):
    model = Evento
    permission_required = 'agroecossistema.visualizar'

    def get_permission_object(self):
        agroecossistema = get_object_or_404(
            Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['agroecossistema'] = Agroecossistema.objects.get(
            id=self.kwargs['agroecossistema_id'])
        context['modulo'] = 'linhadotempo'
        return context


class EventoDeleteView(PermissionRequiredMixin, LumeDeleteView):
    model = Evento
    permission_required = 'agroecossistema.alterar'

    def get_permission_object(self):
        agroecossistema = get_object_or_404(
            Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(EventoDeleteView, self).dispatch(*args, **kwargs)


class EventoRestUpdateView(PermissionRequiredMixin, AjaxableResponseMixin, UpdateView):
    model = Evento
    form_class = EventoForm
    permission_required = 'agroecossistema.cadastrar'
    success_message = _("Atualizado!")

    def get_permission_object(self):
        agroecossistema = get_object_or_404(
            Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(EventoRestUpdateView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        data = self.request.POST

        evento_gatilho = None
        data_fim = None

        if data.get('evento_gatilho') != '':
            evento_gatilho = EventoGatilho.objects.get(
                pk=data.get('evento_gatilho'))

        if data.get('data_fim'):
            data_fim = int(data.get('data_fim'))

        dimensao = Dimensao.objects.get(pk=int(data.get('dimensao')))

        if data.get('id'):
            obj, created = Evento.objects.get_or_create(
                id=data.get('id')
            )
            obj.titulo = data.get('titulo')
            obj.dimensao = dimensao
            obj.data_inicio = int(data.get('data_inicio'))
            obj.data_fim = data_fim
            obj.detalhes = data.get('detalhes')
            obj.evento_gatilho = evento_gatilho
            return obj

        obj, created = Evento.objects.get_or_create(
            agroecossistema_id=int(data.get('agroecossistema_id')),
            titulo=data.get('titulo'),
            dimensao=dimensao,
            data_inicio=int(data.get('data_inicio')),
            data_fim=data_fim,
            detalhes=data.get('detalhes'),
            evento_gatilho=evento_gatilho
        )
        return obj


class EventoRestDeleteView(PermissionRequiredMixin, AjaxableResponseMixin, LumeDeleteView):
    model = Evento
    permission_required = 'agroecossistema.cadastrar'

    def get_permission_object(self):
        agroecossistema = get_object_or_404(
            Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(EventoRestDeleteView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        data = self.request.POST

        obj = Evento.objects.get(id=data.get('id'))

        return obj


class EventoGrupoListView(PermissionRequiredMixin, ListView):
    model = EventoGrupo
    permission_required = 'agroecossistema.visualizar'

    def get_permission_object(self):
        agroecossistema = get_object_or_404(
            Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['agroecossistema'] = Agroecossistema.objects.get(
            id=self.kwargs['agroecossistema_id'])
        context['modulo'] = 'linhadotempo'
        return context


class EventoGrupoCreateView(CreateView):
    model = EventoGrupo
    form_class = EventoGrupoForm
    titulo = _('Novo agrupamento de eventos')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['agroecossistema'] = Agroecossistema.objects.get(
            id=self.kwargs['agroecossistema_id'])
        context['titulo'] = self.titulo
        context['modulo'] = 'linhadotempo'
        return context


class EventoGrupoDetailView(PermissionRequiredMixin, DetailView):
    model = EventoGrupo
    permission_required = 'agroecossistema.visualizar'

    def get_permission_object(self):
        agroecossistema = get_object_or_404(
            Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['agroecossistema'] = Agroecossistema.objects.get(
            id=self.kwargs['agroecossistema_id'])
        context['modulo'] = 'linhadotempo'
        return context


class EventoGrupoUpdateView(UpdateView):
    model = EventoGrupo
    form_class = EventoGrupoForm
    titulo = _('Editar agrupamento de eventos')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['agroecossistema'] = Agroecossistema.objects.get(
            id=self.kwargs['agroecossistema_id'])
        context['titulo'] = self.titulo
        context['modulo'] = 'linhadotempo'
        return context


class PainelAgroecossistemaEventoListView(EventoListView):
    template_name = "linhadotempo/painel_agroecossistema_eventos.html"

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.order_by('-data_inicio')
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        linhadotempo = {}
        data_inicio_registro_atual = ''
        for evento in self.object_list:
            data_inicio = str(evento.data_inicio)
            if data_inicio != data_inicio_registro_atual:
                data_inicio_registro_atual = data_inicio
                linhadotempo[data_inicio] = []
            linhadotempo[data_inicio].append(evento)
        context['linhadotempo']=linhadotempo
        return context


class PainelAgroecossistemaEventoCreateView(
        PermissionRequiredMixin,
        AjaxableResponseMixin,
        CreateView):
    model = Evento
    form_class = EventoForm
    permission_required = 'agroecossistema.cadastrar'
    success_message = _("Atualizado!")
    template_name = "linhadotempo/painel_agroecossistema_evento_form.html"

    def get_permission_object(self):
        agroecossistema = get_object_or_404(
            Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    def get_prefix(self):
        return self.kwargs['agroecossistema_id']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['agroecossistema_id'] = self.kwargs['agroecossistema_id']
        return context
