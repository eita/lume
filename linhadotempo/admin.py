from django.contrib import admin

from linhadotempo.models import TipoDimensao, Dimensao, Evento, EventoGatilho

# Register your models here.

admin.site.register(TipoDimensao)
admin.site.register(Dimensao)
admin.site.register(Evento)
admin.site.register(EventoGatilho)

