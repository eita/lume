# Generated by Django 2.1.5 on 2020-02-06 14:42

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0021_remove_organizacaousuaria_deleted'),
        ('linhadotempo', '0016_auto_20190512_1235'),
    ]

    operations = [
        migrations.CreateModel(
            name='EventoGatilho',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(blank=True, editable=False, null=True)),
                ('titulo', models.CharField(max_length=80)),
                ('detalhes', models.TextField()),
                ('dimensao', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='linhadotempo.Dimensao')),
                ('organizacao', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='main.Organizacao')),
            ],
            options={
                'ordering': ('titulo',),
            },
        ),
    ]
