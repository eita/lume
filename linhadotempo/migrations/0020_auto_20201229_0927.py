# Generated by Django 2.1.5 on 2020-12-29 12:27

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
from django_markdown.models import MarkdownField

class Migration(migrations.Migration):

    dependencies = [
        ('linhadotempo', '0019_auto_20200211_1814'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dimensao',
            name='descricao',
            field=MarkdownField(blank=True, null=True, verbose_name='descrição'),
        ),
        migrations.AlterField(
            model_name='dimensao',
            name='nome',
            field=models.CharField(max_length=80, verbose_name='nome'),
        ),
        migrations.AlterField(
            model_name='dimensao',
            name='ordem',
            field=models.SmallIntegerField(null=True, verbose_name='ordem'),
        ),
        migrations.AlterField(
            model_name='dimensao',
            name='tipo_dimensao',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='linhadotempo.TipoDimensao', verbose_name='tipo da dimensão'),
        ),
        migrations.AlterField(
            model_name='evento',
            name='agroecossistema',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='eventos', to='agroecossistema.Agroecossistema', verbose_name='agroecossistema'),
        ),
        migrations.AlterField(
            model_name='evento',
            name='criacao',
            field=models.DateTimeField(auto_now_add=True, verbose_name='criação'),
        ),
        migrations.AlterField(
            model_name='evento',
            name='criado_por',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to=settings.AUTH_USER_MODEL, verbose_name='criado por'),
        ),
        migrations.AlterField(
            model_name='evento',
            name='data_fim',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='data final'),
        ),
        migrations.AlterField(
            model_name='evento',
            name='data_inicio',
            field=models.PositiveSmallIntegerField(verbose_name='data inicial'),
        ),
        migrations.AlterField(
            model_name='evento',
            name='detalhes',
            field=models.TextField(verbose_name='detalhes'),
        ),
        migrations.AlterField(
            model_name='evento',
            name='dimensao',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='linhadotempo.Dimensao', verbose_name='dimensão'),
        ),
        migrations.AlterField(
            model_name='evento',
            name='evento_gatilho',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='eventos_relacionados', to='linhadotempo.EventoGatilho', verbose_name='evento-gatilho'),
        ),
        migrations.AlterField(
            model_name='evento',
            name='titulo',
            field=models.CharField(max_length=80, verbose_name='título'),
        ),
        migrations.AlterField(
            model_name='evento',
            name='ultima_alteracao',
            field=models.DateTimeField(auto_now=True, verbose_name='última alteração'),
        ),
        migrations.AlterField(
            model_name='eventogatilho',
            name='detalhes',
            field=models.TextField(verbose_name='detalhes'),
        ),
        migrations.AlterField(
            model_name='eventogatilho',
            name='dimensao',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='linhadotempo.Dimensao', verbose_name='dimensão'),
        ),
        migrations.AlterField(
            model_name='eventogatilho',
            name='organizacao',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='main.Organizacao', verbose_name='organização'),
        ),
        migrations.AlterField(
            model_name='eventogatilho',
            name='titulo',
            field=models.CharField(max_length=80, verbose_name='título'),
        ),
        migrations.AlterField(
            model_name='eventogrupo',
            name='agroecossistema',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='agroecossistema.Agroecossistema', verbose_name='agroecossistema'),
        ),
        migrations.AlterField(
            model_name='eventogrupo',
            name='criacao',
            field=models.DateTimeField(auto_now_add=True, verbose_name='criação'),
        ),
        migrations.AlterField(
            model_name='eventogrupo',
            name='descricao',
            field=models.TextField(blank=True, null=True, verbose_name='descrição'),
        ),
        migrations.AlterField(
            model_name='eventogrupo',
            name='eventos',
            field=models.ManyToManyField(related_name='grupos', to='linhadotempo.Evento', verbose_name='eventos'),
        ),
        migrations.AlterField(
            model_name='eventogrupo',
            name='nome',
            field=models.CharField(max_length=255, verbose_name='nome'),
        ),
        migrations.AlterField(
            model_name='eventogrupo',
            name='ultima_alteracao',
            field=models.DateTimeField(auto_now=True, verbose_name='última alteração'),
        ),
        migrations.AlterField(
            model_name='tipodimensao',
            name='nome',
            field=models.CharField(max_length=80, verbose_name='nome'),
        ),
    ]
