from bootstrap_datepicker_plus import DatePickerInput, YearPickerInput
from django import forms
from django.urls import reverse
from django.utils.translation import gettext as _
from django_summernote.widgets import SummernoteInplaceWidget

from agroecossistema.models import Agroecossistema
from linhadotempo.models import Evento, EventoGrupo, EventoGatilho
from main.forms import LumeModelForm


class EventoGatilhoForm(LumeModelForm):
    def __init__(self, *args, **kwargs):
        self.can_delete = kwargs.pop('can_delete', False)
        if self.can_delete:
            self.delete_confirm_message = _(
                'Deseja realmente apagar este evento-gatilho?')
            self.delete_url = reverse('eventogatilho_delete',
                                      kwargs={'pk': kwargs.get('instance').id})
            self.delete_success_url = reverse('eventogatilho_list')

        user = kwargs['prefix']['user']
        kwargs['prefix'] = None

        super(EventoGatilhoForm, self).__init__(*args, **kwargs)

        # considerar campo Organização suas regras
        eh_admin_geral = user.groups.filter(name='admin')
        if not eh_admin_geral:
            user_orgs_admin = user.org_usuarias.filter(is_admin=True)

            if user.is_superuser:
                pass
            elif len(user_orgs_admin) > 1:
                user_orgs = user_orgs_admin.values_list(
                    'organizacao__id', flat=True)
                self.fields['organizacao'].queryset = self.fields['organizacao'].queryset.filter(
                    id__in=user_orgs)
            else:
                # self.fields['organizacao'].widget = forms.HiddenInput()
                self.fields['organizacao'].queryset = self.fields['organizacao'].queryset.filter(
                    id=user_orgs_admin[0].organizacao.id)
                self.fields['organizacao'].widget.attrs['readonly'] = True
                self.fields['organizacao'].initial = user_orgs_admin[0].organizacao.id

    class Meta:
        model = EventoGatilho
        fields = ['organizacao', 'titulo', 'dimensao', 'detalhes']
        widgets = {'detalhes': SummernoteInplaceWidget()}
        labels = {
            'organizacao': _('Organização'),
            'titulo': _('Título'),
            'detalhes': _('Detalhes'),
            'dimensao': _('Dimensão')
        }


class EventoForm(LumeModelForm):
    is_gatilho = forms.BooleanField(
        label=_('É um evento-gatilho?'), required=False)

    def __init__(self, *args, **kwargs):
        self.can_delete = kwargs.pop('can_delete', False)
        if self.can_delete:
            agroecossistema_id = kwargs.pop('agroecossistema_id')
            self.delete_confirm_message = _(
                'Deseja realmente apagar este evento?')
            self.delete_url = reverse(
                'linhadotempo:evento_delete',
                kwargs={
                    'agroecossistema_id': agroecossistema_id,
                    'pk': kwargs.get('instance').id
                }
            )
            self.delete_success_url = reverse('linhadotempo:evento_list', kwargs={
                                              'agroecossistema_id': agroecossistema_id})
        # recebe o `agroecossistema_id` da view
        agroecossistema_id = kwargs.pop('prefix')

        super(EventoForm, self).__init__(*args, **kwargs)

        # Considera EventoGatilho
        if 'instance' in kwargs and kwargs['instance']:
            self.fields['is_gatilho'].initial = True if kwargs['instance'].evento_gatilho else False
            agroecossistema = kwargs['instance'].agroecossistema
            evento_gatilho = kwargs['instance'].evento_gatilho
        else:
            agroecossistema = Agroecossistema.objects.get(
                id=agroecossistema_id)
            evento_gatilho = None

        # Lista de eventos-gatilho disponíveis
        organizacao = agroecossistema.organizacao

        gatilhos_usados = Evento.objects.filter(
            evento_gatilho__isnull=False,
            agroecossistema=agroecossistema
        ).exclude(
            evento_gatilho=evento_gatilho
        ).values_list('evento_gatilho__id', flat=True)

        evento_gatilho_qs = self.fields['evento_gatilho'].queryset.filter(
            organizacao=organizacao).exclude(id__in=gatilhos_usados)

        # Verifica disponibilidade de gatilho, senão nem apresenta possibilidade
        if len(evento_gatilho_qs):
            self.fields['evento_gatilho'].queryset = evento_gatilho_qs
        else:
            self.fields['is_gatilho'].widget = forms.HiddenInput()
            self.fields['evento_gatilho'].widget = forms.HiddenInput()

        self.fields['agroecossistema'].initial = agroecossistema_id

    class Meta:
        model = Evento
        fields = ['is_gatilho', 'evento_gatilho', 'titulo', 'agroecossistema',
                  'dimensao', 'data_inicio', 'data_fim', 'detalhes']
        widgets = {
            'data_inicio': YearPickerInput(
                options={
                    "format": "YYYY",
                    "locale": "pt-br",
                },
                required=True
            ),
            'agroecossistema': forms.HiddenInput(),
            'detalhes': SummernoteInplaceWidget(),
            'data_fim': YearPickerInput(
                options={
                    "format": "YYYY",
                    "locale": "pt-br",
                }
            )
        }
        labels = {
            'data_inicio': _('Ano'),
            'data_fim': _('Ano final'),
            'titulo': _('Título'),
            'detalhes': _('Detalhes'),
            'dimensao': _('Dimensão')
        }

    def clean_data_inicio(self):
        data = self.cleaned_data['data_inicio']
        # do some stuff
        return data

    def clean_data_fim(self):
        data = self.cleaned_data['data_fim']
        # do some stuff
        return data

    def clean(self):
        data = super().clean()

        if not data.get('is_gatilho', None):
            return data

        evento_gatilho = data.get('evento_gatilho', None)
        if not evento_gatilho:
            self.add_error('evento_gatilho', _('Selecione um evento-gatilho'))

        return data


class EventoGrupoForm(LumeModelForm):
    class Meta:
        model = EventoGrupo
        fields = ['nome', 'descricao', 'eventos']
