import json
from datetime import datetime

from django.conf import settings
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from django.core.serializers.json import DjangoJSONEncoder
from django.db import models
from django.db.models import TextField
from django.urls import reverse
from django_markdown.models import MarkdownField

from django.utils.translation import gettext as _
from django.utils.translation import gettext_lazy as _lazy

from main.models import Organizacao
from anexos.models import AnexoRecipiente
from lume.models import LumeModel
from agroecossistema.models import Agroecossistema
from main.messages_from_db import linhadotempo_helpers


class TipoDimensao(LumeModel):
    nome = models.CharField(max_length=80, verbose_name=_lazy("nome"))

    def __str__(self):
        return self.nome

    class Meta:
        ordering = ("nome",)


class Dimensao(LumeModel):
    nome = models.CharField(max_length=80, verbose_name=_lazy("nome"))
    tipo_dimensao = models.ForeignKey(
        TipoDimensao,
        models.PROTECT,
        verbose_name=_lazy("tipo da dimensão"),
        related_name="dimensoes",
    )
    ordem = models.SmallIntegerField(null=True, verbose_name=_lazy("ordem"))
    descricao = MarkdownField(null=True, blank=True, verbose_name=_lazy("descrição"))

    HELPERS = linhadotempo_helpers

    def __str__(self):
        return self.tipo_dimensao.nome + " / " + self.nome

    class Meta:
        ordering = (
            "tipo_dimensao__nome",
            "ordem",
        )


class EventoGatilho(LumeModel):
    organizacao = models.ForeignKey(
        Organizacao, models.PROTECT, verbose_name=_lazy("organização")
    )
    dimensao = models.ForeignKey(
        Dimensao, models.PROTECT, verbose_name=_lazy("dimensão")
    )
    titulo = models.CharField(max_length=80, verbose_name=_lazy("título"))
    detalhes = TextField(verbose_name=_lazy("detalhes"))

    class Meta:
        ordering = ("titulo",)

    def __str__(self):
        return self.titulo

    def get_absolute_url(self):
        return reverse("eventogatilho_detail", args=(self.pk,))

    def get_update_url(self):
        return reverse("eventogatilho_update", args=(self.pk,))

    def save(self):
        super().save()

        # Atualiza os eventos relacionados ao Evento-gatilho alterado
        self.eventos_relacionados.all().update(
            titulo=self.titulo, dimensao=self.dimensao
        )


class Evento(LumeModel):
    data_inicio = models.PositiveSmallIntegerField(verbose_name=_lazy("data inicial"))
    data_fim = models.PositiveSmallIntegerField(
        null=True, blank=True, verbose_name=_lazy("data final")
    )
    evento_gatilho = models.ForeignKey(
        EventoGatilho,
        models.PROTECT,
        null=True,
        blank=True,
        verbose_name=_lazy("evento-gatilho"),
        related_name="eventos_relacionados",
    )
    titulo = models.CharField(max_length=80, verbose_name=_lazy("título"))
    detalhes = TextField(null=True, blank=True, verbose_name=_lazy("detalhes"))
    dimensao = models.ForeignKey(
        Dimensao, models.PROTECT, verbose_name=_lazy("dimensão")
    )
    agroecossistema = models.ForeignKey(
        Agroecossistema,
        models.CASCADE,
        related_name="eventos",
        verbose_name=_lazy("agroecossistema"),
    )
    criado_por = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        models.DO_NOTHING,
        null=True,
        blank=True,
        verbose_name=_lazy("criado por"),
    )
    anexos = GenericRelation(
        AnexoRecipiente,
        verbose_name=_lazy("anexos"),
        content_type_field="recipiente_content_type",
        object_id_field="recipiente_object_id",
    )
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("última alteração")
    )

    class Meta:
        ordering = ("-pk",)

    def __str__(self):
        return self.titulo

    def __unicode__(self):
        return "%s" % self.pk

    def get_absolute_url(self):
        return (
            reverse(
                "linhadotempo:evento_list",
                kwargs={"agroecossistema_id": self.agroecossistema_id},
            )
            + "?evento_id="
            + str(self.pk)
        )

    def as_visjs_json(self):
        visjs_json = {
            "id": self.id,
            "start": datetime(self.data_inicio, 1, 3),
            "content": self.titulo,
            "group": self.dimensao_id,
            "titulo": self.titulo,
            "detalhes": self.detalhes,
            "anexos_url": self.get_anexos_url(),
            "title": self.titulo,
            "evento_gatilho": (
                None if self.evento_gatilho == None else self.evento_gatilho.id
            ),
            "className": "" if self.evento_gatilho == None else "evento_gatilho",
        }

        if self.criado_por is not None:
            visjs_json["autor"] = self.criado_por.get_username()

        if self.data_fim is not None:
            visjs_json["end"] = datetime(self.data_fim, 12, 30)
        else:
            visjs_json["end"] = datetime(self.data_inicio, 12, 30)

        if (
            self.data_fim != ""
            and self.data_fim is not None
            and self.data_inicio != self.data_fim
        ):
            visjs_json["ano_formatted"] = (
                str(self.data_inicio) + "-" + str(self.data_fim)
            )
        else:
            visjs_json["ano_formatted"] = str(self.data_inicio)

        visjs_json["anexos"] = list(self.get_anexos_list())

        return json.dumps(visjs_json, cls=DjangoJSONEncoder)

    def get_anexos_url(self):
        current_type_id = ContentType.objects.get_by_natural_key(
            self._meta.app_label, self._meta.model_name
        )
        return reverse(
            "anexos:anexo_list", kwargs={"agroecossistema_id": self.agroecossistema.id}
        )

    def get_anexos_list(self):
        a_list = []
        anexo_rs = self.anexos.all()
        for anexo_r in anexo_rs:
            anexo = anexo_r.anexo
            a_list.append(anexo.as_dict())
        return a_list

    def clean_fields(self, exclude=None):
        super().clean_fields(exclude=exclude)
        if (
            self.data_fim is not None
            and self.data_inicio is not None
            and self.data_fim < self.data_inicio
        ):
            raise ValidationError(_("Ano final deve ser maior que ano inicial."))

    def save(self, *args, **kwargs):
        # update ultima_alteracao of agroecossistema:
        self.agroecossistema.save()
        super(Evento, self).save(*args, **kwargs)


class EventoGrupo(LumeModel):
    nome = models.CharField(max_length=255, verbose_name=_lazy("nome"))
    descricao = models.TextField(null=True, blank=True, verbose_name=_lazy("descrição"))
    agroecossistema = models.ForeignKey(
        Agroecossistema, models.CASCADE, verbose_name=_lazy("agroecossistema")
    )
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("última alteração")
    )

    # Relationship Fields
    eventos = models.ManyToManyField(
        "linhadotempo.Evento",
        verbose_name=_lazy("eventos"),
        related_name="grupos",
    )

    class Meta:
        ordering = ("-criacao",)

    def __unicode__(self):
        return "%s" % self.pk

    def get_absolute_url(self):
        return reverse(
            "linhadotempo:eventogrupo_detail",
            args=(
                self.agroecossistema_id,
                self.pk,
            ),
        )

    def get_update_url(self):
        return reverse(
            "linhadotempo:eventogrupo_update",
            args=(
                self.agroecossistema_id,
                self.pk,
            ),
        )
