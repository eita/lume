$(document).ready(function () {
  var options = dataTableDefaultOptions;
  options.orderCellsTop = true;
  options.fixedHeader = true;
  options.scrollY = "40vh";
  options.scrollCollapse = true;
  options.paging = false;
  options.dom = "Bfrtip";
  options.buttons = ["excelHtml5", "csvHtml5", "pdfHtml5"];
  var eventogatilhoListTable = $("#eventogatilho_list_table");
  setTimeout(function () {
    eventogatilhoListTable.DataTable(options);
  }, 5);
  var filter_array = ["Evento-gatilho", "Organização"];
  $("#eventogatilho_list_table thead tr")
    .clone(true)
    .appendTo("#eventogatilho_list_table thead");
  $("#eventogatilho_list_table thead tr:eq(1) th").each(function (i) {
    var title = $(this).text();
    if (filter_array.indexOf(title) > -1) {
      $(this).html(
        '<input style="width: 100%" type="text" placeholder="Filtrar" />'
      );
      $("input", this).on("keyup change", function () {
        if (
          eventogatilhoListTable.DataTable().column(i).search() !== this.value
        ) {
          eventogatilhoListTable
            .DataTable()
            .column(i)
            .search(this.value)
            .draw();
        }
      });
    } else {
      $(this).html("");
    }
  });
});
