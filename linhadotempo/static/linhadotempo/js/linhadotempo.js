let linhadotempo;
let gruposDataset;
let eventosDataset;
let timelineRange;
let primeiroAno;
let ultimoAno;
let skippedYears = [];
let skippedYearsInt = [];
let editable = false;
let paneClosed = true;

const umAno = 365.25 * 24 * 3600 * 1000;

var openPane = function () {
  if (paneClosed) {
    togglePane();
    paneClosed = !paneClosed;
  }
};

var closePane = function () {
  if (!paneClosed) {
    togglePane();
    paneClosed = !paneClosed;
  }
};

var togglePane = function () {
  $(".linhadotempo_pane1").toggleClass("col-md-12 col-md-9");
  $(".linhadotempo_pane2").toggleClass("d-none col-md-3");
};

function updateMenuButtonsState() {
  if (eventosDataset.length == 0) {
    return;
  }
  var w = linhadotempo.getWindow();
  var isAtEnd =
    Math.abs(w.end.getTime() - timelineRange.max.valueOf()) < umAno / 2;
  var isAtStart =
    Math.abs(w.start.getTime() - timelineRange.min.valueOf()) < umAno / 2;
  $("#zoomOut, #seeAll").toggleClass("disabled", isAtEnd && isAtStart);
  $("#moveLeft").toggleClass("disabled", isAtStart);
  $("#moveRight").toggleClass("disabled", isAtEnd);
}

function updateTimelineRange() {
  $(".linhadotempoHeaders").toggle(eventosDataset.length > 1);
  timelineRange = linhadotempo.getItemRange();
  const min =
    timelineRange.min != null
      ? timelineRange.min
      : moment("1900-01-01").valueOf();
  const max =
    timelineRange.max != null
      ? timelineRange.max
      : moment("2100-01-01").valueOf();
  linhadotempo.setOptions({
    min: min,
    max: max,
  });
}

function initLinhaDoTempo() {
  moment.locale("pt-br");
  gruposDataset = new vis.DataSet(grupos);
  eventosDataset = new vis.DataSet(eventos);
  const container = jQuery(".linhadotempo_holder")[0];

  updateSkippedYears();
  // console.log(skippedYears, skippedYearsInt, primeiroAno, ultimoAno);

  $(".toast").toast({ delay: 2000 });
  $(".toast").on("show.bs.toast", function () {
    $(".toast").removeClass("d-none");
  });
  $(".toast").on("hidden.bs.toast", function () {
    $(".toast").addClass("d-none");
  });

  const options = {
    width: "100%",
    height: "calc(100vh - 160px)",
    orientation: {
      axis: "top",
      item: "top",
    },
    zoomMin: isTerritorial ? 10 : 94608000000,
    hiddenDates: isTerritorial ? [] : skippedYears,
    margin: { item: 0 },
    align: "center",
    verticalScroll: true,
    horizontalScroll: true,
    zoomable: true,
    zoomKey: "shiftKey",
    zoomFriction: 30,
    showTooltips: true,
    timeAxis: { scale: "year" },
    groupHeightMode: "fixed",
    format: {
      minorLabels: function (date, scale, step) {
        const currentYear = date.year();
        if (isTerritorial) {
          const currentColuna = currentYear - 1900;
          for (let periodo of periodos) {
            if (periodo.coluna === currentColuna) {
              return periodo.nome;
            }
          }
          return "";
        }

        if (primeiroAno == null || currentYear < primeiroAno) {
          return "<br/>" + currentYear;
        } else {
          let ano = currentYear - primeiroAno + 1;
          return `<span class="linhadotempo-label-top">ano ${ano}</span><br/>${currentYear}`;
        }
      },
    },
    editable: {
      add: true,
      updateTime: true,
      updateGroup: true,
      remove: false,
      overrideItems: false,
    },
    snap: null,
    onUpdate: function (item, callback) {
      if (editable) {
        openEditaEventoForm(item, isTerritorial);
      }
    },
    onMove: function (item, callback) {
      if (editable) {
        if (isTerritorial) {
          const mesInicio = moment(item.start).month();
          const anoInicio =
            mesInicio >= 6
              ? moment(item.start).year() + 1
              : moment(item.start).year();
          item.start = `${anoInicio}-01-03T00:00:00`;
          item.end = `${anoInicio}-12-30T00:00:00`;
          for (let periodo of periodos) {
            if (periodo.coluna === anoInicio - 1900) {
              item.periodo = periodo.id;
              item.periodo_coluna = periodo.coluna;
            }
          }
        }
        atualizaEvento({
          evento: itemToEvento(item, isTerritorial),
          eventoAntigo: itemToEvento(
            eventosDataset.get(item.id),
            isTerritorial
          ),
        });
      }
    },
    onAdd: function (item, callback) {
      if (editable) {
        openCriaEventoForm(item);
      }
      callback(null);
    },
  };

  linhadotempo = new vis.Timeline(
    container,
    eventosDataset,
    gruposDataset,
    options
  );

  const anoAtual = new Date().getFullYear();

  if (isTerritorial) {
    timelineRange = {
      min: dataInicio,
      max: dataFim,
    };
    $(".linhadotempoHeaders").show();
    linhadotempo.setOptions(timelineRange);
    $.ajax(`${ajax_tipos_dimensao_url}`, {
      method: "GET",
      dataType: "json",
      success: function (data) {
        data.results = data.results.map((row) => {
          return {
            id: row.tipo_dimensao_id,
            text: row.text,
          };
        });
        $("#id_tipo_dimensao").select2({
          placeholder: "Escolha...",
          data: data.results,
          width: "100%",
          dropdownParent: $("#modal-right-wrapper"),
        });
      },
      error: function (e) {
        console.log(
          "Não foi possível inicializar o select2 de tipo de dimensão! Erro: " +
            e.msg
        );
      },
    });
    $("#id_subdimensao").select2({
      placeholder: "(sem subdimensão)",
      allowClear: true,
      ajax: {
        type: "GET",
        url: function () {
          return `${ajax_dimensoes_url}${ajax_tipo_dimensao_id}/`;
        },
      },
      width: "100%",
      dropdownParent: $("#modal-right-wrapper"),
    });
    $("#id_tipo_dimensao").on("change", function (e) {
      $("#id_subdimensao").val(null).trigger("change");
      ajax_tipo_dimensao_id = $("#id_tipo_dimensao").val()
        ? $("#id_tipo_dimensao").val()
        : "";
    });
    $("#id_subdimensao").on("change", function (e) {
      $("#id_subdimensao_nome").val($(e.target).find(":selected").text());
    });
    $("#id_subdimensao").on("select2:unselecting", function (e) {
      $("#id_subdimensao").on("select2:opening", function (ev) {
        ev.preventDefault();
        $("#id_subdimensao").off("select2:opening");
      });
    });
  } else {
    updateTimelineRange();
    let anoInicio = anoAtual;
    let yearsRange = 0;
    while (yearsRange <= initialWindowSize) {
      if (!skippedYearsInt.includes(anoInicio)) {
        yearsRange++;
      }
      anoInicio--;
    }
    dataInicio = moment(new Date(`01/01/${anoInicio}`)).valueOf();
    dataFim = moment(new Date()).valueOf();
  }
  setTimeout(function () {
    linhadotempo.setWindow({
      start: dataInicio,
      end: dataFim,
      animation: false,
    });
    $("#modoEdicao")
      .prop("checked", eventosDataset.length == 0)
      .change();
  }, 100);

  var openEvent = function (eventId) {
    let evento = eventosDataset.get(parseInt(eventId));

    // Titulo
    jQuery(".modal-right-title").html(
      evento.titulo + " (" + evento.ano_formatted + ")"
    );

    // Ano ou período
    jQuery("#evento-ano").text(evento.ano_formatted);

    // Evento gatilho
    jQuery("#eventoModalBadgeGatilho").toggleClass(
      "d-none",
      evento.evento_gatilho == null
    );

    // Dimensão ou Tipo de Dimensão (if isTerritorial)
    var tipoLabel = gruposDataset
      .get(parseInt(evento.group))
      .content.replace(" <span></span>", "");
    jQuery("#evento-tipo").text(tipoLabel);

    // Dimensão (if isTerritorial)
    if (isTerritorial) {
      jQuery(".eventoDetailDivSubDimensao").toggleClass(
        "d-none",
        !evento.dimensao_nome
      );
      if (evento.dimensao_nome) {
        jQuery("#evento-subdimensao").text(evento.dimensao_nome);
      }
    }

    // Autor
    jQuery("#evento-autor").text(evento.autor || "");

    // Descrição
    jQuery(".eventoDetailDivDescricao").toggleClass("d-none", !evento.detalhes);
    jQuery("#evento-descricao").html(evento.detalhes);

    jQuery("#edit-button").attr(
      "href",
      window.location.href.replace(/[#?]?.*$/, "eventos/update/" + evento.id)
    );
    jQuery("#anexos-url").attr("href", evento.anexos_url);

    //Anexos:
    setupAnexos(evento, false);

    $(".eventoFormDiv").hide();
    $(".eventoDetailDiv").show();
    $(".modal-right-footer").hide();
    $("#modal-right-wrapper").fadeIn();
  };

  linhadotempo
    .on("select", function (properties) {
      const selectedItemId = parseInt(properties.items[0]);
      if (isNaN(selectedItemId)) {
        $("#modal-right-wrapper").fadeOut();
        linhadotempo.setSelection([]);
        return true;
      }

      if (!editable) {
        openEvent(selectedItemId);
        return true;
      }

      if ($("#modal-right-wrapper").is(":visible")) {
        const item = eventosDataset.get(parseInt(selectedItemId));
        openEditaEventoForm(item, isTerritorial);
      }
    })
    .on("rangechanged", function () {
      updateMenuButtonsState();
      adjustSliderHolderToWindowView();
    })
    .on("changed", function () {
      $(".linhadotempo_menu").css(
        "left",
        $(".vis-labelset").width() - $(".linhadotempo_menu").width() - 10
      );
    })
    .on("contextmenu", function (props) {
      props.event.preventDefault();
      isEditableItem = true;
      if (props.item) {
        const item = eventosDataset.get(props.item);
        isEditableItem = !(
          Object.prototype.hasOwnProperty.call(item, "editable") &&
          !item.editable
        );
      }
      $("#menuVer").toggleClass("d-none", !props.item);
      $("#menuEditar, #menuApagar").toggleClass(
        "d-none",
        !editable || !props.item || !isEditableItem
      );
      $("#menuCriar").toggleClass("d-none", !editable || props.item);
      $("#contextMenu .dropdown-divider").toggleClass(
        "d-none",
        !editable && !props.item
      );
      $("#menuAtivaEdicao").toggleClass("d-none", editable);
      $("#menuDesativaEdicao").toggleClass("d-none", !editable);
      var top = props.pageY + 10;
      var left = props.pageX;
      $("#contextMenu")
        .css({
          display: "block",
          top: top,
          left: left,
        })
        .addClass("show")
        .data({
          id: props.item,
          time: props.time,
          group: props.group,
        });
      return false;
    })
    .on("click", function (props) {
      $("#contextMenu").removeClass("show").hide();
    });

  $("#contextMenu a.dropdown-item").on("click", function (e) {
    $("#contextMenu").removeClass("show").hide();
  });
  $("#menuVer").on("click", function (e) {
    e.preventDefault();
    openEvent($("#contextMenu").data("id"));
  });
  $("#menuEditar").on("click", function (e) {
    e.preventDefault();
    const item = eventosDataset.get(parseInt($("#contextMenu").data("id")));
    openEditaEventoForm(item, isTerritorial);
  });
  $("#menuApagar").on("click", function (e) {
    e.preventDefault();
    if (
      window.confirm(
        "Tem certeza que quer apagar este evento? Não é possível desfazer esta operação."
      )
    ) {
      const eventoId = $("#contextMenu").data("id");
      const evento = itemToEvento(eventosDataset.get(eventoId), isTerritorial);
      atualizaEvento({
        evento,
        apagar: true,
      });
    }
  });
  $("#menuCriar").on("click", function (e) {
    e.preventDefault();
    let item = {
      start: $("#contextMenu").data("time"),
      group: $("#contextMenu").data("group"),
    };
    if (isTerritorial) {
      item.className = "territorial";
    }
    openCriaEventoForm(item);
  });
  $("#menuAtivaEdicao").on("click", function (e) {
    e.preventDefault();
    $("#modoEdicao").prop("checked", true).change();
  });
  $("#menuDesativaEdicao").on("click", function (e) {
    e.preventDefault();
    $("#modoEdicao").prop("checked", false).change();
  });

  $("#modoEdicao").on("change", function (e) {
    editable = $("#modoEdicao").prop("checked");
    $("#modal-right-wrapper").fadeOut();
    updateEditable();
  });

  /* open event if GET parameter */
  var url = new URL(window.location.href);
  var eventoIdParam = url.searchParams.get("evento_id");
  if (eventoIdParam) {
    linhadotempo.setSelection(parseInt(eventoIdParam));
    openEvent(eventoIdParam);
  }

  function getTooltipContent(e) {
    if ($(e).parents(".vis-label").length === 0) {
      return "";
    }
    const classes = $(e).parents(".vis-label").attr("class").split(" ");
    const groupId = classes
      .filter((item) => {
        return item.substr(0, 6) === "group-";
      })[0]
      .replace("group-", "");
    return linhadotempo.groupsData.get(parseInt(groupId)).help;
  }

  // $('[data-toggle=tooltip]').tooltip();
  $(".vis-inner span").tooltip({
    title: function () {
      return getTooltipContent(this);
    },
    trigger: "hover",
    html: true,
    placement: "top",
    boundary: "window",
  });

  $("#zoomIn").on("click", function () {
    linhadotempo.zoomIn(0.2);
  });
  $("#zoomOut").on("click", function () {
    linhadotempo.zoomOut(0.2);
  });
  $("#moveLeft").on("click", function () {
    move(1.2, 200);
  });
  $("#moveRight").on("click", function () {
    move(-1.2, 200);
  });
  $("#seeAll, #menuFit").on("click", function () {
    linhadotempo.fit();
  });
  $(".vis-panel.vis-center").on("wheel", function (e) {
    move(e.originalEvent.deltaY < 0 ? 0.2 : -0.2);
    e.preventDefault();
  });

  $("#download_linhadotempo").on("click", function (e) {
    e.preventDefault();
    $("#loading").show();
    $("#linhadotempo").width(3096);
    linhadotempo.fit();
    setTimeout(function () {
      var useWidth = $(".linhadotempo_pane1").prop("scrollWidth");
      var useHeight = $(".linhadotempo_pane1").prop("scrollHeight");
      html2canvas(document.getElementById("linhadotempo"), {
        width: useWidth,
        height: useHeight + 50,
        scale: 2,
        y: 100,
      }).then(function (canvas) {
        var image = canvas
          .toDataURL("image/png")
          .replace("image/png", "image/octet-stream");
        baixaImagem(
          image,
          getSlug($("#download_linhadotempo").attr("download"))
        );
        setTimeout(function () {
          $("#loading").hide(500);
          $("#linhadotempo").width("100%");
        }, 200);
      });
    }, 1000);
  });

  function initBuscaEventos() {
    const primeiraVez = !$("#buscaEventos").hasClass(
      "select2-hidden-accessible"
    );

    let select2Data = [{ id: "", text: "Pesquisar" }];
    eventosDataset.forEach(function (evento, id) {
      select2Data.push({
        id: evento.id,
        text: `${evento.title} (${evento.ano_formatted})`,
      });
    });

    $("#buscaEventos").empty().select2({
      language: "pt-BR",
      data: select2Data,
      placeholder: "Pesquisar",
    });

    if (primeiraVez) {
      $("#buscaEventos").on("select2:select", function (e) {
        eventosDataset.forEach(function (evento, id) {
          if (evento.id == e.params.data.id) {
            linhadotempo.focus(parseInt(evento.id));
            linhadotempo.setSelection(parseInt(evento.id));
            $("#buscaEventos").val(null).trigger("change");
            return;
          }
        });
      });
    }
  }
  initBuscaEventos();

  function atualizaEvento(params) {
    const onSuccess = function (data, evento, apagar, toastBody) {
      if (!apagar) {
        evento.id = data.pk;
        if (isTerritorial) {
          evento.dimensao = evento.subdimensao;
          evento.dimensao_nome = evento.subdimensao_nome;
        }
        const eventoVelho = eventosDataset.get(evento.id);
        const item = eventoToItem(evento, eventoVelho, isTerritorial);
        eventosDataset.update(item);
      } else {
        eventosDataset.remove(evento.id);
      }
      if (!isTerritorial) {
        updateTimelineRange();
        updateSkippedYears();
        linhadotempo.setOptions({ hiddenDates: skippedYears });
      } else {
        linhadotempo.fit();
      }
      initBuscaEventos();

      const msg = apagar
        ? "Evento apagado com sucesso!"
        : `O evento <i>${evento.titulo}</i> foi ${toastBody} com sucesso!`;

      $("#toastTitle").html(toastTitle);
      $(".toast-body").html(msg);
      $(".toast").toast("show");
    };

    const onError = function (eventoAntigo, evento) {
      if (eventoAntigo) {
        eventosDataset.update(
          eventoToItem(eventoAntigo, eventoAntigo, isTerritorial)
        );

        if (!isTerritorial) {
          updateTimelineRange();
          updateSkippedYears();
          linhadotempo.setOptions({ hiddenDates: skippedYears });
        } else {
          linhadotempo.fit();
        }
        initBuscaEventos();
      }

      $("#toastTitle").html("Erro!");
      $(".toast-body").html(
        `<div class="alert alert-danger">Falha ao atualizar ou criar o evento <i>${evento.titulo}</i>. Operação falhou!</div>`
      );
      $(".toast").toast("show");
    };

    doAtualizaEvento(
      params,
      agroecossistemaId,
      comunidadeOuTerritorioId,
      onSuccess,
      onError,
      restDeleteUrl,
      restUpdateUrl,
      isTerritorial
    );
  }

  function updateEditable() {
    linhadotempo.setSelection([]);
    linhadotempo.setOptions({
      showTooltips: !editable,
      editable: {
        add: editable, // add new items by double tapping
        updateTime: editable, // drag items horizontally
        updateGroup: editable, // drag items from one group to another
        remove: false, // delete an item by tapping the delete button top right
        overrideItems: false, // allow these options to override item.editable
      },
    });
    $("#modoEdicaoWrapper label").toggleClass("checked", editable);
    $("#toastTitle").html(editable ? "Modo de edição" : "Modo de visualização");
    $(".toast-body").html(
      editable
        ? "Agora você pode criar, mover e editar eventos!"
        : "Apenas consulta."
    );
    $(".toast").toast("show");
  }

  ///////////////////////
  // SLIDER
  ///////////////////////
  const resizersWidth = $("#linhadotempoSliderHolderLeft").width();

  function adjustSliderHolderToWindowView() {
    if (eventosDataset.length == 0) {
      return;
    }
    const razaoSlider =
      $("#linhadotempoSlider").width() /
      (timelineRange.max.valueOf() - timelineRange.min.valueOf());
    let windowStart = linhadotempo.getWindow().start.valueOf();
    let windowEnd = linhadotempo.getWindow().end.valueOf();

    let width = (windowEnd - windowStart) * razaoSlider;
    let left = (windowStart - timelineRange.min.valueOf()) * razaoSlider;

    if (timelineRange.max.valueOf() - windowEnd < umAno) {
      windowEnd = timelineRange.max.valueOf();
      width = $("#linhadotempoSlider").width() - left;
    }

    if (windowStart - timelineRange.min.valueOf() < umAno) {
      windowStart = timelineRange.min.valueOf();
      left = 0;
    }

    TweenLite.set(".dragMove", { x: left, width: width });

    $("#linhadotempoSliderHolderLeft").css({ left: left });
    $("#linhadotempoSliderHolderRight").css({
      left: left + width - resizersWidth,
    });
  }

  let mover = Draggable.create(".dragMove", {
    type: "x",
    bounds: "#linhadotempoSlider",
    onDrag: dragMove,
  });

  function dragMove() {
    const razaoSlider =
      $("#linhadotempoSlider").width() /
      (timelineRange.max.valueOf() - timelineRange.min.valueOf());
    var leftPos = $("#linhadotempoSliderHolder").position().left;
    var rightPos =
      leftPos + $("#linhadotempoSliderHolder").width() - resizersWidth;
    $("#linhadotempoSliderHolderLeft").css({ left: leftPos });
    $("#linhadotempoSliderHolderRight").css({ left: rightPos });

    const newRangeStart =
      timelineRange.min.valueOf() + Math.round(leftPos / razaoSlider);
    const newRangeEnd =
      timelineRange.min.valueOf() + Math.round(rightPos / razaoSlider);
    linhadotempo.setWindow({
      start: newRangeStart,
      end: newRangeEnd,
      animation: false,
    });
  }
  function dragResize() {
    const leftPos =
      $("#linhadotempoSliderHolderLeft").position().left + resizersWidth;
    const rightPos = $("#linhadotempoSliderHolderRight").position().left;
    const width = rightPos - leftPos;
    // console.log({leftPos, rightPos, width});
    $("#linhadotempoSliderHolder").css({ width: width, left: leftPos });
  }
  /**
   * Move the timeline a given percentage to left or right
   * @param {Number} percentage   For example 0.1 (left) or -0.1 (right)
   */
  function move(percentage, animation = false) {
    const range = linhadotempo.getWindow();
    let newRangeStart = new Date(range.start.valueOf() - percentage * umAno);
    let newRangeEnd = new Date(range.end.valueOf() - percentage * umAno);
    let changed;

    let newEndYear = newRangeEnd.getFullYear();
    changed = false;
    // console.log('newEndYear: ' + newEndYear);
    while (skippedYearsInt.includes(newEndYear)) {
      newEndYear += percentage > 0 ? -1 : 1;
      changed = true;
      // console.log(' - ' + newEndYear);
    }
    newRangeEnd.setFullYear(newEndYear);

    let newStartYear = newRangeStart.getFullYear();
    changed = false;
    // console.log('percentage: ' + percentage + '. newStartYear: ' + newStartYear);
    while (skippedYearsInt.includes(newStartYear)) {
      newStartYear += percentage > 0 ? -1 : 1;
      changed = true;
      // console.log(' - ' + newStartYear);
    }
    newRangeStart.setFullYear(newStartYear);

    // console.log([newRangeStart.getFullYear(), newRangeEnd.getFullYear()]);
    linhadotempo.setWindow({
      start: newRangeStart.valueOf(),
      end: newRangeEnd.valueOf(),
      animation: animation,
    });
  }

  initEventosFormHelpers(atualizaEvento, isTerritorial);

  function updateSkippedYears() {
    primeiroAno = 5000;
    ultimoAno = 1000;
    skippedYears = [];
    skippedYearsInt = [];
    let usedYears = [];
    eventosDataset.forEach(function (evento, id) {
      let startYear = moment(evento.start).year();
      if (moment(evento.start).month() > 2) {
        startYear++;
      }
      let endYear = moment(evento.end).year();
      if (moment(evento.end).month() < 10) {
        endYear--;
      }

      for (let year = startYear; year <= endYear; year++) {
        if (!usedYears.includes(year)) {
          usedYears.push(year);
        }
      }

      if (startYear < primeiroAno) {
        primeiroAno = startYear;
      }
      if (endYear > ultimoAno) {
        ultimoAno = endYear;
      }
    });
    let skipping = [];
    for (let year = primeiroAno; year <= ultimoAno; year++) {
      if (!usedYears.includes(year)) {
        skippedYearsInt.push(year);
        skipping.push(year);
      } else if (skipping.length > 0) {
        skippedYears.push({
          start: `${skipping[0]}-01-01`,
          end: `${year}-01-01`,
        });
        skipping = [];
      }
    }
    // console.log(skippedYears, usedYears);
  }
}
