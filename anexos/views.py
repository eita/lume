import os

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.contenttypes.models import ContentType
from django.db import transaction
from django.shortcuts import get_object_or_404, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import DetailView, ListView, UpdateView, CreateView
from django.views.generic.edit import DeleteView, FormView, FormMixin

from django.utils.translation import gettext as _
from rules.contrib.views import PermissionRequiredMixin

from anexos.mixins import AjaxableResponseMixin
from anexos.utils import organiza_anexos
from cadernodecampo.models import Anotacao
from linhadotempo.models import Evento
from agroecossistema.models import Agroecossistema
from agroecossistema.forms import AgroecossistemaForm
from main.views import LumeDeleteView
from .forms import PastaForm, PastaFormSet, AnexoForm, AnexoRestForm, AnexoRecipienteForm, AnexoRecipienteForm
from .models import PastaAnexo, Anexo, AnexoRecipiente


class PastaListView(ListView):
    model = PastaAnexo


class PastaCreateView(CreateView):
    model = PastaAnexo
    form_class = PastaForm


class PastaDetailView(DetailView):
    model = PastaAnexo


class PastaUpdateView(PermissionRequiredMixin, UpdateView):
    model = Agroecossistema
    form_class = AgroecossistemaForm
    permission_required = 'permissao_padrao'
    titulo = _('Editar Pasta')
    template_name = 'anexos/pasta_form.html'

    def get_permission_object(self):
        agroecossistema = get_object_or_404(Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = self.titulo
        context['modulo'] = 'anexos'
        context['submodulo'] = 'pastas'
        context['agroecossistema'] = Agroecossistema.objects.get(id=self.kwargs['agroecossistema_id'])
        if self.request.POST:
            context['pastas_formset'] = PastaFormSet(self.request.POST, instance=self.get_object())
        else:
            context['pastas_formset'] = PastaFormSet(instance=self.get_object())

        return context

    def get_form_kwargs(self):
        kwargs = super(PastaUpdateView, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        kwargs.update({'can_delete': self.request.user.has_perm('agroecossistema.apagar', self.get_object())})
        return kwargs

    def form_valid(self, form):
        context = self.get_context_data()
        pastas_formset = context['pastas_formset']

        try:
            with transaction.atomic():
                self.object = form.save()

                if pastas_formset.is_valid():
                    pastas_formset.instance = self.object
                    pastas_formset.save()
                else:
                    tmp = form.full_clean()
                    form._errors[NON_FIELD_ERRORS] = form.error_class([
                        _("Foram encontrados erros no formulário e os dados NÃO foram salvos. Favor fazer as correções indicadas.")
                    ])
                    return self.render_to_response(self.get_context_data(form=form))

        except ValueError:
            tmp = form.full_clean()
            form._errors[NON_FIELD_ERRORS] = form.error_class([
                _("Foram encontrados erros no formulário e os dados NÃO foram salvos. Favor fazer as correções indicadas.")
            ])
            return self.render_to_response(self.get_context_data(form=form))

        return super(PastaUpdateView, self).form_valid(form)

    def get_object(self, queryset=None):
        return Agroecossistema.objects.get(pk=self.kwargs['agroecossistema_id'])

    def get_success_url(self, object_id=None):
        return reverse('anexos:pasta_anexo_update', kwargs={'agroecossistema_id': self.kwargs['agroecossistema_id']})


class PastaDeleteView(PermissionRequiredMixin, LumeDeleteView):
    model = PastaAnexo
    permission_required = 'agroecossistema.alterar'

    def get_permission_object(self):
        agroecossistema = get_object_or_404(Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(PastaDeleteView, self).dispatch(*args, **kwargs)


class AnexoListView(FormMixin, ListView):
    model = Anexo
    template_name = 'anexos/anexo_list.html'
    form_class = AnexoForm

    def get_form_kwargs(self):
        kwargs = super(AnexoListView, self).get_form_kwargs()
        kwargs.update({
            'can_delete': self.request.user.has_perm('permissao_padrao')
        })
        if 'agroecossistema_id' in self.kwargs:
            kwargs['agroecossistema_id'] = self.kwargs['agroecossistema_id']
        if 'data' in kwargs and 'update_pk' in kwargs['data'] and kwargs['data']['update_pk'] != '':
            kwargs['instance'] = Anexo.objects.get(pk=int(kwargs['data']['update_pk']))

        return kwargs

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        anexo = form.save(commit=False)

        anexo.agroecossistema=Agroecossistema.objects.get(pk=self.kwargs['agroecossistema_id'])

        arquivo = form.cleaned_data['arquivo']
        if arquivo.__class__.__name__ == 'InMemoryUploadedFile':
            anexo.mimetype = arquivo.content_type
            if anexo.nome is None or anexo.nome == "":
                anexo.nome = os.path.splitext(arquivo.name)[0]

        anexo.save()

        if 'related_type' in self.kwargs:
            related_type = ContentType.objects.get(app_label=self.kwargs['related_type'], model=self.kwargs['related_model'].lower())
            anexo_recipiente, created = AnexoRecipiente.objects.get_or_create(
                recipiente_object_id=self.kwargs['pk'],
                anexo_id=anexo.id,
                recipiente_content_type_id=related_type.id
            )
            return HttpResponseRedirect(reverse(
                f"{self.kwargs['related_type']}:{self.kwargs['related_model'].lower()}_anexos",
                args=(self.kwargs['agroecossistema_id'],self.kwargs['pk'],)
            ))

        return HttpResponseRedirect(
            reverse('anexos:anexo_list', args=(self.kwargs['agroecossistema_id'],))
        )

    def get_queryset(self):
        relatedType = None
        if 'related_type' in self.kwargs:
            relatedType = self.kwargs['related_type']

        if not relatedType:
            contentType = ContentType.objects.get(app_label='agroecossistema', model='agroecossistema')
            if 'filter' in self.kwargs:
                filter = self.kwargs['filter']
                anexosAll = Anexo.objects.filter(agroecossistema_id=self.kwargs['agroecossistema_id']).order_by('-ultima_alteracao')
                anexos = []
                for anexo in anexosAll:
                    if filter == 'imagens':
                        if anexo.is_image():
                            anexos.append(anexo)
                        continue

                    if filter == 'videos':
                        if anexo.is_video():
                            anexos.append(anexo)
                        continue

                    if not anexo.is_video() and not anexo.is_image():
                        anexos.append(anexo)

            elif 'pastaId' in self.kwargs:
                pastaId = self.kwargs['pastaId']
                anexos = Anexo.objects.filter(agroecossistema_id=self.kwargs['agroecossistema_id'], pasta_id=pastaId).order_by('-ultima_alteracao')

            else:
                anexos = Anexo.objects.filter(agroecossistema_id=self.kwargs['agroecossistema_id']).order_by('-ultima_alteracao')

            return anexos

        # is related content!
        anexosRecipientesList = AnexoRecipiente.objects.filter(recipiente_object_id=self.kwargs['pk']).order_by('-ultima_alteracao').values_list('anexo_id')
        return Anexo.objects.filter(pk__in=anexosRecipientesList)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        relatedType = None
        if 'related_type' in self.kwargs:
            relatedType = self.kwargs['related_type']
        comPastas = True

        if 'agroecossistema_id' in self.kwargs:
            context['agroecossistema'] = Agroecossistema.objects.get(id=self.kwargs['agroecossistema_id'])
        context['modo'] = 'grade' if not ('modo' in self.request.GET) else self.request.GET['modo']
        context['modulo'] = relatedType if relatedType else 'anexos'
        context['isPasta'] = False
        context['titulo'] = _('Anexos')
        context['anexosPlural'] = _('arquivos')
        context['anexosSingular'] = _('arquivo')

        if not relatedType:
            context['submodulo'] = 'todos'
            context['anexosUrlList'] = reverse("anexos:anexo_list", args=(self.kwargs['agroecossistema_id'],))
            context['anexosUrlCreate'] = reverse("anexos:anexo_create", args=(self.kwargs['agroecossistema_id'],))
            if 'filter' in self.kwargs:
                filter = self.kwargs['filter']
                context['submodulo'] = filter
                if filter == 'imagens':
                    context['titulo'] = _("Imagens")
                    context['anexosPlural'] = _("imagens")
                    context['anexosSingular'] = _("imagem")
                elif filter == 'videos':
                    context['titulo'] = _("Vídeos")
                    context['anexosPlural'] = _("vídeos")
                    context['anexosSingular'] = _("vídeo")
                elif filter == 'documentos':
                    context['titulo'] = _("Documentos")
                    context['anexosPlural'] = _("documentos")
                    context['anexosSingular'] = _("documento")

            elif 'pastaId' in self.kwargs:
                pasta = PastaAnexo.objects.get(pk=self.kwargs['pastaId'])
                context['isPasta'] = True
                context['pasta'] = pasta
                context['submodulo'] = pasta.nome
                context['titulo'] = pasta.nome

        else:
            module = __import__(relatedType)
            class_ = getattr(module.models, self.kwargs['related_model'])
            related_object = class_.objects.get(pk=self.kwargs['pk'])
            content_type = ContentType.objects.get(app_label=relatedType, model=self.kwargs['related_model'].lower())

            context['titulo'] = f"{self.kwargs['related_model']}: {related_object.__str__()}"
            context['submodulo'] = 'anexosTodos'
            context['related_id'] = related_object.id
            context['related_type'] = content_type.id
            context['relatedUrl'] = reverse(
                f"{relatedType}:{self.kwargs['related_model'].lower()}_list",
                args=(self.kwargs['agroecossistema_id'],)
            ) if 'related_list_url' not in self.kwargs else reverse(
                self.kwargs['related_list_url'],
                args=(self.kwargs['pk'],)
            )
            # context['anexosUrlCreate'] = reverse(
            #     f"{relatedType}:{self.kwargs['related_model'].lower()}_anexos_create",
            #     args=(
            #         self.kwargs['agroecossistema_id'],
            #         self.kwargs['pk'],
            #     )
            # )

        agroecossistema_id = self.kwargs['agroecossistema_id'] if 'agroecossistema_id' in self.kwargs else None
        context['anexos'] = organiza_anexos(self.object_list, agroecossistema_id, context['isPasta'])

        return context

class AnexoCreateView(PermissionRequiredMixin, CreateView):
    model = Anexo
    form_class = AnexoForm
    titulo = _('Novo Anexo')
    permission_required = 'agroecossistema.alterar'

    def get_permission_object(self):
        agroecossistema = get_object_or_404(Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    def get_form_kwargs(self):
        kwargs = super(AnexoCreateView, self).get_form_kwargs()
        kwargs.update({
            'can_delete': self.request.user.has_perm('permissao_padrao')
        })
        kwargs['agroecossistema_id'] = self.kwargs['agroecossistema_id']
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['agroecossistema'] = Agroecossistema.objects.get(pk=self.kwargs['agroecossistema_id'])
        context['modulo'] = 'anexos'
        context['submodulo'] = 'create'
        context['titulo'] = self.titulo
        return context

    def form_valid(self, form):
        anexo = form.save(commit=False)

        arquivo = form.cleaned_data['arquivo']
        anexo.agroecossistema=Agroecossistema.objects.get(pk=self.kwargs['agroecossistema_id'])
        anexo.mimetype = arquivo.content_type
        if anexo.nome is None or anexo.nome == "":
            anexo.nome = os.path.splitext(arquivo.name)[0]
        anexo.save()

        return HttpResponseRedirect(
            reverse('anexos:anexo_list', args=(self.kwargs['agroecossistema_id'],))
        )


class AnexoRelatedTypeCreateView(LoginRequiredMixin, FormView):
    form_class = AnexoForm
    related_object = None
    template_name = 'anexos/anexo_form.html'

    def dispatch(self, request, *args, **kwargs):
        related_type = ContentType.objects.get(
            app_label=self.kwargs['related_type'],
            model=self.kwargs['related_model'].lower()
        )
        self.related_object = related_type.get_object_for_this_type(pk=self.kwargs['pk'])
        return super(AnexoRelatedTypeCreateView, self).dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(AnexoRelatedTypeCreateView, self).get_form_kwargs()
        kwargs['agroecossistema_id'] = self.kwargs['agroecossistema_id']
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        related_type = ContentType.objects.get(app_label='cadernodecampo', model='anotacao')
        context['related_object'] = self.related_object
        context['related_type'] = related_type.id
        context['related_id'] = self.kwargs['pk']
        if self.kwargs['related_type'] == 'cadernodecampo':
            context['titulo'] = _('Novo Anexo na visita de') + ' ' + self.related_object.data_visita.strftime('%d/%m/%Y')
        elif self.kwargs['related_type'] == 'linhadotempo':
            context['titulo'] = _('Novo Anexo no evento') + ' ' + self.related_object.titulo

        context['agroecossistema'] = self.related_object.agroecossistema
        context['modulo'] = self.kwargs['related_type']
        return context

    def form_valid(self, form):
        try:
            with transaction.atomic():
                anexo = form.save()

                arquivo = form.cleaned_data['arquivo']
                anexo.agroecossistema=Agroecossistema.objects.get(pk=self.kwargs['agroecossistema_id'])
                anexo.mimetype=arquivo.content_type
                if anexo.nome is None or anexo.nome == "":
                    anexo.nome = os.path.splitext(arquivo.name)[0]
                anexo.save()

                anexo_recipiente = AnexoRecipiente()
                anexo_recipiente.anexo = anexo
                anexo_recipiente.recipiente = self.related_object
                anexo_recipiente.save()

                return HttpResponseRedirect(
                    reverse(
                        'cadernodecampo:anotacao_detail',
                        args=(self.kwargs['agroecossistema_id'], self.kwargs['pk'],)
                    )
                )
        except ValueError:
            tmp = form.full_clean()
            form._errors[NON_FIELD_ERRORS] = form.error_class([
                _("Foram encontrados erros no formulário e os dados NÃO foram salvos. Favor fazer as correções indicadas.")
            ])
            return self.render_to_response(self.get_context_data(form=form))


class AnexoUpdateView(LoginRequiredMixin, UpdateView):
    model = Anexo
    form_class = AnexoForm
    titulo = _('Editar anexo')

    def get_form_kwargs(self):
        kwargs = super(AnexoUpdateView, self).get_form_kwargs()
        kwargs.update({'can_delete': self.request.user.has_perm('permissao_padrao')})
        kwargs['agroecossistema_id'] = self.kwargs['agroecossistema_id']
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['agroecossistema'] = Agroecossistema.objects.get(pk=self.kwargs['agroecossistema_id'])
        context['modulo'] = 'anexos'
        context['submodulo'] = 'update'
        context['titulo'] = self.titulo
        return context

    def form_valid(self, form):
        anexo = form.save(commit=False)

        arquivo = form.cleaned_data['arquivo']
        anexo.agroecossistema=Agroecossistema.objects.get(pk=self.kwargs['agroecossistema_id'])
        if arquivo.__class__.__name__ == 'InMemoryUploadedFile':
            anexo.mimetype = arquivo.content_type
            if anexo.nome is None or anexo.nome == "":
                anexo.nome = os.path.splitext(arquivo.name)[0]

        anexo.save()

        return HttpResponseRedirect(
            reverse('anexos:anexo_list', args=(self.kwargs['agroecossistema_id'],))
        )

    def get_success_url(self):
        return reverse('anexos:anexo_list', args=(self.kwargs['agroecossistema_id'],))

class AnexoDetailView(DetailView):
    model = Anexo

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['agroecossistema'] = Agroecossistema.objects.get(pk=self.kwargs.get('agroecossistema_id'))
        context['modulo'] = 'anexos'
        return context

class AnexoRestUpdateView(PermissionRequiredMixin, AjaxableResponseMixin, UpdateView):
    model = Anexo
    form_class = AnexoRestForm
    permission_required = 'agroecossistema.cadastrar'
    success_message = _("Atualizado!")

    def get_permission_object(self):
        agroecossistema = get_object_or_404(Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(AnexoRestUpdateView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        data = self.request.POST

        pasta = None if data.get('pasta_id') == '-1' else PastaAnexo.objects.get(pk=int(data.get('pasta_id')))
        if data.get('id'):
            obj = Anexo.objects.get(pk=int(data.get('id')))
            obj.pasta = pasta
            return obj


class AnexoDeleteAjaxView(PermissionRequiredMixin, LumeDeleteView):
    model = Anexo
    permission_required = 'permissao_padrao'

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(AnexoDeleteAjaxView, self).dispatch(*args, **kwargs)


class AnexoDeleteView(PermissionRequiredMixin, DeleteView):
    model = Anexo
    permission_required = 'permissao_padrao'

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(AnexoDeleteView, self).dispatch(*args, **kwargs)


class AnexoRecipienteListView(ListView):
    model = AnexoRecipiente
    related_object = None

    def get_queryset(self):
        return AnexoRecipiente.objects.filter(recipiente_object_id=self.kwargs['related_id'],
                                              recipiente_content_type=self.kwargs['related_type']).order_by('-pk')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        related_type = ContentType.objects.get_for_id(self.kwargs['related_type'])
        context['related_object'] = self.related_object = related_type.get_object_for_this_type(
            pk=self.kwargs['related_id'])
        context['related_type'] = self.kwargs['related_type']
        context['related_id'] = self.kwargs['related_id']
        if isinstance(self.related_object, Agroecossistema):
            context['agroecossistema'] = self.related_object
        elif isinstance(self.related_object, Evento) or isinstance(self.related_object, Anotacao):
            context['agroecossistema'] = self.related_object.agroecossistema
        context['modulo'] = 'anexos'
        return context

class AnexoRecipienteCreateView(LoginRequiredMixin, FormView):
    form_class = AnexoRecipienteForm
    related_object = None
    template_name = 'anexos/anexo_e_recipiente_form.html'
    titulo = _('Novo Anexo')

    def dispatch(self, request, *args, **kwargs):
        related_type = ContentType.objects.get_for_id(self.kwargs['related_type'])
        self.related_object = related_type.get_object_for_this_type(pk=self.kwargs['related_id'])
        return super(AnexoRecipienteCreateView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['related_object'] = self.related_object
        context['related_type'] = self.kwargs['related_type']
        context['related_id'] = self.kwargs['related_id']
        context['titulo'] = self.titulo
        if isinstance(self.related_object, Agroecossistema):
            context['agroecossistema'] = self.related_object
        elif isinstance(self.related_object, Evento) or isinstance(self.related_object, Anotacao):
            context['agroecossistema'] = self.related_object.agroecossistema
        context['modulo'] = 'anexos'
        return context

    def form_valid(self, form):
        with transaction.atomic():
            for arquivo in form.cleaned_data['arquivos']:
                anexo = Anexo.objects.create(arquivo=arquivo, descricao=form.cleaned_data['descricao'], mimetype=arquivo.content_type, nome=arquivo.name)

                anexo_recipiente = AnexoRecipiente()
                anexo_recipiente.anexo = anexo
                anexo_recipiente.recipiente = self.related_object
                anexo_recipiente.save()

            return HttpResponseRedirect(
                reverse('anexos:anexorecipiente_list', args=(self.kwargs['related_type'], self.kwargs['related_id'])))

class AnexoRecipienteCreateView(CreateView):
    model = AnexoRecipiente
    form_class = AnexoRecipienteForm
    related_object = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        related_type = ContentType.objects.get_for_id(self.kwargs['related_type'])
        context['related_object'] = related_type.get_object_for_this_type(pk=self.kwargs['related_id'])
        context['related_type'] = self.kwargs['related_type']
        context['related_id'] = self.kwargs['related_id']
        context['modulo'] = 'anexos'
        return context


class AnexoRecipienteDetailView(DetailView):
    model = AnexoRecipiente
    related_object = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        related_type = ContentType.objects.get_for_id(self.kwargs['related_type'])
        context['related_object'] = related_type.get_object_for_this_type(pk=self.kwargs['related_id'])
        context['related_type'] = self.kwargs['related_type']
        context['related_id'] = self.kwargs['related_id']
        context['modulo'] = 'anexos'
        return context


class AnexoRecipienteUpdateView(UpdateView):
    model = AnexoRecipiente
    form_class = AnexoRecipienteForm
    related_object = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        related_type = ContentType.objects.get_for_id(self.kwargs['related_type'])
        context['related_object'] = related_type.get_object_for_this_type(pk=self.kwargs['related_id'])
        context['related_type'] = self.kwargs['related_type']
        context['related_id'] = self.kwargs['related_id']
        context['modulo'] = 'anexos'
        return context


class AnexoRecipienteDeleteView(DeleteView):
    model = AnexoRecipiente

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(AnexoRecipienteDeleteView, self).dispatch(*args, **kwargs)
