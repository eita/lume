# Generated by Django 2.2 on 2022-02-15 00:44

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('anexos', '0018_auto_20220210_1513'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='pastaanexo',
            name='deleted',
        ),
    ]
