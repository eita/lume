from django.urls import path
from . import views

app_name = 'anexos'

urlpatterns = []

urlpatterns += (
    # path('pasta/', views.PastaListView.as_view(), name='pasta_anexo_list'),
    # path('pasta/create/', views.PastaCreateView.as_view(), name='pasta_anexo_create'),
    path('pasta/delete/<int:pk>/', views.PastaDeleteView.as_view(), name='pasta_anexo_delete'),
    # path('pasta/detail/<int:pk>/', views.PastaDetailView.as_view(), name='pasta_anexo_detail'),
    path('pasta/', views.PastaUpdateView.as_view(), name='pasta_anexo_update'),
)

urlpatterns += (
    path('', views.AnexoListView.as_view(), name='anexo_list'),
    path('filter/<str:filter>/', views.AnexoListView.as_view(), name='anexo_filter'),
    path('pasta/<int:pastaId>/', views.AnexoListView.as_view(), name='anexo_pasta'),
    path('create/', views.AnexoCreateView.as_view(), name='anexo_create'),
    path('detail/<int:pk>/', views.AnexoDetailView.as_view(), name='anexo_detail'),
    path('update/<int:pk>/', views.AnexoUpdateView.as_view(), name='anexo_update'),
    path('rest/update/', views.AnexoRestUpdateView.as_view(), name="anexo_rest_update"),
    path('delete/ajax/<int:pk>/', views.AnexoDeleteAjaxView.as_view(), name='anexo_delete_ajax'),
    path('delete/<int:pk>/', views.AnexoDeleteView.as_view(), name='anexo_delete'),
)

urlpatterns += (
    # urls for AnexoRecipiente
    path('<int:related_type>/<int:related_id>', views.AnexoRecipienteListView.as_view(), name='anexorecipiente_list'),
    path('<int:related_type>/<int:related_id>/create/', views.AnexoRecipienteCreateView.as_view(), name='anexorecipiente_create'),
    path('<int:related_type>/<int:related_id>/detail/<int:pk>/', views.AnexoRecipienteDetailView.as_view(),
         name='anexorecipiente_detail'),
    path('<int:related_type>/<int:related_id>/update/<int:pk>/', views.AnexoRecipienteUpdateView.as_view(),
         name='anexorecipiente_update'),
    path('<int:related_type>/<int:related_id>/delete/<int:pk>/', views.AnexoRecipienteDeleteView.as_view(),
         name='anexorecipiente_delete'),
)
