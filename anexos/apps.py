from django.apps import AppConfig


class AnexosConfig(AppConfig):
    name = 'anexos'
