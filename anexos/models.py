import re

from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.staticfiles import finders
from django.contrib.staticfiles.storage import staticfiles_storage
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _lazy
from agroecossistema.models import Agroecossistema

from lume.models import LumeModel


def file_upload_path(instance, filename):
    path = 'arquivos_anexos/%Y/%m/%d/{0}'.format(filename)
    return path


class PastaAnexo(models.Model):
    agroecossistema = models.ForeignKey(Agroecossistema, on_delete=models.CASCADE, verbose_name=_lazy('agroecossistema'), related_name='pastas', null=True, blank=True)
    nome = models.CharField(_lazy('nome'), max_length=80)
    mae = models.ForeignKey('self', on_delete=models.CASCADE, verbose_name=_lazy('mãe'), null=True, blank=True)

    def __str__(self):
        str = ''
        if self.mae is not None:
            return '' + self.mae.__str__() + ' / ' + self.nome
        else:
            return self.nome


class Anexo(LumeModel):
    nome = models.CharField(_lazy('nome'), max_length=80, null=True, blank=True)
    descricao = models.TextField(_lazy('descrição'), null=True, blank=True)
    mimetype = models.CharField(max_length=80)
    pasta = models.ForeignKey(PastaAnexo, on_delete=models.SET_NULL, verbose_name=_lazy('pasta'), related_name='anexos', null=True, blank=True)
    agroecossistema = models.ForeignKey(Agroecossistema, on_delete=models.CASCADE, verbose_name=_lazy('agroecossistema'), related_name='anexos', null=True, blank=True)
    arquivo = models.FileField(_lazy('arquivo'), upload_to='anexos/%Y/%m/%d/')
    criacao = models.DateTimeField(_lazy('criacao'), auto_now_add=True, editable=False)
    ultima_alteracao = models.DateTimeField(_lazy('última alteração'), auto_now=True, editable=False)

    def __str__(self):
        return self.nome

    def get_absolute_url(self):
        return reverse('anexos:anexo_detail', args=(self.agroecossistema_id, self.pk))

    def get_update_url(self):
        return reverse('anexos:anexo_update', args=(self.pk,))

    def get_delete_ajax_url(self):
        return ''
        return reverse('anexos:anexo_delete_ajax', args=(self.pk,))

    def is_image(self):
        if self.mimetype is not None:
            return re.match(r'image/(gif|jpeg|png|webp)$', self.mimetype)
        return False

    def is_video(self):
        if self.mimetype is not None:
            return re.match(r'video/(x-matroska|mp4|webm|ogg)$', self.mimetype)
        return False

    def get_awesome_icon(self):
        ICONES = {
            'image': 'file-image',
            'audio': 'file-audio',
            'video': 'file-video',
            'application/pdf': 'file-pdf',
            'application/msword': 'file-word',
            'application/vnd.ms-word': 'file-word',
            'application/vnd.oasis.opendocument.text': 'file-word',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document': 'file-word',
            'application/vnd.ms-excel': 'file-excel',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.spreadsheet': 'file-excel',
            'application/vnd.oasis.opendocument.spreadsheet': 'file-excel',
            'application/vnd.ms-powerpoint': 'file-powerpoint',
            'application/vnd.openxmlformats-officedocument.presentationml.presentation': 'file-powerpoint',
            'application/vnd.oasis.opendocument.presentation': 'file-powerpoint',
            'text/plain': 'file-alt',
            'text/html': 'file-code',
            'xml': 'file-code',
            'application/json': 'file-code',
            'application/gzip': 'file-archive',
            'application/zip': 'file-archive',
        }
        try:
            return ICONES[self.mimetype]
        except:
            return ICONES['text/plain']

    def thumbnail_tag(self):
        tag = None
        if self.is_image():
            tag = '<a href="#" class="d-block mb-1 h-100 img-thumbnail anexo-thumb text-center align-middle">\
            <img class="img-fluid anexo-image" src="' + self.arquivo.url + '" alt=""></a>'
        elif self.is_video():
             tag = '<video width="180" height="180" controls>\
                      <source src="' + self.arquivo.url + '" type="' + self.mimetype + '">\
                        Your browser does not support the video tag.\
                    </video>'
        else:
            results = re.match(r'.*\.([^.]+)$', self.nome)
            if results is not None and results.lastindex > 0:
                extension = results[results.lastindex]
                filename = f'anexos/img/{extension}.png'
                if finders.find(filename) is None:
                    filename = f'anexos/img/file.png'
                tag = '<img class="img-fluid file-icon" src="' + staticfiles_storage.url(filename) + '" alt="">'
        return tag

    def as_dict(self):
        return {
            'nome': self.nome,
            'descricao': self.descricao,
            'mimetype': self.mimetype,
            'url': self.arquivo.url,
            'criacao': self.criacao
        }


class AnexoRecipiente(LumeModel):
    anexo = models.ForeignKey(Anexo, on_delete=models.CASCADE, verbose_name=_lazy('anexo'), related_name='recipientes')
    recipiente_object_id = models.IntegerField()
    recipiente_content_type = models.ForeignKey(
        ContentType,
        on_delete=models.CASCADE,
    )
    recipiente = GenericForeignKey(
        'recipiente_content_type',
        'recipiente_object_id',
    )
    criacao = models.DateTimeField(_lazy('criação'), auto_now_add=True, editable=False)
    ultima_alteracao = models.DateTimeField(_lazy('última alteração'), auto_now=True, editable=False)

    def __str__(self):
        return self.anexo.nome
