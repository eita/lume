from django import forms
from django.forms.models import inlineformset_factory
from django.urls import reverse
from django.utils.translation import gettext as _
from multiupload.fields import MultiFileField

from main.forms import LumeModelForm, LumeForm
from .models import PastaAnexo, Anexo, AnexoRecipiente
from agroecossistema.models import Agroecossistema


class PastaForm(LumeModelForm):

    def __init__(self, *args, **kwargs):
        self.can_delete = kwargs.pop('can_delete', False)

        if self.can_delete:
            agroecossistema_id = kwargs.pop('agroecossistema_id')
            self.delete_confirm_message = _('Deseja realmente apagar esta pasta?')
            self.delete_url = reverse('anexos:pasta_anexo_delete', kwargs={'agroecossistema_id': agroecossistema_id, 'pk': kwargs.get('instance').id})
            self.delete_success_url = reverse('anexos:pasta_anexo_update', kwargs={'agroecossistema_id': agroecossistema_id})
        super(PastaForm, self).__init__(*args, **kwargs)

    class Meta:
        model = PastaAnexo
        fields = ['nome']
        # labels = {
        #     'mae': _('Pasta mãe')
        # }

PastaFormSet = inlineformset_factory(
    Agroecossistema,
    PastaAnexo,
    form=PastaForm,
    can_delete=True,
    extra=1,
    min_num=0
)


class AnexoForm(LumeModelForm):

    def __init__(self, agroecossistema_id = None, *args, **kwargs):
        self.can_delete = kwargs.pop('can_delete', False)
        if self.can_delete:
            self.delete_confirm_message = _('Deseja realmente apagar este arquivo?')
            # self.delete_url = reverse('anexos:anexo_delete_ajax', kwargs={'agroecossistema_id': agroecossistema.id, 'pk': kwargs.get('instance').id})
            # self.delete_success_url = reverse('anexos:anexo_list', kwargs={'agroecossistema_id': agroecossistema.id})

        super(AnexoForm, self).__init__(*args, **kwargs)

        if agroecossistema_id:
            self.fields['pasta'].queryset = PastaAnexo.objects.filter(agroecossistema_id=agroecossistema_id)

    class Meta:
        model = Anexo
        fields = ['arquivo', 'nome', 'descricao', 'pasta']

class AnexoRestForm(LumeModelForm):
    class Meta:
        model = Anexo
        fields = []

class AnexoRecipienteForm(LumeForm):
    arquivos = MultiFileField(min_num=0, required=True)
    descricao = forms.CharField(widget=forms.Textarea, required=False)


class AnexoRecipienteForm(LumeModelForm):
    class Meta:
        model = AnexoRecipiente
        fields = []
