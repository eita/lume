from anexos.models import PastaAnexo

def organiza_anexos(anexos, agroecossistema_id, isPasta = False):
    if isPasta:
        return {"soltos": anexos}

    pastas = PastaAnexo.objects.filter(agroecossistema_id=agroecossistema_id)
    anexosOrganizados = {}
    for pasta in pastas:
        anexosOrganizados[pasta.id] = {
            'id': pasta.id,
            'nome': pasta.nome,
            'anexos': []
        }

    soltos = []
    for anexo in anexos:
        if not anexo.pasta:
            soltos.append(anexo)
            continue

        anexosOrganizados[anexo.pasta.id]['anexos'].append(anexo)
    return {
        'soltos': soltos,
        'anexosOrganizados': anexosOrganizados
    }
