import os
from django import template
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from django.template import Context
from django.utils.translation import gettext as _
from django.urls import reverse
from django.utils.text import slugify
from main.utils import build_navbar_tab
import calendar
import urllib.parse

from agroecossistema.models import CicloAnualReferencia
from economica.models import Analise
from anexos.models import Anexo

register = template.Library()

@register.inclusion_tag('anexos/inclusiontags/anexosModoGrade.html', takes_context = False)
def anexosModoGrade(agroecossistema, anexos, isPasta = False, anexosSingular = 'arquivo', anexosPlural = 'arquivos'):
    return {
        'agroecossistema': agroecossistema,
        'anexos': anexos,
        'isPasta': isPasta,
        'anexosSingular': anexosSingular,
        'anexosPlural': anexosPlural
    }

@register.inclusion_tag('anexos/inclusiontags/anexosModoGradeCard.html', takes_context = False)
def anexosModoGradeCard(agroecossistema, anexos, cardId, showMenu = True, showFooter = False):
    return {
        'agroecossistema': agroecossistema,
        'anexos': anexos,
        'cardId': "anexo_list_" + cardId if cardId != "" else "anexo_list",
        'showMenu': showMenu,
        'showFooter': showFooter
    }

@register.inclusion_tag('anexos/inclusiontags/anexosModoLista.html', takes_context = False)
def anexosModoLista(agroecossistema, anexos, related_id, titulo, isPasta = False, anexosSingular = 'arquivo', anexosPlural = 'arquivos'):
    return {
        'agroecossistema': agroecossistema,
        'anexos': anexos,
        'related_id': related_id,
        'titulo': titulo,
        'isPasta': isPasta,
        'anexosSingular': anexosSingular,
        'anexosPlural': anexosPlural
    }

@register.inclusion_tag('anexos/inclusiontags/anexosModoListaRow.html', takes_context = False)
def anexosModoListaRow(agroecossistema, anexo, related_id):
    return {
        'agroecossistema': agroecossistema,
        'anexo': anexo,
        'related_id': related_id
    }

@register.simple_tag
def get_count_anexos_from_form(form):
    for field in form.hidden_fields():
        if field.label.lower() == 'id' and field.initial is not None:
            return Anexo.objects.filter(pasta_id=field.initial).count()
    return 0

@register.simple_tag
def get_count_total_anexos_from_form(forms):
    total = 0
    for form in forms:
        total += get_count_anexos_from_form(form)
    return total

@register.simple_tag
def get_count_anexos(agroecossistema_id):
    return Anexo.objects.filter(agroecossistema_id=agroecossistema_id).count()

@register.filter
def filename(file):
    return urllib.parse.unquote(os.path.basename(file))
