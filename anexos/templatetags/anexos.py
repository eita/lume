from django import template
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from django.template import Context

from anexos.models import Anexo

register = template.Library()


@register.filter(name='get_class')
def get_class(value):
    return value.__class__.__name__


@register.simple_tag(takes_context=True)
def anexos_slider(context, modelo_relacionado):
    content_type = ContentType.objects.get_for_model(modelo_relacionado)
    anexos = Anexo.objects.filter(anexorecipiente__recipiente_content_type=content_type.pk,
                                  anexorecipiente__recipiente_object_id=modelo_relacionado.pk,
                                  mimetype__istartswith='image')
    t = context.template.engine.get_template('anexos/tag_anexos_slider.html')
    return t.render(Context({'anexos': anexos}, autoescape=context.autoescape))


@register.simple_tag(takes_context=True)
def anexos_docs(context, modelo_relacionado):
    content_type = ContentType.objects.get_for_model(modelo_relacionado)
    anexos = Anexo.objects.filter(anexorecipiente__recipiente_content_type=content_type.pk,
                                  anexorecipiente__recipiente_object_id=modelo_relacionado.pk).exclude(
        mimetype__istartswith='image')
    t = context.template.engine.get_template('anexos/tag_anexos_docs.html')
    return t.render(Context({'anexos': anexos}, autoescape=context.autoescape))


@register.simple_tag(takes_context=True)
def janela_anexos(context, modelo_relacionado):
    t = context.template.engine.get_template('anexos/janela_anexos.html')
    return t.render(Context({'var': '1'}, autoescape=context.autoescape))
