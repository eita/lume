function armengue(ev) {
    ev.preventDefault();
    window.scrollBy(0, -10);
}
function allowDrop(ev) {
    ev.preventDefault();
}
function drag(ev) {
    ev.dataTransfer.setData("anexoId", ev.target.id);
    ev.dataTransfer.setData("originPastaId", $(ev.target).data('pasta'));
    if (!isPasta) {
        $('.card-pasta').addClass('card-pasta-dragging')
        $('#maskArmengue').show();
    }
    $('.menu-pasta').addClass('card-pasta-dragging')
}
function dragEntered(ev) {
    if ($('.card-pasta').length === 0) {
        return false;
    }
    ev.preventDefault();
}
function dragLeft(ev) {
    ev.preventDefault();
}
function drop(ev) {
    ev.preventDefault();
    if ($('.card-pasta').length === 0) {
        return false;
    }
    if (!isPasta) {
        $('.card-pasta').removeClass('card-pasta-dragging');
        $('#maskArmengue').hide();
    }
    $('.menu-pasta').removeClass('card-pasta-dragging');
    const anexoIdEl = ev.dataTransfer.getData("anexoId");
    const anexoId = parseInt(anexoIdEl.replace('anexo_', ''));
    const $anexo = $(`#${anexoIdEl}`);

    let originPastaId = parseInt(ev.dataTransfer.getData("originPastaId"));
    if (isNaN(originPastaId)) {
        originPastaId = "";
    }

    let pastaId = (ev.target.id.substr(0,4) === 'menu')
        ? parseInt(ev.target.id.replace('menu_pasta_', ''))
        : parseInt(ev.target.id.replace('pasta_', ''));

    if (isNaN(pastaId)) {
        pastaId = "";
    }

    if (pastaId === originPastaId) {
        return;
    }

    moveAnexo(anexoId, originPastaId, pastaId);

}

function moveAnexo(anexoId, originPastaId, pastaId, modo='grade') {
    // console.log({anexoId, originPastaId, pastaId});
    const $anexo = $(`#anexo_${anexoId}`);
    const data = {
        id: anexoId,
        pasta_id: (pastaId !== "") ? pastaId : "-1"
    };
    // console.log(data);

    $.ajax(ajaxUrl, {
        method: 'POST',
        data: data,
        dataType: 'json',
        success: function (data) {
            // console.log(data);
            $anexo.data('pasta', pastaId);

            if (modo === 'table') {
                $("#toastTitle").html("Salvo!");
                $(".toast-body").html(`<div class="alert alert-success">Anexo movido com sucesso!</div>`);
                $(".toast").toast('show');
                return;
            }

            const $anexoCard = $anexo.parents('.anexo');

            const $originPastaCard = $((originPastaId !== "") ? `#pasta_${originPastaId}` : '#pasta_solto');
            const newQtdeOrigin = parseInt($originPastaCard.data('qtde')) - 1;
            const newTextOrigin = (newQtdeOrigin === 1)
                ? `1 ${anexosSingular}`
                : `${newQtdeOrigin} ${anexosPlural}`;
            const $targetSpanQtdeAnexos = (isPasta)
                ? $('.qtdeAnexos')
                : $originPastaCard.find('.qtdeAnexos');
            $targetSpanQtdeAnexos.text(`(${newTextOrigin})`)
            $originPastaCard.data('qtde', newQtdeOrigin.toString());
            // console.log(originPastaId, $originPastaCard, $originPastaCard.data('qtde'), $targetSpanQtdeAnexos.text());

            if (!isPasta) {
                const $pastaCard = $((pastaId !== "") ? `#pasta_${pastaId}` : '#pasta_solto');
                const newQtde = parseInt($pastaCard.data('qtde')) + 1;
                const newText = (newQtde === 1)
                ? `1 ${anexosSingular}`
                : `${newQtde} ${anexosPlural}`;
                $pastaCard.find('.qtdeAnexos').text(`(${newText})`);
                $pastaCard.data('qtde', newQtde.toString());
                // console.log(pastaId, $pastaCard, $pastaCard.data('qtde'));

                // console.log(originPastaId + " --> " + pastaId);
                $((pastaId !== "") ? `#anexo_list_${pastaId}` : '#anexo_list')
                    .append($anexoCard)
                    .masonry('appended', $anexoCard);
                $((originPastaId !== "") ? `#anexo_list_${originPastaId}` : '#anexo_list')
                    .masonry('reloadItems')
                    .masonry('layout');

                // console.log($anexoCard);
                $anexoCard.find('.card-anexo').addClass('anexo-card-selected');
                setTimeout(function() {
                    $anexoCard.find('.card-anexo').removeClass('anexo-card-selected');
                }, 1000);
                setTimeout(function() {
                    $([document.documentElement, document.body]).animate({
                        scrollTop: ($anexoCard.offset().top - 150)
                    }, 500);
                }, 300);

                return;
            }

            $anexoCard.fadeOut(100, function() {
                $('#anexo_list')
                    .masonry('remove', $anexo.parents('.anexo'))
                    .masonry('layout');
            });
        },
        error: function (e) {
            console.log('Erro!', e);
            $("#toastTitle").html("Erro!");
            $(".toast-body").html(`<div class="alert alert-danger">Falha ao salvar o evento <i>${e.message}</i>. Operação falhou!</div>`);
            $(".toast").toast('show');
        }
    });
}

function openCardMenu(e, card) {
    if (card.length > 0) {
        e.preventDefault();
        card = card[0];
        if ($('#contextMenu').is(':visible')) {
            $('#contextMenu')
                .fadeOut(300);
            return;
        }
        $('#contextMenu')
            .data('anexoId', card.id.replace('anexo_', ''))
            .data('pastaId', $(card).data('pasta'))
            .data('anexoUrl', $(card).attr('href'));
        var top = e.pageY + 10;
        var left = e.pageX;
        $("#contextMenu")
            .css({
                "top": top,
                "left": left
            })
            .fadeIn(300);
        return false;
    }
}

function populaEAbreForm(anexoId = null) {
    if (anexoId) {
        $('#id_arquivo').attr('required', false);
        $('#modal-right-wrapper .btn-apagar').show();
        $card = $(`#anexo_${anexoId}`);
        $card.find('.anexo-header').clone().appendTo("#id_arquivo_img .img-thumb");
        $("#id_arquivo_img .img-label").text($card.data('arquivo_nome'))
        $('#id_arquivo_img').show();
        $('#div_id_arquivo').hide();

        $('#modal-right-title').text($('#modal-right-title').data('title_editar'));
        $('#id_update_pk').val(anexoId);
        $('#id_nome').val($card.data('nome'));
        $('#id_descricao').val($card.data('descricao'));
        $('#id_pasta').val($card.data('pasta'));

        // $('#modal-right-wrapper').fadeIn(300);
        return;
    }

    $('#id_arquivo').attr('required', true);
    $('#modal-right-wrapper .btn-apagar').hide();
    $('#id_arquivo_img').hide();
    $('#div_id_arquivo').show();
    $('#modal-right-title').text($('#modal-right-title').data('title_criar'));
    $('#id_update_pk, #id_arquivo, #id_nome, #id_descricao, #id_pasta').val('');
    $('#modal-right-wrapper').fadeIn(300);
}


jQuery(document).ready(function () {
    Promise.all(Array.from(document.images).filter(img => !img.complete).map(img => new Promise(resolve => { img.onload = img.onerror = resolve; }))).then(() => {
        $('.anexos-masonry').masonry('layout');
    });

    $('.caption-apagar, .anexos-table-apagar').on('click', function (e) {
        e.preventDefault();
        $button = $(this);
        if (confirm(gettext('Deseja realmente apagar o arquivo?'))) {
            $.ajax($button.data('url'), {
                'method': 'POST',
                'success': function () {
                    $('#modal-right-wrapper').fadeOut(300);
                    if ($button.parents('.anexos-masonry').length > 0) {
                        $button.parents('.anexos-masonry')
                            .masonry('remove', $button.parents('.anexo'))
                            .masonry('layout');
                        return;
                    }

                    $button.parents('tr').fadeOut(300);
                },
                'error': function () {
                    alert(gettext('Não foi possível apagar.'));
                }
            });
        }
    });

    // $('.btn-toggle-modo').on('click', function (e) {
    //     const target = $(e.target).data('target');
    //     $('#modoGrade, #btnLista').toggle(target === 'grade');
    //     $('#modoLista, #btnGrade').toggle(target === 'lista');
    //     if (target === 'grade') {
    //         $('.anexos-masonry').masonry('layout');
    //     }
    // });


    $('body').on('click', function (e) {
        // console.log(e.target);
        if (
            $(e.target).parents('#contextMenu').length > 0 ||
            $(e.target).parents('#contextMenuSub').length > 0 ||
            $(e.target).hasClass('card-anexo-menu') ||
            $(e.target).parent().hasClass('card-anexo-menu')
        ) {
            return;
        }
        $("#contextMenu, #contextMenuSub").fadeOut(300);
    });

    $anexos = $(".anexo");

    $('#btn-criar').on('click', function() {
        populaEAbreForm();
    });
    $('#id_arquivo_img button').on('click', function(e) {
        e.preventDefault();
        $('#id_arquivo_img').fadeOut(300);
        $('#div_id_arquivo').fadeIn(300);
    })

    if ($anexos.length > 0) {
        $anexos.on('contextmenu', function (e) {
            const card = $(e.target).parents('.card-anexo-wrapper');
            openCardMenu(e, card);
        });
        $('.card-anexo-menu').on('click', function (e) {
            const card = $(this).siblings('.card-anexo-wrapper');
            openCardMenu(e, card);
        });
        $('#menuBaixar').on('click', function (e) {
            e.preventDefault();
            const anexoId = $('#contextMenu').data('anexoId');
            $(`#anexo_${anexoId}`)[0].click();
            $("#contextMenu").fadeOut(300);
        });
        $('#menuEditar').on('click', function (e) {
            e.preventDefault();
            const anexoId = $('#contextMenu').data('anexoId');
            populaEAbreForm(anexoId);
            // $(`#anexo_${anexoId}`).parents('.card-anexo').find('.anexo-editar')[0].click();
            $("#contextMenu").fadeOut(300);
        });
        $('#menuApagar, .modal-right-footer .btn-apagar').on('click', function (e) {
            e.preventDefault();
            const anexoId = $('#contextMenu').data('anexoId');
            $(`#anexo_${anexoId}`).parents('.card-anexo').find('.caption-apagar')[0].click();
            $("#contextMenu").fadeOut(300);
        });
        $('#menuCriar').on('click', function (e) {
            e.preventDefault();
            populaEAbreForm();
            $("#contextMenu").fadeOut(300);
        });
        $('#menuMover').on('click', function (e) {
            e.preventDefault();
            var top = e.pageY + 10;
            var left = e.pageX;
            $("#contextMenuSub")
                .css({
                    "top": top,
                    "left": left
                })
                .fadeIn(300);
        });
        $('.context-menu-sub').on('click', function (e) {
            e.preventDefault();
            const anexoId = $('#contextMenu').data('anexoId');
            const originPastaId = $('#contextMenu').data('pastaId')
                ? $('#contextMenu').data('pastaId')
                : "";
            const pastaId = $(this).data('pasta_id');
            if (originPastaId !== pastaId) {
                moveAnexo(anexoId, originPastaId, pastaId);
            }

            $('#contextMenu, #contextMenuSub').fadeOut(300);
        });

        return;
    }

    $('.anexos-table-select').on('change', function (e) {
        const $linha = $(this).parents('tr');
        const anexoId = $linha.attr('id').replace('anexo_', '');
        const originPastaId = $linha.data('pasta');
        const pastaId = $(e.target).val();
        if (originPastaId !== pastaId) {
            moveAnexo(anexoId, originPastaId, pastaId, 'table');
        }
    });

});
