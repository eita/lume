from django.contrib import admin

# Register your models here.
from anexos.models import PastaAnexo, Anexo, AnexoRecipiente

class AnexoAdmin(admin.ModelAdmin):
    list_display = ("nome", "descricao", "mimetype", "criacao",)

class AnexoRecipienteAdmin(admin.ModelAdmin):
    list_display = ("anexo", "recipiente_content_type", "recipiente", "criacao",)

admin.site.register(PastaAnexo)
admin.site.register(Anexo, AnexoAdmin)
admin.site.register(AnexoRecipiente, AnexoRecipienteAdmin)
