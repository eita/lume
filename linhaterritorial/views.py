import json
import logging

from django.core.serializers.json import DjangoJSONEncoder
from django.contrib.contenttypes.models import ContentType
from django.http import JsonResponse, HttpResponseRedirect
from django.apps import apps
from django.shortcuts import get_object_or_404
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.utils.text import slugify
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, CreateView, DetailView, UpdateView, FormView
from django.db.models import Value, CharField, F
from django_markdown.utils import markdown
from django.utils.translation import gettext as _
from rules.contrib.views import PermissionRequiredMixin
from itertools import chain
from dal import autocomplete
from linhaterritorial.utils import get_periodos

from agroecossistema.models import Agroecossistema
from linhaterritorial.forms import EventoTerritorialForm, PeriodoForm, PeriodoFormSet
from linhaterritorial.models import EventoTerritorial, TipoDimensaoTerritorial, DimensaoTerritorial, Periodo
from linhaterritorial.mixins import AjaxableResponseMixin
from main.views import LumeDeleteView
from main.models import Organizacao, OrganizacaoUsuaria
from main.utils import getHelperI18n, busca_inteligente


class EventoTerritorialListView(PermissionRequiredMixin, ListView):
    model = EventoTerritorial
    form_class = EventoTerritorialForm
    grupos = None
    skipped_years = None
    # permission_required = 'comunidade_ou_territorio.alterar'
    permission_required = "permissao_padrao"
    template_name = "linhadotempo/evento_list.html"
    modulo = None
    content_type = None
    object_id = None
    periodos = None
    comunidadeOuTerritorio = None

    def dispatch(self, request, *args, **kwargs):
        related_model = self.kwargs["related_model"]
        self.modulo = related_model.lower()
        self.content_type = ContentType.objects.get(
            app_label="main", model=self.modulo)
        self.object_id = self.kwargs["pk"]
        self.periodos = get_periodos(self.content_type, self.object_id)
        self.comunidadeOuTerritorio = apps.get_model(
            "main", related_model).objects.get(pk=self.object_id)
        return super(EventoTerritorialListView, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        if self.request.user.has_perm("eh_admin_geral"):
            return EventoTerritorial.objects.filter(content_type_id=self.content_type.id, object_id=self.object_id)

        minhas_organizacoes_ids = self.request.user.organizacoes.values_list(
            "id")

        integrantes_minhas_organizacoes_ids = (
            OrganizacaoUsuaria.objects.filter(
                organizacao_id__in=minhas_organizacoes_ids)
            .values_list("usuaria_id")
            .distinct()
        )
        eventos_minhas_organizacoes = EventoTerritorial.objects.filter(
            content_type_id=self.content_type.id,
            object_id=self.object_id,
            criado_por__in=integrantes_minhas_organizacoes_ids,
        ).select_related("dimensao", "criado_por", "periodo")

        integrantes_outras_organizacoes_ids = (
            OrganizacaoUsuaria.objects.exclude(
                organizacao_id__in=minhas_organizacoes_ids)
            .values_list("usuaria_id")
            .distinct()
        )
        eventos_minhas_organizacoes_ids = [
            evento.id for evento in eventos_minhas_organizacoes]
        eventos_outras_organizacoes = (
            EventoTerritorial.objects.filter(
                content_type_id=self.content_type.id,
                object_id=self.object_id,
                criado_por__in=integrantes_outras_organizacoes_ids,
                is_public=True,
            )
            .exclude(id__in=eventos_minhas_organizacoes_ids)
            .select_related("dimensao", "criado_por", "periodo")
            .annotate(
                editable=Value("false", output_field=CharField()),
            )
        )

        return list(chain(eventos_minhas_organizacoes, eventos_outras_organizacoes))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        form = EventoTerritorialForm()
        form.fields["periodo"].queryset = Periodo.objects.filter(
            content_type=self.content_type, object_id=self.object_id
        )

        context["grupos"] = json.dumps(
            self.get_grupos(), cls=DjangoJSONEncoder)
        context["skipped_years"] = None
        context["modulo"] = self.modulo
        context["submodulo"] = "linhaterritorial"
        context["isTerritorial"] = True
        context["content_type"] = self.content_type
        context["link_anexos"] = f"linhaterritorial:{self.content_type.model}_anexos"
        context["comunidadeOuTerritorio"] = self.comunidadeOuTerritorio
        context["titulo"] = context["comunidadeOuTerritorio"].__str__()
        context["form"] = form
        context["periodos"] = list(
            self.periodos.values("id", "coluna", "nome"))
        return context

    def get_grupos(self):
        dimensoes_gerais = (
            DimensaoTerritorial.objects.select_related(
                "tipo_dimensao").filter(nome="-1").order_by("ordem")
        )
        grupos = []
        for dimensao_geral in dimensoes_gerais:
            tipo_dimensao_slug = slugify(dimensao_geral.tipo_dimensao.nome)
            helperI18n = getHelperI18n(
                "linhaterritorial_helpers", dimensao_geral.tipo_dimensao, dimensao_geral.tipo_dimensao.nome
            )
            contentI18n = (
                f"{helperI18n['nome']} <span></span>"
                if "ajuda" in helperI18n and helperI18n["ajuda"]
                else helperI18n["nome"]
            )
            className = f"{tipo_dimensao_slug} group-{dimensao_geral.tipo_dimensao.id}"

            grupos.append(
                {
                    "id": dimensao_geral.tipo_dimensao.id,
                    "order": dimensao_geral.tipo_dimensao.ordem,
                    "content": contentI18n,
                    "help": markdown(helperI18n["ajuda"]) if "ajuda" in helperI18n else "",
                    "className": className,
                }
            )

        return grupos

        # dimensoes = DimensaoTerritorial.objects.select_related('tipo_dimensao').all()
        # grupos = []
        # nested_groups_per_tipo = {}
        # for dimensao in dimensoes:
        #     dimensao_slug = slugify(dimensao.nome)
        #     tipo_dimensao_slug = slugify(dimensao.tipo_dimensao.nome) if dimensao.tipo_dimensao else None
        #
        #     if tipo_dimensao_slug:
        #         if not tipo_dimensao_slug in nested_groups_per_tipo:
        #             nested_groups_per_tipo[tipo_dimensao_slug] = []
        #             helperI18n = getHelperI18n('linhaterritorial_helpers', dimensao.tipo_dimensao, dimensao.tipo_dimensao.nome)
        #             grupos.append({
        #                 'id': 100 + dimensao.tipo_dimensao.id,
        #                 'content': helperI18n['nome'],
        #                 'nestedGroups': nested_groups_per_tipo[tipo_dimensao_slug],
        #                 'className': slugify(dimensao.tipo_dimensao.nome)
        #             })
        #
        #         if not dimensao.id in nested_groups_per_tipo[tipo_dimensao_slug]:
        #             nested_groups_per_tipo[tipo_dimensao_slug].append(dimensao.id)
        #
        #     helperI18n = getHelperI18n('linhaterritorial_helpers', dimensao, dimensao.nome)
        #     contentI18n = f"{helperI18n['nome']} <span></span>" if 'ajuda' in helperI18n and helperI18n['ajuda'] else helperI18n['nome']
        #     className = f"{slugify(tipo_dimensao_slug)} group-{dimensao.id}" if tipo_dimensao_slug else f"group-{dimensao.id}"
        #
        #     grupos.append({
        #         'id': dimensao.id,
        #         'order': dimensao.ordem,
        #         'content': contentI18n,
        #         'help': markdown(helperI18n['ajuda']) if 'ajuda' in helperI18n else '',
        #         'className': className
        #     })
        #
        # return grupos


class EventoTerritorialRestUpdateView(PermissionRequiredMixin, AjaxableResponseMixin, UpdateView):
    model = EventoTerritorial
    form_class = EventoTerritorialForm
    permission_required = "permissao_padrao"
    success_message = _("Atualizado!")

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(EventoTerritorialRestUpdateView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        data = self.request.POST

        titulo = data.get("titulo")
        periodo = int(data.get("periodo"))
        detalhes = data.get("detalhes")
        dimensao_id = data.get("subdimensao")
        if dimensao_id:
            dimensao_id = int(dimensao_id)
        tipo_dimensao_id = int(data.get("tipo_dimensao"))

        dimensao = (
            DimensaoTerritorial.objects.get(pk=dimensao_id)
            if dimensao_id
            else DimensaoTerritorial.objects.get(nome="-1", tipo_dimensao_id=tipo_dimensao_id)
        )

        if data.get("id"):
            obj, created = EventoTerritorial.objects.get_or_create(
                id=data.get("id"))
            obj.titulo = titulo
            obj.dimensao = dimensao
            obj.periodo_id = int(data.get("periodo"))
            obj.detalhes = detalhes
            return obj

        obj, created = EventoTerritorial.objects.get_or_create(
            content_type_id=int(data.get("content_type_id")),
            object_id=int(data.get("object_id")),
            criado_por_id=self.request.user.id,
            titulo=titulo,
            dimensao=dimensao,
            periodo_id=int(data.get("periodo")),
            detalhes=detalhes,
        )
        return obj


class EventoTerritorialRestDeleteView(PermissionRequiredMixin, AjaxableResponseMixin, LumeDeleteView):
    model = EventoTerritorial
    permission_required = "permissao_padrao"

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(EventoTerritorialRestDeleteView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        data = self.request.POST

        obj = EventoTerritorial.objects.get(id=data.get("id"))

        return obj


class PeriodosView(PermissionRequiredMixin, FormView):
    model = Periodo
    permission_required = "permissao_padrao"
    form_class = PeriodoForm
    template_name = "linhaterritorial/periodos.html"
    modulo = None
    content_type = None
    object_id = None
    periodos = None
    comunidadeOuTerritorio = None

    def dispatch(self, request, *args, **kwargs):
        related_model = self.kwargs["related_model"]
        self.modulo = related_model.lower()
        self.content_type = ContentType.objects.get(
            app_label="main", model=self.modulo)
        self.object_id = self.kwargs["pk"]
        self.periodos = get_periodos(self.content_type, self.object_id)
        self.comunidadeOuTerritorio = apps.get_model(
            "main", related_model).objects.get(pk=self.object_id)
        return super(PeriodosView, self).dispatch(request, *args, **kwargs)

    def get_permission_object(self):
        return self.request.user

    def post(self, request, *args, **kwargs):
        formset = PeriodoFormSet(request.POST)
        if formset.is_valid():
            return self.form_valid(formset)
        else:
            return self.form_invalid(formset)

    def form_invalid(self, formset):
        return self.render_to_response(self.get_context_data(form=form))

    def form_valid(self, formset):
        instances = []
        hasErrors = False
        for form in formset:
            if form.is_valid():
                if form.cleaned_data["DELETE"] and form.cleaned_data["id"] is not None:
                    form.instance.delete()
                else:
                    form.instance.content_type = self.content_type
                    form.instance.object_id = self.object_id
                    instances.append(form.save())
            elif not form.cleaned_data["DELETE"]:
                hasErrors = True

        if hasErrors:
            return self.render_to_response(self.get_context_data(form=form))

        return HttpResponseRedirect(reverse(self.modulo + "_linhaterritorial_periodos", kwargs=self.kwargs))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if self.request.GET.get("force_edit"):
            context["force_edit"] = True

        context["modulo"] = self.modulo
        context["submodulo"] = "periodos"
        context["content_type"] = self.content_type
        context["comunidadeOuTerritorio"] = self.comunidadeOuTerritorio
        context["titulo"] = f"Períodos da linha do tempo na {context['modulo']} {self.comunidadeOuTerritorio}"

        if self.request.POST:
            context["formset"] = PeriodoFormSet(self.request.POST)
        else:
            context["formset"] = PeriodoFormSet(queryset=self.periodos)

        return context


class ApiDimensaoView(ListView):
    modulo = None
    content_type = None
    object_id = None
    comunidadeOuTerritorio = None
    tipo_dimensao_id = None
    is_empty = None
    q = None
    """
    Intended to be called via ajax. Returns json list of DimensaoTerritorial from TipoDimensaoTerritorial, or list of TipoDimensaoTerritorial.
    """

    def dispatch(self, request, *args, **kwargs):
        if request.is_ajax():
            related_model = self.kwargs["related_model"]
            self.modulo = related_model.lower()
            self.content_type = ContentType.objects.get(
                app_label="main", model=self.modulo)
            self.object_id = self.kwargs.get("pk", None)
            self.comunidadeOuTerritorio = apps.get_model(
                "main", related_model).objects.get(pk=self.object_id)
            self.is_empty = self.kwargs.get("empty", False)
            self.tipo_dimensao_id = self.kwargs.get("tipo_dimensao_id", None)
            self.q = self.request.GET.get("q", None)
            return super(ApiDimensaoView, self).dispatch(request, *args, **kwargs)
        return JsonResponse("Acesso não permitido", status=500, safe=False)

    def get_queryset(self):
        if self.is_empty:
            return DimensaoTerritorial.objects.none()

        if self.tipo_dimensao_id:
            qs = DimensaoTerritorial.objects.filter(
                tipo_dimensao_id=self.tipo_dimensao_id).exclude(nome="-1")
            if self.q:
                qs = qs.filter(nome__icontains=self.q)
            return qs.order_by("ordem")

        qs = DimensaoTerritorial.objects.select_related(
            "tipo_dimensao").filter(nome="-1")
        if self.q:
            qs = qs.filter(tipo_dimensao__nome__icontains=self.q)
        return qs.order_by("ordem")

    def get(self, request, *args, **kwargs):
        values = ("id" if self.tipo_dimensao_id else "tipo_dimensao_id", "text")
        annotate = {"text": F("nome") if self.tipo_dimensao_id else F(
            "tipo_dimensao__nome")}
        queryset = self.get_queryset().annotate(**annotate).values(*values)
        data = list(queryset)
        return JsonResponse({"results": data}, status=200, safe=False)
