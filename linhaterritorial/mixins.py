from django.http import JsonResponse
from django.utils.translation import gettext as _


class AjaxableResponseMixin:
    def form_invalid(self, form):
        response = super().form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse({'status': 'error', 'message': form.errors.as_text()}, status=400)
        else:
            return response

    def form_valid(self, form):
        response = super().form_valid(form)

        try:
            msg = self.success_message
        except AttributeError:
            msg = _('%(objeto)s gravado com sucesso!') % {'objeto': self.object.__class__.__name__}

        if self.request.is_ajax():
            data = {
                'status': 'success',
                'message': msg,
                'pk' : self.object.id
            }
            return JsonResponse(data)
        else:
            return response
