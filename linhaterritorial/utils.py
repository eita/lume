import math

from django.utils.translation import gettext as _
from datetime import datetime
from linhaterritorial.models import Periodo

def get_periodos(content_type, object_id):
    periodos = Periodo.objects.filter(content_type=content_type, object_id=object_id)
    if periodos.count() > 0:
        return periodos

    # Let's create a default set of periodos
    Periodo.objects.create(
        content_type_id=content_type.id,
        object_id=object_id,
        coluna=1,
        nome=_('Antigamente')
    )

    ano_atual = datetime.now().year
    ultima_decada = int(math.floor(ano_atual / 10) * 10) + 10
    coluna = 2
    for ano in range(1960, ultima_decada, 10):
        Periodo.objects.create(
            content_type_id=content_type.id,
            object_id=object_id,
            coluna=coluna,
            nome=f"{_('Década ')} {ano}"
        )
        coluna += 1
    return Periodo.objects.filter(content_type=content_type, object_id=object_id)
