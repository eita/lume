import json

from django.conf import settings
from django.contrib.contenttypes.fields import GenericRelation, GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from django.core.serializers.json import DjangoJSONEncoder
from django.db import models
from django.urls import reverse
from django_markdown.models import MarkdownField

from django.utils.translation import gettext as _
from django.utils.translation import gettext_lazy as _lazy

from anexos.models import AnexoRecipiente
from lume.models import LumeModel
from main.messages_from_db import linhaterritorial_helpers
from main.models import Organizacao


class TipoDimensaoTerritorial(models.Model):
    nome = models.CharField(max_length=80, verbose_name=_lazy('nome'))
    descricao = MarkdownField(null=True, blank=True,
                              verbose_name=_lazy('descrição'))
    ordem = models.SmallIntegerField(null=True, verbose_name=_lazy('ordem'))

    def __str__(self):
        return self.nome

    class Meta:
        ordering = ('ordem',)


class DimensaoTerritorial(models.Model):
    nome = models.CharField(max_length=80, verbose_name=_lazy('nome'))
    tipo_dimensao = models.ForeignKey(TipoDimensaoTerritorial, models.PROTECT, null=True,
                                      blank=True, verbose_name=_lazy('tipo da dimensão'), related_name='dimensoes')
    ordem = models.SmallIntegerField(null=True, verbose_name=_lazy('ordem'))
    descricao = MarkdownField(null=True, blank=True,
                              verbose_name=_lazy('descrição'))

    HELPERS = linhaterritorial_helpers

    def __str__(self):
        return self.nome if not self.tipo_dimensao else self.tipo_dimensao.nome + ' / ' + self.nome

    class Meta:
        ordering = ('ordem',)


class Periodo(models.Model):
    coluna = models.PositiveSmallIntegerField(verbose_name=_lazy('coluna'))
    nome = models.CharField(max_length=30, verbose_name=_lazy('nome'))

    limit = models.Q(app_label='main', model='comunidade') | models.Q(
        app_label='main', model='territorio')
    content_type = models.ForeignKey(
        ContentType, on_delete=models.CASCADE, limit_choices_to=limit)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()

    def __str__(self):
        return self.nome

    class Meta:
        ordering = ('coluna',)
        unique_together = ('coluna', 'content_type', 'object_id')


class EventoTerritorial(models.Model):
    periodo = models.ForeignKey(Periodo, models.CASCADE, verbose_name=_lazy(
        'período'), related_name='eventos')
    titulo = models.CharField(max_length=1024, verbose_name=_lazy('título'))
    detalhes = models.TextField(
        null=True, blank=True, verbose_name=_lazy('detalhes'))
    dimensao = models.ForeignKey(
        DimensaoTerritorial, models.PROTECT, verbose_name=_lazy('dimensão'))
    criado_por = models.ForeignKey(
        settings.AUTH_USER_MODEL, models.DO_NOTHING, verbose_name=_lazy('criado por'))
    is_public = models.BooleanField(_lazy('É público'), default=False, help_text=_lazy(
        'Caso marcado, o evento trritorial será visível para outras organizações do mesmo território.'))
    anexos = GenericRelation(
        AnexoRecipiente,
        verbose_name=_lazy('anexos'),
        content_type_field='recipiente_content_type',
        object_id_field='recipiente_object_id',
    )
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy('criação'))
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy('última alteração'))

    limit = models.Q(app_label='main', model='comunidade') | models.Q(
        app_label='main', model='territorio')
    content_type = models.ForeignKey(
        ContentType, on_delete=models.CASCADE, limit_choices_to=limit)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()

    class Meta:
        ordering = ('-pk',)

    def __str__(self):
        return self.titulo

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        link = 'comunidade_linhaterritorial' if self.content_type.model == 'comunidade' else 'territorio_linhaterritorial'
        return reverse(link, kwargs={'pk': self.object_id}) + "?evento_id=" + str(self.pk)

    def get_anexos_url(self):
        link = 'linhaterritorial:comunidade_anexos' if self.content_type.model == 'comunidade' else 'linhaterritorial:territorio_anexos'
        return reverse(link, kwargs={"pk": self.pk})

    def get_anexos_list(self):
        a_list = []
        anexo_rs = self.anexos.all()
        for anexo_r in anexo_rs:
            anexo = anexo_r.anexo
            a_list.append(anexo.as_dict())
        return a_list

    # def clean_fields(self, exclude=None):
    #     super().clean_fields(exclude=exclude)
