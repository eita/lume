# Generated by Django 2.2 on 2022-03-23 19:12

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('linhaterritorial', '0005_auto_20220323_1514'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='dimensaoterritorial',
            options={'ordering': ('ordem',)},
        ),
    ]
