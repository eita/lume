# Generated by Django 2.2 on 2022-04-18 10:22

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('linhaterritorial', '0013_auto_20220418_0718'),
    ]

    operations = [
        migrations.CreateModel(
            name='Periodo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('coluna', models.PositiveSmallIntegerField(verbose_name='coluna')),
                ('nome', models.CharField(max_length=30, verbose_name='nome')),
            ],
        ),
        migrations.RemoveField(
            model_name='eventoterritorial',
            name='data_fim',
        ),
        migrations.RemoveField(
            model_name='eventoterritorial',
            name='data_inicio',
        ),
        migrations.AddField(
            model_name='eventoterritorial',
            name='periodo',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='eventos', to='linhaterritorial.Periodo', verbose_name='período'),
            preserve_default=False,
        ),
    ]
