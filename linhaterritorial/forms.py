from bootstrap_datepicker_plus import DatePickerInput, YearPickerInput
from django import forms
from django.urls import reverse
from django.utils.translation import gettext as _
from django_summernote.widgets import SummernoteInplaceWidget

from linhaterritorial.models import EventoTerritorial, Periodo, DimensaoTerritorial
from main.forms import LumeModelForm


class ChoiceFieldNoValidation(forms.ChoiceField):
    def validate(self, value):
        pass


class EventoTerritorialForm(LumeModelForm):
    tipo_dimensao = ChoiceFieldNoValidation(
        required=False,
        choices=[],
        label=_('Dimensão'),
    )
    subdimensao = ChoiceFieldNoValidation(
        required=False,
        choices=[],
        label=_('Subdimensão'),
        help_text=_(
            'A escolha de subdimensão é opcional: você pode deixar em branco')
    )
    subdimensao_nome = forms.CharField(
        required=False, widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        super(EventoTerritorialForm, self).__init__(*args, **kwargs)

    class Meta:
        model = EventoTerritorial
        fields = [
            'titulo',
            'tipo_dimensao',
            'subdimensao',
            'subdimensao_nome',
            'periodo',
            'detalhes',
            'is_public',
        ]
        widgets = {
            'titulo': SummernoteInplaceWidget(),
            'detalhes': SummernoteInplaceWidget()
        }
        labels = {
            'periodo': _('Período'),
            'titulo': _('Eventos'),
            'detalhes': _('Detalhes')
        }


class PeriodoForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.can_delete = kwargs.pop('can_delete', False)
        if self.can_delete:
            self.delete_confirm_message = _(
                'Deseja realmente apagar este período?')
            self.delete_url = reverse('linhaterritorial:periodo_delete', kwargs={
                                      'pk': kwargs.get('instance').id})
            self.delete_success_url = reverse('linhaterritorial:periodos')

        super(PeriodoForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Periodo
        fields = ['coluna', 'nome']


PeriodoFormSet = forms.modelformset_factory(
    Periodo, form=PeriodoForm, can_delete=True, extra=1, min_num=0)
