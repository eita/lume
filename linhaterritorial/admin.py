from django.contrib import admin

from linhaterritorial.models import TipoDimensaoTerritorial, DimensaoTerritorial, EventoTerritorial

# Register your models here.
admin.site.register(TipoDimensaoTerritorial)
admin.site.register(DimensaoTerritorial)
admin.site.register(EventoTerritorial)
