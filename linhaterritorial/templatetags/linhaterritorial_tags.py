import json
from django import template
from datetime import datetime
from django.core.serializers.json import DjangoJSONEncoder
from django.utils.translation import gettext as _
register = template.Library()

@register.filter()
def as_visjs_json(eventos):
    visjs_json = []
    for evento in eventos:
        item = {
            'id': evento.id,
            'start': datetime(evento.periodo.coluna + 1900, 1, 3),
            'end': datetime(evento.periodo.coluna + 1900, 12, 30),
            'content': evento.titulo,
            'group': evento.dimensao.tipo_dimensao.id,
            'dimensao': evento.dimensao.id if evento.dimensao.nome != -1 else None,
            'dimensao_nome': evento.dimensao.nome if evento.dimensao.nome != '-1' else None,
            'periodo': evento.periodo.id,
            'periodo_coluna': evento.periodo.coluna,
            'titulo': evento.titulo,
            'detalhes': evento.detalhes,
            # 'anexos_url': evento.get_anexos_url(),
            'title': evento.titulo,
            'ano_formatted': evento.periodo.nome,
            'is_public': evento.is_public,
            'className': 'territorial'
        }

        if hasattr(evento, 'editable'):
            item['editable'] = False if evento.editable == 'false' else True

        if evento.criado_por is not None:
            item['autor'] = evento.criado_por.get_username()

        # item['anexos'] = list(evento.get_anexos_list())
        item['anexos'] = []

        visjs_json.append(item)

    return json.dumps(visjs_json, cls=DjangoJSONEncoder)
