from django.urls import path
from . import views
from anexos import views as anexosViews

app_name = 'linhaterritorial'
urlpatterns = (
    path('anexos/comunidade/<int:pk>/', anexosViews.AnexoListView.as_view(), {'related_type': 'linhaterritorial', 'related_model': 'EventoTerritorial', 'related_list_url': 'comunidade_linhaterritorial'}, name='comunidade_anexos'),
    path('anexos/territorio/<int:pk>/', anexosViews.AnexoListView.as_view(), {'related_type': 'linhaterritorial', 'related_model': 'EventoTerritorial', 'related_list_url': 'territorio_linhaterritorial'}, name='territorio_anexos'),
    path('evento_territorial/rest/update', views.EventoTerritorialRestUpdateView.as_view(), name="evento_rest_update"),
    path('evento_territorial/rest/delete', views.EventoTerritorialRestDeleteView.as_view(), name="evento_rest_delete"),
)
