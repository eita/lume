import re

from django.conf import settings
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.db import models as models
from django.urls import reverse
from django.utils.html import strip_tags
from django.utils.text import Truncator
from django.utils.safestring import mark_safe
from django.utils.translation import gettext as _
from django.utils.translation import gettext_lazy as _lazy

from anexos.models import AnexoRecipiente
from lume.models import LumeModel
from agroecossistema.models import Agroecossistema


class Anotacao(LumeModel):
    nome = models.CharField(_lazy('nome'), max_length=80, null=True, blank=True)
    texto = models.TextField(verbose_name=_lazy('texto'))
    data_visita = models.DateField(verbose_name=_lazy('data da visita'))
    criacao = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_lazy('criação'))
    ultima_alteracao = models.DateTimeField(auto_now=True, editable=False, verbose_name=_lazy('última alteração'))

    # Relationship Fields
    agroecossistema = models.ForeignKey(
        Agroecossistema, verbose_name=_lazy('agroecossistema'),
        on_delete=models.CASCADE, related_name="anotacoes",
    )
    autor = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_lazy('autor'),
        on_delete=models.PROTECT, related_name="anotacoes",
    )

    anexos = GenericRelation(AnexoRecipiente, verbose_name=_lazy('anexos'),
                             content_type_field='recipiente_content_type',
                             object_id_field='recipiente_object_id', )

    class Meta:
        ordering = ('-criacao',)

    def __unicode__(self):
        return u'%s' % self.pk

    def __str__(self):
        if self.nome:
            return self.nome
        return _('Visita ou atividade de') + ' ' + self.data_visita.strftime('%d/%m/%Y')

    def get_absolute_url(self):
        return reverse('cadernodecampo:anotacao_detail', args=(self.agroecossistema_id, self.pk,))

    def get_update_url(self):
        return reverse('cadernodecampo:anotacao_update', args=(self.agroecossistema_id, self.pk,))

    def get_anexos_url(self):
        return reverse('cadernodecampo:anotacao_anexos', args=(self.agroecossistema_id, self.pk,))

    def get_anexo_add_url(self):
        return reverse('cadernodecampo:anotacao_anexos_create', args=(self.agroecossistema.id, self.pk,))

    def safe_relato(self):
        # solution: https://stackoverflow.com/questions/42166336/embedding-youtube-videos-from-user-submitted-comments-in-django-webpage
        texto = self.texto
        texto = self.convert_text_yt(texto)
        texto = self.convert_text_vimeo(texto)
        return texto

    def convert_text_vimeo(self, text):
        vimeo_regex = (r'http(?:s?):\/\/(?:www\.)?'
                         '(vimeo)\.(com|be)/'
                         '(watch\?v=|embed/|v/|.+\?v=)?([0-9]{4,15})')
        return self.get_embed_from_regex(text, vimeo_regex, self.iframe_video_vimeo)

    def convert_text_yt(self, text):
        youtube_regex = (r'http(?:s?):\/\/(?:www\.)?'
                         '(youtube|youtu|youtube-nocookie)\.(com|be)/'
                         '(watch\?v=|embed/|v/|.+\?v=)?([^&=%\?]{11})')
        return self.get_embed_from_regex(text, youtube_regex, self.iframe_video_yt)

    def get_embed_from_regex(self, text, text_regex, callback_embed):
        pattern = re.compile(text_regex)
        matches = [
            {'origin': m.group(), 'id': m.group(4), 'position': m.start()}
            for m in pattern.finditer(text)
        ]
        for match in matches:
            id = match['id']
            origin = match['origin']
            position = match['position']

            text = text.replace(origin, callback_embed(id))
        return mark_safe(text)

    def iframe_video_yt(self, id):
        return '<iframe width="100%" height="500" src="https://www.youtube.com/embed/{}"></iframe>'.format(id)

    def iframe_video_vimeo(self, id):
        return '<iframe src="https://player.vimeo.com/video/{}" width="100%" height="500" frameborder="0" \
            allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>'.format(id)
