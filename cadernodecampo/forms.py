from bootstrap_datepicker_plus import DatePickerInput
from django import forms
from django.urls import reverse
from django_summernote.widgets import SummernoteInplaceWidget

from main.forms import LumeModelForm
from .models import Anotacao
from django.utils.translation import gettext as _


class AnotacaoForm(LumeModelForm):
    def __init__(self, *args, **kwargs):
        self.can_delete = kwargs.pop('can_delete', False)
        if self.can_delete:
            agroecossistema_id = kwargs.pop('agroecossistema_id')
            self.delete_confirm_message = _(
                'Deseja realmente apagar esta anotacao?')
            self.delete_url = reverse('cadernodecampo:anotacao_delete',
                                      kwargs={'agroecossistema_id': agroecossistema_id,
                                              'pk': kwargs.get('instance').id})
            self.delete_success_url = reverse('cadernodecampo:anotacao_list',
                                              kwargs={'agroecossistema_id': agroecossistema_id})

        super(AnotacaoForm, self).__init__(*args, **kwargs)
        self.fields['data_visita'].input_formats = [
            '%Y-%m-%d', '%d/%m/%Y', '%d/%m/%y']

    class Meta:
        model = Anotacao
        fields = ['nome', 'data_visita', 'texto']
        widgets = {
            'texto': SummernoteInplaceWidget({'disable_attachment': True}),
            'data_visita': DatePickerInput(required=True, options={"locale": "pt-br", "format": "DD/MM/YYYY"})
        }
        labels = {
            'data_visita': _('Data da visita ou atividade')
        }
