from . import models

from rest_framework import serializers


class AnotacaoSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Anotacao
        fields = (
            'pk',
            'texto',
            'criacao',
            'ultima_alteracao',
        )
