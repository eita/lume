from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import DetailView, ListView, UpdateView, CreateView
from django.views.generic.edit import FormMixin
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import gettext as _
from rules.contrib.views import PermissionRequiredMixin

from agroecossistema.models import Agroecossistema
from anexos.models import AnexoRecipiente, Anexo
from anexos.forms import AnexoForm
from main.views import LumeDeleteView
from .models import Anotacao
from .forms import AnotacaoForm
from anexos.utils import organiza_anexos


class AnotacaoListView(PermissionRequiredMixin, ListView):
    model = Anotacao
    permission_required = 'agroecossistema.visualizar'

    def get_permission_object(self):
        agroecossistema = get_object_or_404(Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['agroecossistema'] = Agroecossistema.objects.get(id=self.kwargs['agroecossistema_id'])
        context['modulo'] = 'cadernodecampo'
        context['enable_datatable_export'] = True
        return context

    def get_queryset(self):
        agroecossistema = get_object_or_404(Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return Anotacao.objects.filter(agroecossistema=agroecossistema)


class AnotacaoCreateView(PermissionRequiredMixin, CreateView):
    model = Anotacao
    form_class = AnotacaoForm
    titulo = _('Nova Anotação')
    permission_required = 'agroecossistema.alterar'

    def get_permission_object(self):
        agroecossistema = get_object_or_404(Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['agroecossistema'] = Agroecossistema.objects.get(id=self.kwargs['agroecossistema_id'])
        context['titulo'] = self.titulo
        context['modulo'] = 'cadernodecampo'
        return context

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.agroecossistema = Agroecossistema.objects.get(id=self.kwargs['agroecossistema_id'])
        obj.autor = self.request.user
        obj.save()
        return HttpResponseRedirect(self.get_success_url(obj.id))

    def get_success_url(self, object_id=None):
        if object_id is None:
            object_id = self.object.id

        view_name = 'cadernodecampo:anotacao_list'
        # No need for reverse_lazy here, because it's called inside the method
        return reverse(view_name, kwargs={
            'agroecossistema_id': self.kwargs['agroecossistema_id']})


class AnotacaoDetailView(PermissionRequiredMixin, FormMixin, DetailView):
    model = Anotacao
    permission_required = 'agroecossistema.visualizar'
    form_class = AnexoForm

    def get_form_kwargs(self):
        kwargs = super(AnotacaoDetailView, self).get_form_kwargs()
        kwargs.update({
            'can_delete': self.request.user.has_perm('permissao_padrao')
        })
        kwargs['agroecossistema_id'] = self.kwargs['agroecossistema_id']
        if 'data' in kwargs and 'update_pk' in kwargs['data'] and kwargs['data']['update_pk'] != '':
            kwargs['instance'] = Anexo.objects.get(pk=int(kwargs['data']['update_pk']))

        return kwargs

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        anexo = form.save(commit=False)

        anexo.agroecossistema=Agroecossistema.objects.get(pk=self.kwargs['agroecossistema_id'])

        arquivo = form.cleaned_data['arquivo']
        if arquivo.__class__.__name__ == 'InMemoryUploadedFile':
            anexo.mimetype = arquivo.content_type
            if anexo.nome is None or anexo.nome == "":
                anexo.nome = os.path.splitext(arquivo.name)[0]

        anexo.save()

        related_type = ContentType.objects.get(app_label='cadernodecampo', model='anotacao')
        anexo_recipiente, created = AnexoRecipiente.objects.get_or_create(
            recipiente_object_id=self.kwargs['pk'],
            anexo_id=anexo.id,
            recipiente_content_type_id=related_type.id
        )

        return HttpResponseRedirect(reverse(
            "cadernodecampo:anotacao_detail",
            args=(self.kwargs['agroecossistema_id'],self.kwargs['pk'],)
        ))

    def get_permission_object(self):
        agroecossistema = get_object_or_404(Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        anexosRecipientesList = AnexoRecipiente.objects.filter(recipiente_object_id=self.kwargs['pk']).order_by('-ultima_alteracao').values_list('anexo_id')
        anexos = Anexo.objects.filter(pk__in=anexosRecipientesList)
        context['agroecossistema'] = Agroecossistema.objects.get(id=self.kwargs['agroecossistema_id'])
        context['modulo'] = 'cadernodecampo'
        context['anexos'] = organiza_anexos(anexos, self.kwargs['agroecossistema_id'])
        return context


class AnotacaoUpdateView(PermissionRequiredMixin, UpdateView):
    model = Anotacao
    form_class = AnotacaoForm
    titulo = _('Editar Anotação')
    permission_required = 'agroecossistema.alterar'

    def get_permission_object(self):
        agroecossistema = get_object_or_404(Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    # sending variables to the form
    def get_form_kwargs(self):
        kwargs = super(AnotacaoUpdateView, self).get_form_kwargs()
        kwargs.update(
            {'can_delete': self.request.user.has_perm('agroecossistema.alterar', self.get_object().agroecossistema)})
        kwargs.update({'agroecossistema_id': self.kwargs['agroecossistema_id']})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['agroecossistema'] = Agroecossistema.objects.get(id=self.kwargs['agroecossistema_id'])
        context['titulo'] = self.titulo
        context['modulo'] = 'cadernodecampo'
        return context


class AnotacaoDeleteView(PermissionRequiredMixin, LumeDeleteView):
    model = Anotacao
    permission_required = 'agroecossistema.alterar'

    def get_permission_object(self):
        agroecossistema = get_object_or_404(Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(AnotacaoDeleteView, self).dispatch(*args, **kwargs)
