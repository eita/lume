from . import models
from . import serializers
from rest_framework import viewsets, permissions


class AnotacaoViewSet(viewsets.ModelViewSet):
    """ViewSet for the Anotacao class"""

    queryset = models.Anotacao.objects.all()
    serializer_class = serializers.AnotacaoSerializer
    permission_classes = [permissions.IsAuthenticated]
