import unittest
from django.urls import reverse
from django.test import Client
from .models import Anotacao
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType


def create_django_contrib_auth_models_user(**kwargs):
    defaults = {}
    defaults["username"] = "username"
    defaults["email"] = "username@tempurl.com"
    defaults.update(**kwargs)
    return User.objects.create(**defaults)


def create_django_contrib_auth_models_group(**kwargs):
    defaults = {}
    defaults["name"] = "group"
    defaults.update(**kwargs)
    return Group.objects.create(**defaults)


def create_django_contrib_contenttypes_models_contenttype(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return ContentType.objects.create(**defaults)


def create_anotacao(**kwargs):
    defaults = {}
    defaults["texto"] = "texto"
    defaults.update(**kwargs)
    if "agroecossistema" not in defaults:
        defaults["agroecossistema"] = create_main_models_agroecossistema()
    if "autor" not in defaults:
        defaults["autor"] = create_django_contrib_auth_models_user()
    return Anotacao.objects.create(**defaults)


class AnotacaoViewTest(unittest.TestCase):
    '''
    Tests for Anotacao
    '''

    def setUp(self):
        self.client = Client()

    def test_list_anotacao(self):
        url = reverse('cadernodecampo:anotacao_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_anotacao(self):
        url = reverse('cadernodecampo:anotacao_create')
        data = {
            "texto": "texto",
            "agroecossistema": create_main_models_agroecossistema().pk,
            "autor": create_django_contrib_auth_models_user().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_anotacao(self):
        anotacao = create_anotacao()
        url = reverse('cadernodecampo:anotacao_detail', args=[anotacao.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_anotacao(self):
        anotacao = create_anotacao()
        data = {
            "texto": "texto",
            "agroecossistema": create_main_models_agroecossistema().pk,
            "autor": create_django_contrib_auth_models_user().pk,
        }
        url = reverse('cadernodecampo:anotacao_update', args=[anotacao.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)
