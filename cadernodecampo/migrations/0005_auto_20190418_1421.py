# Generated by Django 2.1.5 on 2019-04-18 14:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cadernodecampo', '0004_anotacao_deleted'),
    ]

    operations = [
        migrations.AlterField(
            model_name='anotacao',
            name='deleted',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
