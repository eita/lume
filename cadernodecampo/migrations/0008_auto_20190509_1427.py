# Generated by Django 2.1.5 on 2019-05-09 14:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cadernodecampo', '0007_auto_20190424_1402'),
    ]

    operations = [
        migrations.AlterField(
            model_name='anotacao',
            name='deleted',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
