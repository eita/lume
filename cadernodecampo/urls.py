from django.urls import path, include
from rest_framework import routers

from . import api
from . import views
from anexos import views as anexosViews

app_name = "cadernodecampo"
router = routers.DefaultRouter()
router.register(r"anotacao", api.AnotacaoViewSet)

urlpatterns = (
    # urls for DRF
    path("api/v1/", include(router.urls)),
    # urls for Anotacao
    path("", views.AnotacaoListView.as_view(), name="anotacao_list"),
    path("notas/create/", views.AnotacaoCreateView.as_view(), name="anotacao_create"),
    path(
        "notas/detail/<int:pk>/",
        views.AnotacaoDetailView.as_view(),
        name="anotacao_detail",
    ),
    path(
        "notas/update/<int:pk>/",
        views.AnotacaoUpdateView.as_view(),
        name="anotacao_update",
    ),
    path(
        "notas/delete/<int:pk>/",
        views.AnotacaoDeleteView.as_view(),
        name="anotacao_delete",
    ),
    path(
        "notas/anexos/<int:pk>/",
        anexosViews.AnexoListView.as_view(),
        {"related_type": "cadernodecampo", "related_model": "Anotacao"},
        name="anotacao_anexos",
    ),
    path(
        "notas/anexos/<int:pk>/create/",
        anexosViews.AnexoRelatedTypeCreateView.as_view(),
        {"related_type": "cadernodecampo", "related_model": "Anotacao"},
        name="anotacao_anexos_create",
    ),
)
