from django.apps import AppConfig


class CadernodecampoConfig(AppConfig):
    name = 'cadernodecampo'
