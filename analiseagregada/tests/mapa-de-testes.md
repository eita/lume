# Cenarios

## Como usuario anonimo

* teste bloqueio ao acessar listagem; OK
* teste bloqueio ao acessar inserção; OK
* teste bloqueio ao acessar alteração; OK
* teste bloqueio ao deletar; OK
* teste bloqueio ao acessar detalhes; OK
* teste bloqueio ao acessar analise economica;
* teste bloqueio ao acessar analise qualitativa;
* teste bloqueio ao acessar universo;
* teste bloqueio ao alterar universo;

## Como usuaria inativa

* teste bloqueio ao acessar listagem;
* teste bloqueio ao acessar inserção;
* teste bloqueio ao acessar alteração;
* teste bloqueio ao acessar deleção;
* teste bloqueio ao acessar detalhes;
* teste bloqueio ao acessar analise economica;
* teste bloqueio ao acessar analise qualitativa;
* teste bloqueio ao acessar universo;
* teste bloqueio ao alterar universo;

## Como usuaria ativa

* teste bloqueio ao acessar listagem;
* teste bloqueio ao acessar inserção;
* teste bloqueio ao acessar alteração;
* teste bloqueio ao acessar deleção;
* teste bloqueio ao acessar detalhes;
* teste bloqueio ao acessar analise economica;
* teste bloqueio ao acessar analise qualitativa;
* teste bloqueio ao acessar universo;
* teste bloqueio ao alterar universo;

## Como usuaria ativa, membra de uma organização

* teste acesso a listagem;
* teste bloqueio ao acessar listagem de outra orgaizacao;
* teste acesso a inserção; OK!
* teste bloqueio ao acessar inserção de outra organização;
* teste acesso a alteração;
* teste bloqueio ao acessar alteração de outra organização;
* teste acesso a deleção; OK!
* teste bloqueio ao acessar deleção de outra organização;
* teste bloqueio para tarefas de admin da organização (verificar);
* teste acesso a detalhes; OK!

## Como usuario ativo, membro e admin de uma organização

* teste acesso a tarefas de admin da organização (verificar);

* teste acesso a listagem; OK
* teste acesso a inserção; OK!

## Como usuario ativo e admin (grupo)

* teste acesso a tarefas de usuarias que estão no grupo admin (verificar);
* verificar relação deste tipo de user com a [não] participação em organização

## Como usuario ativo e superusuaria

* teste acesso a tarefas de superusuaria (verificar);
* verificar relação deste tipo de user com a [não] participação em organização

