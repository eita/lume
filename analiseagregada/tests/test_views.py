from django.urls import reverse
from django.test import TestCase, RequestFactory

from model_bakery import baker

from .cenarios import (CenarioUsuariaAnonima,
                       CenarioUsuariaInativa,
                       CenarioUsuariaAtiva,
                       CenarioUsuariaOrganizacao,
                       CenarioUsuariaOrganizacaoAdmin,
                       CenarioUsuariaGrupoAdmin,
                       CenarioSuperUsuaria,
                       CenarioUsuariaGrupoAgregadoOrganizacoes)
from main.models import Organizacao
from agroecossistema.models import Agroecossistema, CicloAnualReferencia
from linhadotempo.models import EventoGatilho
from analiseagregada.models import Analiseagregada
from analiseagregada import views
from qualitativa.models import Analise as AnaliseQualitativa
from economica.models import Analise as AnaliseEconomica


class AnaliseagregadaUsuariaAnonimaTests(CenarioUsuariaAnonima):

    def test_bloqueio_acesso_quando_get_lista(self):
        request = self.factory.get(reverse('analiseagregada:list'))
        request.user = self.user
        response = views.AnaliseagregadaListView.as_view()(request)

        self.assertEqual(response.status_code, 302)

    def test_bloqueio_acesso_quando_get_form(self):
        request = self.factory.get(reverse('analiseagregada:form'))
        request.user = self.user
        response = views.AnaliseagregadaFormView.as_view()(request)

        self.assertEqual(response.status_code, 302)

    def test_bloqueio_acesso_quando_post_delete(self):
        analiseagregada = baker.make('Analiseagregada')
        kwargs = {'pk': analiseagregada.id}
        request = self.factory.post(reverse('analiseagregada:delete', kwargs=kwargs),
                                    kwargs)
        request.user = self.user
        response = views.AnaliseagregadaDeleteView.as_view()(request, **kwargs)

        self.assertEqual(len(Analiseagregada.objects.all()), 1)

    def test_bloqueio_acesso_quando_get_detalhes(self):
        analiseagregada = baker.make('Analiseagregada')
        kwargs = {'pk': analiseagregada.id}
        request = self.factory.get(reverse('analiseagregada:detail', kwargs=kwargs))
        request.user = self.user
        response = views.AnaliseagregadaDetailView.as_view()(request, **kwargs)

        self.assertEqual(response.status_code, 302)


class AnaliseagregadaUsuariaOrganizacaoTests(CenarioUsuariaOrganizacao):
    def setUp(self):
        super().setUp()
        evento_gatilho = baker.make('EventoGatilho')
        self.analiseagregada = baker.make('Analiseagregada',
                                          autor=self.user,
                                          organizacao=self.usuaria_organizacao.organizacao)

        self.agroecossistema = baker.make('Agroecossistema',
                                          autor=self.user)
        self.analisequalitativa = baker.make(AnaliseQualitativa,
                                             ano=2018,
                                             agroecossistema=self.agroecossistema)

        agroecossistema_ids = f'[[{self.agroecossistema.id}, 0],]'
        agroecossistema_ids = {'agroecossistema_ids': eval(agroecossistema_ids)}

        analise_qualitativa_ids = f'[[{self.agroecossistema.id}, {self.analisequalitativa.id}],]'
        analise_qualitativa_ids = {'agroecossistema_ids': eval(analise_qualitativa_ids)}

        self.analiseagregada_guiada = baker.make('Analiseagregada',
                                                 nome='analise agregada guiada',
                                                 ano=2018,
                                                 autor=self.user,
                                                 json_criterios_selecao=str(analise_qualitativa_ids))

        self.analiseagregada_guiada_gatilho = baker.make('Analiseagregada',
                                                         nome='analise agregada guiada gatilho',
                                                         ano=2018,
                                                         autor=self.user,
                                                         evento_gatilho=evento_gatilho,
                                                         json_criterios_selecao=str(agroecossistema_ids))

        ciclo_anual_ref = baker.make('CicloAnualReferencia',
                                     agroecossistema=self.agroecossistema,
                                     ano=2018)

        analiseeconomica = baker.make(AnaliseEconomica,
                                      ciclo_anual_referencia=ciclo_anual_ref)

        self.form_data_dadosgerais = {'organizacao': self.usuaria_organizacao.organizacao.id,
                                      'nome': 'test dadosgerais',
                                      'descricao': 'Descrição de teste para essa nova análise agregada',
                                      'ano': '2018',
                                      'evento_gatilho': evento_gatilho.id,

                                      'indicadores_economicos-TOTAL_FORMS': '1',
                                      'indicadores_economicos-INITIAL_FORMS': '0',
                                      'indicadores_economicos-MAX_NUM_FORMS': '1000',
                                      'indicadores_economicos-MIN_NUM_FORMS': '0',

                                      'indicadores_qualitativos-TOTAL_FORMS': '1',
                                      'indicadores_qualitativos-INITIAL_FORMS': '0',
                                      'indicadores_qualitativos-MAX_NUM_FORMS': '1000',
                                      'indicadores_qualitativos-MIN_NUM_FORMS': '0'}

    def test_acesso_quando_get_lista(self):
        request = self.factory.get(reverse('analiseagregada:list'))
        request.user = self.user
        response = views.AnaliseagregadaListView.as_view()(request)

        self.assertEqual(response.status_code, 200)

    def test_acesso_quando_get_detalhes(self):
        kwargs = {'pk': self.analiseagregada_guiada.id}
        request = self.factory.get(reverse('analiseagregada:detail', kwargs=kwargs))
        request.user = self.user
        response = views.AnaliseagregadaDetailView.as_view()(request, **kwargs)

        self.assertEqual(response.status_code, 200)

    def test_acesso_quando_get_analise_economica(self):
        kwargs = {'pk': self.analiseagregada_guiada.id}
        request = self.factory.get(reverse('analiseagregada:economica', kwargs=kwargs))
        request.user = self.user
        response = views.AnaliseagregadaEconomicaDetailView.as_view()(request, **kwargs)

        self.assertEqual(response.status_code, 200)

    def test_acesso_quando_get_analise_economica_com_gatilho(self):
        kwargs = {'pk': self.analiseagregada_guiada_gatilho.id}
        request = self.factory.get(reverse('analiseagregada:economica', kwargs=kwargs))
        request.user = self.user
        response = views.AnaliseagregadaEconomicaDetailView.as_view()(request, **kwargs)

        self.assertEqual(response.status_code, 200)

    def test_acesso_quando_get_analise_qualitativa(self):
        kwargs = {'pk': self.analiseagregada_guiada.id}
        request = self.factory.get(reverse('analiseagregada:qualitativa', kwargs=kwargs))
        request.user = self.user
        response = views.AnaliseagregadaQualitativaDetailView.as_view()(request, **kwargs)

        self.assertEqual(response.status_code, 200)

    def test_acesso_quando_get_analise_qualitativa_com_gatilho(self):
        kwargs = {'pk': self.analiseagregada_guiada_gatilho.id}
        request = self.factory.get(reverse('analiseagregada:qualitativa', kwargs=kwargs))
        request.user = self.user
        response = views.AnaliseagregadaQualitativaDetailView.as_view()(request, **kwargs)

        self.assertEqual(response.status_code, 200)

    def test_get_delete_analiseagregada(self):
        kwargs = {'pk': self.analiseagregada.id}
        request = self.factory.get(reverse('analiseagregada:delete', kwargs=kwargs), kwargs)
        request.user = self.user
        response = views.AnaliseagregadaDeleteView.as_view()(request, **kwargs)

    def test_delete_analiseagregada(self):
        kwargs = {'pk': self.analiseagregada.id}
        request = self.factory.post(reverse('analiseagregada:delete', kwargs=kwargs), kwargs)
        request.user = self.user
        response = views.AnaliseagregadaDeleteView.as_view()(request, **kwargs)

        self.assertEqual(len(Analiseagregada.objects.filter(id=kwargs['pk'])), 0)

    def test_create_dadosgerais_analiseagregada(self):
        request = self.factory.post(reverse('analiseagregada:form'),
                                    self.form_data_dadosgerais)
        request.user = self.user
        response = views.AnaliseagregadaFormView.as_view()(request)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(Analiseagregada.objects.filter(nome='test dadosgerais')), 1)

    def test_post_form_dadosgerais(self):
        kwargs = {'pk': self.analiseagregada.pk,
                  'slug_step': 'dados-gerais'}

        self.form_data_dadosgerais['pk'] = self.analiseagregada.id

        request = self.factory.post(reverse('analiseagregada:form_edit', kwargs=kwargs),
                                    self.form_data_dadosgerais)
        request.user = self.user
        response = views.AnaliseagregadaFormView.as_view()(request, **kwargs)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(Analiseagregada.objects.filter(nome='test dadosgerais')), 1)

    def test_post_form_agroecossistemas(self):
        kwargs = {'pk': self.analiseagregada.pk,
                  'slug_step': 'agroecossistemas'}

        form_data_agroecossistemas = {
            'pk': self.analiseagregada.pk,
            'agroecossistemas': [f'{self.agroecossistema.id}']
        }

        request = self.factory.post(reverse('analiseagregada:form_edit', kwargs=kwargs),
                                   form_data_agroecossistemas)
        request.user = self.user
        response = views.AnaliseagregadaFormView.as_view()(request, **kwargs)

        self.assertTrue(
            Analiseagregada.objects.get(
                pk=self.analiseagregada.pk).json_criterios_selecao is not None)

    '''
    TODO fix django.contrib.messages.api.MessageFailure:
    You cannot add messages without installing django.contrib.messages.middleware.MessageMiddleware
    def test_get_form_agroecossistemas(self):
        kwargs = {'pk': self.analiseagregada.pk,
                  'slug_step': 'agroecossistemas'}

        form_data_agroecossistemas = {
            'pk': self.analiseagregada.pk,
            'agroecossistemas': [f'{self.agroecossistema.id}']
        }

        request = self.factory.get(reverse('analiseagregada:form_edit', kwargs=kwargs),
                                   form_data_agroecossistemas)
        request.user = self.user
        response = views.AnaliseagregadaFormView.as_view()(request, **kwargs)

        self.assertEqual(response.status_code, 200)
    '''

    def test_get_form_analisequalitativa(self):
        kwargs = {'pk': self.analiseagregada.pk,
                  'slug_step': 'analise-qualitativa'}

        request = self.factory.get(reverse('analiseagregada:form_edit', kwargs=kwargs))
        request.user = self.user
        response = views.AnaliseagregadaFormView.as_view()(request, **kwargs)

        self.assertEqual(response.status_code, 200)

    def test_post_form_analisequalitativa(self):
        kwargs = {'pk': self.analiseagregada.pk,
                  'slug_step': 'analise-qualitativa'}

        analise_qualitativa_ids = [f'[{self.agroecossistema.id}, {self.analisequalitativa.id}]']
        form_data_analisequalitativa = {
            'pk': self.analiseagregada.pk,
            'analise_qualitativa_ids': analise_qualitativa_ids
        }

        request = self.factory.post(reverse('analiseagregada:form_edit', kwargs=kwargs),
                                    form_data_analisequalitativa)
        request.user = self.user
        response = views.AnaliseagregadaFormView.as_view()(request, **kwargs)

        json = Analiseagregada.objects.get(pk=self.analiseagregada.pk).json_criterios_selecao
        json = eval(json)

        self.assertEqual([str(json['agroecossistema_ids'][0])], analise_qualitativa_ids)

    def test_get_form_resumo(self):
        kwargs = {'pk': self.analiseagregada.pk,
                  'slug_step': 'resumo'}

        request = self.factory.get(reverse('analiseagregada:form_edit', kwargs=kwargs))
        request.user = self.user
        response = views.AnaliseagregadaFormView.as_view()(request, **kwargs)

        self.assertEqual(response.status_code, 200)

    def test_post_form_resumo(self):
        kwargs = {'pk': self.analiseagregada.pk,
                  'slug_step': 'resumo'}

        form_data_resumo = {
            'pk': self.analiseagregada.pk,
            'nome_analiseagregada': 'nome alterado',
            'descricao_analiseagregada': 'descricao alterada'
        }

        request = self.factory.post(reverse('analiseagregada:form_edit', kwargs=kwargs),
                                    form_data_resumo)
        request.user = self.user
        response = views.AnaliseagregadaFormView.as_view()(request, **kwargs)

        self.assertEqual(len(Analiseagregada.objects.filter(nome='nome alterado')), 1)

    def test_get_universo(self):
        kwargs = {'pk': self.analiseagregada_guiada.pk}

        request = self.factory.get(reverse('analiseagregada:universo', kwargs=kwargs))
        request.user = self.user
        response = views.AnaliseagregadaUniversoView.as_view()(request, **kwargs)

        self.assertEqual(response.status_code, 200)

    def test_universo_change_visibility(self):
        kwargs = {'pk': self.analiseagregada_guiada.pk,
                  'agroecossistema': self.agroecossistema.id}

        request = self.factory.get(reverse('analiseagregada:toggle_agroecossistema', kwargs=kwargs))
        request.user = self.user
        # invisibiliza
        response = views.ToggleAgroecossistemaView.as_view()(request, **kwargs)
        # visibiliza
        response = views.ToggleAgroecossistemaView.as_view()(request, **kwargs)

        self.assertEqual(response.status_code, 200)

    def test_get_universo(self):
        kwargs = {'pk': self.analiseagregada_guiada.pk}

        request = self.factory.get(reverse('analiseagregada:universo', kwargs=kwargs))
        request.user = self.user
        response = views.AnaliseagregadaUniversoView.as_view()(request, **kwargs)

        self.assertEqual(response.status_code, 200)


class AnaliseagregadaUsuariaGrupoAdminTests(CenarioUsuariaGrupoAdmin):
    def setUp(self):
        super().setUp()
        evento_gatilho = baker.make('EventoGatilho')
        self.analiseagregada = baker.make('Analiseagregada',
                                          autor=self.user,
                                          evento_gatilho=evento_gatilho)

    def test_acesso_quando_get_lista(self):
        request = self.factory.get(reverse('analiseagregada:list'))
        request.user = self.user
        response = views.AnaliseagregadaListView.as_view()(request)

        self.assertEqual(response.status_code, 200)

    def test_acesso_quando_get_create(self):
        request = self.factory.get(reverse('analiseagregada:form'))
        request.user = self.user
        response = views.AnaliseagregadaFormView.as_view()(request)

        self.assertEqual(response.status_code, 200)

    '''
    TODO fix django.contrib.messages.api.MessageFailure:
    You cannot add messages without installing django.contrib.messages.middleware.MessageMiddleware
    def test_get_form_agroecossistemas(self):
        kwargs = {'pk': self.analiseagregada.pk,
                  'slug_step': 'agroecossistemas'}

        agroecossistema = baker.make('Agroecossistema',
                                     autor = self.user)

        form_data_agroecossistemas = {
            'pk': self.analiseagregada.pk,
            'agroecossistemas': [f'{agroecossistema.id}']
        }

        request = self.factory.get(reverse('analiseagregada:form_edit', kwargs=kwargs),
                                   form_data_agroecossistemas)
        request.user = self.user
        response = views.AnaliseagregadaFormView.as_view()(request, **kwargs)

        self.assertEqual(response.status_code, 200)
    '''

    def test_get_form_analisequalitativa(self):
        kwargs = {'pk': self.analiseagregada.pk,
                  'slug_step': 'analise-qualitativa'}

        request = self.factory.get(reverse('analiseagregada:form_edit', kwargs=kwargs))
        request.user = self.user
        response = views.AnaliseagregadaFormView.as_view()(request, **kwargs)

        self.assertEqual(response.status_code, 200)

    def test_get_form_resumo(self):
        kwargs = {'pk': self.analiseagregada.pk,
                  'slug_step': 'resumo'}

        request = self.factory.get(reverse('analiseagregada:form_edit', kwargs=kwargs))
        request.user = self.user
        response = views.AnaliseagregadaFormView.as_view()(request, **kwargs)

        self.assertEqual(response.status_code, 200)


class AnaliseagregadaSuperUsuariaTests(CenarioSuperUsuaria):

    def test_acesso_quando_get_lista(self):
        request = self.factory.get(reverse('analiseagregada:list'))
        request.user = self.user
        response = views.AnaliseagregadaListView.as_view()(request)

        self.assertEqual(response.status_code, 200)

    def test_acesso_quando_get_create(self):
        request = self.factory.get(reverse('analiseagregada:form'))
        request.user = self.user
        response = views.AnaliseagregadaFormView.as_view()(request)

        self.assertEqual(response.status_code, 200)


class AnaliseagregadaUsuariaGrupoAgregadoOrganizacoes(
        CenarioUsuariaGrupoAgregadoOrganizacoes):

    def setUp(self):
        super().setUp()

        organizacao_a = baker.make('Organizacao')
        self.analiseagregada_a = baker.make('Analiseagregada',
                                            organizacao=organizacao_a,
                                            autor=self.user)

        organizacao_b = baker.make('Organizacao')
        self.analiseagregada_b = baker.make('Analiseagregada',
                                            organizacao=organizacao_b,
                                            autor=self.user)

    def test_consulta_lista_analises_agregadas_inter_organizacoes(self):
        request = self.factory.get(reverse('analiseagregada:list'))
        request.user = self.user
        response = views.AnaliseagregadaListView.as_view()(request)
        context = response.context_data

        # self.assertEqual(len(context['object_list']), 2)
        # self.assertTrue(request.user.has_perm('organizacao.ver_todos'))
        self.assertEqual(response.status_code, 200)
        self.assertTrue(self.analiseagregada_a.organizacao != self.analiseagregada_b.organizacao)

    def test_consulta_detalhes_analises_agregadas_inter_organizacoes(self):
        kwargs = {'pk': self.analiseagregada_a.id}
        request = self.factory.get(reverse('analiseagregada:detail', kwargs=kwargs))
        request.user = self.user
        response_a = views.AnaliseagregadaDetailView.as_view()(request, **kwargs)

        kwargs = {'pk': self.analiseagregada_b.id}
        request = self.factory.get(reverse('analiseagregada:detail', kwargs=kwargs))
        request.user = self.user
        response_b = views.AnaliseagregadaDetailView.as_view()(request, **kwargs)

        self.assertEqual(response_a.status_code, 200)
        self.assertEqual(response_b.status_code, 200)

    # test_criar_evento_gatilho_inter_organizacoes

