from django.contrib.auth.models import AnonymousUser, Group
from django.test import TestCase, RequestFactory
from model_bakery import baker
from main.models import Usuaria, OrganizacaoUsuaria


class CenarioUsuariaAnonima(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = AnonymousUser()


class CenarioUsuariaInativa(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = baker.make('Usuaria',
                               is_active=False)


class CenarioUsuariaAtiva(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = baker.make('Usuaria',
                               is_superuser=False,
                               is_active=True)


class CenarioUsuariaOrganizacao(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = baker.make('Usuaria',
                               is_superuser=False,
                               is_active=True)
        self.usuaria_organizacao = baker.make('OrganizacaoUsuaria',
                                              usuaria=self.user,
                                              is_admin=False)


class CenarioUsuariaOrganizacaoAdmin(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = baker.make('Usuaria',
                               is_superuser=False,
                               is_active=True)
        self.usuaria_organizacao = baker.make('OrganizacaoUsuaria',
                                              usuaria=self.user,
                                              is_admin=True)


class CenarioUsuariaGrupoAdmin(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = baker.make('Usuaria',
                               is_superuser=False,
                               is_active=True)
        self.grupo = baker.make('Group', name='admin')
        self.user.groups.add(self.grupo)
        self.user.save()


class CenarioUsuariaGrupoAgregadoOrganizacoes(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = baker.make('Usuaria',
                               is_superuser=False,
                               is_active=True)
        self.grupo = baker.make('Group', name='agregado-de-organizacoes')
        self.user.groups.add(self.grupo)
        self.user.save()


class CenarioSuperUsuaria(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = baker.make('Usuaria',
                               is_superuser=True,
                               is_active=True)

