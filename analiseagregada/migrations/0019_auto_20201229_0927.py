# Generated by Django 2.1.5 on 2020-12-29 12:27

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('analiseagregada', '0018_auto_20200921_0953'),
    ]

    operations = [
        migrations.AlterField(
            model_name='analiseagregada',
            name='autor',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='analises_agregadas', to=settings.AUTH_USER_MODEL, verbose_name='autor(a)'),
        ),
        migrations.AlterField(
            model_name='analiseagregada',
            name='evento_gatilho',
            field=models.ForeignKey(blank=True, help_text='A escolha de um evento-gatilho implicará na        exclusão dos agroecossistemas que não tenham uma simulação        com este evento gatilho no ciclo definido acima.', null=True, on_delete=django.db.models.deletion.PROTECT, related_name='analiseagregada', to='linhadotempo.EventoGatilho', verbose_name='evento gatilho'),
        ),
        migrations.AlterField(
            model_name='analiseagregadaindicadoreconomico',
            name='analiseagregada',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='indicadores_economicos', to='analiseagregada.Analiseagregada', verbose_name='análise agregada'),
        ),
        migrations.AlterField(
            model_name='analiseagregadaindicadoreconomico',
            name='indicador',
            field=models.CharField(choices=[('pb', 'pb'), ('estoque', 'estoque'), ('doacoes_trocas', 'doacoes_trocas'), ('autoconsumo', 'autoconsumo'), ('venda', 'venda'), ('ci', 'ci'), ('va', 'va'), ('cift', 'cift'), ('vat', 'vat'), ('cp', 'cp'), ('ra', 'ra'), ('ram', 'ram'), ('pt', 'pt'), ('rna_pluriatividade', 'rna_pluriatividade'), ('rna_transferencia', 'rna_transferencia'), ('mercantil', 'mercantil'), ('domestico_cuidados', 'domestico_cuidados'), ('participacao_social', 'participacao_social'), ('pluriatividade', 'pluriatividade'), ('total_cp', 'Recursos Produtivos Mercantis'), ('total_rentabilidade_monetaria_bruta', 'Rentabilidade Monetária Bruta'), ('total_pb_venda', 'Produtos Vendidos'), ('total_pp', 'Recursos Reproduzidos (insumos)'), ('total_mercantilizacao', 'Índice de Mercantilização'), ('total_pb_autoconsumo', 'Produtos Consumidos (autoconsumo)'), ('total_reciprocidade', 'Recursos Recebidos (entrada reciprocidade)'), ('total_recursos_reproduzidos', 'Total recursos reproduzidos'), ('total_pb_doacoes_trocas', 'Produtos Doados (saída reciprocidade)')], max_length=500, verbose_name='indicador'),
        ),
        migrations.AlterField(
            model_name='analiseagregadaindicadoreconomico',
            name='operador',
            field=models.CharField(choices=[('>', '>'), ('>=', '>='), ('<', '<'), ('<=', '<=')], max_length=2, verbose_name='operador'),
        ),
        migrations.AlterField(
            model_name='analiseagregadaindicadoreconomico',
            name='valor',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=5, verbose_name='valor'),
        ),
        migrations.AlterField(
            model_name='analiseagregadaindicadorqualitativo',
            name='analiseagregada',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='indicadores_qualitativos', to='analiseagregada.Analiseagregada', verbose_name='Análise agregada'),
        ),
        migrations.AlterField(
            model_name='analiseagregadaindicadorqualitativo',
            name='atributo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='qualitativa.Atributo', verbose_name='atributo'),
        ),
        migrations.AlterField(
            model_name='analiseagregadaindicadorqualitativo',
            name='operador',
            field=models.CharField(choices=[('>', '>'), ('>=', '>='), ('<', '<'), ('<=', '<='), ('=', '=')], max_length=2, verbose_name='operador'),
        ),
        migrations.AlterField(
            model_name='analiseagregadaindicadorqualitativo',
            name='parametro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='qualitativa.Parametro', verbose_name='parâmetro'),
        ),
        migrations.AlterField(
            model_name='analiseagregadaindicadorqualitativo',
            name='valor',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=5, verbose_name='valor'),
        ),
    ]
