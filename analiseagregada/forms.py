from django import forms
from django.utils.translation import gettext as _

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Button
from dal import autocomplete

from qualitativa.models import Analise as AnaliseQualitativa
from linhadotempo.models import EventoGatilho
from main.forms import LumeModelForm, LumeForm
from agroecossistema.models import (Agroecossistema,
                                    Pessoa, ComposicaoNsga, Terra)
from analiseagregada.models import (Analiseagregada, AnaliseagregadaIndicadorEconomico,
                                    AnaliseagregadaIndicadorQualitativo)


class AnaliseagregadaFormWizard(LumeModelForm):
    def __init__(self, *args, **kwargs):
        self.can_delete = kwargs.pop('can_delete', False)
        if self.can_delete:
            self.delete_confirm_message = _('Deseja realmente apagar esta organização e todos os elementos que fazem parte dela (agroecossistemas, etc)?')
            self.delete_url = reverse('organizacao_delete', kwargs={'pk': kwargs.get('instance').id})
            self.delete_success_url = reverse('organizacao_list')

        super(AnaliseagregadaFormWizard, self).__init__(*args, **kwargs)

        self.helper.form_id = 'FormWizard'
        btn_submit = [i for i, o in enumerate(self.helper.inputs) if o.input_type == 'submit']
        self.helper.inputs[btn_submit[0]].value = _('Próximo')
        self.helper.add_input(
            Button('btnBack', _('Voltar'), css_class='btn-primary action-button-previous'))

    class Meta:
        model = Analiseagregada
        fields = ['nome', 'ano', 'evento_gatilho', 'descricao']


class DadosGeraisForm(AnaliseagregadaFormWizard):

    def __init__(self, *args, **kwargs):
        super(DadosGeraisForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Analiseagregada
        fields = ['organizacao', 'nome', 'ano', 'evento_gatilho', 'descricao']
        labels = {
            'ano': _('Ano'),
            'evento_gatilho': _('Evento gatilho'),
        }
        widgets = {
            'descricao': forms.Textarea(attrs={ 'rows':2, }),
            'organizacao': autocomplete.ModelSelect2(url='organizacao_autocomplete'),
            'evento_gatilho': autocomplete.ModelSelect2(
                url='eventogatilho_autocomplete',
                forward=['organizacao']
            )
        }


class AnaliseagregadaIndicadorEconomicoModelForm(forms.ModelForm):

    class Meta:
        model = AnaliseagregadaIndicadorEconomico
        exclude = ('analiseagregada',)


def indicador_economico_form_set(model):

    return forms.inlineformset_factory(
        model,
        AnaliseagregadaIndicadorEconomico,
        form=AnaliseagregadaIndicadorEconomicoModelForm, extra=1)


class AnaliseagregadaIndicadorQualitativoModelForm(forms.ModelForm):

    class Meta:
        model = AnaliseagregadaIndicadorQualitativo
        exclude = ('analiseagregada',)
        '''
        widgets = {
            'atributo': autocomplete.ModelSelect2(
                url='atributo_autocomplete'),
            'parametro': autocomplete.ModelSelect2(
                url='parametro_autocomplete',
                forward=['atributo']
            )
        }
        '''


def indicador_qualitativo_form_set(model):

    return forms.inlineformset_factory(
        model,
        AnaliseagregadaIndicadorQualitativo,
        form=AnaliseagregadaIndicadorQualitativoModelForm, extra=1)


class AgroecossistemasForm(AnaliseagregadaFormWizard):
    agroecossistemas = forms.CharField(
        widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        super(AgroecossistemasForm, self).__init__(*args, **kwargs)
        self.helper.label_class = 'col-lg-0'
        self.helper.field_class = 'col-lg-12'

    class Meta:
        model = Analiseagregada
        fields = ['agroecossistemas']


class AnaliseQualitativaForm(AnaliseagregadaFormWizard):
    analise_qualitativa_ids = forms.CharField(
        widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        super(AnaliseQualitativaForm, self).__init__(*args, **kwargs)
        self.helper.label_class = 'col-lg-0'
        self.helper.field_class = 'col-lg-12'

    class Meta:
        model = Analiseagregada
        fields = ['analise_qualitativa_ids']


class ResumoForm(AnaliseagregadaFormWizard):
    nome_analiseagregada = forms.CharField(
        widget=forms.HiddenInput())
    descricao_analiseagregada = forms.CharField(
        widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        super(ResumoForm, self).__init__(*args, **kwargs)
        self.helper.label_class = 'col-lg-0'
        self.helper.field_class = 'col-lg-12'
        btn_submit = [i for i, o in enumerate(self.helper.inputs) if o.input_type == 'submit']
        self.helper.inputs[btn_submit[0]].value = _('Concluir')
        self.helper.inputs[btn_submit[0]].field_classes = 'btn btn-success'

    class Meta:
        model = Analiseagregada
        fields = ['nome_analiseagregada']


'''
class AnaliseagregadaResumoForm(DadosGeraisForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'FormWizardResume'
'''
