import json
import re
from collections import OrderedDict
from django.shortcuts import redirect
from django.http import JsonResponse
from django.views.generic.edit import FormView
from django.views.generic import (
    DetailView,
    ListView,
    DeleteView,
)
from django.utils.translation import gettext as _
from django.utils.functional import cached_property
from django.utils.text import slugify
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse, reverse_lazy
from django.contrib import messages
from rules.contrib.views import PermissionRequiredMixin

from formsetview.views.mixins import FormSetViewMixin

from relatorios.serializers import TotaisAnaliseEconomicaSerializer
from relatorios.models import TotaisAnaliseEconomica
from economica.utils import LJSONRenderer
from economica.models import Analise as AnaliseEconomica
from qualitativa.models import Analise, Parametro
from agroecossistema.models import Agroecossistema, CicloAnualReferencia
from .forms import (
    DadosGeraisForm,
    indicador_economico_form_set,
    indicador_qualitativo_form_set,
    ResumoForm,
    AnaliseQualitativaForm,
    AgroecossistemasForm,
)
from .mixins import AjaxableResponseMixin
from .models import Analiseagregada
from main.utils import get_qualitativa_helper_i18n


class AnaliseagregadaListView(PermissionRequiredMixin, ListView):
    model = Analiseagregada
    permission_required = "analiseagregada.visualizar"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["modulo"] = "analiseagregada"
        context["own_analisesagregadas"] = self.get_own_analisesagregadas()
        return context

    def get_queryset(self):
        if self.request.user.has_perm("organizacao.ver_todos"):
            return Analiseagregada.objects.all()
        elif self.request.user.is_anonymous:
            return Analiseagregada.objects.filter(is_analise_modelo=True)
        else:
            return Analiseagregada.objects.filter(
                organizacao__usuarias=self.request.user
            )

    def get_own_analisesagregadas(self):
        if self.request.user.is_anonymous:
            return None
        return Analiseagregada.objects.filter(autor=self.request.user)


class AnaliseagregadaMixin:
    analiseagregada = None
    lista_agroecossistemas = None
    lista_agroecossistemas_only_visible = None

    def serialize_total(self, total):
        if not total:
            return None

        return (
            LJSONRenderer()
            .render(TotaisAnaliseEconomicaSerializer(total).data)
            .decode("utf-8")
        )

    def get_analise_economica(self, car):

        obj = self.get_analiseagregada()

        # obter analise economica
        car_analise_economica = car.analises_economicas.get()

        # obter total relacionado ao ciclo anual ref.
        try:
            totais_economica = car_analise_economica.totais
            # except RelatedObjectDoesNotExist:
        except:
            totais_economica = None

        return totais_economica

    def contabilizar_medias_totais(self, totais, data):
        items = TotaisAnaliseEconomicaSerializer(totais).data

        for k, v in items.items():
            if not k in data:
                data[k] = {"sum": 0, "total_items": 0}

            if not v is None and type(v) is not list:
                data[k]["total_items"] += 1
                data[k]["sum"] += float(v)

        return data

    def get_data_analise_agroecossistemas(self, only_visible=False):

        # obter lista de agroecossistemas
        agroecossistemas = self.get_agroecossistemas_ids(only_visible=only_visible)

        data = []
        data_json = []
        media_analises = {}
        media_simulacoes = {}
        totais_agroecossistemas = []
        obj = self.get_analiseagregada()
        evento_gatilho = obj.evento_gatilho

        # Totais relacionados aos ciclos anuais dos agroecossistemas
        for agroecossistema in agroecossistemas:

            try:
                agroecossistema = Agroecossistema.objects.get(id=agroecossistema)
            except:
                continue

            # obter ciclo anual ref.
            # agroecossistema_id + ano
            ciclo_anual_ref = CicloAnualReferencia.objects.filter(
                agroecossistema=agroecossistema, ano=obj.ano
            )

            # obter não - simulação do car
            car = ciclo_anual_ref.filter(sim_slug__isnull=True)
            try:
                car = car[0]
            except:
                continue

            totais_economica = self.get_analise_economica(car)
            if totais_economica == None:
                continue

            # obter este item para formar as medias
            media_analises = self.contabilizar_medias_totais(
                totais_economica, media_analises
            )

            # data serialization
            totais_economica_json = self.serialize_total(totais_economica)

            # obter simulação do car
            if evento_gatilho:
                car_simulacao = ciclo_anual_ref.filter(
                    sim_evento_gatilho=evento_gatilho
                )
                car_simulacao = car_simulacao[0]

                totais_simulacao = self.get_analise_economica(car_simulacao)

                # obter este item para formar as medias
                media_simulacoes = self.contabilizar_medias_totais(
                    totais_simulacao, media_simulacoes
                )
            else:
                totais_simulacao = []

            # data serialization
            totais_simulacao_json = self.serialize_total(totais_simulacao)

            new_data = {
                "agroecossistema": {
                    "id": agroecossistema.id,
                    "nome": agroecossistema.nsga,
                },
                "car": {"id": car.id, "nome": str(car)},
                "totais_economica": totais_economica,
            }
            if evento_gatilho:
                new_data["totais_simulacao"] = totais_simulacao

            data.append(new_data)

            new_data_json = {
                "agroecossistema": {
                    "id": agroecossistema.id,
                    "nome": agroecossistema.nsga,
                },
                "car": {"id": car.id, "nome": str(car)},
                "totais_economica": totais_economica_json,
            }
            if evento_gatilho:
                new_data_json["totais_simulacao"] = totais_simulacao_json

            data_json.append(new_data_json)

        media_analises = {
            k: 0 if not v["sum"] else (v["sum"] / v["total_items"])
            for k, v in media_analises.items()
        }
        media_analises_json = LJSONRenderer().render(media_analises).decode("utf-8")

        if evento_gatilho:
            media_simulacoes = {
                k: 0 if not v["sum"] else (v["sum"] / v["total_items"])
                for k, v in media_simulacoes.items()
            }
            media_simulacoes_json = (
                LJSONRenderer().render(media_simulacoes).decode("utf-8")
            )

        ret = {
            "agroecossistemas": data,
            "agroecossistemas_json": data_json,
            "totais_medias": {
                "media_analises": media_analises,
                "media_analises_json": media_analises_json,
            },
        }
        if evento_gatilho:
            ret["totais_medias"]["media_simulacoes"] = media_simulacoes
            ret["totais_medias"]["media_simulacoes_json"] = media_simulacoes_json

        return ret

    def get_analise_qualitativa(self, agroecossistema_analise_qualitativa):
        agroecossistema = agroecossistema_analise_qualitativa[0]
        analise_qualitativa = agroecossistema_analise_qualitativa[1]

        if analise_qualitativa == "-" or analise_qualitativa == 0:
            return 0

        if analise_qualitativa:
            return analise_qualitativa

        obj = self.get_analiseagregada()

        """
        Seleção padrão dos itens segundo metodologia Lume

        Primeiro critério - Segundo o evento-gatilho selecionado
        (se não foi selecionado evento gatilho)


        Segunda: Analise qualitativa do ano atual mais próxima do que foi selecionada na primeira página.
                 Se tiver mais de um, o critério de desempate é o ano (referência) mais antigo
        """

        analises_qualitativas = Analise.objects.filter(
            agroecossistema_id=agroecossistema
        ).order_by("ano_referencia")

        # Criterio 1 - evento gatilho
        if obj.evento_gatilho:
            criterio_evento_gatilho = analises_qualitativas.filter(
                evento_gatilho=obj.evento_gatilho, evento_gatilho__isnull=False
            )

            if criterio_evento_gatilho:
                return criterio_evento_gatilho[0].id

        # Criterio 2 - ano
        criterio_ano_atual = 9999
        analise_qualitativa = 0
        for item in analises_qualitativas:
            analise_ano = item.ano
            obj_ano = int(obj.ano)
            diff = analise_ano - obj_ano
            diff = diff * (-1) if diff < 0 else diff

            if diff < criterio_ano_atual:
                criterio_ano_atual = diff
                analise_qualitativa = item.id

        return analise_qualitativa

    def get_analise_qualitativa_ids(self, only_visible=False):
        analiseagregada = self.get_analiseagregada()
        json_criterios_selecao = analiseagregada.get_json_criterios_selecao()
        agroecossistema_ids = json_criterios_selecao.get("agroecossistema_ids", [])
        if only_visible:
            analise_qualitativa_ids = [
                self.get_analise_qualitativa(i)
                for i in agroecossistema_ids
                if len(i) < 4 or i[3] == 1
            ]
        else:
            analise_qualitativa_ids = [
                self.get_analise_qualitativa(i) for i in agroecossistema_ids
            ]

        return analise_qualitativa_ids

    def get_analiseagregada(self):
        if not self.analiseagregada:
            self.analiseagregada = self.get_object()

        return self.analiseagregada

    def get_agroecossistemas_ids(self, only_visible=False):
        if only_visible and self.lista_agroecossistemas_only_visible:
            return self.lista_agroecossistemas_only_visible

        if not only_visible and self.lista_agroecossistemas:
            return self.lista_agroecossistemas

        analiseagregada = self.get_analiseagregada()
        json_criterios_selecao = analiseagregada.get_json_criterios_selecao()

        if not json_criterios_selecao:
            return ""

        agroecossistema_ids = json_criterios_selecao["agroecossistema_ids"]
        if only_visible:
            agroecossistema_ids = [
                i[0] for i in agroecossistema_ids if len(i) < 4 or i[3] == 1
            ]
        else:
            agroecossistema_ids = [i[0] for i in agroecossistema_ids]

        if only_visible:
            self.lista_agroecossistemas_only_visible = agroecossistema_ids
        else:
            self.lista_agroecossistemas = agroecossistema_ids

        return agroecossistema_ids

    def get_agroecossistemas_ar(self):
        analiseagregada = self.get_analiseagregada()
        json_criterios_selecao = analiseagregada.get_json_criterios_selecao()
        if not json_criterios_selecao:
            return []
        return json_criterios_selecao["agroecossistema_ids"]


# TODO class AnaliseagregadaDetailView(PermissionRequiredMixin, DetailView):
class AnaliseagregadaDetailView(
    PermissionRequiredMixin, DetailView, AnaliseagregadaMixin
):
    model = Analiseagregada
    permission_required = "analiseagregada.visualizar"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        analise_economica = self.get_data_analise_agroecossistemas(only_visible=True)
        analiseagregada = self.get_analiseagregada()
        context["analiseagregada"] = analiseagregada
        context["media_analises"] = analise_economica["totais_medias"]["media_analises"]
        context["agroecossitemas_count"] = len(
            self.get_agroecossistemas_ids(only_visible=True)
        )
        context["modulo"] = "analiseagregada"
        context["submodulo"] = "inicio"

        return context


class AnaliseagregadaEconomicaDetailView(DetailView, AnaliseagregadaMixin):
    template_name = "agroecossistema/analise_comparacao.html"
    model = Analiseagregada

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        analiseagregada = self.get_analiseagregada()

        context["modulo"] = "analiseagregada"
        context["submodulo"] = "economica"
        context["base"] = "analiseagregada/base.html"

        dados_consolidados = self.get_data_analise_agroecossistemas(only_visible=True)
        context["agroecossistemas_json"] = dados_consolidados["agroecossistemas_json"]

        context["titulo_geral"] = _("Análise Econômica Agregada")
        subtitulo_agregados_json = (
            LJSONRenderer()
            .render(
                [
                    _("Análise Agregada %(nome)s") % {"nome": analiseagregada.nome},
                    _("Ciclo Anual de Referência %(ano)s ")
                    % {"ano": str(analiseagregada.ano)},
                ]
            )
            .decode("utf-8")
        )
        if analiseagregada.evento_gatilho:
            context["totais1"] = dados_consolidados["totais_medias"]["media_simulacoes"]
            context["totais1_json"] = dados_consolidados["totais_medias"][
                "media_simulacoes_json"
            ]
            context["totais2"] = dados_consolidados["totais_medias"]["media_analises"]
            context["totais2_json"] = dados_consolidados["totais_medias"][
                "media_analises_json"
            ]
            context["titulos"] = {
                "titulo": analiseagregada.nome,
                "subtitulo1": _("Simulação com evento gatilho ")
                + analiseagregada.evento_gatilho.__str__(),
                "subtitulo2": _("Ciclo real"),
                "subtitulo_agregados": subtitulo_agregados_json,
            }
            context["descricao_geral"] = _(
                "Universo Analisado: %(qtd)s agroecossistemas (com simulações)"
            ) % {"qtd": str(len(dados_consolidados["agroecossistemas"]))}
        else:
            context["totais"] = dados_consolidados["totais_medias"]["media_analises"]
            context["totais_json"] = dados_consolidados["totais_medias"][
                "media_analises_json"
            ]
            context["titulos"] = {
                "titulo": analiseagregada.nome,
                "subtitulo": _("Ciclo Anual de Referência: ")
                + str(analiseagregada.ano),
                "subtitulo_agregados": subtitulo_agregados_json,
            }
            context["descricao_geral"] = _(
                "Universo Analisado: %(qtd)s agroecossistemas (sem simulações)"
            ) % {"qtd": str(len(dados_consolidados["agroecossistemas"]))}

        context["RELATORIOS"] = TotaisAnaliseEconomica.RELATORIOS
        context["RELATORIOS_AGREGADOS"] = analiseagregada.RELATORIOS_AGREGADOS
        context["COMPARACOES"] = TotaisAnaliseEconomica.COMPARACOES

        context["grafico_composicao_defs"] = self.grafico_composicao_defs

        context["arquivo_pre"] = slugify(analiseagregada.nome)

        return context

    @cached_property
    def grafico_composicao_defs(self):
        return list(
            filter(
                lambda x: x["id"] == "composicao_rendas",
                TotaisAnaliseEconomica.RELATORIOS["graficosRenda"]["itens"],
            )
        )[0]

    @cached_property
    def totais1(self):
        try:
            return self.analise1.totais
        except Analise.totais.RelatedObjectDoesNotExist:
            return None

    @cached_property
    def totais1_json(self):
        r = (
            LJSONRenderer()
            .render(TotaisAnaliseEconomicaSerializer(self.totais1).data)
            .decode("utf-8")
        )
        return r

    @cached_property
    def totais2(self):
        try:
            return self.analise2.totais
        except Analise.totais.RelatedObjectDoesNotExist:
            return None

    @cached_property
    def totais2_json(self):
        r = (
            LJSONRenderer()
            .render(TotaisAnaliseEconomicaSerializer(self.totais2).data)
            .decode("utf-8")
        )
        return r


class AnaliseagregadaQualitativaDetailView(DetailView, AnaliseagregadaMixin):
    template_name = "analiseagregada/analiseagregada_qualitativa_detail.html"
    model = Analiseagregada

    def contabiliza_avaliacao(self, param, param_data, analise):
        attr_id = param.atributo_id
        data = param_data

        if attr_id not in data:
            attr = param.atributo
            data[attr_id] = {"atributo": attr, "parametros_list": {}}

        if param.id not in data[attr_id]["parametros_list"]:
            data[attr_id]["parametros_list"][param.id] = {
                "parametro": param,
                "media_ano_ref": 0,
                "media_ano_atual": 0,
                "media_count": 0,
            }

        # obter qualificacao ano ref
        try:
            ano_ref = param.qualificacao_set.get(
                agroecossistema_id=analise.agroecossistema_id,
                ano=analise.ano_referencia,
            )
        except:
            return param_data

        ano_ref = (ano_ref.qualificacao - 1) / 4

        data[attr_id]["parametros_list"][param.id]["media_ano_ref"] += ano_ref

        # obter qualificacao ano atual
        try:
            ano_atual = param.qualificacao_set.get(
                agroecossistema_id=analise.agroecossistema_id, ano=analise.ano
            )
        except:
            return param_data

        ano_atual = (ano_atual.qualificacao - 1) / 4

        data[attr_id]["parametros_list"][param.id]["media_ano_atual"] += ano_atual

        # contagem de itens para formar a media
        data[attr_id]["parametros_list"][param.id]["media_count"] += 1

        return data

    def calcular_medias_qualitativas(self, data):

        for attr_id in data:
            labels = []
            data_atual = []
            data_ref = []
            total_ref = 0
            total_atual = 0
            n = 0
            for param_id in data[attr_id]["parametros_list"]:
                media_ano_ref = data[attr_id]["parametros_list"][param_id][
                    "media_ano_ref"
                ]
                media_ano_atual = data[attr_id]["parametros_list"][param_id][
                    "media_ano_atual"
                ]
                media_count = data[attr_id]["parametros_list"][param_id]["media_count"]

                if media_ano_ref and media_ano_atual:
                    data[attr_id]["parametros_list"][param_id]["media_ano_ref"] = (
                        media_ano_ref / media_count
                    )
                    data[attr_id]["parametros_list"][param_id]["media_ano_atual"] = (
                        media_ano_atual / media_count
                    )
                    labels.append(
                        data[attr_id]["parametros_list"][param_id]["parametro"].nome
                    )
                    data_ref.append(
                        data[attr_id]["parametros_list"][param_id]["media_ano_ref"]
                    )
                    data_atual.append(
                        data[attr_id]["parametros_list"][param_id]["media_ano_atual"]
                    )
                    n = n + 1
                    total_ref = (
                        total_ref
                        + data[attr_id]["parametros_list"][param_id]["media_ano_ref"]
                    )
                    total_atual = (
                        total_atual
                        + data[attr_id]["parametros_list"][param_id]["media_ano_atual"]
                    )

            data[attr_id]["data_json"] = {
                "labels": LJSONRenderer().render(labels).decode("utf-8"),
                "data_ref": LJSONRenderer().render(data_ref).decode("utf-8"),
                "data_atual": LJSONRenderer().render(data_atual).decode("utf-8"),
            }
            data[attr_id]["indice"] = {
                "indice_ref": 0 if not total_ref else total_ref / n,
                "indice_atual": 0 if not total_atual else total_atual / n,
            }
        return data

    def get_data_qualitativa_medias(self):

        analises_ids = self.get_analise_qualitativa_ids(only_visible=True)
        analises = Analise.objects.filter(id__in=analises_ids).prefetch_related(
            "agroecossistema", "avaliacoes"
        )

        data = {}

        # obtem analises
        for analise in analises:

            agroecossistema = analise.agroecossistema

            # parametros inativos
            try:
                param_inativos = agroecossistema.parametros_inativos["inativos"]
            except:
                param_inativos = []

            avaliacoes = (
                analise.avaliacoes.exclude(parametro_id__in=param_inativos)
                .prefetch_related("parametro")
                .order_by("parametro__ordem")
            )

            # obtem as avaliações da analise, que
            # contém [atributo][parametro]
            # verificar se atributo tem algum parametro
            for avaliacao in avaliacoes:

                param = avaliacao.parametro

                data = self.contabiliza_avaliacao(param, data, analise)

        # calcular as medias
        data = self.calcular_medias_qualitativas(data)

        sintese_ref = 0
        sintese_atual = 0
        n = 0
        labels = []
        data_ref = []
        data_atual = []
        for attr_id in data:
            n = n + 1
            sintese_ref = sintese_ref + data[attr_id]["indice"]["indice_ref"]
            sintese_atual = sintese_atual + data[attr_id]["indice"]["indice_atual"]
            labels.append(data[attr_id]["atributo"].nome)
            data_ref.append(data[attr_id]["indice"]["indice_ref"])
            data_atual.append(data[attr_id]["indice"]["indice_atual"])

            subsections = {}
            for i, parametro in data[attr_id]["parametros_list"].items():
                atributo_slug = slugify(str(parametro["parametro"].atributo))
                parametro_slug = slugify(str(parametro["parametro"]))

                parametro_i18n = get_qualitativa_helper_i18n(
                    parametro["parametro"], parametro["parametro"].atributo
                )
                atributo_i18n = parametro_i18n["atributo"]
                subsection = parametro_i18n["subsection"]

                if subsection != "" and subsection not in subsections:
                    subsections[subsection] = subsection
                elif (
                    subsection == ""
                    and parametro["parametro"].atributo_id not in subsections
                ):
                    subsections[parametro["parametro"].atributo_id] = atributo_i18n
            data[attr_id]["subsections"] = subsections

        if n > 0:
            sintese_indice = {
                "ref": sintese_ref / n,
                "atual": sintese_atual / n,
            }
        else:
            sintese_indice = {
                "ref": 0,
                "atual": 0,
            }

        sintese = {
            "labels": LJSONRenderer().render(labels).decode("utf-8"),
            "data_ref": LJSONRenderer().render(data_ref).decode("utf-8"),
            "data_atual": LJSONRenderer().render(data_atual).decode("utf-8"),
            "sintese_indice": sintese_indice,
        }

        data = OrderedDict(sorted(data.items()))

        return {"data": data, "sintese": sintese}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        res = self.get_data_qualitativa_medias()
        analiseagregada = self.get_analiseagregada()
        context["analiseagregada"] = analiseagregada
        context["analiseagregada_qualitativa"] = res["data"]
        context["sintese"] = res["sintese"]
        context["modulo"] = "analiseagregada"
        context["submodulo"] = "qualitativa"

        return context


class AnaliseagregadaUniversoView(
    PermissionRequiredMixin, DetailView, AnaliseagregadaMixin
):
    template_name = "analiseagregada/analiseagregada_universo.html"
    model = Analiseagregada
    permission_required = "analiseagregada.alterar"

    def get_agroecossistema_visibilidade(self, agroecossistema_id, analise_id):
        analiseagregada = self.get_analiseagregada()

        agroecossistemas_ar = self.get_agroecossistemas_ar()
        for criterio_selecao in agroecossistemas_ar:
            if agroecossistema_id == criterio_selecao[0]:
                return len(criterio_selecao) < 4 or criterio_selecao[3] == 1

        # regex = re.search(
        #     rf"\[{agroecossistema}\,\ ({analise}|'-')(,\ (\d))?\]",
        #     analiseagregada.json_criterios_selecao,
        # )

        # if not regex:
        #     return 0

        # visibilidade = regex.groups()[2]
        # if visibilidade == None:
        #     visibilidade = 1

        # return int(visibilidade)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        analises_ids = self.get_analise_qualitativa_ids()
        analise_qualitativa = Analise.objects.filter(
            id__in=analises_ids
        ).prefetch_related("agroecossistema")

        analise_economica = self.get_data_analise_agroecossistemas()

        analiseagregada = self.get_analiseagregada()
        agroecossistemas = Agroecossistema.objects.filter(
            id__in=self.get_agroecossistemas_ids()
        )

        data_list = {}

        for agroecossistema in agroecossistemas:
            data_list[agroecossistema.id] = {
                "agroecossistema_id": agroecossistema.id,
                "agroecossistema_nome": agroecossistema.nsga,
                "analise_economica": None,
                "analise_qualitativa_id": None,
                "analise_qualitativa_nome": None,
                "visibilidade": 1,
            }
        for analise in analise_economica["agroecossistemas"]:
            data_list[analise["agroecossistema"]["id"]]["analise_economica"] = analise[
                "totais_economica"
            ]
        for analise in analise_qualitativa:
            data_list[analise.agroecossistema.id]["analise_qualitativa_id"] = analise.id
            data_list[analise.agroecossistema.id][
                "analise_qualitativa_nome"
            ] = analise.__str__()
            data_list[analise.agroecossistema.id]["visibilidade"] = (
                self.get_agroecossistema_visibilidade(
                    analise.agroecossistema.id, analise.id
                )
            )

        context["agroecossistemas"] = data_list
        context["modulo"] = "analiseagregada"
        context["submodulo"] = "universo"

        return context


class AnaliseagregadaFormWizardView(FormView, AnaliseagregadaMixin):
    template_name = "analiseagregada/analiseagregada_form.html"

    form_wizard = {
        "steps": [
            {
                "slug": "dados-gerais",
                "title": _("Dados gerais"),
                "description": _("Dados básicos"),
                "icon": "info",
                "view": "DadosGeraisFormView",
            },
            {
                "slug": "agroecossistemas",
                "title": _("Agroecossistemas"),
                "description": _("Lista dos agroecossistemas selecionados"),
                "icon": "tasks",
                "view": "AgroecossistemasFormView",
            },
            {
                "slug": "analise-qualitativa",
                "title": _("Análise qualitativa"),
                "description": _("Selecione a análise qualitativa:"),
                "icon": "file-invoice",
                "view": "AnaliseQualitativaFormView",
            },
            {
                "slug": "resumo",
                "title": _("Resumo"),
                "description": _("Confirme os itens filtrados para análise"),
                "icon": "clipboard-check",
                "view": "ResumoFormView",
            },
        ],
        "current": 1,
        "title_create": _("Criar análise agregada"),
        "title_edit": _("Configurar análise agregada"),
        "subtitle": "",
    }

    def get_object(self):
        try:
            return self.analise_object
        except:
            analise_id = self.kwargs.get("pk", None)
            if not analise_id:
                return None
            self.analise_object = Analiseagregada.objects.get(id=analise_id)

        return self.analise_object

    def get_previous_url(self, current):
        if current == 1:
            return ""

        slug = self.form_wizard["steps"][current - 2]["slug"]

        return reverse_lazy(
            f"analiseagregada:form_edit", args=[self.kwargs["pk"], slug]
        )

    def set_form_wizard_current(self, current):
        previous = self.get_previous_url(current)

        return {
            "number": current,
            "description": self.form_wizard["steps"][current - 1]["description"],
            "previous": previous,
        }

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["titulo"] = _("Análise agregada")
        context["form_wizard"] = self.form_wizard
        context["form_wizard"]["current"] = self.set_form_wizard_current(1)

        if "pk" in self.kwargs:
            context["modulo"] = "analiseagregada"
            context["submodulo"] = "editar"
            context["analiseagregada"] = self.get_analiseagregada()
            title = self.get_analiseagregada().nome
            title = _("Configurar %(titulo)s") % {"titulo": title}
        else:
            context["modulo"] = "main"
            context["submodulo"] = "create"
            title = _("Criar análise agregada")

        context["form_wizard"]["title"] = title

        return context


class AnaliseagregadaFormView(AnaliseagregadaFormWizardView):

    def dispatch(self, request, *args, **kwargs):

        slug = kwargs.get("slug_step", "")
        step = [i for i in self.form_wizard["steps"] if i["slug"] == slug]
        if not step:
            step = [self.form_wizard["steps"][0]]

        view = eval(step[0]["view"])

        return view.as_view()(request, *args, **kwargs)


class DadosGeraisFormView(
    PermissionRequiredMixin, FormSetViewMixin, AnaliseagregadaFormWizardView
):

    permission_required = "analiseagregada.alterar"
    template_name = "analiseagregada/analiseagregada_form_dados_gerais.html"
    model = Analiseagregada
    form_class = DadosGeraisForm
    formsets = [
        (
            indicador_economico_form_set,
            {"title": _("Indicador econômico"), "is_modal": False},
        ),
        (
            indicador_qualitativo_form_set,
            {"title": _("Indicador qualitativo"), "is_modal": False},
        ),
    ]

    def get_permission_object(self):
        data = self.kwargs
        if not data:
            return Analiseagregada.objects.none()
        return Analiseagregada.objects.get(pk=data.get("pk"))

    def get(self, request, *args, **kwargs):
        return self.render_to_response(self.get_context_data())

    def get_context_data(self, **kwargs):
        aa = Analiseagregada.objects.get(pk=self.kwargs.get("pk"))
        context = super().get_context_data(**kwargs)
        context["form_wizard"]["current"] = self.set_form_wizard_current(1)
        return context

    def get_initial(self):
        initial = super().get_initial()

        if not self.kwargs:
            return initial

        obj = Analiseagregada.objects.get(**{"pk": self.kwargs["pk"]})
        initial["organizacao"] = obj.organizacao
        initial["nome"] = obj.nome
        initial["descricao"] = obj.descricao
        initial["ano"] = obj.ano
        if obj.evento_gatilho:
            initial["evento_gatilho"] = obj.evento_gatilho.id

        return initial

    def form_valid(self, form):
        data = form.cleaned_data

        if self.kwargs:
            obj = Analiseagregada.objects.get(**{"pk": self.kwargs["pk"]})
        else:
            obj = Analiseagregada()
            obj.autor = self.request.user

        # criterios_selecao = obj.get_json_criterios_selecao()

        obj.organizacao = data.get("organizacao")
        obj.nome = data.get("nome")
        obj.descricao = data.get("descricao")
        obj.ano = data.get("ano")
        obj.evento_gatilho = data.get("evento_gatilho")
        obj.json_criterios_selecao = ""
        obj.save()

        form.instance = obj
        self.success_url = reverse(
            "analiseagregada:form_edit", args=[obj.id, "agroecossistemas"]
        )

        return super().form_valid(form)


class AgroecossistemasFormView(PermissionRequiredMixin, AnaliseagregadaFormWizardView):
    permission_required = "analiseagregada.alterar"
    form_class = AgroecossistemasForm
    template_name = "analiseagregada/analiseagregada_form_selecionar.html"
    agroecossistema_ids = []

    def check_indicadores_economicos(self, car, indicadores_economicos):
        if not indicadores_economicos:
            return True

        totais_economica = self.get_analise_economica(car)
        if totais_economica == None:
            # TODO verificar o que fazer neste caso
            return False

        # filtrar pelos indicadores economicos
        for i in indicadores_economicos:
            # obter valor do indicador relacionado ao agroecossistema
            attrname = f"total_{i.indicador}"
            try:
                indicador_valor = totais_economica.__getattribute__(attrname)
            except:
                indicador_valor = None

            if indicador_valor != None:
                is_valid = eval(f"{indicador_valor} {i.operador} {i.valor}")

                if not is_valid:
                    return False

        return True

    def filter_by_indicadores_economicos(self, agroecossistemas):

        obj = self.get_analiseagregada()

        agroecossistema_ids = []

        # indicadores selecionados
        indicadores_economicos = obj.indicadores_economicos.all()

        # obter ciclo anual ref.
        ciclo_anual_ref = CicloAnualReferencia.objects.filter(
            agroecossistema__in=[a for a in agroecossistemas],
            ano=obj.ano,
            sim_slug__isnull=True,
        )

        # listar agroecossistemas
        for agroecossistema in agroecossistemas:
            car = ciclo_anual_ref.filter(agroecossistema_id=agroecossistema.id).first()
            check_ieconomicos = self.check_indicadores_economicos(
                car, indicadores_economicos
            )

            if check_ieconomicos == True:
                agroecossistema_ids.append(
                    [agroecossistema.id, 0, car.analise_economica.id]
                )

        agroecossistemas = agroecossistemas.filter(
            id__in=[a[0] for a in agroecossistema_ids]
        )

        self.agroecossistema_ids = agroecossistema_ids

        return agroecossistemas

    def filter_agroecossistemas(self):

        obj = self.get_analiseagregada()

        # Critério 1
        object_list = Agroecossistema.objects.filter(organizacao=obj.organizacao)

        # Critério 2
        """
        A escolha de um evento-gatilho implicará na exclusão dos agroecossistemas
        que não tenham uma simulação com este evento gatilho no ciclo escolhido.
        """
        if obj.evento_gatilho:
            object_list = object_list.filter(
                cars__ano=obj.ano, cars__sim_evento_gatilho=obj.evento_gatilho
            )

        if self.request.user.has_perm("organizacao.ver_todos"):
            pass
        else:
            object_list = object_list.filter(organizacao__usuarias=self.request.user)

        # filtrar por indicadores economicos
        object_list = self.filter_by_indicadores_economicos(object_list)

        return object_list

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        object_list = self.filter_agroecossistemas()

        context["form_wizard"]["current"] = self.set_form_wizard_current(2)
        context["agroecossistemas_selected"] = [a[0] for a in self.agroecossistema_ids]
        context["agroecossistema_ids"] = self.agroecossistema_ids

        context["object"] = self.get_analiseagregada()

        context["object_list"] = object_list

        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()

        if not context["object_list"]:
            context["object"].delete()
            messages.error(
                request,
                _("Não foi encontrado nenhum agroecossistema dentro desses critérios"),
            )
            return redirect("analiseagregada:form")

        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        context = self.get_context_data()
        selected_agroecossistema_ids = request.POST.getlist("agroecossistemas")
        selected_agroecossistema_ids = selected_agroecossistema_ids[0].split(",")

        analiseagregada_id = self.kwargs.get("pk", None)
        obj = Analiseagregada.objects.get(id=analiseagregada_id)

        criterios_selecao = []

        for agroecossistema_id in selected_agroecossistema_ids:
            for criterio_selecao in self.agroecossistema_ids:
                if int(agroecossistema_id) == criterio_selecao[0]:
                    criterios_selecao.append(criterio_selecao)
                    break

        obj.json_criterios_selecao = str(
            {
                "agroecossistema_ids": criterios_selecao,
            }
        )
        obj.save()

        return redirect(
            "analiseagregada:form_edit", pk=obj.id, slug_step="analise-qualitativa"
        )


class AnaliseQualitativaFormView(
    PermissionRequiredMixin, AnaliseagregadaFormWizardView
):
    permission_required = "analiseagregada.alterar"
    form_class = AnaliseQualitativaForm
    template_name = "analiseagregada/analiseagregada_form_sel_analise_qualitativa.html"

    def check_indicadores_qualitativos(
        self, analise, indicadores_qualitativos, ciclo_anual_ref
    ):

        if not indicadores_qualitativos:
            return True, ""

        for i in indicadores_qualitativos:

            parametro_qualificacao = i.parametro.qualificacao_set.filter(
                agroecossistema_id=analise.agroecossistema_id
            )

            # obter qualificacao ano ciclo
            try:
                analise_ano = analise.__getattribute__(i.ano_ciclo)
                parametro_ano_ciclo = parametro_qualificacao.get(ano=analise_ano)
            except:
                # TODO verificar o que fazer neste caso
                continue

            qualificacao = parametro_ano_ciclo.qualificacao

            is_valid = eval(f"{qualificacao} {i.operador} {i.valor}")

            if not is_valid:
                justificativa = _(
                    "%(atributo)s / %(param)s no %(ano_ciclo)s %(ano)s tem qualificação %(qa)s e deve ser %(operador)s %(valor)s"
                    % {
                        "atributo": i.atributo,
                        "param": i.parametro,
                        "ano_ciclo": i.ano_ciclo,
                        "ano": analise_ano,
                        "qa": qualificacao,
                        "operador": i.operador,
                        "valor": i.valor,
                    }
                )

                return False, justificativa

        return True, ""

    def get_analise_by_agroecossistema(self, object_list):

        agroecossistemas_selected = self.get_agroecossistemas_ids()
        if agroecossistemas_selected:
            object_list = object_list.filter(
                agroecossistema__in=agroecossistemas_selected
            )

        obj = self.get_analiseagregada()

        # indicadores selecionados
        indicadores_qualitativos = obj.indicadores_qualitativos.all()

        # obter ciclo anual ref.
        agroecossistemas = [
            a[0] for a in object_list.distinct().values_list("agroecossistema")
        ]
        ciclo_anual_ref = CicloAnualReferencia.objects.filter(
            agroecossistema__in=agroecossistemas, ano=obj.ano, sim_slug__isnull=True
        )

        object_list = object_list.order_by("agroecossistema", "-criacao")

        analises = {}
        for i in object_list:

            is_valid, justificativa = self.check_indicadores_qualitativos(
                i, indicadores_qualitativos, ciclo_anual_ref
            )

            i.is_valid = is_valid
            i.justificativa = justificativa

            key = i.agroecossistema.__str__()
            if key in analises:
                analises[key].append(i)
            else:
                analises[key] = [i]

        return analises

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["form_wizard"]["current"] = self.set_form_wizard_current(3)
        context["analise_qualitativa_ids"] = self.get_analise_qualitativa_ids()

        obj = self.get_analiseagregada()
        context["object_list"] = Analise.objects.filter(
            agroecossistema__organizacao=obj.organizacao
        )

        if self.request.user.has_perm("organizacao.ver_todos"):
            pass
        else:
            context["object_list"] = context["object_list"].filter(
                agroecossistema__organizacao__usuarias=self.request.user
            )

        context["object_list"] = self.get_analise_by_agroecossistema(
            context["object_list"]
        )

        return context

    def post(self, request, *args, **kwargs):
        analise_qualitativa_ids = request.POST.getlist("analise_qualitativa_ids")
        analise_qualitativa_ids = eval(f"[{analise_qualitativa_ids[0]}]")

        # TODO if analise_qualitativa_ids: ...

        # obj.organizacao = TODO
        analiseagregada_id = self.kwargs.get("pk", None)
        obj = Analiseagregada.objects.get(id=analiseagregada_id)
        criterios_selecao = obj.get_json_criterios_selecao()
        new_criterios_selecao = []
        for agroecossistema_ar in criterios_selecao["agroecossistema_ids"]:
            new_criterio_selecao = agroecossistema_ar
            for analise_qualitativa_ar in analise_qualitativa_ids:
                if agroecossistema_ar[0] == analise_qualitativa_ar[0]:
                    new_criterio_selecao[1] = analise_qualitativa_ar[1]
                    break
            new_criterios_selecao.append(new_criterio_selecao)

        obj.json_criterios_selecao = str({"agroecossistema_ids": new_criterios_selecao})
        obj.save()

        return redirect("analiseagregada:form_edit", pk=obj.id, slug_step="resumo")


class ResumoFormView(PermissionRequiredMixin, AnaliseagregadaFormWizardView):
    permission_required = "analiseagregada.alterar"
    form_class = ResumoForm
    template_name = "analiseagregada/analiseagregada_form_resumo.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        agroecossistema_ar = self.get_agroecossistemas_ar()
        agroecossistemas = Agroecossistema.objects.filter(
            id__in=self.get_agroecossistemas_ids()
        )
        analises_qualitativas = Analise.objects.filter(
            id__in=self.get_analise_qualitativa_ids()
        )
        agroecossistemas_selected = []
        for agroecossistema in agroecossistemas:
            for criterio_selecao in agroecossistema_ar:
                agroecossistema_id = criterio_selecao[0]
                if agroecossistema.id == agroecossistema_id:
                    analise_qualitativa_id = criterio_selecao[1]
                    analise_economica_id = criterio_selecao[2]
                    agroecossistemas_selected.append(
                        {
                            "agroecossistema": agroecossistema,
                            "analise_qualitativa": (
                                None
                                if analise_qualitativa_id == 0
                                else analises_qualitativas.get(
                                    id=analise_qualitativa_id
                                )
                            ),
                            "analise_economica_id": analise_economica_id,
                        }
                    )

        context["form_wizard"]["current"] = self.set_form_wizard_current(4)
        context["agroecossistemas_selected"] = agroecossistemas_selected

        # if self.request.user.has_perm("organizacao.ver_todos"):
        #     context["object_list"] = Agroecossistema.objects.all()
        # else:
        #     context["object_list"] = Agroecossistema.objects.filter(
        #         organizacao__usuarias=self.request.user
        #     )

        # get object
        context["object"] = self.get_analiseagregada()
        context["object"].ano = str(context["object"].ano)

        return context

    def post(self, request, *args, **kwargs):
        nome_analiseagregada = request.POST.get("nome_analiseagregada", "")
        descricao_analiseagregada = request.POST.get("descricao_analiseagregada", "")

        analiseagregada_id = self.kwargs.get("pk", None)
        obj = Analiseagregada.objects.get(id=analiseagregada_id)
        if nome_analiseagregada:
            obj.nome = nome_analiseagregada
        if descricao_analiseagregada:
            obj.descricao = descricao_analiseagregada
        obj.save()

        return redirect("analiseagregada:detail", pk=obj.id)


class AnaliseagregadaDeleteView(PermissionRequiredMixin, DeleteView):
    model = Analiseagregada
    success_url = reverse_lazy("analiseagregada:list")
    permission_required = "analiseagregada.apagar"

    def get(self, *args, **kwargs):
        return self.post(*args, **kwargs)


class ToggleAgroecossistemaView(PermissionRequiredMixin, FormView):
    permission_required = "agroecossistema.alterar"

    def get_permission_object(self):
        data = self.kwargs
        return Agroecossistema.objects.get(pk=data.get("agroecossistema"))

    def get(self, request, *args, **kwargs):
        analise_id = kwargs["pk"]
        analise_object = Analiseagregada.objects.get(id=analise_id)
        agroecossistema_id = kwargs["agroecossistema"]

        criterios_selecao = analise_object.get_json_criterios_selecao()

        for i, criterio_selecao in enumerate(criterios_selecao["agroecossistema_ids"]):
            if agroecossistema_id == criterio_selecao[0]:
                if len(criterio_selecao) < 4:
                    criterios_selecao["agroecossistema_ids"][i].append(0)
                elif criterio_selecao[3] == 1:
                    criterios_selecao["agroecossistema_ids"][i][3] = 0
                else:
                    criterios_selecao["agroecossistema_ids"][i][3] = 1
                analise_object.json_criterios_selecao = criterios_selecao
                analise_object.save()
                visibility = criterios_selecao["agroecossistema_ids"][i][3]
                return JsonResponse({"visibility": visibility})

        return JsonResponse({"error": "Couldn't find the agroecossistema"})

        # visibility = 0
        # regex = rf"(\[{agroecossistema_id}\,\ (\d+|'-'))"
        # json_updated, set_unvisible = re.compile(rf"{regex}(\,\ 1)?]", re.VERBOSE).subn(
        #     r"\1, 0]", data_json
        # )
        # if not set_unvisible:
        #     visibility = 1
        #     json_updated = re.compile(rf"{regex}\,\ 0]", re.VERBOSE).sub(
        #         r"\1, 1]", data_json
        #     )

        # analise_object.json_criterios_selecao = json_updated
        # analise_object.save()

        return JsonResponse({"visibility": visibility})


class ParametrosByAtributo(FormView):

    def post(self, request, *args, **kwargs):
        data = request.POST.dict()
        parametros = Parametro.objects.filter(**data).values_list("id", "nome")

        return JsonResponse({"results": list(parametros)})
