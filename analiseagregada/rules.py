from __future__ import absolute_import

import rules

from agroecossistema.models import Agroecossistema
from main.models import OrganizacaoUsuaria


@rules.predicate
def eh_superusuario(usuaria):
    return usuaria.is_superuser


eh_admin_geral = rules.is_group_member('admin')
eh_agregado_organizacoes = rules.is_group_member('agregado-de-organizacoes')


@rules.predicate
def eh_anonimo(usuaria):
    return usuaria.is_anonymous


@rules.predicate
def eh_admin_da_organizacao(usuaria, organizacao):
    try:
        ou = OrganizacaoUsuaria.objects.get(usuaria_id=usuaria.id, organizacao_id=organizacao.id, is_admin=True)
        return True
    except OrganizacaoUsuaria.DoesNotExist:
        return False


@rules.predicate
def eh_integrante_de_alguma_organizacao(usuaria):
    ous = OrganizacaoUsuaria.objects.filter(usuaria_id=usuaria.id)
    return ous.count() > 0


@rules.predicate
def eh_admin_da_organizacao_ou_autor_do_objeto(usuaria, objeto):

    # se não tiver objeto setado é inserção
    # então verifica se usuaria é admin de alguma organização
    if not objeto:
        try:
            # if usuaria.org_usuarias.filter(is_admin=True).count():
            # TODO verificar qual criterio para user inserir analise
            if usuaria.org_usuarias.all().count():
                return True
            else:
                return False
        except:
            return False

    if not objeto.organizacao:
        return False

    if eh_admin_da_organizacao(usuaria,
                               objeto.organizacao):
        return True

    try:
        if objeto.autor_id == usuaria.id:
            return True
    except:
        return False

def eh_publico(usuaria):
    return True

'''
rules.add_perm('analiseagregada.visualizar',
               eh_superusuario | eh_admin_geral |
               eh_integrante_de_alguma_organizacao |
               eh_agregado_organizacoes)
'''
rules.add_perm('analiseagregada.visualizar', eh_publico)
rules.add_perm('analiseagregada.alterar', eh_superusuario | eh_admin_geral | eh_admin_da_organizacao_ou_autor_do_objeto)
rules.add_perm('analiseagregada.apagar', eh_superusuario | eh_admin_geral | eh_admin_da_organizacao_ou_autor_do_objeto)

