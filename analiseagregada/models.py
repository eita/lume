import json
import datetime

from django.conf import settings
from django.contrib.gis.db import models
from django.db.models.fields.related import ForeignKey
from django.urls import reverse

from django.utils.translation import gettext as _
from django.utils.translation import gettext_lazy as _lazy
from main.utils import get_name_initials

from lume.models import LumeModel
from main.models import Organizacao
from linhadotempo.models import EventoGatilho
from qualitativa.models import Atributo, Parametro
from economica.models import Analise


class Analiseagregada(LumeModel):

    autor = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        null=True,
        blank=False,
        verbose_name=_lazy('autor(a)'),
        related_name="analises_agregadas"
    )
    organizacao = models.ForeignKey(
        Organizacao, models.PROTECT,
        verbose_name=_lazy("Organização"),
        related_name="analiseagregada",
        null=True,
        blank=True
    )
    nome = models.CharField(_lazy('Nome da análise agregada'), max_length=80, null=True, blank=False)
    descricao = models.TextField(_lazy('Descrição'), null=True, blank=True)
    ano = models.SmallIntegerField(
        _lazy('Ano de início do ciclo'),
        null=False,
        blank=False,
        default=(datetime.datetime.now() - datetime.timedelta(days=365.25)).year
    )
    evento_gatilho = models.ForeignKey(
        EventoGatilho,
        models.PROTECT,
        verbose_name=_lazy('evento gatilho'),
        related_name='analiseagregada',
        null=True,
        blank=True,
        help_text=_lazy("A escolha de um evento-gatilho implicará na\
        exclusão dos agroecossistemas que não tenham uma simulação\
        com este evento gatilho no ciclo definido acima.")
    )
    json_criterios_selecao = models.TextField(null=True, blank=True)
    is_analise_modelo = models.BooleanField(
        _lazy('É análise modelo'),
        default=False,
        help_text=_lazy('Caso marcado a análise será pública.')
    )

    def __str__(self):
        return f"{self.nome}"

    def get_initials(self):
        return get_name_initials(self.nome)

    def get_absolute_url(self):
        return reverse('analiseagregada:detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('analiseagregada:form_edit', args=(self.pk, 'dados-gerais'))

    def get_delete_url(self):
        return reverse('analiseagregada:delete', args=(self.pk,))

    def get_json_criterios_selecao(self):
        try:
            json_criterios_selecao = json.loads(
                self.json_criterios_selecao.replace("'", '"'))
        except:
            json_criterios_selecao = {}


        return json_criterios_selecao

    RELATORIOS_AGREGADOS = [
        {
            'titulo': _('Autonomia X Intensidade (1): Índice de Endogeneidade (VA/RB) X Produtividade do Trabalho 1 (VA/HT)'),
            'id': 'agregada_autonomia_intensidade',
            'vars': [],
            'sem_evento_gatilho': True
        },
        {
            'titulo': _('Autonomia X Intensidade (2): Índice de Remuneração (RA/RB) X Produtividade do Trabalho 2 (RA/HT)'),
            'id': 'agregada_autonomia_RA_intensidade',
            'vars': [],
            'sem_evento_gatilho': True
        },
        {
            'titulo': _('Rentabilidade Monetária x Índice de Mercantilização'),
            'id': 'agregada_mercantilizacao_e_rentabilidade',
            'vars': [],
            'sem_evento_gatilho': True
        },
        {
            'titulo': _('Produtividade da terra (1): Riqueza Produzida por Área (VA/ha) X Área (ha)'),
            'id': 'agregada_produtividade_terra_va',
            'vars': [],
            'sem_evento_gatilho': True
        },
        {
            'titulo': _('Produtividade da terra (2): Remuneração por Área (RA/ha) X Área (ha)'),
            'id': 'agregada_produtividade_terra_ra',
            'vars': [],
            'sem_evento_gatilho': True
        },
        {
            'titulo': _('Renda Agrícola (R$) X Renda Não Agrícola (R$)'),
            'id': 'agregada_ra_rna',
            'vars': [],
            'sem_evento_gatilho': True
        },
        {
            'titulo': _('Repartição Proporcional entre Rendas Não Agrícolas e Rendas Agrícolas (%)'),
            'id': 'agregada_compara_rendas_RNAxRA',
            'vars': [],
            'sem_evento_gatilho': True
        },
        {
            'titulo': _('Repartição Proporcional entre Rendas de Pluriatividade e Rendas Agrícolas (%)'),
            'id': 'agregada_compara_rendas_RPlurixRA',
            'vars': [],
            'sem_evento_gatilho': True
        },
        {
            'titulo': _('Repartição Proporcional entre Rendas Agrícolas Monetárias e Rendas Agrícolas Não Monetárias (%)'),
            'id': 'agregada_compara_rendas_RAMxRANM',
            'vars': [],
            'sem_evento_gatilho': True
        },
        {
            'titulo': _('Repartição Proporcional das Rendas: (1) Total; (2) do Trabalho; (3) Agrícola'),
            'id': 'agregada_compara_rendas',
            'vars': [],
            'sem_evento_gatilho': True
        }
    ]


class AnaliseagregadaIndicadorEconomico(LumeModel):

    CHOICE_OPERADORES_ECONOMICO = [(i, i) for i in ['>', '>=', '<', '<=']]

    INDICADORES = [
        (i['def'], i['label'] if i['label'] else i['def'])
        for i in Analise.INDICADORES]

    analiseagregada = models.ForeignKey(
        Analiseagregada, models.CASCADE, verbose_name=_lazy('análise agregada'),
        related_name="indicadores_economicos", null=True, blank=True)
    indicador = models.CharField(max_length=500, choices=INDICADORES, verbose_name=_lazy('indicador'))
    operador = models.CharField(max_length=2, choices=CHOICE_OPERADORES_ECONOMICO, verbose_name=_lazy('operador'))
    valor = models.DecimalField(max_digits=5, decimal_places=2, default=0, verbose_name=_lazy('valor'))


class AnaliseagregadaIndicadorQualitativo(LumeModel):

    CHOICE_OPERADORES_QUALITATIVO = [(i, i) for i in ['>', '>=', '<', '<=', '=']]
    CHOICE_ATUAL = 'ano'
    CHOICE_REFERENCIA = 'ano_referencia'
    CHOICE_ANO_CICLO = [(CHOICE_ATUAL, _('Atual')),
                        (CHOICE_REFERENCIA, _('Referência'))]

    analiseagregada = models.ForeignKey(
        Analiseagregada, models.CASCADE, verbose_name=_lazy('Análise agregada'),
        related_name="indicadores_qualitativos", null=True, blank=True)
    ano_ciclo = models.CharField(_lazy('ano'), max_length=20,
                                 choices=CHOICE_ANO_CICLO)
    atributo = models.ForeignKey(
        Atributo, models.CASCADE, null=True, blank=True, verbose_name=_lazy('atributo'))
    parametro = models.ForeignKey(
        Parametro, models.PROTECT, null=True, blank=True, verbose_name=_lazy('parâmetro'))
    operador = models.CharField(max_length=2, choices=CHOICE_OPERADORES_QUALITATIVO, verbose_name=_lazy('operador'))
    valor = models.DecimalField(max_digits=5, decimal_places=2, default=0, verbose_name=_lazy('valor'))
