var chart = {};

function inicializa_grafico(atributo_id, atributo_nome, labels, data_ref, data_atual) {
            var presets = window.chartColors;
            var chart_data = {
                labels: labels,
                datasets: [{
                    backgroundColor: presets.qualitativaAnoReferencia,
                    borderColor: presets.qualitativaAnoReferenciaLinha,
                    fill: true,
                    data: data_ref,
                    label: 'Ano Referência'
                }, {
                    backgroundColor: presets.qualitativaAnoAtual,
                    borderColor: presets.qualitativaAnoAtualLinha,
                    fill: true,
                    data: data_atual,
                    label: 'Ano Atual'
                }]
            };

            var chart_options = {
                spanGaps: false,
                elements: {
                    line: {
                        tension: 0.000001,
                        fill: 'top'
                    }
                },
                plugins: {
                    title: {
                        display: true,
                        text: atributo_nome,
                        font: {
                            size: 18
                        },
                        padding: {
                            bottom: 30
                        }
                    },
                    legend: {
                        position: 'right',
                        align: 'end'
                    },
                    filler: {
                        propagate: false
                    },
                    'samples-filler-analyser': {
                        target: 'chart-analyser'
                    },
                    afterDraw: function() {
                        if (this.data.datasets.length === 0) {
                          // No data is present
                          this.clear()
                          this.save();
                          this.textAlign = 'center';
                          this.textBaseline = 'middle';
                          this.fillText('No data to display', width / 2, height / 2);
                          this.restore();
                        }
                      }
                },
                scales: {
                    r: {
                        max: 1,
                        min: -0.1,
                        stepSize: 0.1,
                        pointLabels: {
                            font: {
                                size: 14
                            }
                        }
                    }
                },
                animation: {
                    onComplete: function () {
                        var url_base64 = this.toBase64Image();
                        $('#link-' + atributo_id).attr('href', url_base64).attr('download', getSlug(atributo_nome[0] + '.png'));
                    }
                },
            };

            chart["chart-"+atributo_id] = new Chart('aranha-' + atributo_id, {
                type: 'radar',
                data: chart_data,
                options: chart_options
            });
}
