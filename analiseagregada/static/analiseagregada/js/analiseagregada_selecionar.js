$(document).ready(function () {
    var options = dataTableDefaultOptions;
    options.orderCellsTop = true;
    options.fixedHeader = true;
    options.scrollY = '40vh';
    options.scrollCollapse = true;
    options.paging = false;
    options.dom = 'rtip';
    options.columnDefs = [ {
        orderable: false,
        className: 'select-checkbox',
        targets:   0
    } ]
    options.select = {
        style:    'multi',
        selector: 'td:first-child'
    }
    options.order = [[ 1, 'asc' ]]

    var agroecossistemaListTable = $('#agroecossistema_list_table');
    setTimeout(function(){
        agroecossistemaListTable.DataTable(options);
    }, 5);
    var filter_array = [gettext('NSGA'), gettext('Organização'), gettext('Território'), gettext('Comunidade')];
    $('#agroecossistema_list_table thead tr').clone(true).appendTo( '#agroecossistema_list_table thead' );
    $('#agroecossistema_list_table thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        if (filter_array.indexOf( title )>-1) {
            $(this).html( '<input style="width: 100%" type="text" placeholder="' + gettext('Filtrar') + '" />' );
            $( 'input', this ).on( 'keyup change', function () {
                if ( agroecossistemaListTable.DataTable().column(i).search() !== this.value ) {
                    agroecossistemaListTable.DataTable()
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } else {
            $(this).html('');
        }
    } );

    /* Analiseagregada: Get visible and selected items */

    $('#FormWizard').on('submit', function(evt) {
        evt.preventDefault();

        agroecossistemaListTable.DataTable().rows('.selected').select();
        selected_items = agroecossistemaListTable.DataTable().rows({
          selected: true,
          search:'applied'
        }).data();

        if (! selected_items.length) {
            alert(gettext('Selecione ao menos um registro!'));
            return;
        }

        var ids = '';
        selected_items.each( function (i) {
            ids = ids ? ids + ',' + $(i[0]).val() : $(i[0]).val();
        } );
        $('#id_agroecossistemas').val(ids);

        $('#FormWizard')[0].submit();
    });

  $('#collapseAgroecossistemas').collapse();

});

