$(document).ready(function () {
  var COLUMN_GROUP = 1;

  var options = dataTableDefaultOptions;
  options.ordering = false;
  options.orderCellsTop = false;
  options.fixedHeader = true;
  options.scrollY = '40vh';
  options.scrollCollapse = true;
  options.paging = false;
  options.dom = 'rtip';
  options.columnDefs = [ {
    targets:   0,
    orderable: false,
  },
    {
      targets:   COLUMN_GROUP,
      visible: false
    }]
  options.order = [[ COLUMN_GROUP, 'asc' ]]
  options.rowGroup = {dataSrc: COLUMN_GROUP}

  var DataTableObject = $('#agroecossistema_list_table');
  setTimeout(function(){
    DataTableObject.DataTable(options);
  }, 5);

  /* Analiseagregada: Get visible and selected items */

  $('#FormWizard').on('submit', function(evt) {
    evt.preventDefault();

    selected_items = $('.analise_qualitativa_checkbox:checked');

    var ids = '';
    selected_items.each( function (i) {
      ids = ids ? ids + ',' + $(this).val() : $(this).val();
    } );
    $('#id_analise_qualitativa_ids').val(ids);

    $('#FormWizard')[0].submit();
  });

});

