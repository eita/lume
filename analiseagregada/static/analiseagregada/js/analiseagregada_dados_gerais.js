$(document).ready(function () {

  // Organização: caso alterado, limpar relacionados
  yl.jQuery('#id_organizacao').on('select2:select', function(e){
    yl.jQuery('#id_evento_gatilho')
      .val('').trigger("change");
  });

  // Formsets Indicadores: mostrar, caso exista valor
  $('.block-extra-forms').each(function() {
    if ($(this).find('.items .item').length > 1) {
      $(this).parents('.collapse').collapse('show');
    }
  });

  // Filtro por indicadores qualitativos:
  // opções de parametros relacionados ao atributo
  $("[id^=id_indicadores_qualitativos-][id$=-atributo]")
    .on('change', function (e) {
      reload_parametro_options(this);
    });

});

function reload_parametro_options(atributo_field) {
  parametro_selector = atributo_field.id
    .replace('atributo', 'parametro');
  parametro_field = $('#'+parametro_selector);

  atributo_id = $(atributo_field).val();
  $.post(
    URL_PARAMETROS_BY_ATRIBUTO,
    {'atributo_id': atributo_id},
    function(result) {
      $('option:gt(0)', parametro_field).remove();
      new_options = result.results;
      $.each(new_options, function(index, value) {
        parametro_field.append($("<option></option>")
          .attr("value", value[0]).text(value[1]));
      });
    }
  );
}
