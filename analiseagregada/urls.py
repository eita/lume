from django.urls import path

from . import views

app_name = 'analiseagregada'

urlpatterns = (
    # Urls for analiseagregada
    path('',
         views.AnaliseagregadaListView.as_view(),
         name="list"),

    path('<int:pk>/',
         views.AnaliseagregadaDetailView.as_view(),
         name="detail"),

    path('<int:pk>/delete/',
         views.AnaliseagregadaDeleteView.as_view(),
         name="delete"),

    path('<int:pk>/economica/',
         views.AnaliseagregadaEconomicaDetailView.as_view(),
         name="economica"),

    path('<int:pk>/qualitativa/',
         views.AnaliseagregadaQualitativaDetailView.as_view(),
         name="qualitativa"),

    path('<int:pk>/universo/',
         views.AnaliseagregadaUniversoView.as_view(),
         name="universo"),

    path('form/',
         views.DadosGeraisFormView.as_view(),
         name="form"),

    path('form/<int:pk>/<slug:slug_step>/',
         views.AnaliseagregadaFormView.as_view(),
         name="form_edit"),

    path('<int:pk>/toggle_agroecossistema/<int:agroecossistema>/',
          views.ToggleAgroecossistemaView.as_view(),
          name="toggle_agroecossistema"),

    path('parametros_by_atributo/',
         views.ParametrosByAtributo.as_view(),
         name="parametros_by_atributo"),
)
