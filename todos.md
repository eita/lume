# LUME - Entregas 2020

## CORES

Padronizar cores no atributo autonomia IGUAIS às
cores do atributo nas análises singulares
https://gitlab.com/eita/lume/-/issues/159

* Onde alterar?

## MODELO

menu > MODELO
https://gitlab.com/eita/lume/-/issues/168

* Verificar atribuição: Hoje é possível setar pela admin. Verificar outras telas e permissões.

## Exportação

Permitir exportação de todos os dados do agroecossistema
https://gitlab.com/eita/lume/-/issues/62

* Dados: quais? Organizados como?
* Ação exportação: botão na dashboard de agroecossistema?
* Permissão para exportar?

## UTF

Ocultar campo de UTF do NSGA (ver com calma)

* Entender melhor

## EVENTOS

Criar relação entre eventos
https://gitlab.com/eita/lume/-/issues/58

* Desenhar usabilidade
* Um evento pode pertencer a mais de um agrupamento?

# Reunião 12/2020

* comunicação com outros administradores de organização
  * administrador de organização ser inserido por admin do Lume

