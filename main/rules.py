from __future__ import absolute_import

import rules

from agroecossistema.models import Agroecossistema
from main.models import OrganizacaoUsuaria

@rules.predicate
def eh_admin_geral(usuaria):
    return usuaria.is_superuser or rules.is_group_member('admin')(usuaria)

@rules.predicate
def eh_anonimo(usuaria):
    return usuaria.is_anonymous

@rules.predicate
def eh_admin_da_organizacao(usuaria, organizacao):
    try:
        ou = OrganizacaoUsuaria.objects.get(usuaria_id=usuaria.id, organizacao_id=organizacao.id, is_admin=True)
        return True
    except OrganizacaoUsuaria.DoesNotExist:
        return False


@rules.predicate
def eh_admin_da_organizacao_do_agroecossistema(usuaria, agroecossistema):
    try:
        ou = OrganizacaoUsuaria.objects.get(usuaria_id=usuaria.id, organizacao_id=agroecossistema.organizacao_id,
                                            is_admin=True)
        return True
    except OrganizacaoUsuaria.DoesNotExist:
        return False
    except AttributeError:
        return False


@rules.predicate
def rule_pode_alterar_agroecossistema(usuaria, agroecossistema):
    if agroecossistema is None or not isinstance(agroecossistema, Agroecossistema):
        return False
    if agroecossistema.autor_id == usuaria.id:
        return True
    try:
        ou = OrganizacaoUsuaria.objects.get(usuaria_id=usuaria.id, organizacao_id=agroecossistema.organizacao_id)
        return True
    except OrganizacaoUsuaria.DoesNotExist:
        return False


@rules.predicate
def rule_pode_alterar_comunidade_ou_territorio(usuaria, agroecossistemas):
    if agroecossistemas is None:
        return False
    qtde = agroecossistemas.filter(organizacao_id__in=usuaria.organizacoes.values_list('id')).count()
    return qtde > 0


@rules.predicate
def agroecossistema_eh_publico(usuaria, agroecossistema):
    try:
        return bool(agroecossistema.visualizacao_publica)
    except:
        return False


@rules.predicate
def eh_admin_de_alguma_organizacao(usuaria):
    ous = OrganizacaoUsuaria.objects.filter(usuaria_id=usuaria.id, is_admin=True)
    return ous.count() > 0


@rules.predicate
def eh_integrante_de_alguma_organizacao(usuaria):
    ous = OrganizacaoUsuaria.objects.filter(usuaria_id=usuaria.id)
    return ous.count() > 0


@rules.predicate
def eh_integrante_da_organizacao(usuaria, organizacao):
    try:
        ou = OrganizacaoUsuaria.objects.get(usuaria_id=usuaria.id, organizacao_id=organizacao.id)
        return True
    except OrganizacaoUsuaria.DoesNotExist:
        return False

rules.add_perm('eh_admin_geral', eh_admin_geral)
rules.add_perm('organizacao.ver_todos', eh_admin_geral)
rules.add_perm('organizacao.cadastrar', eh_admin_geral)
rules.add_perm('organizacao.alterar', eh_admin_geral | eh_admin_da_organizacao)
rules.add_perm('organizacao.visualizar', eh_admin_geral | eh_integrante_da_organizacao)
rules.add_perm('organizacao.apagar', eh_admin_geral | eh_admin_da_organizacao)

rules.add_perm('eventogatilho.visualizar', eh_admin_geral | eh_admin_de_alguma_organizacao)

# ao cadastrar: ha um filtro de organizacoes possiveis
rules.add_perm('agroecossistema.cadastrar', eh_admin_geral | eh_integrante_de_alguma_organizacao)
rules.add_perm('agroecossistema.alterar', eh_admin_geral | rule_pode_alterar_agroecossistema)
rules.add_perm('agroecossistema.apagar', eh_admin_geral | rule_pode_alterar_agroecossistema)
rules.add_perm('agroecossistema.visualizar',
               eh_admin_geral | eh_admin_da_organizacao_do_agroecossistema |
               rule_pode_alterar_agroecossistema | agroecossistema_eh_publico)

rules.add_perm('comunidade.cadastrar', eh_admin_geral | eh_integrante_de_alguma_organizacao)
rules.add_perm('comunidade.apagar', eh_admin_geral | eh_integrante_de_alguma_organizacao)

rules.add_perm('territorio.cadastrar', eh_admin_geral | eh_integrante_de_alguma_organizacao)
rules.add_perm('territorio.apagar', eh_admin_geral | eh_integrante_de_alguma_organizacao)

rules.add_perm('comunidade_ou_territorio.alterar', eh_admin_geral | rule_pode_alterar_comunidade_ou_territorio)

rules.add_perm('permissao_padrao', eh_admin_geral | eh_integrante_de_alguma_organizacao)
