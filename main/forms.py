from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Button
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse
from django.utils.translation import gettext as _

from dal import autocomplete

from municipios.widgets import SelectMunicipioWidget
from municipios.models import UF, Municipio
from .models import Comunidade, Organizacao, OrganizacaoUsuaria, Usuaria, Territorio, Unidade, Servico, Produto, Ajuda, AjudaIndicador
from paises_municipios.models import Pais


class LumeModelForm(forms.ModelForm):
    can_delete = False
    delete_confirm_message = ''
    delete_success_url = ''
    delete_url = ''

    def __init__(self, *args, **kwargs):
        super(LumeModelForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-vertical'
        # self.helper.label_class = 'col-lg-2'
        # self.helper.field_class = 'col-lg-8'
        if self.can_delete:
            self.helper.add_input(Button('delete-button', _("Apagar"), css_class='btn btn-danger',
                                         data_delete_confirm_message=self.delete_confirm_message,
                                         data_success_url=self.delete_success_url,
                                         data_delete_url=self.delete_url))
        self.helper.add_input(Submit('btn_submit datatable-export-disabled', _('Concluir')))


class LumeForm(forms.Form):

    def __init__(self, *args, **kwargs):
        super(LumeForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-2'
        self.helper.field_class = 'col-lg-8'
        self.helper.add_input(Submit('submit', _('Gravar')))


class UFModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.nome


class ComunidadeForm(LumeModelForm):

    pais = forms.ModelChoiceField(
        queryset=Pais.objects.all(),
        widget=autocomplete.ModelSelect2(url='paises_municipios:pais-autocomplete')
    )

    uf = UFModelChoiceField(
        queryset=UF.objects.all(),
        widget=autocomplete.ModelSelect2(url='paises_municipios:uf-autocomplete',
                                         forward=['pais'])
    )

    municipio = forms.ModelChoiceField(
        queryset=Municipio.objects.all(),
        widget=autocomplete.ModelSelect2(url='paises_municipios:municipio-autocomplete',
                                         forward=['uf'])
    )

    def __init__(self, *args, **kwargs):
        self.can_delete = kwargs.pop('can_delete', False)
        if self.can_delete:
            self.delete_confirm_message = _('Deseja realmente apagar esta comunidade?')
            self.delete_url = reverse('comunidade_delete', kwargs={'pk': kwargs.get('instance').id})
            self.delete_success_url = reverse('comunidade_list')

        prefix = kwargs['prefix']
        kwargs['prefix'] = None

        super().__init__(*args, **kwargs)

        if kwargs['instance']:
            self.fields['uf'].initial = self.instance.municipio.uf.id
        else:
            try:
                pais_sugerido = prefix['pais_sugerido']
            except:
                pais_sugerido = None
            if pais_sugerido:
                self.fields['pais'].initial = pais_sugerido

    class Meta:
        model = Comunidade
        fields = ['nome', 'pais', 'uf', 'municipio']
        exclude = ['geoponto', 'poligono']
        '''
        widgets = {
            'municipio': SelectMunicipioWidget()
        }
        '''


class OrganizacaoForm(LumeModelForm):
    def __init__(self, *args, **kwargs):
        self.can_delete = kwargs.pop('can_delete', False)
        if self.can_delete:
            self.delete_confirm_message = _('Deseja realmente apagar esta organização e todos os elementos que fazem parte dela (agroecossistemas, etc)?')
            self.delete_url = reverse('organizacao_delete', kwargs={'pk': kwargs.get('instance').id})
            self.delete_success_url = reverse('organizacao_list')

        super(OrganizacaoForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Organizacao
        fields = ['nome']


class OrganizacaoUsuariaForm(forms.ModelForm):
    class Meta:
        model = OrganizacaoUsuaria
        fields = ['is_admin', 'organizacao', 'usuaria']


class UsuariaCreationForm(UserCreationForm, LumeModelForm):
    is_organization_admin = forms.BooleanField(label=_('É admin da organização?'), required=False)

    class Meta:
        model = Usuaria
        fields = ['username', 'first_name', 'last_name', 'email', 'password1', 'password2', 'is_organization_admin']
        labels = {
            'username': _('Nome de usuário')
        }


class TerritorioForm(LumeModelForm):
    def __init__(self, *args, **kwargs):
        self.can_delete = kwargs.pop('can_delete', False)
        if self.can_delete:
            self.delete_confirm_message = _('Deseja realmente apagar este territorio?')
            self.delete_url = reverse('territorio_delete', kwargs={'pk': kwargs.get('instance').id})
            self.delete_success_url = reverse('territorio_list')

        super(TerritorioForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Territorio
        fields = ['nome', 'descricao']


class UnidadeForm(LumeModelForm):

    def __init__(self, *args, **kwargs):
        self.can_delete = kwargs.pop('can_delete', False)
        if self.can_delete:
            # self.delete_confirm_message = _('REVER Deseja realmente apagar esta unidade?')
            self.delete_confirm_message = _('Deseja realmente apagar este registro?')
            self.delete_url = reverse('unidade_delete', kwargs={'pk': kwargs.get('instance').id})
            self.delete_success_url = reverse('unidade_list')
        super(UnidadeForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Unidade
        fields = ['nome']


class ServicoForm(LumeModelForm):

    def __init__(self, *args, **kwargs):
        self.can_delete = kwargs.pop('can_delete', False)
        if self.can_delete:
            # self.delete_confirm_message = _('REVER Deseja realmente apagar este servico?')
            self.delete_confirm_message = _('Deseja realmente apagar este registro?')
            self.delete_url = reverse('servico_delete', kwargs={'pk': kwargs.get('instance').id})
            self.delete_success_url = reverse('servico_list')
        super(ServicoForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Servico
        fields = ['nome']


class ProdutoForm(LumeModelForm):

    def __init__(self, *args, **kwargs):
        self.can_delete = kwargs.pop('can_delete', False)
        if self.can_delete:
            # self.delete_confirm_message = _('REVER Deseja realmente apagar este produto?')
            self.delete_confirm_message = _('Deseja realmente apagar este registro?')
            self.delete_url = reverse('produto_delete', kwargs={'pk': kwargs.get('instance').id})
            self.delete_success_url = reverse('produto_list')
        super(ProdutoForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Produto
        fields = ['nome']

class DateInput(forms.DateInput):
    input_type='date'

class AjudaForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.can_delete = kwargs.pop('can_delete', False)
        if self.can_delete:
            self.delete_confirm_message = _('Deseja realmente apagar esta ajuda?')
            self.delete_url = reverse('ajuda_delete', kwargs={'pk': kwargs.get('instance').id})
            self.delete_success_url = reverse('ajuda')

        super(AjudaForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Ajuda
        fields = ['modulo', 'submodulo', 'nome', 'ajuda', 'ajuda_curta']

AjudaFormSet = forms.modelformset_factory(Ajuda, form=AjudaForm, can_delete=True, extra=1, min_num=0)

class AjudaIndicadorForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.can_delete = kwargs.pop('can_delete', False)
        if self.can_delete:
            self.delete_confirm_message = _('Deseja realmente apagar esta ajuda de indicador?')
            self.delete_url = reverse('ajuda_delete', kwargs={'pk': kwargs.get('instance').id})
            self.delete_success_url = reverse('ajuda_indicador')

        super(AjudaIndicadorForm, self).__init__(*args, **kwargs)

    class Meta:
        model = AjudaIndicador
        fields = ['nome', 'slug', 'formula', 'ajuda', 'ajuda_curta']

AjudaIndicadorFormSet = forms.modelformset_factory(AjudaIndicador, form=AjudaIndicadorForm, can_delete=True, extra=1, min_num=0)
