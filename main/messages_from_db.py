def add_helper(dicionario, atributo, parametro, valor):
    if atributo not in dicionario:
        dicionario[atributo]={parametro: valor}
    else:
        dicionario[atributo][parametro] = valor

    return dicionario

def add_simple_helper(dicionario, slug, nome, ajuda = ""):
    if slug in dicionario:
        return dicionario

    dicionario[slug] = {
        'nome': nome,
        'ajuda': ajuda
    }
    return dicionario

qualitativa_helpers = {}
linhadotempo_helpers = {}
linhaterritorial_helpers = {}
ajuda_indicador_helpers = {}

try:
    from qualitativa.messages_from_db_generated import *
except ImportError:
    pass

try:
    from linhadotempo.messages_from_db_generated import *
except ImportError:
    pass

try:
    from linhaterritorial.messages_from_db_generated import *
except ImportError:
    pass

try:
    from main.messages_from_db_generated_ajuda_indicador import *
except ImportError:
    pass
