from django.contrib.auth.models import AbstractUser
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.gis.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _lazy
from django.utils.text import slugify
from django.core.exceptions import ValidationError

from main.utils import get_name_initials
from lume.models import LumeModel
from municipios.models import Municipio
from paises_municipios.models import Pais
from main.utils import getHelperI18n


class Usuaria(AbstractUser):

    def __str__(self):
        return self.get_full_name() if self.get_full_name() != "" else self.username

    def get_displayname(self):
        return self.first_name if self.first_name != "" else self.username

    def get_displayname_initials(self):
        nome = self.get_full_name() if self.first_name != "" else self.username
        return get_name_initials(nome)

    def clean(self):
        username = slugify(self.username.lower())
        if self.username == username:
            return super().clean()
        raise ValidationError(
            _lazy("Utilize apenas minúsculas, sem acentuação, para o nome de usuária")
        )

    class Meta:
        db_table = "usuaria"
        ordering = ("first_name",)


class Organizacao(LumeModel):
    nome = models.CharField(max_length=80, verbose_name=_lazy("nome"))
    usuarias = models.ManyToManyField(
        Usuaria,
        through="OrganizacaoUsuaria",
        related_name="organizacoes",
        verbose_name=_lazy("usuárias"),
    )
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("última alteração")
    )

    class Meta:
        db_table = "organizacao"
        ordering = [
            "nome",
        ]

    def __str__(self):
        return self.nome

    def get_absolute_url(self):
        return reverse("organizacao_detail", args=(self.pk,))

    def get_update_url(self):
        return reverse("organizacao_update", args=(self.pk,))


class OrganizacaoUsuaria(models.Model):
    organizacao = models.ForeignKey(
        Organizacao,
        models.CASCADE,
        related_name="org_usuarias",
        verbose_name=_lazy("organização"),
    )
    usuaria = models.ForeignKey(
        Usuaria,
        models.CASCADE,
        related_name="org_usuarias",
        verbose_name=_lazy("usuária"),
    )
    is_admin = models.BooleanField(
        default=False, verbose_name=_lazy("é admin da organização")
    )

    class Meta:
        ordering = (
            "-is_admin",
            "usuaria__username",
        )


class Territorio(LumeModel):
    nome = models.CharField(max_length=80, verbose_name=_lazy("nome"))
    descricao = models.TextField(verbose_name=_lazy("descrição"))
    eventos = GenericRelation("linhaterritorial.EventoTerritorial")
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("última alteração")
    )

    class Meta:
        ordering = ("nome",)

    def __str__(self):
        return self.nome

    def get_absolute_url(self):
        return reverse("territorio_detail", args=(self.pk,))

    def get_update_url(self):
        return reverse("territorio_update", args=(self.pk,))


class Comunidade(LumeModel):
    nome = models.CharField(max_length=80, verbose_name=_lazy("nome"))
    pais = models.ForeignKey(
        Pais, models.PROTECT, verbose_name=_lazy("país"), null=True
    )
    municipio = models.ForeignKey(
        Municipio, models.PROTECT, verbose_name=_lazy("município")
    )
    geoponto = models.PointField(blank=True, null=True, verbose_name=_lazy("geoponto"))
    poligono = models.PolygonField(
        blank=True, null=True, verbose_name=_lazy("poligono")
    )
    eventos = GenericRelation("linhaterritorial.EventoTerritorial")
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("última alteração")
    )

    class Meta:
        db_table = "comunidade"
        ordering = ("nome",)

    def __str__(self):
        str = self.nome
        try:
            if self.municipio is not None:
                str += (
                    " - " + self.municipio.__str__() + " - " + self.municipio.uf.sigla
                )
        except:
            pass
        return str

    def get_absolute_url(self):
        return reverse("comunidade_detail", args=(self.pk,))

    def get_update_url(self):
        return reverse("comunidade_update", args=(self.pk,))


class Unidade(LumeModel):
    # Fields
    nome = models.CharField(max_length=60, verbose_name=_lazy("nome"))
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("última alteração")
    )

    class Meta:
        ordering = ("nome",)

    def __unicode__(self):
        return "%s" % self.pk

    def __str__(self):
        return self.nome

    def get_absolute_url(self):
        return reverse("unidade_detail", args=(self.pk,))

    def get_update_url(self):
        return reverse("unidade_update", args=(self.pk,))


class Servico(LumeModel):
    # Fields
    nome = models.CharField(max_length=60, verbose_name=_lazy("nome"))
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("última alteração")
    )

    class Meta:
        ordering = ("nome",)

    def __unicode__(self):
        return "%s" % self.pk

    def __str__(self):
        return self.nome

    def get_absolute_url(self):
        return reverse("servico_detail", args=(self.pk,))

    def get_update_url(self):
        return reverse("servico_update", args=(self.pk,))


class Produto(LumeModel):
    nome = models.CharField(max_length=100, verbose_name=_lazy("nome"))
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("última alteração")
    )

    class Meta:
        ordering = ("nome",)

    def __unicode__(self):
        return "%s" % self.pk

    def __str__(self):
        return self.nome

    def get_absolute_url(self):
        return reverse("produto_detail", args=(self.pk,))

    def get_update_url(self):
        return reverse("produto_update", args=(self.pk,))


class Ajuda(models.Model):
    modulo = models.CharField(max_length=100, verbose_name=_lazy("modulo"))
    submodulo = models.CharField(
        blank=True, null=True, max_length=100, verbose_name=_lazy("submodulo")
    )
    nome = models.CharField(max_length=100, verbose_name=_lazy("nome"))
    ajuda = models.TextField(verbose_name=_lazy("Ajuda"))
    ajuda_curta = models.TextField(
        blank=True, null=True, verbose_name=_lazy("Ajuda resumida")
    )

    class Meta:
        unique_together = (
            "modulo",
            "submodulo",
        )

    def __str__(self):
        return self.ajuda

    def get_absolute_url(self):
        return reverse("ajuda")


class AjudaIndicador(models.Model):
    slug = models.CharField(
        max_length=50, blank=True, null=True, verbose_name=_lazy("abreviação")
    )
    nome = models.CharField(max_length=80, verbose_name=_lazy("indicador"))
    formula = models.CharField(
        max_length=200, blank=True, null=True, verbose_name=_lazy("fórmula")
    )
    ajuda = models.TextField(blank=True, null=True, verbose_name=_lazy("ajuda"))
    ajuda_curta = models.TextField(
        blank=True, null=True, verbose_name=_lazy("ajuda resumida")
    )

    class Meta:
        unique_together = ("nome",)
        ordering = ("nome",)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.cachedHelperI18n = getHelperI18n(
            "ajuda_indicador_helpers", self, self.nome
        )

    def __str__(self):
        if self.slug:
            return self.slug
        return self.getNome()

    def getNome(self):
        return self.cachedHelperI18n["nome"]

    def getAjuda(self):
        return self.cachedHelperI18n["ajuda"]

    def getAjudaCurta(self):
        return self.cachedHelperI18n["ajuda_curta"]

    def getFormula(self):
        if not self.formula:
            return ""
        if "=" in self.formula:
            return self.formula
        if self.slug:
            return f"{self.slug} = {self.formula}"
        return f"{self.__str__().title()} = {self.formula}"
