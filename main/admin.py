from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from main.models import Usuaria, Organizacao, OrganizacaoUsuaria, Ajuda, AjudaIndicador, Comunidade, Territorio

class AjudaAdmin(admin.ModelAdmin):
    list_display = ("nome", "modulo", "submodulo",)

class AjudaIndicadorAdmin(admin.ModelAdmin):
    list_display = ("nome", "slug", "formula",)

class OrganizacaoUsuariaAdmin(admin.ModelAdmin):
    list_display = ("usuaria", "organizacao", "is_admin",)

admin.site.register(Usuaria, UserAdmin)
admin.site.register(Comunidade)
admin.site.register(Territorio)
admin.site.register(Organizacao)
admin.site.register(OrganizacaoUsuaria, OrganizacaoUsuariaAdmin)
admin.site.register(AjudaIndicador, AjudaIndicadorAdmin)
admin.site.register(Ajuda, AjudaAdmin)
