# Generated by Django 2.1.5 on 2019-04-02 23:47

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Territorio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=80)),
                ('descricao', models.TextField()),
                ('criacao', models.DateTimeField(auto_now_add=True)),
                ('ultima_alteracao', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ('-criacao',),
            },
        ),
        migrations.RemoveField(
            model_name='usuaria',
            name='organizacoes',
        ),
        migrations.AddField(
            model_name='comunidade',
            name='criacao',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='comunidade',
            name='ultima_alteracao',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AddField(
            model_name='organizacao',
            name='criacao',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='organizacao',
            name='ultima_alteracao',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AddField(
            model_name='organizacao',
            name='usuarias',
            field=models.ManyToManyField(related_name='organizacoes', through='main.OrganizacaoUsuaria', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='organizacaousuaria',
            name='organizacao',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='org_usuarias', to='main.Organizacao'),
        ),
        migrations.AlterField(
            model_name='organizacaousuaria',
            name='usuaria',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='org_usuarias', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='agroecossistema',
            name='territorio',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='main.Territorio'),
        ),
    ]
