# Generated by Django 2.1.5 on 2019-04-10 16:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_auto_20190408_1127'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agroecossistema',
            name='territorio',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='main.Territorio'),
        ),
    ]
