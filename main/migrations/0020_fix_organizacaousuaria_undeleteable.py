from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('main', '0019_auto_20190613_1254'),
    ]

    operations = [
        migrations.RunSQL("""
        DELETE FROM main_organizacaousuaria
        WHERE deleted IS NOT NULL
        """)
    ]
