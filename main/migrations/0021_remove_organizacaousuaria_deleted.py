# Generated by Django 2.1.5 on 2019-10-17 18:50

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0020_fix_organizacaousuaria_undeleteable'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='organizacaousuaria',
            name='deleted',
        ),
    ]
