# Generated by Django 2.1.5 on 2019-04-12 13:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0006_change_ondelete'),
    ]

    operations = [
        migrations.AddField(
            model_name='agroecossistema',
            name='deleted',
            field=models.DateTimeField(editable=False, null=True),
        ),
        migrations.AddField(
            model_name='comunidade',
            name='deleted',
            field=models.DateTimeField(editable=False, null=True),
        ),
        migrations.AddField(
            model_name='organizacao',
            name='deleted',
            field=models.DateTimeField(editable=False, null=True),
        ),
        migrations.AddField(
            model_name='organizacaousuaria',
            name='deleted',
            field=models.DateTimeField(editable=False, null=True),
        ),
        migrations.AddField(
            model_name='territorio',
            name='deleted',
            field=models.DateTimeField(editable=False, null=True),
        ),
    ]
