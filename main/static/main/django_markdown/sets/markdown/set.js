// -------------------------------------------------------------------
// markItUp!
// -------------------------------------------------------------------
// Copyright (C) 2008 Jay Salvat
// http://markitup.jaysalvat.com/
// -------------------------------------------------------------------
// MarkDown tags example
// http://en.wikipedia.org/wiki/Markdown
// http://daringfireball.net/projects/markdown/
// -------------------------------------------------------------------
// Feel free to add more tags
// -------------------------------------------------------------------
mySettings = {
  onShiftEnter: { keepDefault: false, openWith: "\n\n" },
  markupSet: [
    { name: "Negrito", key: "B", openWith: "**", closeWith: "**" },
    { name: "Itálico", key: "I", openWith: "_", closeWith: "_" },
    { name: "Lista", openWith: "- " },
    {
      name: "Lista numérica",
      openWith: function (markItUp) {
        return markItUp.line + ". ";
      },
    },
    {
      name: "Link",
      key: "L",
      openWith: "[",
      closeWith: ']([![Url:!:http://]!] "[![Title]!]")',
      placeHolder: "Seu texto para o link aqui...",
    },
    { name: "Preview", call: "preview", className: "preview" },
  ],
};

// mIu nameSpace to avoid conflict.
miu = {
  markdownTitle: function (markItUp, achar) {
    heading = "";
    n = jQuery.trim(markItUp.selection || markItUp.placeHolder).length;
    // work around bug in python-markdown where header underlines must be at least 3 chars
    if (n < 3) {
      n = 3;
    }
    for (i = 0; i < n; i++) {
      heading += achar;
    }
    return "\n" + heading;
  },
};
