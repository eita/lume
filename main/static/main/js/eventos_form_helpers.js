const CHECKBOX_GATILHO = "#id_is_gatilho";

function setupAnexos(evento, editable) {
  if (!editable) {
    const images = evento.anexos.filter(function (anexo) {
      return anexo.mimetype.match(/^image/);
    });
    evento.anexos.filter(function (anexo) {
      return anexo.mimetype.match(/^image/) == null;
    });

    const imagesWrapper = $("#evento-anexos-carousel .carousel-inner").empty();

    if (images.length) {
      //jQuery('.carousel').carousel('dispose')
      $("#evento-anexos-carousel").removeClass("d-none");
      var i = 0;
      for (var key in images) {
        imagesWrapper.append(
          '<div class="carousel-item ' +
            (i == 0 ? "active" : "") +
            '">\n' +
            '<img class="d-block w-100 evento-anexo-image" src="' +
            images[key].url +
            '" alt="' +
            images[key].nome +
            '">\n' +
            "</div>"
        );
        i++;
      }
    } else {
      $("#evento-anexos-carousel").addClass("d-none");
    }
    $(".carousel").carousel();
  }

  $("#evento-anexos-files-wrapper").toggle(evento.anexos.length > 0);
  $("#evento-anexos-files-empty").toggle(evento.anexos.length === 0);
  if (evento.anexos.length > 0) {
    var filesWrapper = $("#evento-anexos-files").empty();
    for (let anexo of evento.anexos) {
      filesWrapper.append(
        '<a class="list-group-item list-group-item-action" href="' +
          anexo.url +
          '">' +
          anexo.nome +
          "</a>"
      );
    }
  }

  const href = $("#evento-anexos")
    .data("href")
    .replace("999999999999", evento.id);
  $("#evento-anexos").attr("href", href).toggle(editable);
}

function openCriaEventoForm(item) {
  $(".modal-right-title").html(`Criar evento`);
  item.novo = true;
  populateEditaEventoForm(item);

  setTimeout(function () {
    $("#modal-right-wrapper").scrollTop(0);
    $(".modal-right-body").scrollTop(0);
  }, 600);
  $(".eventoFormDiv").show();
  $(".eventoDetailDiv").hide();
  $(".modal-right-footer").show();
  $("#modal-right-wrapper").fadeIn();
}

function openEditaEventoForm(item, shouldSetupAnexos = true) {
  $(".modal-right-title").html(item.title);
  $(".modal-right-footer .btn-apagar").data("id", item.id);
  item.novo = false;
  populateEditaEventoForm(item);

  setTimeout(function () {
    $("#modal-right-wrapper").scrollTop(0);
    $(".modal-right-body").scrollTop(0);
  }, 600);
  $(".eventoFormDiv").show();
  $(".eventoDetailDiv").hide();
  $(".modal-right-footer").show();
  $("#modal-right-wrapper").fadeIn();

  if (shouldSetupAnexos) {
    setupAnexos(item, true);
  }
}

function populateEditaEventoForm(item, isTerritorial = false) {
  if (
    Object.prototype.hasOwnProperty.call(item, "novo") &&
    (item.novo === true || item.novo === "true")
  ) {
    let anoInicio = moment(item.start).year();
    let end = "";
    item.id = "";
    item.is_gatilho = false;
    item.evento_gatilho = null;
    item.titulo = item.titulo || "";
    item.start = `${anoInicio}-01-03T00:00:00`;
    item.end = end;
    item.detalhes = "";
    item.criado_por = "";
    item.is_public = false;

    if (isTerritorial) {
      // $('#id_tipo_dimensao').val(null).trigger('change');
      for (let periodo of periodos) {
        if (periodo.coluna === anoInicio - 1900) {
          item.periodo = periodo.id;
          item.periodo_coluna = periodo.coluna;
          break;
        }
      }
    }
  }

  evento = itemToEvento(item, isTerritorial);

  for (let key in evento) {
    switch (key) {
      case "is_gatilho":
      case "is_public":
        $(`#id_${key}`).prop("checked", evento[key]).trigger("change");
        break;
      case "detalhes":
        $("#id_detalhes").summernote("code", evento[key]);
        break;
      case "titulo":
        if (isTerritorial) {
          $("#id_titulo").summernote("code", evento[key]);
        } else {
          $("#id_titulo").val(evento[key]).trigger("change");
        }
        break;
      case "tipo_dimensao":
        $("#id_tipo_dimensao").val(evento.tipo_dimensao).trigger("change");
        // Update dimensao if exists:
        if (evento.dimensao_nome) {
          const option = new Option(
            evento.dimensao_nome,
            evento.dimensao,
            true,
            true
          );
          $("#id_subdimensao").append(option).trigger("change");
          $("#id_subdimensao").val(evento.dimensao).trigger("change");
          $("#id_subdimensao_nome").val(evento.dimensao_nome);
        }
        break;
      default:
        $(`#id_${key}`).val(evento[key]).trigger("change");
        break;
    }
  }
}

function itemToEvento(item, isTerritorial = false) {
  let data_inicio = moment(item.start).year();
  if (moment(item.start).month() > 6) {
    data_inicio++;
  }
  let data_fim =
    item.end && moment(item.end).year() != data_inicio
      ? moment(item.end).year()
      : null;
  if (data_fim && item.end && moment(item.end).month() < 6) {
    data_fim--;
  }

  return {
    novo: item.novo,
    id: item.id,
    is_gatilho: item.evento_gatilho != null,
    evento_gatilho: item.evento_gatilho,
    titulo: item.titulo,
    dimensao: isTerritorial ? item.dimensao : item.group,
    dimensao_nome: isTerritorial ? item.dimensao_nome : null,
    tipo_dimensao: isTerritorial ? item.group : null,
    data_inicio: data_inicio,
    data_fim: data_fim,
    periodo: item.periodo,
    periodo_coluna: item.periodo_coluna,
    detalhes: item.detalhes,
    criado_por: item.criado_por,
    is_public: item.is_public,
  };
}

function eventoToItem(evento, eventoVelho = null, isTerritorial = false) {
  let anoFormatted =
    evento.data_inicio != evento.data_fim && evento.data_fim
      ? `${evento.data_inicio} - ${evento.data_fim}`
      : `${evento.data_inicio}`;

  let periodo_coluna;
  let periodo_id;
  if (isTerritorial) {
    periodo_coluna = evento.data_inicio - 1900;
    for (periodo of periodos) {
      if (periodo.coluna === periodo_coluna) {
        anoFormatted = periodo.nome;
        periodo_id = periodo.id;
        break;
      }
    }
  }

  let item = {
    id: evento.id,
    evento_gatilho: evento.evento_gatilho,
    titulo: evento.titulo,
    title: evento.titulo,
    content: evento.titulo,
    anexos: eventoVelho ? eventoVelho.anexos : [],
    anexos_url: eventoVelho ? eventoVelho.anexos_url : "",
    ano_formatted: anoFormatted,
    className: "",
    detalhes: evento.detalhes,
    start: `${evento.data_inicio}-01-03T00:00:00`,
    end: evento.data_fim
      ? `${evento.data_fim}-12-30T00:00:00`
      : `${evento.data_inicio}-12-30T00:00:00`,
    group: isTerritorial ? evento.tipo_dimensao : evento.dimensao,
    dimensao: isTerritorial ? evento.dimensao : null,
    dimensao_nome: isTerritorial ? evento.dimensao_nome : null,
    criado_por: evento.criado_por,
    is_public: evento.is_public,
  };

  if (isTerritorial) {
    item.periodo = periodo_id;
    item.periodo_coluna = periodo_coluna;
    item.className = "territorial";
  }

  if (Object.prototype.hasOwnProperty.call(evento, "editable")) {
    item.editable = evento.editable;
  }

  return item;
}

function doAtualizaEvento(
  params,
  agroecossistemaId = null,
  comunidadeOuTerritorioId = null,
  onSuccess,
  onError,
  restDeleteUrl,
  restUpdateUrl,
  isTerritorial = false
) {
  if (!params.hasOwnProperty("evento")) {
    return false;
  }
  const evento = params.evento;
  const apagar = params.hasOwnProperty("apagar") && params.apagar;
  const eventoAntigo = params.hasOwnProperty("eventoAntigo")
    ? params.eventoAntigo
    : null;
  let data = {
    id: evento.id,
  };
  let action = "delete";
  let url = restDeleteUrl;
  let toastTitle = "Apagado";
  let toastBody = "apagado";

  if (!apagar) {
    action = "update";
    url = restUpdateUrl;
    toastBody = "atualizado";
    if (
      evento.hasOwnProperty("novo") &&
      (evento.novo === true || evento.novo == "true")
    ) {
      delete evento.id;
      toastBody = "criado";
    }
    delete evento.novo;
    data = isTerritorial
      ? {
          ...evento,
          content_type_id: contentTypeId,
          object_id: comunidadeOuTerritorioId,
        }
      : {
          ...evento,
          agroecossistema_id: agroecossistemaId,
        };
    toastTitle = "Salvo";
  }
  $.ajax(`${url}`, {
    method: "POST",
    data,
    dataType: "json",
    success: function (data) {
      onSuccess(data, evento, apagar, toastBody);
    },
    error: function () {
      onError(eventoAntigo, evento);
    },
  });
}

function initFormEvento() {
  is_gatilho = $(CHECKBOX_GATILHO).is(":checked");
  $("<input>")
    .attr({
      type: "hidden",
      name: "id",
      id: "id_id",
    })
    .appendTo("#modal-right-wrapper form");
  $("<input>")
    .attr({
      type: "hidden",
      name: "novo",
      id: "id_novo",
    })
    .appendTo("#modal-right-wrapper form");
  $("#id_evento_gatilho").attr("disabled", !is_gatilho);
  $("#id_evento_gatilho").next(".invalid-feedback").eq(0).toggle(is_gatilho);
  $("#id_titulo").attr("disabled", is_gatilho);
  $("#id_titulo").next(".invalid-feedback").eq(0).toggle(!is_gatilho);
  $("#id_dimensao").attr("disabled", is_gatilho);
  $("#id_dimensao").next(".invalid-feedback").eq(0).toggle(!is_gatilho);
}

function initEventosFormHelpers(atualizaEvento, isTerritorial = false) {
  $("#modal-right-wrapper form").on("submit", function (e) {
    e.preventDefault();
    let evento = {};
    $('#modal-right-wrapper form [id^="id_"]').each(function (k, v) {
      const field = v.id.substr(3);
      switch (field) {
        case "is_gatilho":
        case "is_public":
          evento[field] = $(`#id_${field}`).prop("checked");
          break;
        case "evento_gatilho":
          evento.evento_gatilho =
            $("#id_evento_gatilho").val() != ""
              ? $("#id_evento_gatilho").val()
              : null;
          break;
        case "detalhes":
          evento.detalhes = $("#id_detalhes").summernote("code");
          break;
        case "titulo":
          evento.titulo = isTerritorial
            ? $("#id_titulo").summernote("code")
            : $(`#id_titulo`).val();
          break;
        case "periodo":
          evento.periodo = $(`#${v.id}`).val();
          for (let periodo of periodos) {
            if (periodo.id === parseInt(evento.periodo)) {
              evento.periodo_coluna = periodo.coluna;
              evento.data_inicio = periodo.coluna + 1900;
              evento.data_fim = null;
              break;
            }
          }
          break;
        default:
          evento[field] = $(`#${v.id}`).val();
          break;
      }
    });
    atualizaEvento({ evento });

    $("#modal-right-wrapper").fadeOut();
  });

  $(".modal-right-footer .btn-apagar").on("click", function (e) {
    e.preventDefault();
    if (
      window.confirm(
        "Tem certeza que quer apagar este evento? Não é possível desfazer esta operação."
      )
    ) {
      const eventoId = $(e.target).data("id");
      $("#modal-right-wrapper").fadeOut();
      atualizaEvento({
        evento: { id: eventoId },
        apagar: true,
      });
    }
  });

  $(CHECKBOX_GATILHO).ready(initFormEvento).on("change", initFormEvento);
}
