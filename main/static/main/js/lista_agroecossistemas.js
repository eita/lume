$(document).ready(function () {
  var options = dataTableDefaultOptions;
  options.orderCellsTop = true;
  options.fixedHeader = true;
  options.scrollY = "40vh";
  options.scrollCollapse = true;
  options.paging = false;
  options.dom = "Bfrtip";
  options.buttons = ["excelHtml5", "csvHtml5", "pdfHtml5"];
  var agroecossistemaListTable = $("#agroecossistema_list_table");
  setTimeout(function () {
    agroecossistemaListTable.DataTable(options);
  }, 5);
  var filter_array = [
    gettext("NSGA"),
    gettext("Organização"),
    gettext("Território"),
    gettext("Comunidade"),
  ];
  $("#agroecossistema_list_table thead tr")
    .clone(true)
    .appendTo("#agroecossistema_list_table thead");
  $("#agroecossistema_list_table thead tr:eq(1) th").each(function (i) {
    var title = $(this).text();
    if (filter_array.indexOf(title) > -1) {
      $(this).html(
        '<input style="width: 100%" type="text" placeholder="' +
          gettext("Filtrar") +
          '" />'
      );
      $("input", this).on("keyup change", function () {
        if (
          agroecossistemaListTable.DataTable().column(i).search() !== this.value
        ) {
          agroecossistemaListTable
            .DataTable()
            .column(i)
            .search(this.value)
            .draw();
        }
      });
    } else {
      $(this).html("");
    }
  });
});
