var isSubmitting = {};
var onChangeHooks = [];
var calculosCallbacks = [];
let initComplete = {};
let datatablesManager = {};
window.onscroll = function () {
  scrollFunction();
};

function getStyle(
  colIdx,
  cellStyleBreakPoints,
  type,
  value,
  colHasFloats = false
) {
  // Title Style:
  // 1
  // Header styles:
  // 2 - even
  // 3 - odd
  // Data styles:
  // 4 - even (string)
  // 8 - even (float)
  // 7 - even (int)
  // 5 - odd (string)
  // 9 - odd (float)
  // 6 - odd (int)
  // Footer styles:
  // 11 - even (string)
  // 12 - even (float)
  // 16 - even (int)
  // 15 - odd (string)
  // 13 - odd (float)
  // 14 - odd (int)

  let evenStyle;
  let oddStyle;
  let evenNumberStyle;
  let oddNumberStyle;
  switch (type) {
    case "title":
      return 1;
    case "header":
      evenStyle = 2;
      oddStyle = 3;
      break;
    case "data":
      evenNumberStyle =
        !isNaN(parseFloat(value)) &&
        (parseFloat(value) !== parseInt(value) || colHasFloats)
          ? 8
          : 7;
      oddNumberStyle =
        !isNaN(parseFloat(value)) &&
        (parseFloat(value) !== parseInt(value) || colHasFloats)
          ? 9
          : 6;
      evenStyle =
        value === "" || isNaN(parseFloat(value)) ? 4 : evenNumberStyle;
      oddStyle = value === "" || isNaN(parseFloat(value)) ? 5 : oddNumberStyle;
      break;
    case "footer":
      evenNumberStyle =
        !isNaN(parseFloat(value)) &&
        (parseFloat(value) !== parseInt(value) || colHasFloats)
          ? 12
          : 16;
      oddNumberStyle =
        !isNaN(parseFloat(value)) &&
        (parseFloat(value) !== parseInt(value) || colHasFloats)
          ? 13
          : 14;
      evenStyle =
        value === "" || isNaN(parseFloat(value)) ? 11 : evenNumberStyle;
      oddStyle = value === "" || isNaN(parseFloat(value)) ? 15 : oddNumberStyle;
  }
  for (
    let breakPointIndex = 0;
    breakPointIndex < cellStyleBreakPoints.length;
    breakPointIndex++
  ) {
    const breakPointStart = cellStyleBreakPoints[breakPointIndex];
    const breakPointEnd =
      breakPointIndex < cellStyleBreakPoints.length - 1
        ? cellStyleBreakPoints[breakPointIndex + 1]
        : 90000;
    if (colIdx >= breakPointStart && colIdx < breakPointEnd) {
      return breakPointIndex % 2 === 0 ? evenStyle : oddStyle;
    }
  }
  return evenStyle;
}

function customizeXLS(xlsx, buttonEl, table) {
  // Get number of columns to remove last hidden index column.
  const headerTrList = $(table.header()).find("tr");
  const bodyTrList = $(table.body()).find("tr");
  const footerTrList = $(table.footer()).find("tr");
  const lastHeader = $(headerTrList[headerTrList.length - 1]);
  const noOfColumn = lastHeader.find(
    "th:not(.datatable-export-disabled)"
  ).length;
  const columnIndexesToSkip = [];
  lastHeader.find("th").each(function (index, element) {
    if ($(element).hasClass("datatable-export-disabled")) {
      columnIndexesToSkip.push(index);
    }
  });
  const rowSpansOfColumns = [];
  for (let i = 0; i < noOfColumn; i++) {
    rowSpansOfColumns.push([]);
  }
  const noOfExtraHeaderRow = $(table.header()).find(
    "tr:not(.tableDummyFix)"
  ).length;
  const allHeaderRowsLength = $(table.header()).find("tr").length;

  const sheet = xlsx.xl.worksheets["sheet1.xml"];
  const styles = xlsx.xl["styles.xml"];
  const defaultStyles = getDefaultXLSXStyles();
  $("styleSheet", styles).html(defaultStyles);

  // console.log(defaultStyles);
  const col = $("col", sheet);
  $(col[1]).attr("width", 20);

  const sheetData = $("sheetData", sheet).clone();
  $("sheetData", sheet).empty();

  const mergeCells = $("mergeCells", sheet);

  const cellStyleBreakPoints = [0];
  let rowIndex = 1;

  // Iterate each row in the sheet data.
  $(sheetData)
    .children()
    .each(function (rowElIndex, rowEl) {
      // rowElIndex = 0: Table name
      // rowElIndex = 1: last header TR before data
      const isTitle = rowElIndex === 0;
      const isLastHeader = rowElIndex === 1;
      const isFooter = rowElIndex === $(sheetData).children().length - 1;

      if (isLastHeader) {
        headerTrList.each(function (headerTrElIndex, headerTrEl) {
          const headerTr = $(headerTrEl);
          if (
            headerTr.hasClass("tableDummyFix") ||
            headerTr.find("h4.tableTitle").length > 0 ||
            headerTrElIndex === headerTrList.length - 1
          ) {
            return true;
          }

          const newHeaderRowEl = document.createElement("row");
          const newHeaderRow = $(newHeaderRowEl);
          newHeaderRow.attr("r", rowIndex);
          let colIdx = 0;
          while (
            colIdx < rowSpansOfColumns.length &&
            rowSpansOfColumns[colIdx].includes(rowIndex)
          ) {
            colIdx++;
          }
          headerTr
            .find("th")
            .each(function (headerTrColElIndex, headerTrColEl) {
              let colSpan = parseInt(headerTrColEl.getAttribute("colSpan"));
              for (
                let colCounter = colIdx;
                colCounter < colIdx + colSpan;
                colCounter++
              ) {
                if (columnIndexesToSkip.includes(colCounter)) {
                  colSpan--;
                }
              }
              let rowSpan = parseInt(headerTrColEl.getAttribute("rowSpan"));
              if (rowSpan > 1) {
                for (
                  let colCount = colIdx;
                  colCount < colIdx + colSpan;
                  colCount++
                ) {
                  for (
                    let rowCount = rowIndex + 1;
                    rowCount < realIndex + rowSpan;
                    rowCount++
                  ) {
                    rowSpansOfColumns[colCount].push(rowCount);
                  }
                }
              }

              const value = $(headerTrColEl).text();

              const newHeaderCellEl = document.createElement("c");
              const newHeaderCell = $(newHeaderCellEl);
              newHeaderCell.attr(
                "r",
                `${String.fromCharCode(65 + colIdx)}${rowIndex}`
              );
              newHeaderCell.attr("t", "inlineStr");

              if (
                rowIndex === 2 &&
                colSpan > 1 &&
                colIdx + colSpan <= noOfColumn
              ) {
                cellStyleBreakPoints.push(colIdx + colSpan);
              }
              newHeaderCell.attr(
                "s",
                getStyle(colIdx, cellStyleBreakPoints, "header", value)
              );

              const newTEl = document.createElement("t");
              const newT = $(newTEl);
              newT.attr("xml:space", "preserve");
              newT.text(value);

              const newIsEl = document.createElement("is");
              const newIs = $(newIsEl);
              newT.appendTo(newIs);
              newIs.appendTo(newHeaderCell);
              newHeaderCell.appendTo(newHeaderRow);

              if (colSpan > 1) {
                const colStart = String.fromCharCode(65 + colIdx);
                const colEnd = String.fromCharCode(65 + colIdx + colSpan - 1);
                const mergeStart = `${colStart}${rowIndex}`;
                const mergeEnd = `${colEnd}${rowIndex}`;
                mergeCells.append(
                  $.parseXML(`<mergeCell ref="${mergeStart}:${mergeEnd}" />`)
                    .documentElement
                );
              }

              colIdx += colSpan;
            });

          const newRowElHTML = newHeaderRow[0].outerHTML;
          $("sheetData", sheet).append(newRowElHTML);
          rowIndex++;
        });
      }

      // add existing sheet row, normally:
      let colHasFloats = false;
      const row = $(rowEl.outerHTML);
      row.attr("r", rowIndex);
      if (isTitle) {
        row.attr("ht", 25);
      }
      let colCount = 1;
      row.children().each(function (cellElIndex, cellEl) {
        const cell = $(cellEl);
        let rc = cell.attr("r");
        const cellColIdx = rc.replace(/\d+$/, "").charCodeAt(0) - 65;
        rc = rc.replace(/\d+$/, "") + rowIndex;
        if (rowElIndex > 1 && !isFooter) {
          if ($(bodyTrList[rowElIndex - 2]).find("td")[cellColIdx]) {
            const valueStr = $(
              $(bodyTrList[rowElIndex - 2]).find("td")[cellColIdx]
            ).text();
            const value = valueStr.replace(/\./g, "").replace(/\,/g, ".");
            if (value !== "" && !isNaN(parseFloat(value))) {
              cell.html(`<v>${parseFloat(value)}</v>`);
              cell.removeAttr("t");
              colHasFloats = valueStr.indexOf(",") > -1;
            }
          }
        }

        if (isFooter) {
          if ($(footerTrList[0]).find("th")[cellColIdx]) {
            const valueStr = $(
              $(footerTrList[0]).find("th")[cellColIdx]
            ).text();
            const value = valueStr.replace(/\./g, "").replace(/\,/g, ".");
            if (value !== "" && !isNaN(parseFloat(value))) {
              cell.html(`<v>${parseFloat(value)}</v>`);
              cell.removeAttr("t");
              colHasFloats = valueStr.indexOf(",") > -1;
            }
          }
        }

        cell.attr("r", rc);
        if (columnIndexesToSkip.includes(cellElIndex)) {
          cell.html("");
        }
        let type;
        if (isTitle) {
          type = "title";
        } else if (isLastHeader) {
          type = "header";
        } else if (isFooter) {
          type = "footer";
        } else {
          type = "data";
        }
        cell.attr(
          "s",
          getStyle(
            cellColIdx,
            cellStyleBreakPoints,
            type,
            cell.text(),
            colHasFloats
          )
        );
        colCount++;
      });

      // Get the row HTML and append to sheetData.
      const newRowElHTML = row[0].outerHTML;
      $("sheetData", sheet).append(newRowElHTML);
      rowIndex++;
    });

  const newMergeCellsCount = mergeCells.find("mergeCell").length;
  mergeCells.attr("count", newMergeCellsCount);
}

function customizePDF(pdfDocument, buttonEl, table) {
  let contentIndex = null;
  for (let i = 0; i < pdfDocument.content.length; i++) {
    if (pdfDocument.content[i].table) {
      contentIndex = i;
      break;
    }
  }
  if (contentIndex === null) {
    return;
  }
  let headerRows = [];
  const headerTrList = $(table.header()).find("tr");
  const lastHeader = $(headerTrList[headerTrList.length - 1]);
  const noOfColumn = lastHeader.find(
    "th:not(.datatable-export-disabled)"
  ).length;
  const columnIndexesToSkip = [];
  lastHeader.find("th").each(function (index, element) {
    if ($(element).hasClass("datatable-export-disabled")) {
      columnIndexesToSkip.push(index);
    }
  });
  const rowSpansOfColumns = [];
  for (let i = 0; i < noOfColumn; i++) {
    rowSpansOfColumns.push([]);
  }
  const noOfExtraHeaderRow = $(table.header()).find(
    "tr:not(.tableDummyFix)"
  ).length;
  const allHeaderRowsLength = $(table.header()).find("tr").length;
  pdfDocument.content[contentIndex].table.headerRows = noOfExtraHeaderRow;
  let realIndex = 1;
  for (let i = 1; i < allHeaderRowsLength; i++) {
    if (
      $(table.header()).find(`tr:nth-child(${i})`).hasClass("tableDummyFix")
    ) {
      continue;
    }
    let headerRow = [];
    let colIdx = 0;
    while (
      colIdx < rowSpansOfColumns.length &&
      rowSpansOfColumns[colIdx].includes(realIndex)
    ) {
      headerRow.push({});
      colIdx++;
    }
    $(table.header())
      .find(`tr:nth-child(${i})>th`)
      .each(function (index, element) {
        let colSpan = parseInt(element.getAttribute("colSpan"));
        for (let col = colIdx; col < colIdx + colSpan; col++) {
          if (columnIndexesToSkip.includes(col)) {
            colSpan--;
          }
        }
        let rowSpan = parseInt(element.getAttribute("rowSpan"));
        if (rowSpan > 1) {
          for (let col = colIdx; col < colIdx + colSpan; col++) {
            for (let row = realIndex + 1; row < realIndex + rowSpan; row++) {
              rowSpansOfColumns[col].push(row);
            }
          }
        }
        const elementText =
          $(element).find("h4.tableTitle").length > 0
            ? $(element).find("h4.tableTitle").text().trim()
            : $(element).text().trim();
        headerRow.push({
          text: elementText,
          style: "tableHeader",
          colSpan,
          rowSpan,
        });
        colIdx++;
        for (let j = 0; j < colSpan - 1; j++) {
          headerRow.push({});
          colIdx++;
        }
        while (
          colIdx < rowSpansOfColumns.length &&
          rowSpansOfColumns[colIdx].includes(i)
        ) {
          headerRow.push({});
          colIdx++;
        }
        realIndex++;
      });

    headerRows.push(headerRow);
  }
  for (let i = 0; i < headerRows.length; i++) {
    pdfDocument.content[contentIndex].table.body.unshift(
      headerRows[headerRows.length - 1 - i]
    );
  }
  pdfDocument.content[contentIndex].table.widths = Array(
    pdfDocument.content[contentIndex].table.body[0].length + 1
  )
    .join("*")
    .split("");

  let fontSize = 12;
  if (noOfColumn > 16) {
    fontSize = 4.5;
  } else if (noOfColumn > 14) {
    fontSize = 6.5;
  } else if (noOfColumn > 10) {
    fontSize = 8;
  } else if (noOfColumn > 6) {
    fontSize = 10;
  }
  pdfDocument.styles.tableHeader.fontSize = fontSize;
  pdfDocument.styles.tableFooter.fontSize = fontSize;
  pdfDocument.defaultStyle.fontSize = fontSize;
  pdfDocument.pageMargins = [20, 20, 20, 20];
}

function updateMarkItUpPreviews($table) {
  $table.find(".markItUpButton.preview").each((i, el) => {
    $(el).mouseup();
  });
  setTimeout(() => {
    $table.find(".markItUpContainer").each((i, containerEl) => {
      $(containerEl)
        .find(".markItUpPreviewFrame")
        .each((j, previewFrameEl) => {
          if (j > 0) {
            $(previewFrameEl).remove();
          }
        });
    });
  }, [10]);
}

function alerta($table, message, error = false) {
  let preTable = $table.prev(".preTable");
  if (preTable.length === 0) {
    preTable = $table
      .parents(`#${$table.attr("id")}_wrapper`)
      .prev(".preTable");
  }
  if (preTable.length === 0) {
    return;
  }
  const notifyMessage = preTable.find(".notify");
  const notifyWrapper = preTable.find(".notify-wrapper");
  notifyMessage.html(message);
  notifyWrapper.toggleClass("alert-danger", error);
  notifyWrapper.toggleClass("alert-success", !error);
  notifyWrapper.fadeIn(1500);
  setTimeout(function () {
    notifyWrapper.fadeOut(1500);
  }, 1500);
}

function addToInitComplete(id, fn) {
  if (!initComplete.hasOwnProperty(id)) {
    initComplete[id] = [];
  }
  initComplete[id].push(fn);
}
function generateInitComplete(id) {
  if (initComplete.hasOwnProperty(id) && initComplete[id].length > 0) {
    return function (settings, json) {
      initComplete[id].forEach(function (fn) {
        fn(settings, json);
      });
    };
  }
  return null;
}

function toggleSideBar(sidebarMin) {
  var sideBarIsHidden =
    typeof sidebarMin == "undefined"
      ? $("#sidebar-wrapper").hasClass("sidebar-min")
      : sidebarMin == "false";
  $(
    "#sidebar-wrapper, #page-content-wrapper, .list-group-embaixo, #footer"
  ).toggleClass("sidebar-min", !sideBarIsHidden);
  if (typeof sidebarMin == "undefined") {
    localStorage.setItem("sidebar-min", sideBarIsHidden ? "false" : "true");
  }

  $(".datatable.fit-to-page").each(function (i, table) {
    const tableId = table.id;
    if (tableId && tableId in datatablesManager) {
      setTimeout(function () {
        datatablesManager[tableId].columns.adjust();
      }, 300);
    }
  });
}

function toggleSubmitForm(formEl, forceToggle = null) {
  var form = $(formEl);
  var f = FormChanges(formEl);
  var shouldDisable =
    f.length == 0 && (!forceToggle || forceToggle == "disable");
  form.toggleClass("form-disabled", shouldDisable);
  return shouldDisable;
}

function removeMoneySelectAndNumber(form) {
  // This is to prevent moneyfields to be in type "number" which destroys the formatting:
  form.find("td.moneyfield select").hide();
  form.find("td.moneyfield input[type=number]").each(function () {
    $(this).get(0).type = "text";
  });
}

function cleanValues(tr) {
  tr.find(":input:not([type=hidden])").each(function (i, inputEl) {
    var input = $(inputEl);
    if (input.parent("td").hasClass("decimalfield")) {
      //addMask(input);
    } else if (input.siblings("span").first().hasClass("select2")) {
      var select2Span = input
        .siblings("span")
        .first()
        .find(".select2-selection__rendered")
        .first();
      if (select2Span) {
        const placeholder = input.data("placeholder")
          ? input.data("placeholder")
          : "";
        select2Span.html(
          `<span class="select2-selection__placeholder">${placeholder}</span>`
        );
      }
    }

    if (inputEl.className.match(/.*select2.*/)) {
      input.val("").trigger("change");
      input.prop("defaultValue", "");
    } else if (["checkbox", "radio"].includes(input.attr("type"))) {
      input.prop("checked", false);
    } else if (
      input.parent("td").hasClass("decimalfield") ||
      input.parent("td").hasClass("moneyfield")
    ) {
      input.val(
        input.prop("defaultValue")
          ? formatNumber(parseFloat(input.prop("defaultValue")))
          : ""
      );
    } else {
      input.val(input.prop("defaultValue") ? input.prop("defaultValue") : "");
    }
  });
}

function formatInputVal(inputEl, direction = "toText") {
  if (
    $(inputEl).parents("td.decimalfield").length == 0 &&
    $(inputEl).parents("td.moneyfield").length == 0
  ) {
    return;
  }
  var input = $(inputEl);
  var val = input.val();
  if (direction == "toText") {
    val = formatNumber(val, "toFloat");
    if (isNaN(val)) {
      return;
    }
  }
  var valRet = $(inputEl).parents("td").hasClass("td_qtde")
    ? formatQtde(val, direction)
    : formatNumber(val, direction);
  input.val(valRet);
}

function formatNumber(number, direction = "toText") {
  if (isNaN(parseFloat(number))) {
    return direction == "toText" ? "" : 0.0;
  }
  switch (direction) {
    case "toText":
      return number.toLocaleString(gettext("pt-BR"), {
        minimumFractionDigits: 2,
      });
    case "toFloat":
      return parseFloat(number.replace(".", "").replace(",", "."));
  }
}

function formatQtde(number, direction = "toText") {
  if (isNaN(parseFloat(number))) {
    return direction == "toText" ? "0" : 0.0;
  }
  switch (direction) {
    case "toText":
      return parseFloat(number) == 0.0
        ? "0"
        : number.toLocaleString(gettext("pt-BR"), {
            minimumFractionDigits: 0,
            maximumFractionDigits: 1,
          });
    case "toFloat":
      return parseFloat(number.replace(".", "").replace(",", "."));
  }
}

function scrollFunction() {
  $("#gotoTop").toggleClass(
    "d-none",
    document.body.scrollTop < 20 && document.documentElement.scrollTop < 20
  );
}

function gotoTop() {
  $("html, body").animate({ scrollTop: 0 }, 300);
}

function filtraTextoExportadoR(el, ret) {
  if (
    $(el).children().length > 0 &&
    !$(el).is("select") &&
    !$(el).hasClass("select2-container")
  ) {
    $(el)
      .children()
      .each(function (i, elSub) {
        const subs = filtraTextoExportadoR(elSub, ret);
        for (let sub of subs) {
          if (!ret.includes(sub)) {
            ret.push(sub);
          }
        }
      });
    return ret;
  }

  const $el = $(el)[0].id ? $(`#${$(el)[0].id}`) : $(el);

  if ($(el)[0].id && $el.parents("tr").is(":hidden")) {
    return [];
  }

  if ($el.is("select") && $el.find(":selected").text().trim()) {
    // console.log('SELECT', $el.find(':selected').text().trim());
    ret.push($el.find(":selected").text().trim());
  } else if ($el.is("input") && $el.attr("type") === "checkbox") {
    // console.log('CHECKBOX', $el);
    ret.push($el.is(":checked") ? "Sim" : "Não");
  } else if ($el.is("input") && $el.attr("type") !== "hidden" && $el.val()) {
    // console.log('INPUT', $el.val());
    ret.push($el.val());
  } else if ($el.text().trim() && !$el.hasClass("select2-container")) {
    // console.log('TEXT', $el.text().trim());
    ret.push($el.text().trim());
  } else {
    // console.log('Recusado!', $el);
  }
  return ret;
}

function filtraTextoExportado(data, node) {
  try {
    if ($(data).length === 0) {
      const tmp = document.createElement("div");
      tmp.innerHTML = data;
      const text = tmp.textContent || tmp.innerText || "";
      return node.nodeType !== 1 ||
        !node.getAttribute("datatable-export-disabled")
        ? text.replace(/\s+/g, " ").trim()
        : "";
    }

    let ret = [];
    $(data).each(function (i, d) {
      const sub = filtraTextoExportadoR(d, []);
      for (let s of sub) {
        if (s) {
          ret.push(s);
        }
      }
    });
    return ret.join("\n");
  } catch (e) {
    const tmp = document.createElement("div");
    tmp.innerHTML = data;
    const text = tmp.textContent || tmp.innerText || "";
    return node.nodeType !== 1 ||
      !node.getAttribute("datatable-export-disabled")
      ? text.replace(/\s+/g, " ").trim()
      : "";
  }
}

var dataTableDefaultOptions = {
  language: {
    sEmptyTable: gettext("Nenhum registro encontrado"),
    sInfo: gettext("Mostrando de _START_ até _END_ de _TOTAL_ registros"),
    sInfoEmpty: gettext("Mostrando 0 até 0 de 0 registros"),
    sInfoFiltered: gettext("(Filtrados de _MAX_ registros)"),
    sInfoPostFix: "",
    sInfoThousands: ".",
    sLengthMenu: gettext("_MENU_ resultados por página"),
    sLoadingRecords: gettext("Carregando..."),
    sProcessing: gettext("Processando..."),
    sZeroRecords: gettext("Nenhum registro encontrado"),
    sSearch: "",
    searchPlaceholder: gettext("Pesquisar"),
    oPaginate: {
      sNext: gettext("Próximo"),
      sPrevious: gettext("Anterior"),
      sFirst: gettext("Primeiro"),
      sLast: gettext("Último"),
    },
    oAria: {
      sSortAscending: ": " + gettext("Ordenar colunas de forma ascendente"),
      sSortDescending: ": " + gettext("Ordenar colunas de forma descendente"),
    },
  },
  paging: true,
  searching: true,
  ordering: true,
  bInfo: false,
  responsive: false,
  columnDefs: [
    { targets: "no-sort", orderable: false },
    { targets: "no-search", searchable: false },
  ],
};
var dataTableExtraOptions = {
  "fit-to-page": {
    scrollX: true,
    scrollY: "45vh",
    scrollCollapse: true,
    paging: false,
  },
  keyTable: {
    keys: {
      columns: ":visible :not(.datatable-export-disabled)",
    },
  },
  "fit-to-width": {
    scrollX: true,
    scrollCollapse: true,
  },
  "fit-dropdown-menu": {
    scrollY: "20rem",
  },
  "add-export-buttons": {
    dom: "Bfrtip",
    buttons: ["excelHtml5", "csvHtml5", "pdfHtml5"],
  },
  "add-export-buttons-collapsed": {
    dom: "Bfrtip",
    buttons: [
      {
        extend: "collection",
        text: "",
        buttons: [
          {
            text: "Excel .xls",
            extend: "excelHtml5",
            footer: true,
            customize: customizeXLS,
            exportOptions: {
              format: {
                body: function (data, row, column, node) {
                  return filtraTextoExportado(data, node);
                },
              },
              columns: ":visible :not(.datatable-export-disabled)",
            },
          },
          {
            text: "Tabela .csv",
            extend: "csvHtml5",
            footer: true,
            exportOptions: {
              format: {
                body: function (data, row, column, node) {
                  return filtraTextoExportado(data, node);
                },
              },
              columns: ":visible :not(.datatable-export-disabled)",
            },
          },
          {
            text: ".PDF",
            extend: "pdfHtml5",
            footer: true,
            customize: customizePDF,
            exportOptions: {
              format: {
                body: function (data, row, column, node) {
                  return filtraTextoExportado(data, node);
                },
              },
              columns: ":visible :not(.datatable-export-disabled)",
            },
          },
        ],
      },
    ],
  },
  "add-export-buttons-collapsed-landscape": {
    dom: "Bfrtip",
    buttons: [
      {
        extend: "collection",
        text: "",
        buttons: [
          {
            text: "Excel .xls",
            extend: "excelHtml5",
            footer: true,
            customize: customizeXLS,
            exportOptions: {
              format: {
                body: function (data, row, column, node) {
                  return filtraTextoExportado(data, node);
                },
              },
              columns: ":visible :not(.datatable-export-disabled)",
            },
          },
          {
            text: "Tabela .csv",
            extend: "csvHtml5",
            footer: true,
            exportOptions: {
              format: {
                body: function (data, row, column, node) {
                  return filtraTextoExportado(data, node);
                },
              },
            },
            columns: ":visible :not(.datatable-export-disabled)",
          },
          {
            text: ".PDF",
            extend: "pdfHtml5",
            footer: true,
            orientation: "landscape",
            pageSize: "A4",
            customize: customizePDF,
            exportOptions: {
              format: {
                body: function (data, row, column, node) {
                  return filtraTextoExportado(data, node);
                },
              },
              columns: ":visible :not(.datatable-export-disabled)",
              rows: ":not(.datatable-export-disabled)",
            },
          },
        ],
      },
    ],
  },
  "no-paging": {
    paging: false,
  },
  "no-searching": {
    searching: false,
  },
  "no-ordering": {
    ordering: false,
  },
  "no-sort-on-start": {
    order: [],
  },
  "fixed-headers": {
    fixedHeader: {
      headerOffset: 48,
    },
  },
  "fixed-columns": {
    fixedColumns: true,
  },
  "datatable-indicators": {
    dom: "Bt",
    paging: false,
    searching: false,
    ordering: false,
    bInfo: false,
    responsive: false,
    buttons: [
      {
        extend: "excelHtml5",
        footer: true,
        customize: customizeXLS,
        exportOptions: {
          format: {
            body: function (data, row, column, node) {
              return filtraTextoExportado(data, node);
            },
          },
          columns: ":visible :not(.datatable-export-disabled)",
        },
      },
      {
        extend: "csvHtml5",
        footer: true,
        exportOptions: {
          format: {
            body: function (data, row, column, node) {
              return filtraTextoExportado(data, node);
            },
          },
          columns: ":visible :not(.datatable-export-disabled)",
        },
      },
      {
        extend: "pdfHtml5",
        footer: true,
        customize: customizePDF,
        exportOptions: {
          format: {
            body: function (data, row, column, node) {
              return filtraTextoExportado(data, node);
            },
          },
          columns: ":visible :not(.datatable-export-disabled)",
        },
      },
      {
        extend: "colvis",
        text: gettext("Ocultar/mostrar colunas"),
      },
    ],
  },
  "datatable-indicators-nocolvis": {
    dom: "Bt",
    paging: false,
    searching: false,
    ordering: false,
    bInfo: false,
    responsive: false,
    buttons: [
      {
        extend: "excelHtml5",
        footer: true,
        customize: customizeXLS,
        exportOptions: {
          format: {
            body: function (data, row, column, node) {
              return filtraTextoExportado(data, node);
            },
          },
          columns: ":visible :not(.datatable-export-disabled)",
        },
      },
      {
        extend: "csvHtml5",
        footer: true,
        exportOptions: {
          format: {
            body: function (data, row, column, node) {
              return filtraTextoExportado(data, node);
            },
          },
          columns: ":visible :not(.datatable-export-disabled)",
        },
      },
      {
        extend: "pdfHtml5",
        footer: true,
        customize: customizePDF,
        exportOptions: {
          format: {
            body: function (data, row, column, node) {
              return filtraTextoExportado(data, node);
            },
          },
          columns: ":visible :not(.datatable-export-disabled)",
        },
      },
    ],
  },
  "has-markitup": {
    drawCallback: (settings) => {
      updateMarkItUpPreviews($(settings.nTable));
    },
  },
};

function addMask(input) {
  input.mask("#.##0,00", {
    reverse: true,
    onKeyPress: function (k, el) {
      var r = "";
      if (k.length == 1) {
        r = "0,0" + k.toString();
      } else if (k.length == 2) {
        r = "0," + k.toString();
      } else if (k[0] == 0) {
        r = k.substr(1);
      }
      if (r) {
        $(el.target).val(r);
      }
    },
  });
  input.prop("defaultValue", input.val());
}

/**
 * Alterna o modo de Edição da tabela
 * @method toggleEditar
 * @param  {object}     table          A tabela
 * @param  {boolean}     [tornarEditavel=null] deve ficar editável?
 */
function toggleEditar(tables, tornarEditavel = null) {
  if (tornarEditavel === null) {
    tornarEditavel = !tables.hasClass("modoEdicao");
  }
  tables.each(function (k, table) {
    const $table = $(table);
    const $form = $table.parents("form");
    const tableId =
      table.id ||
      $table
        .attr("class")
        .split(" ")
        .filter((item) => item.includes("table_atributo_"))[0];

    let preTable = $($table.siblings(".preTable"));
    if (preTable.length == 0) {
      preTable = $table.parents(".dataTables_wrapper").siblings(".preTable");
    }
    $table.find("thead .hideOnToggleEditar").each(function (j, el) {
      $(el).css("color", tornarEditavel ? "inherit" : "transparent");
    });
    $table
      .find("tbody")
      .find(
        'select, textarea, input, a.btn, button[type="submit"], .input-group.date'
      )
      .each(function (j, el) {
        if ($(el).parents(".noToggleEditar").length == 0) {
          if ($(el).is("a") || $(el).hasClass("input-group")) {
            $(el).toggleClass("disabled", !tornarEditavel);
          } else {
            if (tornarEditavel) {
              $(el).removeAttr("disabled");
            } else {
              $(el).attr("disabled", "disabled");
            }
          }
        }
      });
    $table.toggleClass("modoEdicao", tornarEditavel);
    preTable.toggleClass("modoEdicao", tornarEditavel);
    if (tableId in datatablesManager) {
      datatablesManager[tableId].columns.adjust();
    }

    if ($(`#buttonsRow_${tableId}`).length === 0) {
      $(`<div id='buttonsRow_${tableId}' class='buttonsRow'></div>`).appendTo(
        "body"
      );
    }

    let real_tfoot = $(`#${tableId}_wrapper .dataTables_scrollFoot tfoot`)
      .length
      ? $(`#${tableId}_wrapper .dataTables_scrollFoot tfoot`)
      : $table.find("tfoot");

    if (tornarEditavel) {
      $(`#buttonsRow_${tableId} .dynamic-form-add`).appendTo(real_tfoot);
      real_tfoot.find("a.btn").removeClass("disabled");
    } else {
      $form.find(".dynamic-form-add").appendTo(`#buttonsRow_${tableId}`);
    }
  });
}

function formChangesListener() {
  $(".monitor-formchanges")
    .find("tbody")
    .on("change", "input,select,textarea", function () {
      for (var i = 0; i < onChangeHooks.length; i++) {
        eval(onChangeHooks[i] + "(this)");
      }
      const tableId = $(this).parents("table").attr("id");
      toggleSubmitForm(this.form);
    });
  $(".monitor-formchanges")
    .find("tbody")
    .on("input", "textarea", function () {
      const tableId = $(this).parents("table").attr("id");
      toggleSubmitForm(this.form);
    });
  $(".decimalfield input, .moneyfield input").on("focus", function (e) {
    $(this).select();
  });
}

function autoCenterNavTabs() {
  const ul = ".nav_bar_titles ul";
  const x = gsap.getProperty(ul, "x");
  const nTabs = $(".nav_bar_titles .nav-link").length;
  const width = gsap.getProperty(".nav_bar_titles", "width");
  const scrollWidth = $(".nav_bar_titles")[0].scrollWidth;
  const tabWidth = width / nTabs;
  let nActive = 0;
  $(".nav_bar_titles .nav-link").each(function (i, el) {
    if ($(el).hasClass("active")) {
      nActive = i;
      return false;
    }
  });
  let newX = nActive * tabWidth;
  if (newX < width / 2) {
    gsap.to(ul, { duration: 0.8, x: 0 });
  } else if (width - scrollWidth - newX > 0) {
    gsap.to(ul, { duration: 0.8, x: -newX });
  } else {
    gsap.to(ul, { duration: 0.8, x: width - scrollWidth });
  }
}

function initModalRightListeners(queryset) {
  $(".updateObject").on("click", function (el) {
    const pk = $(this).data("pk");
    if (pk) {
      for (obj of queryset) {
        if (obj.pk == pk) {
          $("#id_update_pk").val(pk);
          let href = $("#modal_right_delete_btn").attr("href");
          let parts = href.split("/");
          parts[parts.length - 2] = pk;
          href = parts.join("/");
          $("#modal_right_delete_btn").attr("href", href);
          for (key in obj.fields) {
            const formEl = $(`[name=${key}]`);
            if (formEl.length) {
              value = obj.fields[key];
              assignValueToFormElement(formEl, value);
            }
          }
          break;
        }
      }
    } else {
      $("#id_update_pk").val(null);
      const obj = queryset[0];
      for (key in obj.fields) {
        const formEl = $(`#modal-right-wrapper #id_${key}`);
        if (formEl.length) {
          assignValueToFormElement(formEl, null);
        }
      }
    }
  });
}

function assignValueToFormElement(formEl, value) {
  type = formEl.attr("type") ? formEl.attr("type") : "textarea";

  if (type == "radio") {
    formEl.each(function (i, el) {
      $(el).prop("checked", $(el).val() == value);
    });
    return;
  }

  switch (type) {
    case "text":
    case "select":
      formEl.val(value);
      break;
    case "number":
      formEl.val(value);
      break;
    case "checkbox":
      formEl.prop("checked", value);
      break;
    case "textarea":
      formEl.text(value);
      break;
  }
}

$(document).ready(function () {
  if ($("#sidebar-wrapper").length == 0) {
    $("#footer, #page-content-wrapper").addClass("no-sidebar");
  } else if (localStorage.getItem("sidebar-min") !== null) {
    toggleSideBar(localStorage.getItem("sidebar-min"));
  }

  // ajudaDaTela
  if ($("#ajudaDaTela").length) {
    const localStorageAjudaKey = $("#ajudaDaTela").data("ajuda-storage-key");
    $(".ajudaDaTelaBtn").hide();
    if (localStorage.getItem(localStorageAjudaKey)) {
      $("#ajudaDaTela").hide();
      $(".ajudaDaTelaBtn").first().show();
    }
    $("#ajudaDaTela .close, .ajudaDaTelaBtn").on("click", function (e) {
      if ($("#ajudaDaTela").is(":visible")) {
        localStorage.setItem(localStorageAjudaKey, "1");
        $(".ajudaDaTelaBtn").first().show();
      } else {
        localStorage.removeItem(localStorageAjudaKey);
        $(".ajudaDaTelaBtn").hide();
      }
      $("#ajudaDaTela").fadeToggle();
    });
  } else {
    $(".ajudaDaTelaBtn").hide();
  }

  // Modal right
  $(".modal-right-close").on("click", function () {
    $(".modal-right").fadeOut();
  });
  if ($("#modal-right-wrapper").length > 0) {
    $(".modal-right-open").on("click", function () {
      $("#modal-right-wrapper .modal-right-title").text($(this).data("title"));
      $("#modal-right-wrapper").fadeIn();
    });
  }

  // Listener for subsistemaselector
  $("#subsistemaSelector").on("change", function (e) {
    window.location = this.value;
  });

  // Operations on tabelaEditavel table:
  $("table.tabelaEditavel").each(function (a, table) {
    const $table = $(table);
    const tableId = table.id;

    if (!tableId) {
      return;
    }

    const tfoot = $(`#${tableId}_wrapper .dataTables_scrollFoot tfoot`).length
      ? $(`#${tableId}_wrapper .dataTables_scrollFoot tfoot`)
      : $table.find("tfoot");
    const tbody = $table.find("tbody");

    // If table is a formset, remove last row:
    // console.log($('.errorlist').length);
    if (
      $(".errorlist").length === 0 &&
      !$table.hasClass("has-errors") &&
      $table.find("tr.dynamic-form-add").length > 0
    ) {
      const numRows = tbody.find("tr:not(.dynamic-form-add)").length;
      if (numRows > 1) {
        tbody
          .find("tr:not(.dynamic-form-add)")
          .last()
          .find(".button-delete")
          .trigger("click");
      }
    } else {
      const numRows = tbody.find("tr:not(.dynamic-form-add)").length;
      if (numRows > 1) {
        tbody.find("tr:not(.dynamic-form-add)").last().show();
      }
    }

    const buttonNew =
      $(`#${tableId}_wrapper .dataTables_scrollFoot tfoot`).length > 0
        ? $(`#${tableId}_wrapper .button-new`)
        : $table.find(".button-new");

    buttonNew.click(function (e) {
      const scrollBody = $table.parent();
      scrollBody.animate({ scrollTop: scrollBody.get(0).scrollHeight });

      removeMoneySelectAndNumber($table);

      // cleanValues(tfoot.find(".formset_row").last());
      // tfoot.find(".formset_row").last().appendTo(tbody);
      formChangesListener();
      if (tableId in datatablesManager) {
        datatablesManager[tableId].columns.adjust();
      }
    });

    let colCount = 0;
    $table.find("thead tr:nth-child(1) th").each(function () {
      if ($(this).attr("colspan")) {
        colCount += parseInt($(this).attr("colspan"));
      } else {
        colCount++;
      }
    });

    $table.find(".dynamic-form-add").appendTo(tfoot);
    $table.find(".dynamic-form-add td").attr("colspan", colCount);

    // If there are errors, let table editable!
    if (
      $(".errorlist").length > 0 ||
      $table.hasClass("has-errors") ||
      $table.hasClass("force_edit")
    ) {
      toggleEditar($table, true);
      return;
    }
    if ($table.hasClass("edit-disabled")) {
      toggleEditar($table, false);
      return;
    }

    // Check if table is entirely empty:
    let vazio = true;
    tbody.find("select, textarea, input").each(function (i, el) {
      if ($(el).is("textarea")) {
        if ($(el).text() != "") {
          vazio = false;
          return false;
        }
      } else {
        if ($(el).val() != "" && $(el).attr("type") != "hidden") {
          vazio = false;
          return false;
        }
      }
    });
    toggleEditar($table, vazio);
  });

  $(".toggleEditar").on("click", function () {
    tables = $("." + $(this).attr("target"));
    toggleEditar(tables);
  });

  // executes all calcs:
  setTimeout(function () {
    for (var i = 0; i < calculosCallbacks.length; i++) {
      eval(calculosCallbacks[i] + "()");
    }
  }, 1000);

  // monitor changes in form
  formChangesListener();

  $(".decimalfield_noniput, .moneyfield_noniput").each(function (i, el) {
    var val = parseFloat($(el).text().replace(",", "."));
    if (!isNaN(val)) {
      minFields = val > 0 && val < 0.01 ? 4 : 2;
      $(el).text(
        val.toLocaleString(gettext("pt-BR"), {
          minimumFractionDigits: minFields,
        })
      );
    }
  });
  $(".decimalfield_noniput_currency").each(function (i, el) {
    var val = parseFloat($(el).text().replace(",", "."));
    if (!isNaN(val)) {
      $(el).text(
        val.toLocaleString(gettext("pt-BR"), {
          currency: gettext("BRL"),
          style: "currency",
        })
      );
    }
  });
  $("form.convert-numbers-to-float").each(function (i, formEl) {
    var form = $(formEl);

    removeMoneySelectAndNumber(form);

    form.submit(function (e) {
      if (!form.hasClass("form-disabled")) {
        if (isSubmitting[formEl]) {
          e.preventDefault();
          return;
        }
        isSubmitting[formEl] = true;
        $(".decimalfield input, .moneyfield input").each(function (i, el) {
          $(el).removeAttr("disabled");
          var clean = $(el).parents("td").hasClass("td_qtde")
            ? $(el).val().replace(/\./g, "").replace(",", ".")
            : $(el).val().replace(/\./g, "").replace(",", ".");
          $(el).val(clean);
        });
      }
    });
    form
      .find(".decimalfield input, .moneyfield input")
      .each(function (i, inputEl) {
        let valRaw = $(inputEl).val();
        if (typeof valRaw === "string") {
          const valRawAr = valRaw.split(String.fromCharCode(160));
          valRaw = valRawAr.length === 1 ? valRawAr[0] : valRawAr[1];
        }

        if (!isNaN(parseFloat(valRaw))) {
          var valStr = $(inputEl).parents("td").hasClass("td_qtde")
            ? formatQtde(parseFloat(valRaw), "toText")
            : formatNumber(parseFloat(valRaw), "toText");
          $(inputEl).val(valStr);
          $(inputEl).prop("defaultValue", valStr);
        }
        if (i == 0) {
          onChangeHooks.push("formatInputVal");
        }
      });
  });

  $("#sidebar-button").click(function (e) {
    e.preventDefault();
    toggleSideBar();
  });
  $(".list-group-item.disabled").click(function (e) {
    e.preventDefault();
    alert($(this).data("alert-text"));
  });
  $(".href-button").click(function (e) {
    e.preventDefault();
    if ($(this).data("target-url")) {
      window.location = $(this).data("target-url");
    }
  });
  if ($(".datatable").length > 0) {
    $(".datatable").each(function (index, el) {
      const table = $(el);
      const options = $.extend(true, {}, dataTableDefaultOptions);
      const extraOptions = $.extend(true, {}, dataTableExtraOptions);
      const tableId = table.attr("id")
        ? table.attr("id")
        : `datatable_${index}`;
      for (const htmlClass in extraOptions) {
        if (table.hasClass(htmlClass)) {
          if (extraOptions.hasOwnProperty(htmlClass)) {
            var subOptions = $.extend({}, extraOptions[htmlClass]);
            for (var prop in extraOptions[htmlClass]) {
              if (subOptions.hasOwnProperty(prop)) {
                options[prop] = extraOptions[htmlClass][prop];
              }
            }
            if (options.buttons) {
              let buttons = options.buttons;
              if (options.buttons[0].hasOwnProperty("buttons")) {
                buttons = options.buttons[0].buttons;
              }
              for (let i = 0; i < buttons.length; i++) {
                if (buttons[i].hasOwnProperty("exportOptions")) {
                  if (table.attr("datatable-title")) {
                    buttons[i].title = table.attr("datatable-title");
                  } else if (table.find("caption")) {
                    buttons[i].title = table.find("caption").first().text();
                  }
                  if (table.attr("datatable-filename")) {
                    buttons[i].filename = table.attr("datatable-filename");
                  }
                }
              }
            }
          }
        }
      }

      if (
        table.hasClass("add-export-buttons-collapsed") ||
        table.hasClass("add-export-buttons-collapsed-landscape")
      ) {
        addToInitComplete(tableId, function (settings, json) {
          const buttons = $(`#${tableId}_wrapper .dt-buttons`);
          buttons.find(".buttons-collection").trigger("click");
          setTimeout(function () {
            if (!buttons.find(".dt-button-collection").length) {
              buttons.find(".buttons-collection").trigger("click");
              return;
            }
            const target = `.exportButtons_${tableId}`;
            buttons
              .find(".dt-button-collection .dropdown-menu")
              .children()
              .appendTo(target)
              // .addClass('dropdown-item')
              .removeClass("dt-button")
              .attr("role", "button");
            buttons.hide();
          }, 100);
        });
      }
      if (table.hasClass("search-is-title")) {
        addToInitComplete(tableId, function (settings, json) {
          $(`#${tableId}_wrapper .tableTitle`).replaceWith(
            $(`#${tableId}_filter`)
          );
        });
      }

      opInitComplete = generateInitComplete(tableId);

      if (opInitComplete) {
        options.initComplete = opInitComplete;
      }

      setTimeout(function () {
        datatablesManager[tableId] = table.DataTable(options);

        if (table.hasClass("keyTable")) {
          datatablesManager[tableId]
            .on("key-focus", function (e, datatable, cell, originalEvent) {
              if (!$(cell.node()).is(":visible")) {
                if ($(cell.node()).parents("tr").is(":last-child")) {
                  datatablesManager[tableId].keys.move("up");
                  return false;
                }

                const direction =
                  originalEvent.originalEvent.key &&
                  (originalEvent.originalEvent.key == "ArrowUp" ||
                    originalEvent.originalEvent.key == "ArrowLeft")
                    ? "up"
                    : "down";
                datatablesManager[tableId].keys.move(direction);
                return false;
              }

              if ($(":input", cell.node()).length > 0) {
                $(":input", cell.node()).focus();
              } else {
                $(":input").blur();
              }
            })
            .on("focus", "td :input", function () {
              $(this).select();
            });
          table
            .parents("form")
            .first()
            .on("keypress", function (e) {
              const keyCode = e.keyCode || e.which;
              if (keyCode === 13) {
                e.preventDefault();
                datatablesManager[tableId].keys.move("right");
                return false;
              }
            });
        }

        setTimeout(function () {
          datatablesManager[tableId].columns.adjust();
        }, 1000);
      }, 100);
    });
  }

  $("[data-delete-url]").click(function (evt) {
    var elm = $(this);
    if (confirm(elm.data("delete-confirm-message"))) {
      $.ajax(elm.data("delete-url"), {
        method: "POST",
        success: function () {
          window.location = elm.data("success-url");
        },
        error: function () {
          alert("Não foi possível apagar.");
        },
      });
    }
    return false;
  });
  $(".button-delete").click(function (el) {
    toggleSubmitForm($(this).parents("form")[0], "enable");
    for (var i = 0; i < calculosCallbacks.length; i++) {
      eval(calculosCallbacks[i] + "()");
    }
  });

  var form_saving = [];
  if ($(".monitor-formchanges").length) {
    $(".monitor-formchanges").each(function (i, formEl) {
      const disabled = toggleSubmitForm(formEl);
      var form = $(formEl);
      if (form.hasClass("warn-changed-beforeunload")) {
        form_saving.push(false);
        form.submit(function (e) {
          if (!form.find("table").hasClass("modoEdicao")) {
            e.preventDefault();
            return;
          }
          if (form.hasClass("form-disabled")) {
            // console.log('não há alterações para salvar!');
            let table = form.find("table");
            toggleEditar(table, false);
            e.preventDefault();
            return;
          } else {
            form_saving[i] = true;
          }
        });
      }
    });
    $(window).on("beforeunload", function () {
      var hasUnsavedChanges = false;
      $(".monitor-formchanges").each(function (i, formEl) {
        var form = $(formEl);
        if (form.find("table.tabelaEditavel.modoEdicao").length == 0) {
          return true;
        }
        if (form.hasClass("warn-changed-beforeunload")) {
          if (!form_saving[i]) {
            var f = FormChanges(formEl);
            if (f.length > 0) {
              hasUnsavedChanges = true;
              return null;
            }
          }
        }
      });
      if (hasUnsavedChanges) {
        return gettext(
          "Você tem alterações não salvas! Tem certeza de que quer sair?"
        );
      }
    });
  }

  $(".nav_bar_titles_wrapper i").on("click", function () {
    const ul = ".nav_bar_titles ul";
    const scrollWidth = $(".nav_bar_titles")[0].scrollWidth;
    let width = gsap.getProperty(".nav_bar_titles", "width");
    let x = gsap.getProperty(ul, "x");
    if ($(this).hasClass("fa-angle-left")) {
      let newX = x + 100;
      if (newX <= 0) {
        gsap.to(ul, { duration: 0.8, x: newX });
      } else if (x < 0) {
        gsap.to(ul, { duration: 0.8, x: 0 });
      }
    } else {
      let newX = x - 100;
      if (scrollWidth - 100 - width > 0) {
        gsap.to(ul, { duration: 0.8, x: newX });
      } else if (x > 0) {
        gsap.to(ul, { duration: 0.8, x: scrollWidth - width });
      }
    }
  });
  if ($(".nav_bar_titles").length > 0) {
    autoCenterNavTabs();
  }

  $(document).on("click", ".indicador, #navbarGlossario", function (e) {
    e.preventDefault();
    $("#modal-right-wrapper").hide();
    $("#modal-ameba-right-wrapper").hide();
    if (!$("#glossario").is(":visible")) {
      $("#glossario").fadeIn();
    }
    $("#glossarioTable tbody tr").removeClass("bg-dark");
    if ($(this).data("id")) {
      const targetIndicador = $(`#glossario_${$(this).data("id")}`);
      targetIndicador.addClass("bg-dark");
      setTimeout(function () {
        $("#glossario .modal-right-body").animate({
          scrollTop: targetIndicador.position().top,
        });
      }, 500);
    }
  });
  const glossarioTable = $("#glossarioTable").DataTable({
    ...dataTableDefaultOptions,
    dom: "fti",
    pageLength: -1,
    select: "single",
    columns: [
      {
        name: "slug",
      },
      {
        name: "nome",
      },
      {
        name: "ajuda",
        searchable: false,
      },
    ],
  });
  $("#glossarioTable_filter").appendTo("#glossario .modal-right-footer");
  $("#glossarioTable_filter input").on("keypress", function () {
    $("#glossarioTable tbody tr").removeClass("bg-dark");
  });

  $(document)
    .on("keydown", function (event) {
      if (event.key === "Escape" && $(".modal-right").is(":visible")) {
        $(".modal-right").fadeOut();
      }
    })
    .on("click", function (event) {
      if (
        $(event.target).attr("id") !== "navbarGlossario" &&
        !$(event.target).hasClass("indicador") &&
        !$(event.target).hasClass("mudancas_eventos-editar") &&
        !$(event.target).hasClass("modal-right-open") &&
        $(event.target).parents(".linhadotempo_holder").length === 0 &&
        $(event.target).parents(".modal-right").length === 0 &&
        $(".modal-right").is(":visible")
      ) {
        $(".modal-right").fadeOut();
      }
    });

  $(".select_lazy").on("focus", function (e) {
    if (e.target.options.length <= 1) {
      const options = eval($(e.target).data("src"));
      for (let option of options) {
        const optionHTML = document.createElement("option");
        optionHTML.value = option.id;
        optionHTML.text = option.nome;
        e.target.add(optionHTML);
      }
    }
  });
});

var callbacks = {};

function calculate_table_cells(
  table_selector,
  origin1_selector,
  origin2_selector,
  destination_selector,
  grand_total_selector
) {
  var trs = $(table_selector + " tr");
  var grand_total = 0;
  var val1, val2, total, tr, text;
  for (var ix = 0; ix < trs.length; ix++) {
    tr = trs[ix];
    if ($(tr).is(":hidden")) {
      continue;
    }
    var origin1 = $(tr).find(origin1_selector);
    var origin2 = $(tr).find(origin2_selector);
    var destination = jQuery(tr).find(destination_selector);
    if (origin1.length) {
      val1 = formatNumber(origin1.val(), "toFloat");
      val2 = "undefined";
      if (origin2.length) {
        val2 = formatNumber(origin2.val(), "toFloat");
      }
      if (!isNaN(val1) && !isNaN(val2)) {
        total = val1 * val2;
        grand_total += total;
      } else if (!isNaN(val1) && isNaN(val2)) {
        total = val1;
        grand_total += val1;
      } else {
        total = 0;
      }
      total_str = formatNumber(total, "toText");
      if (total == 0) {
        total_str = "-";
      }

      if (destination.length) {
        destination.text(total_str);
      }
    }
  }

  // Grand total
  $(table_selector + " " + grand_total_selector).each(function (i, el) {
    if ($(el).find("div").length) {
      $(el).find("div").text(formatNumber(grand_total, "toText"));
    } else {
      $(el).text(formatNumber(grand_total, "toText"));
    }
  });
}

function bind_calc_to_inputs(
  table_selector,
  origin1_selector,
  origin2_selector,
  callback
) {
  jQuery(
    table_selector +
      " " +
      origin1_selector +
      "," +
      table_selector +
      " " +
      origin2_selector
  )
    .off(".init_calc")
    .on("change.init_calc", callback);
}

function init_calc(
  table_selector,
  origin1_selector,
  origin2_selector = "",
  destination_selector = "",
  grand_total_selector = ""
) {
  if (callbacks[table_selector + destination_selector] == null) {
    callbacks[table_selector + destination_selector] = function () {
      calculate_table_cells(
        table_selector,
        origin1_selector,
        origin2_selector,
        destination_selector,
        grand_total_selector
      );
    };
  }
  callbacks[table_selector + destination_selector]();
  bind_calc_to_inputs(
    table_selector,
    origin1_selector,
    origin2_selector,
    callbacks[table_selector + destination_selector]
  );
}

// lista_colunas, valor_origem_name, out_quantidade_field, out_valor_field
function soma_colunas_bulk(table_selector, params, footer_selector) {
  var rows = [];
  var fn_calc_soma = function (elm) {
    var table;

    if (!elm) {
      return;
    }

    table = $(elm);

    var totais = {};

    if (rows.length == 0) {
      table.find("tbody tr").each(function (ix, rowEl) {
        var inputsRaw = $(rowEl).find("input");
        if (inputsRaw.length) {
          var inputs = {};
          for (var i = 0; i < inputsRaw.length; i++) {
            if (!$(inputsRaw[i]).is(":hidden")) {
              inputs[inputsRaw[i].name.replace(/.*-/, "")] = $(inputsRaw[i]);
            }
          }
          if (Object.keys(inputs).length) {
            rows.push({ row: $(rowEl), inputs: inputs });
          }
        }
      });
    }

    let valor_total_coluna = {};
    for (r = 0; r < rows.length; r++) {
      var row = rows[r].row;
      var inputs = rows[r].inputs;
      for (n = 0; n < params.length; n++) {
        var item = params[n];
        var quantidade_linha = 0.0;
        const targetRow = item.lista_colunas_sub_row ? row.next() : row;

        for (i = 0; i < item.lista_colunas.length; i++) {
          if (!valor_total_coluna[item.lista_colunas[i]]) {
            valor_total_coluna[item.lista_colunas[i]] = 0.0;
          }
          var value_in_input = inputs[item.lista_colunas[i]]
            ? formatNumber(inputs[item.lista_colunas[i]].val(), "toFloat")
            : formatNumber(
                targetRow
                  .find("[data-field-row=" + item.lista_colunas[i] + "]")
                  .html(),
                "toFloat"
              );

          if (value_in_input && !isNaN(value_in_input)) {
            quantidade_linha += value_in_input;
            valor_total_coluna[item.lista_colunas[i]] += value_in_input;
          }
        }
        let valor_origem = 0.0;
        let valor_total_linha = 0.0;
        let quantidade_linha_str = "";
        let valor_total_linha_str = "-";

        if (item.valor_origem_name) {
          valor_origem = formatNumber(
            inputs[item.valor_origem_name].val(),
            "toFloat"
          );
          valor_total_linha = valor_origem * quantidade_linha;

          // quantidade_linha_str = (inputs[item.valor_origem_name].parents('.td_qtde').length > 0)
          //     ? formatQtde(quantidade_linha)
          //     : formatNumber(quantidade_linha);
          // valor_total_linha_str = (inputs[item.valor_origem_name].parents('.td_qtde').length > 0)
          //     ? formatQtde(valor_total_linha)
          //     : formatNumber(valor_total_linha);
        } else {
          valor_total_linha = quantidade_linha;
          // valor_total_linha_str = formatNumber(valor_total_linha);
        }

        // if (quantidade_linha_str == 'NaN' || quantidade_linha_str == '0') {
        //     quantidade_linha_str = '';
        // }
        //
        // if (valor_total_linha_str == 'NaN' || valor_total_linha_str == '0,00') {
        //     valor_total_linha_str = '-';
        // }

        const targetOutRow = item.out_sub_row ? row.next() : row;

        // console.log(targetOutRow, item.out_quantidade_field, quantidade_linha_str);
        if (item.out_quantidade_field) {
          const tInputOutQtde = targetOutRow.find(
            "[data-field-row=" + item.out_quantidade_field + "]"
          );
          const quantidade_linha_str = tInputOutQtde.hasClass("td_qtde")
            ? formatQtde(quantidade_linha)
            : formatNumber(quantidade_linha);
          tInputOutQtde.text(quantidade_linha_str);
        }

        if (item.out_valor_field) {
          const tInputOutValor = targetOutRow.find(
            "[data-field-row=" + item.out_valor_field + "]"
          );
          const valor_total_linha_str = tInputOutValor.hasClass("td_qtde")
            ? formatQtde(valor_total_linha)
            : formatNumber(valor_total_linha);
          tInputOutValor.text(valor_total_linha_str);

          if (!(item.out_valor_field in totais)) {
            totais[item.out_valor_field] = 0.0;
          }
          if (!isNaN(valor_total_linha)) {
            totais[item.out_valor_field] += valor_total_linha;
          }
        }
      }
    }

    const footer =
      $(footer_selector).length > 0 ? $(footer_selector) : table.find("footer");

    for (n = 0; n < params.length; n++) {
      var item = params[n];
      if (item.out_valor_field) {
        const tFooter = footer.find(
          "[data-field-total=" + item.out_valor_field + "]"
        );
        let valor_total_tabela_str = tFooter.hasClass("td_qtde")
          ? formatQtde(totais[item.out_valor_field])
          : formatNumber(totais[item.out_valor_field]);
        if (
          valor_total_tabela_str == "NaN" ||
          valor_total_tabela_str == "0,00"
        ) {
          valor_total_tabela_str = "-";
        }
        tFooter.text(valor_total_tabela_str);
      }
      if (item.sum_columns) {
        for (coluna_field of item.lista_colunas) {
          if (coluna_field in valor_total_coluna) {
            const tFooterCol = footer.find(
              `[data-field-total=${coluna_field}]`
            );
            let valor_total_coluna_str = tFooterCol.hasClass("td_qtde")
              ? formatQtde(valor_total_coluna[coluna_field])
              : formatNumber(valor_total_coluna[coluna_field]);
            if (
              valor_total_coluna_str == "NaN" ||
              valor_total_coluna_str == "0,00"
            ) {
              valor_total_coluna_str = "-";
            }
            tFooterCol.text(valor_total_coluna_str);
          }
        }
      }
    }
  };

  // $(table_selector).on('change', 'input', function (evt) {
  //     fn_calc_soma($(table_selector));
  // });
  fn_calc_soma($(table_selector));
}

(function () {
  /**
   * Ajuste decimal de um número.
   *
   * @param    {String}    type    O tipo de arredondamento.
   * @param    {Number}    value    O número a arredondar.
   * @param    {Integer}    exp        O expoente (o logaritmo decimal da base pretendida).
   * @returns    {Number}            O valor depois de ajustado.
   */
  function decimalAdjust(type, value, exp) {
    // Se exp é indefinido ou zero...
    if (typeof exp === "undefined" || +exp === 0) {
      return Math[type](value);
    }
    value = +value;
    exp = +exp;
    // Se o valor não é um número ou o exp não é inteiro...
    if (isNaN(value) || !(typeof exp === "number" && exp % 1 === 0)) {
      return NaN;
    }
    // Transformando para string
    value = value.toString().split("e");
    value = Math[type](+(value[0] + "e" + (value[1] ? +value[1] - exp : -exp)));
    // Transformando de volta
    value = value.toString().split("e");
    return +(value[0] + "e" + (value[1] ? +value[1] + exp : exp));
  }

  // Arredondamento decimal
  if (!Math.round10) {
    Math.round10 = function (value, exp) {
      return decimalAdjust("round", value, exp);
    };
  }
  // Decimal arredondado para baixo
  if (!Math.floor10) {
    Math.floor10 = function (value, exp) {
      return decimalAdjust("floor", value, exp);
    };
  }
  // Decimal arredondado para cima
  if (!Math.ceil10) {
    Math.ceil10 = function (value, exp) {
      return decimalAdjust("ceil", value, exp);
    };
  }
})();

function clone(obj) {
  if (null == obj || "object" != typeof obj) return obj;
  var copy = obj.constructor();
  for (var attr in obj) {
    if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
  }
  return copy;
}

jQuery.ajaxSetup({
  beforeSend: function (xhr, settings) {
    function getCookie(name) {
      var cookieValue = null;
      if (document.cookie && document.cookie != "") {
        var cookies = document.cookie.split(";");
        for (var i = 0; i < cookies.length; i++) {
          var cookie = jQuery.trim(cookies[i]);
          // Does this cookie string begin with the name we want?
          if (cookie.substring(0, name.length + 1) == name + "=") {
            cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
            break;
          }
        }
      }
      return cookieValue;
    }

    if (settings.url.substring(window.location.host)) {
      // Only send the token to relative URLs i.e. locally.
      xhr.setRequestHeader("X-CSRFToken", getCookie("csrftoken"));
    }
  },
});

function getSlug(str) {
  if (!str) {
    return "";
  }
  str = str.replace(/'/, "").replace(/ /g, "_").toLowerCase();
  return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
}

function baixaImagem(conteudo, nome) {
  var link = document.createElement("a");
  link.download = nome;
  link.href = conteudo;
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
  delete link;
}

function getRoundedMaxMinValues(values) {
  var maxTickValue = Math.ceil(Math.max.apply(Math, values) * 1.1);
  var minTickValue = Math.floor(Math.min.apply(Math, values) * 1.1);
  if (maxTickValue > 100000) {
    maxTickValue = Math.round(maxTickValue / 100000) * 100000;
  } else if (maxTickValue > 10000) {
    maxTickValue = Math.round(maxTickValue / 10000) * 10000;
  } else if (maxTickValue > 1000) {
    maxTickValue = Math.round(maxTickValue / 1000) * 1000;
  } else if (maxTickValue > 100) {
    maxTickValue = Math.round(maxTickValue / 100) * 100;
  } else if (maxTickValue > 10) {
    maxTickValue = Math.round(maxTickValue / 10) * 10;
  }
  if (minTickValue > 0) {
    minTickValue = 0;
  } else if (minTickValue < -100000) {
    minTickValue = Math.round(minTickValue / 100000) * 100000;
  } else if (minTickValue < -10000) {
    minTickValue = Math.round(minTickValue / 10000) * 10000;
  } else if (minTickValue < -1000) {
    minTickValue = Math.round(minTickValue / 1000) * 1000;
  } else if (minTickValue < -100) {
    minTickValue = Math.round(minTickValue / 100) * 100;
  } else if (minTickValue < -10) {
    minTickValue = Math.round(minTickValue / 10) * 10;
  }
  return { maxTickValue, minTickValue };
}

function valueToBRLCurrency(value, withCurrency = true, decimals = 2) {
  var prepend = withCurrency ? gettext("R$ ") : "";
  if (!value) {
    value = 0;
  }
  return (
    prepend +
    parseFloat(value).toLocaleString(gettext("pt-BR"), {
      minimumFractionDigits: decimals,
      maximumFractionDigits: decimals,
    })
  );
}

function onRowAdded(newRow) {
  const markitUpEl = newRow.find(".markItUpEditorConfig");
  if (markitUpEl.length > 0) {
    const textarea = markitUpEl.siblings("textarea");
    const id = textarea.attr("id");
    const config = JSON.parse(markitUpEl[0].textContent);
    markitUpEl.remove();
    const newMarkitUpEl = $(`
      <script type="application/json" class="markItUpEditorConfig">
        {
          "selector": "#${id}",
          "extra_settings": ${config["extra_settings"]}
        }
      </script>
    `);
    newMarkitUpEl.insertAfter(textarea);
    $(`#${id}`).each(function (k, el) {
      var el = $(el);
      if (!el.hasClass("markItUpEditor")) {
        el.markItUp(mySettings, config["extra_settings"]);
      }
    });
  }
}

function getDefaultXLSXStyles() {
  return `<numFmts count="3"><numFmt numFmtId="164" formatCode="General"/><numFmt numFmtId="165" formatCode="#,##0"/><numFmt numFmtId="166" formatCode="#,##0.00"/></numFmts><fonts count="6"><font><sz val="11"/><name val="Calibri"/><family val="0"/><charset val="1"/></font><font><sz val="10"/><name val="Arial"/><family val="0"/></font><font><sz val="10"/><name val="Arial"/><family val="0"/></font><font><sz val="10"/><name val="Arial"/><family val="0"/></font><font><sz val="14"/><name val="Calibri"/><family val="0"/><charset val="1"/></font><font><b val="true"/><sz val="11"/><name val="Calibri"/><family val="0"/><charset val="1"/></font></fonts><fills count="4"><fill><patternFill patternType="none"/></fill><fill><patternFill patternType="gray125"/></fill><fill><patternFill patternType="solid"><fgColor rgb="FFDDDDDD"/><bgColor rgb="FFCCFFCC"/></patternFill></fill><fill><patternFill patternType="solid"><fgColor rgb="FFB2B2B2"/><bgColor rgb="FF969696"/></patternFill></fill></fills><borders count="2"><border diagonalUp="false" diagonalDown="false"><left/><right/><top/><bottom/><diagonal/></border><border diagonalUp="false" diagonalDown="false"><left/><right/><top style="thin"><color rgb="FFFFFFFF"/></top><bottom style="thin"><color rgb="FFFFFFFF"/></bottom><diagonal/></border></borders><cellStyleXfs count="20"><xf numFmtId="164" fontId="0" fillId="0" borderId="0" applyFont="true" applyBorder="true" applyAlignment="true" applyProtection="true"><alignment horizontal="general" vertical="bottom" textRotation="0" wrapText="false" indent="0" shrinkToFit="false"/><protection locked="true" hidden="false"/></xf><xf numFmtId="0" fontId="1" fillId="0" borderId="0" applyFont="true" applyBorder="false" applyAlignment="false" applyProtection="false"></xf><xf numFmtId="0" fontId="1" fillId="0" borderId="0" applyFont="true" applyBorder="false" applyAlignment="false" applyProtection="false"></xf><xf numFmtId="0" fontId="2" fillId="0" borderId="0" applyFont="true" applyBorder="false" applyAlignment="false" applyProtection="false"></xf><xf numFmtId="0" fontId="2" fillId="0" borderId="0" applyFont="true" applyBorder="false" applyAlignment="false" applyProtection="false"></xf><xf numFmtId="0" fontId="0" fillId="0" borderId="0" applyFont="true" applyBorder="false" applyAlignment="false" applyProtection="false"></xf><xf numFmtId="0" fontId="0" fillId="0" borderId="0" applyFont="true" applyBorder="false" applyAlignment="false" applyProtection="false"></xf><xf numFmtId="0" fontId="0" fillId="0" borderId="0" applyFont="true" applyBorder="false" applyAlignment="false" applyProtection="false"></xf><xf numFmtId="0" fontId="0" fillId="0" borderId="0" applyFont="true" applyBorder="false" applyAlignment="false" applyProtection="false"></xf><xf numFmtId="0" fontId="0" fillId="0" borderId="0" applyFont="true" applyBorder="false" applyAlignment="false" applyProtection="false"></xf><xf numFmtId="0" fontId="0" fillId="0" borderId="0" applyFont="true" applyBorder="false" applyAlignment="false" applyProtection="false"></xf><xf numFmtId="0" fontId="0" fillId="0" borderId="0" applyFont="true" applyBorder="false" applyAlignment="false" applyProtection="false"></xf><xf numFmtId="0" fontId="0" fillId="0" borderId="0" applyFont="true" applyBorder="false" applyAlignment="false" applyProtection="false"></xf><xf numFmtId="0" fontId="0" fillId="0" borderId="0" applyFont="true" applyBorder="false" applyAlignment="false" applyProtection="false"></xf><xf numFmtId="0" fontId="0" fillId="0" borderId="0" applyFont="true" applyBorder="false" applyAlignment="false" applyProtection="false"></xf><xf numFmtId="43" fontId="1" fillId="0" borderId="0" applyFont="true" applyBorder="false" applyAlignment="false" applyProtection="false"></xf><xf numFmtId="41" fontId="1" fillId="0" borderId="0" applyFont="true" applyBorder="false" applyAlignment="false" applyProtection="false"></xf><xf numFmtId="44" fontId="1" fillId="0" borderId="0" applyFont="true" applyBorder="false" applyAlignment="false" applyProtection="false"></xf><xf numFmtId="42" fontId="1" fillId="0" borderId="0" applyFont="true" applyBorder="false" applyAlignment="false" applyProtection="false"></xf><xf numFmtId="9" fontId="1" fillId="0" borderId="0" applyFont="true" applyBorder="false" applyAlignment="false" applyProtection="false"></xf></cellStyleXfs><cellXfs count="17"><xf numFmtId="164" fontId="0" fillId="0" borderId="0" xfId="0" applyFont="false" applyBorder="false" applyAlignment="false" applyProtection="false"><alignment horizontal="general" vertical="bottom" textRotation="0" wrapText="false" indent="0" shrinkToFit="false"/><protection locked="true" hidden="false"/></xf><xf numFmtId="164" fontId="4" fillId="0" borderId="0" xfId="0" applyFont="true" applyBorder="true" applyAlignment="true" applyProtection="false"><alignment horizontal="left" vertical="bottom" textRotation="0" wrapText="false" indent="0" shrinkToFit="false"/><protection locked="true" hidden="false"/></xf><xf numFmtId="164" fontId="5" fillId="2" borderId="0" xfId="0" applyFont="true" applyBorder="true" applyAlignment="true" applyProtection="false"><alignment horizontal="center" vertical="bottom" textRotation="0" wrapText="false" indent="0" shrinkToFit="false"/><protection locked="true" hidden="false"/></xf><xf numFmtId="164" fontId="5" fillId="3" borderId="0" xfId="0" applyFont="true" applyBorder="true" applyAlignment="true" applyProtection="false"><alignment horizontal="center" vertical="bottom" textRotation="0" wrapText="false" indent="0" shrinkToFit="false"/><protection locked="true" hidden="false"/></xf><xf numFmtId="164" fontId="0" fillId="2" borderId="1" xfId="0" applyFont="true" applyBorder="true" applyAlignment="false" applyProtection="false"><alignment horizontal="general" vertical="bottom" textRotation="0" wrapText="false" indent="0" shrinkToFit="false"/><protection locked="true" hidden="false"/></xf><xf numFmtId="164" fontId="0" fillId="3" borderId="1" xfId="0" applyFont="true" applyBorder="true" applyAlignment="false" applyProtection="false"><alignment horizontal="general" vertical="bottom" textRotation="0" wrapText="false" indent="0" shrinkToFit="false"/><protection locked="true" hidden="false"/></xf><xf numFmtId="165" fontId="0" fillId="3" borderId="1" xfId="0" applyFont="true" applyBorder="true" applyAlignment="false" applyProtection="false"><alignment horizontal="general" vertical="bottom" textRotation="0" wrapText="false" indent="0" shrinkToFit="false"/><protection locked="true" hidden="false"/></xf><xf numFmtId="165" fontId="0" fillId="2" borderId="1" xfId="0" applyFont="true" applyBorder="true" applyAlignment="false" applyProtection="false"><alignment horizontal="general" vertical="bottom" textRotation="0" wrapText="false" indent="0" shrinkToFit="false"/><protection locked="true" hidden="false"/></xf><xf numFmtId="166" fontId="0" fillId="2" borderId="1" xfId="0" applyFont="true" applyBorder="true" applyAlignment="false" applyProtection="false"><alignment horizontal="general" vertical="bottom" textRotation="0" wrapText="false" indent="0" shrinkToFit="false"/><protection locked="true" hidden="false"/></xf><xf numFmtId="166" fontId="0" fillId="3" borderId="1" xfId="0" applyFont="true" applyBorder="true" applyAlignment="false" applyProtection="false"><alignment horizontal="general" vertical="bottom" textRotation="0" wrapText="false" indent="0" shrinkToFit="false"/><protection locked="true" hidden="false"/></xf><xf numFmtId="164" fontId="0" fillId="3" borderId="1" xfId="0" applyFont="true" applyBorder="true" applyAlignment="false" applyProtection="false"><alignment horizontal="general" vertical="bottom" textRotation="0" wrapText="false" indent="0" shrinkToFit="false"/><protection locked="true" hidden="false"/></xf><xf numFmtId="164" fontId="5" fillId="2" borderId="0" xfId="0" applyFont="true" applyBorder="true" applyAlignment="false" applyProtection="false"><alignment horizontal="general" vertical="bottom" textRotation="0" wrapText="false" indent="0" shrinkToFit="false"/><protection locked="true" hidden="false"/></xf><xf numFmtId="166" fontId="5" fillId="2" borderId="0" xfId="0" applyFont="true" applyBorder="true" applyAlignment="false" applyProtection="false"><alignment horizontal="general" vertical="bottom" textRotation="0" wrapText="false" indent="0" shrinkToFit="false"/><protection locked="true" hidden="false"/></xf><xf numFmtId="166" fontId="5" fillId="3" borderId="0" xfId="0" applyFont="true" applyBorder="true" applyAlignment="false" applyProtection="false"><alignment horizontal="general" vertical="bottom" textRotation="0" wrapText="false" indent="0" shrinkToFit="false"/><protection locked="true" hidden="false"/></xf><xf numFmtId="165" fontId="5" fillId="3" borderId="0" xfId="0" applyFont="true" applyBorder="true" applyAlignment="false" applyProtection="false"><alignment horizontal="general" vertical="bottom" textRotation="0" wrapText="false" indent="0" shrinkToFit="false"/><protection locked="true" hidden="false"/></xf><xf numFmtId="164" fontId="5" fillId="3" borderId="0" xfId="0" applyFont="true" applyBorder="true" applyAlignment="false" applyProtection="false"><alignment horizontal="general" vertical="bottom" textRotation="0" wrapText="false" indent="0" shrinkToFit="false"/><protection locked="true" hidden="false"/></xf><xf numFmtId="165" fontId="5" fillId="2" borderId="0" xfId="0" applyFont="true" applyBorder="true" applyAlignment="false" applyProtection="false"><alignment horizontal="general" vertical="bottom" textRotation="0" wrapText="false" indent="0" shrinkToFit="false"/><protection locked="true" hidden="false"/></xf></cellXfs><cellStyles count="6"><cellStyle name="Normal" xfId="0" builtinId="0"/><cellStyle name="Comma" xfId="15" builtinId="3"/><cellStyle name="Comma [0]" xfId="16" builtinId="6"/><cellStyle name="Currency" xfId="17" builtinId="4"/><cellStyle name="Currency [0]" xfId="18" builtinId="7"/><cellStyle name="Percent" xfId="19" builtinId="5"/></cellStyles><colors><indexedColors><rgbColor rgb="FF000000"/><rgbColor rgb="FFFFFFFF"/><rgbColor rgb="FFFF0000"/><rgbColor rgb="FF00FF00"/><rgbColor rgb="FF0000FF"/><rgbColor rgb="FFFFFF00"/><rgbColor rgb="FFFF00FF"/><rgbColor rgb="FF00FFFF"/><rgbColor rgb="FF800000"/><rgbColor rgb="FF008000"/><rgbColor rgb="FF000080"/><rgbColor rgb="FF808000"/><rgbColor rgb="FF800080"/><rgbColor rgb="FF008080"/><rgbColor rgb="FFB2B2B2"/><rgbColor rgb="FF808080"/><rgbColor rgb="FF9999FF"/><rgbColor rgb="FF993366"/><rgbColor rgb="FFFFFFCC"/><rgbColor rgb="FFCCFFFF"/><rgbColor rgb="FF660066"/><rgbColor rgb="FFFF8080"/><rgbColor rgb="FF0066CC"/><rgbColor rgb="FFDDDDDD"/><rgbColor rgb="FF000080"/><rgbColor rgb="FFFF00FF"/><rgbColor rgb="FFFFFF00"/><rgbColor rgb="FF00FFFF"/><rgbColor rgb="FF800080"/><rgbColor rgb="FF800000"/><rgbColor rgb="FF008080"/><rgbColor rgb="FF0000FF"/><rgbColor rgb="FF00CCFF"/><rgbColor rgb="FFCCFFFF"/><rgbColor rgb="FFCCFFCC"/><rgbColor rgb="FFFFFF99"/><rgbColor rgb="FF99CCFF"/><rgbColor rgb="FFFF99CC"/><rgbColor rgb="FFCC99FF"/><rgbColor rgb="FFFFCC99"/><rgbColor rgb="FF3366FF"/><rgbColor rgb="FF33CCCC"/><rgbColor rgb="FF99CC00"/><rgbColor rgb="FFFFCC00"/><rgbColor rgb="FFFF9900"/><rgbColor rgb="FFFF6600"/><rgbColor rgb="FF666699"/><rgbColor rgb="FF969696"/><rgbColor rgb="FF003366"/><rgbColor rgb="FF339966"/><rgbColor rgb="FF003300"/><rgbColor rgb="FF333300"/><rgbColor rgb="FF993300"/><rgbColor rgb="FF993366"/><rgbColor rgb="FF333399"/><rgbColor rgb="FF333333"/></indexedColors></colors>`;
}
