--
-- PostgreSQL database dump
--

-- Dumped from database version 10.8 (Ubuntu 10.8-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.8 (Ubuntu 10.8-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: account_emailaddress; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.account_emailaddress (
    id integer NOT NULL,
    email character varying(254) NOT NULL,
    verified boolean NOT NULL,
    "primary" boolean NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.account_emailaddress OWNER TO dtygel;

--
-- Name: account_emailaddress_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.account_emailaddress_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_emailaddress_id_seq OWNER TO dtygel;

--
-- Name: account_emailaddress_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.account_emailaddress_id_seq OWNED BY public.account_emailaddress.id;


--
-- Name: account_emailconfirmation; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.account_emailconfirmation (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    sent timestamp with time zone,
    key character varying(64) NOT NULL,
    email_address_id integer NOT NULL
);


ALTER TABLE public.account_emailconfirmation OWNER TO dtygel;

--
-- Name: account_emailconfirmation_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.account_emailconfirmation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_emailconfirmation_id_seq OWNER TO dtygel;

--
-- Name: account_emailconfirmation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.account_emailconfirmation_id_seq OWNED BY public.account_emailconfirmation.id;


--
-- Name: agroecossistema; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.agroecossistema (
    id integer NOT NULL,
    geoponto public.geometry(Point,4326),
    poligono public.geometry(Polygon,4326),
    criacao timestamp with time zone NOT NULL,
    ultima_alteracao timestamp with time zone NOT NULL,
    nsga character varying(80),
    tipo_gestao_nsga character varying(1) NOT NULL,
    autor_id integer NOT NULL,
    comunidade_id integer NOT NULL,
    organizacao_id integer,
    territorio_id integer,
    parametros_inativos jsonb NOT NULL,
    deleted timestamp with time zone,
    mes_fim_ciclo integer NOT NULL,
    mes_inicio_ciclo integer NOT NULL
);


ALTER TABLE public.agroecossistema OWNER TO dtygel;

--
-- Name: agroecossistema_cicloanualreferencia; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.agroecossistema_cicloanualreferencia (
    id integer NOT NULL,
    deleted timestamp with time zone,
    ano integer NOT NULL,
    criacao timestamp with time zone NOT NULL,
    ultima_alteracao timestamp with time zone NOT NULL,
    agroecossistema_id integer NOT NULL
);


ALTER TABLE public.agroecossistema_cicloanualreferencia OWNER TO dtygel;

--
-- Name: agroecossistema_cicloanualreferencia_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.agroecossistema_cicloanualreferencia_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agroecossistema_cicloanualreferencia_id_seq OWNER TO dtygel;

--
-- Name: agroecossistema_cicloanualreferencia_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.agroecossistema_cicloanualreferencia_id_seq OWNED BY public.agroecossistema_cicloanualreferencia.id;


--
-- Name: agroecossistema_composicaonsga; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.agroecossistema_composicaonsga (
    id integer NOT NULL,
    utf_agroecossistema numeric(10,2) NOT NULL,
    utf_pluriatividade numeric(10,2) NOT NULL,
    responsavel boolean NOT NULL,
    criacao timestamp with time zone NOT NULL,
    ultima_alteracao timestamp with time zone NOT NULL,
    ciclo_anual_referencia_id integer NOT NULL,
    pessoa_id integer NOT NULL
);


ALTER TABLE public.agroecossistema_composicaonsga OWNER TO dtygel;

--
-- Name: agroecossistema_composicaonsga_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.agroecossistema_composicaonsga_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agroecossistema_composicaonsga_id_seq OWNER TO dtygel;

--
-- Name: agroecossistema_composicaonsga_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.agroecossistema_composicaonsga_id_seq OWNED BY public.agroecossistema_composicaonsga.id;


--
-- Name: agroecossistema_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.agroecossistema_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agroecossistema_id_seq OWNER TO dtygel;

--
-- Name: agroecossistema_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.agroecossistema_id_seq OWNED BY public.agroecossistema.id;


--
-- Name: agroecossistema_pessoa; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.agroecossistema_pessoa (
    id integer NOT NULL,
    deleted timestamp with time zone,
    nome character varying(100) NOT NULL,
    sexo character varying(1) NOT NULL,
    data_nascimento date NOT NULL,
    criacao timestamp with time zone NOT NULL,
    ultima_alteracao timestamp with time zone NOT NULL,
    agroecossistema_id integer NOT NULL
);


ALTER TABLE public.agroecossistema_pessoa OWNER TO dtygel;

--
-- Name: agroecossistema_pessoa_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.agroecossistema_pessoa_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agroecossistema_pessoa_id_seq OWNER TO dtygel;

--
-- Name: agroecossistema_pessoa_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.agroecossistema_pessoa_id_seq OWNED BY public.agroecossistema_pessoa.id;


--
-- Name: agroecossistema_terra; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.agroecossistema_terra (
    id integer NOT NULL,
    deleted timestamp with time zone,
    poligono public.geometry(Polygon,4326),
    area_territorio numeric(12,4) NOT NULL,
    tipo character varying(1) NOT NULL,
    criacao timestamp with time zone NOT NULL,
    ultima_alteracao timestamp with time zone NOT NULL,
    ciclo_anual_referencia_id integer NOT NULL,
    observacoes text
);


ALTER TABLE public.agroecossistema_terra OWNER TO dtygel;

--
-- Name: agroecossistema_terra_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.agroecossistema_terra_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agroecossistema_terra_id_seq OWNER TO dtygel;

--
-- Name: agroecossistema_terra_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.agroecossistema_terra_id_seq OWNED BY public.agroecossistema_terra.id;


--
-- Name: anexos_anexo; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.anexos_anexo (
    id integer NOT NULL,
    nome character varying(80) NOT NULL,
    descricao text,
    mimetype character varying(80) NOT NULL,
    ultima_alteracao timestamp with time zone NOT NULL,
    arquivo character varying(100) NOT NULL,
    tipo_id integer,
    criacao timestamp with time zone NOT NULL,
    deleted timestamp with time zone
);


ALTER TABLE public.anexos_anexo OWNER TO dtygel;

--
-- Name: anexos_anexo_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.anexos_anexo_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.anexos_anexo_id_seq OWNER TO dtygel;

--
-- Name: anexos_anexo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.anexos_anexo_id_seq OWNED BY public.anexos_anexo.id;


--
-- Name: anexos_anexorecipiente; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.anexos_anexorecipiente (
    id integer NOT NULL,
    recipiente_object_id integer NOT NULL,
    anexo_id integer NOT NULL,
    recipiente_content_type_id integer NOT NULL,
    criacao timestamp with time zone NOT NULL,
    ultima_alteracao timestamp with time zone NOT NULL,
    deleted timestamp with time zone
);


ALTER TABLE public.anexos_anexorecipiente OWNER TO dtygel;

--
-- Name: anexos_anexorecipiente_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.anexos_anexorecipiente_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.anexos_anexorecipiente_id_seq OWNER TO dtygel;

--
-- Name: anexos_anexorecipiente_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.anexos_anexorecipiente_id_seq OWNED BY public.anexos_anexorecipiente.id;


--
-- Name: anexos_tipoanexo; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.anexos_tipoanexo (
    id integer NOT NULL,
    nome character varying(80) NOT NULL,
    mae_id integer,
    deleted timestamp with time zone
);


ALTER TABLE public.anexos_tipoanexo OWNER TO dtygel;

--
-- Name: anexos_tipoanexo_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.anexos_tipoanexo_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.anexos_tipoanexo_id_seq OWNER TO dtygel;

--
-- Name: anexos_tipoanexo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.anexos_tipoanexo_id_seq OWNED BY public.anexos_tipoanexo.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO dtygel;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO dtygel;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO dtygel;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO dtygel;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO dtygel;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO dtygel;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: cadernodecampo_anotacao; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.cadernodecampo_anotacao (
    id integer NOT NULL,
    texto text NOT NULL,
    criacao timestamp with time zone NOT NULL,
    ultima_alteracao timestamp with time zone NOT NULL,
    agroecossistema_id integer NOT NULL,
    autor_id integer NOT NULL,
    data_visita date NOT NULL,
    deleted timestamp with time zone
);


ALTER TABLE public.cadernodecampo_anotacao OWNER TO dtygel;

--
-- Name: cadernodecampo_anotacao_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.cadernodecampo_anotacao_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cadernodecampo_anotacao_id_seq OWNER TO dtygel;

--
-- Name: cadernodecampo_anotacao_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.cadernodecampo_anotacao_id_seq OWNED BY public.cadernodecampo_anotacao.id;


--
-- Name: comunidade; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.comunidade (
    id integer NOT NULL,
    nome character varying(80) NOT NULL,
    geoponto public.geometry(Point,4326),
    poligono public.geometry(Polygon,4326),
    municipio_id integer NOT NULL,
    criacao timestamp with time zone NOT NULL,
    ultima_alteracao timestamp with time zone NOT NULL,
    deleted timestamp with time zone
);


ALTER TABLE public.comunidade OWNER TO dtygel;

--
-- Name: comunidade_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.comunidade_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.comunidade_id_seq OWNER TO dtygel;

--
-- Name: comunidade_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.comunidade_id_seq OWNED BY public.comunidade.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO dtygel;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO dtygel;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO dtygel;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO dtygel;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO dtygel;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO dtygel;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO dtygel;

--
-- Name: django_site; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.django_site (
    id integer NOT NULL,
    domain character varying(100) NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.django_site OWNER TO dtygel;

--
-- Name: django_site_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.django_site_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_site_id_seq OWNER TO dtygel;

--
-- Name: django_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.django_site_id_seq OWNED BY public.django_site.id;


--
-- Name: django_summernote_attachment; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.django_summernote_attachment (
    id integer NOT NULL,
    name character varying(255),
    file character varying(100) NOT NULL,
    uploaded timestamp with time zone NOT NULL
);


ALTER TABLE public.django_summernote_attachment OWNER TO dtygel;

--
-- Name: django_summernote_attachment_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.django_summernote_attachment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_summernote_attachment_id_seq OWNER TO dtygel;

--
-- Name: django_summernote_attachment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.django_summernote_attachment_id_seq OWNED BY public.django_summernote_attachment.id;


--
-- Name: economica_analise; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.economica_analise (
    id integer NOT NULL,
    deleted timestamp with time zone,
    data_coleta timestamp with time zone,
    criacao timestamp with time zone NOT NULL,
    ultima_alteracao timestamp with time zone NOT NULL,
    ciclo_anual_referencia_id integer NOT NULL
);


ALTER TABLE public.economica_analise OWNER TO dtygel;

--
-- Name: economica_analise_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.economica_analise_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.economica_analise_id_seq OWNER TO dtygel;

--
-- Name: economica_analise_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.economica_analise_id_seq OWNED BY public.economica_analise.id;


--
-- Name: economica_estoqueinsumos; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.economica_estoqueinsumos (
    id integer NOT NULL,
    deleted timestamp with time zone,
    valor_unitario_inicio numeric(10,2) NOT NULL,
    quantidade_inicio numeric(10,2) NOT NULL,
    valor_unitario_final numeric(10,2) NOT NULL,
    quantidade_final numeric(10,2) NOT NULL,
    criacao timestamp with time zone NOT NULL,
    ultima_alteracao timestamp with time zone NOT NULL,
    analise_id integer NOT NULL,
    produto_id integer NOT NULL,
    unidade_id integer NOT NULL,
    observacoes text
);


ALTER TABLE public.economica_estoqueinsumos OWNER TO dtygel;

--
-- Name: economica_estoqueinsumos_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.economica_estoqueinsumos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.economica_estoqueinsumos_id_seq OWNER TO dtygel;

--
-- Name: economica_estoqueinsumos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.economica_estoqueinsumos_id_seq OWNED BY public.economica_estoqueinsumos.id;


--
-- Name: economica_pagamentoterceiros; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.economica_pagamentoterceiros (
    id integer NOT NULL,
    deleted timestamp with time zone,
    valor_unitario numeric(10,2) NOT NULL,
    quantidade numeric(10,2) NOT NULL,
    criacao timestamp with time zone NOT NULL,
    ultima_alteracao timestamp with time zone NOT NULL,
    analise_id integer NOT NULL,
    servico_id integer NOT NULL,
    unidade_id integer NOT NULL,
    observacoes text
);


ALTER TABLE public.economica_pagamentoterceiros OWNER TO dtygel;

--
-- Name: economica_pagamentoterceiros_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.economica_pagamentoterceiros_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.economica_pagamentoterceiros_id_seq OWNER TO dtygel;

--
-- Name: economica_pagamentoterceiros_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.economica_pagamentoterceiros_id_seq OWNED BY public.economica_pagamentoterceiros.id;


--
-- Name: economica_patrimonio; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.economica_patrimonio (
    id integer NOT NULL,
    deleted timestamp with time zone,
    item character varying(255) NOT NULL,
    valor_unitario_inicio numeric(10,2) NOT NULL,
    quantidade_inicio numeric(10,2) NOT NULL,
    valor_unitario_final numeric(10,2) NOT NULL,
    tipo character varying(1) NOT NULL,
    observacoes text,
    criacao timestamp with time zone NOT NULL,
    ultima_alteracao timestamp with time zone NOT NULL,
    analise_id integer NOT NULL,
    unidade_id integer NOT NULL,
    quantidade_final numeric(10,2) NOT NULL
);


ALTER TABLE public.economica_patrimonio OWNER TO dtygel;

--
-- Name: economica_patrimonio_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.economica_patrimonio_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.economica_patrimonio_id_seq OWNER TO dtygel;

--
-- Name: economica_patrimonio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.economica_patrimonio_id_seq OWNED BY public.economica_patrimonio.id;


--
-- Name: economica_rendanaoagricola; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.economica_rendanaoagricola (
    id integer NOT NULL,
    deleted timestamp with time zone,
    tipo character varying(1) NOT NULL,
    item character varying(60) NOT NULL,
    valor_unitario numeric(10,2) NOT NULL,
    quantidade numeric(10,2) NOT NULL,
    observacoes text,
    criacao timestamp with time zone NOT NULL,
    ultima_alteracao timestamp with time zone NOT NULL,
    analise_id integer NOT NULL,
    unidade_id integer NOT NULL
);


ALTER TABLE public.economica_rendanaoagricola OWNER TO dtygel;

--
-- Name: economica_rendanaoagricola_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.economica_rendanaoagricola_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.economica_rendanaoagricola_id_seq OWNER TO dtygel;

--
-- Name: economica_rendanaoagricola_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.economica_rendanaoagricola_id_seq OWNED BY public.economica_rendanaoagricola.id;


--
-- Name: economica_subsistema; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.economica_subsistema (
    id integer NOT NULL,
    deleted timestamp with time zone,
    area numeric(10,2),
    nome character varying(80) NOT NULL,
    observacoes text,
    criacao timestamp with time zone NOT NULL,
    ultima_alteracao timestamp with time zone NOT NULL,
    analise_id integer NOT NULL,
    extrativismo boolean NOT NULL
);


ALTER TABLE public.economica_subsistema OWNER TO dtygel;

--
-- Name: economica_subsistema_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.economica_subsistema_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.economica_subsistema_id_seq OWNER TO dtygel;

--
-- Name: economica_subsistema_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.economica_subsistema_id_seq OWNED BY public.economica_subsistema.id;


--
-- Name: economica_subsistemainsumo; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.economica_subsistemainsumo (
    id integer NOT NULL,
    deleted timestamp with time zone,
    valor_unitario numeric(10,2) NOT NULL,
    quantidade numeric(10,2) NOT NULL,
    origem character varying(1),
    observacoes text,
    tipo character varying(1) NOT NULL,
    criacao timestamp with time zone NOT NULL,
    ultima_alteracao timestamp with time zone NOT NULL,
    produto_id integer NOT NULL,
    subsistema_id integer NOT NULL,
    unidade_id integer NOT NULL
);


ALTER TABLE public.economica_subsistemainsumo OWNER TO dtygel;

--
-- Name: economica_subsistemaaquisicao_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.economica_subsistemaaquisicao_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.economica_subsistemaaquisicao_id_seq OWNER TO dtygel;

--
-- Name: economica_subsistemaaquisicao_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.economica_subsistemaaquisicao_id_seq OWNED BY public.economica_subsistemainsumo.id;


--
-- Name: economica_subsistemaaquisicaoreciprocidade; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.economica_subsistemaaquisicaoreciprocidade (
    id integer NOT NULL,
    deleted timestamp with time zone,
    valor_unitario numeric(10,2) NOT NULL,
    quantidade numeric(10,2) NOT NULL,
    observacoes text,
    tipo character varying(1) NOT NULL,
    criacao timestamp with time zone NOT NULL,
    ultima_alteracao timestamp with time zone NOT NULL,
    produto_id integer,
    subsistema_id integer NOT NULL,
    unidade_id integer NOT NULL,
    servico_id integer
);


ALTER TABLE public.economica_subsistemaaquisicaoreciprocidade OWNER TO dtygel;

--
-- Name: economica_subsistemaaquisicaoreciprocidade_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.economica_subsistemaaquisicaoreciprocidade_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.economica_subsistemaaquisicaoreciprocidade_id_seq OWNER TO dtygel;

--
-- Name: economica_subsistemaaquisicaoreciprocidade_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.economica_subsistemaaquisicaoreciprocidade_id_seq OWNED BY public.economica_subsistemaaquisicaoreciprocidade.id;


--
-- Name: economica_subsistemapagamentoterceiros; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.economica_subsistemapagamentoterceiros (
    id integer NOT NULL,
    deleted timestamp with time zone,
    valor_unitario numeric(10,2) NOT NULL,
    quantidade numeric(10,2) NOT NULL,
    origem character varying(1),
    observacoes text,
    criacao timestamp with time zone NOT NULL,
    ultima_alteracao timestamp with time zone NOT NULL,
    servico_id integer NOT NULL,
    subsistema_id integer NOT NULL,
    unidade_id integer NOT NULL
);


ALTER TABLE public.economica_subsistemapagamentoterceiros OWNER TO dtygel;

--
-- Name: economica_subsistemapagamentoterceiros_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.economica_subsistemapagamentoterceiros_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.economica_subsistemapagamentoterceiros_id_seq OWNER TO dtygel;

--
-- Name: economica_subsistemapagamentoterceiros_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.economica_subsistemapagamentoterceiros_id_seq OWNED BY public.economica_subsistemapagamentoterceiros.id;


--
-- Name: economica_subsistemaproducao; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.economica_subsistemaproducao (
    id integer NOT NULL,
    deleted timestamp with time zone,
    observacoes text,
    valor_unitario numeric(10,2) NOT NULL,
    quantidade_venda numeric(10,2),
    quantidade_autoconsumo numeric(10,2),
    quantidade_doacoes_trocas numeric(10,2),
    quantidade_estoque numeric(10,2),
    quantidade_insumos_produzidos numeric(10,2),
    criacao timestamp with time zone NOT NULL,
    ultima_alteracao timestamp with time zone NOT NULL,
    produto_id integer NOT NULL,
    subsistema_id integer NOT NULL,
    unidade_id integer NOT NULL
);


ALTER TABLE public.economica_subsistemaproducao OWNER TO dtygel;

--
-- Name: economica_subsistemaproducao_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.economica_subsistemaproducao_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.economica_subsistemaproducao_id_seq OWNER TO dtygel;

--
-- Name: economica_subsistemaproducao_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.economica_subsistemaproducao_id_seq OWNED BY public.economica_subsistemaproducao.id;


--
-- Name: economica_subsistematempotrabalho; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.economica_subsistematempotrabalho (
    id integer NOT NULL,
    horas numeric(10,2) NOT NULL,
    criacao timestamp with time zone NOT NULL,
    ultima_alteracao timestamp with time zone NOT NULL,
    subsistema_id integer NOT NULL,
    composicao_nsga_id integer NOT NULL
);


ALTER TABLE public.economica_subsistematempotrabalho OWNER TO dtygel;

--
-- Name: economica_subsistematempotrabalho_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.economica_subsistematempotrabalho_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.economica_subsistematempotrabalho_id_seq OWNER TO dtygel;

--
-- Name: economica_subsistematempotrabalho_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.economica_subsistematempotrabalho_id_seq OWNED BY public.economica_subsistematempotrabalho.id;


--
-- Name: economica_tempotrabalho; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.economica_tempotrabalho (
    id integer NOT NULL,
    criacao timestamp with time zone NOT NULL,
    ultima_alteracao timestamp with time zone NOT NULL,
    analise_id integer NOT NULL,
    composicao_nsga_id integer NOT NULL,
    horas_domestico_cuidados integer NOT NULL,
    horas_participacao_social integer NOT NULL,
    horas_pluriatividade integer NOT NULL
);


ALTER TABLE public.economica_tempotrabalho OWNER TO dtygel;

--
-- Name: economica_tempotrabalho_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.economica_tempotrabalho_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.economica_tempotrabalho_id_seq OWNER TO dtygel;

--
-- Name: economica_tempotrabalho_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.economica_tempotrabalho_id_seq OWNED BY public.economica_tempotrabalho.id;


--
-- Name: linhadotempo_dimensao; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.linhadotempo_dimensao (
    id integer NOT NULL,
    nome character varying(80) NOT NULL,
    ordem smallint,
    descricao text,
    tipo_dimensao_id integer NOT NULL,
    deleted timestamp with time zone
);


ALTER TABLE public.linhadotempo_dimensao OWNER TO dtygel;

--
-- Name: linhadotempo_dimensao_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.linhadotempo_dimensao_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.linhadotempo_dimensao_id_seq OWNER TO dtygel;

--
-- Name: linhadotempo_dimensao_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.linhadotempo_dimensao_id_seq OWNED BY public.linhadotempo_dimensao.id;


--
-- Name: linhadotempo_evento; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.linhadotempo_evento (
    id integer NOT NULL,
    data_inicio smallint NOT NULL,
    data_fim smallint,
    titulo character varying(80) NOT NULL,
    detalhes text NOT NULL,
    agroecossistema_id integer NOT NULL,
    criado_por_id integer,
    dimensao_id integer NOT NULL,
    criacao timestamp with time zone NOT NULL,
    ultima_alteracao timestamp with time zone NOT NULL,
    deleted timestamp with time zone,
    CONSTRAINT linhadotempo_evento_data_fim_check CHECK ((data_fim >= 0)),
    CONSTRAINT linhadotempo_evento_data_inicio_check CHECK ((data_inicio >= 0))
);


ALTER TABLE public.linhadotempo_evento OWNER TO dtygel;

--
-- Name: linhadotempo_evento_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.linhadotempo_evento_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.linhadotempo_evento_id_seq OWNER TO dtygel;

--
-- Name: linhadotempo_evento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.linhadotempo_evento_id_seq OWNED BY public.linhadotempo_evento.id;


--
-- Name: linhadotempo_eventogrupo; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.linhadotempo_eventogrupo (
    id integer NOT NULL,
    nome character varying(255) NOT NULL,
    descricao text,
    criacao timestamp with time zone NOT NULL,
    ultima_alteracao timestamp with time zone NOT NULL,
    agroecossistema_id integer NOT NULL,
    deleted timestamp with time zone
);


ALTER TABLE public.linhadotempo_eventogrupo OWNER TO dtygel;

--
-- Name: linhadotempo_eventogrupo_eventos; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.linhadotempo_eventogrupo_eventos (
    id integer NOT NULL,
    eventogrupo_id integer NOT NULL,
    evento_id integer NOT NULL
);


ALTER TABLE public.linhadotempo_eventogrupo_eventos OWNER TO dtygel;

--
-- Name: linhadotempo_eventogrupo_eventos_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.linhadotempo_eventogrupo_eventos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.linhadotempo_eventogrupo_eventos_id_seq OWNER TO dtygel;

--
-- Name: linhadotempo_eventogrupo_eventos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.linhadotempo_eventogrupo_eventos_id_seq OWNED BY public.linhadotempo_eventogrupo_eventos.id;


--
-- Name: linhadotempo_eventogrupo_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.linhadotempo_eventogrupo_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.linhadotempo_eventogrupo_id_seq OWNER TO dtygel;

--
-- Name: linhadotempo_eventogrupo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.linhadotempo_eventogrupo_id_seq OWNED BY public.linhadotempo_eventogrupo.id;


--
-- Name: linhadotempo_tipodimensao; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.linhadotempo_tipodimensao (
    id integer NOT NULL,
    nome character varying(80) NOT NULL,
    deleted timestamp with time zone
);


ALTER TABLE public.linhadotempo_tipodimensao OWNER TO dtygel;

--
-- Name: linhadotempo_tipodimensao_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.linhadotempo_tipodimensao_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.linhadotempo_tipodimensao_id_seq OWNER TO dtygel;

--
-- Name: linhadotempo_tipodimensao_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.linhadotempo_tipodimensao_id_seq OWNED BY public.linhadotempo_tipodimensao.id;


--
-- Name: main_organizacaousuaria; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.main_organizacaousuaria (
    id integer NOT NULL,
    is_admin boolean NOT NULL,
    organizacao_id integer NOT NULL,
    usuaria_id integer NOT NULL,
    deleted timestamp with time zone
);


ALTER TABLE public.main_organizacaousuaria OWNER TO dtygel;

--
-- Name: main_organizacaousuaria_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.main_organizacaousuaria_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_organizacaousuaria_id_seq OWNER TO dtygel;

--
-- Name: main_organizacaousuaria_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.main_organizacaousuaria_id_seq OWNED BY public.main_organizacaousuaria.id;


--
-- Name: main_produto; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.main_produto (
    id integer NOT NULL,
    deleted timestamp with time zone,
    nome character varying(100) NOT NULL,
    criacao timestamp with time zone NOT NULL,
    ultima_alteracao timestamp with time zone NOT NULL
);


ALTER TABLE public.main_produto OWNER TO dtygel;

--
-- Name: main_produto_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.main_produto_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_produto_id_seq OWNER TO dtygel;

--
-- Name: main_produto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.main_produto_id_seq OWNED BY public.main_produto.id;


--
-- Name: main_servico; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.main_servico (
    id integer NOT NULL,
    deleted timestamp with time zone,
    nome character varying(60) NOT NULL,
    criacao timestamp with time zone NOT NULL,
    ultima_alteracao timestamp with time zone NOT NULL
);


ALTER TABLE public.main_servico OWNER TO dtygel;

--
-- Name: main_servico_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.main_servico_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_servico_id_seq OWNER TO dtygel;

--
-- Name: main_servico_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.main_servico_id_seq OWNED BY public.main_servico.id;


--
-- Name: main_territorio; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.main_territorio (
    id integer NOT NULL,
    nome character varying(80) NOT NULL,
    descricao text NOT NULL,
    criacao timestamp with time zone NOT NULL,
    ultima_alteracao timestamp with time zone NOT NULL,
    deleted timestamp with time zone
);


ALTER TABLE public.main_territorio OWNER TO dtygel;

--
-- Name: main_territorio_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.main_territorio_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_territorio_id_seq OWNER TO dtygel;

--
-- Name: main_territorio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.main_territorio_id_seq OWNED BY public.main_territorio.id;


--
-- Name: main_unidade; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.main_unidade (
    id integer NOT NULL,
    deleted timestamp with time zone,
    nome character varying(60) NOT NULL,
    criacao timestamp with time zone NOT NULL,
    ultima_alteracao timestamp with time zone NOT NULL
);


ALTER TABLE public.main_unidade OWNER TO dtygel;

--
-- Name: main_unidade_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.main_unidade_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_unidade_id_seq OWNER TO dtygel;

--
-- Name: main_unidade_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.main_unidade_id_seq OWNED BY public.main_unidade.id;


--
-- Name: municipios_diocese; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.municipios_diocese (
    id integer NOT NULL,
    nome character varying(500) NOT NULL
);


ALTER TABLE public.municipios_diocese OWNER TO dtygel;

--
-- Name: municipios_diocese_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.municipios_diocese_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.municipios_diocese_id_seq OWNER TO dtygel;

--
-- Name: municipios_diocese_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.municipios_diocese_id_seq OWNED BY public.municipios_diocese.id;


--
-- Name: municipios_municipio; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.municipios_municipio (
    id integer NOT NULL,
    nome character varying(500) NOT NULL,
    diocese_id integer,
    regiao_imediata_id integer,
    uf_id integer
);


ALTER TABLE public.municipios_municipio OWNER TO dtygel;

--
-- Name: municipios_municipio_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.municipios_municipio_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.municipios_municipio_id_seq OWNER TO dtygel;

--
-- Name: municipios_municipio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.municipios_municipio_id_seq OWNED BY public.municipios_municipio.id;


--
-- Name: municipios_regiao; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.municipios_regiao (
    id integer NOT NULL,
    nome character varying(500) NOT NULL
);


ALTER TABLE public.municipios_regiao OWNER TO dtygel;

--
-- Name: municipios_regiao_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.municipios_regiao_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.municipios_regiao_id_seq OWNER TO dtygel;

--
-- Name: municipios_regiao_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.municipios_regiao_id_seq OWNED BY public.municipios_regiao.id;


--
-- Name: municipios_regiaoimediata; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.municipios_regiaoimediata (
    id integer NOT NULL,
    nome character varying(500) NOT NULL,
    regiao_intermediaria_id integer
);


ALTER TABLE public.municipios_regiaoimediata OWNER TO dtygel;

--
-- Name: municipios_regiaoimediata_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.municipios_regiaoimediata_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.municipios_regiaoimediata_id_seq OWNER TO dtygel;

--
-- Name: municipios_regiaoimediata_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.municipios_regiaoimediata_id_seq OWNED BY public.municipios_regiaoimediata.id;


--
-- Name: municipios_regiaointermediaria; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.municipios_regiaointermediaria (
    id integer NOT NULL,
    nome character varying(500) NOT NULL,
    uf_id integer
);


ALTER TABLE public.municipios_regiaointermediaria OWNER TO dtygel;

--
-- Name: municipios_regiaointermediaria_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.municipios_regiaointermediaria_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.municipios_regiaointermediaria_id_seq OWNER TO dtygel;

--
-- Name: municipios_regiaointermediaria_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.municipios_regiaointermediaria_id_seq OWNED BY public.municipios_regiaointermediaria.id;


--
-- Name: municipios_uf; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.municipios_uf (
    id integer NOT NULL,
    nome character varying(500) NOT NULL,
    sigla character varying(500) NOT NULL,
    regiao_id integer
);


ALTER TABLE public.municipios_uf OWNER TO dtygel;

--
-- Name: municipios_uf_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.municipios_uf_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.municipios_uf_id_seq OWNER TO dtygel;

--
-- Name: municipios_uf_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.municipios_uf_id_seq OWNED BY public.municipios_uf.id;


--
-- Name: organizacao; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.organizacao (
    id integer NOT NULL,
    nome character varying(80) NOT NULL,
    criacao timestamp with time zone NOT NULL,
    ultima_alteracao timestamp with time zone NOT NULL,
    deleted timestamp with time zone
);


ALTER TABLE public.organizacao OWNER TO dtygel;

--
-- Name: organizacao_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.organizacao_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.organizacao_id_seq OWNER TO dtygel;

--
-- Name: organizacao_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.organizacao_id_seq OWNED BY public.organizacao.id;


--
-- Name: qualitativa_analise; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.qualitativa_analise (
    id integer NOT NULL,
    ano smallint NOT NULL,
    ano_referencia smallint NOT NULL,
    criacao timestamp with time zone NOT NULL,
    ultima_alteracao timestamp with time zone NOT NULL,
    agroecossistema_id integer NOT NULL,
    deleted timestamp with time zone
);


ALTER TABLE public.qualitativa_analise OWNER TO dtygel;

--
-- Name: qualitativa_analise_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.qualitativa_analise_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.qualitativa_analise_id_seq OWNER TO dtygel;

--
-- Name: qualitativa_analise_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.qualitativa_analise_id_seq OWNED BY public.qualitativa_analise.id;


--
-- Name: qualitativa_atributo; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.qualitativa_atributo (
    id integer NOT NULL,
    nome character varying(90) NOT NULL,
    deleted timestamp with time zone
);


ALTER TABLE public.qualitativa_atributo OWNER TO dtygel;

--
-- Name: qualitativa_atributo_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.qualitativa_atributo_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.qualitativa_atributo_id_seq OWNER TO dtygel;

--
-- Name: qualitativa_atributo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.qualitativa_atributo_id_seq OWNED BY public.qualitativa_atributo.id;


--
-- Name: qualitativa_avaliacaoparametro; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.qualitativa_avaliacaoparametro (
    id integer NOT NULL,
    mudancas text,
    justificativa text,
    ultima_alteracao timestamp with time zone NOT NULL,
    analise_id integer NOT NULL,
    parametro_id integer NOT NULL,
    criacao timestamp with time zone NOT NULL,
    deleted timestamp with time zone
);


ALTER TABLE public.qualitativa_avaliacaoparametro OWNER TO dtygel;

--
-- Name: qualitativa_avaliacaocriterio_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.qualitativa_avaliacaocriterio_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.qualitativa_avaliacaocriterio_id_seq OWNER TO dtygel;

--
-- Name: qualitativa_avaliacaocriterio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.qualitativa_avaliacaocriterio_id_seq OWNED BY public.qualitativa_avaliacaoparametro.id;


--
-- Name: qualitativa_parametro; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.qualitativa_parametro (
    id integer NOT NULL,
    nome character varying(90) NOT NULL,
    atributo_id integer NOT NULL,
    subsection character varying(200) NOT NULL,
    agroecossistema_id integer,
    deleted timestamp with time zone,
    ordem integer NOT NULL
);


ALTER TABLE public.qualitativa_parametro OWNER TO dtygel;

--
-- Name: qualitativa_criterio_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.qualitativa_criterio_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.qualitativa_criterio_id_seq OWNER TO dtygel;

--
-- Name: qualitativa_criterio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.qualitativa_criterio_id_seq OWNED BY public.qualitativa_parametro.id;


--
-- Name: qualitativa_qualificacao; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.qualitativa_qualificacao (
    id integer NOT NULL,
    ano smallint NOT NULL,
    qualificacao integer,
    ultima_alteracao timestamp with time zone NOT NULL,
    agroecossistema_id integer NOT NULL,
    parametro_id integer NOT NULL,
    criacao timestamp with time zone NOT NULL,
    deleted timestamp with time zone
);


ALTER TABLE public.qualitativa_qualificacao OWNER TO dtygel;

--
-- Name: qualitativa_qualificacao_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.qualitativa_qualificacao_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.qualitativa_qualificacao_id_seq OWNER TO dtygel;

--
-- Name: qualitativa_qualificacao_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.qualitativa_qualificacao_id_seq OWNED BY public.qualitativa_qualificacao.id;


--
-- Name: sitetree_tree; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.sitetree_tree (
    id integer NOT NULL,
    title character varying(100) NOT NULL,
    alias character varying(80) NOT NULL
);


ALTER TABLE public.sitetree_tree OWNER TO dtygel;

--
-- Name: sitetree_tree_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.sitetree_tree_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sitetree_tree_id_seq OWNER TO dtygel;

--
-- Name: sitetree_tree_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.sitetree_tree_id_seq OWNED BY public.sitetree_tree.id;


--
-- Name: sitetree_treeitem; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.sitetree_treeitem (
    id integer NOT NULL,
    title character varying(100) NOT NULL,
    hint character varying(200) NOT NULL,
    url character varying(200) NOT NULL,
    urlaspattern boolean NOT NULL,
    hidden boolean NOT NULL,
    alias character varying(80),
    description text NOT NULL,
    inmenu boolean NOT NULL,
    inbreadcrumbs boolean NOT NULL,
    insitetree boolean NOT NULL,
    access_loggedin boolean NOT NULL,
    access_guest boolean NOT NULL,
    access_restricted boolean NOT NULL,
    access_perm_type integer NOT NULL,
    sort_order integer NOT NULL,
    parent_id integer,
    tree_id integer NOT NULL
);


ALTER TABLE public.sitetree_treeitem OWNER TO dtygel;

--
-- Name: sitetree_treeitem_access_permissions; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.sitetree_treeitem_access_permissions (
    id integer NOT NULL,
    treeitem_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.sitetree_treeitem_access_permissions OWNER TO dtygel;

--
-- Name: sitetree_treeitem_access_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.sitetree_treeitem_access_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sitetree_treeitem_access_permissions_id_seq OWNER TO dtygel;

--
-- Name: sitetree_treeitem_access_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.sitetree_treeitem_access_permissions_id_seq OWNED BY public.sitetree_treeitem_access_permissions.id;


--
-- Name: sitetree_treeitem_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.sitetree_treeitem_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sitetree_treeitem_id_seq OWNER TO dtygel;

--
-- Name: sitetree_treeitem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.sitetree_treeitem_id_seq OWNED BY public.sitetree_treeitem.id;


--
-- Name: socialaccount_socialaccount; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.socialaccount_socialaccount (
    id integer NOT NULL,
    provider character varying(30) NOT NULL,
    uid character varying(191) NOT NULL,
    last_login timestamp with time zone NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    extra_data text NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.socialaccount_socialaccount OWNER TO dtygel;

--
-- Name: socialaccount_socialaccount_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.socialaccount_socialaccount_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.socialaccount_socialaccount_id_seq OWNER TO dtygel;

--
-- Name: socialaccount_socialaccount_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.socialaccount_socialaccount_id_seq OWNED BY public.socialaccount_socialaccount.id;


--
-- Name: socialaccount_socialapp; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.socialaccount_socialapp (
    id integer NOT NULL,
    provider character varying(30) NOT NULL,
    name character varying(40) NOT NULL,
    client_id character varying(191) NOT NULL,
    secret character varying(191) NOT NULL,
    key character varying(191) NOT NULL
);


ALTER TABLE public.socialaccount_socialapp OWNER TO dtygel;

--
-- Name: socialaccount_socialapp_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.socialaccount_socialapp_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.socialaccount_socialapp_id_seq OWNER TO dtygel;

--
-- Name: socialaccount_socialapp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.socialaccount_socialapp_id_seq OWNED BY public.socialaccount_socialapp.id;


--
-- Name: socialaccount_socialapp_sites; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.socialaccount_socialapp_sites (
    id integer NOT NULL,
    socialapp_id integer NOT NULL,
    site_id integer NOT NULL
);


ALTER TABLE public.socialaccount_socialapp_sites OWNER TO dtygel;

--
-- Name: socialaccount_socialapp_sites_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.socialaccount_socialapp_sites_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.socialaccount_socialapp_sites_id_seq OWNER TO dtygel;

--
-- Name: socialaccount_socialapp_sites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.socialaccount_socialapp_sites_id_seq OWNED BY public.socialaccount_socialapp_sites.id;


--
-- Name: socialaccount_socialtoken; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.socialaccount_socialtoken (
    id integer NOT NULL,
    token text NOT NULL,
    token_secret text NOT NULL,
    expires_at timestamp with time zone,
    account_id integer NOT NULL,
    app_id integer NOT NULL
);


ALTER TABLE public.socialaccount_socialtoken OWNER TO dtygel;

--
-- Name: socialaccount_socialtoken_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.socialaccount_socialtoken_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.socialaccount_socialtoken_id_seq OWNER TO dtygel;

--
-- Name: socialaccount_socialtoken_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.socialaccount_socialtoken_id_seq OWNED BY public.socialaccount_socialtoken.id;


--
-- Name: usuaria; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.usuaria (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.usuaria OWNER TO dtygel;

--
-- Name: usuaria_groups; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.usuaria_groups (
    id integer NOT NULL,
    usuaria_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.usuaria_groups OWNER TO dtygel;

--
-- Name: usuaria_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.usuaria_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuaria_groups_id_seq OWNER TO dtygel;

--
-- Name: usuaria_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.usuaria_groups_id_seq OWNED BY public.usuaria_groups.id;


--
-- Name: usuaria_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.usuaria_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuaria_id_seq OWNER TO dtygel;

--
-- Name: usuaria_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.usuaria_id_seq OWNED BY public.usuaria.id;


--
-- Name: usuaria_user_permissions; Type: TABLE; Schema: public; Owner: dtygel
--

CREATE TABLE public.usuaria_user_permissions (
    id integer NOT NULL,
    usuaria_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.usuaria_user_permissions OWNER TO dtygel;

--
-- Name: usuaria_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: dtygel
--

CREATE SEQUENCE public.usuaria_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuaria_user_permissions_id_seq OWNER TO dtygel;

--
-- Name: usuaria_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dtygel
--

ALTER SEQUENCE public.usuaria_user_permissions_id_seq OWNED BY public.usuaria_user_permissions.id;


--
-- Name: view_composicao_nsga_categoria; Type: VIEW; Schema: public; Owner: dtygel
--

CREATE VIEW public.view_composicao_nsga_categoria AS
 SELECT comp_nsga.id AS composicao_nsga_id,
    comp_nsga.ciclo_anual_referencia_id,
    p2.sexo,
        CASE
            WHEN comp_nsga.responsavel THEN 'A'::text
            WHEN (p2.idade < (14)::double precision) THEN 'C'::text
            WHEN ((p2.idade >= (14)::double precision) AND (p2.idade <= (29)::double precision)) THEN 'J'::text
            ELSE 'O'::text
        END AS categoria
   FROM (( SELECT pessoa.id,
            pessoa.sexo,
            pessoa.nome,
            date_part('year'::text, age((pessoa.data_nascimento)::timestamp with time zone)) AS idade
           FROM public.agroecossistema_pessoa pessoa
          WHERE (pessoa.deleted IS NULL)) p2
     JOIN public.agroecossistema_composicaonsga comp_nsga ON ((p2.id = comp_nsga.pessoa_id)));


ALTER TABLE public.view_composicao_nsga_categoria OWNER TO dtygel;

--
-- Name: view_analise_tempostrabalho; Type: VIEW; Schema: public; Owner: dtygel
--

CREATE VIEW public.view_analise_tempostrabalho AS
 SELECT row_number() OVER () AS id,
    p2.analise_id,
    p2.sexo,
    p2.categoria,
    sum(p2.horas_mercantil) AS horas_mercantil,
    sum(p2.horas_domestico_cuidados) AS horas_domestico_cuidados,
    sum(p2.horas_participacao_social) AS horas_participacao_social,
    sum(p2.horas_pluriatividade) AS horas_pluriatividade
   FROM ( SELECT analise.id AS analise_id,
            comp_nsga.sexo,
            comp_nsga.categoria,
            sum(ss_tt.horas) AS horas_mercantil,
            0 AS horas_domestico_cuidados,
            0 AS horas_participacao_social,
            0 AS horas_pluriatividade
           FROM (((public.economica_analise analise
             JOIN public.economica_subsistema ss ON ((analise.id = ss.analise_id)))
             JOIN public.economica_subsistematempotrabalho ss_tt ON ((ss.id = ss_tt.subsistema_id)))
             JOIN public.view_composicao_nsga_categoria comp_nsga ON ((ss_tt.composicao_nsga_id = comp_nsga.composicao_nsga_id)))
          GROUP BY comp_nsga.sexo, comp_nsga.categoria, analise.id
        UNION
         SELECT analise.id AS analise_id,
            comp_nsga.sexo,
            comp_nsga.categoria,
            0 AS horas_mercantil,
            tt.horas_domestico_cuidados,
            tt.horas_participacao_social,
            tt.horas_pluriatividade
           FROM (((public.economica_analise analise
             JOIN public.economica_subsistema ss ON ((analise.id = ss.analise_id)))
             JOIN public.economica_tempotrabalho tt ON ((analise.id = tt.analise_id)))
             JOIN public.view_composicao_nsga_categoria comp_nsga ON ((tt.composicao_nsga_id = comp_nsga.composicao_nsga_id)))) p2
  GROUP BY p2.sexo, p2.categoria, p2.analise_id;


ALTER TABLE public.view_analise_tempostrabalho OWNER TO dtygel;

--
-- Name: view_economica_analise_totais; Type: VIEW; Schema: public; Owner: dtygel
--

CREATE VIEW public.view_economica_analise_totais AS
 SELECT row_number() OVER () AS id,
    analise.id AS analise_id,
    sum(
        CASE
            WHEN ((rendanaoagricola.tipo)::text = 'P'::text) THEN (rendanaoagricola.quantidade * rendanaoagricola.valor_unitario)
            ELSE (0)::numeric
        END) AS renda_pluriatividade,
    sum(
        CASE
            WHEN ((rendanaoagricola.tipo)::text = 'T'::text) THEN (rendanaoagricola.quantidade * rendanaoagricola.valor_unitario)
            ELSE (0)::numeric
        END) AS renda_transferencia
   FROM (public.economica_analise analise
     JOIN public.economica_rendanaoagricola rendanaoagricola ON ((analise.id = rendanaoagricola.analise_id)))
  GROUP BY analise.id;


ALTER TABLE public.view_economica_analise_totais OWNER TO dtygel;

--
-- Name: view_economica_subsistema_totais; Type: VIEW; Schema: public; Owner: dtygel
--

CREATE VIEW public.view_economica_subsistema_totais AS
 SELECT row_number() OVER () AS id,
    ss_data2.subsistema_id,
    ss_data2.analise_id,
    ss_data2.extrativismo,
    ss_data2.nome,
    ss_data2.area,
    ss_data2.ci,
    ss_data2.ci_f,
    ss_data2.ci_t,
    ss_data2.pp,
    ss_data2.pt,
    ss_data2.pt_f,
    ss_data2.pt_t,
    ss_data2.pb_venda,
    ss_data2.pb_autoconsumo,
    ss_data2.pb_doacoes_trocas,
    ss_data2.pb_estoque,
    ss_data2.pb_insumos_produzidos,
    ss_data2.reciprocidade,
    ss_data2.pb,
    ss_data2.rb,
    ss_data2.cp,
    (ss_data2.rb - ss_data2.ci) AS va,
    (ss_data2.rb - ss_data2.ci_f) AS vat,
    ((ss_data2.rb - ss_data2.ci) - ss_data2.pt) AS ra,
    ((1)::numeric - (ss_data2.pt / (ss_data2.rb - ss_data2.ci))) AS valor_agregado,
    ((ss_data2.rb - ss_data2.ci) / ss_data2.area) AS produtividade_terra,
    (((ss_data2.rb - ss_data2.ci) - ss_data2.pt) / ss_data2.area) AS ra_area,
    (ss_data2.pb_venda - ss_data2.cp) AS ram,
    ((ss_data2.pb_venda - ss_data2.cp) / ss_data2.cp) AS rentabilidade_monetaria,
    ((ss_data2.rb - ss_data2.ci) / ss_data2.cp) AS rentabilidade_total,
    ((ss_data2.rb - ss_data2.ci) / ss_data2.rb) AS endogeneidade,
    ((ss_data2.ci + ss_data2.pt) / (((ss_data2.ci + ss_data2.pt) + ss_data2.pp) + ss_data2.reciprocidade)) AS mercantilizacao
   FROM ( SELECT ss_data.subsistema_id,
            ss_data.analise_id,
            ss_data.extrativismo,
            ss_data.nome,
            ss_data.area,
            ss_data.ci,
            ss_data.ci_f,
            ss_data.ci_t,
            ss_data.pp,
            ss_data.pt,
            ss_data.pt_f,
            ss_data.pt_t,
            ss_data.pb_venda,
            ss_data.pb_autoconsumo,
            ss_data.pb_doacoes_trocas,
            ss_data.pb_estoque,
            ss_data.pb_insumos_produzidos,
            ss_data.reciprocidade,
            (((ss_data.pb_venda + ss_data.pb_estoque) + ss_data.pb_doacoes_trocas) + ss_data.pb_autoconsumo) AS pb,
            ((ss_data.pb_venda + ss_data.pb_doacoes_trocas) + ss_data.pb_autoconsumo) AS rb,
            (ss_data.ci + ss_data.pt) AS cp
           FROM ( SELECT subsistema.id AS subsistema_id,
                    subsistema.analise_id,
                    subsistema.extrativismo,
                    subsistema.nome,
                    subsistema.area,
                    sum(
                        CASE
                            WHEN ((ss_insumo.tipo)::text = 'C'::text) THEN (ss_insumo.quantidade * ss_insumo.valor_unitario)
                            ELSE (0)::numeric
                        END) AS ci,
                    sum(
                        CASE
                            WHEN (((ss_insumo.origem)::text = 'F'::text) AND ((ss_insumo.tipo)::text = 'C'::text)) THEN (ss_insumo.quantidade * ss_insumo.valor_unitario)
                            ELSE (0)::numeric
                        END) AS ci_f,
                    sum(
                        CASE
                            WHEN (((ss_insumo.origem)::text = 'T'::text) AND ((ss_insumo.tipo)::text = 'C'::text)) THEN (ss_insumo.quantidade * ss_insumo.valor_unitario)
                            ELSE (0)::numeric
                        END) AS ci_t,
                    sum(
                        CASE
                            WHEN ((ss_insumo.tipo)::text = 'P'::text) THEN (ss_insumo.quantidade * ss_insumo.valor_unitario)
                            ELSE (0)::numeric
                        END) AS pp,
                    COALESCE(sum((ss_pagamentoterceiros.quantidade * ss_pagamentoterceiros.valor_unitario)), (0)::numeric) AS pt,
                    sum(
                        CASE
                            WHEN ((ss_pagamentoterceiros.origem)::text = 'F'::text) THEN (ss_pagamentoterceiros.quantidade * ss_pagamentoterceiros.valor_unitario)
                            ELSE (0)::numeric
                        END) AS pt_f,
                    sum(
                        CASE
                            WHEN ((ss_pagamentoterceiros.origem)::text = 'T'::text) THEN (ss_pagamentoterceiros.quantidade * ss_pagamentoterceiros.valor_unitario)
                            ELSE (0)::numeric
                        END) AS pt_t,
                    COALESCE(sum((ss_producao.quantidade_venda * ss_producao.valor_unitario)), (0)::numeric) AS pb_venda,
                    COALESCE(sum((ss_producao.quantidade_autoconsumo * ss_producao.valor_unitario)), (0)::numeric) AS pb_autoconsumo,
                    COALESCE(sum((ss_producao.quantidade_doacoes_trocas * ss_producao.valor_unitario)), (0)::numeric) AS pb_doacoes_trocas,
                    COALESCE(sum((ss_producao.quantidade_estoque * ss_producao.valor_unitario)), (0)::numeric) AS pb_estoque,
                    COALESCE(sum((ss_producao.quantidade_insumos_produzidos * ss_producao.valor_unitario)), (0)::numeric) AS pb_insumos_produzidos,
                    COALESCE(sum((ss_reciprocidade.quantidade * ss_producao.valor_unitario)), (0)::numeric) AS reciprocidade
                   FROM ((((public.economica_subsistema subsistema
                     JOIN public.economica_subsistemainsumo ss_insumo ON ((subsistema.id = ss_insumo.subsistema_id)))
                     JOIN public.economica_subsistemaproducao ss_producao ON ((subsistema.id = ss_producao.subsistema_id)))
                     JOIN public.economica_subsistemapagamentoterceiros ss_pagamentoterceiros ON ((subsistema.id = ss_pagamentoterceiros.subsistema_id)))
                     JOIN public.economica_subsistemaaquisicaoreciprocidade ss_reciprocidade ON ((subsistema.id = ss_reciprocidade.subsistema_id)))
                  GROUP BY subsistema.analise_id, subsistema.extrativismo, subsistema.nome, subsistema.area, subsistema.id) ss_data) ss_data2;


ALTER TABLE public.view_economica_subsistema_totais OWNER TO dtygel;

--
-- Name: account_emailaddress id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.account_emailaddress ALTER COLUMN id SET DEFAULT nextval('public.account_emailaddress_id_seq'::regclass);


--
-- Name: account_emailconfirmation id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.account_emailconfirmation ALTER COLUMN id SET DEFAULT nextval('public.account_emailconfirmation_id_seq'::regclass);


--
-- Name: agroecossistema id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.agroecossistema ALTER COLUMN id SET DEFAULT nextval('public.agroecossistema_id_seq'::regclass);


--
-- Name: agroecossistema_cicloanualreferencia id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.agroecossistema_cicloanualreferencia ALTER COLUMN id SET DEFAULT nextval('public.agroecossistema_cicloanualreferencia_id_seq'::regclass);


--
-- Name: agroecossistema_composicaonsga id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.agroecossistema_composicaonsga ALTER COLUMN id SET DEFAULT nextval('public.agroecossistema_composicaonsga_id_seq'::regclass);


--
-- Name: agroecossistema_pessoa id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.agroecossistema_pessoa ALTER COLUMN id SET DEFAULT nextval('public.agroecossistema_pessoa_id_seq'::regclass);


--
-- Name: agroecossistema_terra id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.agroecossistema_terra ALTER COLUMN id SET DEFAULT nextval('public.agroecossistema_terra_id_seq'::regclass);


--
-- Name: anexos_anexo id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.anexos_anexo ALTER COLUMN id SET DEFAULT nextval('public.anexos_anexo_id_seq'::regclass);


--
-- Name: anexos_anexorecipiente id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.anexos_anexorecipiente ALTER COLUMN id SET DEFAULT nextval('public.anexos_anexorecipiente_id_seq'::regclass);


--
-- Name: anexos_tipoanexo id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.anexos_tipoanexo ALTER COLUMN id SET DEFAULT nextval('public.anexos_tipoanexo_id_seq'::regclass);


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: cadernodecampo_anotacao id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.cadernodecampo_anotacao ALTER COLUMN id SET DEFAULT nextval('public.cadernodecampo_anotacao_id_seq'::regclass);


--
-- Name: comunidade id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.comunidade ALTER COLUMN id SET DEFAULT nextval('public.comunidade_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: django_site id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.django_site ALTER COLUMN id SET DEFAULT nextval('public.django_site_id_seq'::regclass);


--
-- Name: django_summernote_attachment id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.django_summernote_attachment ALTER COLUMN id SET DEFAULT nextval('public.django_summernote_attachment_id_seq'::regclass);


--
-- Name: economica_analise id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_analise ALTER COLUMN id SET DEFAULT nextval('public.economica_analise_id_seq'::regclass);


--
-- Name: economica_estoqueinsumos id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_estoqueinsumos ALTER COLUMN id SET DEFAULT nextval('public.economica_estoqueinsumos_id_seq'::regclass);


--
-- Name: economica_pagamentoterceiros id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_pagamentoterceiros ALTER COLUMN id SET DEFAULT nextval('public.economica_pagamentoterceiros_id_seq'::regclass);


--
-- Name: economica_patrimonio id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_patrimonio ALTER COLUMN id SET DEFAULT nextval('public.economica_patrimonio_id_seq'::regclass);


--
-- Name: economica_rendanaoagricola id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_rendanaoagricola ALTER COLUMN id SET DEFAULT nextval('public.economica_rendanaoagricola_id_seq'::regclass);


--
-- Name: economica_subsistema id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_subsistema ALTER COLUMN id SET DEFAULT nextval('public.economica_subsistema_id_seq'::regclass);


--
-- Name: economica_subsistemaaquisicaoreciprocidade id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_subsistemaaquisicaoreciprocidade ALTER COLUMN id SET DEFAULT nextval('public.economica_subsistemaaquisicaoreciprocidade_id_seq'::regclass);


--
-- Name: economica_subsistemainsumo id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_subsistemainsumo ALTER COLUMN id SET DEFAULT nextval('public.economica_subsistemaaquisicao_id_seq'::regclass);


--
-- Name: economica_subsistemapagamentoterceiros id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_subsistemapagamentoterceiros ALTER COLUMN id SET DEFAULT nextval('public.economica_subsistemapagamentoterceiros_id_seq'::regclass);


--
-- Name: economica_subsistemaproducao id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_subsistemaproducao ALTER COLUMN id SET DEFAULT nextval('public.economica_subsistemaproducao_id_seq'::regclass);


--
-- Name: economica_subsistematempotrabalho id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_subsistematempotrabalho ALTER COLUMN id SET DEFAULT nextval('public.economica_subsistematempotrabalho_id_seq'::regclass);


--
-- Name: economica_tempotrabalho id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_tempotrabalho ALTER COLUMN id SET DEFAULT nextval('public.economica_tempotrabalho_id_seq'::regclass);


--
-- Name: linhadotempo_dimensao id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.linhadotempo_dimensao ALTER COLUMN id SET DEFAULT nextval('public.linhadotempo_dimensao_id_seq'::regclass);


--
-- Name: linhadotempo_evento id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.linhadotempo_evento ALTER COLUMN id SET DEFAULT nextval('public.linhadotempo_evento_id_seq'::regclass);


--
-- Name: linhadotempo_eventogrupo id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.linhadotempo_eventogrupo ALTER COLUMN id SET DEFAULT nextval('public.linhadotempo_eventogrupo_id_seq'::regclass);


--
-- Name: linhadotempo_eventogrupo_eventos id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.linhadotempo_eventogrupo_eventos ALTER COLUMN id SET DEFAULT nextval('public.linhadotempo_eventogrupo_eventos_id_seq'::regclass);


--
-- Name: linhadotempo_tipodimensao id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.linhadotempo_tipodimensao ALTER COLUMN id SET DEFAULT nextval('public.linhadotempo_tipodimensao_id_seq'::regclass);


--
-- Name: main_organizacaousuaria id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.main_organizacaousuaria ALTER COLUMN id SET DEFAULT nextval('public.main_organizacaousuaria_id_seq'::regclass);


--
-- Name: main_produto id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.main_produto ALTER COLUMN id SET DEFAULT nextval('public.main_produto_id_seq'::regclass);


--
-- Name: main_servico id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.main_servico ALTER COLUMN id SET DEFAULT nextval('public.main_servico_id_seq'::regclass);


--
-- Name: main_territorio id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.main_territorio ALTER COLUMN id SET DEFAULT nextval('public.main_territorio_id_seq'::regclass);


--
-- Name: main_unidade id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.main_unidade ALTER COLUMN id SET DEFAULT nextval('public.main_unidade_id_seq'::regclass);


--
-- Name: municipios_diocese id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.municipios_diocese ALTER COLUMN id SET DEFAULT nextval('public.municipios_diocese_id_seq'::regclass);


--
-- Name: municipios_municipio id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.municipios_municipio ALTER COLUMN id SET DEFAULT nextval('public.municipios_municipio_id_seq'::regclass);


--
-- Name: municipios_regiao id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.municipios_regiao ALTER COLUMN id SET DEFAULT nextval('public.municipios_regiao_id_seq'::regclass);


--
-- Name: municipios_regiaoimediata id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.municipios_regiaoimediata ALTER COLUMN id SET DEFAULT nextval('public.municipios_regiaoimediata_id_seq'::regclass);


--
-- Name: municipios_regiaointermediaria id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.municipios_regiaointermediaria ALTER COLUMN id SET DEFAULT nextval('public.municipios_regiaointermediaria_id_seq'::regclass);


--
-- Name: municipios_uf id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.municipios_uf ALTER COLUMN id SET DEFAULT nextval('public.municipios_uf_id_seq'::regclass);


--
-- Name: organizacao id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.organizacao ALTER COLUMN id SET DEFAULT nextval('public.organizacao_id_seq'::regclass);


--
-- Name: qualitativa_analise id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.qualitativa_analise ALTER COLUMN id SET DEFAULT nextval('public.qualitativa_analise_id_seq'::regclass);


--
-- Name: qualitativa_atributo id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.qualitativa_atributo ALTER COLUMN id SET DEFAULT nextval('public.qualitativa_atributo_id_seq'::regclass);


--
-- Name: qualitativa_avaliacaoparametro id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.qualitativa_avaliacaoparametro ALTER COLUMN id SET DEFAULT nextval('public.qualitativa_avaliacaocriterio_id_seq'::regclass);


--
-- Name: qualitativa_parametro id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.qualitativa_parametro ALTER COLUMN id SET DEFAULT nextval('public.qualitativa_criterio_id_seq'::regclass);


--
-- Name: qualitativa_qualificacao id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.qualitativa_qualificacao ALTER COLUMN id SET DEFAULT nextval('public.qualitativa_qualificacao_id_seq'::regclass);


--
-- Name: sitetree_tree id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.sitetree_tree ALTER COLUMN id SET DEFAULT nextval('public.sitetree_tree_id_seq'::regclass);


--
-- Name: sitetree_treeitem id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.sitetree_treeitem ALTER COLUMN id SET DEFAULT nextval('public.sitetree_treeitem_id_seq'::regclass);


--
-- Name: sitetree_treeitem_access_permissions id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.sitetree_treeitem_access_permissions ALTER COLUMN id SET DEFAULT nextval('public.sitetree_treeitem_access_permissions_id_seq'::regclass);


--
-- Name: socialaccount_socialaccount id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.socialaccount_socialaccount ALTER COLUMN id SET DEFAULT nextval('public.socialaccount_socialaccount_id_seq'::regclass);


--
-- Name: socialaccount_socialapp id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.socialaccount_socialapp ALTER COLUMN id SET DEFAULT nextval('public.socialaccount_socialapp_id_seq'::regclass);


--
-- Name: socialaccount_socialapp_sites id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.socialaccount_socialapp_sites ALTER COLUMN id SET DEFAULT nextval('public.socialaccount_socialapp_sites_id_seq'::regclass);


--
-- Name: socialaccount_socialtoken id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.socialaccount_socialtoken ALTER COLUMN id SET DEFAULT nextval('public.socialaccount_socialtoken_id_seq'::regclass);


--
-- Name: usuaria id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.usuaria ALTER COLUMN id SET DEFAULT nextval('public.usuaria_id_seq'::regclass);


--
-- Name: usuaria_groups id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.usuaria_groups ALTER COLUMN id SET DEFAULT nextval('public.usuaria_groups_id_seq'::regclass);


--
-- Name: usuaria_user_permissions id; Type: DEFAULT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.usuaria_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.usuaria_user_permissions_id_seq'::regclass);


--
-- Data for Name: account_emailaddress; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.account_emailaddress (id, email, verified, "primary", user_id) FROM stdin;
\.


--
-- Data for Name: account_emailconfirmation; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.account_emailconfirmation (id, created, sent, key, email_address_id) FROM stdin;
\.


--
-- Data for Name: agroecossistema; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.agroecossistema (id, geoponto, poligono, criacao, ultima_alteracao, nsga, tipo_gestao_nsga, autor_id, comunidade_id, organizacao_id, territorio_id, parametros_inativos, deleted, mes_fim_ciclo, mes_inicio_ciclo) FROM stdin;
1	\N	\N	2019-04-05 09:49:29.03487-03	2019-05-08 15:16:59.135347-03	Família Brand	F	1	1	1	\N	{"inativos": [15, 16, 17, 18, 19, 21, 22, 24, 25, 27, 28, 29, 31, 32, 33, 34, 35]}	\N	6	7
2	\N	\N	2019-05-10 09:16:56.856505-03	2019-05-10 09:16:56.856534-03	João e Maria	F	2	2	2	\N	{}	\N	3	4
3	\N	\N	2019-05-12 20:10:53.272814-03	2019-05-12 20:10:53.272862-03	Mamamia	C	2	1	2	\N	{}	\N	9	4
4	\N	\N	2019-05-14 18:15:20.475218-03	2019-05-14 18:15:20.475274-03	Jucilene da Conceição e Rondom dos Santos	F	1	3	2	\N	{}	\N	12	1
\.


--
-- Data for Name: agroecossistema_cicloanualreferencia; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.agroecossistema_cicloanualreferencia (id, deleted, ano, criacao, ultima_alteracao, agroecossistema_id) FROM stdin;
2	\N	2015	2019-05-10 07:20:09.176969-03	2019-05-10 07:20:09.176993-03	1
4	\N	2014	2019-05-10 07:24:33.50118-03	2019-05-10 07:24:33.501209-03	1
6	\N	2013	2019-05-10 07:28:25.584272-03	2019-05-10 07:28:25.584297-03	1
8	\N	2012	2019-05-10 07:29:04.311295-03	2019-05-10 07:29:04.311321-03	1
10	\N	2011	2019-05-10 07:29:51.583052-03	2019-05-10 07:29:51.583084-03	1
12	\N	2010	2019-05-10 07:30:33.138204-03	2019-05-10 07:30:33.138228-03	1
14	\N	2009	2019-05-10 08:09:55.309797-03	2019-05-10 08:09:55.309822-03	1
15	\N	2019	2019-05-10 08:12:01.468444-03	2019-05-10 08:12:01.46847-03	1
1	\N	2018	2019-05-09 16:40:46.3896-03	2019-05-10 15:48:10.994287-03	1
16	\N	2017	2019-05-14 18:16:53.843303-03	2019-05-14 18:42:53.579875-03	4
\.


--
-- Data for Name: agroecossistema_composicaonsga; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.agroecossistema_composicaonsga (id, utf_agroecossistema, utf_pluriatividade, responsavel, criacao, ultima_alteracao, ciclo_anual_referencia_id, pessoa_id) FROM stdin;
1	0.20	0.10	t	2019-05-10 09:51:35.80282-03	2019-05-10 09:51:35.802847-03	1	2
3	1.00	0.00	t	2019-05-10 10:07:56.714443-03	2019-05-10 10:07:56.714469-03	1	3
2	0.80	0.20	f	2019-05-10 09:51:35.833297-03	2019-05-10 09:51:35.833325-03	1	1
4	0.80	0.20	t	2019-05-14 18:38:14.621105-03	2019-05-14 18:38:14.621134-03	16	4
5	0.50	0.00	t	2019-05-14 18:39:05.065465-03	2019-05-14 18:39:05.065501-03	16	6
6	0.50	0.00	f	2019-05-14 18:39:18.717912-03	2019-05-14 18:39:18.717944-03	16	5
7	0.30	0.00	f	2019-05-14 18:40:15.171161-03	2019-05-14 18:40:15.171208-03	16	7
8	0.30	0.00	f	2019-05-14 18:40:15.17299-03	2019-05-14 18:40:15.173031-03	16	8
9	0.40	0.00	f	2019-05-14 18:40:15.174369-03	2019-05-14 18:40:19.975902-03	16	9
10	0.50	0.00	f	2019-05-14 18:42:15.698441-03	2019-05-14 18:42:15.698471-03	16	13
11	0.00	0.10	f	2019-05-14 18:42:15.699593-03	2019-05-14 18:42:15.699616-03	16	14
\.


--
-- Data for Name: agroecossistema_pessoa; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.agroecossistema_pessoa (id, deleted, nome, sexo, data_nascimento, criacao, ultima_alteracao, agroecossistema_id) FROM stdin;
1	\N	Maria	F	1974-01-01	2019-05-10 06:51:29.194835-03	2019-05-10 06:51:29.194878-03	1
2	\N	Maria José	F	2019-04-29	2019-05-10 06:59:18.440332-03	2019-05-10 09:43:42.545429-03	1
3	\N	Joaquim da Silva	M	2000-02-16	2019-05-10 10:06:37.903145-03	2019-05-10 10:06:37.90319-03	1
4	\N	Mulher 1	F	1972-06-07	2019-05-14 18:17:44.959788-03	2019-05-14 18:17:44.959819-03	4
5	\N	Homem 1	M	1947-12-09	2019-05-14 18:18:22.747763-03	2019-05-14 18:18:22.747793-03	4
6	\N	Homem 2	M	1965-08-06	2019-05-14 18:18:52.607705-03	2019-05-14 18:18:52.607754-03	4
7	\N	Mulher jovem 1	F	2003-01-12	2019-05-14 18:19:46.369511-03	2019-05-14 18:19:46.369543-03	4
8	\N	Mulher jovem 2	F	2003-08-12	2019-05-14 18:20:15.10884-03	2019-05-14 18:20:15.108868-03	4
9	\N	Mulher jovem 3	F	2003-12-12	2019-05-14 18:20:45.463761-03	2019-05-14 18:20:45.463786-03	4
10	\N	Criança 1	M	2019-10-04	2019-05-14 18:29:34.014838-03	2019-05-14 18:29:34.014881-03	4
11	\N	Criança 2	F	2019-04-03	2019-05-14 18:29:49.05234-03	2019-05-14 18:29:49.052364-03	4
12	\N	Outra 1	F	2018-05-06	2019-05-14 18:32:35.016165-03	2019-05-14 18:32:35.016192-03	4
13	\N	Home jovem	M	2001-11-02	2019-05-14 18:41:19.064701-03	2019-05-14 18:41:19.064733-03	4
14	\N	Homem jovem 2	M	2019-11-05	2019-05-14 18:41:40.392361-03	2019-05-14 18:41:40.392389-03	4
\.


--
-- Data for Name: agroecossistema_terra; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.agroecossistema_terra (id, deleted, poligono, area_territorio, tipo, criacao, ultima_alteracao, ciclo_anual_referencia_id, observacoes) FROM stdin;
1	\N	\N	2.0000	P	2019-05-10 06:50:30.292107-03	2019-05-10 06:50:30.292146-03	1	testes
2	\N	\N	34.0000	T	2019-05-10 07:00:33.76432-03	2019-05-10 07:00:33.764346-03	1	É uma terra muito massa
3	\N	\N	42.3500	P	2019-05-14 18:42:53.490878-03	2019-05-14 18:42:53.490904-03	16	sendo 21,78 ha (lote onde moram) + 20,57 (metade de 41,14 ha adquirido em parceria com o irmão do Rondom)
4	\N	\N	1.2400	T	2019-05-14 18:42:53.519051-03	2019-05-14 18:42:53.519089-03	16	
\.


--
-- Data for Name: anexos_anexo; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.anexos_anexo (id, nome, descricao, mimetype, ultima_alteracao, arquivo, tipo_id, criacao, deleted) FROM stdin;
2	enem01.pdf	pois é.	application/pdf	2019-04-08 15:17:34.628434-03	anexos/2019/04/08/enem01_fvI5ASA.pdf	\N	2019-04-08 15:17:34.628377-03	\N
3	Nomeação.pdf	Esta é uma nomeação.	application/pdf	2019-04-08 15:32:18.106617-03	anexos/2019/04/08/Nomeação.pdf	\N	2019-04-08 15:32:18.10658-03	\N
4	enem01.pdf	Este é um anexo.	application/pdf	2019-04-11 13:31:35.249152-03	anexos/2019/04/11/enem01.pdf	\N	2019-04-11 13:31:35.249116-03	\N
5	Captura de tela_2019-05-03_10-22-43.png	teste	image/png	2019-05-10 10:40:27.674251-03	anexos/2019/05/10/Captura_de_tela_2019-05-03_10-22-43.png	\N	2019-05-10 10:40:27.674203-03	\N
\.


--
-- Data for Name: anexos_anexorecipiente; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.anexos_anexorecipiente (id, recipiente_object_id, anexo_id, recipiente_content_type_id, criacao, ultima_alteracao, deleted) FROM stdin;
2	1	2	24	2019-04-08 15:17:34.677581-03	2019-04-08 15:17:34.677608-03	\N
4	1	3	24	2019-04-08 15:32:18.116866-03	2019-04-08 15:32:18.116894-03	\N
5	2	4	29	2019-04-11 13:31:35.27299-03	2019-04-11 13:31:35.273028-03	\N
6	1	4	24	2019-04-11 13:31:35.284155-03	2019-04-11 13:31:35.284187-03	\N
1	1	2	29	2019-04-08 15:17:34.630749-03	2019-04-08 15:17:34.630795-03	2019-04-17 16:37:32.412118-03
3	1	3	29	2019-04-08 15:32:18.108228-03	2019-04-08 15:32:18.108252-03	2019-04-17 16:37:32.412118-03
7	1	5	40	2019-05-10 10:40:27.687579-03	2019-05-10 10:40:27.687623-03	\N
\.


--
-- Data for Name: anexos_tipoanexo; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.anexos_tipoanexo (id, nome, mae_id, deleted) FROM stdin;
\.


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.auth_group (id, name) FROM stdin;
1	usuarios
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
1	1	161
2	1	165
3	1	169
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can view log entry	1	view_logentry
5	Can add permission	2	add_permission
6	Can change permission	2	change_permission
7	Can delete permission	2	delete_permission
8	Can view permission	2	view_permission
9	Can add group	3	add_group
10	Can change group	3	change_group
11	Can delete group	3	delete_group
12	Can view group	3	view_group
13	Can add content type	4	add_contenttype
14	Can change content type	4	change_contenttype
15	Can delete content type	4	delete_contenttype
16	Can view content type	4	view_contenttype
17	Can add session	5	add_session
18	Can change session	5	change_session
19	Can delete session	5	delete_session
20	Can view session	5	view_session
21	Can add site	6	add_site
22	Can change site	6	change_site
23	Can delete site	6	delete_site
24	Can view site	6	view_site
25	Can add email address	7	add_emailaddress
26	Can change email address	7	change_emailaddress
27	Can delete email address	7	delete_emailaddress
28	Can view email address	7	view_emailaddress
29	Can add email confirmation	8	add_emailconfirmation
30	Can change email confirmation	8	change_emailconfirmation
31	Can delete email confirmation	8	delete_emailconfirmation
32	Can view email confirmation	8	view_emailconfirmation
33	Can add social account	9	add_socialaccount
34	Can change social account	9	change_socialaccount
35	Can delete social account	9	delete_socialaccount
36	Can view social account	9	view_socialaccount
37	Can add social application	10	add_socialapp
38	Can change social application	10	change_socialapp
39	Can delete social application	10	delete_socialapp
40	Can view social application	10	view_socialapp
41	Can add social application token	11	add_socialtoken
42	Can change social application token	11	change_socialtoken
43	Can delete social application token	11	delete_socialtoken
44	Can view social application token	11	view_socialtoken
45	Can add Site Tree	12	add_tree
46	Can change Site Tree	12	change_tree
47	Can delete Site Tree	12	delete_tree
48	Can view Site Tree	12	view_tree
49	Can add Site Tree Item	13	add_treeitem
50	Can change Site Tree Item	13	change_treeitem
51	Can delete Site Tree Item	13	delete_treeitem
52	Can view Site Tree Item	13	view_treeitem
53	Can add diocese	14	add_diocese
54	Can change diocese	14	change_diocese
55	Can delete diocese	14	delete_diocese
56	Can view diocese	14	view_diocese
57	Can add municipio	15	add_municipio
58	Can change municipio	15	change_municipio
59	Can delete municipio	15	delete_municipio
60	Can view municipio	15	view_municipio
61	Can add regiao	16	add_regiao
62	Can change regiao	16	change_regiao
63	Can delete regiao	16	delete_regiao
64	Can view regiao	16	view_regiao
65	Can add regiao imediata	17	add_regiaoimediata
66	Can change regiao imediata	17	change_regiaoimediata
67	Can delete regiao imediata	17	delete_regiaoimediata
68	Can view regiao imediata	17	view_regiaoimediata
69	Can add regiao intermediaria	18	add_regiaointermediaria
70	Can change regiao intermediaria	18	change_regiaointermediaria
71	Can delete regiao intermediaria	18	delete_regiaointermediaria
72	Can view regiao intermediaria	18	view_regiaointermediaria
73	Can add uf	19	add_uf
74	Can change uf	19	change_uf
75	Can delete uf	19	delete_uf
76	Can view uf	19	view_uf
77	Can add anexo	20	add_anexo
78	Can change anexo	20	change_anexo
79	Can delete anexo	20	delete_anexo
80	Can view anexo	20	view_anexo
81	Can add anexo recipiente	21	add_anexorecipiente
82	Can change anexo recipiente	21	change_anexorecipiente
83	Can delete anexo recipiente	21	delete_anexorecipiente
84	Can view anexo recipiente	21	view_anexorecipiente
85	Can add tipo anexo	22	add_tipoanexo
86	Can change tipo anexo	22	change_tipoanexo
87	Can delete tipo anexo	22	delete_tipoanexo
88	Can view tipo anexo	22	view_tipoanexo
89	Can add usuaria	23	add_usuaria
90	Can change usuaria	23	change_usuaria
91	Can delete usuaria	23	delete_usuaria
92	Can view usuaria	23	view_usuaria
93	Can add agroecossistema	24	add_agroecossistema
94	Can change agroecossistema	24	change_agroecossistema
95	Can delete agroecossistema	24	delete_agroecossistema
96	Can view agroecossistema	24	view_agroecossistema
97	Can add comunidade	25	add_comunidade
98	Can change comunidade	25	change_comunidade
99	Can delete comunidade	25	delete_comunidade
100	Can view comunidade	25	view_comunidade
101	Can add organizacao	26	add_organizacao
102	Can change organizacao	26	change_organizacao
103	Can delete organizacao	26	delete_organizacao
104	Can view organizacao	26	view_organizacao
105	Can add organizacao usuaria	27	add_organizacaousuaria
106	Can change organizacao usuaria	27	change_organizacaousuaria
107	Can delete organizacao usuaria	27	delete_organizacaousuaria
108	Can view organizacao usuaria	27	view_organizacaousuaria
109	Can add dimensao	28	add_dimensao
110	Can change dimensao	28	change_dimensao
111	Can delete dimensao	28	delete_dimensao
112	Can view dimensao	28	view_dimensao
113	Can add evento	29	add_evento
114	Can change evento	29	change_evento
115	Can delete evento	29	delete_evento
116	Can view evento	29	view_evento
117	Can add tipo dimensao	30	add_tipodimensao
118	Can change tipo dimensao	30	change_tipodimensao
119	Can delete tipo dimensao	30	delete_tipodimensao
120	Can view tipo dimensao	30	view_tipodimensao
121	Can add analise	31	add_analise
122	Can change analise	31	change_analise
123	Can delete analise	31	delete_analise
124	Can view analise	31	view_analise
125	Can add atributo	32	add_atributo
126	Can change atributo	32	change_atributo
127	Can delete atributo	32	delete_atributo
128	Can view atributo	32	view_atributo
129	Can add avaliacao criterio	33	add_avaliacaocriterio
130	Can change avaliacao criterio	33	change_avaliacaocriterio
131	Can delete avaliacao criterio	33	delete_avaliacaocriterio
132	Can view avaliacao criterio	33	view_avaliacaocriterio
133	Can add criterio	34	add_criterio
134	Can change criterio	34	change_criterio
135	Can delete criterio	34	delete_criterio
136	Can view criterio	34	view_criterio
137	Can add qualificacao	35	add_qualificacao
138	Can change qualificacao	35	change_qualificacao
139	Can delete qualificacao	35	delete_qualificacao
140	Can view qualificacao	35	view_qualificacao
141	Can add territorio	36	add_territorio
142	Can change territorio	36	change_territorio
143	Can delete territorio	36	delete_territorio
144	Can view territorio	36	view_territorio
145	Can add evento grupo	37	add_eventogrupo
146	Can change evento grupo	37	change_eventogrupo
147	Can delete evento grupo	37	delete_eventogrupo
148	Can view evento grupo	37	view_eventogrupo
149	Can add anotacao	38	add_anotacao
150	Can change anotacao	38	change_anotacao
151	Can delete anotacao	38	delete_anotacao
152	Can view anotacao	38	view_anotacao
153	Can add attachment	39	add_attachment
154	Can change attachment	39	change_attachment
155	Can delete attachment	39	delete_attachment
156	Can view attachment	39	view_attachment
157	Can add agroecossistema	40	add_agroecossistema
158	Can change agroecossistema	40	change_agroecossistema
159	Can delete agroecossistema	40	delete_agroecossistema
160	Can view agroecossistema	40	view_agroecossistema
161	Can add produto	41	add_produto
162	Can change produto	41	change_produto
163	Can delete produto	41	delete_produto
164	Can view produto	41	view_produto
165	Can add servico	42	add_servico
166	Can change servico	42	change_servico
167	Can delete servico	42	delete_servico
168	Can view servico	42	view_servico
169	Can add unidade	43	add_unidade
170	Can change unidade	43	change_unidade
171	Can delete unidade	43	delete_unidade
172	Can view unidade	43	view_unidade
173	Can add ciclo anual referencia	44	add_cicloanualreferencia
174	Can change ciclo anual referencia	44	change_cicloanualreferencia
175	Can delete ciclo anual referencia	44	delete_cicloanualreferencia
176	Can view ciclo anual referencia	44	view_cicloanualreferencia
177	Can add composicao nsga	45	add_composicaonsga
178	Can change composicao nsga	45	change_composicaonsga
179	Can delete composicao nsga	45	delete_composicaonsga
180	Can view composicao nsga	45	view_composicaonsga
181	Can add pessoa	46	add_pessoa
182	Can change pessoa	46	change_pessoa
183	Can delete pessoa	46	delete_pessoa
184	Can view pessoa	46	view_pessoa
185	Can add terra	47	add_terra
186	Can change terra	47	change_terra
187	Can delete terra	47	delete_terra
188	Can view terra	47	view_terra
189	Can add avaliacao parametro	33	add_avaliacaoparametro
190	Can change avaliacao parametro	33	change_avaliacaoparametro
191	Can delete avaliacao parametro	33	delete_avaliacaoparametro
192	Can view avaliacao parametro	33	view_avaliacaoparametro
193	Can add parametro	34	add_parametro
194	Can change parametro	34	change_parametro
195	Can delete parametro	34	delete_parametro
196	Can view parametro	34	view_parametro
197	Can add analise	48	add_analise
198	Can change analise	48	change_analise
199	Can delete analise	48	delete_analise
200	Can view analise	48	view_analise
201	Can add estoque insumos	49	add_estoqueinsumos
202	Can change estoque insumos	49	change_estoqueinsumos
203	Can delete estoque insumos	49	delete_estoqueinsumos
204	Can view estoque insumos	49	view_estoqueinsumos
205	Can add pagamento terceiros	50	add_pagamentoterceiros
206	Can change pagamento terceiros	50	change_pagamentoterceiros
207	Can delete pagamento terceiros	50	delete_pagamentoterceiros
208	Can view pagamento terceiros	50	view_pagamentoterceiros
209	Can add patrimonio	51	add_patrimonio
210	Can change patrimonio	51	change_patrimonio
211	Can delete patrimonio	51	delete_patrimonio
212	Can view patrimonio	51	view_patrimonio
213	Can add renda nao agricola	52	add_rendanaoagricola
214	Can change renda nao agricola	52	change_rendanaoagricola
215	Can delete renda nao agricola	52	delete_rendanaoagricola
216	Can view renda nao agricola	52	view_rendanaoagricola
217	Can add subsistema	53	add_subsistema
218	Can change subsistema	53	change_subsistema
219	Can delete subsistema	53	delete_subsistema
220	Can view subsistema	53	view_subsistema
221	Can add subsistema aquisicao	54	add_subsistemaaquisicao
222	Can change subsistema aquisicao	54	change_subsistemaaquisicao
223	Can delete subsistema aquisicao	54	delete_subsistemaaquisicao
224	Can view subsistema aquisicao	54	view_subsistemaaquisicao
225	Can add subsistema producao	55	add_subsistemaproducao
226	Can change subsistema producao	55	change_subsistemaproducao
227	Can delete subsistema producao	55	delete_subsistemaproducao
228	Can view subsistema producao	55	view_subsistemaproducao
229	Can add subsistema tempo trabalho	56	add_subsistematempotrabalho
230	Can change subsistema tempo trabalho	56	change_subsistematempotrabalho
231	Can delete subsistema tempo trabalho	56	delete_subsistematempotrabalho
232	Can view subsistema tempo trabalho	56	view_subsistematempotrabalho
233	Can add tempo trabalho	57	add_tempotrabalho
234	Can change tempo trabalho	57	change_tempotrabalho
235	Can delete tempo trabalho	57	delete_tempotrabalho
236	Can view tempo trabalho	57	view_tempotrabalho
237	Can add subsistema insumo	54	add_subsistemainsumo
238	Can change subsistema insumo	54	change_subsistemainsumo
239	Can delete subsistema insumo	54	delete_subsistemainsumo
240	Can view subsistema insumo	54	view_subsistemainsumo
241	Can add subsistema aquisicao reciprocidade	58	add_subsistemaaquisicaoreciprocidade
242	Can change subsistema aquisicao reciprocidade	58	change_subsistemaaquisicaoreciprocidade
243	Can delete subsistema aquisicao reciprocidade	58	delete_subsistemaaquisicaoreciprocidade
244	Can view subsistema aquisicao reciprocidade	58	view_subsistemaaquisicaoreciprocidade
245	Can add subsistema pagamento terceiros	59	add_subsistemapagamentoterceiros
246	Can change subsistema pagamento terceiros	59	change_subsistemapagamentoterceiros
247	Can delete subsistema pagamento terceiros	59	delete_subsistemapagamentoterceiros
248	Can view subsistema pagamento terceiros	59	view_subsistemapagamentoterceiros
249	Can add analise totais	60	add_analisetotais
250	Can change analise totais	60	change_analisetotais
251	Can delete analise totais	60	delete_analisetotais
252	Can view analise totais	60	view_analisetotais
253	Can add subsistema totais	61	add_subsistematotais
254	Can change subsistema totais	61	change_subsistematotais
255	Can delete subsistema totais	61	delete_subsistematotais
256	Can view subsistema totais	61	view_subsistematotais
257	Can add analise tempos trabalho	62	add_analisetempostrabalho
258	Can change analise tempos trabalho	62	change_analisetempostrabalho
259	Can delete analise tempos trabalho	62	delete_analisetempostrabalho
260	Can view analise tempos trabalho	62	view_analisetempostrabalho
\.


--
-- Data for Name: cadernodecampo_anotacao; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.cadernodecampo_anotacao (id, texto, criacao, ultima_alteracao, agroecossistema_id, autor_id, data_visita, deleted) FROM stdin;
1	<p>Teste de anotação.<br></p>	2019-04-08 14:55:05.126068-03	2019-04-08 14:55:05.126099-03	1	1	2019-04-09	\N
2	<h1>Lorem Ipsum</h1>\r\n<h4>"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."</h4>\r\n<h5>"There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain..."</h5>\r\n\r\n\r\n<hr>\r\n\r\n\r\n\r\n\r\n\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus blandit\r\n tristique mauris, at tempor tortor luctus vel. Etiam in nulla congue, \r\npretium quam vitae, ultricies lacus. Pellentesque felis est, rutrum ut \r\nlibero commodo, egestas fermentum tortor. Quisque rhoncus neque nec \r\nmetus dapibus, eget rhoncus odio semper. Aliquam iaculis nunc nec elit \r\ndignissim finibus vel quis metus. Integer dignissim malesuada nisi, sed \r\nconvallis sapien rhoncus eu. Vivamus odio nunc, ornare et finibus vitae,\r\n vehicula nec nibh.\r\n</p>\r\n<p>\r\nPhasellus rhoncus sed tortor vel lobortis. Aenean aliquam eros nibh, a \r\ncommodo diam suscipit sit amet. Vestibulum mollis, nisl vel tincidunt \r\nauctor, leo urna ultrices tellus, sit amet fringilla nisi mi sed enim. \r\nNam in magna in mauris hendrerit porttitor id et urna. Donec cursus \r\nscelerisque magna mollis hendrerit. Donec metus lacus, fringilla et nunc\r\n vitae, condimentum euismod quam. Aliquam rutrum purus nisl, ac \r\nelementum lorem condimentum eu. Suspendisse eleifend magna a ex \r\ntristique lobortis. Donec elementum fermentum magna, sit amet pharetra \r\nenim luctus nec. Fusce ligula magna, laoreet sed dapibus id, aliquam nec\r\n nibh. Nunc maximus erat dui, in faucibus lectus feugiat et. Donec eget \r\nbibendum turpis. Vivamus purus ex, condimentum ut euismod nec, rutrum in\r\n justo. Aliquam eu pellentesque eros. Aliquam erat volutpat. Aliquam \r\nfinibus lorem sed nibh vehicula, eu rutrum libero fermentum.\r\n</p>\r\n<p>\r\nVivamus scelerisque eros a est bibendum malesuada. Sed eu tortor quis \r\narcu faucibus sagittis. Quisque porttitor, elit quis scelerisque \r\nlaoreet, metus leo consequat metus, eu scelerisque neque purus at nulla.\r\n Nunc at risus ac mauris tempor vestibulum a a mauris. Integer iaculis \r\nlorem sed erat scelerisque accumsan. Donec cursus condimentum neque, in \r\nporta ipsum egestas ut. In hac habitasse platea dictumst. Duis eget \r\nplacerat tortor. Nulla eu malesuada lacus. Sed quis blandit turpis, a \r\nconsequat urna. Sed tristique turpis ac orci egestas, at laoreet est \r\ncondimentum. Nulla in lacus orci. In tortor dui, eleifend ac congue vel,\r\n aliquam nec eros.\r\n</p>	2019-04-09 15:08:57.205219-03	2019-04-09 15:08:57.205276-03	1	2	2019-04-09	2019-04-17 16:39:04.749533-03
\.


--
-- Data for Name: comunidade; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.comunidade (id, nome, geoponto, poligono, municipio_id, criacao, ultima_alteracao, deleted) FROM stdin;
1	Barreirinhos	\N	\N	3110301	2019-04-05 09:48:59.779056-03	2019-04-05 09:48:59.779086-03	\N
2	Minha comunidade	\N	\N	4200051	2019-05-10 09:16:09.797836-03	2019-05-10 09:16:09.797863-03	\N
3	P.A. Sete de Janeiro	\N	\N	1702208	2019-05-14 18:14:54.995714-03	2019-05-14 18:14:54.995754-03	\N
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2019-05-10 09:43:42.547119-03	2	Maria José	2	[{"changed": {"fields": ["nome"]}}]	46	1
2	2019-05-12 22:29:39.322377-03	1	Terra de Terceiros	2	[{"changed": {"fields": ["nome", "ordem"]}}]	34	1
3	2019-05-12 22:30:05.514942-03	2	Sementes, mudas, mat. propag., crias	2	[{"changed": {"fields": ["ordem"]}}]	34	1
4	2019-05-12 22:30:17.748642-03	3	Água	2	[{"changed": {"fields": ["ordem"]}}]	34	1
5	2019-05-12 22:30:30.188261-03	4	Fertilizantes	2	[{"changed": {"fields": ["ordem"]}}]	34	1
6	2019-05-12 22:30:50.198521-03	5	Forragem/ração	2	[{"changed": {"fields": ["ordem"]}}]	34	1
7	2019-05-12 22:31:21.207078-03	6	Trabalho de Terceiros	2	[{"changed": {"fields": ["ordem"]}}]	34	1
8	2019-05-12 22:31:47.826923-03	7	Autoabastecimento Alimentar (quantidade, diversidade e qualidade)	2	[{"changed": {"fields": ["ordem"]}}]	34	1
9	2019-05-12 22:32:19.955832-03	8	Equipamentos/Infraestrutura	2	[{"changed": {"fields": ["ordem"]}}]	34	1
10	2019-05-12 22:32:28.277128-03	9	Força de Trabalho	2	[{"changed": {"fields": ["ordem"]}}]	34	1
11	2019-05-12 22:32:41.736664-03	10	Disponibilidade de Forragem/Ração	2	[{"changed": {"fields": ["ordem"]}}]	34	1
12	2019-05-12 22:32:51.87343-03	11	Fertilidade do Solo	2	[{"changed": {"fields": ["ordem"]}}]	34	1
13	2019-05-12 22:33:07.266127-03	12	Disponibilidade de Água	2	[{"changed": {"fields": ["ordem"]}}]	34	1
14	2019-05-12 22:33:17.712538-03	13	Biodiversidade (inter e intraespecífica)	2	[{"changed": {"fields": ["ordem"]}}]	34	1
15	2019-05-12 22:33:25.428773-03	14	Disponibilidade de Terra	2	[{"changed": {"fields": ["ordem"]}}]	34	1
16	2019-05-12 22:33:46.189939-03	15	Biodiversidade (planejada ou associada)	2	[{"changed": {"fields": ["ordem"]}}]	34	1
17	2019-05-12 22:33:54.576338-03	16	Diversidade de Mercados Acessados	2	[{"changed": {"fields": ["ordem"]}}]	34	1
18	2019-05-12 22:34:05.244092-03	17	Diversidade de Rendas (agrícolas e não-agrícolas)	2	[{"changed": {"fields": ["ordem"]}}]	34	1
19	2019-05-12 22:34:17.142381-03	18	Estoques de Insumos	2	[{"changed": {"fields": ["ordem"]}}]	34	1
20	2019-05-12 22:34:37.076794-03	19	Estoques Vivos	2	[{"changed": {"fields": ["ordem"]}}]	34	1
21	2019-05-12 22:34:59.600177-03	33	Participação em espaços político-organizativos	2	[{"changed": {"fields": ["ordem"]}}]	34	1
22	2019-05-12 22:35:07.631423-03	34	Acesso a políticas públicas	2	[{"changed": {"fields": ["ordem"]}}]	34	1
23	2019-05-12 22:35:23.184724-03	22	Participação em redes sociotécnicas de aprendizagem	2	[{"changed": {"fields": ["ordem"]}}]	34	1
24	2019-05-12 22:35:30.594654-03	23	Apropriação da riqueza produzida no agroecossistema pelo NSGA	2	[{"changed": {"fields": ["ordem"]}}]	34	1
25	2019-05-12 22:35:39.308081-03	24	Participação em espaços de gestão de bens comuns	2	[{"changed": {"fields": ["ordem"]}}]	34	1
26	2019-05-12 22:36:01.97474-03	25	Divisão sexual do trabalho doméstico e de cuidados (adultos)	2	[{"changed": {"fields": ["ordem"]}}]	34	1
27	2019-05-12 22:36:10.261227-03	26	Divisão sexual do trabalho doméstico e de cuidados (jovens)	2	[{"changed": {"fields": ["ordem"]}}]	34	1
28	2019-05-12 22:36:22.183258-03	32	Participação nas decisões de gestão do agroecossistema	2	[{"changed": {"fields": ["ordem"]}}]	34	1
29	2019-05-12 22:37:01.010179-03	28	Participação em espaços sócio-organizativos	2	[{"changed": {"fields": ["ordem"]}}]	34	1
30	2019-05-12 22:37:14.426665-03	29	Apropriação da riqueza gerada no agroecossistema	2	[{"changed": {"fields": ["ordem"]}}]	34	1
31	2019-05-12 22:37:34.528564-03	30	Acesso a políticas públicas	2	[{"changed": {"fields": ["ordem"]}}]	34	1
32	2019-05-12 22:37:51.757747-03	31	Participação em espaços de aprendizagem	2	[{"changed": {"fields": ["ordem"]}}]	34	1
33	2019-05-12 22:38:16.606384-03	27	Participação nas decisões de gestão do agroecossistema	2	[{"changed": {"fields": ["ordem"]}}]	34	1
34	2019-05-12 22:38:39.000209-03	32	Participação nas decisões de gestão do agroecossistema	2	[{"changed": {"fields": ["ordem"]}}]	34	1
35	2019-05-12 22:39:06.197149-03	20	Participação em espaços político-organizativos	2	[{"changed": {"fields": ["ordem"]}}]	34	1
36	2019-05-12 22:39:26.392328-03	33	Participação em espaços político-organizativos	2	[{"changed": {"fields": ["ordem"]}}]	34	1
37	2019-05-12 22:39:41.99839-03	21	Acesso a políticas públicas	2	[{"changed": {"fields": ["ordem"]}}]	34	1
38	2019-05-12 22:39:58.84489-03	34	Acesso a políticas públicas	2	[{"changed": {"fields": ["ordem"]}}]	34	1
39	2019-05-12 22:40:15.554248-03	35	Autonomia econômica	2	[{"changed": {"fields": ["ordem"]}}]	34	1
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	contenttypes	contenttype
5	sessions	session
6	sites	site
7	account	emailaddress
8	account	emailconfirmation
9	socialaccount	socialaccount
10	socialaccount	socialapp
11	socialaccount	socialtoken
12	sitetree	tree
13	sitetree	treeitem
14	municipios	diocese
15	municipios	municipio
16	municipios	regiao
17	municipios	regiaoimediata
18	municipios	regiaointermediaria
19	municipios	uf
20	anexos	anexo
21	anexos	anexorecipiente
22	anexos	tipoanexo
23	main	usuaria
24	main	agroecossistema
25	main	comunidade
26	main	organizacao
27	main	organizacaousuaria
28	linhadotempo	dimensao
29	linhadotempo	evento
30	linhadotempo	tipodimensao
31	qualitativa	analise
32	qualitativa	atributo
35	qualitativa	qualificacao
36	main	territorio
37	linhadotempo	eventogrupo
38	cadernodecampo	anotacao
39	django_summernote	attachment
40	agroecossistema	agroecossistema
33	qualitativa	avaliacaoparametro
34	qualitativa	parametro
41	main	produto
42	main	servico
43	main	unidade
44	agroecossistema	cicloanualreferencia
45	agroecossistema	composicaonsga
46	agroecossistema	pessoa
47	agroecossistema	terra
48	economica	analise
49	economica	estoqueinsumos
50	economica	pagamentoterceiros
51	economica	patrimonio
52	economica	rendanaoagricola
53	economica	subsistema
55	economica	subsistemaproducao
56	economica	subsistematempotrabalho
57	economica	tempotrabalho
54	economica	subsistemainsumo
58	economica	subsistemaaquisicaoreciprocidade
59	economica	subsistemapagamentoterceiros
60	economica	analisetotais
61	economica	subsistematotais
62	economica	analisetempostrabalho
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	municipios	0001_initial	2019-04-04 12:14:07.186436-03
2	contenttypes	0001_initial	2019-04-04 12:14:07.341866-03
3	contenttypes	0002_remove_content_type_name	2019-04-04 12:14:07.385877-03
4	auth	0001_initial	2019-04-04 12:14:07.78614-03
5	auth	0002_alter_permission_name_max_length	2019-04-04 12:14:07.808555-03
6	auth	0003_alter_user_email_max_length	2019-04-04 12:14:07.828079-03
7	auth	0004_alter_user_username_opts	2019-04-04 12:14:07.848933-03
8	auth	0005_alter_user_last_login_null	2019-04-04 12:14:07.872113-03
9	auth	0006_require_contenttypes_0002	2019-04-04 12:14:07.88643-03
10	auth	0007_alter_validators_add_error_messages	2019-04-04 12:14:07.90486-03
11	auth	0008_alter_user_username_max_length	2019-04-04 12:14:07.926983-03
12	auth	0009_alter_user_last_name_max_length	2019-04-04 12:14:07.954299-03
13	main	0001_initial	2019-04-04 12:14:09.608528-03
14	account	0001_initial	2019-04-04 12:14:09.963646-03
15	account	0002_email_max_length	2019-04-04 12:14:09.99582-03
16	admin	0001_initial	2019-04-04 12:14:10.307037-03
17	admin	0002_logentry_remove_auto_add	2019-04-04 12:14:10.334808-03
18	admin	0003_logentry_add_action_flag_choices	2019-04-04 12:14:10.359548-03
19	anexos	0001_initial	2019-04-04 12:14:10.81833-03
20	linhadotempo	0001_initial	2019-04-04 12:14:11.062926-03
21	linhadotempo	0002_relate_agroecossistema_to_evento	2019-04-04 12:14:11.507525-03
22	qualitativa	0001_initial	2019-04-04 12:14:12.484851-03
23	sessions	0001_initial	2019-04-04 12:14:12.64004-03
24	sites	0001_initial	2019-04-04 12:14:12.739558-03
25	sites	0002_alter_domain_unique	2019-04-04 12:14:12.861513-03
26	sitetree	0001_initial	2019-04-04 12:14:14.427232-03
27	socialaccount	0001_initial	2019-04-04 12:14:15.411282-03
28	socialaccount	0002_token_max_lengths	2019-04-04 12:14:15.511591-03
29	socialaccount	0003_extra_data_default_dict	2019-04-04 12:14:15.542536-03
30	anexos	0002_auto_20190402_2347	2019-04-04 12:15:42.849862-03
31	linhadotempo	0003_auto_20190402_2347	2019-04-04 12:15:43.381346-03
32	main	0002_auto_20190402_2347	2019-04-04 12:15:44.353091-03
33	qualitativa	0002_auto_20190402_2347	2019-04-04 12:15:44.759096-03
34	main	0003_auto_20190404_1928	2019-04-05 09:35:12.605211-03
35	linhadotempo	0004_eventogrupo	2019-04-05 09:35:13.208002-03
36	linhadotempo	0005_eventogrupo_agroecossistema	2019-04-05 09:35:13.462207-03
37	main	0002_auto_20190402_1233	2019-04-08 09:58:42.048342-03
38	main	0003_auto_20190402_1237	2019-04-08 09:58:42.139091-03
39	main	0004_merge_20190408_0228	2019-04-08 09:58:42.146433-03
40	cadernodecampo	0001_initial	2019-04-08 09:58:42.604335-03
41	django_summernote	0001_initial	2019-04-08 09:58:42.669585-03
42	django_summernote	0002_update-help_text	2019-04-08 09:58:42.688713-03
43	main	0005_auto_20190408_1127	2019-04-08 09:58:42.734795-03
44	anexos	0003_auto_20190408_1345	2019-04-08 15:17:26.823216-03
45	anexos	0004_auto_20190409_1058	2019-04-09 07:59:02.996249-03
46	linhadotempo	0006_auto_20190409_1058	2019-04-09 07:59:03.216768-03
47	cadernodecampo	0002_anotacao_data_visita	2019-04-09 16:54:28.706052-03
48	qualitativa	0003_criterio_css_classes	2019-04-09 20:09:58.293355-03
49	qualitativa	0004_criterio_subsection	2019-04-09 20:09:58.404424-03
50	qualitativa	0005_remove_criterio_css_classes	2019-04-09 20:09:58.437353-03
51	qualitativa	0006_auto_20190410_1612	2019-04-10 13:13:00.631847-03
52	anexos	0005_change_ondelete	2019-04-11 13:18:55.163352-03
53	cadernodecampo	0003_change_ondelete	2019-04-11 13:18:55.364137-03
54	linhadotempo	0007_change_ondelete	2019-04-11 13:18:56.491546-03
55	main	0006_change_ondelete	2019-04-11 13:18:56.59469-03
56	qualitativa	0006_change_ondelete	2019-04-11 13:18:56.918193-03
57	qualitativa	0007_merge_20190410_1655	2019-04-11 13:18:56.929978-03
58	anexos	0006_auto_20190412_1316	2019-04-17 15:49:26.68966-03
59	cadernodecampo	0004_anotacao_deleted	2019-04-17 15:49:26.822157-03
60	linhadotempo	0008_auto_20190412_1316	2019-04-17 15:49:27.154931-03
61	main	0007_auto_20190412_1316	2019-04-17 15:49:27.287028-03
62	main	0008_auto_20190415_1556	2019-04-17 15:49:28.021001-03
63	qualitativa	0008_auto_20190412_1316	2019-04-17 15:49:28.142673-03
64	qualitativa	0009_auto_20190415_1556	2019-04-17 15:49:28.34371-03
65	anexos	0007_auto_20190418_1421	2019-04-18 11:21:34.958725-03
66	cadernodecampo	0005_auto_20190418_1421	2019-04-18 11:21:35.092957-03
67	linhadotempo	0009_auto_20190418_1421	2019-04-18 11:21:35.210759-03
68	main	0009_auto_20190418_1421	2019-04-18 11:21:35.308328-03
69	qualitativa	0010_auto_20190418_1421	2019-04-18 11:21:35.396738-03
70	main	0010_auto_20190424_1351	2019-04-24 11:23:23.344495-03
71	agroecossistema	0001_initial	2019-04-24 11:23:23.430732-03
72	anexos	0008_auto_20190424_1351	2019-04-24 11:23:23.478054-03
73	cadernodecampo	0006_auto_20190424_1351	2019-04-24 11:23:23.521578-03
74	cadernodecampo	0007_auto_20190424_1402	2019-04-24 11:23:23.825111-03
75	linhadotempo	0010_auto_20190424_1351	2019-04-24 11:23:23.914338-03
76	linhadotempo	0011_auto_20190424_1402	2019-04-24 11:23:24.235155-03
77	main	0011_move_agroecossistema2	2019-04-24 11:23:24.252523-03
78	qualitativa	0011_auto_20190424_1351	2019-04-24 11:23:24.314618-03
79	qualitativa	0012_auto_20190424_1402	2019-04-24 11:30:54.462109-03
80	main	0011_move_agroecossistema3	2019-04-24 11:30:54.479659-03
81	agroecossistema	0002_auto_20190424_1613	2019-05-07 12:23:50.614683-03
82	agroecossistema	0003_criterios_inativos_to_parametros	2019-05-07 12:23:50.72519-03
83	agroecossistema	0004_added_mes_inicio_e_fim_ciclo	2019-05-07 12:23:50.813994-03
84	agroecossistema	0005_auto_20190424_2015	2019-05-07 12:23:50.981509-03
85	main	0012_produto_servico_unidade	2019-05-07 12:23:51.181198-03
86	economica	0001_initial	2019-05-07 12:23:53.23793-03
87	economica	0002_auto_20190424_2015	2019-05-07 12:23:53.325512-03
88	qualitativa	0013_renamed_models	2019-05-07 12:23:53.969496-03
89	qualitativa	0014_renamed_fields	2019-05-07 12:23:54.136084-03
90	agroecossistema	0006_auto_20190503_2028	2019-05-07 20:03:39.638713-03
91	agroecossistema	0007_auto_20190506_2225	2019-05-07 20:03:39.816081-03
92	agroecossistema	0008_auto_20190508_0150	2019-05-08 07:57:15.851919-03
93	economica	0003_auto_20190507_2347	2019-05-08 07:57:15.884456-03
94	economica	0004_auto_20190508_0150	2019-05-08 07:57:16.477064-03
95	economica	0005_auto_20190508_0159	2019-05-08 07:57:16.730505-03
96	economica	0006_auto_20190508_0210	2019-05-08 07:57:17.342044-03
97	agroecossistema	0009_auto_20190508_1130	2019-05-08 08:31:04.359984-03
98	anexos	0009_auto_20190508_1130	2019-05-08 08:31:04.408604-03
99	cadernodecampo	0008_auto_20190508_1130	2019-05-08 08:31:04.442764-03
100	economica	0007_auto_20190508_1130	2019-05-08 08:31:04.649574-03
101	linhadotempo	0012_auto_20190508_1130	2019-05-08 08:31:04.744366-03
102	main	0013_auto_20190508_1130	2019-05-08 08:31:04.877265-03
103	qualitativa	0015_auto_20190508_1130	2019-05-08 08:31:04.966948-03
104	economica	0008_auto_20190509_0743	2019-05-09 06:08:23.10036-03
105	agroecossistema	0009_auto_20190509_1427	2019-05-09 16:43:43.322002-03
106	agroecossistema	0010_merge_20190509_1943	2019-05-09 16:43:43.348121-03
107	anexos	0009_auto_20190509_1427	2019-05-09 16:43:43.396179-03
108	anexos	0010_merge_20190509_1943	2019-05-09 16:43:43.402755-03
109	cadernodecampo	0008_auto_20190509_1427	2019-05-09 16:43:43.439012-03
110	cadernodecampo	0009_merge_20190509_1943	2019-05-09 16:43:43.447368-03
111	economica	0007_auto_20190509_1427	2019-05-09 16:43:43.825268-03
112	economica	0009_merge_20190509_1943	2019-05-09 16:43:43.985591-03
113	linhadotempo	0012_auto_20190509_1427	2019-05-09 16:43:44.077259-03
114	linhadotempo	0013_merge_20190509_1943	2019-05-09 16:43:44.090803-03
115	main	0013_auto_20190509_1427	2019-05-09 16:43:44.21424-03
116	main	0014_merge_20190509_1943	2019-05-09 16:43:44.236023-03
117	qualitativa	0015_auto_20190509_1427	2019-05-09 16:43:44.344669-03
118	qualitativa	0016_merge_20190509_1943	2019-05-09 16:43:44.380254-03
119	agroecossistema	0011_auto_20190510_0843	2019-05-10 05:43:36.663766-03
120	economica	0010_subsistematempotrabalho_composicao_nsga	2019-05-10 05:43:37.527237-03
121	agroecossistema	0012_auto_20190510_1011	2019-05-10 09:30:37.134321-03
122	agroecossistema	0013_auto_20190510_1021	2019-05-10 09:30:37.18976-03
123	anexos	0011_auto_20190510_1011	2019-05-10 09:30:37.235535-03
124	cadernodecampo	0010_auto_20190510_1011	2019-05-10 09:30:37.295008-03
125	economica	0010_auto_20190510_1011	2019-05-10 09:38:38.257314-03
126	economica	0011_merge_20190510_1230	2019-05-10 09:38:38.313872-03
127	linhadotempo	0014_auto_20190510_1011	2019-05-10 09:38:38.405021-03
128	main	0015_auto_20190510_1011	2019-05-10 09:38:38.552199-03
129	qualitativa	0017_auto_20190510_1011	2019-05-10 09:38:38.648202-03
130	agroecossistema	0014_auto_20190510_1222	2019-05-10 09:43:55.851611-03
131	economica	0011_auto_20190510_1222	2019-05-10 09:43:55.962385-03
132	economica	0012_merge_20190510_1240	2019-05-10 09:43:55.97387-03
133	economica	0012_merge_20190510_1637	2019-05-10 13:38:01.609157-03
134	agroecossistema	0015_auto_20190510_2012	2019-05-11 09:40:20.231934-03
135	economica	0012_merge_20190511_1240	2019-05-11 09:40:20.354325-03
136	main	0016_add_group_usuarios	2019-05-11 09:40:21.125551-03
137	agroecossistema	0016_auto_20190511_1712	2019-05-11 14:12:16.133067-03
138	anexos	0012_auto_20190511_1712	2019-05-11 14:12:16.212882-03
139	cadernodecampo	0011_auto_20190511_1712	2019-05-11 14:12:16.251828-03
140	economica	0013_auto_20190511_1712	2019-05-11 14:12:16.468299-03
141	linhadotempo	0015_auto_20190511_1712	2019-05-11 14:12:16.551341-03
142	main	0017_auto_20190511_1712	2019-05-11 14:12:16.668585-03
143	qualitativa	0018_auto_20190511_1712	2019-05-11 14:12:16.759167-03
144	agroecossistema	0017_auto_20190512_1235	2019-05-12 18:01:48.527308-03
145	anexos	0013_auto_20190512_1235	2019-05-12 18:01:48.718477-03
146	cadernodecampo	0012_auto_20190512_1235	2019-05-12 18:01:48.752249-03
147	economica	0014_auto_20190512_1235	2019-05-12 18:01:48.963475-03
148	linhadotempo	0016_auto_20190512_1235	2019-05-12 18:01:49.043069-03
149	main	0018_auto_20190512_1235	2019-05-12 18:01:49.149139-03
150	qualitativa	0019_auto_20190512_1235	2019-05-12 18:01:49.240745-03
151	qualitativa	0020_auto_20190512_1712	2019-05-12 18:01:49.838914-03
152	economica	0015_auto_20190512_1954	2019-05-12 19:54:50.977067-03
153	agroecossistema	0018_auto_20190512_2231	2019-05-12 23:22:26.492079-03
154	economica	0016_auto_20190512_2203	2019-05-12 23:22:26.625544-03
155	economica	0017_auto_20190512_2211	2019-05-12 23:22:27.060726-03
156	economica	0018_auto_20190512_2231	2019-05-12 23:22:28.684242-03
157	economica	0019_subsistemaaquisicaoreciprocidade_servico	2019-05-12 23:22:29.250801-03
158	economica	0020_remove_subsistemaaquisicaoreciprocidade_origem	2019-05-12 23:22:29.372249-03
159	economica	0021_auto_20190512_2258	2019-05-12 23:22:29.671939-03
160	economica	0022_auto_20190513_0035	2019-05-13 00:35:20.476625-03
161	economica	0023_auto_20190513_0745	2019-05-13 11:08:05.115153-03
162	economica	0024_auto_20190513_0819	2019-05-13 11:08:05.224876-03
163	agroecossistema	0019_auto_20190513_1229	2019-05-13 12:29:46.99357-03
164	economica	0025_auto_20190513_1229	2019-05-13 12:29:47.230032-03
165	economica	0026_create_view_totais_subsistema	2019-05-14 17:22:18.781264-03
166	economica	0027_create_view_totais_analise	2019-05-14 18:02:07.653271-03
167	economica	0028_analisetotais_subsistematotais	2019-05-14 20:28:10.698317-03
168	agroecossistema	0020_create_view_composicaonsga_categoria	2019-05-15 11:26:13.71237-03
169	economica	0028_auto_20190515_0115	2019-05-15 11:26:13.855193-03
170	economica	0029_merge_20190515_1125	2019-05-15 11:26:13.866799-03
171	economica	0030_auto_20190515_1126	2019-05-15 11:26:14.149718-03
172	economica	0031_auto_20190515_1615	2019-05-15 16:15:28.387254-03
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
k82b7p5dymb9a3r5yrmg5u49w6dlt079	NzZkODViNzQwMTVjNWQzMzk4MmNkMWFjNGUxNTY3MTUyNGMyNjM4Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI0NjVkMjM2YTE5OTU4YmQ0ODVlZmU4ZTVlOTBjODViNDRhMjcxMTVhIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==	2019-04-24 10:45:26.562452-03
mfave3zgayvowm2bptt2vypiu4dnqi5c	NWNiOTE1ODgzNDkzNjFmMDBjNjI2MjhjNjQyMzJiYmI4NzFmMjZjYzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5YjJlYjE2ZmRhY2Y4ZTBmMzE3ZWUzODcyMzhlMjFjYzQ5Zjc3N2UzIiwiX3Nlc3Npb25fZXhwaXJ5IjoxMjA5NjAwfQ==	2019-04-25 13:30:59.440071-03
f6dyysa03hldm2jdcw6bl7na0244ivf4	NzZkODViNzQwMTVjNWQzMzk4MmNkMWFjNGUxNTY3MTUyNGMyNjM4Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI0NjVkMjM2YTE5OTU4YmQ0ODVlZmU4ZTVlOTBjODViNDRhMjcxMTVhIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==	2019-05-21 12:24:23.330667-03
ogj2h9ly2076b3tt627w28xomex3zj1u	NzZkODViNzQwMTVjNWQzMzk4MmNkMWFjNGUxNTY3MTUyNGMyNjM4Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI0NjVkMjM2YTE5OTU4YmQ0ODVlZmU4ZTVlOTBjODViNDRhMjcxMTVhIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==	2019-05-22 07:01:04.08043-03
o56d8rgqoe780gdilyc29q66xmt8umxx	ODYyMDQ4MWQyZjk5MTZhYzM2ZThjZDU5NjU0MGQ5ZDYwMzM1YWYwYzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5YjJlYjE2ZmRhY2Y4ZTBmMzE3ZWUzODcyMzhlMjFjYzQ5Zjc3N2UzIiwiYW5vX2NhciI6MjAxNywibm9tZV9jYXIiOiJKYW4yMDE3IC0gRGVjMjAxNyJ9	2019-05-29 14:45:49.361064-03
brf8juk10m4ebduwwim38ywlwljfm4br	MDI5MGMxYWYwODUwODU0ODY2ZGNkNzdlNzJmN2M2NmFmYTU4ZGQ0NDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5YjJlYjE2ZmRhY2Y4ZTBmMzE3ZWUzODcyMzhlMjFjYzQ5Zjc3N2UzIiwiYW5vX2NhciI6MjAxOSwibm9tZV9jYXIiOiJKdWwyMDE5IC0gSnVuMjAyMCJ9	2019-05-25 19:41:53.175655-03
\.


--
-- Data for Name: django_site; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.django_site (id, domain, name) FROM stdin;
1	example.com	example.com
\.


--
-- Data for Name: django_summernote_attachment; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.django_summernote_attachment (id, name, file, uploaded) FROM stdin;
1	WhatsApp Image 2018-09-28 at 10.11.01.jpeg	django-summernote/2019-04-08/f233a132-45ef-4af2-b350-4d9247de195a.jpeg	2019-04-08 15:10:56.723645-03
\.


--
-- Data for Name: economica_analise; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.economica_analise (id, deleted, data_coleta, criacao, ultima_alteracao, ciclo_anual_referencia_id) FROM stdin;
1	\N	\N	2019-05-09 21:29:16.0001-03	2019-05-14 10:23:25.148683-03	1
2	\N	\N	2019-05-10 09:47:02.784632-03	2019-05-14 13:17:02.804402-03	15
3	\N	2017-04-11 00:00:00-03	2019-05-14 18:43:01.2321-03	2019-05-15 14:50:17.174496-03	16
\.


--
-- Data for Name: economica_estoqueinsumos; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.economica_estoqueinsumos (id, deleted, valor_unitario_inicio, quantidade_inicio, valor_unitario_final, quantidade_final, criacao, ultima_alteracao, analise_id, produto_id, unidade_id, observacoes) FROM stdin;
1	2019-05-15 10:07:52.306702-03	2.22	1.00	2.22	1.00	2019-05-15 10:07:34.241643-03	2019-05-15 10:07:34.241671-03	3	19	16	\N
\.


--
-- Data for Name: economica_pagamentoterceiros; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.economica_pagamentoterceiros (id, deleted, valor_unitario, quantidade, criacao, ultima_alteracao, analise_id, servico_id, unidade_id, observacoes) FROM stdin;
1	\N	55.00	12.00	2019-05-14 19:57:09.81135-03	2019-05-14 19:57:09.811378-03	3	3	10	\N
2	\N	120.00	1.00	2019-05-14 19:57:09.830716-03	2019-05-14 19:57:09.830782-03	3	4	11	\N
\.


--
-- Data for Name: economica_patrimonio; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.economica_patrimonio (id, deleted, item, valor_unitario_inicio, quantidade_inicio, valor_unitario_final, tipo, observacoes, criacao, ultima_alteracao, analise_id, unidade_id, quantidade_final) FROM stdin;
19	\N	Carroçinha (uso coletivo)	383.00	1.00	333.00	E	Equipamento utilizado por mais cinco famílias em regime de rodízio	2019-05-14 19:50:19.503508-03	2019-05-14 19:50:30.254073-03	3	5	1.00
18	\N	Roçadeira (uso coletivo)	417.00	1.00	333.00	E	Equipamento utilizado por mais cinco famílias em regime de rodízio	2019-05-14 19:50:19.502259-03	2019-05-14 19:50:30.255272-03	3	5	1.00
17	\N	tratorito (uso coletivo)	417.00	1.00	417.00	E	Equipamento utilizado por mais cinco famílias em regime de rodízio e adquirido com recursos do PPP-Ecos	2019-05-14 19:50:19.501155-03	2019-05-14 19:50:30.256476-03	3	5	1.00
16	\N	Kit cerca elétrica	300.00	1.00	300.00	E		2019-05-14 19:50:19.499612-03	2019-05-14 19:50:30.257733-03	3	5	1.00
15	\N	Forrageira	450.00	1.00	450.00	E		2019-05-14 19:50:19.497944-03	2019-05-14 19:50:30.259363-03	3	5	1.00
14	\N	motocicletas	2750.00	2.00	2750.00	E		2019-05-14 19:50:19.494726-03	2019-05-14 19:50:30.261697-03	3	5	2.00
2	\N	Lote em parceria com irmão do Rondom	3646.00	20.57	3646.00	F		2019-05-14 19:01:27.453244-03	2019-05-15 14:50:17.150737-03	3	6	20.57
1	\N	Lote onde reside o NSGA	6887.00	21.78	6887.00	F		2019-05-14 19:01:27.395043-03	2019-05-15 14:50:17.152796-03	3	6	21.78
13	\N	Árvores	50.00	1300.00	50.00	F		2019-05-14 19:01:27.469293-03	2019-05-15 14:50:17.135928-03	3	5	1300.00
12	\N	Cercas	8.97	4400.00	8.97	F		2019-05-14 19:01:27.467888-03	2019-05-15 14:50:17.137406-03	3	7	4400.00
11	\N	Estacas extraídas do lote.	15.00	300.00	15.00	F		2019-05-14 19:01:27.466339-03	2019-05-15 14:50:17.13868-03	3	5	300.00
10	\N	Barracão dos bezerros	500.00	1.00	500.00	F		2019-05-14 19:01:27.464632-03	2019-05-15 14:50:17.139935-03	3	5	1.00
9	\N	Pocilga	500.00	1.00	500.00	F		2019-05-14 19:01:27.462279-03	2019-05-15 14:50:17.141177-03	3	5	1.00
8	\N	Barracão do babaçu	500.00	1.00	500.00	F		2019-05-14 19:01:27.460948-03	2019-05-15 14:50:17.142609-03	3	5	1.00
7	\N	Açude	3300.00	2.00	3300.00	F		2019-05-14 19:01:27.459613-03	2019-05-15 14:50:17.143991-03	3	5	2.00
6	\N	Poço	1300.00	1.00	1300.00	F		2019-05-14 19:01:27.458377-03	2019-05-15 14:50:17.14529-03	3	5	1.00
5	\N	Cisterna/calçadão	14000.00	1.00	14000.00	F		2019-05-14 19:01:27.457044-03	2019-05-15 14:50:17.146548-03	3	5	1.00
4	\N	Curral	2000.00	1.00	2000.00	F		2019-05-14 19:01:27.455843-03	2019-05-15 14:50:17.148143-03	3	5	1.00
3	\N	Residência	12000.00	1.00	12000.00	F		2019-05-14 19:01:27.454615-03	2019-05-15 14:50:17.149462-03	3	5	1.00
21	\N	Triturador	1792.00	1.00	1792.00	E	Equipamento utilizado por mais cinco famílias em regime de mutirão	2019-05-14 19:50:19.506583-03	2019-05-14 19:50:30.25139-03	3	5	1.00
20	\N	Equipamentos de apicultura	2230.00	1.00	2230.00	E	Adquirido com recursos do Ecoforte	2019-05-14 19:50:19.505046-03	2019-05-14 19:50:30.252766-03	3	5	1.00
30	\N	Abelha	115.00	1.00	115.00	V		2019-05-14 19:55:38.417639-03	2019-05-14 19:55:44.610306-03	3	9	1.00
29	\N	Galinhas	25.00	34.00	25.00	V		2019-05-14 19:55:38.416641-03	2019-05-14 19:55:44.611665-03	3	8	20.00
28	\N	Porcos	100.00	24.00	100.00	V		2019-05-14 19:55:38.415369-03	2019-05-14 19:55:44.612886-03	3	8	13.00
27	\N	Boi	2500.00	1.50	2500.00	V		2019-05-14 19:55:38.414073-03	2019-05-14 19:55:44.614639-03	3	8	1.50
26	\N	Bezerros	450.00	39.00	450.00	V		2019-05-14 19:55:38.413063-03	2019-05-14 19:55:44.61594-03	3	8	24.00
25	\N	Novilha	1500.00	7.00	1500.00	V		2019-05-14 19:55:38.412208-03	2019-05-14 19:55:44.61717-03	3	8	7.00
24	\N	Vacas	1320.00	26.00	2500.00	V		2019-05-14 19:55:38.411326-03	2019-05-14 19:55:44.618344-03	3	8	21.00
23	\N	Cavalo	900.00	3.00	900.00	V		2019-05-14 19:55:38.410438-03	2019-05-14 19:55:44.619548-03	3	8	3.00
22	\N	jumento	75.00	2.00	75.00	V		2019-05-14 19:55:38.40911-03	2019-05-14 19:55:44.620748-03	3	8	2.00
\.


--
-- Data for Name: economica_rendanaoagricola; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.economica_rendanaoagricola (id, deleted, tipo, item, valor_unitario, quantidade, observacoes, criacao, ultima_alteracao, analise_id, unidade_id) FROM stdin;
1	\N	P	teste	30.00	1.00		2019-05-14 13:17:02.751563-03	2019-05-14 13:17:02.751587-03	2	1
2	\N	P	teste2	70.00	1.00		2019-05-14 13:17:02.772574-03	2019-05-14 13:17:02.772612-03	2	2
4	\N	P	Revendedora de cosméticos	75.00	12.00	Informações sobre a retirada média da Agricultura/proprietária do lote	2019-05-14 18:47:42.027924-03	2019-05-14 18:47:42.027958-03	3	4
5	\N	P	Faxinas para vizinhos	50.00	1.00	Agricultura/proprietária do lote	2019-05-14 18:47:42.029782-03	2019-05-14 18:47:42.029825-03	3	3
6	\N	T	Bolsa família + Bolsa jovem	540.00	1.00	Benefício para a família e em função de alguns filhos na escola	2019-05-14 18:48:20.356077-03	2019-05-14 18:48:20.356115-03	3	5
7	2019-05-15 14:48:51.617243-03	P	teste	0.21	1.00		2019-05-15 10:24:23.88568-03	2019-05-15 10:24:23.885719-03	3	3
3	\N	P	Prestação de serviços em outros lotes (roçadas, capinas, etc	50.00	10.00	Jovem homem	2019-05-14 18:47:42.026163-03	2019-05-15 14:49:15.714989-03	3	3
\.


--
-- Data for Name: economica_subsistema; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.economica_subsistema (id, deleted, area, nome, observacoes, criacao, ultima_alteracao, analise_id, extrativismo) FROM stdin;
1	\N	\N	Viveiro de galinhas	\N	2019-05-10 16:55:49.064846-03	2019-05-10 16:55:49.064876-03	1	f
3	\N	\N	Viveiro de galinhas	\N	2019-05-11 19:20:37.012799-03	2019-05-11 19:20:37.012855-03	2	f
11	\N	1.80	Babaçu	O babaçual está distribuído nas pastagens e na área de APP do lote. Para cálculo da área foi conisderda a área de 15 m² utilizada por cada pé de babaçu  multiplicado pelo número aproximado de 1.200 pés existentes no lote	2019-05-15 15:59:20.361036-03	2019-05-15 16:07:27.389261-03	3	f
2	\N	\N	galinhas2	\N	2019-05-10 18:11:19.948619-03	2019-05-14 11:08:12.092671-03	1	f
5	\N	32.38	Pastagem/gado	19,36 ha no lote analisado + 13,02 ha (metade de 26,05 área do outro lote em parceria com irmão). Total: 32,38 ha	2019-05-14 20:12:45.829999-03	2019-05-15 15:07:27.420744-03	3	f
12	\N	\N	Peixes e Estacas	Os peixes são pescados nas lagoas próximas ao assentamento onde mora o NSGA	2019-05-15 16:08:10.481395-03	2019-05-15 16:10:07.045187-03	3	t
6	\N	0.03	SAFs/Horta		2019-05-15 15:07:52.719387-03	2019-05-15 15:33:24.506068-03	3	f
7	\N	0.02	Porcos		2019-05-15 15:34:22.08151-03	2019-05-15 15:41:02.547002-03	3	f
8	\N	0.09	Galinhas	As galinhas são criadas soltas e a área considerada é o quintal onde pastam	2019-05-15 15:42:41.069795-03	2019-05-15 15:48:11.01694-03	3	f
9	\N	0.09	Pomar/quintal		2019-05-15 15:49:03.000078-03	2019-05-15 15:55:49.744382-03	3	f
4	\N	1.24	Roças (feijão, milho, melancia, fava)	0,33 ha de roça cultivada no lote da mãe da Jucilene e 0,91 ha no lote que a família tem em parceria com o irmão do Rondom	2019-05-14 19:57:36.356312-03	2019-05-15 15:02:30.341609-03	3	f
10	\N	0.01	Peixe/açude		2019-05-15 15:57:26.381865-03	2019-05-15 15:58:21.623624-03	3	f
\.


--
-- Data for Name: economica_subsistemaaquisicaoreciprocidade; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.economica_subsistemaaquisicaoreciprocidade (id, deleted, valor_unitario, quantidade, observacoes, tipo, criacao, ultima_alteracao, produto_id, subsistema_id, unidade_id, servico_id) FROM stdin;
1	\N	50.00	14.50		S	2019-05-14 20:10:56.330605-03	2019-05-14 20:10:56.330672-03	\N	4	2	5
2	2019-05-15 15:06:16.257339-03	50.00	1.25		I	2019-05-14 20:26:53.569238-03	2019-05-14 20:26:53.569259-03	22	5	2	\N
3	\N	50.00	1.25		S	2019-05-15 15:06:48.902235-03	2019-05-15 15:06:48.902261-03	\N	5	3	9
4	\N	50.00	13.50		S	2019-05-15 15:33:24.470345-03	2019-05-15 15:33:24.470371-03	\N	6	2	5
5	\N	0.05	36500.00		I	2019-05-15 15:41:02.453987-03	2019-05-15 15:41:02.454008-03	56	7	1	\N
6	\N	50.00	172.00		S	2019-05-15 16:07:27.320992-03	2019-05-15 16:07:27.321016-03	\N	11	2	5
\.


--
-- Data for Name: economica_subsistemainsumo; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.economica_subsistemainsumo (id, deleted, valor_unitario, quantidade, origem, observacoes, tipo, criacao, ultima_alteracao, produto_id, subsistema_id, unidade_id) FROM stdin;
3	\N	10.00	0.00	\N		P	2019-05-14 20:08:13.27306-03	2019-05-14 20:08:13.273091-03	9	4	12
4	\N	3.70	12.00	F	Gasto de deslocamento para ir até o roçado que fica no outro lote do NSGA, mas onde não residem	C	2019-05-14 20:09:03.990261-03	2019-05-14 20:09:03.990305-03	10	4	13
6	\N	27.00	10.00	F		C	2019-05-14 20:24:25.037969-03	2019-05-14 20:24:25.037991-03	15	5	12
7	\N	0.36	250.00	F		C	2019-05-14 20:24:25.038861-03	2019-05-14 20:24:25.038882-03	16	5	12
8	\N	1.60	217.00	F		C	2019-05-14 20:24:25.039713-03	2019-05-14 20:24:25.039732-03	17	5	15
9	\N	4.00	12.00	F		C	2019-05-14 20:24:25.040584-03	2019-05-14 20:24:25.040604-03	18	5	15
10	\N	330.00	3.00	F		C	2019-05-14 20:24:25.041478-03	2019-05-14 20:24:25.041499-03	19	5	16
11	\N	200.00	12.50	F		C	2019-05-14 20:24:25.04272-03	2019-05-14 20:24:25.042746-03	20	5	14
12	\N	65.00	3.00	F		C	2019-05-14 20:24:25.043724-03	2019-05-14 20:24:25.043745-03	21	5	14
5	\N	60.00	7.00	F		C	2019-05-14 20:24:25.036677-03	2019-05-14 20:24:33.088096-03	14	5	14
1	2019-05-15 12:35:25.260357-03	7.50	2.00	\N		P	2019-05-14 20:08:13.245703-03	2019-05-14 20:08:18.253741-03	6	4	12
2	2019-05-15 12:43:26.123937-03	0.77	6.00	\N		P	2019-05-14 20:08:13.271709-03	2019-05-14 20:08:13.271745-03	8	4	12
13	\N	50.00	0.60	F		P	2019-05-15 15:31:33.816994-03	2019-05-15 15:31:33.817019-03	39	6	12
14	\N	10.00	2.00	\N		P	2019-05-15 15:31:33.818485-03	2019-05-15 15:31:33.818509-03	40	6	12
15	\N	0.77	2.00	\N		P	2019-05-15 15:31:33.819781-03	2019-05-15 15:31:33.819806-03	41	6	12
16	\N	5.00	20.00	\N		P	2019-05-15 15:31:33.820944-03	2019-05-15 15:31:33.820966-03	42	6	14
17	\N	15.00	1.00	\N		P	2019-05-15 15:31:33.821897-03	2019-05-15 15:31:33.821918-03	43	6	14
18	\N	1.33	18.00	\N		P	2019-05-15 15:31:33.822825-03	2019-05-15 15:31:33.822845-03	44	6	14
19	\N	10.00	1.00	\N		P	2019-05-15 15:31:33.823841-03	2019-05-15 15:31:33.823862-03	45	6	1
20	\N	2.00	20.00	\N		P	2019-05-15 15:31:33.824909-03	2019-05-15 15:31:33.824931-03	46	6	1
21	\N	0.13	20.00	\N		P	2019-05-15 15:31:33.825801-03	2019-05-15 15:31:45.169176-03	47	6	12
22	\N	75.00	0.40	F		C	2019-05-15 15:32:40.070058-03	2019-05-15 15:32:40.070081-03	48	6	12
23	\N	1.00	24.00	F		P	2019-05-15 15:38:24.451861-03	2019-05-15 15:38:24.451891-03	53	7	12
24	2019-05-15 15:38:41.629002-03	100.00	2400.00	F		P	2019-05-15 15:38:24.767509-03	2019-05-15 15:38:24.767542-03	53	7	12
25	\N	0.77	144.00	F		C	2019-05-15 15:48:10.903134-03	2019-05-15 15:48:10.903161-03	3	8	12
26	\N	3.70	12.00	F		C	2019-05-15 16:10:07.012024-03	2019-05-15 16:10:07.012063-03	10	12	13
\.


--
-- Data for Name: economica_subsistemapagamentoterceiros; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.economica_subsistemapagamentoterceiros (id, deleted, valor_unitario, quantidade, origem, observacoes, criacao, ultima_alteracao, servico_id, subsistema_id, unidade_id) FROM stdin;
1	\N	13.00	1.00	T		2019-05-14 11:07:34.281302-03	2019-05-14 11:07:34.281328-03	1	2	1
2	\N	70.00	3.00	F		2019-05-14 11:08:12.029956-03	2019-05-14 11:08:12.029984-03	2	2	2
3	\N	50.00	21.50	T		2019-05-14 20:26:17.364419-03	2019-05-14 20:26:17.364444-03	6	5	2
4	\N	160.00	35.00	F		2019-05-14 20:26:17.365776-03	2019-05-14 20:26:17.365797-03	7	5	17
5	\N	100.00	5.00	T		2019-05-14 20:26:17.36667-03	2019-05-14 20:26:17.36669-03	8	5	18
\.


--
-- Data for Name: economica_subsistemaproducao; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.economica_subsistemaproducao (id, deleted, observacoes, valor_unitario, quantidade_venda, quantidade_autoconsumo, quantidade_doacoes_trocas, quantidade_estoque, quantidade_insumos_produzidos, criacao, ultima_alteracao, produto_id, subsistema_id, unidade_id) FROM stdin;
3	\N		0.80	\N	24.00	6.00	\N	\N	2019-05-14 20:05:08.333188-03	2019-05-14 20:05:08.333209-03	4	4	12
4	\N		10.00	\N	19.00	3.50	\N	10.00	2019-05-14 20:05:08.334033-03	2019-05-14 20:05:08.334054-03	5	4	12
5	\N		0.75	21158.00	730.00	12.00	\N	\N	2019-05-14 20:20:53.921366-03	2019-05-14 20:20:53.921391-03	11	5	13
6	\N		450.00	15.00	\N	\N	24.00	\N	2019-05-14 20:20:53.922633-03	2019-05-14 20:20:53.922655-03	12	5	8
7	\N		1320.00	4.75	0.25	\N	\N	\N	2019-05-14 20:20:53.92352-03	2019-05-14 20:20:53.92354-03	13	5	8
1	2019-05-15 12:41:05.583061-03		7.00	\N	104.50	5.50	\N	20.00	2019-05-14 20:05:08.301241-03	2019-05-14 20:05:08.30127-03	2	4	12
2	2019-05-15 12:41:17.411773-03		0.77	\N	13.50	0.50	56.00	15.00	2019-05-14 20:05:08.33211-03	2019-05-14 20:05:08.332136-03	3	4	12
9	\N		2.00	0.45	\N	\N	\N	\N	2019-05-15 13:20:30.358581-03	2019-05-15 13:20:30.358612-03	21	5	12
10	\N		0.77	\N	13.50	0.50	56.00	15.00	2019-05-15 14:59:55.582291-03	2019-05-15 14:59:55.582314-03	3	4	12
11	\N		7.00	\N	104.50	5.50	\N	20.00	2019-05-15 14:59:55.583612-03	2019-05-15 14:59:55.583636-03	2	4	12
8	\N		5.00	\N	\N	\N	\N	30.00	2019-05-14 20:20:53.924576-03	2019-05-15 15:04:56.62064-03	1	5	14
12	\N		2.00	12.50	11.88	0.62	\N	\N	2019-05-15 15:26:57.7752-03	2019-05-15 15:26:57.775243-03	23	6	12
13	\N		2.00	10.00	9.50	0.50	\N	\N	2019-05-15 15:26:57.777357-03	2019-05-15 15:26:57.777401-03	24	6	12
14	\N		2.00	20.00	56.00	5.00	\N	\N	2019-05-15 15:26:57.778968-03	2019-05-15 15:26:57.779007-03	25	6	12
15	\N		2.00	100.00	47.00	3.00	\N	\N	2019-05-15 15:26:57.780324-03	2019-05-15 15:26:57.780347-03	26	6	19
16	\N		4.00	15.00	4.50	0.50	\N	\N	2019-05-15 15:26:57.781312-03	2019-05-15 15:26:57.781334-03	27	6	12
17	\N		4.00	10.00	9.50	0.50	\N	\N	2019-05-15 15:26:57.782231-03	2019-05-15 15:26:57.782252-03	28	6	12
18	\N		3.00	\N	47.50	2.50	\N	\N	2019-05-15 15:26:57.783156-03	2019-05-15 15:26:57.783177-03	29	6	12
19	\N		2.00	5.00	14.25	0.75	\N	\N	2019-05-15 15:26:57.784022-03	2019-05-15 15:26:57.784042-03	30	6	12
20	\N		12.00	2.00	7.50	0.50	\N	\N	2019-05-15 15:26:57.784871-03	2019-05-15 15:26:57.78489-03	31	6	12
21	\N		8.00	2.00	2.85	0.15	\N	\N	2019-05-15 15:26:57.785811-03	2019-05-15 15:26:57.785833-03	32	6	12
22	\N		30.00	0.50	6.65	0.35	\N	\N	2019-05-15 15:26:57.786709-03	2019-05-15 15:26:57.786731-03	33	6	12
23	\N		4.00	10.00	9.50	0.50	\N	\N	2019-05-15 15:26:57.787599-03	2019-05-15 15:26:57.787619-03	34	6	12
24	\N		2.00	\N	3.80	0.20	\N	\N	2019-05-15 15:26:57.788514-03	2019-05-15 15:26:57.788535-03	35	6	12
25	\N		20.00	1.50	0.47	0.03	\N	\N	2019-05-15 15:26:57.789407-03	2019-05-15 15:26:57.789427-03	36	6	12
26	\N		4.00	1.00	1.90	0.10	\N	\N	2019-05-15 15:26:57.790274-03	2019-05-15 15:26:57.790294-03	37	6	12
27	\N		10.00	3.50	3.80	0.20	\N	2.00	2019-05-15 15:26:57.791152-03	2019-05-15 15:26:57.791181-03	38	6	12
28	\N		50.00	\N	\N	\N	\N	0.60	2019-05-15 15:26:57.792314-03	2019-05-15 15:26:57.792337-03	39	6	12
29	\N		100.00	7.00	\N	1.00	3.00	\N	2019-05-15 15:37:57.099048-03	2019-05-15 15:37:57.099087-03	49	7	8
30	\N		250.00	3.00	\N	\N	8.00	\N	2019-05-15 15:37:57.100614-03	2019-05-15 15:37:57.100637-03	50	7	8
31	\N		7.33	\N	15.00	55.00	\N	\N	2019-05-15 15:37:57.10157-03	2019-05-15 15:37:57.101592-03	51	7	12
32	\N		1.50	\N	2.00	\N	\N	\N	2019-05-15 15:37:57.102447-03	2019-05-15 15:37:57.102467-03	52	7	1
33	\N		25.00	\N	12.00	2.00	20.00	\N	2019-05-15 15:47:30.789493-03	2019-05-15 15:47:30.789516-03	57	8	8
34	\N		8.00	\N	24.00	1.00	\N	\N	2019-05-15 15:47:30.79087-03	2019-05-15 15:47:30.790894-03	58	8	20
35	\N	Esse esterco é utilizado nas plantas do Pomar/quintal	0.30	\N	\N	\N	\N	25.00	2019-05-15 15:47:30.791818-03	2019-05-15 15:47:30.791838-03	1	8	12
36	\N		1.00	\N	105.00	105.00	\N	\N	2019-05-15 15:55:49.679133-03	2019-05-15 15:55:49.679159-03	59	9	12
37	\N		1.00	\N	300.00	\N	\N	\N	2019-05-15 15:55:49.680485-03	2019-05-15 15:55:49.680509-03	60	9	12
38	\N		1.00	\N	50.00	\N	\N	\N	2019-05-15 15:55:49.681564-03	2019-05-15 15:55:49.681588-03	61	9	12
39	\N		1.00	\N	20.00	\N	\N	\N	2019-05-15 15:55:49.68253-03	2019-05-15 15:55:49.682553-03	62	9	12
40	\N		1.50	5.00	50.00	\N	\N	\N	2019-05-15 15:55:49.683478-03	2019-05-15 15:55:49.6835-03	63	9	12
41	\N		1.00	\N	20.00	10.00	\N	\N	2019-05-15 15:55:49.684429-03	2019-05-15 15:55:49.684451-03	4	9	12
42	\N		1.00	\N	500.00	400.00	\N	\N	2019-05-15 15:55:49.685351-03	2019-05-15 15:55:49.685373-03	64	9	5
43	\N		1.00	\N	5.00	\N	\N	\N	2019-05-15 15:55:49.68642-03	2019-05-15 15:55:49.68646-03	65	9	12
44	\N		1.00	\N	2.00	\N	\N	\N	2019-05-15 15:55:49.687914-03	2019-05-15 15:55:49.687944-03	66	9	12
45	\N		1.00	\N	2.00	\N	\N	\N	2019-05-15 15:55:49.689025-03	2019-05-15 15:55:49.689051-03	67	9	12
46	\N		1.33	\N	\N	\N	\N	18.00	2019-05-15 15:55:49.690029-03	2019-05-15 15:55:49.690052-03	68	9	14
47	\N		7.00	\N	10.00	\N	\N	\N	2019-05-15 15:58:21.580848-03	2019-05-15 15:58:21.580871-03	69	10	12
48	\N		10.00	52.00	48.00	\N	\N	\N	2019-05-15 16:04:02.585279-03	2019-05-15 16:04:02.585302-03	70	11	13
49	\N		1.50	\N	160.00	20.00	\N	\N	2019-05-15 16:04:02.586635-03	2019-05-15 16:04:02.586657-03	71	11	21
50	\N		20.00	10.00	48.00	\N	\N	\N	2019-05-15 16:04:02.587509-03	2019-05-15 16:04:02.58754-03	72	11	14
51	\N		0.20	\N	20.00	\N	\N	\N	2019-05-15 16:04:02.588699-03	2019-05-15 16:04:02.588721-03	73	11	12
52	\N		0.15	\N	300.00	\N	\N	\N	2019-05-15 16:04:02.589647-03	2019-05-15 16:04:02.589667-03	74	11	5
53	\N		0.13	\N	\N	\N	\N	60.00	2019-05-15 16:04:02.5905-03	2019-05-15 16:04:02.590521-03	47	11	12
54	\N		7.00	\N	15.00	3.00	\N	\N	2019-05-15 16:09:27.086767-03	2019-05-15 16:09:27.086792-03	69	12	12
55	\N		15.00	\N	100.00	\N	\N	\N	2019-05-15 16:09:27.0881-03	2019-05-15 16:09:27.088123-03	80	12	5
\.


--
-- Data for Name: economica_subsistematempotrabalho; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.economica_subsistematempotrabalho (id, horas, criacao, ultima_alteracao, subsistema_id, composicao_nsga_id) FROM stdin;
1	8.00	2019-05-14 11:05:14.006239-03	2019-05-14 11:05:14.006284-03	2	1
2	3.00	2019-05-14 11:05:24.864577-03	2019-05-14 11:05:24.864609-03	2	3
3	92.00	2019-05-14 19:59:07.135222-03	2019-05-14 19:59:07.135264-03	4	4
4	100.00	2019-05-14 19:59:07.136737-03	2019-05-14 19:59:07.136773-03	4	6
5	100.00	2019-05-14 19:59:07.137764-03	2019-05-14 19:59:07.137792-03	4	5
6	4.00	2019-05-14 19:59:07.138623-03	2019-05-14 19:59:07.138647-03	4	7
7	4.00	2019-05-14 19:59:07.139434-03	2019-05-14 19:59:07.139457-03	4	8
8	45.00	2019-05-14 19:59:07.140292-03	2019-05-14 19:59:07.140315-03	4	10
9	51.00	2019-05-14 19:59:07.14106-03	2019-05-14 19:59:07.141083-03	4	11
10	1000.00	2019-05-14 20:16:06.106897-03	2019-05-14 20:16:06.106924-03	5	6
11	1046.00	2019-05-14 20:16:06.108217-03	2019-05-14 20:16:06.108287-03	5	5
12	500.00	2019-05-14 20:16:06.109543-03	2019-05-14 20:16:06.109577-03	5	10
13	540.25	2019-05-14 20:16:06.110767-03	2019-05-14 20:16:06.110818-03	5	11
14	218.00	2019-05-15 15:08:49.032692-03	2019-05-15 15:08:49.032726-03	6	4
15	200.00	2019-05-15 15:08:49.034094-03	2019-05-15 15:08:49.034125-03	6	7
16	150.00	2019-05-15 15:08:49.034991-03	2019-05-15 15:08:49.035017-03	6	8
17	78.50	2019-05-15 15:08:49.035896-03	2019-05-15 15:08:49.035922-03	6	9
18	100.00	2019-05-15 15:35:07.763909-03	2019-05-15 15:35:07.763937-03	7	6
19	84.00	2019-05-15 15:35:07.765108-03	2019-05-15 15:35:07.765132-03	7	5
20	3.00	2019-05-15 15:35:07.765908-03	2019-05-15 15:35:07.765931-03	7	10
21	61.00	2019-05-15 15:45:19.949968-03	2019-05-15 15:45:19.949996-03	8	4
22	30.00	2019-05-15 15:45:19.95114-03	2019-05-15 15:45:19.951163-03	8	10
23	31.00	2019-05-15 15:45:19.951947-03	2019-05-15 15:45:19.951993-03	8	11
24	46.00	2019-05-15 15:49:53.701824-03	2019-05-15 15:49:53.701863-03	9	4
25	100.00	2019-05-15 15:49:53.70315-03	2019-05-15 15:49:53.703175-03	9	7
26	36.86	2019-05-15 15:49:53.703896-03	2019-05-15 15:49:53.703926-03	9	9
27	20.00	2019-05-15 15:49:53.704976-03	2019-05-15 15:49:53.705016-03	9	11
29	2.00	2019-05-15 15:57:36.981015-03	2019-05-15 15:57:36.981046-03	10	11
30	512.00	2019-05-15 16:00:36.260449-03	2019-05-15 16:00:36.260489-03	11	4
31	112.00	2019-05-15 16:00:36.262314-03	2019-05-15 16:00:36.262348-03	11	5
32	400.00	2019-05-15 16:00:36.263272-03	2019-05-15 16:00:36.263296-03	11	7
33	400.00	2019-05-15 16:00:36.264067-03	2019-05-15 16:00:36.264091-03	11	8
34	424.00	2019-05-15 16:00:36.264922-03	2019-05-15 16:00:36.264946-03	11	9
35	84.00	2019-05-15 16:00:36.265753-03	2019-05-15 16:00:36.265776-03	11	10
36	100.00	2019-05-15 16:00:36.266583-03	2019-05-15 16:00:36.266605-03	11	11
37	24.00	2019-05-15 16:08:23.986177-03	2019-05-15 16:08:23.98625-03	12	10
\.


--
-- Data for Name: economica_tempotrabalho; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.economica_tempotrabalho (id, criacao, ultima_alteracao, analise_id, composicao_nsga_id, horas_domestico_cuidados, horas_participacao_social, horas_pluriatividade) FROM stdin;
1	2019-05-14 10:23:25.085427-03	2019-05-14 10:23:25.085473-03	1	1	4	0	0
2	2019-05-14 10:23:25.139742-03	2019-05-14 10:23:25.139772-03	1	3	2	0	0
3	2019-05-14 18:44:42.682927-03	2019-05-14 18:44:42.682953-03	3	4	1095	116	36
4	2019-05-14 18:44:42.684193-03	2019-05-14 18:44:42.684216-03	3	6	0	16	0
5	2019-05-14 18:44:42.685017-03	2019-05-14 18:44:42.68504-03	3	7	1000	0	0
6	2019-05-14 18:44:42.68578-03	2019-05-14 18:44:42.685803-03	3	8	825	0	0
7	2019-05-14 18:44:42.68653-03	2019-05-14 18:44:42.686552-03	3	9	0	196	0
8	2019-05-14 18:45:31.996617-03	2019-05-14 18:45:31.996667-03	3	10	0	0	40
9	2019-05-14 18:45:31.998125-03	2019-05-14 18:45:31.998166-03	3	11	0	0	40
\.


--
-- Data for Name: linhadotempo_dimensao; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.linhadotempo_dimensao (id, nome, ordem, descricao, tipo_dimensao_id, deleted) FROM stdin;
1	Capital fundiário e equipamentos	1	##### Inovações:\r\n- Aquisição de terra (herança, compra);\r\n- Construção e ou reformas da casa;\r\n- Infraestruturas hídricas (barreiros, barragens, barragem subterrânea, tanques de pedra, etc. \r\n- Aquisição de equipamentos: carroça, boi de tração, moto, carro, etc..	1	\N
2	Produção animal	2	##### Inovações:\r\n- Infraestruturas do sistema pecuário (curral, aviário, pocilga, campos de palma, cercas), maquina forrageira, etc..\r\n- Aquisição de matrizes bovino, ovino e caprinos, etc..	1	\N
3	Produção vegetal	3	##### Inovações:\r\nCultivos anuais, pomar, bosques. biofertilizante, caldas bioprotetoras, cerca viva, compostagem, etc..	1	\N
4	Sistema peridoméstico	4	##### Inovações nos sub-sistemas do quintal:\r\n- Infraestruturas dos SPDs - cisternas, canteiro econômico, telas, cercados, faxina;\r\n- Aves, suínos, pequenos ruminantes;\r\n- Pomar, hortas, medicinais;\r\n- Processamento doméstico;\r\n- Fogão ecoeficiente.	1	\N
5	Ciclo de vida da família	5	Casamento, nascimento filhos, migração, falecimentos, etc..	1	\N
6	Participação na gestão de bens comuns	1	##### Inovações:\r\n\r\n###### Paraticipação/acesso a:\r\n\r\n- Insumos e equipamentos: Fundos Solidários (Banco de Sementes, FRS Animais, etc..)\r\n- Conhecimento - Redes de gestão do conhecimento (intercâmbios, oficinas);\r\n- Força de trabalho - mutirões, troca de dias de trabalho, etc..	2	\N
7	Integração a espaços político-organizativos	2	##### Inovações:\r\n###### Participação em:\r\n- Associações\r\n- STRs\r\n- Redes.\r\n- Polos Sindicais\r\n- Movimentos Sociais	2	\N
8	Acesso aos mercados	3	##### Inovações:\r\n- CEASA.\r\n- Atravessador.\r\n- Mercado de proximidade/vizinhança\r\n- Mercado institucional: PAA/PNAE\r\n- Feira Livre.\r\n- Feira Agroecológicas.\r\n- Mercado Orgânico.	2	\N
9	Acesso a políticas públicas	4	##### Inovações:\r\n- ATER\r\n- Pesquisa\r\n- Crédito\r\n- Seguro safra\r\n- P1MC/P1+2\r\n- PAA/PNA\r\n- Credito Fundiário\r\n- Reforma Agrária\r\n- Previdência\r\n- Auxílio maternidade\r\n- Outros	2	\N
10	Outros	9	- Perturbações climáticas: seca, tempestades, enchentes.\r\n- Ameaças externas: empresas mineradoras, grandes obras,etc..\r\n- Outros	2	\N
11	Outros	9		1	\N
\.


--
-- Data for Name: linhadotempo_evento; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.linhadotempo_evento (id, data_inicio, data_fim, titulo, detalhes, agroecossistema_id, criado_por_id, dimensao_id, criacao, ultima_alteracao, deleted) FROM stdin;
2	2014	2020	Mais um evento	<p>Esta é uma coisa.<br></p>	1	2	8	2019-04-10 13:15:41.358232-03	2019-04-10 13:15:41.358279-03	\N
3	2014	\N	Casamento de jorge	<p>Já separaram?<br></p>	1	1	6	2019-04-11 13:32:33.764513-03	2019-04-11 13:32:33.764572-03	\N
1	2004	\N	Casamento de sofia	<p>O porta-voz da Presidência, Otávio do Rêgo Barros, classificou como \r\n“incidente” o fuzilamento carro de uma família no&nbsp;Rio de \r\nJaneiro,&nbsp;alvejado por mais de 80 tiros no último domingo, 9. Questionado\r\n se o presidente&nbsp;Jair Bolsonaro&nbsp;fez algum tipo de manifestação de pesar \r\npela&nbsp;morte de uma das vítimas, o músico Evaldo Rosa dos Santos, o \r\nporta-voz negou. “Não, não fez”, respondeu. Passados dois dias, o \r\npresidente ainda não falou publicamente sobre o ocorrido.</p>\r\n<p>Ao ser indagado se o fuzilamento causa constrangimento ao Planalto, o\r\n porta-voz classificou o episódio como “incidente”. “A questão referente\r\n ao incidente com a morte do cidadão, eu repito: o Comando Militar do \r\nLeste e o Exército Brasileiro estão apurando os eventos em um inquérito \r\npolicial militar, que está sendo acompanhado pela Justiça Militar e \r\nMinistério Público”, disse.</p>\r\n<p>Rêgo Barros afirmou que o presidente Bolsonaro pede que o caso seja \r\n“o mais rapidamente elucidado”, destacando que&nbsp;existe independência \r\nentre os poderes. Também defendeu que seja feita uma apuração “mais \r\ncorreta e justa possível”. “As instituições do Exército Brasileiro, as \r\ninstituições das Forças Armadas não compartilham com o equívoco dos seus\r\n integrantes, mas por óbvio precisa que seja feita uma apuração mais \r\ncorreta e justa possível.”</p><div class="code-block code-block-2" style="margin: 8px auto; text-align: center; display: block; clear: both;">\r\n\r\n</div>\r\n\r\n<p>O porta-voz disse, ainda, que o Palácio do Planalto “confia no \r\ndesempenho e nas ações da Justiça Militar e do Ministério Público \r\nMilitar e mais ainda nas ações do destacadas pelo Exército na condução \r\ndo inquérito&nbsp; para elucidação total do fato”.</p>\r\n<p>Sobre o fato de que Bolsonaro utilizar com frequência as redes \r\nsociais para comentar assuntos diversos relacionados à crimes e ao porte\r\n de armas, mas não ter mencionado o episódio em questão, o porta-voz \r\ndesconversou.</p>	1	1	3	2019-04-08 15:11:02.210451-03	2019-04-10 13:18:12.04784-03	2019-04-17 16:37:32.412118-03
\.


--
-- Data for Name: linhadotempo_eventogrupo; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.linhadotempo_eventogrupo (id, nome, descricao, criacao, ultima_alteracao, agroecossistema_id, deleted) FROM stdin;
\.


--
-- Data for Name: linhadotempo_eventogrupo_eventos; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.linhadotempo_eventogrupo_eventos (id, eventogrupo_id, evento_id) FROM stdin;
\.


--
-- Data for Name: linhadotempo_tipodimensao; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.linhadotempo_tipodimensao (id, nome, deleted) FROM stdin;
1	Agroecossistema	\N
2	Território/mercados	\N
\.


--
-- Data for Name: main_organizacaousuaria; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.main_organizacaousuaria (id, is_admin, organizacao_id, usuaria_id, deleted) FROM stdin;
1	t	1	2	\N
2	f	2	2	\N
\.


--
-- Data for Name: main_produto; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.main_produto (id, deleted, nome, criacao, ultima_alteracao) FROM stdin;
1	\N	Esterco	2019-05-11 11:46:39.321083-03	2019-05-11 11:46:39.321108-03
2	\N	Feijão	2019-05-14 19:59:30.846574-03	2019-05-14 19:59:30.846607-03
3	\N	Milho	2019-05-14 19:59:41.663946-03	2019-05-14 19:59:41.663999-03
4	\N	Melancia	2019-05-14 19:59:46.517359-03	2019-05-14 19:59:46.517384-03
5	\N	Fava	2019-05-14 19:59:52.367872-03	2019-05-14 19:59:52.367902-03
6	\N	Semente de feijão	2019-05-14 20:05:26.316269-03	2019-05-14 20:05:26.316294-03
7	\N	emente de milho	2019-05-14 20:05:38.721496-03	2019-05-14 20:05:38.721523-03
8	\N	Semente de milho	2019-05-14 20:05:57.107757-03	2019-05-14 20:05:57.107786-03
9	\N	Semente de fava	2019-05-14 20:06:02.690359-03	2019-05-14 20:06:02.690388-03
10	\N	deslocamento/gasolina	2019-05-14 20:08:38.515797-03	2019-05-14 20:08:38.515832-03
11	\N	Leite	2019-05-14 20:17:18.738183-03	2019-05-14 20:17:18.738229-03
12	\N	Bezerro	2019-05-14 20:17:37.45308-03	2019-05-14 20:17:37.453104-03
13	\N	Vacas	2019-05-14 20:17:46.259139-03	2019-05-14 20:17:46.259168-03
14	\N	Ração	2019-05-14 20:21:20.578337-03	2019-05-14 20:21:20.578363-03
15	\N	Sal mineral	2019-05-14 20:21:26.478483-03	2019-05-14 20:21:26.478512-03
16	\N	Sal branco	2019-05-14 20:21:32.105263-03	2019-05-14 20:21:32.10529-03
17	\N	Vacinas	2019-05-14 20:21:37.094377-03	2019-05-14 20:21:37.094403-03
18	\N	Vermífugo	2019-05-14 20:21:43.720876-03	2019-05-14 20:21:43.720902-03
19	\N	Arame de cerca	2019-05-14 20:21:54.491859-03	2019-05-14 20:21:54.491884-03
20	\N	Semente de braquíaria	2019-05-14 20:22:01.091527-03	2019-05-14 20:22:01.091557-03
21	\N	Semente de quiquio	2019-05-14 20:22:09.687495-03	2019-05-14 20:22:09.68752-03
22	\N	Queimada	2019-05-14 20:26:33.423846-03	2019-05-14 20:26:33.423876-03
23	\N	Maxixe	2019-05-15 15:09:20.070715-03	2019-05-15 15:09:20.070746-03
24	\N	Quiabo	2019-05-15 15:09:25.718606-03	2019-05-15 15:09:25.718631-03
25	\N	Cheiro verde	2019-05-15 15:09:38.259239-03	2019-05-15 15:09:38.259274-03
26	\N	Alface	2019-05-15 15:09:43.297396-03	2019-05-15 15:09:43.297423-03
27	\N	Jiló	2019-05-15 15:09:48.360357-03	2019-05-15 15:09:48.360396-03
28	\N	Berinjela	2019-05-15 15:10:20.323745-03	2019-05-15 15:10:20.323775-03
29	\N	Batata doce	2019-05-15 15:10:45.995294-03	2019-05-15 15:10:45.995318-03
30	\N	Abobrinha	2019-05-15 15:10:51.16546-03	2019-05-15 15:10:51.165489-03
31	\N	Vinagreira/cuchá	2019-05-15 15:10:56.818755-03	2019-05-15 15:10:56.818783-03
32	\N	Couve	2019-05-15 15:11:04.045483-03	2019-05-15 15:11:04.045509-03
33	\N	Rúcula	2019-05-15 15:11:09.20411-03	2019-05-15 15:11:09.204141-03
34	\N	Milho Verde	2019-05-15 15:11:14.360366-03	2019-05-15 15:11:14.360395-03
35	\N	Gerimum	2019-05-15 15:11:50.911861-03	2019-05-15 15:11:50.91189-03
36	\N	Pimenta de cheiro	2019-05-15 15:11:56.225998-03	2019-05-15 15:11:56.226027-03
37	\N	Tomatinho	2019-05-15 15:12:01.721893-03	2019-05-15 15:12:01.72192-03
38	\N	Feijão verde	2019-05-15 15:12:07.418363-03	2019-05-15 15:12:07.418395-03
39	\N	Sementes de hortaliças	2019-05-15 15:12:12.09007-03	2019-05-15 15:12:12.090101-03
40	\N	Sementes de feijão	2019-05-15 15:27:33.206979-03	2019-05-15 15:27:33.207005-03
41	\N	Sementes de milho	2019-05-15 15:27:38.453395-03	2019-05-15 15:27:38.453424-03
42	\N	Esterco de gado	2019-05-15 15:27:45.063921-03	2019-05-15 15:27:45.063946-03
43	\N	Tronco de babaçu	2019-05-15 15:27:50.49746-03	2019-05-15 15:27:50.497485-03
44	\N	Folha do quintal	2019-05-15 15:27:57.188341-03	2019-05-15 15:27:57.188387-03
45	\N	Óleo pirolenhoso	2019-05-15 15:28:05.045338-03	2019-05-15 15:28:05.045368-03
46	\N	Extrato de Neem	2019-05-15 15:28:12.947866-03	2019-05-15 15:28:12.947907-03
47	\N	Cinza	2019-05-15 15:28:19.250638-03	2019-05-15 15:28:19.250663-03
48	\N	Semente de hortaliças	2019-05-15 15:32:20.978219-03	2019-05-15 15:32:20.978246-03
49	\N	Leitão apartado (criadores)	2019-05-15 15:35:23.465925-03	2019-05-15 15:35:23.465959-03
50	\N	Leitão apartado (açougue)	2019-05-15 15:35:34.557682-03	2019-05-15 15:35:34.557709-03
51	\N	Carne	2019-05-15 15:35:40.127073-03	2019-05-15 15:35:40.127104-03
52	\N	Banha	2019-05-15 15:35:47.498185-03	2019-05-15 15:35:47.498212-03
53	\N	Restos de frutas	2019-05-15 15:38:11.340797-03	2019-05-15 15:38:11.340824-03
54	\N	Ração/milho	2019-05-15 15:38:55.863439-03	2019-05-15 15:38:55.863473-03
55	\N	Ração/farelo de arroz	2019-05-15 15:39:51.614357-03	2019-05-15 15:39:51.614387-03
56	\N	Soro	2019-05-15 15:40:30.263959-03	2019-05-15 15:40:30.264027-03
57	\N	Galinhas	2019-05-15 15:45:33.930091-03	2019-05-15 15:45:33.930123-03
58	\N	Ovos	2019-05-15 15:45:43.515459-03	2019-05-15 15:45:43.515487-03
59	\N	Laranja	2019-05-15 15:50:05.356464-03	2019-05-15 15:50:05.356494-03
60	\N	Manga	2019-05-15 15:50:19.686213-03	2019-05-15 15:50:19.686243-03
61	\N	Lima	2019-05-15 15:50:24.825901-03	2019-05-15 15:50:24.825926-03
62	\N	Limão	2019-05-15 15:50:29.487228-03	2019-05-15 15:50:29.487254-03
63	\N	Acerola	2019-05-15 15:50:34.202807-03	2019-05-15 15:50:34.202833-03
64	\N	Coco	2019-05-15 15:51:12.333059-03	2019-05-15 15:51:12.333093-03
65	\N	Ata	2019-05-15 15:51:17.306053-03	2019-05-15 15:51:17.306081-03
66	\N	Dão	2019-05-15 15:51:22.126141-03	2019-05-15 15:51:22.126187-03
67	\N	Cajá manga	2019-05-15 15:51:27.057167-03	2019-05-15 15:51:27.057207-03
68	\N	Folhas	2019-05-15 15:51:31.678558-03	2019-05-15 15:51:31.678585-03
69	\N	Peixe	2019-05-15 15:57:51.808759-03	2019-05-15 15:57:51.808792-03
70	\N	Azeite de Babaçu	2019-05-15 16:01:03.29709-03	2019-05-15 16:01:03.297119-03
71	\N	Sabão de Babaçu	2019-05-15 16:01:10.298082-03	2019-05-15 16:01:10.298108-03
72	\N	Carvão	2019-05-15 16:01:17.45608-03	2019-05-15 16:01:17.456114-03
73	\N	Adubo do tronco 	2019-05-15 16:01:23.668321-03	2019-05-15 16:01:23.668349-03
74	\N	Talo para cerca	2019-05-15 16:01:29.626356-03	2019-05-15 16:01:29.626381-03
75	\N	Amêndoa de Babaçu	2019-05-15 16:04:27.699091-03	2019-05-15 16:04:27.699122-03
76	\N	Garrafa Pet	2019-05-15 16:04:37.6512-03	2019-05-15 16:04:37.651229-03
77	\N	Lenha	2019-05-15 16:04:43.873094-03	2019-05-15 16:04:43.873132-03
78	\N	Latão	2019-05-15 16:06:15.480302-03	2019-05-15 16:06:15.480328-03
79	\N	Soda cáustica	2019-05-15 16:06:23.933952-03	2019-05-15 16:06:23.933981-03
80	\N	Estaca	2019-05-15 16:08:38.104925-03	2019-05-15 16:08:38.104955-03
\.


--
-- Data for Name: main_servico; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.main_servico (id, deleted, nome, criacao, ultima_alteracao) FROM stdin;
1	\N	roçado	2019-05-14 11:07:07.774302-03	2019-05-14 11:07:07.774332-03
2	\N	capina	2019-05-14 11:07:50.408097-03	2019-05-14 11:07:50.408146-03
3	\N	Energia elétrica	2019-05-14 19:56:39.703605-03	2019-05-14 19:56:39.703632-03
4	\N	Associação comunitária	2019-05-14 19:56:47.389242-03	2019-05-14 19:56:47.389268-03
5	\N	Mutirão	2019-05-14 20:10:26.68395-03	2019-05-14 20:10:26.683997-03
6	\N	formar pasto	2019-05-14 20:24:44.027261-03	2019-05-14 20:24:44.027287-03
7	\N	corte de terra com trator	2019-05-14 20:24:55.631029-03	2019-05-14 20:24:55.631066-03
8	\N	Tirar estacas e furar	2019-05-14 20:25:04.23332-03	2019-05-14 20:25:04.233346-03
9	\N	Queimada	2019-05-15 15:06:28.414812-03	2019-05-15 15:06:28.414837-03
\.


--
-- Data for Name: main_territorio; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.main_territorio (id, nome, descricao, criacao, ultima_alteracao, deleted) FROM stdin;
\.


--
-- Data for Name: main_unidade; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.main_unidade (id, deleted, nome, criacao, ultima_alteracao) FROM stdin;
1	\N	litro	2019-05-11 11:47:06.695037-03	2019-05-11 11:47:06.695069-03
2	\N	diária	2019-05-14 11:07:56.650816-03	2019-05-14 11:07:56.650846-03
3	\N	diárias	2019-05-14 18:46:00.662261-03	2019-05-14 18:46:00.662289-03
4	\N	retirada	2019-05-14 18:46:25.777653-03	2019-05-14 18:46:25.77768-03
5	\N	unidade	2019-05-14 18:48:07.661659-03	2019-05-14 18:48:07.66169-03
6	\N	ha	2019-05-14 18:48:41.645768-03	2019-05-14 18:48:41.645793-03
7	\N	metro	2019-05-14 18:53:31.924421-03	2019-05-14 18:53:31.924447-03
8	\N	cabeça	2019-05-14 19:52:05.764396-03	2019-05-14 19:52:05.764439-03
9	\N	enxame	2019-05-14 19:52:29.386106-03	2019-05-14 19:52:29.386139-03
10	\N	mensalidade	2019-05-14 19:56:53.509931-03	2019-05-14 19:56:53.509956-03
11	\N	anuidade	2019-05-14 19:56:57.011348-03	2019-05-14 19:56:57.011381-03
12	\N	kg	2019-05-14 19:59:59.375178-03	2019-05-14 19:59:59.375203-03
13	\N	litros	2019-05-14 20:08:43.643343-03	2019-05-14 20:08:43.643376-03
14	\N	saco	2019-05-14 20:18:13.561985-03	2019-05-14 20:18:13.562026-03
15	\N	dose	2019-05-14 20:22:27.833719-03	2019-05-14 20:22:27.833751-03
16	\N	rolo	2019-05-14 20:22:34.59386-03	2019-05-14 20:22:34.593887-03
17	\N	horas	2019-05-14 20:25:14.421446-03	2019-05-14 20:25:14.421472-03
18	\N	estaca	2019-05-14 20:25:20.080825-03	2019-05-14 20:25:20.080851-03
19	\N	pés	2019-05-15 15:12:30.509914-03	2019-05-15 15:12:30.509942-03
20	\N	dúzia	2019-05-15 15:45:57.239321-03	2019-05-15 15:45:57.239353-03
21	\N	barras	2019-05-15 16:01:52.005957-03	2019-05-15 16:01:52.005994-03
22	\N	m2	2019-05-15 16:05:03.793463-03	2019-05-15 16:05:03.793489-03
23	\N	m3	2019-05-15 16:05:12.1855-03	2019-05-15 16:05:12.185526-03
\.


--
-- Data for Name: municipios_diocese; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.municipios_diocese (id, nome) FROM stdin;
8	A. Goiânia
9	D. Patos de Minas
10	D. Anápolis
11	D. Luz
12	D. Abaetetuba
13	D. Crato
14	D. Livramento de Nossa Senhora
15	D. Paulo Afonso
16	D. Jacarezinho
17	D. Joaçaba
18	D. Marabá
19	D. Chapecó
20	A. São Salvador da Bahia
21	A. Mariana
22	A. Olinda e Recife
23	P. Cristalândia
24	A. Porto Velho
30	D. Imperatriz
31	D. Alagoinhas
37	A. Fortaleza
38	D. Sobral
39	D. Caicó
40	D. Picos
41	D. Itabira/Coronel Fabriciano
42	D. Iguatu
43	A. Cuiabá
44	D. Rio Branco
45	D. São Luís de Montes Belos
46	D. Mossoró
48	D. Governador Valadares
49	D. Marília
50	D. Goiás
59	D. Rio Preto
60	D. Paranaguá
62	D. Afogados da Ingazeira
63	A. Natal
64	A. Vitória
65	D. Brejo
66	D. Petrolina
67	P. Marajó
68	D. Macapá
69	D. Caruaru
70	A. Terezina
71	D. Rio do Sul
72	D. Guanhães
73	D. Barra do Garças
74	D. Palmeira dos Índios
75	D. Patos
77	A. Três Lagoas
78	A. Uberaba
80	D. São Mateus
81	D. Feira de Santana
82	D. Formosa
83	D. Itumbiara
85	D. Palmares
86	D. Passo Fundo
87	D. São Carlos
88	D. São João da Boa Vista
89	D. Oliveira
90	D. Garanhuns
92	A. Campinas
93	A. Botucatu
94	D. Piracicaba
95	D. Teófilo Otoni
96	D. Luziânia
97	A. Florianópolis
98	D. Araçuaí
99	D. Cachoeira do Sul
100	D. Bauru
101	A. Curitiba
103	D. Cajazeiras
104	D. Tocantinópolis
106	D. Jequié
108	D. Campanha
109	D. Cruz Alta
110	A. Paraíba
111	D. Campina Grande
112	D. Guarabira
113	D. Pesqueira
116	A. Sorocaba
117	A. Pouso Alegre
118	D. Pinheiro
121	P. Coxim
124	D. Teixeira de Freitas-Caravelas
130	D. Caxias do Maranhão
132	D. Teixeira de Freitas
133	D. Santo Ângelo
134	D. Cachoeiro do Itapemirim
135	D. Uruguaiana
137	D. Leopoldina
138	P. Óbidos
140	D. Ituiutaba
142	D. Guaxupé
144	D. Presidente Prudente
146	D. Nazaré
148	A. Porto Nacional
149	D. Ilhéus
150	D. Sete Lagoas
151	D. Santarém
152	D. Almenara
154	D. Itapipoca
156	D. Frederico Westphalen
158	D. Sinop
159	D. Guajará-Mirim
160	D. Ji-Paraná
162	P. Xingu
163	D. Bacabal
164	D. Guarapuava
167	A. Ribeirão Preto
168	D. Apucarana
169	D. Roraima
171	D. Lins
172	D. Coroatá
173	D. Viana
174	D. Guiratinga
176	P. São Félix do Araguaia
177	D. Caratinga
180	A. Porto Alegre
181	D. Uruaçu
183	D. Campo Maior
184	D. Joinville
185	D. Diamantino
188	D. Paranavaí
189	D. Balsas
190	D. Umuarama
192	D. Limoeiro do Norte
195	D. Osasco
196	P. Tefé
200	D. Assis
202	D. Porto Nacional
203	A. Diamantina
204	D. Bom Jesus da Gurgéia
206	A. Londrina
207	D. Dourados
209	D. Zé Doca
212	D. Santa Cruz do Sul
216	D. Amargosa
217	D. Alto Solimões
219	D. Irecê
220	D. Limeira
227	D. Propriá
229	D. Palmas/Francisco Beltrão
230	D. Caxias do Sul
231	D. Penedo
232	D. Vitória da Conquista
233	A. Cascavel
236	P. Coari
237	A. Belém do Pará
238	O. Militar do Brasil
240	D. Jardim
244	D. Ruy Barbosa
246	D. Bonfim
249	D. Araçatuba
250	D. Vacaria
251	D. São João Del Rei
252	A. Maceió
256	D. Barreiras
259	D. Itaguaí
263	A. Campo Grande
264	D. Ipameri
267	D. São Raimundo Nonato
268	D. Lages
269	D. Valença
275	D. Oeiras/Floriano
279	D. União da Vitória
280	EP. São João Batista em Curiti
284	A. Aparecida
285	D. Jales
287	D. Jaboticabal
289	D. Jataí
290	A. Palmas
291	D. Três Lagoas
292	D. Campos
294	D. Itapeva
299	D. Humaitá
303	A. Maringá
307	A. Aracaju
313	D. Caetité
315	A. Juiz de Fora
316	D. Colatina
320	D. Rubiataba/Mozarlândia
321	D. Uberlândia
322	D. Grajaú
323	D. Franca
326	D. Tianguá
327	D. Lorena
333	D. Tubarão
336	D. Crateús
338	D. Novo Hamburgo
341	A. Niterói
343	D. Campo Mourão
344	D. Itabuna
345	D. Erexim
348	D. Estância
350	D. Divinópolis
358	D. Petrópolis
364	D. Paracatu
378	D. Santa Maria
379	D. Pelotas
380	D. Caçador
383	D. Mogi das Cruzes
387	D. Cornélio Procópio
391	D. Toledo
393	D. Bragança Paulista
395	D. Bragança do Pará
400	P. Borba
403	A. São Luís do Maranhão
409	P. Cametá
413	D. Taubaté
417	D. Quixadá
425	D. Rondonópolis
429	D. São Gabriel da Cachoeira
431	D. Barra
434	D. Nova Friburgo
442	D. Miracema do Tocantins
445	D. Registro
451	D. Parintins
454	D. Barretos
461	D. Parnaíba
463	A. Bauru
470	D. Floresta
476	D. Nova Iguaçu
480	D. Eunápolis
483	O. P/Fiéis de Ritos Orientais
484	EP. N.S. do Líbano em São Paul
485	EP. N.S. do Paraíso em São Pau
486	A. Belo Horizonte
495	D. Santos
497	D. Montes Claros
520	D. Bom Jesus da Lapa
536	D. Januária
539	A. Brasília
551	D. Jundiaí
556	D. Bagé
557	D. São Luís de Cáceres
560	D. Ponta de Pedras
586	D. Carolina
600	P. Lábrea
606	P. Itacoatiara
607	A. Manaus
612	D. Cruzeiro do Sul
623	P. Itaituba
625	D. Ssma. Conceição do Araguaia
644	D. Juazeiro
656	D. Duque de Caxias
660	A. São Sebastião do Rio de Jan
664	Abadia N.S. do Monserrate
675	Abadia Claraval
679	A. São Paulo
699	D. Itapetininga
714	D. Foz do Iguaçu
719	D. Ponta Grossa
730	D. Rio Grande
736	D. Criciúma
746	D. Juína
749	D. Goiás/São Luís M. Belos
750	D. Macapá/P. Marajó
752	D. Araçatuba/São Carlos
753	D. São José dos Campos
754	D. Paracatu/Januária
755	D. Guanhães/Governador Valadares
756	D. Caratinga/Guaxupé
757	D. Uberlândia/Uberaba
758	D. Nova Friburgo/Niterói
759	D.Barra do Piraí/Volta Redonda
760	D. Caçador/Joinville
761	A. Goiânia/D. S. Luiz de Montes Belos
767	D. Corumbá
768	P. Itaituba/P. do Xingu
769	A. Belém/D. Marabá
770	D. Cristalândia
771	D. Rondonópolis/Guiratinga
772	A. Campinas/A. São Paulo
774	D. Bragança Paulista/S. Paulo
775	D. Barra do Piraí/Volta Redonda
776	D.Viana/D.Bacabal
777	D. Luz/Campanha
778	D. Piracicaba/Limeira
779	D. Grato
780	D. Divinópolis/A. Belo Horizonte
781	D. Santo André
782	D. Osório
783	D. Campo Limpo
784	D. Janaúba
785	D. Amparo
786	D. Ourinhos
787	D. Caraguatatuba
788	D. Guarulhos
789	D. Catanduva
790	A. Vitória da Conquista
792	D. Blumenau
793	A. Feira de Santana
\.


--
-- Data for Name: municipios_municipio; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.municipios_municipio (id, nome, diocese_id, regiao_imediata_id, uf_id) FROM stdin;
1100015	Alta Floresta D'Oeste	\N	110005	11
1100023	Ariquemes	24	110002	11
1100031	Cabixi	159	110006	11
1100049	Cacoal	160	110005	11
1100056	Cerejeiras	159	110006	11
1100064	Colorado do Oeste	159	110006	11
1100072	Corumbiara	159	110006	11
1100080	Costa Marques	159	110004	11
1100098	Espigão D'Oeste	\N	110005	11
1100106	Guajará-Mirim	159	110001	11
1100114	Jaru	160	110003	11
1100122	Ji-Paraná	160	110004	11
1100130	Machadinho D'Oeste	\N	110003	11
1100148	Nova Brasilândia D'Oeste	\N	110005	11
1100155	Ouro Preto do Oeste	160	110004	11
1100189	Pimenta Bueno	160	110005	11
1100205	Porto Velho	24	110001	11
1100254	Presidente Médici	160	110004	11
1100262	Rio Crespo	24	110002	11
1100288	Rolim de Moura	160	110005	11
1100296	Santa Luzia D'Oeste	\N	110005	11
1100304	Vilhena	160	110006	11
1100320	São Miguel do Guaporé	159	110004	11
1100338	Nova Mamoré	159	110001	11
1100346	Alvorada D'Oeste	\N	110004	11
1100379	Alto Alegre dos Parecis	\N	110005	11
1100403	Alto Paraíso	24	110002	11
1100452	Buritis	24	110002	11
1100502	Novo Horizonte do Oeste	\N	110005	11
1100601	Cacaulândia	24	110002	11
1100700	Campo Novo de Rondônia	24	110002	11
1100809	Candeias do Jamari	24	110001	11
1100908	Castanheiras	160	110005	11
1100924	Chupinguaia	160	110006	11
1100940	Cujubim	24	110002	11
1101005	Governador Jorge Teixeira	160	110003	11
1101104	Itapuã do Oeste	24	110001	11
1101203	Ministro Andreazza	160	110005	11
1101302	Mirante da Serra	160	110004	11
1101401	Monte Negro	24	110002	11
1101435	Nova União	160	110004	11
1101450	Parecis	160	110005	11
1101468	Pimenteiras do Oeste	\N	110006	11
1101476	Primavera de Rondônia	\N	110005	11
1101484	São Felipe D'Oeste	\N	110005	11
1101492	São Francisco do Guaporé	159	110004	11
1101500	Seringueiras	159	110004	11
1101559	Teixeirópolis	\N	110004	11
1101609	Theobroma	160	110003	11
1101708	Urupá	\N	110004	11
1101757	Vale do Anari	24	110003	11
1101807	Vale do Paraíso	160	110004	11
1200013	Acrelândia	44	120001	12
1200054	Assis Brasil	44	120002	12
1200104	Brasiléia	44	120002	12
1200138	Bujari	44	120001	12
1200179	Capixaba	44	120001	12
1200203	Cruzeiro do Sul	612	120004	12
1200252	Epitaciolândia	44	120002	12
1200302	Feijó	612	120005	12
1200328	Jordão	\N	120005	12
1200336	Mâncio Lima	612	120004	12
1200344	Manoel Urbano	44	120003	12
1200351	Marechal Thaumaturgo	612	120004	12
1200385	Plácido de Castro	44	120001	12
1200393	Porto Walter	\N	120004	12
1200401	Rio Branco	44	120001	12
1200427	Rodrigues Alves	612	120004	12
1200435	Santa Rosa do Purus	44	120003	12
1200450	Senador Guiomard	44	120001	12
1200500	Sena Madureira	44	120003	12
1200609	Tarauacá	612	120005	12
1200708	Xapuri	44	120002	12
1200807	Porto Acre	44	120001	12
1300029	Alvarães	196	130005	13
1300060	Amaturá	\N	130006	13
1300086	Anamã	236	130004	13
1300102	Anori	236	130003	13
1300144	Apuí	299	130009	13
1300201	Atalaia do Norte	217	130006	13
1300300	Autazes	400	130001	13
1300409	Barcelos	429	130002	13
1300508	Barreirinha	451	130010	13
1300607	Benjamin Constant	217	130006	13
1300631	Beruri	236	130003	13
1300680	Boa Vista do Ramos	\N	130010	13
1300706	Boca do Acre	44	130008	13
1300805	Borba	400	130001	13
1300839	Caapiranga	\N	130004	13
1300904	Canutama	600	130008	13
1301001	Carauari	196	130005	13
1301100	Careiro	607	130001	13
1301159	Careiro da Várzea	607	130001	13
1301209	Coari	236	130003	13
1301308	Codajás	236	130003	13
1301407	Eirunepé	612	130007	13
1301506	Envira	612	130007	13
1301605	Fonte Boa	196	130005	13
1301654	Guajará	\N	130007	13
1301704	Humaitá	299	130009	13
1301803	Ipixuna	612	130007	13
1301852	Iranduba	607	130001	13
1301902	Itacoatiara	606	130011	13
1301951	Itamarati	196	130007	13
1302009	Itapiranga	606	130011	13
1302108	Japurá	196	130005	13
1302207	Juruá	196	130005	13
1302306	Jutaí	196	130005	13
1302405	Lábrea	600	130008	13
1302504	Manacapuru	236	130004	13
1302553	Manaquiri	607	130001	13
1302603	Manaus	607	130001	13
1302702	Manicoré	400	130009	13
1302801	Maraã	196	130005	13
1302900	Maués	451	130010	13
1303007	Nhamundá	\N	130010	13
1303106	Nova Olinda do Norte	400	130001	13
1303205	Novo Airão	607	130004	13
1303304	Novo Aripuanã	400	130009	13
1303403	Parintins	451	130010	13
1303502	Pauini	600	130008	13
1303536	Presidente Figueiredo	606	130001	13
1303569	Rio Preto da Eva	606	130001	13
1303601	Santa Isabel do Rio Negro	429	130002	13
1303700	Santo Antônio do Içá	217	130006	13
1303809	São Gabriel da Cachoeira	429	130002	13
1303908	São Paulo de Olivença	217	130006	13
1303957	São Sebastião do Uatumã	\N	130011	13
1304005	Silves	606	130011	13
1304062	Tabatinga	217	130006	13
1304104	Tapauá	600	130008	13
1304203	Tefé	238	130005	13
1304237	Tonantins	217	130006	13
1304260	Uarini	\N	130005	13
1304302	Urucará	606	130011	13
1304401	Urucurituba	606	130011	13
1400027	Amajari	169	140002	14
1400050	Alto Alegre	169	140001	14
1400100	Boa Vista	169	140001	14
1400159	Bonfim	169	140001	14
1400175	Cantá	169	140001	14
1400209	Caracaraí	169	140004	14
1400233	Caroebe	169	140003	14
1400282	Iracema	169	140004	14
1400308	Mucajaí	169	140001	14
1400407	Normandia	169	140002	14
1400456	Pacaraima	169	140002	14
1400472	Rorainópolis	169	140003	14
1400506	São João da Baliza	169	140003	14
1400605	São Luiz	\N	140003	14
1400704	Uiramutã	169	140002	14
1500107	Abaetetuba	12	150003	15
1500131	Abel Figueiredo	18	150009	15
1500206	Acará	12	150001	15
1500305	Afuá	750	150020	15
1500347	Água Azul do Norte	18	150014	15
1500404	Alenquer	138	150015	15
1500503	Almeirim	151	150019	15
1500602	Altamira	162	150018	15
1500701	Anajás	67	150020	15
1500800	Ananindeua	237	150001	15
1500859	Anapu	162	150018	15
1500909	Augusto Corrêa	\N	150005	15
1500958	Aurora do Pará	395	150007	15
1501006	Aveiro	151	150016	15
1501105	Bagre	409	150020	15
1501204	Baião	409	150011	15
1501253	Bannach	162	150012	15
1501303	Barcarena	12	150001	15
1501402	Belém	237	150001	15
1501451	Belterra	151	150015	15
1501501	Benevides	237	150001	15
1501576	Bom Jesus do Tocantins	18	150009	15
1501600	Bonito	\N	150006	15
1501709	Bragança	395	150005	15
1501725	Brasil Novo	162	150018	15
1501758	Brejo Grande do Araguaia	18	150009	15
1501782	Breu Branco	409	150011	15
1501808	Breves	67	150020	15
1501907	Bujaru	12	150001	15
1501956	Cachoeira do Piriá	395	150005	15
1502004	Cachoeira do Arari	560	150021	15
1502103	Cametá	409	150002	15
1502152	Canaã dos Carajás	18	150010	15
1502202	Capanema	237	150006	15
1502301	Capitão Poço	395	150008	15
1502400	Castanhal	237	150004	15
1502509	Chaves	67	150020	15
1502608	Colares	\N	150001	15
1502707	Conceição do Araguaia	625	150012	15
1502756	Concórdia do Pará	12	150001	15
1502764	Cumaru do Norte	625	150012	15
1502772	Curionópolis	18	150010	15
1502806	Curralinho	560	150020	15
1502855	Curuá	\N	150017	15
1502905	Curuçá	237	150004	15
1502939	Dom Eliseu	395	150007	15
1502954	Eldorado do Carajás	\N	150010	15
1503002	Faro	138	150017	15
1503044	Floresta do Araguaia	625	150012	15
1503077	Garrafão do Norte	395	150008	15
1503093	Goianésia do Pará	18	150011	15
1503101	Gurupá	750	150020	15
1503200	Igarapé-Açu	237	150004	15
1503309	Igarapé-Miri	409	150003	15
1503408	Inhangapi	\N	150004	15
1503457	Ipixuna do Pará	395	150007	15
1503507	Irituia	395	150004	15
1503606	Itaituba	623	150016	15
1503705	Itupiranga	18	150009	15
1503754	Jacareacanga	623	150016	15
1503804	Jacundá	18	150009	15
1503903	Juruti	138	150017	15
1504000	Limoeiro do Ajuru	\N	150002	15
1504059	Mãe do Rio	395	150007	15
1504109	Magalhães Barata	237	150004	15
1504208	Marabá	18	150009	15
1504307	Maracanã	\N	150004	15
1504406	Marapanim	237	150004	15
1504422	Marituba	\N	150001	15
1504455	Medicilândia	162	150018	15
1504505	Melgaço	67	150020	15
1504604	Mocajuba	\N	150002	15
1504703	Moju	12	150003	15
1504752	Mojuí dos Campos	\N	150015	15
1504802	Monte Alegre	151	150015	15
1504901	Muaná	\N	150021	15
1504950	Nova Esperança do Piriá	\N	150008	15
1504976	Nova Ipixuna	18	150009	15
1505007	Nova Timboteua	\N	150006	15
1505031	Novo Progresso	623	150016	15
1505064	Novo Repartimento	409	150011	15
1505106	Óbidos	138	150017	15
1505205	Oeiras do Pará	409	150002	15
1505304	Oriximiná	138	150017	15
1505403	Ourém	\N	150008	15
1505437	Ourilândia do Norte	162	150013	15
1505486	Pacajá	67	150011	15
1505494	Palestina do Pará	18	150009	15
1505502	Paragominas	395	150007	15
1505536	Parauapebas	18	150010	15
1505551	Pau D'Arco	\N	150012	15
1505601	Peixe-Boi	\N	150006	15
1505635	Piçarra	625	150009	15
1505650	Placas	151	150016	15
1505700	Ponta de Pedras	560	150021	15
1505809	Portel	67	150020	15
1505908	Porto de Moz	162	150019	15
1506005	Prainha	151	150015	15
1506104	Primavera	\N	150006	15
1506112	Quatipuru	\N	150006	15
1506138	Redenção	625	150012	15
1506161	Rio Maria	625	150014	15
1506187	Rondon do Pará	395	150009	15
1506195	Rurópolis	623	150016	15
1506203	Salinópolis	\N	150006	15
1506302	Salvaterra	67	150021	15
1506351	Santa Bárbara do Pará	237	150001	15
1506401	Santa Cruz do Arari	560	150021	15
1506500	Santa Izabel do Pará	\N	150001	15
1506559	Santa Luzia do Pará	395	150005	15
1506583	Santa Maria das Barreiras	625	150012	15
1506609	Santa Maria do Pará	237	150004	15
1506708	Santana do Araguaia	625	150012	15
1506807	Santarém	151	150015	15
1506906	Santarém Novo	237	150006	15
1507003	Santo Antônio do Tauá	\N	150001	15
1507102	São Caetano de Odivelas	237	150001	15
1507151	São Domingos do Araguaia	18	150009	15
1507201	São Domingos do Capim	395	150004	15
1507300	São Félix do Xingu	162	150013	15
1507409	São Francisco do Pará	237	150004	15
1507458	São Geraldo do Araguaia	625	150009	15
1507466	São João da Ponta	237	150004	15
1507474	São João de Pirabas	\N	150006	15
1507508	São João do Araguaia	18	150009	15
1507607	São Miguel do Guamá	395	150004	15
1507706	São Sebastião da Boa Vista	\N	150020	15
1507755	Sapucaia	625	150014	15
1507805	Senador José Porfírio	162	150018	15
1507904	Soure	67	150021	15
1507953	Tailândia	12	150003	15
1507961	Terra Alta	237	150004	15
1507979	Terra Santa	138	150017	15
1508001	Tomé-Açu	395	150001	15
1508035	Tracuateua	\N	150005	15
1508050	Trairão	11	150016	15
1508084	Tucumã	162	150013	15
1508100	Tucuruí	18	150011	15
1508126	Ulianópolis	395	150007	15
1508159	Uruará	162	150018	15
1508209	Vigia	237	150001	15
1508308	Viseu	395	150005	15
1508357	Vitória do Xingu	162	150018	15
1508407	Xinguara	625	150014	15
1600055	Serra do Navio	68	160004	16
1600105	Amapá	68	160003	16
1600154	Pedra Branca do Amapari	68	160004	16
1600204	Calçoene	68	160003	16
1600212	Cutias	\N	160003	16
1600238	Ferreira Gomes	68	160004	16
1600253	Itaubal	\N	160001	16
1600279	Laranjal do Jari	68	160002	16
1600303	Macapá	68	160001	16
1600402	Mazagão	68	160001	16
1600501	Oiapoque	68	160003	16
1600535	Porto Grande	68	160004	16
1600550	Pracuúba	68	160003	16
1600600	Santana	68	160001	16
1600709	Tartarugalzinho	68	160003	16
1600808	Vitória do Jari	68	160002	16
1700251	Abreulândia	23	170003	17
1700301	Aguiarnópolis	104	170008	17
1700350	Aliança do Tocantins	202	170010	17
1700400	Almas	202	170011	17
1700707	Alvorada	202	170010	17
1701002	Ananás	104	170005	17
1701051	Angico	104	170005	17
1701101	Aparecida do Rio Negro	\N	170001	17
1701309	Aragominas	104	170005	17
1701903	Araguacema	23	170003	17
1702000	Araguaçu	442	170010	17
1702109	Araguaína	104	170005	17
1702158	Araguanã	104	170005	17
1702208	Araguatins	104	170009	17
1702307	Arapoema	104	170005	17
1702406	Arraias	202	170011	17
1702554	Augustinópolis	104	170009	17
1702703	Aurora do Tocantins	202	170011	17
1702901	Axixá do Tocantins	104	170009	17
1703008	Babaçulândia	104	170005	17
1703057	Bandeirantes do Tocantins	104	170007	17
1703073	Barra do Ouro	442	170005	17
1703107	Barrolândia	442	170003	17
1703206	Bernardo Sayão	442	170007	17
1703305	Bom Jesus do Tocantins	\N	170006	17
1703602	Brasilândia do Tocantins	\N	170007	17
1703701	Brejinho de Nazaré	202	170002	17
1703800	Buriti do Tocantins	\N	170009	17
1703826	Cachoeirinha	104	170008	17
1703842	Campos Lindos	442	170005	17
1703867	Cariri do Tocantins	202	170010	17
1703883	Carmolândia	104	170005	17
1703891	Carrasco Bonito	104	170009	17
1703909	Caseara	770	170003	17
1704105	Centenário	\N	170006	17
1704600	Chapada de Areia	770	170003	17
1705102	Chapada da Natividade	202	170002	17
1705508	Colinas do Tocantins	442	170007	17
1705557	Combinado	202	170011	17
1705607	Conceição do Tocantins	202	170011	17
1706001	Couto Magalhães	\N	170006	17
1706100	Cristalândia	770	170003	17
1706258	Crixás do Tocantins	202	170010	17
1706506	Darcinópolis	104	170005	17
1707009	Dianópolis	202	170011	17
1707108	Divinópolis do Tocantins	442	170003	17
1707207	Dois Irmãos do Tocantins	770	170004	17
1707306	Dueré	202	170010	17
1707405	Esperantina	104	170009	17
1707553	Fátima	202	170002	17
1707652	Figueirópolis	\N	170010	17
1707702	Filadélfia	104	170005	17
1708205	Formoso do Araguaia	23	170010	17
1708254	Fortaleza do Tabocão	442	170006	17
1708304	Goianorte	23	170006	17
1709005	Goiatins	442	170005	17
1709302	Guaraí	442	170006	17
1709500	Gurupi	104	170010	17
1709807	Ipueiras	202	170002	17
1710508	Itacajá	442	170007	17
1710706	Itaguatins	104	170009	17
1710904	Itapiratins	104	170007	17
1711100	Itaporã do Tocantins	442	170006	17
1711506	Jaú do Tocantins	202	170010	17
1711803	Juarina	104	170007	17
1711902	Lagoa da Confusão	770	170003	17
1711951	Lagoa do Tocantins	\N	170001	17
1712009	Lajeado	\N	170001	17
1712157	Lavandeira	\N	170011	17
1712405	Lizarda	290	170001	17
1712454	Luzinópolis	104	170008	17
1712504	Marianópolis do Tocantins	442	170003	17
1712702	Mateiros	290	170001	17
1712801	Maurilândia do Tocantins	104	170008	17
1713205	Miracema do Tocantins	442	170004	17
1713304	Miranorte	442	170004	17
1713601	Monte do Carmo	202	170002	17
1713700	Monte Santo do Tocantins	\N	170003	17
1713809	Palmeiras do Tocantins	\N	170008	17
1713957	Muricilândia	104	170005	17
1714203	Natividade	202	170002	17
1714302	Nazaré	104	170008	17
1714880	Nova Olinda	290	170005	17
1715002	Nova Rosalândia	\N	170003	17
1715101	Novo Acordo	290	170001	17
1715150	Novo Alegre	\N	170011	17
1715259	Novo Jardim	202	170011	17
1715507	Oliveira de Fátima	148	170002	17
1715705	Palmeirante	104	170007	17
1715754	Palmeirópolis	\N	170010	17
1716109	Paraíso do Tocantins	\N	170003	17
1716208	Paranã	202	170010	17
1716307	Pau D'Arco	\N	170005	17
1716505	Pedro Afonso	442	170006	17
1716604	Peixe	202	170010	17
1716653	Pequizeiro	442	170006	17
1716703	Colméia	442	170006	17
1717008	Pindorama do Tocantins	202	170002	17
1717206	Piraquê	104	170005	17
1717503	Pium	770	170003	17
1717800	Ponte Alta do Bom Jesus	202	170011	17
1717909	Ponte Alta do Tocantins	202	170002	17
1718006	Porto Alegre do Tocantins	202	170011	17
1718204	Porto Nacional	202	170002	17
1718303	Praia Norte	104	170009	17
1718402	Presidente Kennedy	442	170006	17
1718451	Pugmil	770	170003	17
1718501	Recursolândia	442	170006	17
1718550	Riachinho	104	170005	17
1718659	Rio da Conceição	\N	170011	17
1718709	Rio dos Bois	\N	170004	17
1718758	Rio Sono	290	170001	17
1718808	Sampaio	104	170009	17
1718840	Sandolândia	23	170010	17
1718865	Santa Fé do Araguaia	104	170005	17
1718881	Santa Maria do Tocantins	442	170006	17
1718899	Santa Rita do Tocantins	202	170002	17
1718907	Santa Rosa do Tocantins	202	170002	17
1719004	Santa Tereza do Tocantins	290	170001	17
1720002	Santa Terezinha do Tocantins	\N	170008	17
1720101	São Bento do Tocantins	104	170009	17
1720150	São Félix do Tocantins	290	170001	17
1720200	São Miguel do Tocantins	104	170009	17
1720259	São Salvador do Tocantins	202	170010	17
1720309	São Sebastião do Tocantins	104	170009	17
1720499	São Valério	202	170010	17
1720655	Silvanópolis	202	170002	17
1720804	Sítio Novo do Tocantins	104	170009	17
1720853	Sucupira	202	170010	17
1720903	Taguatinga	202	170011	17
1720937	Taipas do Tocantins	202	170011	17
1720978	Talismã	202	170010	17
1721000	Palmas	202	170001	17
1721109	Tocantínia	442	170004	17
1721208	Tocantinópolis	104	170008	17
1721257	Tupirama	\N	170006	17
1721307	Tupiratins	442	170007	17
1722081	Wanderlândia	104	170005	17
1722107	Xambioá	104	170005	17
2100055	Açailândia	30	210021	21
2100105	Afonso Cunha	65	210013	21
2100154	Água Doce do Maranhão	65	210007	21
2100204	Alcântara	118	210001	21
2100303	Aldeias Altas	130	210013	21
2100402	Altamira do Maranhão	163	210010	21
2100436	Alto Alegre do Maranhão	172	210010	21
2100477	Alto Alegre do Pindaré	173	210009	21
2100501	Alto Parnaíba	189	210022	21
2100550	Amapá do Maranhão	209	210011	21
2100600	Amarante do Maranhão	30	210019	21
2100709	Anajatuba	172	210004	21
2100808	Anapurus	65	210003	21
2100832	Apicum-Açu	\N	210008	21
2100873	Araguanã	209	210009	21
2100907	Araioses	65	210007	21
2100956	Arame	322	210020	21
2101004	Arari	172	210005	21
2101103	Axixá	\N	210001	21
2101202	Bacabal	163	210010	21
2101251	Bacabeira	403	210001	21
2101301	Bacuri	118	210008	21
2101350	Bacurituba	173	210002	21
2101400	Balsas	189	210022	21
2101509	Barão de Grajaú	\N	210017	21
2101608	Barra do Corda	322	210020	21
2101707	Barreirinhas	65	210006	21
2101731	Belágua	65	210003	21
2101772	Bela Vista do Maranhão	173	210009	21
2101806	Benedito Leite	189	210017	21
2101905	Bequimão	118	210002	21
2101939	Bernardo do Mearim	\N	210012	21
2101970	Boa Vista do Gurupi	209	210011	21
2102002	Bom Jardim	209	210009	21
2102036	Bom Jesus das Selvas	173	210021	21
2102077	Bom Lugar	\N	210010	21
2102101	Brejo	65	210003	21
2102150	Brejo de Areia	163	210010	21
2102200	Buriti	65	210003	21
2102309	Buriti Bravo	\N	210014	21
2102325	Buriticupu	173	210021	21
2102358	Buritirana	30	210019	21
2102374	Cachoeira Grande	403	210001	21
2102408	Cajapió	173	210005	21
2102507	Cajari	173	210005	21
2102556	Campestre do Maranhão	586	210019	21
2102606	Cândido Mendes	209	210011	21
2102705	Cantanhede	172	210004	21
2102754	Capinzal do Norte	163	210012	21
2102804	Carolina	586	210022	21
2102903	Carutapera	209	210011	21
2103000	Caxias	130	210013	21
2103109	Cedral	118	210008	21
2103125	Central do Maranhão	118	210008	21
2103158	Centro do Guilherme	209	210011	21
2103174	Centro Novo do Maranhão	209	210011	21
2103208	Chapadinha	65	210003	21
2103257	Cidelândia	30	210019	21
2103307	Codó	163	210015	21
2103406	Coelho Neto	65	210013	21
2103505	Colinas	130	210018	21
2103554	Conceição do Lago-Açu	\N	210010	21
2103604	Coroatá	172	210015	21
2103703	Cururupu	118	210008	21
2103752	Davinópolis	30	210019	21
2103802	Dom Pedro	322	210016	21
2103901	Duque Bacelar	65	210013	21
2104008	Esperantinópolis	163	210012	21
2104057	Estreito	586	210019	21
2104073	Feira Nova do Maranhão	\N	210022	21
2104081	Fernando Falcão	322	210020	21
2104099	Formosa da Serra Negra	322	210020	21
2104107	Fortaleza dos Nogueiras	\N	210022	21
2104206	Fortuna	130	210016	21
2104305	Godofredo Viana	209	210011	21
2104404	Gonçalves Dias	130	210016	21
2104503	Governador Archer	322	210016	21
2104552	Governador Edison Lobão	30	210019	21
2104602	Governador Eugênio Barros	\N	210016	21
2104628	Governador Luiz Rocha	\N	210016	21
2104651	Governador Newton Bello	209	210009	21
2104677	Governador Nunes Freire	209	210011	21
2104701	Graça Aranha	\N	210016	21
2104800	Grajaú	322	210020	21
2104909	Guimarães	118	210008	21
2105005	Humberto de Campos	403	210006	21
2105104	Icatu	403	210001	21
2105153	Igarapé do Meio	173	210009	21
2105203	Igarapé Grande	\N	210012	21
2105302	Imperatriz	30	210019	21
2105351	Itaipava do Grajaú	322	210020	21
2105401	Itapecuru Mirim	172	210004	21
2105427	Itinga do Maranhão	30	210021	21
2105450	Jatobá	\N	210018	21
2105476	Jenipapo dos Vieiras	\N	210020	21
2105500	João Lisboa	30	210019	21
2105609	Joselândia	\N	210012	21
2105658	Junco do Maranhão	209	210011	21
2105708	Lago da Pedra	163	210010	21
2105807	Lago do Junco	163	210012	21
2105906	Lago Verde	163	210010	21
2105922	Lagoa do Mato	\N	210017	21
2105948	Lago dos Rodrigues	\N	210012	21
2105963	Lagoa Grande do Maranhão	\N	210010	21
2105989	Lajeado Novo	586	210019	21
2106003	Lima Campos	\N	210012	21
2106102	Loreto	189	210022	21
2106201	Luís Domingues	209	210011	21
2106300	Magalhães de Almeida	65	210007	21
2106326	Maracaçumé	209	210011	21
2106359	Marajá do Sena	\N	210010	21
2106375	Maranhãozinho	209	210011	21
2106409	Mata Roma	65	210003	21
2106508	Matinha	173	210005	21
2106607	Matões	130	210014	21
2106631	Matões do Norte	172	210004	21
2106672	Milagres do Maranhão	\N	210003	21
2106706	Mirador	189	210018	21
2106755	Miranda do Norte	172	210004	21
2106805	Mirinzal	118	210008	21
2106904	Monção	173	210009	21
2107001	Montes Altos	586	210019	21
2107100	Morros	403	210001	21
2107209	Nina Rodrigues	172	210004	21
2107258	Nova Colinas	189	210022	21
2107308	Nova Iorque	189	210017	21
2107357	Nova Olinda do Maranhão	209	210009	21
2107407	Olho d'Água das Cunhãs	\N	210010	21
2107456	Olinda Nova do Maranhão	173	210005	21
2107506	Paço do Lumiar	403	210001	21
2107605	Palmeirândia	118	210002	21
2107704	Paraibano	\N	210017	21
2107803	Parnarama	130	210014	21
2107902	Passagem Franca	\N	210017	21
2108009	Pastos Bons	189	210017	21
2108058	Paulino Neves	65	210007	21
2108108	Paulo Ramos	163	210010	21
2108207	Pedreiras	163	210012	21
2108256	Pedro do Rosário	173	210002	21
2108306	Penalva	173	210005	21
2108405	Peri Mirim	\N	210002	21
2108454	Peritoró	172	210015	21
2108504	Pindaré-Mirim	\N	210009	21
2108603	Pinheiro	118	210002	21
2108702	Pio XII	163	210009	21
2108801	Pirapemas	172	210004	21
2108900	Poção de Pedras	163	210012	21
2109007	Porto Franco	189	210019	21
2109056	Porto Rico do Maranhão	\N	210008	21
2109106	Presidente Dutra	322	210016	21
2109205	Presidente Juscelino	403	210001	21
2109239	Presidente Médici	\N	210011	21
2109270	Presidente Sarney	118	210002	21
2109304	Presidente Vargas	172	210004	21
2109403	Primeira Cruz	403	210006	21
2109452	Raposa	403	210001	21
2109502	Riachão	189	210022	21
2109551	Ribamar Fiquene	586	210019	21
2109601	Rosário	403	210001	21
2109700	Sambaíba	189	210022	21
2109759	Santa Filomena do Maranhão	\N	210016	21
2109809	Santa Helena	118	210002	21
2109908	Santa Inês	173	210009	21
2110005	Santa Luzia	173	210009	21
2110039	Santa Luzia do Paruá	209	210011	21
2110104	Santa Quitéria do Maranhão	65	210003	21
2110203	Santa Rita	403	210001	21
2110237	Santana do Maranhão	\N	210007	21
2110278	Santo Amaro do Maranhão	403	210006	21
2110302	Santo Antônio dos Lopes	163	210012	21
2110401	São Benedito do Rio Preto	65	210003	21
2110500	São Bento	118	210002	21
2110609	São Bernardo	65	210007	21
2110658	São Domingos do Azeitão	\N	210017	21
2110708	São Domingos do Maranhão	\N	210016	21
2110807	São Félix de Balsas	189	210022	21
2110856	São Francisco do Brejão	30	210021	21
2110906	São Francisco do Maranhão	\N	210017	21
2111003	São João Batista	173	210005	21
2111029	São João do Carú	209	210009	21
2111052	São João do Paraíso	586	210019	21
2111078	São João do Soter	130	210013	21
2111102	São João dos Patos	\N	210017	21
2111201	São José de Ribamar	403	210001	21
2111250	São José dos Basílios	322	210016	21
2111300	São Luís	403	210001	21
2111409	São Luís Gonzaga do Maranhão	163	210010	21
2111508	São Mateus do Maranhão	172	210010	21
2111532	São Pedro da Água Branca	30	210019	21
2111573	São Pedro dos Crentes	\N	210020	21
2111607	São Raimundo das Mangabeiras	189	210022	21
2111631	São Raimundo do Doca Bezerra	163	210012	21
2111672	São Roberto	\N	210012	21
2111706	São Vicente Ferrer	173	210005	21
2111722	Satubinha	163	210010	21
2111748	Senador Alexandre Costa	\N	210016	21
2111763	Senador La Rocque	30	210019	21
2111789	Serrano do Maranhão	118	210008	21
2111805	Sítio Novo	322	210020	21
2111904	Sucupira do Norte	189	210018	21
2111953	Sucupira do Riachão	\N	210017	21
2112001	Tasso Fragoso	189	210022	21
2112100	Timbiras	172	210015	21
2112209	Timon	130	210014	21
2112233	Trizidela do Vale	\N	210012	21
2112274	Tufilândia	173	210009	21
2112308	Tuntum	322	210016	21
2112407	Turiaçu	118	210002	21
2112456	Turilândia	118	210002	21
2112506	Tutóia	65	210007	21
2112605	Urbano Santos	65	210003	21
2112704	Vargem Grande	172	210004	21
2112803	Viana	173	210005	21
2112852	Vila Nova dos Martírios	30	210019	21
2112902	Vitória do Mearim	173	210005	21
2113009	Vitorino Freire	163	210010	21
2114007	Zé Doca	209	210009	21
2200053	Acauã	40	220010	22
2200103	Agricolândia	70	220002	22
2200202	Água Branca	70	220002	22
2200251	Alagoinha do Piauí	40	220009	22
2200277	Alegrete do Piauí	40	220009	22
2200301	Alto Longá	\N	220001	22
2200400	Altos	183	220001	22
2200459	Alvorada do Gurguéia	204	220016	22
2200509	Amarante	70	220002	22
2200608	Angical do Piauí	70	220002	22
2200707	Anísio de Abreu	267	220013	22
2200806	Antônio Almeida	275	220018	22
2200905	Aroazes	70	220004	22
2200954	Aroeiras do Itaim	\N	220009	22
2201002	Arraial	\N	220017	22
2201051	Assunção do Piauí	183	220003	22
2201101	Avelino Lopes	\N	220015	22
2201150	Baixa Grande do Ribeiro	275	220018	22
2201176	Barra D'Alcântara	\N	220004	22
2201200	Barras	183	220005	22
2201309	Barreiras do Piauí	204	220015	22
2201408	Barro Duro	\N	220002	22
2201507	Batalha	461	220008	22
2201556	Bela Vista do Piauí	\N	220012	22
2201572	Belém do Piauí	40	220009	22
2201606	Beneditinos	183	220001	22
2201705	Bertolínia	275	220018	22
2201739	Betânia do Piauí	40	220010	22
2201770	Boa Hora	\N	220005	22
2201804	Bocaina	40	220009	22
2201903	Bom Jesus	204	220016	22
2201919	Bom Princípio do Piauí	461	220006	22
2201929	Bonfim do Piauí	\N	220013	22
2201945	Boqueirão do Piauí	\N	220003	22
2201960	Brasileira	\N	220007	22
2201988	Brejo do Piauí	267	220019	22
2202000	Buriti dos Lopes	461	220006	22
2202026	Buriti dos Montes	183	220003	22
2202059	Cabeceiras do Piauí	\N	220005	22
2202075	Cajazeiras do Piauí	275	220011	22
2202083	Cajueiro da Praia	\N	220006	22
2202091	Caldeirão Grande do Piauí	40	220009	22
2202109	Campinas do Piauí	\N	220012	22
2202117	Campo Alegre do Fidalgo	267	220014	22
2202133	Campo Grande do Piauí	40	220009	22
2202174	Campo Largo do Piauí	183	220005	22
2202208	Campo Maior	183	220003	22
2202251	Canavieira	275	220017	22
2202307	Canto do Buriti	267	220019	22
2202406	Capitão de Campos	183	220007	22
2202455	Capitão Gervásio Oliveira	267	220014	22
2202505	Caracol	267	220013	22
2202539	Caraúbas do Piauí	461	220006	22
2202554	Caridade do Piauí	40	220010	22
2202604	Castelo do Piauí	183	220003	22
2202653	Caxingó	461	220006	22
2202703	Cocal	461	220006	22
2202711	Cocal de Telha	\N	220003	22
2202729	Cocal dos Alves	\N	220006	22
2202737	Coivaras	183	220001	22
2202752	Colônia do Gurguéia	\N	220016	22
2202778	Colônia do Piauí	275	220011	22
2202802	Conceição do Canindé	275	220012	22
2202851	Coronel José Dias	267	220013	22
2202901	Corrente	204	220015	22
2203008	Cristalândia do Piauí	\N	220015	22
2203107	Cristino Castro	204	220016	22
2203206	Curimatá	204	220015	22
2203230	Currais	461	220016	22
2203255	Curralinhos	70	220001	22
2203271	Curral Novo do Piauí	40	220010	22
2203305	Demerval Lobão	70	220001	22
2203354	Dirceu Arcoverde	267	220013	22
2203404	Dom Expedito Lopes	40	220009	22
2203420	Domingos Mourão	\N	220007	22
2203453	Dom Inocêncio	267	220013	22
2203503	Elesbão Veloso	70	220002	22
2203602	Eliseu Martins	\N	220019	22
2203701	Esperantina	461	220008	22
2203750	Fartura do Piauí	267	220013	22
2203800	Flores do Piauí	275	220019	22
2203859	Floresta do Piauí	\N	220011	22
2203909	Floriano	275	220017	22
2204006	Francinópolis	\N	220002	22
2204105	Francisco Ayres	\N	220017	22
2204154	Francisco Macedo	40	220009	22
2204204	Francisco Santos	40	220009	22
2204303	Fronteiras	40	220009	22
2204352	Geminiano	\N	220009	22
2204402	Gilbués	204	220015	22
2204501	Guadalupe	275	220017	22
2204550	Guaribas	267	220013	22
2204600	Hugo Napoleão	70	220002	22
2204659	Ilha Grande	461	220006	22
2204709	Inhuma	\N	220004	22
2204808	Ipiranga do Piauí	\N	220004	22
2204907	Isaías Coelho	\N	220009	22
2205003	Itainópolis	40	220009	22
2205102	Itaueira	275	220017	22
2205151	Jacobina do Piauí	40	220010	22
2205201	Jaicós	40	220009	22
2205250	Jardim do Mulato	70	220002	22
2205276	Jatobá do Piauí	\N	220003	22
2205300	Jerumenha	\N	220017	22
2205359	João Costa	267	220014	22
2205409	Joaquim Pires	461	220008	22
2205458	Joca Marques	\N	220008	22
2205508	José de Freitas	183	220001	22
2205516	Juazeiro do Piauí	\N	220003	22
2205524	Júlio Borges	\N	220015	22
2205532	Jurema	267	220013	22
2205540	Lagoinha do Piauí	\N	220002	22
2205557	Lagoa Alegre	\N	220001	22
2205565	Lagoa do Barro do Piauí	267	220014	22
2205573	Lagoa de São Francisco	\N	220007	22
2205581	Lagoa do Piauí	\N	220001	22
2205599	Lagoa do Sítio	70	220004	22
2205607	Landri Sales	275	220017	22
2205706	Luís Correia	461	220006	22
2205805	Luzilândia	461	220008	22
2205854	Madeiro	461	220008	22
2205904	Manoel Emídio	275	220018	22
2205953	Marcolândia	40	220009	22
2206001	Marcos Parente	275	220017	22
2206050	Massapê do Piauí	40	220009	22
2206100	Matias Olímpio	461	220008	22
2206209	Miguel Alves	70	220001	22
2206308	Miguel Leão	70	220002	22
2206357	Milton Brandão	461	220007	22
2206407	Monsenhor Gil	70	220001	22
2206506	Monsenhor Hipólito	\N	220009	22
2206605	Monte Alegre do Piauí	204	220015	22
2206654	Morro Cabeça no Tempo	204	220015	22
2206670	Morro do Chapéu do Piauí	461	220008	22
2206696	Murici dos Portelas	461	220006	22
2206704	Nazaré do Piauí	275	220017	22
2206720	Nazária	70	220001	22
2206753	Nossa Senhora de Nazaré	\N	220003	22
2206803	Nossa Senhora dos Remédios	183	220005	22
2206902	Novo Oriente do Piauí	\N	220004	22
2206951	Novo Santo Antônio	\N	220001	22
2207009	Oeiras	275	220011	22
2207108	Olho D'Água do Piauí	\N	220002	22
2207207	Padre Marcos	40	220009	22
2207306	Paes Landim	\N	220012	22
2207355	Pajeú do Piauí	\N	220019	22
2207405	Palmeira do Piauí	204	220016	22
2207504	Palmeirais	70	220002	22
2207553	Paquetá	40	220009	22
2207603	Parnaguá	204	220015	22
2207702	Parnaíba	461	220006	22
2207751	Passagem Franca do Piauí	\N	220002	22
2207777	Patos do Piauí	40	220010	22
2207793	Pau D'Arco do Piauí	\N	220001	22
2207801	Paulistana	40	220010	22
2207850	Pavussu	\N	220019	22
2207900	Pedro II	\N	220007	22
2207934	Pedro Laurentino	\N	220014	22
2207959	Nova Santa Rita	267	220014	22
2208007	Picos	40	220009	22
2208106	Pimenteiras	70	220004	22
2208205	Pio IX	40	220009	22
2208304	Piracuruca	461	220007	22
2208403	Piripiri	461	220007	22
2208502	Porto	183	220005	22
2208551	Porto Alegre do Piauí	275	220017	22
2208601	Prata do Piauí	\N	220002	22
2208650	Queimada Nova	40	220010	22
2208700	Redenção do Gurguéia	204	220016	22
2208809	Regeneração	\N	220002	22
2208858	Riacho Frio	204	220015	22
2208874	Ribeira do Piauí	\N	220014	22
2208908	Ribeiro Gonçalves	204	220018	22
2209005	Rio Grande do Piauí	275	220019	22
2209104	Santa Cruz do Piauí	40	220009	22
2209153	Santa Cruz dos Milagres	\N	220004	22
2209203	Santa Filomena	204	220015	22
2209302	Santa Luz	204	220016	22
2209351	Santana do Piauí	\N	220009	22
2209377	Santa Rosa do Piauí	\N	220011	22
2209401	Santo Antônio de Lisboa	\N	220009	22
2209450	Santo Antônio dos Milagres	\N	220002	22
2209500	Santo Inácio do Piauí	\N	220011	22
2209559	São Braz do Piauí	267	220013	22
2209609	São Félix do Piauí	70	220002	22
2209658	São Francisco de Assis do Piauí	275	220012	22
2209708	São Francisco do Piauí	\N	220017	22
2209757	São Gonçalo do Gurguéia	\N	220015	22
2209807	São Gonçalo do Piauí	70	220002	22
2209856	São João da Canabrava	40	220009	22
2209872	São João da Fronteira	\N	220007	22
2209906	São João da Serra	\N	220003	22
2209955	São João da Varjota	\N	220011	22
2209971	São João do Arraial	461	220008	22
2210003	São João do Piauí	267	220014	22
2210052	São José do Divino	461	220007	22
2210102	São José do Peixe	\N	220017	22
2210201	São José do Piauí	\N	220009	22
2210300	São Julião	\N	220009	22
2210359	São Lourenço do Piauí	267	220013	22
2210375	São Luis do Piauí	\N	220009	22
2210383	São Miguel da Baixa Grande	70	220002	22
2210391	São Miguel do Fidalgo	\N	220011	22
2210409	São Miguel do Tapuio	183	220003	22
2210508	São Pedro do Piauí	\N	220002	22
2210607	São Raimundo Nonato	267	220013	22
2210623	Sebastião Barros	\N	220015	22
2210631	Sebastião Leal	275	220018	22
2210656	Sigefredo Pacheco	183	220003	22
2210706	Simões	40	220010	22
2210805	Simplício Mendes	275	220012	22
2210904	Socorro do Piauí	\N	220012	22
2210938	Sussuapara	\N	220009	22
2210953	Tamboril do Piauí	\N	220019	22
2210979	Tanque do Piauí	275	220011	22
2211001	Teresina	70	220001	22
2211100	União	70	220001	22
2211209	Uruçuí	275	220018	22
2211308	Valença do Piauí	70	220004	22
2211357	Várzea Branca	267	220013	22
2211407	Várzea Grande	\N	220002	22
2211506	Vera Mendes	40	220009	22
2211605	Vila Nova do Piauí	40	220009	22
2211704	Wall Ferraz	\N	220009	22
2300101	Abaiara	13	230012	23
2300150	Acarape	37	230003	23
2300200	Acaraú	38	230017	23
2300309	Acopiara	42	230009	23
2300408	Aiuaba	42	230011	23
2300507	Alcântaras	38	230015	23
2300606	Altaneira	13	230011	23
2300705	Alto Santo	192	230007	23
2300754	Amontada	154	230002	23
2300804	Antonina do Norte	13	230011	23
2300903	Apuiarés	\N	230005	23
2301000	Aquiraz	37	230001	23
2301109	Aracati	37	230008	23
2301208	Aracoiaba	37	230003	23
2301257	Ararendá	336	230013	23
2301307	Araripe	13	230011	23
2301406	Aratuba	\N	230003	23
2301505	Arneiroz	\N	230014	23
2301604	Assaré	13	230011	23
2301703	Aurora	13	230011	23
2301802	Baixio	\N	230010	23
2301851	Banabuiú	\N	230006	23
2301901	Barbalha	779	230011	23
2301950	Barreira	\N	230003	23
2302008	Barro	\N	230012	23
2302057	Barroquinha	326	230018	23
2302107	Baturité	37	230003	23
2302206	Beberibe	37	230001	23
2302305	Bela Cruz	38	230017	23
2302404	Boa Viagem	417	230004	23
2302503	Brejo Santo	13	230012	23
2302602	Camocim	326	230018	23
2302701	Campos Sales	13	230011	23
2302800	Canindé	37	230004	23
2302909	Capistrano	\N	230003	23
2303006	Caridade	37	230004	23
2303105	Cariré	\N	230015	23
2303204	Caririaçu	13	230011	23
2303303	Cariús	42	230009	23
2303402	Carnaubal	326	230016	23
2303501	Cascavel	37	230001	23
2303600	Catarina	\N	230009	23
2303659	Catunda	\N	230015	23
2303709	Caucaia	37	230001	23
2303808	Cedro	42	230009	23
2303907	Chaval	\N	230018	23
2303931	Choró	417	230006	23
2303956	Chorozinho	37	230001	23
2304004	Coreaú	\N	230015	23
2304103	Crateús	336	230013	23
2304202	Crato	13	230011	23
2304236	Croatá	326	230016	23
2304251	Cruz	38	230017	23
2304269	Deputado Irapuan Pinheiro	\N	230006	23
2304277	Ererê	\N	230007	23
2304285	Eusébio	\N	230001	23
2304301	Farias Brito	13	230011	23
2304350	Forquilha	38	230015	23
2304400	Fortaleza	37	230001	23
2304459	Fortim	\N	230008	23
2304509	Frecheirinha	\N	230015	23
2304608	General Sampaio	\N	230005	23
2304657	Graça	326	230015	23
2304707	Granja	326	230018	23
2304806	Granjeiro	13	230011	23
2304905	Groaíras	38	230015	23
2304954	Guaiúba	37	230001	23
2305001	Guaraciaba do Norte	326	230016	23
2305100	Guaramiranga	\N	230003	23
2305209	Hidrolândia	38	230015	23
2305233	Horizonte	37	230001	23
2305266	Ibaretama	417	230006	23
2305308	Ibiapina	\N	230016	23
2305332	Ibicuitinga	\N	230006	23
2305357	Icapuí	192	230008	23
2305407	Icó	42	230010	23
2305506	Iguatu	42	230009	23
2305605	Independência	336	230013	23
2305654	Ipaporanga	336	230013	23
2305704	Ipaumirim	13	230010	23
2305803	Ipu	\N	230016	23
2305902	Ipueiras	\N	230016	23
2306009	Iracema	192	230007	23
2306108	Irauçuba	154	230005	23
2306207	Itaiçaba	192	230008	23
2306256	Itaitinga	\N	230001	23
2306306	Itapagé	154	230005	23
2306405	Itapipoca	154	230002	23
2306504	Itapiúna	417	230003	23
2306553	Itarema	154	230017	23
2306603	Itatira	417	230004	23
2306702	Jaguaretama	192	230007	23
2306801	Jaguaribara	192	230007	23
2306900	Jaguaribe	192	230007	23
2307007	Jaguaruana	192	230008	23
2307106	Jardim	\N	230011	23
2307205	Jati	13	230012	23
2307254	Jijoca de Jericoacoara	\N	230017	23
2307304	Juazeiro do Norte	13	230011	23
2307403	Jucás	\N	230009	23
2307502	Lavras da Mangabeira	13	230011	23
2307601	Limoeiro do Norte	192	230007	23
2307635	Madalena	417	230004	23
2307650	Maracanaú	37	230001	23
2307700	Maranguape	\N	230001	23
2307809	Marco	\N	230017	23
2307908	Martinópole	\N	230015	23
2308005	Massapê	38	230015	23
2308104	Mauriti	13	230012	23
2308203	Meruoca	38	230015	23
2308302	Milagres	13	230012	23
2308351	Milhã	\N	230006	23
2308377	Miraíma	\N	230002	23
2308401	Missão Velha	13	230011	23
2308500	Mombaça	\N	230009	23
2308609	Monsenhor Tabosa	336	230013	23
2308708	Morada Nova	37	230007	23
2308807	Moraújo	\N	230015	23
2308906	Morrinhos	\N	230015	23
2309003	Mucambo	\N	230015	23
2309102	Mulungu	37	230003	23
2309201	Nova Olinda	13	230011	23
2309300	Nova Russas	336	230013	23
2309409	Novo Oriente	336	230013	23
2309458	Ocara	37	230003	23
2309508	Orós	42	230010	23
2309607	Pacajus	37	230001	23
2309706	Pacatuba	37	230001	23
2309805	Pacoti	\N	230003	23
2309904	Pacujá	\N	230015	23
2310001	Palhano	192	230007	23
2310100	Palmácia	\N	230001	23
2310209	Paracuru	154	230001	23
2310258	Paraipaba	154	230001	23
2310308	Parambu	336	230014	23
2310407	Paramoti	37	230004	23
2310506	Pedra Branca	42	230006	23
2310605	Penaforte	\N	230012	23
2310704	Pentecoste	\N	230005	23
2310803	Pereiro	\N	230007	23
2310852	Pindoretama	\N	230001	23
2310902	Piquet Carneiro	42	230009	23
2310951	Pires Ferreira	\N	230016	23
2311009	Poranga	336	230013	23
2311108	Porteiras	13	230012	23
2311207	Potengi	\N	230011	23
2311231	Potiretama	192	230007	23
2311264	Quiterianópolis	336	230013	23
2311306	Quixadá	417	230006	23
2311355	Quixelô	42	230009	23
2311405	Quixeramobim	417	230006	23
2311504	Quixeré	192	230007	23
2311603	Redenção	37	230003	23
2311702	Reriutaba	\N	230015	23
2311801	Russas	192	230007	23
2311900	Saboeiro	42	230009	23
2311959	Salitre	13	230011	23
2312007	Santana do Acaraú	38	230015	23
2312106	Santana do Cariri	\N	230011	23
2312205	Santa Quitéria	38	230015	23
2312304	São Benedito	326	230016	23
2312403	São Gonçalo do Amarante	37	230001	23
2312502	São João do Jaguaribe	192	230007	23
2312601	São Luís do Curu	37	230001	23
2312700	Senador Pompeu	42	230006	23
2312809	Senador Sá	\N	230015	23
2312908	Sobral	37	230015	23
2313005	Solonópole	42	230006	23
2313104	Tabuleiro do Norte	192	230007	23
2313203	Tamboril	336	230013	23
2313252	Tarrafas	\N	230011	23
2313302	Tauá	336	230014	23
2313351	Tejuçuoca	\N	230005	23
2313401	Tianguá	\N	230016	23
2313500	Trairi	154	230002	23
2313559	Tururu	154	230002	23
2313609	Ubajara	\N	230016	23
2313708	Umari	\N	230010	23
2313757	Umirim	154	230002	23
2313807	Uruburetama	154	230002	23
2313906	Uruoca	\N	230015	23
2313955	Varjota	\N	230015	23
2314003	Várzea Alegre	13	230011	23
2314102	Viçosa do Ceará	326	230016	23
2400109	Acari	39	240008	24
2400208	Açu	46	240011	24
2400307	Afonso Bezerra	63	240011	24
2400406	Água Nova	46	240010	24
2400505	Alexandria	\N	240010	24
2400604	Almino Afonso	\N	240010	24
2400703	Alto do Rodrigues	63	240011	24
2400802	Angicos	46	240011	24
2400901	Antônio Martins	\N	240010	24
2401008	Apodi	46	240009	24
2401107	Areia Branca	63	240009	24
2401206	Arês	63	240003	24
2401305	Augusto Severo	46	240009	24
2401404	Baía Formosa	63	240003	24
2401453	Baraúna	46	240009	24
2401503	Barcelona	\N	240006	24
2401602	Bento Fernandes	63	240001	24
2401651	Bodó	\N	240008	24
2401701	Bom Jesus	\N	240001	24
2401800	Brejinho	\N	240001	24
2401859	Caiçara do Norte	\N	240005	24
2401909	Caiçara do Rio do Vento	\N	240006	24
2402006	Caicó	39	240007	24
2402105	Campo Redondo	\N	240004	24
2402204	Canguaretama	63	240003	24
2402303	Caraúbas	46	240009	24
2402402	Carnaúba dos Dantas	63	240008	24
2402501	Carnaubais	46	240011	24
2402600	Ceará-Mirim	63	240001	24
2402709	Cerro Corá	\N	240008	24
2402808	Coronel Ezequiel	\N	240004	24
2402907	Coronel João Pessoa	\N	240010	24
2403004	Cruzeta	\N	240007	24
2403103	Currais Novos	39	240008	24
2403202	Doutor Severiano	46	240010	24
2403251	Parnamirim	63	240001	24
2403301	Encanto	\N	240010	24
2403400	Equador	\N	240007	24
2403509	Espírito Santo	\N	240003	24
2403608	Extremoz	63	240001	24
2403707	Felipe Guerra	46	240009	24
2403756	Fernando Pedroza	\N	240011	24
2403806	Florânia	\N	240008	24
2403905	Francisco Dantas	\N	240010	24
2404002	Frutuoso Gomes	\N	240010	24
2404101	Galinhos	63	240005	24
2404200	Goianinha	63	240003	24
2404309	Governador Dix-Sept Rosado	46	240009	24
2404408	Grossos	46	240009	24
2404507	Guamaré	63	240011	24
2404606	Ielmo Marinho	63	240001	24
2404705	Ipanguaçu	\N	240011	24
2404804	Ipueira	\N	240007	24
2404853	Itajá	\N	240011	24
2404903	Itaú	\N	240009	24
2405009	Jaçanã	\N	240004	24
2405108	Jandaíra	\N	240005	24
2405207	Janduís	46	240009	24
2405306	Januário Cicco	\N	240002	24
2405405	Japi	\N	240004	24
2405504	Jardim de Angicos	\N	240005	24
2405603	Jardim de Piranhas	\N	240007	24
2405702	Jardim do Seridó	\N	240007	24
2405801	João Câmara	63	240005	24
2405900	João Dias	\N	240010	24
2406007	José da Penha	\N	240010	24
2406106	Jucurutu	39	240007	24
2406155	Jundiá	\N	240001	24
2406205	Lagoa d'Anta	\N	240002	24
2406304	Lagoa de Pedras	63	240002	24
2406403	Lagoa de Velhos	\N	240006	24
2406502	Lagoa Nova	46	240008	24
2406601	Lagoa Salgada	\N	240001	24
2406700	Lajes	\N	240011	24
2406809	Lajes Pintadas	\N	240004	24
2406908	Lucrécia	\N	240010	24
2407005	Luís Gomes	\N	240010	24
2407104	Macaíba	63	240001	24
2407203	Macau	63	240011	24
2407252	Major Sales	\N	240010	24
2407302	Marcelino Vieira	\N	240010	24
2407401	Martins	\N	240010	24
2407500	Maxaranguape	63	240001	24
2407609	Messias Targino	\N	240009	24
2407708	Montanhas	\N	240003	24
2407807	Monte Alegre	\N	240001	24
2407906	Monte das Gameleiras	\N	240002	24
2408003	Mossoró	46	240009	24
2408102	Natal	63	240001	24
2408201	Nísia Floresta	63	240001	24
2408300	Nova Cruz	\N	240002	24
2408409	Olho-d'Água do Borges	\N	240010	24
2408508	Ouro Branco	\N	240007	24
2408607	Paraná	\N	240010	24
2408706	Paraú	\N	240011	24
2408805	Parazinho	\N	240005	24
2408904	Parelhas	39	240007	24
2408953	Rio do Fogo	63	240001	24
2409100	Passa e Fica	\N	240002	24
2409209	Passagem	\N	240002	24
2409308	Patu	46	240010	24
2409332	Santa Maria	63	240006	24
2409407	Pau dos Ferros	46	240010	24
2409506	Pedra Grande	63	240005	24
2409605	Pedra Preta	\N	240005	24
2409704	Pedro Avelino	\N	240005	24
2409803	Pedro Velho	63	240003	24
2409902	Pendências	63	240011	24
2410009	Pilões	\N	240010	24
2410108	Poço Branco	63	240001	24
2410207	Portalegre	\N	240010	24
2410256	Porto do Mangue	\N	240011	24
2410306	Serra Caiada	\N	240002	24
2410405	Pureza	63	240001	24
2410504	Rafael Fernandes	\N	240010	24
2410603	Rafael Godeiro	\N	240010	24
2410702	Riacho da Cruz	\N	240010	24
2410801	Riacho de Santana	\N	240010	24
2410900	Riachuelo	\N	240006	24
2411007	Rodolfo Fernandes	\N	240009	24
2411056	Tibau	46	240009	24
2411106	Ruy Barbosa	63	240006	24
2411205	Santa Cruz	63	240004	24
2411403	Santana do Matos	\N	240011	24
2411429	Santana do Seridó	39	240007	24
2411502	Santo Antônio	\N	240002	24
2411601	São Bento do Norte	63	240005	24
2411700	São Bento do Trairí	\N	240004	24
2411809	São Fernando	\N	240007	24
2411908	São Francisco do Oeste	\N	240010	24
2412005	São Gonçalo do Amarante	63	240001	24
2412104	São João do Sabugi	\N	240007	24
2412203	São José de Mipibu	\N	240001	24
2412302	São José do Campestre	63	240002	24
2412401	São José do Seridó	\N	240007	24
2412500	São Miguel	\N	240010	24
2412559	São Miguel do Gostoso	63	240001	24
2412609	São Paulo do Potengi	63	240006	24
2412708	São Pedro	\N	240006	24
2412807	São Rafael	63	240011	24
2412906	São Tomé	\N	240006	24
2413003	São Vicente	\N	240008	24
2413102	Senador Elói de Souza	63	240006	24
2413201	Senador Georgino Avelino	63	240001	24
2413300	Serra de São Bento	\N	240002	24
2413359	Serra do Mel	46	240009	24
2413409	Serra Negra do Norte	\N	240007	24
2413508	Serrinha	\N	240002	24
2413557	Serrinha dos Pintos	\N	240010	24
2413607	Severiano Melo	\N	240009	24
2413706	Sítio Novo	63	240004	24
2413805	Taboleiro Grande	\N	240010	24
2413904	Taipu	63	240001	24
2414001	Tangará	\N	240004	24
2414100	Tenente Ananias	\N	240010	24
2414159	Tenente Laurentino Cruz	\N	240008	24
2414209	Tibau do Sul	63	240003	24
2414308	Timbaúba dos Batistas	\N	240007	24
2414407	Touros	63	240001	24
2414456	Triunfo Potiguar	\N	240011	24
2414506	Umarizal	\N	240010	24
2414605	Upanema	46	240009	24
2414704	Várzea	\N	240002	24
2414753	Venha-Ver	\N	240010	24
2414803	Vera Cruz	63	240001	24
2414902	Viçosa	\N	240010	24
2415008	Vila Flor	\N	240003	24
2500106	Água Branca	\N	250009	25
2500205	Aguiar	103	250010	25
2500304	Alagoa Grande	110	250005	25
2500403	Alagoa Nova	111	250005	25
2500502	Alagoinha	\N	250002	25
2500536	Alcantil	111	250005	25
2500577	Algodão de Jandaíra	112	250005	25
2500601	Alhandra	110	250001	25
2500700	São João do Rio do Peixe	\N	250015	25
2500734	Amparo	\N	250008	25
2500775	Aparecida	103	250014	25
2500809	Araçagi	112	250002	25
2500908	Arara	112	250002	25
2501005	Araruna	112	250002	25
2501104	Areia	110	250005	25
2501153	Areia de Baraúnas	\N	250009	25
2501203	Areial	111	250005	25
2501302	Aroeiras	111	250005	25
2501351	Assunção	75	250005	25
2501401	Baía da Traição	110	250003	25
2501500	Bananeiras	112	250002	25
2501534	Baraúna	\N	250006	25
2501575	Barra de Santana	\N	250005	25
2501609	Barra de Santa Rosa	111	250006	25
2501708	Barra de São Miguel	111	250005	25
2501807	Bayeux	110	250001	25
2501906	Belém	\N	250002	25
2502003	Belém do Brejo do Cruz	\N	250011	25
2502052	Bernardino Batista	\N	250014	25
2502102	Boa Ventura	\N	250010	25
2502151	Boa Vista	111	250005	25
2502201	Bom Jesus	\N	250015	25
2502300	Bom Sucesso	\N	250011	25
2502409	Bonito de Santa Fé	103	250015	25
2502508	Boqueirão	111	250005	25
2502607	Igaracy	\N	250010	25
2502706	Borborema	112	250002	25
2502805	Brejo do Cruz	\N	250011	25
2502904	Brejo dos Santos	\N	250011	25
2503001	Caaporã	110	250001	25
2503100	Cabaceiras	111	250005	25
2503209	Cabedelo	110	250001	25
2503308	Cachoeira dos Índios	103	250015	25
2503407	Cacimba de Areia	75	250009	25
2503506	Cacimba de Dentro	\N	250002	25
2503555	Cacimbas	\N	250009	25
2503605	Caiçara	\N	250002	25
2503704	Cajazeiras	103	250015	25
2503753	Cajazeirinhas	\N	250012	25
2503803	Caldas Brandão	\N	250001	25
2503902	Camalaú	111	250007	25
2504009	Campina Grande	111	250005	25
2504033	Capim	110	250003	25
2504074	Caraúbas	\N	250005	25
2504108	Carrapateira	\N	250015	25
2504157	Casserengue	112	250002	25
2504207	Catingueira	75	250009	25
2504306	Catolé do Rocha	\N	250011	25
2504355	Caturité	\N	250005	25
2504405	Conceição	\N	250010	25
2504504	Condado	75	250012	25
2504603	Conde	110	250001	25
2504702	Congo	\N	250008	25
2504801	Coremas	103	250009	25
2504850	Coxixola	\N	250008	25
2504900	Cruz do Espírito Santo	110	250001	25
2505006	Cubati	111	250005	25
2505105	Cuité	\N	250006	25
2505204	Cuitegi	\N	250002	25
2505238	Cuité de Mamanguape	\N	250001	25
2505279	Curral de Cima	110	250003	25
2505303	Curral Velho	\N	250010	25
2505352	Damião	\N	250006	25
2505402	Desterro	75	250009	25
2505501	Vista Serrana	\N	250009	25
2505600	Diamante	103	250010	25
2505709	Dona Inês	112	250002	25
2505808	Duas Estradas	\N	250002	25
2505907	Emas	75	250009	25
2506004	Esperança	111	250005	25
2506103	Fagundes	\N	250005	25
2506202	Frei Martinho	\N	250006	25
2506251	Gado Bravo	\N	250005	25
2506301	Guarabira	112	250002	25
2506400	Gurinhém	110	250001	25
2506509	Gurjão	\N	250005	25
2506608	Ibiara	103	250010	25
2506707	Imaculada	75	250009	25
2506806	Ingá	110	250005	25
2506905	Itabaiana	110	250004	25
2507002	Itaporanga	\N	250010	25
2507101	Itapororoca	\N	250003	25
2507200	Itatuba	110	250005	25
2507309	Jacaraú	110	250003	25
2507408	Jericó	103	250011	25
2507507	João Pessoa	110	250001	25
2507606	Juarez Távora	110	250001	25
2507705	Juazeirinho	111	250005	25
2507804	Junco do Seridó	\N	250005	25
2507903	Juripiranga	110	250001	25
2508000	Juru	75	250013	25
2508109	Lagoa	103	250012	25
2508208	Lagoa de Dentro	\N	250002	25
2508307	Lagoa Seca	111	250005	25
2508406	Lastro	\N	250014	25
2508505	Livramento	75	250008	25
2508554	Logradouro	\N	250002	25
2508604	Lucena	\N	250001	25
2508703	Mãe d'Água	\N	250009	25
2508802	Malta	\N	250009	25
2508901	Mamanguape	110	250003	25
2509008	Manaíra	\N	250013	25
2509057	Marcação	110	250003	25
2509107	Mari	110	250001	25
2509156	Marizópolis	103	250014	25
2509206	Massaranduba	111	250005	25
2509305	Mataraca	110	250003	25
2509339	Matinhas	111	250005	25
2509370	Mato Grosso	\N	250011	25
2509396	Maturéia	75	250009	25
2509404	Mogeiro	110	250004	25
2509503	Montadas	\N	250005	25
2509602	Monte Horebe	\N	250015	25
2509701	Monteiro	111	250007	25
2509800	Mulungu	\N	250002	25
2509909	Natuba	\N	250004	25
2510006	Nazarezinho	\N	250014	25
2510105	Nova Floresta	\N	250006	25
2510204	Nova Olinda	\N	250010	25
2510303	Nova Palmeira	\N	250006	25
2510402	Olho d'Água	\N	250009	25
2510501	Olivedos	\N	250005	25
2510600	Ouro Velho	\N	250007	25
2510659	Parari	\N	250008	25
2510709	Passagem	75	250009	25
2510808	Patos	75	250009	25
2510907	Paulista	103	250012	25
2511004	Pedra Branca	\N	250010	25
2511103	Pedra Lavrada	\N	250006	25
2511202	Pedras de Fogo	110	250001	25
2511301	Piancó	75	250010	25
2511400	Picuí	111	250006	25
2511509	Pilar	110	250001	25
2511608	Pilões	112	250002	25
2511707	Pilõezinhos	\N	250002	25
2511806	Pirpirituba	\N	250002	25
2511905	Pitimbu	110	250001	25
2512002	Pocinhos	111	250005	25
2512036	Poço Dantas	103	250014	25
2512077	Poço de José de Moura	\N	250015	25
2512101	Pombal	103	250012	25
2512200	Prata	\N	250007	25
2512309	Princesa Isabel	\N	250013	25
2512408	Puxinanã	\N	250005	25
2512507	Queimadas	111	250005	25
2512606	Quixabá	\N	250009	25
2512705	Remígio	110	250005	25
2512721	Pedro Régis	\N	250003	25
2512747	Riachão	\N	250002	25
2512754	Riachão do Bacamarte	110	250005	25
2512762	Riachão do Poço	110	250001	25
2512788	Riacho de Santo Antônio	\N	250005	25
2512804	Riacho dos Cavalos	\N	250011	25
2512903	Rio Tinto	110	250003	25
2513000	Salgadinho	75	250009	25
2513109	Salgado de São Félix	110	250004	25
2513158	Santa Cecília	\N	250005	25
2513208	Santa Cruz	103	250014	25
2513307	Santa Helena	\N	250015	25
2513356	Santa Inês	\N	250010	25
2513406	Santa Luzia	75	250009	25
2513505	Santana de Mangueira	\N	250010	25
2513604	Santana dos Garrotes	75	250010	25
2513653	Joca Claudino	\N	250014	25
2513703	Santa Rita	110	250001	25
2513802	Santa Teresinha	75	250009	25
2513851	Santo André	\N	250005	25
2513901	São Bento	103	250011	25
2513927	São Bentinho	\N	250012	25
2513943	São Domingos do Cariri	\N	250005	25
2513968	São Domingos	\N	250012	25
2513984	São Francisco	\N	250014	25
2514008	São João do Cariri	111	250005	25
2514107	São João do Tigre	\N	250007	25
2514206	São José da Lagoa Tapada	103	250014	25
2514305	São José de Caiana	\N	250010	25
2514404	São José de Espinharas	\N	250009	25
2514453	São José dos Ramos	110	250004	25
2514503	São José de Piranhas	103	250015	25
2514552	São José de Princesa	\N	250013	25
2514602	São José do Bonfim	\N	250009	25
2514651	São José do Brejo do Cruz	\N	250011	25
2514701	São José do Sabugi	\N	250009	25
2514800	São José dos Cordeiros	\N	250008	25
2514909	São Mamede	75	250009	25
2515005	São Miguel de Taipu	110	250001	25
2515104	São Sebastião de Lagoa de Roça	\N	250005	25
2515203	São Sebastião do Umbuzeiro	111	250007	25
2515302	Sapé	110	250001	25
2515401	São Vicente do Seridó	\N	250005	25
2515500	Serra Branca	111	250008	25
2515609	Serra da Raiz	\N	250002	25
2515708	Serra Grande	\N	250015	25
2515807	Serra Redonda	\N	250005	25
2515906	Serraria	112	250002	25
2515930	Sertãozinho	\N	250002	25
2515971	Sobrado	110	250001	25
2516003	Solânea	112	250002	25
2516102	Soledade	111	250005	25
2516151	Sossêgo	111	250006	25
2516201	Sousa	103	250014	25
2516300	Sumé	111	250008	25
2516409	Tacima	\N	250002	25
2516508	Taperoá	75	250005	25
2516607	Tavares	\N	250013	25
2516706	Teixeira	75	250009	25
2516755	Tenório	\N	250005	25
2516805	Triunfo	103	250015	25
2516904	Uiraúna	103	250014	25
2517001	Umbuzeiro	\N	250005	25
2517100	Várzea	75	250009	25
2517209	Vieirópolis	\N	250014	25
2517407	Zabelê	\N	250007	25
2600054	Abreu e Lima	22	260001	26
2600104	Afogados da Ingazeira	62	260014	26
2600203	Afrânio	66	260015	26
2600302	Agrestina	69	260009	26
2600401	Água Preta	85	260003	26
2600500	Águas Belas	90	260010	26
2600609	Alagoinha	113	260012	26
2600708	Aliança	146	260002	26
2600807	Altinho	69	260009	26
2600906	Amaraji	22	260018	26
2601003	Angelim	90	260010	26
2601052	Araçoiaba	146	260001	26
2601102	Araripina	66	260016	26
2601201	Arcoverde	113	260011	26
2601300	Barra de Guabiraba	69	260009	26
2601409	Barreiros	85	260007	26
2601508	Belém de Maria	85	260003	26
2601607	Belém do São Francisco	\N	260017	26
2601706	Belo Jardim	113	260012	26
2601805	Betânia	470	260013	26
2601904	Bezerros	69	260009	26
2602001	Bodocó	66	260016	26
2602100	Bom Conselho	90	260010	26
2602209	Bom Jardim	146	260004	26
2602308	Bonito	69	260009	26
2602407	Brejão	90	260010	26
2602506	Brejinho	62	260014	26
2602605	Brejo da Madre de Deus	113	260009	26
2602704	Buenos Aires	146	260006	26
2602803	Buíque	113	260011	26
2602902	Cabo de Santo Agostinho	22	260001	26
2603009	Cabrobó	470	260017	26
2603108	Cachoeirinha	69	260009	26
2603207	Caetés	\N	260010	26
2603306	Calçado	\N	260010	26
2603405	Calumbi	\N	260013	26
2603454	Camaragibe	\N	260001	26
2603504	Camocim de São Félix	69	260009	26
2603603	Camutanga	146	260002	26
2603702	Canhotinho	90	260010	26
2603801	Capoeiras	90	260010	26
2603900	Carnaíba	62	260014	26
2603926	Carnaubeira da Penha	470	260013	26
2604007	Carpina	146	260006	26
2604106	Caruaru	69	260009	26
2604155	Casinhas	\N	260008	26
2604205	Catende	146	260003	26
2604304	Cedro	\N	260017	26
2604403	Chã de Alegria	146	260005	26
2604502	Chã Grande	69	260009	26
2604601	Condado	146	260002	26
2604700	Correntes	\N	260010	26
2604809	Cortês	85	260018	26
2604908	Cumaru	146	260009	26
2605004	Cupira	85	260009	26
2605103	Custódia	22	260011	26
2605152	Dormentes	\N	260015	26
2605202	Escada	22	260018	26
2605301	Exu	66	260016	26
2605400	Feira Nova	146	260004	26
2605459	Fernando de Noronha	\N	260001	26
2605509	Ferreiros	146	260002	26
2605608	Flores	62	260013	26
2605707	Floresta	470	260013	26
2605806	Frei Miguelinho	\N	260008	26
2605905	Gameleira	85	260003	26
2606002	Garanhuns	90	260010	26
2606101	Glória do Goitá	146	260005	26
2606200	Goiana	146	260002	26
2606309	Granito	66	260016	26
2606408	Gravatá	69	260009	26
2606507	Iati	90	260010	26
2606606	Ibimirim	470	260011	26
2606705	Ibirajuba	69	260009	26
2606804	Igarassu	22	260001	26
2606903	Iguaraci	62	260014	26
2607000	Inajá	470	260011	26
2607109	Ingazeira	\N	260014	26
2607208	Ipojuca	22	260001	26
2607307	Ipubi	66	260016	26
2607406	Itacuruba	470	260017	26
2607505	Itaíba	90	260011	26
2607604	Ilha de Itamaracá	22	260001	26
2607653	Itambé	146	260002	26
2607703	Itapetim	\N	260014	26
2607752	Itapissuma	22	260001	26
2607802	Itaquitinga	146	260002	26
2607901	Jaboatão dos Guararapes	22	260001	26
2607950	Jaqueira	85	260003	26
2608008	Jataúba	113	260009	26
2608057	Jatobá	470	260013	26
2608107	João Alfredo	146	260004	26
2608206	Joaquim Nabuco	85	260003	26
2608255	Jucati	90	260010	26
2608305	Jupi	\N	260010	26
2608404	Jurema	90	260010	26
2608453	Lagoa do Carro	146	260006	26
2608503	LAGOA DE ITAENGA	\N	260006	26
2608602	Lagoa do Ouro	\N	260010	26
2608701	Lagoa dos Gatos	85	260009	26
2608750	Lagoa Grande	66	260015	26
2608800	Lajedo	90	260010	26
2608909	Limoeiro	146	260004	26
2609006	Macaparana	146	260002	26
2609105	Machados	\N	260004	26
2609154	Manari	470	260011	26
2609204	Maraial	85	260003	26
2609303	Mirandiba	62	260013	26
2609402	Moreno	22	260001	26
2609501	Nazaré da Mata	146	260006	26
2609600	Olinda	22	260001	26
2609709	Orobó	146	260004	26
2609808	Orocó	470	260015	26
2609907	Ouricuri	66	260016	26
2610004	Palmares	85	260003	26
2610103	Palmeirina	\N	260010	26
2610202	Panelas	90	260009	26
2610301	Paranatama	90	260010	26
2610400	Parnamirim	66	260017	26
2610509	Passira	146	260004	26
2610608	Paudalho	146	260001	26
2610707	Paulista	22	260001	26
2610806	Pedra	113	260011	26
2610905	Pesqueira	113	260012	26
2611002	Petrolândia	470	260013	26
2611101	Petrolina	66	260015	26
2611200	Poção	113	260012	26
2611309	Pombos	22	260005	26
2611408	Primavera	22	260018	26
2611507	Quipapá	90	260010	26
2611533	Quixaba	62	260014	26
2611606	Recife	22	260001	26
2611705	Riacho das Almas	69	260009	26
2611804	Ribeirão	85	260018	26
2611903	Rio Formoso	85	260007	26
2612000	Sairé	69	260009	26
2612109	Salgadinho	146	260004	26
2612208	Salgueiro	66	260017	26
2612307	Saloá	\N	260010	26
2612406	Sanharó	113	260012	26
2612455	Santa Cruz	66	260016	26
2612471	Santa Cruz da Baixa Verde	\N	260013	26
2612505	Santa Cruz do Capibaribe	69	260009	26
2612554	Santa Filomena	66	260016	26
2612604	Santa Maria da Boa Vista	66	260015	26
2612703	Santa Maria do Cambucá	\N	260008	26
2612802	Santa Terezinha	\N	260014	26
2612901	São Benedito do Sul	90	260003	26
2613008	São Bento do Una	90	260012	26
2613107	São Caitano	69	260009	26
2613206	São João	\N	260010	26
2613305	São Joaquim do Monte	69	260009	26
2613404	São José da Coroa Grande	85	260007	26
2613503	São José do Belmonte	62	260013	26
2613602	São José do Egito	62	260014	26
2613701	São Lourenço da Mata	22	260001	26
2613800	São Vicente Ferrer	146	260002	26
2613909	Serra Talhada	62	260013	26
2614006	Serrita	66	260017	26
2614105	Sertânia	113	260011	26
2614204	Sirinhaém	85	260007	26
2614303	Moreilândia	\N	260016	26
2614402	Solidão	\N	260014	26
2614501	Surubim	146	260008	26
2614600	Tabira	62	260014	26
2614709	Tacaimbó	69	260012	26
2614808	Tacaratu	470	260013	26
2614857	Tamandaré	85	260007	26
2615003	Taquaritinga do Norte	69	260009	26
2615102	Terezinha	\N	260010	26
2615201	Terra Nova	66	260017	26
2615300	Timbaúba	146	260002	26
2615409	Toritama	69	260009	26
2615508	Tracunhaém	146	260006	26
2615607	Trindade	66	260016	26
2615706	Triunfo	62	260013	26
2615805	Tupanatinga	113	260011	26
2615904	Tuparetama	62	260014	26
2616001	Venturosa	\N	260011	26
2616100	Verdejante	\N	260017	26
2616183	Vertente do Lério	\N	260008	26
2616209	Vertentes	69	260008	26
2616308	Vicência	146	260002	26
2616407	Vitória de Santo Antão	22	260005	26
2616506	Xexéu	85	260003	26
2700102	Água Branca	74	270009	27
2700201	Anadia	231	270004	27
2700300	Arapiraca	252	270007	27
2700409	Atalaia	252	270006	27
2700508	Barra de Santo Antônio	252	270001	27
2700607	Barra de São Miguel	\N	270001	27
2700706	Batalha	74	270011	27
2700805	Belém	\N	270008	27
2700904	Belo Monte	74	270011	27
2701001	Boca da Mata	231	270004	27
2701100	Branquinha	252	270005	27
2701209	Cacimbinhas	74	270008	27
2701308	Cajueiro	252	270006	27
2701357	Campestre	\N	270002	27
2701407	Campo Alegre	231	270004	27
2701506	Campo Grande	\N	270007	27
2701605	Canapi	\N	270010	27
2701704	Capela	252	270006	27
2701803	Carneiros	\N	270010	27
2701902	Chã Preta	252	270006	27
2702009	Coité do Nóia	\N	270007	27
2702108	Colônia Leopoldina	252	270002	27
2702207	Coqueiro Seco	252	270001	27
2702306	Coruripe	231	270003	27
2702355	Craíbas	231	270007	27
2702405	Delmiro Gouveia	74	270009	27
2702504	Dois Riachos	\N	270010	27
2702553	Estrela de Alagoas	\N	270008	27
2702603	Feira Grande	231	270007	27
2702702	Feliz Deserto	\N	270003	27
2702801	Flexeiras	252	270001	27
2702900	Girau do Ponciano	231	270007	27
2703007	Ibateguara	252	270005	27
2703106	Igaci	74	270008	27
2703205	Igreja Nova	231	270003	27
2703304	Inhapi	74	270009	27
2703403	Jacaré dos Homens	74	270011	27
2703502	Jacuípe	252	270002	27
2703601	Japaratinga	252	270002	27
2703700	Jaramataia	74	270007	27
2703759	Jequiá da Praia	231	270004	27
2703809	Joaquim Gomes	252	270001	27
2703908	Jundiá	252	270002	27
2704005	Junqueiro	231	270007	27
2704104	Lagoa da Canoa	\N	270007	27
2704203	Limoeiro de Anadia	\N	270007	27
2704302	Maceió	252	270001	27
2704401	Major Isidoro	\N	270008	27
2704500	Maragogi	252	270002	27
2704609	Maravilha	\N	270010	27
2704708	Marechal Deodoro	252	270001	27
2704807	Maribondo	\N	270007	27
2704906	Mar Vermelho	\N	270006	27
2705002	Mata Grande	74	270009	27
2705101	Matriz de Camaragibe	\N	270002	27
2705200	Messias	252	270001	27
2705309	Minador do Negrão	\N	270008	27
2705408	Monteirópolis	\N	270011	27
2705507	Murici	252	270005	27
2705606	Novo Lino	252	270002	27
2705705	Olho d'Água das Flores	\N	270011	27
2705804	Olho d'Água do Casado	\N	270009	27
2705903	Olho d'Água Grande	\N	270007	27
2706000	Olivença	\N	270010	27
2706109	Ouro Branco	\N	270010	27
2706208	Palestina	\N	270011	27
2706307	Palmeira dos Índios	74	270008	27
2706406	Pão de Açúcar	74	270011	27
2706422	Pariconha	74	270009	27
2706448	Paripueira	252	270001	27
2706505	Passo de Camaragibe	\N	270002	27
2706604	Paulo Jacinto	\N	270008	27
2706703	Penedo	231	270003	27
2706802	Piaçabuçu	231	270003	27
2706901	Pilar	252	270001	27
2707008	Pindoba	\N	270006	27
2707107	Piranhas	74	270009	27
2707206	Poço das Trincheiras	74	270010	27
2707305	Porto Calvo	252	270002	27
2707404	Porto de Pedras	252	270002	27
2707503	Porto Real do Colégio	231	270003	27
2707602	Quebrangulo	74	270008	27
2707701	Rio Largo	252	270001	27
2707800	Roteiro	231	270004	27
2707909	Santa Luzia do Norte	252	270001	27
2708006	Santana do Ipanema	\N	270010	27
2708105	Santana do Mundaú	252	270005	27
2708204	São Brás	231	270003	27
2708303	São José da Laje	\N	270005	27
2708402	São José da Tapera	74	270011	27
2708501	São Luís do Quitunde	252	270002	27
2708600	São Miguel dos Campos	231	270004	27
2708709	São Miguel dos Milagres	252	270002	27
2708808	São Sebastião	231	270007	27
2708907	Satuba	252	270001	27
2708956	Senador Rui Palmeira	74	270010	27
2709004	Tanque d'Arca	\N	270007	27
2709103	Taquarana	231	270007	27
2709152	Teotônio Vilela	231	270007	27
2709202	Traipu	231	270007	27
2709301	União dos Palmares	252	270005	27
2709400	Viçosa	252	270006	27
2800100	Amparo de São Francisco	227	280003	28
2800209	Aquidabã	\N	280003	28
2800308	Aracaju	307	280001	28
2800407	Arauá	348	280002	28
2800506	Areia Branca	\N	280004	28
2800605	Barra dos Coqueiros	307	280001	28
2800670	Boquim	348	280002	28
2800704	Brejo Grande	227	280003	28
2801009	Campo do Brito	\N	280004	28
2801108	Canhoba	227	280003	28
2801207	Canindé de São Francisco	227	280006	28
2801306	Capela	307	280001	28
2801405	Carira	307	280004	28
2801504	Carmópolis	\N	280001	28
2801603	Cedro de São João	\N	280003	28
2801702	Cristinápolis	348	280002	28
2801900	Cumbe	307	280001	28
2802007	Divina Pastora	\N	280001	28
2802106	Estância	348	280002	28
2802205	Feira Nova	\N	280006	28
2802304	Frei Paulo	307	280004	28
2802403	Gararu	227	280006	28
2802502	General Maynard	\N	280001	28
2802601	Gracho Cardoso	227	280006	28
2802700	Ilha das Flores	\N	280003	28
2802809	Indiaroba	348	280002	28
2802908	Itabaiana	307	280004	28
2803005	Itabaianinha	\N	280002	28
2803104	Itabi	227	280006	28
2803203	Itaporanga d'Ajuda	\N	280001	28
2803302	Japaratuba	227	280001	28
2803401	Japoatã	227	280003	28
2803500	Lagarto	348	280005	28
2803609	Laranjeiras	307	280001	28
2803708	Macambira	307	280004	28
2803807	Malhada dos Bois	227	280003	28
2803906	Malhador	307	280004	28
2804003	Maruim	307	280001	28
2804102	Moita Bonita	\N	280004	28
2804201	Monte Alegre de Sergipe	227	280006	28
2804300	Muribeca	227	280003	28
2804409	Neópolis	227	280003	28
2804458	Nossa Senhora Aparecida	\N	280004	28
2804508	Nossa Senhora da Glória	227	280006	28
2804607	Nossa Senhora das Dores	\N	280001	28
2804706	Nossa Senhora de Lourdes	\N	280003	28
2804805	Nossa Senhora do Socorro	307	280001	28
2804904	Pacatuba	227	280003	28
2805000	Pedra Mole	307	280004	28
2805109	Pedrinhas	348	280002	28
2805208	Pinhão	307	280004	28
2805307	Pirambu	\N	280001	28
2805406	Poço Redondo	227	280006	28
2805505	Poço Verde	348	280005	28
2805604	Porto da Folha	227	280006	28
2805703	Propriá	227	280003	28
2805802	Riachão do Dantas	348	280005	28
2805901	Riachuelo	307	280001	28
2806008	Ribeirópolis	307	280004	28
2806107	Rosário do Catete	\N	280001	28
2806206	Salgado	\N	280005	28
2806305	Santa Luzia do Itanhy	348	280002	28
2806404	Santana do São Francisco	\N	280003	28
2806503	Santa Rosa de Lima	\N	280001	28
2806602	Santo Amaro das Brotas	307	280001	28
2806701	São Cristóvão	307	280001	28
2806800	São Domingos	\N	280004	28
2806909	São Francisco	227	280003	28
2807006	São Miguel do Aleixo	\N	280004	28
2807105	Simão Dias	348	280005	28
2807204	Siriri	307	280001	28
2807303	Telha	227	280003	28
2807402	Tobias Barreto	348	280005	28
2807501	Tomar do Geru	348	280002	28
2807600	Umbaúba	348	280002	28
2900108	Abaíra	14	290013	29
2900207	Abaré	15	290024	29
2900306	Acajutiba	31	290002	29
2900355	Adustina	15	290027	29
2900405	Água Fria	793	290029	29
2900504	Érico Cardoso	\N	290013	29
2900603	Aiquara	106	290012	29
2900702	Alagoinhas	31	290002	29
2900801	Alcobaça	124	290008	29
2900900	Almadina	149	290007	29
2901007	Amargosa	\N	290003	29
2901106	Amélia Rodrigues	81	290029	29
2901155	América Dourada	\N	290020	29
2901205	Anagé	232	290011	29
2901304	Andaraí	244	290031	29
2901353	Andorinha	246	290023	29
2901403	Angical	\N	290018	29
2901502	Anguera	\N	290029	29
2901601	Antas	15	290027	29
2901700	Antônio Cardoso	81	290029	29
2901809	Antônio Gonçalves	246	290023	29
2901908	Aporá	\N	290002	29
2901957	Apuarema	\N	290012	29
2902005	Aracatu	\N	290013	29
2902054	Araças	31	290002	29
2902104	Araci	\N	290033	29
2902203	Aramari	\N	290002	29
2902252	Arataca	344	290010	29
2902302	Aratuípe	\N	290006	29
2902401	Aurelino Leal	149	290007	29
2902500	Baianópolis	256	290018	29
2902609	Baixa Grande	244	290029	29
2902658	Banzaê	\N	290025	29
2902708	Barra	431	290021	29
2902807	Barra da Estiva	\N	290011	29
2902906	Barra do Choça	232	290011	29
2903003	Barra do Mendes	\N	290020	29
2903102	Barra do Rocha	149	290014	29
2903201	Barreiras	256	290018	29
2903235	Barro Alto	219	290020	29
2903276	Barrocas	\N	290033	29
2903300	Barro Preto	344	290007	29
2903409	Belmonte	480	290009	29
2903508	Belo Campo	\N	290011	29
2903607	Biritinga	81	290033	29
2903706	Boa Nova	\N	290011	29
2903805	Boa Vista do Tupim	244	290031	29
2903904	Bom Jesus da Lapa	520	290017	29
2903953	Bom Jesus da Serra	790	290011	29
2904001	Boninal	\N	290034	29
2904050	Bonito	244	290020	29
2904100	Boquira	313	290017	29
2904209	Botuporã	\N	290016	29
2904308	Brejões	\N	290003	29
2904407	Brejolândia	\N	290018	29
2904506	Brotas de Macaúbas	431	290021	29
2904605	Brumado	313	290013	29
2904704	Buerarema	344	290007	29
2904753	Buritirama	431	290021	29
2904803	Caatiba	790	290011	29
2904852	Cabaceiras do Paraguaçu	\N	290004	29
2904902	Cachoeira	20	290004	29
2905008	Caculé	313	290016	29
2905107	Caém	246	290030	29
2905156	Caetanos	\N	290011	29
2905206	Caetité	313	290016	29
2905305	Cafarnaum	219	290020	29
2905404	Cairu	216	290005	29
2905503	Caldeirão Grande	246	290030	29
2905602	Camacan	344	290010	29
2905701	Camaçari	20	290001	29
2905800	Camamu	149	290005	29
2905909	Campo Alegre de Lourdes	644	290022	29
2906006	Campo Formoso	246	290023	29
2906105	Canápolis	520	290019	29
2906204	Canarana	219	290020	29
2906303	Canavieiras	344	290010	29
2906402	Candeal	\N	290029	29
2906501	Candeias	20	290001	29
2906600	Candiba	\N	290016	29
2906709	Cândido Sales	232	290011	29
2906808	Cansanção	246	290026	29
2906824	Canudos	\N	290026	29
2906857	Capela do Alto Alegre	\N	290029	29
2906873	Capim Grosso	246	290030	29
2906899	Caraíbas	313	290011	29
2906907	Caravelas	132	290008	29
2907004	Cardeal da Silva	31	290002	29
2907103	Carinhanha	520	290016	29
2907202	Casa Nova	644	290022	29
2907301	Castro Alves	\N	290004	29
2907400	Catolândia	\N	290018	29
2907509	Catu	31	290001	29
2907558	Caturama	\N	290013	29
2907608	Central	219	290020	29
2907707	Chorrochó	15	290024	29
2907806	Cícero Dantas	\N	290027	29
2907905	Cipó	\N	290025	29
2908002	Coaraci	149	290007	29
2908101	Cocos	520	290019	29
2908200	Conceição da Feira	\N	290029	29
2908309	Conceição do Almeida	216	290004	29
2908408	Conceição do Coité	793	290032	29
2908507	Conceição do Jacuípe	\N	290029	29
2908606	Conde	31	290002	29
2908705	Condeúba	\N	290011	29
2908804	Contendas do Sincorá	\N	290011	29
2908903	Coração de Maria	\N	290029	29
2909000	Cordeiros	313	290011	29
2909109	Coribe	520	290019	29
2909208	Coronel João Sá	\N	290028	29
2909307	Correntina	520	290019	29
2909406	Cotegipe	256	290018	29
2909505	Cravolândia	\N	290012	29
2909604	Crisópolis	\N	290002	29
2909703	Cristópolis	256	290018	29
2909802	Cruz das Almas	20	290004	29
2909901	Curaçá	644	290022	29
2910008	Dário Meira	\N	290014	29
2910057	Dias d'Ávila	\N	290001	29
2910107	Dom Basílio	\N	290013	29
2910206	Dom Macedo Costa	\N	290003	29
2910305	Elísio Medrado	\N	290003	29
2910404	Encruzilhada	790	290011	29
2910503	Entre Rios	31	290002	29
2910602	Esplanada	31	290002	29
2910701	Euclides da Cunha	15	290026	29
2910727	Eunápolis	480	290009	29
2910750	Fátima	\N	290027	29
2910776	Feira da Mata	\N	290016	29
2910800	Feira de Santana	81	290029	29
2910859	Filadélfia	\N	290023	29
2910909	Firmino Alves	\N	290007	29
2911006	Floresta Azul	344	290007	29
2911105	Formosa do Rio Preto	256	290018	29
2911204	Gandu	149	290014	29
2911253	Gavião	\N	290029	29
2911303	Gentio do Ouro	431	290021	29
2911402	Glória	15	290024	29
2911501	Gongogi	149	290014	29
2911600	Governador Mangabeira	\N	290004	29
2911659	Guajeru	\N	290016	29
2911709	Guanambi	313	290016	29
2911808	Guaratinga	132	290009	29
2911857	Heliópolis	\N	290027	29
2911907	Iaçu	216	290031	29
2912004	Ibiassucê	313	290016	29
2912103	Ibicaraí	344	290007	29
2912202	Ibicoara	14	290011	29
2912301	Ibicuí	790	290007	29
2912400	Ibipeba	\N	290020	29
2912509	Ibipitanga	\N	290017	29
2912608	Ibiquera	244	290031	29
2912707	Ibirapitanga	149	290007	29
2912806	Ibirapuã	124	290008	29
2912905	Ibirataia	149	290014	29
2913002	Ibitiara	14	290034	29
2913101	Ibititá	219	290020	29
2913200	Ibotirama	431	290021	29
2913309	Ichu	\N	290029	29
2913408	Igaporã	313	290016	29
2913457	Igrapiúna	149	290005	29
2913507	Iguaí	232	290011	29
2913606	Ilhéus	149	290007	29
2913705	Inhambupe	\N	290002	29
2913804	Ipecaetá	\N	290029	29
2913903	Ipiaú	149	290014	29
2914000	Ipirá	244	290029	29
2914109	Ipupiara	\N	290021	29
2914208	Irajuba	\N	290012	29
2914307	Iramaia	14	290011	29
2914406	Iraquara	\N	290034	29
2914505	Irará	\N	290029	29
2914604	Irecê	219	290020	29
2914653	Itabela	480	290009	29
2914703	Itaberaba	244	290031	29
2914802	Itabuna	344	290007	29
2914901	Itacaré	149	290007	29
2915007	Itaeté	244	290031	29
2915106	Itagi	\N	290012	29
2915205	Itagibá	106	290014	29
2915304	Itagimirim	480	290009	29
2915353	Itaguaçu da Bahia	431	290020	29
2915403	Itaju do Colônia	344	290007	29
2915502	Itajuípe	149	290007	29
2915601	Itamaraju	124	290008	29
2915700	Itamari	\N	290014	29
2915809	Itambé	232	290011	29
2915908	Itanagra	31	290001	29
2916005	Itanhém	124	290008	29
2916104	Itaparica	20	290006	29
2916203	Itapé	344	290007	29
2916302	Itapebi	480	290009	29
2916401	Itapetinga	232	290015	29
2916500	Itapicuru	\N	290002	29
2916609	Itapitanga	149	290007	29
2916708	Itaquara	\N	290012	29
2916807	Itarantim	\N	290015	29
2916856	Itatim	\N	290004	29
2916906	Itiruçu	\N	290012	29
2917003	Itiúba	246	290023	29
2917102	Itororó	\N	290015	29
2917201	Ituaçu	\N	290011	29
2917300	Ituberá	149	290005	29
2917334	Iuiú	313	290016	29
2917359	Jaborandi	520	290019	29
2917409	Jacaraci	\N	290016	29
2917508	Jacobina	246	290030	29
2917607	Jaguaquara	106	290012	29
2917706	Jaguarari	246	290023	29
2917805	Jaguaripe	216	290006	29
2917904	Jandaíra	31	290002	29
2918001	Jequié	106	290012	29
2918100	Jeremoabo	15	290028	29
2918209	Jiquiriçá	216	290003	29
2918308	Jitaúna	\N	290012	29
2918357	João Dourado	\N	290020	29
2918407	Juazeiro	644	290022	29
2918456	Jucuruçu	124	290008	29
2918506	Jussara	219	290020	29
2918555	Jussari	344	290010	29
2918605	Jussiape	\N	290013	29
2918704	Lafaiete Coutinho	\N	290012	29
2918753	Lagoa Real	313	290016	29
2918803	Laje	\N	290003	29
2918902	Lajedão	124	290008	29
2919009	Lajedinho	\N	290031	29
2919058	Lajedo do Tabocal	\N	290012	29
2919108	Lamarão	\N	290029	29
2919157	Lapão	\N	290020	29
2919207	Lauro de Freitas	679	290001	29
2919306	Lençóis	219	290034	29
2919405	Licínio de Almeida	\N	290016	29
2919504	Livramento de Nossa Senhora	\N	290013	29
2919553	Luís Eduardo Magalhães	\N	290018	29
2919603	Macajuba	\N	290029	29
2919702	Macarani	790	290015	29
2919801	Macaúbas	313	290017	29
2919900	Macururé	\N	290024	29
2919926	Madre de Deus	20	290001	29
2919959	Maetinga	\N	290011	29
2920007	Maiquinique	232	290015	29
2920106	Mairi	244	290029	29
2920205	Malhada	313	290016	29
2920304	Malhada de Pedras	\N	290013	29
2920403	Manoel Vitorino	\N	290012	29
2920452	Mansidão	\N	290018	29
2920502	Maracás	106	290012	29
2920601	Maragogipe	20	290006	29
2920700	Maraú	149	290007	29
2920809	Marcionílio Souza	106	290031	29
2920908	Mascote	344	290010	29
2921005	Mata de São João	31	290001	29
2921054	Matina	\N	290016	29
2921104	Medeiros Neto	124	290008	29
2921203	Miguel Calmon	244	290030	29
2921302	Milagres	\N	290003	29
2921401	Mirangaba	246	290030	29
2921450	Mirante	\N	290011	29
2921500	Monte Santo	246	290026	29
2921609	Morpará	431	290021	29
2921708	Morro do Chapéu	219	290020	29
2921807	Mortugaba	\N	290016	29
2921906	Mucugê	\N	290034	29
2922003	Mucuri	124	290008	29
2922052	Mulungu do Morro	219	290020	29
2922102	Mundo Novo	244	290029	29
2922201	Muniz Ferreira	\N	290003	29
2922250	Muquém de São Francisco	431	290021	29
2922300	Muritiba	\N	290004	29
2922409	Mutuípe	\N	290003	29
2922508	Nazaré	\N	290006	29
2922607	Nilo Peçanha	216	290005	29
2922656	Nordestina	246	290032	29
2922706	Nova Canaã	\N	290011	29
2922730	Nova Fátima	\N	290029	29
2922755	Nova Ibiá	149	290014	29
2922805	Nova Itarana	106	290003	29
2922854	Nova Redenção	244	290031	29
2922904	Nova Soure	31	290025	29
2923001	Nova Viçosa	124	290008	29
2923035	Novo Horizonte	\N	290034	29
2923050	Novo Triunfo	\N	290028	29
2923100	Olindina	31	290025	29
2923209	Oliveira dos Brejinhos	431	290021	29
2923308	Ouriçangas	\N	290029	29
2923357	Ourolândia	246	290030	29
2923407	Palmas de Monte Alto	313	290016	29
2923506	Palmeiras	\N	290034	29
2923605	Paramirim	\N	290013	29
2923704	Paratinga	\N	290017	29
2923803	Paripiranga	\N	290027	29
2923902	Pau Brasil	344	290010	29
2924009	Paulo Afonso	15	290024	29
2924058	Pé de Serra	\N	290029	29
2924108	Pedrão	\N	290002	29
2924207	Pedro Alexandre	15	290028	29
2924306	Piatã	\N	290034	29
2924405	Pilão Arcado	644	290022	29
2924504	Pindaí	313	290016	29
2924603	Pindobaçu	246	290023	29
2924652	Pintadas	\N	290029	29
2924678	Piraí do Norte	\N	290014	29
2924702	Piripá	313	290011	29
2924801	Piritiba	244	290030	29
2924900	Planaltino	106	290012	29
2925006	Planalto	232	290011	29
2925105	Poções	790	290011	29
2925204	Pojuca	\N	290001	29
2925253	Ponto Novo	246	290023	29
2925303	Porto Seguro	480	290009	29
2925402	Potiraguá	344	290015	29
2925501	Prado	132	290008	29
2925600	Presidente Dutra	\N	290020	29
2925709	Presidente Jânio Quadros	313	290011	29
2925758	Presidente Tancredo Neves	\N	290005	29
2925808	Queimadas	246	290032	29
2925907	Quijingue	15	290026	29
2925931	Quixabeira	\N	290030	29
2925956	Rafael Jambeiro	\N	290029	29
2926004	Remanso	644	290022	29
2926103	Retirolândia	\N	290032	29
2926202	Riachão das Neves	256	290018	29
2926301	Riachão do Jacuípe	\N	290029	29
2926400	Riacho de Santana	313	290016	29
2926509	Ribeira do Amparo	31	290025	29
2926608	Ribeira do Pombal	15	290025	29
2926657	Ribeirão do Largo	232	290011	29
2926707	Rio de Contas	14	290013	29
2926806	Rio do Antônio	\N	290016	29
2926905	Rio do Pires	\N	290013	29
2927002	Rio Real	31	290002	29
2927101	Rodelas	15	290024	29
2927200	Ruy Barbosa	244	290031	29
2927309	Salinas da Margarida	20	290006	29
2927408	Salvador	20	290001	29
2927507	Santa Bárbara	81	290029	29
2927606	Santa Brígida	15	290024	29
2927705	Santa Cruz Cabrália	480	290009	29
2927804	Santa Cruz da Vitória	\N	290007	29
2927903	Santa Inês	\N	290012	29
2928000	Santaluz	\N	290032	29
2928059	Santa Luzia	344	290010	29
2928109	Santa Maria da Vitória	284	290019	29
2928208	Santana	520	290018	29
2928307	Santanópolis	\N	290029	29
2928406	Santa Rita de Cássia	256	290018	29
2928505	Santa Teresinha	\N	290004	29
2928604	Santo Amaro	20	290001	29
2928703	Santo Antônio de Jesus	\N	290003	29
2928802	Santo Estêvão	\N	290029	29
2928901	São Desidério	256	290018	29
2928950	São Domingos	\N	290032	29
2929008	São Félix	20	290004	29
2929057	São Félix do Coribe	\N	290019	29
2929107	São Felipe	\N	290004	29
2929206	São Francisco do Conde	20	290001	29
2929255	São Gabriel	219	290020	29
2929305	São Gonçalo dos Campos	793	290029	29
2929354	São José da Vitória	344	290007	29
2929370	São José do Jacuípe	246	290030	29
2929404	São Miguel das Matas	216	290003	29
2929503	São Sebastião do Passé	20	290001	29
2929602	Sapeaçu	\N	290004	29
2929701	Sátiro Dias	31	290002	29
2929750	Saubara	20	290001	29
2929800	Saúde	246	290030	29
2929909	Seabra	219	290034	29
2930006	Sebastião Laranjeiras	313	290016	29
2930105	Senhor do Bonfim	246	290023	29
2930154	Serra do Ramalho	520	290017	29
2930204	Sento Sé	644	290022	29
2930303	Serra Dourada	520	290018	29
2930402	Serra Preta	\N	290029	29
2930501	Serrinha	793	290033	29
2930600	Serrolândia	246	290030	29
2930709	Simões Filho	20	290001	29
2930758	Sítio do Mato	520	290017	29
2930766	Sítio do Quinto	15	290028	29
2930774	Sobradinho	463	290022	29
2930808	Souto Soares	219	290034	29
2930907	Tabocas do Brejo Velho	256	290018	29
2931004	Tanhaçu	\N	290011	29
2931053	Tanque Novo	\N	290016	29
2931103	Tanquinho	\N	290029	29
2931202	Taperoá	216	290005	29
2931301	Tapiramutá	244	290030	29
2931350	Teixeira de Freitas	124	290008	29
2931400	Teodoro Sampaio	31	290002	29
2931509	Teofilândia	149	290033	29
2931608	Teolândia	149	290014	29
2931707	Terra Nova	20	290001	29
2931806	Tremedal	313	290011	29
2931905	Tucano	15	290025	29
2932002	Uauá	15	290022	29
2932101	Ubaíra	\N	290003	29
2932200	Ubaitaba	149	290007	29
2932309	Ubatã	149	290014	29
2932408	Uibaí	\N	290020	29
2932457	Umburanas	246	290030	29
2932507	Una	344	290010	29
2932606	Urandi	\N	290016	29
2932705	Uruçuca	149	290007	29
2932804	Utinga	244	290031	29
2932903	Valença	216	290005	29
2933000	Valente	81	290032	29
2933059	Várzea da Roça	\N	290029	29
2933109	Várzea do Poço	244	290030	29
2933158	Várzea Nova	246	290030	29
2933174	Varzedo	\N	290003	29
2933208	Vera Cruz	\N	290006	29
2933257	Vereda	124	290008	29
2933307	Vitória da Conquista	232	290011	29
2933406	Wagner	244	290031	29
2933455	Wanderley	256	290018	29
2933505	Wenceslau Guimarães	149	290014	29
2933604	Xique-Xique	431	290021	29
3100104	Abadia dos Dourados	75	310061	31
3100203	Abaeté	11	310070	31
3100302	Abre Campo	21	310028	31
3100401	Acaiaca	21	310030	31
3100500	Açucena	48	310024	31
3100609	Água Boa	72	310014	31
3100708	Água Comprida	78	310055	31
3100807	Aguanil	89	310048	31
3100906	Águas Formosas	95	310019	31
3101003	Águas Vermelhas	98	310018	31
3101102	Aimorés	48	310023	31
3101201	Aiuruoca	108	310054	31
3101300	Alagoa	108	310053	31
3101409	Albertina	\N	310050	31
3101508	Além Paraíba	\N	310036	31
3101607	Alfenas	142	310042	31
3101631	Alfredo Vasconcelos	\N	310037	31
3101706	Almenara	152	310015	31
3101805	Alpercata	48	310020	31
3101904	Alpinópolis	\N	310041	31
3102001	Alterosa	\N	310042	31
3102050	Alto Caparaó	\N	310028	31
3102100	Alto Rio Doce	\N	310037	31
3102209	Alvarenga	\N	310025	31
3102308	Alvinópolis	41	310030	31
3102407	Alvorada de Minas	203	310016	31
3102506	Amparo do Serra	\N	310030	31
3102605	Andradas	\N	310051	31
3102704	Cachoeira de Pajeú	\N	310018	31
3102803	Andrelândia	\N	310027	31
3102852	Angelândia	203	310014	31
3102902	Antônio Carlos	\N	310037	31
3103009	Antônio Dias	41	310024	31
3103108	Antônio Prado de Minas	\N	310031	31
3103207	Araçaí	\N	310002	31
3103306	Aracitaba	\N	310027	31
3103405	Araçuaí	98	310017	31
3103504	Araguari	321	310059	31
3103603	Arantina	\N	310027	31
3103702	Araponga	\N	310033	31
3103751	Araporã	321	310059	31
3103801	Arapuá	\N	310062	31
3103900	Araújos	\N	310065	31
3104007	Araxá	9	310056	31
3104106	Arceburgo	142	310044	31
3104205	Arcos	\N	310066	31
3104304	Areado	\N	310042	31
3104403	Argirita	\N	310032	31
3104452	Aricanduva	\N	310014	31
3104502	Arinos	364	310063	31
3104601	Astolfo Dutra	\N	310032	31
3104700	Ataléia	\N	310013	31
3104809	Augusto de Lima	\N	310004	31
3104908	Baependi	\N	310054	31
3105004	Baldim	\N	310002	31
3105103	Bambuí	11	310066	31
3105202	Bandeira	152	310015	31
3105301	Bandeira do Sul	\N	310051	31
3105400	Barão de Cocais	21	310003	31
3105509	Barão de Monte Alto	\N	310031	31
3105608	Barbacena	\N	310037	31
3105707	Barra Longa	21	310030	31
3105905	Barroso	\N	310037	31
3106002	Bela Vista de Minas	\N	310026	31
3106101	Belmiro Braga	\N	310027	31
3106200	Belo Horizonte	486	310001	31
3106309	Belo Oriente	41	310024	31
3106408	Belo Vale	\N	310038	31
3106507	Berilo	497	310017	31
3106606	Bertópolis	95	310019	31
3106655	Berizal	497	310008	31
3106705	Betim	486	310001	31
3106804	Bias Fortes	\N	310027	31
3106903	Bicas	\N	310035	31
3107000	Biquinhas	\N	310070	31
3107109	Boa Esperança	\N	310046	31
3107208	Bocaina de Minas	\N	310027	31
3107307	Bocaiúva	497	310006	31
3107406	Bom Despacho	\N	310067	31
3107505	Bom Jardim de Minas	\N	310027	31
3107604	Bom Jesus da Penha	142	310041	31
3107703	Bom Jesus do Amparo	\N	310005	31
3107802	Bom Jesus do Galho	177	310025	31
3107901	Bom Repouso	\N	310050	31
3108008	Bom Sucesso	89	310043	31
3108107	Bonfim	\N	310069	31
3108206	Bonfinópolis de Minas	364	310063	31
3108255	Bonito de Minas	536	310009	31
3108305	Borda da Mata	\N	310050	31
3108404	Botelhos	142	310051	31
3108503	Botumirim	497	310006	31
3108552	Brasilândia de Minas	364	310062	31
3108602	Brasília de Minas	497	310006	31
3108701	Brás Pires	\N	310029	31
3108800	Braúnas	72	310024	31
3108909	Brazópolis	\N	310052	31
3109006	Brumadinho	486	310001	31
3109105	Bueno Brandão	\N	310050	31
3109204	Buenópolis	\N	310004	31
3109253	Bugre	177	310024	31
3109303	Buritis	364	310063	31
3109402	Buritizeiro	185	310010	31
3109451	Cabeceira Grande	\N	310063	31
3109501	Cabo Verde	142	310044	31
3109600	Cachoeira da Prata	\N	310002	31
3109709	Cachoeira de Minas	\N	310050	31
3109808	Cachoeira Dourada	\N	310060	31
3109907	Caetanópolis	\N	310002	31
3110004	Caeté	486	310001	31
3110103	Caiana	\N	310034	31
3110202	Cajuri	\N	310033	31
3110301	Caldas	117	310051	31
3110400	Camacho	\N	310065	31
3110509	Camanducaia	\N	310050	31
3110608	Cambuí	117	310050	31
3110707	Cambuquira	108	310045	31
3110806	Campanário	95	310013	31
3110905	Campanha	108	310045	31
3111002	Campestre	142	310051	31
3111101	Campina Verde	140	310059	31
3111150	Campo Azul	497	310006	31
3111200	Campo Belo	\N	310048	31
3111309	Campo do Meio	108	310042	31
3111408	Campo Florido	78	310055	31
3111507	Campos Altos	11	310056	31
3111606	Campos Gerais	108	310042	31
3111705	Canaã	\N	310033	31
3111804	Canápolis	140	310059	31
3111903	Cana Verde	\N	310043	31
3112000	Candeias	\N	310048	31
3112059	Cantagalo	\N	310021	31
3112109	Caparaó	\N	310028	31
3112208	Capela Nova	\N	310038	31
3112307	Capelinha	203	310014	31
3112406	Capetinga	\N	310041	31
3112505	Capim Branco	\N	310002	31
3112604	Capinópolis	140	310060	31
3112653	Capitão Andrade	\N	310020	31
3112703	Capitão Enéas	497	310006	31
3112802	Capitólio	\N	310049	31
3112901	Caputira	\N	310028	31
3113008	Caraí	\N	310013	31
3113107	Caranaíba	\N	310038	31
3113206	Carandaí	\N	310038	31
3113305	Carangola	177	310034	31
3113404	Caratinga	177	310025	31
3113503	Carbonita	\N	310016	31
3113602	Careaçu	\N	310050	31
3113701	Carlos Chagas	95	310013	31
3113800	Carmésia	72	310005	31
3113909	Carmo da Cachoeira	108	310045	31
3114006	Carmo da Mata	89	310065	31
3114105	Carmo de Minas	108	310053	31
3114204	Carmo do Cajuru	350	310065	31
3114303	Carmo do Paranaíba	\N	310062	31
3114402	Carmo do Rio Claro	142	310041	31
3114501	Carmópolis de Minas	\N	310069	31
3114550	Carneirinho	140	310058	31
3114600	Carrancas	\N	310043	31
3114709	Carvalhópolis	\N	310042	31
3114808	Carvalhos	\N	310054	31
3114907	Casa Grande	\N	310038	31
3115003	Cascalho Rico	321	310059	31
3115102	Cássia	142	310041	31
3115201	Conceição da Barra de Minas	\N	310039	31
3115300	Cataguases	\N	310032	31
3115359	Catas Altas	21	310003	31
3115409	Catas Altas da Noruega	\N	310038	31
3115458	Catuji	\N	310013	31
3115474	Catuti	\N	310012	31
3115508	Caxambu	\N	310054	31
3115607	Cedro do Abaeté	\N	310070	31
3115706	Central de Minas	\N	310022	31
3115805	Centralina	\N	310059	31
3115904	Chácara	\N	310027	31
3116001	Chalé	\N	310028	31
3116100	Chapada do Norte	98	310014	31
3116159	Chapada Gaúcha	536	310011	31
3116209	Chiador	315	310027	31
3116308	Cipotânea	\N	310037	31
3116407	Claraval	675	310041	31
3116506	Claro dos Poções	497	310006	31
3116605	Cláudio	350	310065	31
3116704	Coimbra	\N	310033	31
3116803	Coluna	\N	310021	31
3116902	Comendador Gomes	78	310057	31
3117009	Comercinho	\N	310018	31
3117108	Conceição da Aparecida	142	310042	31
3117207	Conceição das Pedras	\N	310052	31
3117306	Conceição das Alagoas	78	310055	31
3117405	Conceição de Ipanema	\N	310028	31
3117504	Conceição do Mato Dentro	72	310002	31
3117603	Conceição do Pará	\N	310065	31
3117702	Conceição do Rio Verde	\N	310053	31
3117801	Conceição dos Ouros	\N	310050	31
3117836	Cônego Marinho	536	310009	31
3117876	Confins	\N	310001	31
3117900	Congonhal	\N	310050	31
3118007	Congonhas	117	310038	31
3118106	Congonhas do Norte	\N	310002	31
3118205	Conquista	\N	310055	31
3118304	Conselheiro Lafaiete	\N	310038	31
3118403	Conselheiro Pena	48	310020	31
3118502	Consolação	\N	310050	31
3118601	Contagem	486	310001	31
3118700	Coqueiral	\N	310046	31
3118809	Coração de Jesus	497	310006	31
3118908	Cordisburgo	\N	310002	31
3119005	Cordislândia	\N	310040	31
3119104	Corinto	\N	310004	31
3119203	Coroaci	\N	310020	31
3119302	Coromandel	9	310064	31
3119401	Coronel Fabriciano	41	310024	31
3119500	Coronel Murta	98	310017	31
3119609	Coronel Pacheco	315	310027	31
3119708	Coronel Xavier Chaves	\N	310039	31
3119807	Córrego Danta	11	310066	31
3119906	Córrego do Bom Jesus	\N	310050	31
3119955	Córrego Fundo	\N	310066	31
3120003	Córrego Novo	177	310025	31
3120102	Couto de Magalhães de Minas	\N	310016	31
3120151	Crisólita	\N	310019	31
3120201	Cristais	\N	310048	31
3120300	Cristália	497	310006	31
3120409	Cristiano Otoni	\N	310038	31
3120508	Cristina	\N	310053	31
3120607	Crucilândia	\N	310069	31
3120706	Cruzeiro da Fortaleza	\N	310064	31
3120805	Cruzília	\N	310054	31
3120839	Cuparaque	\N	310023	31
3120870	Curral de Dentro	\N	310008	31
3120904	Curvelo	203	310004	31
3121001	Datas	\N	310016	31
3121100	Delfim Moreira	\N	310052	31
3121209	Delfinópolis	\N	310041	31
3121258	Delta	78	310055	31
3121308	Descoberto	\N	310035	31
3121407	Desterro de Entre Rios	\N	310038	31
3121506	Desterro do Melo	\N	310037	31
3121605	Diamantina	203	310016	31
3121704	Diogo de Vasconcelos	21	310030	31
3121803	Dionísio	41	310024	31
3121902	Divinésia	\N	310029	31
3122009	Divino	\N	310034	31
3122108	Divino das Laranjeiras	\N	310020	31
3122207	Divinolândia de Minas	\N	310021	31
3122306	Divinópolis	350	310065	31
3122355	Divisa Alegre	\N	310018	31
3122405	Divisa Nova	\N	310042	31
3122454	Divisópolis	\N	310018	31
3122470	Dom Bosco	364	310063	31
3122504	Dom Cavati	\N	310024	31
3122603	Dom Joaquim	\N	310021	31
3122702	Dom Silvério	\N	310030	31
3122801	Dom Viçoso	\N	310053	31
3122900	Dona Eusébia	\N	310032	31
3123007	Dores de Campos	\N	310037	31
3123106	Dores de Guanhães	\N	310021	31
3123205	Dores do Indaiá	\N	310067	31
3123304	Dores do Turvo	\N	310029	31
3123403	Doresópolis	\N	310049	31
3123502	Douradoquara	\N	310061	31
3123528	Durandé	\N	310028	31
3123601	Elói Mendes	\N	310040	31
3123700	Engenheiro Caldas	\N	310020	31
3123809	Engenheiro Navarro	497	310006	31
3123858	Entre Folhas	\N	310025	31
3123908	Entre Rios de Minas	\N	310038	31
3124005	Ervália	21	310033	31
3124104	Esmeraldas	486	310001	31
3124203	Espera Feliz	\N	310034	31
3124302	Espinosa	536	310012	31
3124401	Espírito Santo do Dourado	\N	310050	31
3124500	Estiva	117	310050	31
3124609	Estrela Dalva	\N	310036	31
3124708	Estrela do Indaiá	\N	310067	31
3124807	Estrela do Sul	\N	310061	31
3124906	Eugenópolis	137	310031	31
3125002	Ewbank da Câmara	\N	310027	31
3125101	Extrema	\N	310050	31
3125200	Fama	\N	310042	31
3125309	Faria Lemos	\N	310034	31
3125408	Felício dos Santos	\N	310016	31
3125507	São Gonçalo do Rio Preto	\N	310016	31
3125606	Felisburgo	152	310015	31
3125705	Felixlândia	\N	310004	31
3125804	Fernandes Tourinho	48	310020	31
3125903	Ferros	\N	310005	31
3125952	Fervedouro	\N	310031	31
3126000	Florestal	\N	310001	31
3126109	Formiga	\N	310066	31
3126208	Formoso	364	310063	31
3126307	Fortaleza de Minas	\N	310041	31
3126406	Fortuna de Minas	\N	310002	31
3126505	Francisco Badaró	98	310017	31
3126604	Francisco Dumont	497	310006	31
3126703	Francisco Sá	497	310006	31
3126752	Franciscópolis	\N	310013	31
3126802	Frei Gaspar	\N	310013	31
3126901	Frei Inocêncio	95	310020	31
3126950	Frei Lagonegro	\N	310021	31
3127008	Fronteira	\N	310057	31
3127057	Fronteira dos Vales	\N	310019	31
3127073	Fruta de Leite	497	310008	31
3127107	Frutal	78	310057	31
3127206	Funilândia	150	310002	31
3127305	Galiléia	48	310020	31
3127339	Gameleiras	\N	310012	31
3127354	Glaucilândia	\N	310006	31
3127370	Goiabeira	\N	310020	31
3127388	Goianá	315	310027	31
3127404	Gonçalves	\N	310052	31
3127503	Gonzaga	48	310020	31
3127602	Gouveia	\N	310016	31
3127701	Governador Valadares	48	310020	31
3127800	Grão Mogol	497	310006	31
3127909	Grupiara	\N	310061	31
3128006	Guanhães	72	310021	31
3128105	Guapé	777	310041	31
3128204	Guaraciaba	21	310030	31
3128253	Guaraciama	497	310006	31
3128303	Guaranésia	142	310044	31
3128402	Guarani	\N	310029	31
3128501	Guarará	\N	310035	31
3128600	Guarda-Mor	\N	310062	31
3128709	Guaxupé	142	310044	31
3128808	Guidoval	\N	310029	31
3128907	Guimarânia	\N	310064	31
3129004	Guiricema	\N	310029	31
3129103	Gurinhatã	140	310060	31
3129202	Heliodora	\N	310050	31
3129301	Iapu	177	310024	31
3129400	Ibertioga	\N	310037	31
3129509	Ibiá	75	310056	31
3129608	Ibiaí	497	310010	31
3129657	Ibiracatu	\N	310006	31
3129707	Ibiraci	675	310041	31
3129806	Ibirité	486	310001	31
3129905	Ibitiúra de Minas	\N	310051	31
3130002	Ibituruna	\N	310043	31
3130051	Icaraí de Minas	536	310011	31
3130101	Igarapé	\N	310001	31
3130200	Igaratinga	\N	310068	31
3130309	Iguatama	\N	310066	31
3130408	Ijaci	\N	310043	31
3130507	Ilicínea	\N	310046	31
3130556	Imbé de Minas	\N	310025	31
3130606	Inconfidentes	\N	310050	31
3130655	Indaiabira	497	310008	31
3130705	Indianópolis	\N	310059	31
3130804	Ingaí	\N	310043	31
3130903	Inhapim	\N	310025	31
3131000	Inhaúma	\N	310002	31
3131109	Inimutaba	\N	310004	31
3131158	Ipaba	177	310024	31
3131208	Ipanema	\N	310028	31
3131307	Ipatinga	41	310024	31
3131406	Ipiaçu	140	310060	31
3131505	Ipuiúna	\N	310050	31
3131604	Iraí de Minas	\N	310061	31
3131703	Itabira	\N	310005	31
3131802	Itabirinha	\N	310022	31
3131901	Itabirito	21	310003	31
3132008	Itacambira	\N	310006	31
3132107	Itacarambi	536	310009	31
3132206	Itaguara	\N	310069	31
3132305	Itaipé	\N	310013	31
3132404	Itajubá	\N	310052	31
3132503	Itamarandiba	203	310014	31
3132602	Itamarati de Minas	\N	310032	31
3132701	Itambacuri	95	310013	31
3132800	Itambé do Mato Dentro	41	310005	31
3132909	Itamogi	\N	310047	31
3133006	Itamonte	\N	310053	31
3133105	Itanhandu	\N	310053	31
3133204	Itanhomi	\N	310020	31
3133303	Itaobim	98	310013	31
3133402	Itapagipe	\N	310057	31
3133501	Itapecerica	\N	310065	31
3133600	Itapeva	\N	310050	31
3133709	Itatiaiuçu	350	310065	31
3133758	Itaú de Minas	\N	310041	31
3133808	Itaúna	\N	310065	31
3133907	Itaverava	\N	310038	31
3134004	Itinga	98	310017	31
3134103	Itueta	48	310023	31
3134202	Ituiutaba	140	310060	31
3134301	Itumirim	\N	310043	31
3134400	Iturama	140	310058	31
3134509	Itutinga	\N	310043	31
3134608	Jaboticatubas	150	310001	31
3134707	Jacinto	\N	310015	31
3134806	Jacuí	142	310047	31
3134905	Jacutinga	\N	310050	31
3135001	Jaguaraçu	\N	310024	31
3135050	Jaíba	536	310007	31
3135076	Jampruca	95	310020	31
3135100	Janaúba	497	310007	31
3135209	Januária	536	310009	31
3135308	Japaraíba	\N	310065	31
3135357	Japonvar	497	310006	31
3135407	Jeceaba	\N	310038	31
3135456	Jenipapo de Minas	98	310017	31
3135506	Jequeri	\N	310030	31
3135605	Jequitaí	497	310006	31
3135704	Jequitibá	\N	310002	31
3135803	Jequitinhonha	152	310015	31
3135902	Jesuânia	\N	310053	31
3136009	Joaíma	152	310015	31
3136108	Joanésia	72	310024	31
3136207	João Monlevade	\N	310026	31
3136306	João Pinheiro	364	310062	31
3136405	Joaquim Felício	\N	310006	31
3136504	Jordânia	152	310015	31
3136520	José Gonçalves de Minas	98	310017	31
3136553	José Raydan	\N	310021	31
3136579	Josenópolis	497	310006	31
3136603	Nova União	486	310001	31
3136652	Juatuba	350	310001	31
3136702	Juiz de Fora	315	310027	31
3136801	Juramento	\N	310006	31
3136900	Juruaia	\N	310044	31
3136959	Juvenília	536	310009	31
3137007	Ladainha	95	310013	31
3137106	Lagamar	\N	310062	31
3137205	Lagoa da Prata	11	310065	31
3137304	Lagoa dos Patos	497	310006	31
3137403	Lagoa Dourada	\N	310039	31
3137502	Lagoa Formosa	\N	310062	31
3137536	Lagoa Grande	9	310062	31
3137601	Lagoa Santa	\N	310001	31
3137700	Lajinha	\N	310028	31
3137809	Lambari	\N	310053	31
3137908	Lamim	\N	310038	31
3138005	Laranjal	137	310032	31
3138104	Lassance	203	310010	31
3138203	Lavras	\N	310043	31
3138302	Leandro Ferreira	\N	310065	31
3138351	Leme do Prado	98	310014	31
3138401	Leopoldina	137	310032	31
3138500	Liberdade	\N	310027	31
3138609	Lima Duarte	\N	310027	31
3138625	Limeira do Oeste	140	310058	31
3138658	Lontra	\N	310006	31
3138674	Luisburgo	\N	310028	31
3138682	Luislândia	\N	310006	31
3138708	Luminárias	\N	310043	31
3138807	Luz	11	310067	31
3138906	Machacalis	95	310019	31
3139003	Machado	117	310042	31
3139102	Madre de Deus de Minas	\N	310039	31
3139201	Malacacheta	\N	310013	31
3139250	Mamonas	\N	310012	31
3139300	Manga	536	310007	31
3139409	Manhuaçu	177	310028	31
3139508	Manhumirim	\N	310028	31
3139607	Mantena	\N	310022	31
3139706	Maravilhas	\N	310068	31
3139805	Mar de Espanha	315	310035	31
3139904	Maria da Fé	\N	310052	31
3140001	Mariana	21	310003	31
3140100	Marilac	\N	310020	31
3140159	Mário Campos	486	310001	31
3140209	Maripá de Minas	\N	310035	31
3140308	Marliéria	41	310024	31
3140407	Marmelópolis	\N	310052	31
3140506	Martinho Campos	\N	310067	31
3140530	Martins Soares	\N	310028	31
3140555	Mata Verde	\N	310015	31
3140605	Materlândia	\N	310021	31
3140704	Mateus Leme	350	310001	31
3140803	Matias Barbosa	\N	310027	31
3140852	Matias Cardoso	536	310007	31
3140902	Matipó	\N	310028	31
3141009	Mato Verde	\N	310012	31
3141108	Matozinhos	\N	310002	31
3141207	Matutina	\N	310062	31
3141306	Medeiros	\N	310066	31
3141405	Medina	\N	310018	31
3141504	Mendes Pimentel	\N	310022	31
3141603	Mercês	\N	310029	31
3141702	Mesquita	\N	310024	31
3141801	Minas Novas	98	310014	31
3141900	Minduri	\N	310054	31
3142007	Mirabela	\N	310006	31
3142106	Miradouro	137	310031	31
3142205	Miraí	137	310031	31
3142254	Miravânia	536	310007	31
3142304	Moeda	\N	310001	31
3142403	Moema	\N	310067	31
3142502	Monjolos	\N	310004	31
3142601	Monsenhor Paulo	\N	310040	31
3142700	Montalvânia	536	310009	31
3142809	Monte Alegre de Minas	321	310059	31
3142908	Monte Azul	\N	310012	31
3143005	Monte Belo	142	310044	31
3143104	Monte Carmelo	\N	310061	31
3143153	Monte Formoso	\N	310013	31
3143203	Monte Santo de Minas	\N	310047	31
3143302	Montes Claros	497	310006	31
3143401	Monte Sião	\N	310050	31
3143450	Montezuma	497	310012	31
3143500	Morada Nova de Minas	\N	310070	31
3143609	Morro da Garça	\N	310004	31
3143708	Morro do Pilar	72	310002	31
3143807	Munhoz	\N	310050	31
3143906	Muriaé	137	310031	31
3144003	Mutum	\N	310028	31
3144102	Muzambinho	142	310044	31
3144201	Nacip Raydan	\N	310020	31
3144300	Nanuque	\N	310013	31
3144359	Naque	48	310024	31
3144375	Natalândia	\N	310063	31
3144409	Natércia	\N	310050	31
3144508	Nazareno	\N	310039	31
3144607	Nepomuceno	486	310043	31
3144656	Ninheira	497	310008	31
3144672	Nova Belém	95	310022	31
3144706	Nova Era	41	310026	31
3144805	Nova Lima	486	310001	31
3144904	Nova Módica	\N	310013	31
3145000	Nova Ponte	\N	310055	31
3145059	Nova Porteirinha	784	310007	31
3145109	Nova Resende	142	310044	31
3145208	Nova Serrana	\N	310065	31
3145307	Novo Cruzeiro	98	310013	31
3145356	Novo Oriente de Minas	\N	310013	31
3145372	Novorizonte	497	310008	31
3145406	Olaria	\N	310027	31
3145455	Olhos-d'Água	\N	310006	31
3145505	Olímpio Noronha	\N	310053	31
3145604	Oliveira	89	310069	31
3145703	Oliveira Fortes	\N	310027	31
3145802	Onça de Pitangui	\N	310068	31
3145851	Oratórios	\N	310030	31
3145877	Orizânia	\N	310034	31
3145901	Ouro Branco	\N	310038	31
3146008	Ouro Fino	\N	310050	31
3146107	Ouro Preto	21	310003	31
3146206	Ouro Verde de Minas	95	310013	31
3146255	Padre Carvalho	\N	310008	31
3146305	Padre Paraíso	\N	310013	31
3146404	Paineiras	\N	310070	31
3146503	Pains	\N	310066	31
3146552	Pai Pedro	\N	310007	31
3146602	Paiva	\N	310027	31
3146701	Palma	\N	310032	31
3146750	Palmópolis	152	310015	31
3146909	Papagaios	\N	310068	31
3147006	Paracatu	364	310062	31
3147105	Pará de Minas	350	310068	31
3147204	Paraguaçu	142	310042	31
3147303	Paraisópolis	\N	310052	31
3147402	Paraopeba	150	310002	31
3147501	Passabém	\N	310005	31
3147600	Passa Quatro	108	310053	31
3147709	Passa Tempo	\N	310069	31
3147808	Passa-Vinte	\N	310027	31
3147907	Passos	\N	310041	31
3147956	Patis	\N	310006	31
3148004	Patos de Minas	9	310062	31
3148103	Patrocínio	9	310064	31
3148202	Patrocínio do Muriaé	\N	310031	31
3148301	Paula Cândido	21	310033	31
3148400	Paulistas	\N	310021	31
3148509	Pavão	95	310013	31
3148608	Peçanha	\N	310021	31
3148707	Pedra Azul	98	310018	31
3148756	Pedra Bonita	\N	310034	31
3148806	Pedra do Anta	\N	310033	31
3148905	Pedra do Indaiá	\N	310065	31
3149002	Pedra Dourada	\N	310034	31
3149101	Pedralva	\N	310052	31
3149150	Pedras de Maria da Cruz	536	310009	31
3149200	Pedrinópolis	\N	310056	31
3149309	Pedro Leopoldo	486	310001	31
3149408	Pedro Teixeira	\N	310027	31
3149507	Pequeri	\N	310035	31
3149606	Pequi	150	310068	31
3149705	Perdigão	\N	310065	31
3149804	Perdizes	9	310056	31
3149903	Perdões	\N	310043	31
3149952	Periquito	48	310024	31
3150000	Pescador	\N	310013	31
3150109	Piau	\N	310027	31
3150158	Piedade de Caratinga	177	310025	31
3150208	Piedade de Ponte Nova	\N	310030	31
3150307	Piedade do Rio Grande	\N	310039	31
3150406	Piedade dos Gerais	\N	310069	31
3150505	Pimenta	\N	310066	31
3150539	Pingo-d'Água	\N	310024	31
3150570	Pintópolis	536	310011	31
3150604	Piracema	\N	310069	31
3150703	Pirajuba	78	310057	31
3150802	Piranga	\N	310038	31
3150901	Piranguçu	\N	310052	31
3151008	Piranguinho	\N	310052	31
3151107	Pirapetinga	137	310036	31
3151206	Pirapora	203	310010	31
3151305	Piraúba	\N	310029	31
3151404	Pitangui	\N	310065	31
3151503	Piumhi	\N	310049	31
3151602	Planura	\N	310057	31
3151701	Poço Fundo	117	310042	31
3151800	Poços de Caldas	\N	310051	31
3151909	Pocrane	177	310028	31
3152006	Pompéu	150	310067	31
3152105	Ponte Nova	21	310030	31
3152131	Ponto Chique	\N	310010	31
3152170	Ponto dos Volantes	\N	310013	31
3152204	Porteirinha	497	310007	31
3152303	Porto Firme	21	310033	31
3152402	Poté	\N	310013	31
3152501	Pouso Alegre	117	310050	31
3152600	Pouso Alto	\N	310053	31
3152709	Prados	\N	310039	31
3152808	Prata	78	310059	31
3152907	Pratápolis	\N	310041	31
3153004	Pratinha	\N	310056	31
3153103	Presidente Bernardes	21	310033	31
3153202	Presidente Juscelino	\N	310004	31
3153301	Presidente Kubitschek	203	310016	31
3153400	Presidente Olegário	9	310062	31
3153509	Alto Jequitibá	\N	310028	31
3153608	Prudente de Morais	\N	310002	31
3153707	Quartel Geral	\N	310067	31
3153806	Queluzito	\N	310038	31
3153905	Raposos	\N	310001	31
3154002	Raul Soares	177	310025	31
3154101	Recreio	137	310032	31
3154150	Reduto	\N	310028	31
3154200	Resende Costa	\N	310039	31
3154309	Resplendor	48	310023	31
3154408	Ressaquinha	\N	310037	31
3154457	Riachinho	364	310063	31
3154507	Riacho dos Machados	784	310007	31
3154606	Ribeirão das Neves	\N	310001	31
3154705	Ribeirão Vermelho	\N	310043	31
3154804	Rio Acima	\N	310001	31
3154903	Rio Casca	21	310030	31
3155009	Rio Doce	21	310030	31
3155108	Rio do Prado	\N	310015	31
3155207	Rio Espera	\N	310038	31
3155306	Rio Manso	486	310069	31
3155405	Rio Novo	\N	310027	31
3155504	Rio Paranaíba	\N	310062	31
3155603	Rio Pardo de Minas	497	310008	31
3155702	Rio Piracicaba	41	310026	31
3155801	Rio Pomba	\N	310029	31
3155900	Rio Preto	\N	310027	31
3156007	Rio Vermelho	72	310021	31
3156106	Ritápolis	\N	310039	31
3156205	Rochedo de Minas	\N	310035	31
3156304	Rodeiro	\N	310029	31
3156403	Romaria	\N	310061	31
3156452	Rosário da Limeira	\N	310031	31
3156502	Rubelita	\N	310008	31
3156601	Rubim	152	310015	31
3156700	Sabará	\N	310001	31
3156809	Sabinópolis	\N	310021	31
3156908	Sacramento	78	310055	31
3157005	Salinas	497	310008	31
3157104	Salto da Divisa	152	310015	31
3157203	Santa Bárbara	21	310003	31
3157252	Santa Bárbara do Leste	\N	310025	31
3157278	Santa Bárbara do Monte Verde	\N	310027	31
3157302	Santa Bárbara do Tugúrio	\N	310037	31
3157336	Santa Cruz de Minas	\N	310039	31
3157377	Santa Cruz de Salinas	\N	310008	31
3157401	Santa Cruz do Escalvado	21	310030	31
3157500	Santa Efigênia de Minas	\N	310020	31
3157609	Santa Fé de Minas	536	310010	31
3157658	Santa Helena de Minas	95	310019	31
3157708	Santa Juliana	78	310055	31
3157807	Santa Luzia	486	310001	31
3157906	Santa Margarida	\N	310028	31
3158003	Santa Maria de Itabira	\N	310005	31
3158102	Santa Maria do Salto	\N	310015	31
3158201	Santa Maria do Suaçuí	72	310021	31
3158300	Santana da Vargem	\N	310046	31
3158409	Santana de Cataguases	137	310032	31
3158508	Santana de Pirapama	\N	310002	31
3158607	Santana do Deserto	\N	310027	31
3158706	Santana do Garambéu	\N	310037	31
3158805	Santana do Jacaré	\N	310048	31
3158904	Santana do Manhuaçu	\N	310028	31
3158953	Santana do Paraíso	41	310024	31
3159001	Santana do Riacho	150	310002	31
3159100	Santana dos Montes	\N	310038	31
3159209	Santa Rita de Caldas	\N	310051	31
3159308	Santa Rita de Jacutinga	\N	310027	31
3159357	Santa Rita de Minas	\N	310025	31
3159407	Santa Rita de Ibitipoca	\N	310037	31
3159506	Santa Rita do Itueto	\N	310023	31
3159605	Santa Rita do Sapucaí	\N	310050	31
3159704	Santa Rosa da Serra	\N	310056	31
3159803	Santa Vitória	140	310060	31
3159902	Santo Antônio do Amparo	89	310043	31
3160009	Santo Antônio do Aventureiro	\N	310036	31
3160108	Santo Antônio do Grama	\N	310030	31
3160207	Santo Antônio do Itambé	\N	310016	31
3160306	Santo Antônio do Jacinto	\N	310015	31
3160405	Santo Antônio do Monte	\N	310065	31
3160454	Santo Antônio do Retiro	\N	310012	31
3160504	Santo Antônio do Rio Abaixo	\N	310005	31
3160603	Santo Hipólito	\N	310004	31
3160702	Santos Dumont	\N	310027	31
3160801	São Bento Abade	\N	310045	31
3160900	São Brás do Suaçuí	21	310038	31
3160959	São Domingos das Dores	\N	310025	31
3161007	São Domingos do Prata	41	310026	31
3161056	São Félix de Minas	\N	310022	31
3161106	São Francisco	536	310011	31
3161205	São Francisco de Paula	\N	310069	31
3161304	São Francisco de Sales	\N	310058	31
3161403	São Francisco do Glória	\N	310031	31
3161502	São Geraldo	\N	310029	31
3161601	São Geraldo da Piedade	\N	310020	31
3161650	São Geraldo do Baixio	\N	310020	31
3161700	São Gonçalo do Abaeté	\N	310062	31
3161809	São Gonçalo do Pará	\N	310065	31
3161908	São Gonçalo do Rio Abaixo	\N	310026	31
3162005	São Gonçalo do Sapucaí	\N	310040	31
3162104	São Gotardo	9	310062	31
3162203	São João Batista do Glória	\N	310041	31
3162252	São João da Lagoa	\N	310006	31
3162302	São João da Mata	\N	310050	31
3162401	São João da Ponte	497	310006	31
3162450	São João das Missões	536	310009	31
3162500	São João del Rei	251	310039	31
3162559	São João do Manhuaçu	177	310028	31
3162575	São João do Manteninha	\N	310022	31
3162609	São João do Oriente	\N	310024	31
3162658	São João do Pacuí	\N	310006	31
3162708	São João do Paraíso	497	310008	31
3162807	São João Evangelista	\N	310021	31
3162906	São João Nepomuceno	\N	310035	31
3162922	São Joaquim de Bicas	350	310001	31
3162948	São José da Barra	\N	310041	31
3162955	São José da Lapa	\N	310001	31
3163003	São José da Safira	72	310020	31
3163102	São José da Varginha	\N	310068	31
3163201	São José do Alegre	\N	310052	31
3163300	São José do Divino	\N	310013	31
3163409	São José do Goiabal	41	310024	31
3163508	São José do Jacuri	72	310021	31
3163607	São José do Mantimento	\N	310028	31
3163706	São Lourenço	108	310053	31
3163805	São Miguel do Anta	\N	310033	31
3163904	São Pedro da União	\N	310044	31
3164001	São Pedro dos Ferros	21	310030	31
3164100	São Pedro do Suaçuí	\N	310021	31
3164209	São Romão	536	310011	31
3164308	São Roque de Minas	11	310049	31
3164407	São Sebastião da Bela Vista	\N	310050	31
3164431	São Sebastião da Vargem Alegre	\N	310031	31
3164472	São Sebastião do Anta	\N	310025	31
3164506	São Sebastião do Maranhão	\N	310021	31
3164605	São Sebastião do Oeste	\N	310065	31
3164704	São Sebastião do Paraíso	142	310047	31
3164803	São Sebastião do Rio Preto	\N	310005	31
3164902	São Sebastião do Rio Verde	\N	310053	31
3165008	São Tiago	\N	310039	31
3165107	São Tomás de Aquino	\N	310047	31
3165206	São Thomé das Letras	\N	310045	31
3165305	São Vicente de Minas	\N	310039	31
3165404	Sapucaí-Mirim	\N	310052	31
3165503	Sardoá	\N	310020	31
3165537	Sarzedo	\N	310001	31
3165552	Setubinha	\N	310013	31
3165560	Sem-Peixe	\N	310030	31
3165578	Senador Amaral	117	310050	31
3165602	Senador Cortes	\N	310035	31
3165701	Senador Firmino	\N	310029	31
3165800	Senador José Bento	\N	310050	31
3165909	Senador Modestino Gonçalves	\N	310016	31
3166006	Senhora de Oliveira	\N	310038	31
3166105	Senhora do Porto	\N	310021	31
3166204	Senhora dos Remédios	\N	310037	31
3166303	Sericita	\N	310030	31
3166402	Seritinga	\N	310054	31
3166501	Serra Azul de Minas	\N	310016	31
3166600	Serra da Saudade	\N	310067	31
3166709	Serra dos Aimorés	\N	310013	31
3166808	Serra do Salitre	9	310064	31
3166907	Serrania	\N	310042	31
3166956	Serranópolis de Minas	497	310007	31
3167004	Serranos	\N	310054	31
3167103	Serro	486	310016	31
3167202	Sete Lagoas	150	310002	31
3167301	Silveirânia	\N	310029	31
3167400	Silvianópolis	117	310050	31
3167509	Simão Pereira	315	310027	31
3167608	Simonésia	177	310028	31
3167707	Sobrália	48	310020	31
3167806	Soledade de Minas	\N	310053	31
3167905	Tabuleiro	\N	310029	31
3168002	Taiobeiras	497	310008	31
3168051	Taparuba	\N	310028	31
3168101	Tapira	78	310056	31
3168200	Tapiraí	\N	310066	31
3168309	Taquaraçu de Minas	\N	310001	31
3168408	Tarumirim	177	310020	31
3168507	Teixeiras	\N	310033	31
3168606	Teófilo Otoni	95	310013	31
3168705	Timóteo	41	310024	31
3168804	Tiradentes	\N	310039	31
3168903	Tiros	11	310062	31
3169000	Tocantins	137	310029	31
3169059	Tocos do Moji	\N	310050	31
3169109	Toledo	\N	310050	31
3169208	Tombos	177	310034	31
3169307	Três Corações	108	310045	31
3169356	Três Marias	203	310004	31
3169406	Três Pontas	108	310046	31
3169505	Tumiritinga	48	310020	31
3169604	Tupaciguara	321	310059	31
3169703	Turmalina	98	310014	31
3169802	Turvolândia	\N	310050	31
3169901	Ubá	\N	310029	31
3170008	Ubaí	\N	310011	31
3170057	Ubaporanga	\N	310025	31
3170107	Uberaba	78	310055	31
3170206	Uberlândia	321	310059	31
3170305	Umburatiba	\N	310019	31
3170404	Unaí	364	310063	31
3170438	União de Minas	140	310058	31
3170479	Uruana de Minas	364	310063	31
3170503	Urucânia	\N	310030	31
3170529	Urucuia	536	310063	31
3170578	Vargem Alegre	\N	310025	31
3170602	Vargem Bonita	\N	310049	31
3170651	Vargem Grande do Rio Pardo	784	310008	31
3170701	Varginha	108	310040	31
3170750	Varjão de Minas	9	310062	31
3170800	Várzea da Palma	203	310010	31
3170909	Varzelândia	497	310006	31
3171006	Vazante	364	310062	31
3171030	Verdelândia	497	310007	31
3171071	Veredinha	\N	310014	31
3171105	Veríssimo	78	310055	31
3171154	Vermelho Novo	\N	310025	31
3171204	Vespasiano	\N	310001	31
3171303	Viçosa	21	310033	31
3171402	Vieiras	\N	310031	31
3171501	Mathias Lobato	48	310020	31
3171600	Virgem da Lapa	98	310017	31
3171709	Virgínia	\N	310053	31
3171808	Virginópolis	\N	310021	31
3171907	Virgolândia	\N	310020	31
3172004	Visconde do Rio Branco	137	310029	31
3172103	Volta Grande	\N	310036	31
3172202	Wenceslau Braz	\N	310052	31
3200102	Afonso Cláudio	64	320002	32
3200136	Águia Branca	80	320005	32
3200169	Água Doce do Norte	\N	320006	32
3200201	Alegre	134	320008	32
3200300	Alfredo Chaves	\N	320001	32
3200359	Alto Rio Novo	\N	320005	32
3200409	Anchieta	64	320001	32
3200508	Apiacá	134	320008	32
3200607	Aracruz	316	320004	32
3200706	Atilio Vivacqua	\N	320007	32
3200805	Baixo Guandu	316	320005	32
3200904	Barra de São Francisco	80	320006	32
3201001	Boa Esperança	\N	320003	32
3201100	Bom Jesus do Norte	\N	320008	32
3201159	Brejetuba	64	320002	32
3201209	Cachoeiro de Itapemirim	\N	320007	32
3201308	Cariacica	64	320001	32
3201407	Castelo	\N	320007	32
3201506	Colatina	316	320005	32
3201605	Conceição da Barra	80	320003	32
3201704	Conceição do Castelo	\N	320002	32
3201803	Divino de São Lourenço	134	320008	32
3201902	Domingos Martins	64	320002	32
3202009	Dores do Rio Preto	\N	320008	32
3202108	Ecoporanga	80	320006	32
3202207	Fundão	64	320001	32
3202256	Governador Lindenberg	\N	320005	32
3202306	Guaçuí	134	320008	32
3202405	Guarapari	\N	320001	32
3202454	Ibatiba	\N	320008	32
3202504	Ibiraçu	\N	320004	32
3202553	Ibitirama	\N	320008	32
3202603	Iconha	134	320007	32
3202652	Irupi	\N	320008	32
3202702	Itaguaçu	\N	320005	32
3202801	Itapemirim	134	320007	32
3202900	Itarana	\N	320002	32
3203007	Iúna	\N	320008	32
3203056	Jaguaré	80	320003	32
3203106	Jerônimo Monteiro	\N	320007	32
3203130	João Neiva	316	320004	32
3203163	Laranja da Terra	\N	320002	32
3203205	Linhares	316	320004	32
3203304	Mantenópolis	\N	320005	32
3203320	Marataízes	\N	320007	32
3203346	Marechal Floriano	64	320002	32
3203353	Marilândia	\N	320005	32
3203403	Mimoso do Sul	134	320007	32
3203502	Montanha	80	320003	32
3203601	Mucurici	80	320003	32
3203700	Muniz Freire	134	320008	32
3203809	Muqui	134	320007	32
3203908	Nova Venécia	80	320006	32
3204005	Pancas	316	320005	32
3204054	Pedro Canário	80	320003	32
3204104	Pinheiros	80	320003	32
3204203	Piúma	134	320001	32
3204252	Ponto Belo	80	320003	32
3204302	Presidente Kennedy	134	320007	32
3204351	Rio Bananal	\N	320004	32
3204401	Rio Novo do Sul	\N	320007	32
3204500	Santa Leopoldina	64	320002	32
3204559	Santa Maria de Jetibá	\N	320002	32
3204609	Santa Teresa	316	320002	32
3204658	São Domingos do Norte	316	320005	32
3204708	São Gabriel da Palha	80	320005	32
3204807	São José do Calçado	134	320008	32
3204906	São Mateus	80	320003	32
3204955	São Roque do Canaã	\N	320005	32
3205002	Serra	64	320001	32
3205010	Sooretama	316	320004	32
3205036	Vargem Alta	\N	320007	32
3205069	Venda Nova do Imigrante	134	320002	32
3205101	Viana	64	320001	32
3205150	Vila Pavão	\N	320006	32
3205176	Vila Valério	80	320005	32
3205200	Vila Velha	64	320001	32
3205309	Vitória	64	320001	32
3300100	Angra dos Reis	259	330002	33
3300159	Aperibé	\N	330012	33
3300209	Araruama	341	330013	33
3300225	Areal	\N	330007	33
3300233	Armação dos Búzios	341	330013	33
3300258	Arraial do Cabo	341	330013	33
3300308	Barra do Piraí	775	330004	33
3300407	Barra Mansa	759	330004	33
3300456	Belford Roxo	\N	330001	33
3300506	Bom Jardim	434	330008	33
3300605	Bom Jesus do Itabapoana	292	330011	33
3300704	Cabo Frio	341	330013	33
3300803	Cachoeiras de Macacu	434	330003	33
3300902	Cambuci	\N	330012	33
3300936	Carapebus	434	330014	33
3300951	Comendador Levy Gasparian	\N	330009	33
3301009	Campos dos Goytacazes	292	330010	33
3301108	Cantagalo	434	330008	33
3301157	Cardoso Moreira	292	330010	33
3301207	Carmo	\N	330008	33
3301306	Casimiro de Abreu	758	330014	33
3301405	Conceição de Macabu	434	330014	33
3301504	Cordeiro	\N	330008	33
3301603	Duas Barras	434	330008	33
3301702	Duque de Caxias	656	330001	33
3301801	Engenheiro Paulo de Frontin	\N	330004	33
3301850	Guapimirim	358	330001	33
3301876	Iguaba Grande	\N	330013	33
3301900	Itaboraí	341	330001	33
3302007	Itaguaí	259	330001	33
3302056	Italva	292	330010	33
3302106	Itaocara	\N	330012	33
3302205	Itaperuna	292	330011	33
3302254	Itatiaia	775	330005	33
3302270	Japeri	\N	330001	33
3302304	Laje do Muriaé	\N	330011	33
3302403	Macaé	434	330014	33
3302452	Macuco	434	330008	33
3302502	Magé	358	330001	33
3302601	Mangaratiba	259	330001	33
3302700	Maricá	341	330001	33
3302809	Mendes	\N	330004	33
3302858	Mesquita	\N	330001	33
3302908	Miguel Pereira	269	330006	33
3303005	Miracema	\N	330012	33
3303104	Natividade	292	330011	33
3303203	Nilópolis	\N	330001	33
3303302	Niterói	341	330001	33
3303401	Nova Friburgo	\N	330008	33
3303500	Nova Iguaçu	476	330001	33
3303609	Paracambi	476	330001	33
3303708	Paraíba do Sul	\N	330009	33
3303807	Paraty	\N	330002	33
3303856	Paty do Alferes	\N	330006	33
3303906	Petrópolis	358	330007	33
3303955	Pinheiral	775	330004	33
3304003	Piraí	775	330004	33
3304102	Porciúncula	\N	330011	33
3304110	Porto Real	\N	330005	33
3304128	Quatis	775	330005	33
3304144	Queimados	476	330001	33
3304151	Quissamã	434	330014	33
3304201	Resende	759	330005	33
3304300	Rio Bonito	\N	330003	33
3304409	Rio Claro	775	330004	33
3304508	Rio das Flores	\N	330006	33
3304524	Rio das Ostras	434	330014	33
3304557	Rio de Janeiro	660	330001	33
3304607	Santa Maria Madalena	434	330008	33
3304706	Santo Antônio de Pádua	292	330012	33
3304755	São Francisco de Itabapoana	\N	330010	33
3304805	São Fidélis	292	330010	33
3304904	São Gonçalo	341	330001	33
3305000	São João da Barra	292	330010	33
3305109	São João de Meriti	\N	330001	33
3305133	São José de Ubá	\N	330011	33
3305158	São José do Vale do Rio Preto	\N	330007	33
3305208	São Pedro da Aldeia	341	330013	33
3305307	São Sebastião do Alto	\N	330008	33
3305406	Sapucaia	269	330009	33
3305505	Saquarema	341	330001	33
3305554	Seropédica	259	330001	33
3305604	Silva Jardim	341	330003	33
3305703	Sumidouro	\N	330008	33
3305752	Tanguá	\N	330001	33
3305802	Teresópolis	\N	330007	33
3305901	Trajano de Moraes	\N	330008	33
3306008	Três Rios	\N	330009	33
3306107	Valença	269	330006	33
3306156	Varre-Sai	292	330011	33
3306206	Vassouras	\N	330006	33
3306305	Volta Redonda	775	330004	33
3500105	Adamantina	49	350019	35
3500204	Adolfo	59	350025	35
3500303	Aguaí	88	350044	35
3500402	Águas da Prata	88	350044	35
3500501	Águas de Lindóia	92	350048	35
3500550	Águas de Santa Bárbara	\N	350007	35
3500600	Águas de São Pedro	94	350040	35
3500709	Agudos	100	350009	35
3500758	Alambari	116	350006	35
3500808	Alfredo Marcondes	\N	350018	35
3500907	Altair	59	350025	35
3501004	Altinópolis	167	350031	35
3501103	Alto Alegre	\N	350023	35
3501152	Alumínio	\N	350003	35
3501202	Álvares Florence	\N	350027	35
3501301	Álvares Machado	144	350018	35
3501400	Álvaro de Carvalho	\N	350013	35
3501509	Alvinlândia	786	350013	35
3501608	Americana	220	350038	35
3501707	Américo Brasiliense	87	350036	35
3501806	Américo de Campos	\N	350027	35
3501905	Amparo	\N	350048	35
3502002	Analândia	\N	350046	35
3502101	Andradina	249	350024	35
3502200	Angatuba	699	350006	35
3502309	Anhembi	93	350011	35
3502408	Anhumas	\N	350018	35
3502507	Aparecida	679	350052	35
3502606	Aparecida d'Oeste	\N	350028	35
3502705	Apiaí	294	350004	35
3502754	Araçariguama	195	350003	35
3502804	Araçatuba	285	350022	35
3502903	Araçoiaba da Serra	116	350003	35
3503000	Aramina	\N	350035	35
3503109	Arandu	\N	350007	35
3503158	Arapeí	\N	350053	35
3503208	Araraquara	87	350036	35
3503307	Araras	220	350045	35
3503356	Arco-Íris	49	350016	35
3503406	Arealva	100	350009	35
3503505	Areias	\N	350053	35
3503604	Areiópolis	\N	350011	35
3503703	Ariranha	789	350026	35
3503802	Artur Nogueira	\N	350038	35
3503901	Arujá	383	350001	35
3503950	Aspásia	\N	350028	35
3504008	Assis	200	350014	35
3504107	Atibaia	393	350041	35
3504206	Auriflama	\N	350022	35
3504305	Avaí	100	350009	35
3504404	Avanhandava	171	350023	35
3504503	Avaré	93	350007	35
3504602	Bady Bassitt	\N	350025	35
3504701	Balbinos	\N	350009	35
3504800	Bálsamo	\N	350025	35
3504909	Bananal	\N	350053	35
3505005	Barão de Antonina	\N	350004	35
3505104	Barbosa	\N	350023	35
3505203	Bariri	87	350010	35
3505302	Barra Bonita	87	350010	35
3505351	Barra do Chapéu	\N	350004	35
3505401	Barra do Turvo	445	350005	35
3505500	Barretos	454	350032	35
3505609	Barrinha	83	350031	35
3505708	Barueri	195	350001	35
3505807	Bastos	\N	350016	35
3505906	Batatais	167	350031	35
3506003	Bauru	100	350009	35
3506102	Bebedouro	287	350032	35
3506201	Bento de Abreu	249	350022	35
3506300	Bernardino de Campos	786	350015	35
3506359	Bertioga	495	350002	35
3506409	Bilac	\N	350023	35
3506508	Birigui	171	350023	35
3506607	Biritiba-Mirim	\N	350001	35
3506706	Boa Esperança do Sul	87	350036	35
3506805	Bocaina	87	350010	35
3506904	Bofete	\N	350011	35
3507001	Boituva	116	350003	35
3507100	Bom Jesus dos Perdões	\N	350041	35
3507159	Bom Sucesso de Itararé	294	350004	35
3507209	Borá	200	350014	35
3507308	Boracéia	\N	350010	35
3507407	Borborema	\N	350036	35
3507456	Borebi	93	350009	35
3507506	Botucatu	93	350011	35
3507605	Bragança Paulista	393	350041	35
3507704	Braúna	\N	350023	35
3507753	Brejo Alegre	249	350023	35
3507803	Brodowski	167	350031	35
3507902	Brotas	87	350010	35
3508009	Buri	294	350004	35
3508108	Buritama	59	350023	35
3508207	Buritizal	\N	350035	35
3508306	Cabrália Paulista	100	350009	35
3508405	Cabreúva	551	350039	35
3508504	Caçapava	238	350049	35
3508603	Cachoeira Paulista	\N	350053	35
3508702	Caconde	88	350047	35
3508801	Cafelândia	171	350012	35
3508900	Caiabu	\N	350018	35
3509007	Caieiras	393	350001	35
3509106	Caiuá	144	350021	35
3509205	Cajamar	679	350001	35
3509254	Cajati	445	350005	35
3509304	Cajobi	\N	350032	35
3509403	Cajuru	167	350031	35
3509452	Campina do Monte Alegre	\N	350006	35
3509502	Campinas	92	350038	35
3509601	Campo Limpo Paulista	\N	350039	35
3509700	Campos do Jordão	413	350050	35
3509809	Campos Novos Paulista	\N	350013	35
3509908	Cananéia	445	350005	35
3509957	Canas	\N	350052	35
3510005	Cândido Mota	200	350014	35
3510104	Cândido Rodrigues	287	350036	35
3510153	Canitar	\N	350015	35
3510203	Capão Bonito	294	350004	35
3510302	Capela do Alto	\N	350003	35
3510401	Capivari	94	350040	35
3510500	Caraguatatuba	787	350051	35
3510609	Carapicuíba	\N	350001	35
3510708	Cardoso	285	350027	35
3510807	Casa Branca	\N	350044	35
3510906	Cássia dos Coqueiros	\N	350031	35
3511003	Castilho	171	350024	35
3511102	Catanduva	59	350026	35
3511201	Catiguá	789	350026	35
3511300	Cedral	\N	350025	35
3511409	Cerqueira César	\N	350007	35
3511508	Cerquilho	\N	350003	35
3511607	Cesário Lange	\N	350008	35
3511706	Charqueada	94	350040	35
3511904	Clementina	171	350023	35
3512001	Colina	454	350032	35
3512100	Colômbia	454	350032	35
3512209	Conchal	220	350045	35
3512308	Conchas	\N	350011	35
3512407	Cordeirópolis	\N	350042	35
3512506	Coroados	249	350023	35
3512605	Coronel Macedo	\N	350007	35
3512704	Corumbataí	\N	350046	35
3512803	Cosmópolis	220	350038	35
3512902	Cosmorama	\N	350027	35
3513009	Cotia	195	350001	35
3513108	Cravinhos	167	350031	35
3513207	Cristais Paulista	323	350033	35
3513306	Cruzália	\N	350014	35
3513405	Cruzeiro	327	350053	35
3513504	Cubatão	495	350002	35
3513603	Cunha	\N	350052	35
3513702	Descalvado	220	350037	35
3513801	Diadema	781	350001	35
3513850	Dirce Reis	\N	350028	35
3513900	Divinolândia	88	350047	35
3514007	Dobrada	287	350036	35
3514106	Dois Córregos	87	350010	35
3514205	Dolcinópolis	\N	350028	35
3514304	Dourado	\N	350037	35
3514403	Dracena	49	350020	35
3514502	Duartina	100	350009	35
3514601	Dumont	\N	350031	35
3514700	Echaporã	\N	350013	35
3514809	Eldorado	445	350005	35
3514908	Elias Fausto	\N	350038	35
3514924	Elisiário	\N	350026	35
3514957	Embaúba	\N	350026	35
3515004	Embu das Artes	\N	350001	35
3515103	Embu-Guaçu	783	350001	35
3515129	Emilianópolis	144	350018	35
3515152	Engenheiro Coelho	220	350042	35
3515186	Espírito Santo do Pinhal	88	350044	35
3515194	Espírito Santo do Turvo	93	350015	35
3515202	Estrela d'Oeste	\N	350029	35
3515301	Estrela do Norte	144	350018	35
3515350	Euclides da Cunha Paulista	144	350018	35
3515400	Fartura	294	350017	35
3515509	Fernandópolis	285	350029	35
3515608	Fernando Prestes	\N	350026	35
3515657	Fernão	\N	350013	35
3515707	Ferraz de Vasconcelos	\N	350001	35
3515806	Flora Rica	49	350020	35
3515905	Floreal	\N	350027	35
3516002	Flórida Paulista	49	350019	35
3516101	Florínia	\N	350014	35
3516200	Franca	323	350033	35
3516309	Francisco Morato	\N	350001	35
3516408	Franco da Rocha	393	350001	35
3516507	Gabriel Monteiro	\N	350023	35
3516606	Gália	100	350013	35
3516705	Garça	49	350013	35
3516804	Gastão Vidigal	\N	350022	35
3516853	Gavião Peixoto	\N	350036	35
3516903	General Salgado	285	350022	35
3517000	Getulina	171	350013	35
3517109	Glicério	171	350023	35
3517208	Guaiçara	171	350012	35
3517307	Guaimbê	171	350013	35
3517406	Guaíra	454	350032	35
3517505	Guapiaçu	\N	350025	35
3517604	Guapiara	294	350004	35
3517703	Guará	323	350035	35
3517802	Guaraçaí	249	350024	35
3517901	Guaraci	454	350032	35
3518008	Guarani d'Oeste	\N	350029	35
3518107	Guarantã	171	350012	35
3518206	Guararapes	249	350022	35
3518305	Guararema	383	350001	35
3518404	Guaratinguetá	\N	350052	35
3518503	Guareí	699	350006	35
3518602	Guariba	287	350031	35
3518701	Guarujá	495	350002	35
3518800	Guarulhos	788	350001	35
3518859	Guatapará	167	350031	35
3518909	Guzolândia	285	350022	35
3519006	Herculândia	\N	350016	35
3519055	Holambra	\N	350038	35
3519071	Hortolândia	\N	350038	35
3519105	Iacanga	\N	350009	35
3519204	Iacri	49	350016	35
3519253	Iaras	93	350007	35
3519303	Ibaté	87	350037	35
3519402	Ibirá	\N	350025	35
3519501	Ibirarema	200	350015	35
3519600	Ibitinga	\N	350036	35
3519709	Ibiúna	195	350003	35
3519808	Icém	59	350025	35
3519907	Iepê	200	350018	35
3520004	Igaraçu do Tietê	93	350010	35
3520103	Igarapava	323	350035	35
3520202	Igaratá	\N	350049	35
3520301	Iguape	445	350005	35
3520400	Ilhabela	787	350051	35
3520426	Ilha Comprida	\N	350005	35
3520442	Ilha Solteira	285	350024	35
3520509	Indaiatuba	92	350038	35
3520608	Indiana	144	350018	35
3520707	Indiaporã	\N	350029	35
3520806	Inúbia Paulista	\N	350019	35
3520905	Ipaussu	\N	350015	35
3521002	Iperó	116	350003	35
3521101	Ipeúna	\N	350046	35
3521150	Ipiguá	\N	350025	35
3521200	Iporanga	445	350005	35
3521309	Ipuã	\N	350034	35
3521408	Iracemápolis	\N	350042	35
3521507	Irapuã	789	350025	35
3521606	Irapuru	49	350020	35
3521705	Itaberá	294	350004	35
3521804	Itaí	294	350007	35
3521903	Itajobi	\N	350026	35
3522000	Itaju	\N	350010	35
3522109	Itanhaém	495	350002	35
3522158	Itaóca	294	350004	35
3522208	Itapecerica da Serra	783	350001	35
3522307	Itapetininga	116	350006	35
3522406	Itapeva	294	350004	35
3522505	Itapevi	195	350001	35
3522604	Itapira	92	350043	35
3522653	Itapirapuã Paulista	\N	350004	35
3522703	Itápolis	87	350036	35
3522802	Itaporanga	294	350004	35
3522901	Itapuí	87	350010	35
3523008	Itapura	285	350024	35
3523107	Itaquaquecetuba	383	350001	35
3523206	Itararé	294	350004	35
3523305	Itariri	445	350002	35
3523404	Itatiba	393	350039	35
3523503	Itatinga	93	350011	35
3523602	Itirapina	87	350037	35
3523701	Itirapuã	323	350033	35
3523800	Itobi	88	350047	35
3523909	Itu	551	350003	35
3524006	Itupeva	\N	350039	35
3524105	Ituverava	323	350035	35
3524204	Jaborandi	454	350032	35
3524303	Jaboticabal	287	350031	35
3524402	Jacareí	238	350049	35
3524501	Jaci	\N	350025	35
3524600	Jacupiranga	445	350005	35
3524709	Jaguariúna	\N	350038	35
3524808	Jales	285	350028	35
3524907	Jambeiro	\N	350049	35
3525003	Jandira	195	350001	35
3525102	Jardinópolis	167	350031	35
3525201	Jarinu	\N	350039	35
3525300	Jaú	87	350010	35
3525409	Jeriquara	\N	350033	35
3525508	Joanópolis	\N	350041	35
3525607	João Ramalho	200	350018	35
3525706	José Bonifácio	59	350025	35
3525805	Júlio Mesquita	\N	350013	35
3525854	Jumirim	\N	350003	35
3525904	Jundiaí	551	350039	35
3526001	Junqueirópolis	49	350020	35
3526100	Juquiá	445	350005	35
3526209	Juquitiba	783	350001	35
3526308	Lagoinha	284	350050	35
3526407	Laranjal Paulista	93	350040	35
3526506	Lavínia	249	350024	35
3526605	Lavrinhas	\N	350053	35
3526704	Leme	220	350045	35
3526803	Lençóis Paulista	93	350009	35
3526902	Limeira	220	350042	35
3527009	Lindóia	\N	350048	35
3527108	Lins	171	350012	35
3527207	Lorena	\N	350052	35
3527256	Lourdes	\N	350023	35
3527306	Louveira	\N	350039	35
3527405	Lucélia	49	350019	35
3527504	Lucianópolis	\N	350009	35
3527603	Luís Antônio	167	350031	35
3527702	Luiziânia	\N	350023	35
3527801	Lupércio	\N	350013	35
3527900	Lutécia	\N	350014	35
3528007	Macatuba	93	350009	35
3528106	Macaubal	59	350025	35
3528205	Macedônia	\N	350029	35
3528304	Magda	59	350022	35
3528403	Mairinque	195	350003	35
3528502	Mairiporã	393	350001	35
3528601	Manduri	\N	350007	35
3528700	Marabá Paulista	144	350021	35
3528809	Maracaí	200	350014	35
3528858	Marapoama	\N	350026	35
3528908	Mariápolis	\N	350019	35
3529005	Marília	49	350013	35
3529104	Marinópolis	\N	350028	35
3529203	Martinópolis	144	350018	35
3529302	Matão	87	350036	35
3529401	Mauá	\N	350001	35
3529500	Mendonça	\N	350025	35
3529609	Meridiano	\N	350029	35
3529658	Mesópolis	\N	350028	35
3529708	Miguelópolis	454	350035	35
3529807	Mineiros do Tietê	87	350010	35
3529906	Miracatu	445	350005	35
3530003	Mira Estrela	\N	350029	35
3530102	Mirandópolis	249	350024	35
3530201	Mirante do Paranapanema	144	350018	35
3530300	Mirassol	59	350025	35
3530409	Mirassolândia	59	350025	35
3530508	Mococa	88	350047	35
3530607	Mogi das Cruzes	383	350001	35
3530706	Mogi Guaçu	\N	350043	35
3530805	Moji Mirim	\N	350043	35
3530904	Mombuca	\N	350040	35
3531001	Monções	\N	350022	35
3531100	Mongaguá	\N	350002	35
3531209	Monte Alegre do Sul	\N	350048	35
3531308	Monte Alto	287	350031	35
3531407	Monte Aprazível	59	350025	35
3531506	Monte Azul Paulista	287	350032	35
3531605	Monte Castelo	\N	350020	35
3531704	Monteiro Lobato	\N	350049	35
3531803	Monte Mor	92	350038	35
3531902	Morro Agudo	454	350034	35
3532009	Morungaba	\N	350039	35
3532058	Motuca	87	350036	35
3532108	Murutinga do Sul	249	350024	35
3532157	Nantes	200	350018	35
3532207	Narandiba	144	350018	35
3532306	Natividade da Serra	\N	350050	35
3532405	Nazaré Paulista	\N	350041	35
3532504	Neves Paulista	\N	350025	35
3532603	Nhandeara	\N	350027	35
3532702	Nipoã	\N	350025	35
3532801	Nova Aliança	\N	350025	35
3532827	Nova Campina	\N	350004	35
3532843	Nova Canaã Paulista	\N	350030	35
3532868	Nova Castilho	\N	350022	35
3532900	Nova Europa	87	350036	35
3533007	Nova Granada	59	350025	35
3533106	Nova Guataporanga	\N	350020	35
3533205	Nova Independência	249	350024	35
3533254	Novais	\N	350026	35
3533304	Nova Luzitânia	\N	350022	35
3533403	Nova Odessa	220	350038	35
3533502	Novo Horizonte	87	350025	35
3533601	Nuporanga	323	350034	35
3533700	Ocauçu	\N	350013	35
3533809	Óleo	786	350007	35
3533908	Olímpia	454	350032	35
3534005	Onda Verde	\N	350025	35
3534104	Oriente	49	350013	35
3534203	Orindiúva	59	350025	35
3534302	Orlândia	323	350034	35
3534401	Osasco	195	350001	35
3534500	Oscar Bressane	\N	350013	35
3534609	Osvaldo Cruz	\N	350019	35
3534708	Ourinhos	786	350015	35
3534757	Ouroeste	285	350029	35
3534807	Ouro Verde	\N	350020	35
3534906	Pacaembu	49	350019	35
3535002	Palestina	\N	350025	35
3535101	Palmares Paulista	287	350026	35
3535200	Palmeira d'Oeste	\N	350028	35
3535309	Palmital	200	350014	35
3535408	Panorama	144	350020	35
3535507	Paraguaçu Paulista	200	350014	35
3535606	Paraibuna	753	350049	35
3535705	Paraíso	\N	350026	35
3535804	Paranapanema	699	350007	35
3535903	Paranapuã	\N	350028	35
3536000	Parapuã	49	350016	35
3536109	Pardinho	\N	350011	35
3536208	Pariquera-Açu	445	350005	35
3536257	Parisi	\N	350027	35
3536307	Patrocínio Paulista	323	350033	35
3536406	Paulicéia	49	350020	35
3536505	Paulínia	92	350038	35
3536570	Paulistânia	100	350009	35
3536604	Paulo de Faria	59	350025	35
3536703	Pederneiras	100	350009	35
3536802	Pedra Bela	\N	350041	35
3536901	Pedranópolis	\N	350029	35
3537008	Pedregulho	323	350033	35
3537107	Pedreira	\N	350038	35
3537156	Pedrinhas Paulista	\N	350014	35
3537206	Pedro de Toledo	445	350002	35
3537305	Penápolis	171	350023	35
3537404	Pereira Barreto	285	350024	35
3537503	Pereiras	\N	350008	35
3537602	Peruíbe	495	350002	35
3537701	Piacatu	249	350023	35
3537800	Piedade	116	350003	35
3537909	Pilar do Sul	\N	350003	35
3538006	Pindamonhangaba	413	350050	35
3538105	Pindorama	789	350026	35
3538204	Pinhalzinho	\N	350041	35
3538303	Piquerobi	144	350021	35
3538501	Piquete	\N	350052	35
3538600	Piracaia	393	350041	35
3538709	Piracicaba	94	350040	35
3538808	Piraju	\N	350017	35
3538907	Pirajuí	93	350009	35
3539004	Pirangi	287	350026	35
3539103	Pirapora do Bom Jesus	551	350001	35
3539202	Pirapozinho	144	350018	35
3539301	Pirassununga	220	350037	35
3539400	Piratininga	100	350009	35
3539509	Pitangueiras	287	350031	35
3539608	Planalto	\N	350025	35
3539707	Platina	\N	350014	35
3539806	Poá	\N	350001	35
3539905	Poloni	59	350025	35
3540002	Pompéia	\N	350013	35
3540101	Pongaí	\N	350012	35
3540200	Pontal	167	350031	35
3540259	Pontalinda	\N	350028	35
3540309	Pontes Gestal	\N	350027	35
3540408	Populina	\N	350028	35
3540507	Porangaba	\N	350008	35
3540606	Porto Feliz	116	350003	35
3540705	Porto Ferreira	220	350037	35
3540754	Potim	\N	350052	35
3540804	Potirendaba	\N	350025	35
3540853	Pracinha	\N	350019	35
3540903	Pradópolis	287	350031	35
3541000	Praia Grande	\N	350002	35
3541059	Pratânia	93	350011	35
3541109	Presidente Alves	171	350009	35
3541208	Presidente Bernardes	144	350018	35
3541307	Presidente Epitácio	144	350021	35
3541406	Presidente Prudente	144	350018	35
3541505	Presidente Venceslau	144	350021	35
3541604	Promissão	171	350012	35
3541653	Quadra	699	350008	35
3541703	Quatá	200	350018	35
3541802	Queiroz	\N	350016	35
3541901	Queluz	327	350053	35
3542008	Quintana	\N	350013	35
3542107	Rafard	\N	350040	35
3542206	Rancharia	200	350018	35
3542305	Redenção da Serra	\N	350050	35
3542404	Regente Feijó	144	350018	35
3542503	Reginópolis	171	350009	35
3542602	Registro	445	350005	35
3542701	Restinga	323	350033	35
3542800	Ribeira	294	350004	35
3542909	Ribeirão Bonito	87	350037	35
3543006	Ribeirão Branco	294	350004	35
3543105	Ribeirão Corrente	\N	350033	35
3543204	Ribeirão do Sul	\N	350015	35
3543238	Ribeirão dos Índios	\N	350018	35
3543253	Ribeirão Grande	\N	350004	35
3543303	Ribeirão Pires	\N	350001	35
3543402	Ribeirão Preto	167	350031	35
3543501	Riversul	294	350004	35
3543600	Rifaina	\N	350033	35
3543709	Rincão	\N	350036	35
3543808	Rinópolis	49	350016	35
3543907	Rio Claro	94	350046	35
3544004	Rio das Pedras	94	350040	35
3544103	Rio Grande da Serra	\N	350001	35
3544202	Riolândia	\N	350027	35
3544251	Rosana	144	350018	35
3544301	Roseira	\N	350052	35
3544400	Rubiácea	\N	350022	35
3544509	Rubinéia	\N	350030	35
3544608	Sabino	171	350012	35
3544707	Sagres	\N	350019	35
3544806	Sales	\N	350025	35
3544905	Sales Oliveira	323	350034	35
3545001	Salesópolis	\N	350001	35
3545100	Salmourão	49	350019	35
3545159	Saltinho	\N	350040	35
3545209	Salto	\N	350003	35
3545308	Salto de Pirapora	116	350003	35
3545407	Salto Grande	\N	350015	35
3545506	Sandovalina	144	350018	35
3545605	Santa Adélia	\N	350026	35
3545704	Santa Albertina	\N	350028	35
3545803	Santa Bárbara d'Oeste	\N	350038	35
3546009	Santa Branca	\N	350049	35
3546108	Santa Clara d'Oeste	\N	350030	35
3546207	Santa Cruz da Conceição	\N	350045	35
3546256	Santa Cruz da Esperança	\N	350031	35
3546306	Santa Cruz das Palmeiras	88	350044	35
3546405	Santa Cruz do Rio Pardo	786	350015	35
3546504	Santa Ernestina	\N	350031	35
3546603	Santa Fé do Sul	285	350030	35
3546702	Santa Gertrudes	\N	350046	35
3546801	Santa Isabel	383	350001	35
3546900	Santa Lúcia	87	350036	35
3547007	Santa Maria da Serra	94	350040	35
3547106	Santa Mercedes	\N	350020	35
3547205	Santana da Ponte Pensa	\N	350030	35
3547304	Santana de Parnaíba	551	350001	35
3547403	Santa Rita d'Oeste	\N	350030	35
3547502	Santa Rita do Passa Quatro	167	350037	35
3547601	Santa Rosa de Viterbo	\N	350031	35
3547650	Santa Salete	\N	350028	35
3547700	Santo Anastácio	144	350018	35
3547809	Santo André	679	350001	35
3547908	Santo Antônio da Alegria	\N	350031	35
3548005	Santo Antônio de Posse	92	350038	35
3548054	Santo Antônio do Aracanguá	285	350022	35
3548104	Santo Antônio do Jardim	\N	350044	35
3548203	Santo Antônio do Pinhal	\N	350050	35
3548302	Santo Expedito	\N	350018	35
3548401	Santópolis do Aguapeí	\N	350023	35
3548500	Santos	495	350002	35
3548609	São Bento do Sapucaí	\N	350050	35
3548708	São Bernardo do Campo	781	350001	35
3548807	São Caetano do Sul	\N	350001	35
3548906	São Carlos	87	350037	35
3549003	São Francisco	285	350028	35
3549102	São João da Boa Vista	88	350044	35
3549201	São João das Duas Pontes	\N	350029	35
3549250	São João de Iracema	\N	350029	35
3549300	São João do Pau d'Alho	\N	350020	35
3549409	São Joaquim da Barra	\N	350034	35
3549508	São José da Bela Vista	\N	350033	35
3549607	São José do Barreiro	327	350053	35
3549706	São José do Rio Pardo	\N	350047	35
3549805	São José do Rio Preto	59	350025	35
3549904	São José dos Campos	238	350049	35
3549953	São Lourenço da Serra	\N	350001	35
3550001	São Luís do Paraitinga	413	350050	35
3550100	São Manuel	93	350011	35
3550209	São Miguel Arcanjo	699	350006	35
3550308	São Paulo	679	350001	35
3550407	São Pedro	\N	350040	35
3550506	São Pedro do Turvo	\N	350015	35
3550605	São Roque	195	350003	35
3550704	São Sebastião	495	350051	35
3550803	São Sebastião da Grama	\N	350047	35
3550902	São Simão	167	350031	35
3551009	São Vicente	495	350002	35
3551108	Sarapuí	\N	350003	35
3551207	Sarutaiá	\N	350017	35
3551306	Sebastianópolis do Sul	59	350027	35
3551405	Serra Azul	167	350031	35
3551504	Serrana	167	350031	35
3551603	Serra Negra	\N	350048	35
3551702	Sertãozinho	167	350031	35
3551801	Sete Barras	445	350005	35
3551900	Severínia	\N	350032	35
3552007	Silveiras	\N	350053	35
3552106	Socorro	\N	350041	35
3552205	Sorocaba	116	350003	35
3552304	Sud Mennucci	285	350024	35
3552403	Sumaré	92	350038	35
3552502	Suzano	\N	350001	35
3552551	Suzanápolis	285	350028	35
3552601	Tabapuã	\N	350026	35
3552700	Tabatinga	\N	350036	35
3552809	Taboão da Serra	783	350001	35
3552908	Taciba	144	350018	35
3553005	Taguaí	\N	350007	35
3553104	Taiaçu	\N	350032	35
3553203	Taiúva	\N	350032	35
3553302	Tambaú	\N	350044	35
3553401	Tanabi	\N	350025	35
3553500	Tapiraí	116	350003	35
3553609	Tapiratiba	\N	350047	35
3553658	Taquaral	\N	350032	35
3553708	Taquaritinga	287	350036	35
3553807	Taquarituba	\N	350007	35
3553856	Taquarivaí	294	350004	35
3553906	Tarabai	144	350018	35
3553955	Tarumã	\N	350014	35
3554003	Tatuí	699	350008	35
3554102	Taubaté	413	350050	35
3554201	Tejupá	\N	350017	35
3554300	Teodoro Sampaio	144	350018	35
3554409	Terra Roxa	287	350032	35
3554508	Tietê	116	350003	35
3554607	Timburi	\N	350017	35
3554656	Torre de Pedra	\N	350008	35
3554706	Torrinha	\N	350010	35
3554755	Trabiju	\N	350036	35
3554805	Tremembé	413	350050	35
3554904	Três Fronteiras	\N	350030	35
3554953	Tuiuti	\N	350041	35
3555000	Tupã	49	350016	35
3555109	Tupi Paulista	49	350020	35
3555208	Turiúba	\N	350023	35
3555307	Turmalina	285	350028	35
3555356	Ubarana	59	350025	35
3555406	Ubatuba	495	350051	35
3555505	Ubirajara	786	350009	35
3555604	Uchoa	\N	350025	35
3555703	União Paulista	\N	350025	35
3555802	Urânia	285	350028	35
3555901	Uru	\N	350012	35
3556008	Urupês	\N	350025	35
3556107	Valentim Gentil	\N	350027	35
3556206	Valinhos	92	350038	35
3556305	Valparaíso	249	350022	35
3556354	Vargem	\N	350041	35
3556404	Vargem Grande do Sul	\N	350044	35
3556453	Vargem Grande Paulista	\N	350001	35
3556503	Várzea Paulista	\N	350039	35
3556602	Vera Cruz	\N	350013	35
3556701	Vinhedo	\N	350038	35
3556800	Viradouro	\N	350032	35
3556909	Vista Alegre do Alto	287	350026	35
3556958	Vitória Brasil	\N	350028	35
3557006	Votorantim	\N	350003	35
3557105	Votuporanga	59	350027	35
3557154	Zacarias	\N	350023	35
3557204	Chavantes	\N	350015	35
3557303	Estiva Gerbi	\N	350043	35
4100103	Abatiá	16	410022	41
4100202	Adrianópolis	60	410001	41
4100301	Agudos do Sul	101	410001	41
4100400	Almirante Tamandaré	\N	410001	41
4100459	Altamira do Paraná	\N	410015	41
4100509	Altônia	\N	410016	41
4100608	Alto Paraná	\N	410017	41
4100707	Alto Piquiri	\N	410016	41
4100806	Alvorada do Sul	206	410021	41
4100905	Amaporã	188	410017	41
4101002	Ampére	\N	410009	41
4101051	Anahy	\N	410006	41
4101101	Andirá	\N	410022	41
4101150	Ângulo	\N	410014	41
4101200	Antonina	60	410002	41
4101309	Antônio Olinto	\N	410003	41
4101408	Apucarana	168	410023	41
4101507	Arapongas	168	410021	41
4101606	Arapoti	\N	410027	41
4101655	Arapuã	\N	410025	41
4101705	Araruna	\N	410015	41
4101804	Araucária	101	410001	41
4101853	Ariranha do Ivaí	\N	410025	41
4101903	Assaí	\N	410021	41
4102000	Assis Chateaubriand	\N	410008	41
4102109	Astorga	168	410014	41
4102208	Atalaia	\N	410014	41
4102307	Balsa Nova	101	410001	41
4102406	Bandeirantes	16	410024	41
4102505	Barbosa Ferraz	343	410015	41
4102604	Barracão	\N	410009	41
4102703	Barra do Jacaré	\N	410022	41
4102752	Bela Vista da Caroba	\N	410009	41
4102802	Bela Vista do Paraíso	206	410021	41
4102901	Bituruna	279	410003	41
4103008	Boa Esperança	\N	410015	41
4103024	Boa Esperança do Iguaçu	\N	410012	41
4103040	Boa Ventura de São Roque	164	410005	41
4103057	Boa Vista da Aparecida	\N	410006	41
4103107	Bocaiúva do Sul	60	410001	41
4103156	Bom Jesus do Sul	\N	410009	41
4103206	Bom Sucesso	\N	410023	41
4103222	Bom Sucesso do Sul	\N	410010	41
4103305	Borrazópolis	168	410023	41
4103354	Braganey	\N	410006	41
4103370	Brasilândia do Sul	\N	410016	41
4103404	Cafeara	\N	410021	41
4103453	Cafelândia	\N	410006	41
4103479	Cafezal do Sul	\N	410016	41
4103503	Califórnia	\N	410023	41
4103602	Cambará	16	410022	41
4103701	Cambé	\N	410021	41
4103800	Cambira	168	410023	41
4103909	Campina da Lagoa	343	410015	41
4103958	Campina do Simão	164	410004	41
4104006	Campina Grande do Sul	60	410001	41
4104055	Campo Bonito	233	410006	41
4104105	Campo do Tenente	\N	410001	41
4104204	Campo Largo	101	410001	41
4104253	Campo Magro	101	410001	41
4104303	Campo Mourão	101	410015	41
4104402	Cândido de Abreu	164	410025	41
4104428	Candói	164	410004	41
4104451	Cantagalo	164	410004	41
4104501	Capanema	229	410009	41
4104600	Capitão Leônidas Marques	233	410006	41
4104659	Carambeí	719	410027	41
4104709	Carlópolis	\N	410022	41
4104808	Cascavel	233	410006	41
4104907	Castro	719	410027	41
4105003	Catanduvas	233	410006	41
4105102	Centenário do Sul	206	410021	41
4105201	Cerro Azul	60	410001	41
4105300	Céu Azul	714	410006	41
4105409	Chopinzinho	229	410010	41
4105508	Cianorte	\N	410018	41
4105607	Cidade Gaúcha	190	410016	41
4105706	Clevelândia	229	410010	41
4105805	Colombo	\N	410001	41
4105904	Colorado	168	410019	41
4106001	Congonhinhas	387	410024	41
4106100	Conselheiro Mairinck	\N	410026	41
4106209	Contenda	\N	410001	41
4106308	Corbélia	233	410006	41
4106407	Cornélio Procópio	387	410024	41
4106456	Coronel Domingos Soares	229	410010	41
4106506	Coronel Vivida	229	410010	41
4106555	Corumbataí do Sul	\N	410015	41
4106571	Cruzeiro do Iguaçu	\N	410012	41
4106605	Cruzeiro do Oeste	190	410016	41
4106704	Cruzeiro do Sul	303	410019	41
4106803	Cruz Machado	279	410003	41
4106852	Cruzmaltina	\N	410025	41
4106902	Curitiba	101	410001	41
4107009	Curiúva	387	410028	41
4107108	Diamante do Norte	188	410017	41
4107124	Diamante do Sul	\N	410006	41
4107157	Diamante D'Oeste	\N	410006	41
4107207	Dois Vizinhos	\N	410012	41
4107256	Douradina	\N	410016	41
4107306	Doutor Camargo	\N	410014	41
4107405	Enéas Marques	101	410009	41
4107504	Engenheiro Beltrão	343	410015	41
4107520	Esperança Nova	\N	410016	41
4107538	Entre Rios do Oeste	\N	410013	41
4107546	Espigão Alto do Iguaçu	164	410011	41
4107553	Farol	\N	410015	41
4107603	Faxinal	168	410025	41
4107652	Fazenda Rio Grande	101	410001	41
4107702	Fênix	\N	410015	41
4107736	Fernandes Pinheiro	\N	410029	41
4107751	Figueira	\N	410026	41
4107801	Floraí	\N	410014	41
4107850	Flor da Serra do Sul	229	410009	41
4107900	Floresta	303	410014	41
4108007	Florestópolis	206	410021	41
4108106	Flórida	\N	410014	41
4108205	Formosa do Oeste	\N	410008	41
4108304	Foz do Iguaçu	714	410007	41
4108320	Francisco Alves	\N	410016	41
4108403	Francisco Beltrão	229	410009	41
4108452	Foz do Jordão	164	410004	41
4108502	General Carneiro	279	410003	41
4108551	Godoy Moreira	168	410025	41
4108601	Goioerê	343	410015	41
4108650	Goioxim	164	410004	41
4108700	Grandes Rios	164	410025	41
4108809	Guaíra	391	410008	41
4108908	Guairaçá	188	410017	41
4108957	Guamiranga	\N	410004	41
4109005	Guapirama	16	410022	41
4109104	Guaporema	\N	410018	41
4109203	Guaraci	168	410021	41
4109302	Guaraniaçu	233	410006	41
4109401	Guarapuava	164	410004	41
4109500	Guaraqueçaba	60	410002	41
4109609	Guaratuba	60	410002	41
4109658	Honório Serpa	229	410010	41
4109708	Ibaiti	\N	410026	41
4109757	Ibema	233	410006	41
4109807	Ibiporã	\N	410021	41
4109906	Icaraíma	190	410016	41
4110003	Iguaraçu	\N	410014	41
4110052	Iguatu	\N	410006	41
4110078	Imbaú	719	410028	41
4110102	Imbituva	719	410029	41
4110201	Inácio Martins	164	410004	41
4110300	Inajá	\N	410019	41
4110409	Indianópolis	\N	410018	41
4110508	Ipiranga	\N	410027	41
4110607	Iporã	\N	410016	41
4110656	Iracema do Oeste	\N	410008	41
4110706	Irati	719	410029	41
4110805	Iretama	343	410015	41
4110904	Itaguajé	168	410019	41
4110953	Itaipulândia	714	410007	41
4111001	Itambaracá	\N	410024	41
4111100	Itambé	\N	410014	41
4111209	Itapejara d'Oeste	\N	410010	41
4111258	Itaperuçu	\N	410001	41
4111308	Itaúna do Sul	188	410017	41
4111407	Ivaí	\N	410027	41
4111506	Ivaiporã	168	410025	41
4111555	Ivaté	190	410016	41
4111605	Ivatuba	\N	410014	41
4111704	Jaboti	\N	410026	41
4111803	Jacarezinho	16	410022	41
4111902	Jaguapitã	206	410021	41
4112009	Jaguariaíva	\N	410027	41
4112108	Jandaia do Sul	\N	410023	41
4112207	Janiópolis	\N	410015	41
4112306	Japira	\N	410026	41
4112405	Japurá	\N	410018	41
4112504	Jardim Alegre	168	410025	41
4112603	Jardim Olinda	303	410019	41
4112702	Jataizinho	206	410021	41
4112751	Jesuítas	\N	410008	41
4112801	Joaquim Távora	\N	410022	41
4112900	Jundiaí do Sul	16	410022	41
4112959	Juranda	\N	410015	41
4113007	Jussara	\N	410018	41
4113106	Kaloré	\N	410023	41
4113205	Lapa	101	410001	41
4113254	Laranjal	164	410005	41
4113304	Laranjeiras do Sul	164	410011	41
4113403	Leópolis	\N	410024	41
4113429	Lidianópolis	\N	410025	41
4113452	Lindoeste	233	410006	41
4113502	Loanda	188	410020	41
4113601	Lobato	\N	410019	41
4113700	Londrina	206	410021	41
4113734	Luiziana	343	410015	41
4113759	Lunardelli	\N	410025	41
4113809	Lupionópolis	\N	410021	41
4113908	Mallet	279	410029	41
4114005	Mamborê	101	410015	41
4114104	Mandaguaçu	\N	410014	41
4114203	Mandaguari	303	410014	41
4114302	Mandirituba	101	410001	41
4114351	Manfrinópolis	\N	410009	41
4114401	Mangueirinha	229	410010	41
4114500	Manoel Ribas	164	410025	41
4114609	Marechal Cândido Rondon	391	410013	41
4114708	Maria Helena	\N	410016	41
4114807	Marialva	303	410014	41
4114906	Marilândia do Sul	168	410023	41
4115002	Marilena	188	410017	41
4115101	Mariluz	343	410016	41
4115200	Maringá	303	410014	41
4115309	Mariópolis	229	410010	41
4115358	Maripá	\N	410008	41
4115408	Marmeleiro	229	410009	41
4115457	Marquinho	\N	410011	41
4115507	Marumbi	\N	410023	41
4115606	Matelândia	714	410006	41
4115705	Matinhos	60	410002	41
4115739	Mato Rico	\N	410005	41
4115754	Mauá da Serra	168	410023	41
4115804	Medianeira	\N	410007	41
4115853	Mercedes	\N	410013	41
4115903	Mirador	188	410017	41
4116000	Miraselva	\N	410021	41
4116059	Missal	\N	410007	41
4116109	Moreira Sales	343	410015	41
4116208	Morretes	\N	410002	41
4116307	Munhoz de Melo	\N	410014	41
4116406	Nossa Senhora das Graças	\N	410019	41
4116505	Nova Aliança do Ivaí	\N	410017	41
4116604	Nova América da Colina	\N	410024	41
4116703	Nova Aurora	\N	410006	41
4116802	Nova Cantu	343	410015	41
4116901	Nova Esperança	\N	410014	41
4116950	Nova Esperança do Sudoeste	\N	410009	41
4117008	Nova Fátima	387	410024	41
4117057	Nova Laranjeiras	\N	410011	41
4117107	Nova Londrina	188	410017	41
4117206	Nova Olímpia	190	410016	41
4117214	Nova Santa Bárbara	\N	410024	41
4117222	Nova Santa Rosa	\N	410013	41
4117255	Nova Prata do Iguaçu	229	410012	41
4117271	Nova Tebas	164	410005	41
4117297	Novo Itacolomi	\N	410023	41
4117305	Ortigueira	719	410028	41
4117404	Ourizona	\N	410014	41
4117453	Ouro Verde do Oeste	\N	410008	41
4117503	Paiçandu	\N	410014	41
4117602	Palmas	229	410010	41
4117701	Palmeira	101	410027	41
4117800	Palmital	164	410005	41
4117909	Palotina	\N	410008	41
4118006	Paraíso do Norte	\N	410017	41
4118105	Paranacity	303	410019	41
4118204	Paranaguá	60	410002	41
4118303	Paranapoema	303	410019	41
4118402	Paranavaí	188	410017	41
4118451	Pato Bragado	\N	410013	41
4118501	Pato Branco	101	410010	41
4118600	Paula Freitas	279	410003	41
4118709	Paulo Frontin	\N	410003	41
4118808	Peabiru	343	410015	41
4118857	Perobal	190	410016	41
4118907	Pérola	\N	410016	41
4119004	Pérola d'Oeste	\N	410009	41
4119103	Piên	\N	410001	41
4119152	Pinhais	\N	410001	41
4119202	Pinhalão	16	410026	41
4119251	Pinhal de São Bento	\N	410009	41
4119301	Pinhão	164	410004	41
4119400	Piraí do Sul	719	410027	41
4119509	Piraquara	101	410001	41
4119608	Pitanga	164	410005	41
4119657	Pitangueiras	\N	410021	41
4119707	Planaltina do Paraná	188	410017	41
4119806	Planalto	229	410009	41
4119905	Ponta Grossa	719	410027	41
4119954	Pontal do Paraná	60	410002	41
4120002	Porecatu	206	410021	41
4120101	Porto Amazonas	101	410027	41
4120150	Porto Barreiro	164	410011	41
4120200	Porto Rico	188	410020	41
4120309	Porto Vitória	279	410003	41
4120333	Prado Ferreira	\N	410021	41
4120358	Pranchita	\N	410009	41
4120408	Presidente Castelo Branco	303	410014	41
4120507	Primeiro de Maio	206	410021	41
4120606	Prudentópolis	164	410004	41
4120655	Quarto Centenário	\N	410015	41
4120705	Quatiguá	\N	410022	41
4120804	Quatro Barras	\N	410001	41
4120853	Quatro Pontes	\N	410013	41
4120903	Quedas do Iguaçu	164	410011	41
4121000	Querência do Norte	188	410020	41
4121109	Quinta do Sol	343	410015	41
4121208	Quitandinha	\N	410001	41
4121257	Ramilândia	714	410006	41
4121307	Rancho Alegre	387	410024	41
4121356	Rancho Alegre D'Oeste	\N	410015	41
4121406	Realeza	229	410009	41
4121505	Rebouças	279	410029	41
4121604	Renascença	229	410009	41
4121703	Reserva	719	410028	41
4121752	Reserva do Iguaçu	164	410004	41
4121802	Ribeirão Claro	\N	410022	41
4121901	Ribeirão do Pinhal	16	410022	41
4122008	Rio Azul	279	410029	41
4122107	Rio Bom	\N	410023	41
4122156	Rio Bonito do Iguaçu	164	410011	41
4122172	Rio Branco do Ivaí	164	410025	41
4122206	Rio Branco do Sul	101	410001	41
4122305	Rio Negro	\N	410001	41
4122404	Rolândia	206	410021	41
4122503	Roncador	\N	410015	41
4122602	Rondon	190	410018	41
4122651	Rosário do Ivaí	164	410025	41
4122701	Sabáudia	\N	410021	41
4122800	Salgado Filho	\N	410009	41
4122909	Salto do Itararé	\N	410022	41
4123006	Salto do Lontra	\N	410012	41
4123105	Santa Amélia	16	410024	41
4123204	Santa Cecília do Pavão	387	410024	41
4123303	Santa Cruz de Monte Castelo	\N	410020	41
4123402	Santa Fé	\N	410014	41
4123501	Santa Helena	714	410008	41
4123600	Santa Inês	168	410019	41
4123709	Santa Isabel do Ivaí	\N	410020	41
4123808	Santa Izabel do Oeste	\N	410009	41
4123824	Santa Lúcia	\N	410006	41
4123857	Santa Maria do Oeste	164	410005	41
4123907	Santa Mariana	\N	410024	41
4123956	Santa Mônica	\N	410020	41
4124004	Santana do Itararé	\N	410022	41
4124020	Santa Tereza do Oeste	233	410006	41
4124053	Santa Terezinha de Itaipu	714	410007	41
4124103	Santo Antônio da Platina	16	410022	41
4124202	Santo Antônio do Caiuá	\N	410017	41
4124301	Santo Antônio do Paraíso	\N	410024	41
4124400	Santo Antônio do Sudoeste	229	410009	41
4124509	Santo Inácio	168	410019	41
4124608	São Carlos do Ivaí	\N	410017	41
4124707	São Jerônimo da Serra	387	410024	41
4124806	São João	\N	410010	41
4124905	São João do Caiuá	188	410017	41
4125001	São João do Ivaí	\N	410025	41
4125100	São João do Triunfo	279	410027	41
4125209	São Jorge d'Oeste	\N	410012	41
4125308	São Jorge do Ivaí	\N	410014	41
4125357	São Jorge do Patrocínio	\N	410016	41
4125407	São José da Boa Vista	\N	410022	41
4125456	São José das Palmeiras	\N	410008	41
4125506	São José dos Pinhais	101	410001	41
4125555	São Manoel do Paraná	\N	410018	41
4125605	São Mateus do Sul	279	410003	41
4125704	São Miguel do Iguaçu	714	410007	41
4125753	São Pedro do Iguaçu	\N	410008	41
4125803	São Pedro do Ivaí	303	410023	41
4125902	São Pedro do Paraná	\N	410020	41
4126009	São Sebastião da Amoreira	\N	410024	41
4126108	São Tomé	\N	410018	41
4126207	Sapopema	387	410024	41
4126256	Sarandi	\N	410014	41
4126272	Saudade do Iguaçu	229	410010	41
4126306	Sengés	\N	410027	41
4126355	Serranópolis do Iguaçu	\N	410007	41
4126405	Sertaneja	387	410024	41
4126504	Sertanópolis	206	410021	41
4126603	Siqueira Campos	\N	410022	41
4126652	Sulina	\N	410010	41
4126678	Tamarana	206	410021	41
4126702	Tamboara	\N	410017	41
4126801	Tapejara	\N	410018	41
4126900	Tapira	190	410016	41
4127007	Teixeira Soares	719	410029	41
4127106	Telêmaco Borba	719	410028	41
4127205	Terra Boa	343	410018	41
4127304	Terra Rica	188	410017	41
4127403	Terra Roxa	391	410008	41
4127502	Tibagi	719	410028	41
4127601	Tijucas do Sul	101	410001	41
4127700	Toledo	\N	410008	41
4127809	Tomazina	16	410022	41
4127858	Três Barras do Paraná	233	410006	41
4127882	Tunas do Paraná	60	410001	41
4127908	Tuneiras do Oeste	\N	410018	41
4127957	Tupãssi	\N	410008	41
4127965	Turvo	164	410004	41
4128005	Ubiratã	\N	410015	41
4128104	Umuarama	190	410016	41
4128203	União da Vitória	279	410003	41
4128302	Uniflor	303	410014	41
4128401	Uraí	\N	410024	41
4128500	Wenceslau Braz	16	410022	41
4128534	Ventania	\N	410028	41
4128559	Vera Cruz do Oeste	\N	410006	41
4128609	Verê	\N	410009	41
4128625	Alto Paraíso	190	410016	41
4128633	Doutor Ulysses	60	410001	41
4128658	Virmond	\N	410011	41
4128708	Vitorino	\N	410010	41
4128807	Xambrê	190	410016	41
4200051	Abdon Batista	17	420008	42
4200101	Abelardo Luz	19	420011	42
4200200	Agrolândia	71	420022	42
4200309	Agronômica	71	420022	42
4200408	Água Doce	17	420008	42
4200507	Águas de Chapecó	19	420007	42
4200556	Águas Frias	19	420007	42
4200606	Águas Mornas	97	420001	42
4200705	Alfredo Wagner	\N	420001	42
4200754	Alto Bela Vista	\N	420010	42
4200804	Anchieta	19	420009	42
4200903	Angelina	97	420001	42
4201000	Anita Garibaldi	268	420005	42
4201109	Anitápolis	97	420001	42
4201208	Antônio Carlos	\N	420001	42
4201257	Apiúna	\N	420019	42
4201273	Arabutã	\N	420010	42
4201307	Araquari	184	420016	42
4201406	Araranguá	\N	420004	42
4201505	Armazém	\N	420003	42
4201604	Arroio Trinta	\N	420015	42
4201653	Arvoredo	19	420007	42
4201703	Ascurra	\N	420019	42
4201802	Atalanta	\N	420022	42
4201901	Aurora	\N	420022	42
4201950	Balneário Arroio do Silva	\N	420004	42
4202008	Balneário Camboriú	97	420020	42
4202057	Balneário Barra do Sul	184	420016	42
4202073	Balneário Gaivota	\N	420004	42
4202081	Bandeirante	19	420009	42
4202099	Barra Bonita	19	420009	42
4202107	Barra Velha	\N	420020	42
4202131	Bela Vista do Toldo	\N	420017	42
4202156	Belmonte	\N	420009	42
4202206	Benedito Novo	792	420019	42
4202305	Biguaçu	97	420001	42
4202404	Blumenau	184	420019	42
4202438	Bocaina do Sul	268	420005	42
4202453	Bombinhas	\N	420020	42
4202503	Bom Jardim da Serra	\N	420005	42
4202537	Bom Jesus	19	420011	42
4202578	Bom Jesus do Oeste	\N	420012	42
4202602	Bom Retiro	\N	420005	42
4202701	Botuverá	\N	420021	42
4202800	Braço do Norte	\N	420003	42
4202859	Braço do Trombudo	\N	420022	42
4202875	Brunópolis	17	420006	42
4202909	Brusque	97	420021	42
4203006	Caçador	760	420014	42
4203105	Caibi	\N	420007	42
4203154	Calmon	380	420014	42
4203204	Camboriú	\N	420020	42
4203253	Capão Alto	268	420005	42
4203303	Campo Alegre	184	420018	42
4203402	Campo Belo do Sul	268	420005	42
4203501	Campo Erê	19	420013	42
4203600	Campos Novos	17	420008	42
4203709	Canelinha	\N	420021	42
4203808	Canoinhas	380	420017	42
4203907	Capinzal	\N	420008	42
4203956	Capivari de Baixo	\N	420003	42
4204004	Catanduvas	17	420008	42
4204103	Caxambu do Sul	19	420007	42
4204152	Celso Ramos	268	420008	42
4204178	Cerro Negro	268	420005	42
4204194	Chapadão do Lageado	\N	420024	42
4204202	Chapecó	19	420007	42
4204251	Cocal do Sul	\N	420002	42
4204301	Concórdia	17	420010	42
4204350	Cordilheira Alta	\N	420007	42
4204400	Coronel Freitas	\N	420007	42
4204459	Coronel Martins	\N	420011	42
4204509	Corupá	\N	420016	42
4204558	Correia Pinto	268	420005	42
4204608	Criciúma	736	420002	42
4204707	Cunha Porã	19	420012	42
4204756	Cunhataí	\N	420007	42
4204806	Curitibanos	268	420006	42
4204905	Descanso	19	420009	42
4205001	Dionísio Cerqueira	19	420009	42
4205100	Dona Emma	\N	420023	42
4205159	Doutor Pedrinho	71	420019	42
4205175	Entre Rios	19	420011	42
4205191	Ermo	\N	420004	42
4205209	Erval Velho	17	420008	42
4205308	Faxinal dos Guedes	19	420011	42
4205357	Flor do Sertão	\N	420009	42
4205407	Florianópolis	97	420001	42
4205431	Formosa do Sul	\N	420007	42
4205456	Forquilhinha	736	420002	42
4205506	Fraiburgo	760	420015	42
4205555	Frei Rogério	268	420006	42
4205605	Galvão	\N	420013	42
4205704	Garopaba	\N	420001	42
4205803	Garuva	184	420016	42
4205902	Gaspar	184	420019	42
4206009	Governador Celso Ramos	\N	420001	42
4206108	Grão Pará	333	420003	42
4206207	Gravatal	\N	420003	42
4206306	Guabiruba	\N	420021	42
4206405	Guaraciaba	19	420009	42
4206504	Guaramirim	\N	420016	42
4206603	Guarujá do Sul	19	420009	42
4206652	Guatambú	19	420007	42
4206702	Herval d'Oeste	\N	420008	42
4206751	Ibiam	\N	420015	42
4206801	Ibicaré	\N	420008	42
4206900	Ibirama	71	420023	42
4207007	Içara	333	420002	42
4207106	Ilhota	792	420019	42
4207205	Imaruí	\N	420003	42
4207304	Imbituba	333	420001	42
4207403	Imbuia	71	420024	42
4207502	Indaial	\N	420019	42
4207577	Iomerê	\N	420015	42
4207601	Ipira	\N	420010	42
4207650	Iporã do Oeste	\N	420009	42
4207684	Ipuaçu	19	420011	42
4207700	Ipumirim	19	420010	42
4207759	Iraceminha	\N	420012	42
4207809	Irani	17	420010	42
4207858	Irati	\N	420007	42
4207908	Irineópolis	380	420017	42
4208005	Itá	19	420010	42
4208104	Itaiópolis	184	420017	42
4208203	Itajaí	97	420020	42
4208302	Itapema	\N	420020	42
4208401	Itapiranga	19	420009	42
4208450	Itapoá	184	420016	42
4208500	Ituporanga	71	420024	42
4208609	Jaborá	\N	420008	42
4208708	Jacinto Machado	\N	420004	42
4208807	Jaguaruna	\N	420003	42
4208906	Jaraguá do Sul	\N	420016	42
4208955	Jardinópolis	\N	420007	42
4209003	Joaçaba	17	420008	42
4209102	Joinville	184	420016	42
4209151	José Boiteux	71	420023	42
4209177	Jupiá	\N	420013	42
4209201	Lacerdópolis	\N	420008	42
4209300	Lages	268	420005	42
4209409	Laguna	333	420003	42
4209458	Lajeado Grande	19	420007	42
4209508	Laurentino	\N	420022	42
4209607	Lauro Muller	736	420002	42
4209706	Lebon Régis	380	420014	42
4209805	Leoberto Leal	\N	420024	42
4209854	Lindóia do Sul	17	420010	42
4209904	Lontras	\N	420022	42
4210001	Luiz Alves	\N	420020	42
4210035	Luzerna	\N	420008	42
4210050	Macieira	\N	420014	42
4210100	Mafra	184	420017	42
4210209	Major Gercino	97	420021	42
4210308	Major Vieira	380	420017	42
4210407	Maracajá	\N	420004	42
4210506	Maravilha	19	420012	42
4210555	Marema	\N	420007	42
4210605	Massaranduba	\N	420016	42
4210704	Matos Costa	380	420014	42
4210803	Meleiro	\N	420004	42
4210852	Mirim Doce	\N	420022	42
4210902	Modelo	\N	420007	42
4211009	Mondaí	19	420007	42
4211058	Monte Carlo	17	420015	42
4211108	Monte Castelo	380	420017	42
4211207	Morro da Fumaça	\N	420002	42
4211256	Morro Grande	\N	420002	42
4211306	Navegantes	792	420020	42
4211405	Nova Erechim	\N	420007	42
4211454	Nova Itaberaba	\N	420007	42
4211504	Nova Trento	\N	420021	42
4211603	Nova Veneza	736	420002	42
4211652	Novo Horizonte	\N	420013	42
4211702	Orleans	\N	420002	42
4211751	Otacílio Costa	268	420005	42
4211801	Ouro	\N	420008	42
4211850	Ouro Verde	\N	420011	42
4211876	Paial	268	420007	42
4211892	Painel	268	420005	42
4211900	Palhoça	97	420001	42
4212007	Palma Sola	19	420009	42
4212056	Palmeira	\N	420005	42
4212106	Palmitos	19	420007	42
4212205	Papanduva	380	420017	42
4212239	Paraíso	19	420009	42
4212254	Passo de Torres	\N	420004	42
4212270	Passos Maia	17	420011	42
4212304	Paulo Lopes	97	420001	42
4212403	Pedras Grandes	\N	420003	42
4212502	Penha	\N	420020	42
4212601	Peritiba	\N	420010	42
4212650	Pescaria Brava	\N	420003	42
4212700	Petrolândia	71	420024	42
4212809	Balneário Piçarras	\N	420020	42
4212908	Pinhalzinho	19	420007	42
4213005	Pinheiro Preto	\N	420015	42
4213104	Piratuba	17	420010	42
4213153	Planalto Alegre	\N	420007	42
4213203	Pomerode	\N	420019	42
4213302	Ponte Alta	\N	420005	42
4213351	Ponte Alta do Norte	268	420006	42
4213401	Ponte Serrada	17	420011	42
4213500	Porto Belo	97	420020	42
4213609	Porto União	380	420017	42
4213708	Pouso Redondo	\N	420022	42
4213807	Praia Grande	736	420004	42
4213906	Presidente Castello Branco	\N	420010	42
4214003	Presidente Getúlio	\N	420023	42
4214102	Presidente Nereu	\N	420022	42
4214151	Princesa	19	420009	42
4214201	Quilombo	19	420007	42
4214300	Rancho Queimado	97	420001	42
4214409	Rio das Antas	380	420015	42
4214508	Rio do Campo	\N	420022	42
4214607	Rio do Oeste	\N	420022	42
4214706	Rio dos Cedros	184	420019	42
4214805	Rio do Sul	71	420022	42
4214904	Rio Fortuna	\N	420003	42
4215000	Rio Negrinho	184	420018	42
4215059	Rio Rufino	\N	420005	42
4215075	Riqueza	\N	420007	42
4215109	Rodeio	\N	420019	42
4215208	Romelândia	19	420009	42
4215307	Salete	\N	420022	42
4215356	Saltinho	\N	420012	42
4215406	Salto Veloso	\N	420015	42
4215455	Sangão	\N	420003	42
4215505	Santa Cecília	380	420006	42
4215554	Santa Helena	\N	420009	42
4215604	Santa Rosa de Lima	\N	420003	42
4215653	Santa Rosa do Sul	\N	420004	42
4215679	Santa Terezinha	71	420022	42
4215687	Santa Terezinha do Progresso	\N	420012	42
4215695	Santiago do Sul	\N	420007	42
4215703	Santo Amaro da Imperatriz	97	420001	42
4215752	São Bernardino	\N	420013	42
4215802	São Bento do Sul	\N	420018	42
4215901	São Bonifácio	\N	420001	42
4216008	São Carlos	87	420007	42
4216057	São Cristovão do Sul	268	420006	42
4216107	São Domingos	19	420011	42
4216206	São Francisco do Sul	184	420016	42
4216255	São João do Oeste	\N	420009	42
4216305	São João Batista	\N	420021	42
4216354	São João do Itaperiú	\N	420016	42
4216404	São João do Sul	\N	420004	42
4216503	São Joaquim	268	420005	42
4216602	São José	97	420001	42
4216701	São José do Cedro	19	420009	42
4216800	São José do Cerrito	268	420005	42
4216909	São Lourenço do Oeste	19	420013	42
4217006	São Ludgero	\N	420003	42
4217105	São Martinho	\N	420003	42
4217154	São Miguel da Boa Vista	\N	420012	42
4217204	São Miguel do Oeste	19	420009	42
4217253	São Pedro de Alcântara	\N	420001	42
4217303	Saudades	19	420007	42
4217402	Schroeder	\N	420016	42
4217501	Seara	19	420010	42
4217550	Serra Alta	\N	420007	42
4217600	Siderópolis	\N	420002	42
4217709	Sombrio	\N	420004	42
4217758	Sul Brasil	\N	420007	42
4217808	Taió	71	420022	42
4217907	Tangará	17	420015	42
4217956	Tigrinhos	\N	420012	42
4218004	Tijucas	97	420020	42
4218103	Timbé do Sul	\N	420004	42
4218202	Timbó	\N	420019	42
4218251	Timbó Grande	380	420014	42
4218301	Três Barras	380	420017	42
4218350	Treviso	736	420002	42
4218400	Treze de Maio	333	420003	42
4218509	Treze Tílias	\N	420008	42
4218608	Trombudo Central	\N	420022	42
4218707	Tubarão	\N	420003	42
4218756	Tunápolis	\N	420009	42
4218806	Turvo	\N	420004	42
4218855	União do Oeste	\N	420007	42
4218905	Urubici	268	420005	42
4218954	Urupema	\N	420005	42
4219002	Urussanga	\N	420002	42
4219101	Vargeão	19	420011	42
4219150	Vargem	17	420008	42
4219176	Vargem Bonita	17	420008	42
4219200	Vidal Ramos	71	420024	42
4219309	Videira	\N	420015	42
4219358	Vitor Meireles	71	420023	42
4219408	Witmarsum	\N	420023	42
4219507	Xanxerê	19	420011	42
4219606	Xavantina	19	420011	42
4219705	Xaxim	\N	420007	42
4219853	Zortéa	17	420008	42
4220000	Balneário Rincão	\N	420002	42
4300034	Aceguá	556	430010	43
4300059	Água Santa	86	430032	43
4300109	Agudo	99	430011	43
4300208	Ajuricaba	109	430018	43
4300307	Alecrim	133	430019	43
4300406	Alegrete	135	430015	43
4300455	Alegria	133	430023	43
4300471	Almirante Tamandaré do Sul	86	430028	43
4300505	Alpestre	156	430035	43
4300554	Alto Alegre	\N	430025	43
4300570	Alto Feliz	\N	430036	43
4300604	Alvorada	180	430001	43
4300638	Amaral Ferrador	\N	430005	43
4300646	Ametista do Sul	\N	430029	43
4300661	André da Rocha	\N	430038	43
4300703	Anta Gorda	\N	430043	43
4300802	Antônio Prado	\N	430036	43
4300851	Arambaré	180	430005	43
4300877	Araricá	\N	430002	43
4300901	Aratiba	345	430026	43
4301008	Arroio do Meio	212	430041	43
4301057	Arroio do Sal	\N	430008	43
4301073	Arroio do Padre	\N	430009	43
4301107	Arroio dos Ratos	180	430001	43
4301206	Arroio do Tigre	109	430042	43
4301305	Arroio Grande	378	430009	43
4301404	Arvorezinha	212	430031	43
4301503	Augusto Pestana	109	430018	43
4301552	Áurea	\N	430026	43
4301602	Bagé	556	430010	43
4301636	Balneário Pinhal	\N	430003	43
4301651	Barão	\N	430037	43
4301701	Barão de Cotegipe	\N	430026	43
4301750	Barão do Triunfo	\N	430001	43
4301800	Barracão	250	430033	43
4301859	Barra do Guarita	\N	430021	43
4301875	Barra do Quaraí	\N	430015	43
4301909	Barra do Ribeiro	180	430001	43
4301925	Barra do Rio Azul	\N	430026	43
4301958	Barra Funda	\N	430028	43
4302006	Barros Cassal	\N	430031	43
4302055	Benjamin Constant do Sul	345	430026	43
4302105	Bento Gonçalves	230	430037	43
4302154	Boa Vista das Missões	156	430029	43
4302204	Boa Vista do Buricá	\N	430023	43
4302220	Boa Vista do Cadeado	\N	430027	43
4302238	Boa Vista do Incra	109	430027	43
4302253	Boa Vista do Sul	\N	430037	43
4302303	Bom Jesus	250	430039	43
4302352	Bom Princípio	\N	430002	43
4302378	Bom Progresso	\N	430021	43
4302402	Bom Retiro do Sul	\N	430041	43
4302451	Boqueirão do Leão	\N	430041	43
4302501	Bossoroca	133	430022	43
4302584	Bozano	\N	430018	43
4302600	Braga	\N	430021	43
4302659	Brochier	\N	430007	43
4302709	Butiá	180	430006	43
4302808	Caçapava do Sul	99	430012	43
4302907	Cacequi	556	430011	43
4303004	Cachoeira do Sul	99	430013	43
4303103	Cachoeirinha	\N	430001	43
4303202	Cacique Doble	250	430033	43
4303301	Caibaté	133	430022	43
4303400	Caiçara	\N	430029	43
4303509	Camaquã	180	430005	43
4303558	Camargo	\N	430030	43
4303608	Cambará do Sul	230	430036	43
4303673	Campestre da Serra	\N	430036	43
4303707	Campina das Missões	\N	430019	43
4303806	Campinas do Sul	345	430026	43
4303905	Campo Bom	\N	430002	43
4304002	Campo Novo	\N	430021	43
4304101	Campos Borges	\N	430025	43
4304200	Candelária	212	430040	43
4304309	Cândido Godói	\N	430019	43
4304358	Candiota	556	430010	43
4304408	Canela	338	430036	43
4304507	Canguçu	379	430009	43
4304606	Canoas	180	430001	43
4304614	Canudos do Vale	\N	430041	43
4304622	Capão Bonito do Sul	\N	430033	43
4304630	Capão da Canoa	\N	430003	43
4304655	Capão do Cipó	133	430014	43
4304663	Capão do Leão	379	430009	43
4304671	Capivari do Sul	782	430001	43
4304689	Capela de Santana	180	430002	43
4304697	Capitão	\N	430041	43
4304705	Carazinho	86	430028	43
4304713	Caraá	\N	430001	43
4304804	Carlos Barbosa	\N	430037	43
4304853	Carlos Gomes	\N	430026	43
4304903	Casca	86	430030	43
4304952	Caseiros	250	430033	43
4305009	Catuípe	133	430018	43
4305108	Caxias do Sul	230	430036	43
4305116	Centenário	\N	430026	43
4305124	Cerrito	\N	430009	43
4305132	Cerro Branco	\N	430013	43
4305157	Cerro Grande	\N	430034	43
4305173	Cerro Grande do Sul	\N	430005	43
4305207	Cerro Largo	\N	430024	43
4305306	Chapada	109	430028	43
4305355	Charqueadas	180	430006	43
4305371	Charrua	86	430032	43
4305405	Chiapetta	\N	430018	43
4305439	Chuí	\N	430009	43
4305447	Chuvisca	\N	430005	43
4305454	Cidreira	\N	430003	43
4305504	Ciríaco	\N	430030	43
4305587	Colinas	\N	430041	43
4305603	Colorado	\N	430028	43
4305702	Condor	\N	430018	43
4305801	Constantina	156	430028	43
4305835	Coqueiro Baixo	\N	430041	43
4305850	Coqueiros do Sul	86	430028	43
4305871	Coronel Barros	\N	430018	43
4305900	Coronel Bicaco	156	430018	43
4305934	Coronel Pilar	\N	430037	43
4305959	Cotiporã	\N	430037	43
4305975	Coxilha	\N	430025	43
4306007	Crissiumal	156	430021	43
4306056	Cristal	\N	430005	43
4306072	Cristal do Sul	\N	430029	43
4306106	Cruz Alta	109	430027	43
4306130	Cruzaltense	\N	430026	43
4306205	Cruzeiro do Sul	212	430041	43
4306304	David Canabarro	86	430030	43
4306320	Derrubadas	156	430021	43
4306353	Dezesseis de Novembro	\N	430022	43
4306379	Dilermando de Aguiar	\N	430011	43
4306403	Dois Irmãos	\N	430002	43
4306429	Dois Irmãos das Missões	\N	430034	43
4306452	Dois Lajeados	\N	430038	43
4306502	Dom Feliciano	\N	430005	43
4306551	Dom Pedro de Alcântara	\N	430008	43
4306601	Dom Pedrito	556	430010	43
4306700	Dona Francisca	\N	430011	43
4306734	Doutor Maurício Cardoso	133	430023	43
4306759	Doutor Ricardo	212	430043	43
4306767	Eldorado do Sul	180	430001	43
4306809	Encantado	\N	430043	43
4306908	Encruzilhada do Sul	212	430040	43
4306924	Engenho Velho	156	430028	43
4306932	Entre-Ijuís	133	430020	43
4306957	Entre Rios do Sul	\N	430026	43
4306973	Erebango	345	430026	43
4307005	Erechim	345	430026	43
4307054	Ernestina	\N	430025	43
4307104	Herval	379	430009	43
4307203	Erval Grande	\N	430026	43
4307302	Erval Seco	156	430029	43
4307401	Esmeralda	250	430039	43
4307450	Esperança do Sul	156	430021	43
4307500	Espumoso	\N	430025	43
4307559	Estação	\N	430026	43
4307609	Estância Velha	\N	430002	43
4307708	Esteio	180	430001	43
4307807	Estrela	180	430041	43
4307815	Estrela Velha	\N	430042	43
4307831	Eugênio de Castro	\N	430020	43
4307864	Fagundes Varela	\N	430037	43
4307906	Farroupilha	\N	430036	43
4308003	Faxinal do Soturno	\N	430011	43
4308052	Faxinalzinho	345	430026	43
4308078	Fazenda Vilanova	\N	430041	43
4308102	Feliz	\N	430036	43
4308201	Flores da Cunha	\N	430036	43
4308250	Floriano Peixoto	345	430026	43
4308300	Fontoura Xavier	109	430031	43
4308409	Formigueiro	\N	430011	43
4308433	Forquetinha	\N	430041	43
4308458	Fortaleza dos Valos	\N	430027	43
4308508	Frederico Westphalen	156	430029	43
4308607	Garibaldi	\N	430037	43
4308656	Garruchos	\N	430022	43
4308706	Gaurama	345	430026	43
4308805	General Câmara	180	430006	43
4308854	Gentil	86	430030	43
4308904	Getúlio Vargas	\N	430026	43
4309001	Giruá	133	430020	43
4309050	Glorinha	\N	430001	43
4309100	Gramado	\N	430036	43
4309126	Gramado dos Loureiros	156	430035	43
4309159	Gramado Xavier	\N	430040	43
4309209	Gravataí	\N	430001	43
4309258	Guabiju	\N	430038	43
4309308	Guaíba	180	430001	43
4309407	Guaporé	\N	430038	43
4309506	Guarani das Missões	133	430020	43
4309555	Harmonia	\N	430002	43
4309571	Herveiras	\N	430040	43
4309605	Horizontina	\N	430023	43
4309654	Hulha Negra	556	430010	43
4309704	Humaitá	\N	430021	43
4309753	Ibarama	\N	430042	43
4309803	Ibiaçá	\N	430032	43
4309902	Ibiraiaras	250	430033	43
4309951	Ibirapuitã	\N	430031	43
4310009	Ibirubá	\N	430027	43
4310108	Igrejinha	\N	430004	43
4310207	Ijuí	109	430018	43
4310306	Ilópolis	\N	430043	43
4310330	Imbé	782	430003	43
4310363	Imigrante	\N	430041	43
4310405	Independência	\N	430023	43
4310413	Inhacorá	\N	430018	43
4310439	Ipê	250	430036	43
4310462	Ipiranga do Sul	\N	430026	43
4310504	Iraí	156	430029	43
4310538	Itaara	\N	430011	43
4310553	Itacurubi	135	430014	43
4310579	Itapuca	\N	430031	43
4310603	Itaqui	\N	430017	43
4310652	Itati	\N	430003	43
4310702	Itatiba do Sul	\N	430026	43
4310751	Ivorá	\N	430011	43
4310801	Ivoti	\N	430002	43
4310850	Jaboticaba	156	430029	43
4310876	Jacuizinho	\N	430027	43
4310900	Jacutinga	\N	430026	43
4311007	Jaguarão	379	430009	43
4311106	Jaguari	\N	430011	43
4311122	Jaquirana	\N	430039	43
4311130	Jari	\N	430011	43
4311155	Jóia	109	430018	43
4311205	Júlio de Castilhos	378	430011	43
4311239	Lagoa Bonita do Sul	99	430042	43
4311254	Lagoão	\N	430042	43
4311270	Lagoa dos Três Cantos	\N	430025	43
4311304	Lagoa Vermelha	250	430033	43
4311403	Lajeado	212	430041	43
4311429	Lajeado do Bugre	\N	430034	43
4311502	Lavras do Sul	556	430012	43
4311601	Liberato Salzano	\N	430029	43
4311627	Lindolfo Collor	\N	430002	43
4311643	Linha Nova	\N	430002	43
4311700	Machadinho	250	430033	43
4311718	Maçambará	\N	430017	43
4311734	Mampituba	\N	430008	43
4311759	Manoel Viana	135	430015	43
4311775	Maquiné	782	430003	43
4311791	Maratá	\N	430007	43
4311809	Marau	86	430030	43
4311908	Marcelino Ramos	345	430026	43
4311981	Mariana Pimentel	\N	430001	43
4312005	Mariano Moro	\N	430026	43
4312054	Marques de Souza	212	430041	43
4312104	Mata	\N	430011	43
4312138	Mato Castelhano	86	430025	43
4312153	Mato Leitão	\N	430040	43
4312179	Mato Queimado	\N	430024	43
4312203	Maximiliano de Almeida	250	430032	43
4312252	Minas do Leão	\N	430006	43
4312302	Miraguaí	\N	430021	43
4312351	Montauri	\N	430038	43
4312377	Monte Alegre dos Campos	\N	430039	43
4312385	Monte Belo do Sul	\N	430037	43
4312401	Montenegro	180	430007	43
4312427	Mormaço	\N	430031	43
4312443	Morrinhos do Sul	\N	430008	43
4312450	Morro Redondo	\N	430009	43
4312476	Morro Reuter	\N	430002	43
4312500	Mostardas	730	430001	43
4312609	Muçum	\N	430043	43
4312617	Muitos Capões	\N	430039	43
4312625	Muliterno	86	430030	43
4312658	Não-Me-Toque	86	430028	43
4312674	Nicolau Vergueiro	\N	430025	43
4312708	Nonoai	156	430035	43
4312757	Nova Alvorada	\N	430030	43
4312807	Nova Araçá	\N	430038	43
4312906	Nova Bassano	\N	430038	43
4312955	Nova Boa Vista	\N	430028	43
4313003	Nova Bréscia	212	430043	43
4313011	Nova Candelária	\N	430023	43
4313037	Nova Esperança do Sul	\N	430014	43
4313060	Nova Hartz	338	430002	43
4313086	Nova Pádua	\N	430036	43
4313102	Nova Palma	\N	430011	43
4313201	Nova Petrópolis	\N	430036	43
4313300	Nova Prata	\N	430038	43
4313334	Nova Ramada	\N	430018	43
4313359	Nova Roma do Sul	\N	430036	43
4313375	Nova Santa Rita	180	430001	43
4313391	Novo Cabrais	\N	430013	43
4313409	Novo Hamburgo	338	430002	43
4313425	Novo Machado	133	430019	43
4313441	Novo Tiradentes	\N	430029	43
4313466	Novo Xingu	\N	430028	43
4313490	Novo Barreiro	156	430034	43
4313508	Osório	782	430003	43
4313607	Paim Filho	\N	430032	43
4313656	Palmares do Sul	782	430001	43
4313706	Palmeira das Missões	156	430034	43
4313805	Palmitinho	156	430029	43
4313904	Panambi	\N	430018	43
4313953	Pantano Grande	212	430040	43
4314001	Paraí	\N	430038	43
4314027	Paraíso do Sul	\N	430013	43
4314035	Pareci Novo	\N	430007	43
4314050	Parobé	\N	430004	43
4314068	Passa Sete	\N	430042	43
4314076	Passo do Sobrado	212	430040	43
4314100	Passo Fundo	86	430025	43
4314134	Paulo Bento	\N	430026	43
4314159	Paverama	\N	430041	43
4314175	Pedras Altas	556	430010	43
4314209	Pedro Osório	379	430009	43
4314308	Pejuçara	\N	430018	43
4314407	Pelotas	379	430009	43
4314423	Picada Café	\N	430036	43
4314456	Pinhal	\N	430029	43
4314464	Pinhal da Serra	345	430039	43
4314472	Pinhal Grande	\N	430011	43
4314498	Pinheirinho do Vale	\N	430029	43
4314506	Pinheiro Machado	556	430010	43
4314548	Pinto Bandeira	\N	430037	43
4314555	Pirapó	133	430022	43
4314605	Piratini	379	430009	43
4314704	Planalto	156	430035	43
4314753	Poço das Antas	\N	430041	43
4314779	Pontão	86	430025	43
4314787	Ponte Preta	\N	430026	43
4314803	Portão	\N	430002	43
4314902	Porto Alegre	180	430001	43
4315008	Porto Lucena	133	430019	43
4315057	Porto Mauá	133	430019	43
4315073	Porto Vera Cruz	\N	430019	43
4315107	Porto Xavier	133	430024	43
4315131	Pouso Novo	\N	430041	43
4315149	Presidente Lucena	\N	430002	43
4315156	Progresso	\N	430041	43
4315172	Protásio Alves	\N	430038	43
4315206	Putinga	\N	430043	43
4315305	Quaraí	\N	430016	43
4315313	Quatro Irmãos	\N	430026	43
4315321	Quevedos	\N	430011	43
4315354	Quinze de Novembro	\N	430027	43
4315404	Redentora	156	430021	43
4315453	Relvado	\N	430043	43
4315503	Restinga Seca	378	430011	43
4315552	Rio dos Índios	156	430035	43
4315602	Rio Grande	730	430009	43
4315701	Rio Pardo	212	430040	43
4315750	Riozinho	\N	430004	43
4315800	Roca Sales	\N	430043	43
4315909	Rodeio Bonito	\N	430029	43
4315958	Rolador	\N	430022	43
4316006	Rolante	\N	430004	43
4316105	Ronda Alta	86	430025	43
4316204	Rondinha	\N	430028	43
4316303	Roque Gonzales	133	430024	43
4316402	Rosário do Sul	556	430016	43
4316428	Sagrada Família	\N	430034	43
4316436	Saldanha Marinho	\N	430028	43
4316451	Salto do Jacuí	109	430027	43
4316477	Salvador das Missões	\N	430024	43
4316501	Salvador do Sul	\N	430007	43
4316600	Sananduva	250	430032	43
4316709	Santa Bárbara do Sul	109	430027	43
4316733	Santa Cecília do Sul	\N	430032	43
4316758	Santa Clara do Sul	\N	430041	43
4316808	Santa Cruz do Sul	212	430040	43
4316907	Santa Maria	378	430011	43
4316956	Santa Maria do Herval	\N	430002	43
4316972	Santa Margarida do Sul	556	430012	43
4317004	Santana da Boa Vista	\N	430012	43
4317103	Sant'Ana do Livramento	\N	430016	43
4317202	Santa Rosa	133	430019	43
4317251	Santa Tereza	\N	430037	43
4317301	Santa Vitória do Palmar	730	430009	43
4317400	Santiago	181	430014	43
4317509	Santo Ângelo	133	430020	43
4317558	Santo Antônio do Palma	\N	430030	43
4317608	Santo Antônio da Patrulha	\N	430001	43
4317707	Santo Antônio das Missões	133	430022	43
4317756	Santo Antônio do Planalto	\N	430028	43
4317806	Santo Augusto	156	430018	43
4317905	Santo Cristo	133	430019	43
4317954	Santo Expedito do Sul	\N	430032	43
4318002	São Borja	135	430017	43
4318051	São Domingos do Sul	\N	430030	43
4318101	São Francisco de Assis	135	430011	43
4318200	São Francisco de Paula	230	430036	43
4318309	São Gabriel	556	430012	43
4318408	São Jerônimo	180	430006	43
4318424	São João da Urtiga	\N	430032	43
4318432	São João do Polêsine	\N	430011	43
4318440	São Jorge	\N	430038	43
4318457	São José das Missões	\N	430034	43
4318465	São José do Herval	\N	430031	43
4318481	São José do Hortêncio	\N	430002	43
4318499	São José do Inhacorá	\N	430023	43
4318507	São José do Norte	730	430009	43
4318606	São José do Ouro	\N	430033	43
4318614	São José do Sul	\N	430007	43
4318622	São José dos Ausentes	\N	430039	43
4318705	São Leopoldo	338	430002	43
4318804	São Lourenço do Sul	379	430009	43
4318903	São Luiz Gonzaga	133	430022	43
4319000	São Marcos	\N	430036	43
4319109	São Martinho	\N	430021	43
4319125	São Martinho da Serra	\N	430011	43
4319158	São Miguel das Missões	133	430020	43
4319208	São Nicolau	133	430022	43
4319307	São Paulo das Missões	\N	430024	43
4319356	São Pedro da Serra	\N	430007	43
4319364	São Pedro das Missões	\N	430034	43
4319372	São Pedro do Butiá	\N	430024	43
4319406	São Pedro do Sul	\N	430011	43
4319505	São Sebastião do Caí	\N	430002	43
4319604	São Sepé	378	430011	43
4319703	São Valentim	345	430026	43
4319711	São Valentim do Sul	\N	430037	43
4319737	São Valério do Sul	\N	430018	43
4319752	São Vendelino	\N	430002	43
4319802	São Vicente do Sul	\N	430011	43
4319901	Sapiranga	338	430002	43
4320008	Sapucaia do Sul	180	430001	43
4320107	Sarandi	86	430028	43
4320206	Seberi	156	430029	43
4320230	Sede Nova	\N	430021	43
4320263	Segredo	\N	430042	43
4320305	Selbach	\N	430027	43
4320321	Senador Salgado Filho	\N	430019	43
4320354	Sentinela do Sul	\N	430005	43
4320404	Serafina Corrêa	86	430038	43
4320453	Sério	\N	430041	43
4320503	Sertão	86	430025	43
4320552	Sertão Santana	\N	430001	43
4320578	Sete de Setembro	\N	430020	43
4320602	Severiano de Almeida	\N	430026	43
4320651	Silveira Martins	\N	430011	43
4320677	Sinimbu	\N	430040	43
4320701	Sobradinho	92	430042	43
4320800	Soledade	109	430031	43
4320859	Tabaí	\N	430041	43
4320909	Tapejara	86	430032	43
4321006	Tapera	\N	430025	43
4321105	Tapes	117	430005	43
4321204	Taquara	338	430004	43
4321303	Taquari	180	430041	43
4321329	Taquaruçu do Sul	156	430029	43
4321352	Tavares	730	430001	43
4321402	Tenente Portela	156	430021	43
4321436	Terra de Areia	\N	430003	43
4321451	Teutônia	180	430041	43
4321469	Tio Hugo	\N	430025	43
4321477	Tiradentes do Sul	156	430021	43
4321493	Toropi	\N	430011	43
4321501	Torres	782	430008	43
4321600	Tramandaí	\N	430003	43
4321626	Travesseiro	\N	430041	43
4321634	Três Arroios	\N	430026	43
4321667	Três Cachoeiras	782	430008	43
4321709	Três Coroas	\N	430004	43
4321808	Três de Maio	\N	430023	43
4321832	Três Forquilhas	\N	430008	43
4321857	Três Palmeiras	\N	430025	43
4321907	Três Passos	156	430021	43
4321956	Trindade do Sul	\N	430035	43
4322004	Triunfo	180	430006	43
4322103	Tucunduva	133	430019	43
4322152	Tunas	\N	430042	43
4322186	Tupanci do Sul	\N	430033	43
4322202	Tupanciretã	378	430027	43
4322251	Tupandi	\N	430002	43
4322301	Tuparendi	133	430019	43
4322327	Turuçu	\N	430009	43
4322343	Ubiretama	\N	430024	43
4322350	União da Serra	\N	430038	43
4322376	Unistalda	\N	430014	43
4322400	Uruguaiana	135	430015	43
4322509	Vacaria	250	430039	43
4322525	Vale Verde	\N	430040	43
4322533	Vale do Sol	212	430040	43
4322541	Vale Real	\N	430036	43
4322558	Vanini	\N	430030	43
4322608	Venâncio Aires	212	430040	43
4322707	Vera Cruz	\N	430040	43
4322806	Veranópolis	\N	430037	43
4322855	Vespasiano Correa	\N	430043	43
4322905	Viadutos	345	430026	43
4323002	Viamão	180	430001	43
4323101	Vicente Dutra	156	430029	43
4323200	Victor Graeff	\N	430025	43
4323309	Vila Flores	\N	430037	43
4323358	Vila Lângaro	\N	430032	43
4323408	Vila Maria	\N	430030	43
4323457	Vila Nova do Sul	\N	430012	43
4323507	Vista Alegre	\N	430029	43
4323606	Vista Alegre do Prata	\N	430038	43
4323705	Vista Gaúcha	\N	430021	43
4323754	Vitória das Missões	\N	430020	43
4323770	Westfalia	\N	430041	43
4323804	Xangri-lá	\N	430003	43
5000203	Água Clara	291	500002	50
5000252	Alcinópolis	121	500004	50
5000609	Amambai	207	500009	50
5000708	Anastácio	240	500012	50
5000807	Anaurilândia	207	500007	50
5000856	Angélica	207	500007	50
5000906	Antônio João	207	500008	50
5001003	Aparecida do Taboado	291	500003	50
5001102	Aquidauana	240	500012	50
5001243	Aral Moreira	207	500008	50
5001508	Bandeirantes	263	500001	50
5001904	Bataguassu	207	500002	50
5002001	Batayporã	\N	500007	50
5002100	Bela Vista	240	500011	50
5002159	Bodoquena	240	500012	50
5002209	Bonito	240	500011	50
5002308	Brasilândia	291	500002	50
5002407	Caarapó	207	500005	50
5002605	Camapuã	121	500001	50
5002704	Campo Grande	263	500001	50
5002803	Caracol	240	500011	50
5002902	Cassilândia	291	500003	50
5002951	Chapadão do Sul	291	500003	50
5003108	Corguinho	263	500001	50
5003157	Coronel Sapucaia	207	500009	50
5003207	Corumbá	767	500010	50
5003256	Costa Rica	121	500004	50
5003306	Coxim	121	500004	50
5003454	Deodápolis	207	500005	50
5003488	Dois Irmãos do Buriti	240	500001	50
5003504	Douradina	207	500005	50
5003702	Dourados	207	500005	50
5003751	Eldorado	207	500006	50
5003801	Fátima do Sul	\N	500005	50
5003900	Figueirão	121	500004	50
5004007	Glória de Dourados	207	500005	50
5004106	Guia Lopes da Laguna	240	500011	50
5004304	Iguatemi	207	500006	50
5004403	Inocência	291	500003	50
5004502	Itaporã	207	500005	50
5004601	Itaquiraí	207	500006	50
5004700	Ivinhema	207	500007	50
5004809	Japorã	207	500006	50
5004908	Jaraguari	263	500001	50
5005004	Jardim	240	500011	50
5005103	Jateí	207	500005	50
5005152	Juti	207	500005	50
5005202	Ladário	767	500010	50
5005251	Laguna Carapã	207	500005	50
5005400	Maracaju	207	500005	50
5005608	Miranda	240	500012	50
5005681	Mundo Novo	207	500006	50
5005707	Naviraí	207	500006	50
5005806	Nioaque	240	500011	50
5006002	Nova Alvorada do Sul	207	500001	50
5006200	Nova Andradina	207	500007	50
5006259	Novo Horizonte do Sul	207	500007	50
5006275	Paraíso das Águas	\N	500003	50
5006309	Paranaíba	291	500003	50
5006358	Paranhos	207	500009	50
5006408	Pedro Gomes	121	500004	50
5006606	Ponta Porã	207	500008	50
5006903	Porto Murtinho	240	500011	50
5007109	Ribas do Rio Pardo	263	500001	50
5007208	Rio Brilhante	207	500005	50
5007307	Rio Negro	121	500001	50
5007406	Rio Verde de Mato Grosso	121	500004	50
5007505	Rochedo	263	500001	50
5007554	Santa Rita do Pardo	291	500002	50
5007695	São Gabriel do Oeste	121	500001	50
5007703	Sete Quedas	207	500009	50
5007802	Selvíria	291	500002	50
5007901	Sidrolândia	263	500001	50
5007935	Sonora	121	500004	50
5007950	Tacuru	207	500009	50
5007976	Taquarussu	207	500007	50
5008008	Terenos	263	500001	50
5008305	Três Lagoas	291	500002	50
5008404	Vicentina	\N	500005	50
5100102	Acorizal	43	510001	51
5100201	Água Boa	73	510015	51
5100250	Alta Floresta	158	510010	51
5100300	Alto Araguaia	158	510016	51
5100359	Alto Boa Vista	176	510014	51
5100409	Alto Garças	174	510016	51
5100508	Alto Paraguai	185	510003	51
5100607	Alto Taquari	174	510016	51
5100805	Apiacás	158	510010	51
5101001	Araguaiana	\N	510013	51
5101209	Araguainha	\N	510016	51
5101258	Araputanga	557	510006	51
5101308	Arenápolis	185	510003	51
5101407	Aripuanã	160	510009	51
5101605	Barão de Melgaço	\N	510001	51
5101704	Barra do Bugres	557	510002	51
5101803	Barra do Garças	73	510013	51
5101852	Bom Jesus do Araguaia	43	510014	51
5101902	Brasnorte	185	510002	51
5102504	Cáceres	557	510004	51
5102603	Campinápolis	73	510015	51
5102637	Campo Novo do Parecis	\N	510002	51
5102678	Campo Verde	425	510001	51
5102686	Campos de Júlio	160	510005	51
5102694	Canabrava do Norte	176	510014	51
5102702	Canarana	73	510015	51
5102793	Carlinda	158	510010	51
5102850	Castanheira	160	510009	51
5103007	Chapada dos Guimarães	425	510001	51
5103056	Cláudia	158	510007	51
5103106	Cocalinho	73	510015	51
5103205	Colíder	158	510007	51
5103254	Colniza	160	510009	51
5103304	Comodoro	160	510005	51
5103353	Confresa	176	510014	51
5103361	Conquista D'Oeste	\N	510005	51
5103379	Cotriguaçu	746	510009	51
5103403	Cuiabá	43	510001	51
5103437	Curvelândia	557	510004	51
5103452	Denise	185	510002	51
5103502	Diamantino	185	510003	51
5103601	Dom Aquino	425	510018	51
5103700	Feliz Natal	158	510007	51
5103809	Figueirópolis D'Oeste	\N	510006	51
5103858	Gaúcha do Norte	158	510015	51
5103908	General Carneiro	73	510013	51
5103957	Glória D'Oeste	\N	510006	51
5104104	Guarantã do Norte	158	510011	51
5104203	Guiratinga	174	510016	51
5104500	Indiavaí	\N	510006	51
5104526	Ipiranga do Norte	43	510008	51
5104542	Itanhangá	158	510008	51
5104559	Itaúba	158	510007	51
5104609	Itiquira	425	510016	51
5104807	Jaciara	425	510018	51
5104906	Jangada	43	510001	51
5105002	Jauru	557	510006	51
5105101	Juara	158	510012	51
5105150	Juína	160	510009	51
5105176	Juruena	160	510009	51
5105200	Juscimeira	425	510018	51
5105234	Lambari D'Oeste	\N	510004	51
5105259	Lucas do Rio Verde	185	510008	51
5105309	Luciara	176	510014	51
5105507	Vila Bela da Santíssima Trindade	557	510005	51
5105580	Marcelândia	158	510007	51
5105606	Matupá	158	510011	51
5105622	Mirassol D'Oeste	\N	510006	51
5105903	Nobres	43	510001	51
5106000	Nortelândia	185	510003	51
5106109	Nossa Senhora do Livramento	557	510001	51
5106158	Nova Bandeirantes	158	510010	51
5106174	Nova Nazaré	\N	510015	51
5106182	Nova Lacerda	160	510005	51
5106190	Nova Santa Helena	158	510007	51
5106208	Nova Brasilândia	425	510001	51
5106216	Nova Canaã do Norte	158	510007	51
5106224	Nova Mutum	185	510008	51
5106232	Nova Olímpia	557	510002	51
5106240	Nova Ubiratã	158	510008	51
5106257	Nova Xavantina	73	510013	51
5106265	Novo Mundo	158	510011	51
5106273	Novo Horizonte do Norte	\N	510012	51
5106281	Novo São Joaquim	73	510013	51
5106299	Paranaíta	158	510010	51
5106307	Paranatinga	158	510017	51
5106315	Novo Santo Antônio	\N	510014	51
5106372	Pedra Preta	425	510016	51
5106422	Peixoto de Azevedo	158	510011	51
5106455	Planalto da Serra	425	510001	51
5106505	Poconé	557	510001	51
5106653	Pontal do Araguaia	73	510013	51
5106703	Ponte Branca	\N	510013	51
5106752	Pontes e Lacerda	557	510005	51
5106778	Porto Alegre do Norte	176	510014	51
5106802	Porto dos Gaúchos	158	510012	51
5106828	Porto Esperidião	\N	510006	51
5106851	Porto Estrela	557	510002	51
5107008	Poxoréo	\N	510017	51
5107040	Primavera do Leste	174	510017	51
5107065	Querência	176	510015	51
5107107	São José dos Quatro Marcos	557	510006	51
5107156	Reserva do Cabaçal	557	510006	51
5107180	Ribeirão Cascalheira	176	510015	51
5107198	Ribeirãozinho	\N	510013	51
5107206	Rio Branco	557	510004	51
5107248	Santa Carmem	\N	510007	51
5107263	Santo Afonso	185	510003	51
5107297	São José do Povo	425	510016	51
5107305	São José do Rio Claro	185	510003	51
5107354	São José do Xingu	176	510014	51
5107404	São Pedro da Cipa	\N	510018	51
5107578	Rondolândia	160	510009	51
5107602	Rondonópolis	425	510016	51
5107701	Rosário Oeste	\N	510001	51
5107743	Santa Cruz do Xingu	176	510014	51
5107750	Salto do Céu	557	510004	51
5107768	Santa Rita do Trivelato	43	510008	51
5107776	Santa Terezinha	176	510014	51
5107792	Santo Antônio do Leste	174	510017	51
5107800	Santo Antônio do Leverger	43	510001	51
5107859	São Félix do Araguaia	176	510014	51
5107875	Sapezal	185	510002	51
5107883	Serra Nova Dourada	176	510014	51
5107909	Sinop	158	510007	51
5107925	Sorriso	158	510008	51
5107941	Tabaporã	158	510012	51
5107958	Tangará da Serra	185	510002	51
5108006	Tapurah	158	510008	51
5108055	Terra Nova do Norte	\N	510007	51
5108105	Tesouro	174	510016	51
5108204	Torixoréu	\N	510013	51
5108303	União do Sul	158	510007	51
5108352	Vale de São Domingos	\N	510005	51
5108402	Várzea Grande	43	510001	51
5108501	Vera	158	510008	51
5108600	Vila Rica	176	510014	51
5108808	Nova Guarita	158	510007	51
5108857	Nova Marilândia	185	510003	51
5108907	Nova Maringá	185	510003	51
5108956	Nova Monte Verde	158	510010	51
5200050	Abadia de Goiás	8	520001	52
5200100	Abadiânia	10	520002	52
5200134	Acreúna	45	520015	52
5200159	Adelândia	749	520003	52
5200175	Água Fria de Goiás	\N	520022	52
5200209	Água Limpa	83	520008	52
5200258	Águas Lindas de Goiás	\N	520020	52
5200308	Alexânia	10	520002	52
5200506	Aloândia	\N	520007	52
5200555	Alto Horizonte	\N	520017	52
5200605	Alto Paraíso de Goiás	82	520022	52
5200803	Alvorada do Norte	\N	520021	52
5200829	Amaralina	181	520017	52
5200852	Americano do Brasil	45	520003	52
5200902	Amorinópolis	\N	520014	52
5201108	Anápolis	10	520002	52
5201207	Anhanguera	\N	520004	52
5201306	Anicuns	45	520003	52
5201405	Aparecida de Goiânia	8	520001	52
5201454	Aparecida do Rio Doce	\N	520010	52
5201504	Aporé	289	520011	52
5201603	Araçu	\N	520003	52
5201702	Aragarças	45	520014	52
5201801	Aragoiânia	8	520001	52
5202155	Araguapaz	320	520005	52
5202353	Arenópolis	45	520014	52
5202502	Aruanã	320	520005	52
5202601	Aurilândia	\N	520013	52
5202809	Avelinópolis	\N	520003	52
5203104	Baliza	45	520014	52
5203203	Barro Alto	181	520018	52
5203302	Bela Vista de Goiás	8	520001	52
5203401	Bom Jardim de Goiás	45	520014	52
5203500	Bom Jesus de Goiás	83	520007	52
5203559	Bonfinópolis	\N	520001	52
5203575	Bonópolis	23	520016	52
5203609	Brazabrantes	8	520001	52
5203807	Britânia	50	520005	52
5203906	Buriti Alegre	\N	520007	52
5203939	Buriti de Goiás	50	520013	52
5203962	Buritinópolis	\N	520021	52
5204003	Cabeceiras	82	520020	52
5204102	Cachoeira Alta	289	520010	52
5204201	Cachoeira de Goiás	\N	520013	52
5204250	Cachoeira Dourada	\N	520007	52
5204300	Caçu	289	520010	52
5204409	Caiapônia	45	520011	52
5204508	Caldas Novas	264	520008	52
5204557	Caldazinha	\N	520001	52
5204607	Campestre de Goiás	8	520015	52
5204656	Campinaçu	181	520016	52
5204706	Campinorte	181	520017	52
5204805	Campo Alegre de Goiás	264	520004	52
5204854	Campo Limpo de Goiás	\N	520002	52
5204904	Campos Belos	\N	520021	52
5204953	Campos Verdes	\N	520018	52
5205000	Carmo do Rio Verde	50	520018	52
5205059	Castelândia	\N	520010	52
5205109	Catalão	264	520004	52
5205208	Caturaí	\N	520001	52
5205307	Cavalcante	82	520022	52
5205406	Ceres	50	520018	52
5205455	Cezarina	45	520015	52
5205471	Chapadão do Céu	289	520011	52
5205497	Cidade Ocidental	96	520019	52
5205513	Cocalzinho de Goiás	10	520020	52
5205521	Colinas do Sul	82	520017	52
5205703	Córrego do Ouro	\N	520013	52
5205802	Corumbá de Goiás	10	520002	52
5205901	Corumbaíba	264	520008	52
5206206	Cristalina	96	520019	52
5206305	Cristianópolis	\N	520009	52
5206404	Crixás	320	520018	52
5206503	Cromínia	\N	520009	52
5206602	Cumari	\N	520004	52
5206701	Damianópolis	\N	520021	52
5206800	Damolândia	\N	520003	52
5206909	Davinópolis	264	520004	52
5207105	Diorama	45	520014	52
5207253	Doverlândia	45	520011	52
5207352	Edealina	83	520009	52
5207402	Edéia	45	520015	52
5207501	Estrela do Norte	181	520016	52
5207535	Faina	320	520005	52
5207600	Fazenda Nova	50	520013	52
5207808	Firminópolis	45	520015	52
5207907	Flores de Goiás	82	520022	52
5208004	Formosa	82	520020	52
5208103	Formoso	181	520016	52
5208152	Gameleira de Goiás	\N	520002	52
5208301	Divinópolis de Goiás	82	520021	52
5208400	Goianápolis	\N	520002	52
5208509	Goiandira	264	520004	52
5208608	Goianésia	181	520018	52
5208707	Goiânia	8	520001	52
5208806	Goianira	8	520001	52
5208905	Goiás	50	520005	52
5209101	Goiatuba	83	520007	52
5209150	Gouvelândia	289	520012	52
5209200	Guapó	8	520001	52
5209291	Guaraíta	\N	520005	52
5209408	Guarani de Goiás	82	520021	52
5209457	Guarinos	\N	520018	52
5209606	Heitoraí	50	520003	52
5209705	Hidrolândia	8	520001	52
5209804	Hidrolina	\N	520017	52
5209903	Iaciara	82	520021	52
5209937	Inaciolândia	83	520012	52
5209952	Indiara	45	520015	52
5210000	Inhumas	8	520003	52
5210109	Ipameri	264	520004	52
5210158	Ipiranga de Goiás	\N	520018	52
5210208	Iporá	45	520014	52
5210307	Israelândia	45	520014	52
5210406	Itaberaí	50	520003	52
5210562	Itaguari	50	520003	52
5210604	Itaguaru	50	520003	52
5210802	Itajá	289	520010	52
5210901	Itapaci	181	520018	52
5211008	Itapirapuã	50	520005	52
5211206	Itapuranga	50	520005	52
5211305	Itarumã	289	520010	52
5211404	Itauçu	8	520003	52
5211503	Itumbiara	83	520007	52
5211602	Ivolândia	\N	520013	52
5211701	Jandaia	45	520015	52
5211800	Jaraguá	10	520002	52
5211909	Jataí	289	520011	52
5212006	Jaupaci	45	520014	52
5212055	Jesúpolis	\N	520002	52
5212105	Joviânia	83	520007	52
5212204	Jussara	50	520005	52
5212253	Lagoa Santa	289	520010	52
5212303	Leopoldo de Bulhões	\N	520002	52
5212501	Luziânia	96	520019	52
5212600	Mairipotaba	83	520009	52
5212709	Mambaí	82	520021	52
5212808	Mara Rosa	181	520017	52
5212907	Marzagão	\N	520008	52
5212956	Matrinchã	\N	520005	52
5213004	Maurilândia	\N	520010	52
5213053	Mimoso de Goiás	\N	520020	52
5213087	Minaçu	181	520016	52
5213103	Mineiros	289	520011	52
5213400	Moiporá	\N	520013	52
5213509	Monte Alegre de Goiás	202	520021	52
5213707	Montes Claros de Goiás	45	520014	52
5213756	Montividiu	289	520010	52
5213772	Montividiu do Norte	\N	520016	52
5213806	Morrinhos	83	520008	52
5213855	Morro Agudo de Goiás	\N	520018	52
5213905	Mossâmedes	50	520005	52
5214002	Mozarlândia	320	520005	52
5214051	Mundo Novo	320	520016	52
5214101	Mutunópolis	23	520016	52
5214408	Nazário	45	520015	52
5214507	Nerópolis	10	520001	52
5214606	Niquelândia	181	520017	52
5214705	Nova América	\N	520018	52
5214804	Nova Aurora	\N	520004	52
5214838	Nova Crixás	320	520005	52
5214861	Nova Glória	\N	520018	52
5214879	Nova Iguaçu de Goiás	\N	520017	52
5214903	Nova Roma	82	520021	52
5215009	Nova Veneza	10	520001	52
5215207	Novo Brasil	50	520005	52
5215231	Novo Gama	96	520019	52
5215256	Novo Planalto	23	520016	52
5215306	Orizona	264	520006	52
5215405	Ouro Verde de Goiás	10	520002	52
5215504	Ouvidor	\N	520004	52
5215603	Padre Bernardo	96	520020	52
5215652	Palestina de Goiás	\N	520014	52
5215702	Palmeiras de Goiás	45	520015	52
5215801	Palmelo	\N	520006	52
5215900	Palminópolis	\N	520015	52
5216007	Panamá	\N	520007	52
5216304	Paranaiguara	289	520012	52
5216403	Paraúna	45	520015	52
5216452	Perolândia	289	520011	52
5216809	Petrolina de Goiás	\N	520002	52
5216908	Pilar de Goiás	181	520018	52
5217104	Piracanjuba	83	520009	52
5217203	Piranhas	45	520014	52
5217302	Pirenópolis	10	520002	52
5217401	Pires do Rio	264	520006	52
5217609	Planaltina	82	520020	52
5217708	Pontalina	\N	520009	52
5218003	Porangatu	23	520016	52
5218052	Porteirão	83	520010	52
5218102	Portelândia	289	520011	52
5218300	Posse	82	520021	52
5218391	Professor Jamil	83	520009	52
5218508	Quirinópolis	289	520012	52
5218607	Rialma	181	520018	52
5218706	Rianápolis	\N	520018	52
5218789	Rio Quente	\N	520008	52
5218805	Rio Verde	289	520010	52
5218904	Rubiataba	320	520018	52
5219001	Sanclerlândia	50	520013	52
5219100	Santa Bárbara de Goiás	\N	520015	52
5219209	Santa Cruz de Goiás	264	520006	52
5219258	Santa Fé de Goiás	\N	520005	52
5219308	Santa Helena de Goiás	289	520010	52
5219357	Santa Isabel	181	520018	52
5219407	Santa Rita do Araguaia	289	520011	52
5219456	Santa Rita do Novo Destino	181	520018	52
5219506	Santa Rosa de Goiás	\N	520002	52
5219605	Santa Tereza de Goiás	181	520016	52
5219704	Santa Terezinha de Goiás	181	520018	52
5219712	Santo Antônio da Barra	289	520010	52
5219738	Santo Antônio de Goiás	8	520001	52
5219753	Santo Antônio do Descoberto	96	520019	52
5219803	São Domingos	82	520021	52
5219902	São Francisco de Goiás	\N	520002	52
5220009	São João d'Aliança	\N	520022	52
5220058	São João da Paraúna	\N	520015	52
5220108	São Luís de Montes Belos	45	520013	52
5220157	São Luíz do Norte	181	520017	52
5220207	São Miguel do Araguaia	770	520016	52
5220264	São Miguel do Passa Quatro	\N	520002	52
5220280	São Patrício	\N	520018	52
5220405	São Simão	289	520012	52
5220454	Senador Canedo	\N	520001	52
5220504	Serranópolis	289	520011	52
5220603	Silvânia	8	520002	52
5220686	Simolândia	\N	520021	52
5220702	Sítio d'Abadia	\N	520021	52
5221007	Taquaral de Goiás	50	520003	52
5221080	Teresina de Goiás	\N	520022	52
5221197	Terezópolis de Goiás	\N	520001	52
5221304	Três Ranchos	\N	520004	52
5221403	Trindade	8	520001	52
5221452	Trombas	\N	520016	52
5221502	Turvânia	\N	520015	52
5221551	Turvelândia	45	520010	52
5221577	Uirapuru	320	520018	52
5221601	Uruaçu	181	520017	52
5221700	Uruana	50	520018	52
5221809	Urutaí	264	520006	52
5221858	Valparaíso de Goiás	96	520019	52
5221908	Varjão	8	520001	52
5222005	Vianópolis	8	520002	52
5222054	Vicentinópolis	83	520009	52
5222203	Vila Boa	82	520022	52
5222302	Vila Propício	10	520018	52
5300108	Brasília	539	530001	53
\.


--
-- Data for Name: municipios_regiao; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.municipios_regiao (id, nome) FROM stdin;
1	Norte
2	Nordeste
3	Sudeste
4	Sul
5	Centro-Oeste
\.


--
-- Data for Name: municipios_regiaoimediata; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.municipios_regiaoimediata (id, nome, regiao_intermediaria_id) FROM stdin;
110001	Porto Velho	1101
110002	Ariquemes	1101
110003	Jaru	1101
110004	Ji-Paraná	1102
110005	Cacoal	1102
110006	Vilhena	1102
120001	Rio Branco	1201
120002	Brasiléia	1201
120003	Sena Madureira	1201
120004	Cruzeiro do Sul	1202
120005	Tarauacá	1202
130001	Manaus	1301
130002	São Gabriel da Cachoeira	1301
130003	Coari	1301
130004	Manacapuru	1301
130005	Tefé	1302
130006	Tabatinga	1302
130007	Eirunepé	1302
130008	Lábrea	1303
130009	Manicoré	1303
130010	Parintins	1304
130011	Itacoatiara	1304
140001	Boa Vista	1401
140002	Pacaraima	1401
140003	Rorainópolis	1402
140004	Caracaraí	1402
150001	Belém	1501
150002	Cametá	1501
150003	Abaetetuba	1501
150004	Castanhal	1502
150005	Bragança	1502
150006	Capanema	1502
150007	Paragominas	1502
150008	Capitão Poço	1502
150009	Marabá	1503
150010	Parauapebas	1503
150011	Tucuruí	1503
150012	Redenção	1504
150013	Tucumã - São Félix do Xingu	1504
150014	Xinguara	1504
150015	Santarém	1505
150016	Itaituba	1505
150017	Oriximiná	1505
150018	Altamira	1506
150019	Almeirim - Porto de Moz	1506
150020	Breves	1507
150021	Soure-Salvaterra	1507
160001	Macapá	1601
160002	Laranjal do Jari	1601
160003	Oiapoque	1602
160004	Porto Grande	1602
170001	Palmas	1701
170002	Porto Nacional	1701
170003	Paraíso do Tocantins	1701
170004	Miracema do Tocantins	1701
170005	Araguaína	1702
170006	Guaraí	1702
170007	Colinas do Tocantins	1702
170008	Tocantinópolis	1702
170009	Araguatins	1702
170010	Gurupi	1703
170011	Dianópolis	1703
210001	São Luís	2101
210002	Pinheiro	2101
210003	Chapadinha	2101
210004	Itapecuru Mirim	2101
210005	Viana	2101
210006	Barreirinhas	2101
210007	Tutóia - Araioses	2101
210008	Cururupu	2101
210009	Santa Inês	2102
210010	Bacabal	2102
210011	Governador Nunes Freire	2102
210012	Pedreiras	2102
210013	Caxias	2103
210014	Timon	2103
210015	Codó	2103
210016	Presidente Dutra	2104
210017	São João dos Patos	2104
210018	Colinas	2104
210019	Imperatriz	2105
210020	Barra do Corda	2105
210021	Açailândia	2105
210022	Balsas	2105
220001	Teresina	2201
220002	Amarante - Água Branca - Regeneração	2201
220003	Campo Maior	2201
220004	Valença do Piauí	2201
220005	Barras	2201
220006	Parnaíba	2202
220007	Piripiri	2202
220008	Esperantina	2202
220009	Picos	2203
220010	Paulistana	2203
220011	Oeiras	2203
220012	Simplício Mendes	2203
220013	São Raimundo Nonato	2204
220014	São João do Piauí	2204
220015	Corrente	2205
220016	Bom Jesus	2205
220017	Floriano	2206
220018	Uruçuí	2206
220019	Canto do Buriti	2206
230001	Fortaleza	2301
230002	Itapipoca	2301
230003	Redenção-Acarape	2301
230004	Canindé	2301
230005	Itapagé	2301
230006	Quixadá	2302
230007	Russas - Limoeiro do Norte	2302
230008	Aracati	2302
230009	Iguatu	2303
230010	Icó	2303
230011	Juazeiro do Norte	2304
230012	Brejo Santo	2304
230013	Crateús	2305
230014	Tauá	2305
230015	Sobral	2306
230016	São Benedito - Ipu - Guaraciaba do Norte - Tianguá	2306
230017	Acaraú	2306
230018	Camocim	2306
240001	Natal	2401
240002	Santo Antônio - Passa e Fica - Nova Cruz	2401
240003	Canguaretama	2401
240004	Santa Cruz	2401
240005	João Câmara	2401
240006	São Paulo do Potengi	2401
240007	Caicó	2402
240008	Currais Novos	2402
240009	Mossoró	2403
240010	Pau dos Ferros	2403
240011	Açu	2403
250001	João Pessoa	2501
250002	Guarabira	2501
250003	Mamanguape - Rio Tinto	2501
250004	Itabaiana	2501
250005	Campina Grande	2502
250006	Cuité - Nova Floresta	2502
250007	Monteiro	2502
250008	Sumé	2502
250009	Patos	2503
250010	Itaporanga	2503
250011	Catolé do Rocha - São Bento	2503
250012	Pombal	2503
250013	Princesa Isabel	2503
250014	Sousa	2504
250015	Cajazeiras	2504
260001	Recife	2601
260002	Goiana – Timbaúba	2601
260003	Palmares	2601
260004	Limoeiro	2601
260005	Vitória de Santo Antão	2601
260006	Carpina	2601
260007	Barreiros - Sirinhaém	2601
260008	Surubim	2601
260009	Caruaru	2602
260010	Garanhuns	2602
260011	Arcoverde	2602
260012	Belo Jardim – Pesqueira	2602
260013	Serra Talhada	2603
260014	Afogados da Ingazeira	2603
260015	Petrolina	2604
260016	Araripina	2604
260017	Salgueiro	2604
260018	Escada - Ribeirão	2601
270001	Maceió	2701
270002	Porto Calvo - São Luís do Quitunde	2701
270003	Penedo	2701
270004	São Miguel dos Campos	2701
270005	União dos Palmares	2701
270006	Atalaia	2701
270007	Arapiraca	2702
270008	Palmeira dos Índios	2702
270009	Delmiro Gouveia	2702
270010	Santana do Ipanema	2702
270011	Pão de Açúcar - Olho d'Água das Flores – Batalha	2702
280001	Aracaju	2801
280002	Estância	2801
280003	Propriá	2801
280004	Itabaiana	2802
280005	Lagarto	2802
280006	Nossa Senhora da Glória	2802
290001	Salvador	2901
290002	Alagoinhas	2901
290003	Santo Antônio de Jesus	2902
290004	Cruz das Almas	2902
290005	Valença	2902
290006	Nazaré – Maragogipe	2902
290007	Ilhéus – Itabuna	2903
290008	Teixeira de Freitas	2903
290009	Eunápolis - Porto Seguro	2903
290010	Camacan	2903
290011	Vitória da Conquista	2904
290012	Jequié	2904
290013	Brumado	2904
290014	Ipiaú	2904
290015	Itapetinga	2904
290016	Guanambi	2905
290017	Bom Jesus da Lapa	2905
290018	Barreiras	2906
290019	Santa Maria da Vitoria	2906
290020	Irecê	2907
290021	Xique-Xique – Barra	2907
290022	Juazeiro	2908
290023	Senhor do Bonfim	2908
290024	Paulo Afonso	2909
290025	Ribeira do Pombal	2909
290026	Euclides da Cunha	2909
290027	Cícero Dantas	2909
290028	Jeremoabo	2909
290029	Feira de Santana	2910
290030	Jacobina	2910
290031	Itaberaba	2910
290032	Conceição do Coité	2910
290033	Serrinha	2910
290034	Seabra	2910
310001	Belo Horizonte	3101
310002	Sete Lagoas	3101
310003	Santa Bárbara - Ouro Preto	3101
310004	Curvelo	3101
310005	Itabira	3101
310006	Montes Claros	3102
310007	Janaúba	3102
310008	Salinas	3102
310009	Januária	3102
310010	Pirapora	3102
310011	São Francisco	3102
310012	Espinosa	3102
310013	Teófilo Otoni	3103
310014	Capelinha	3103
310015	Almenara	3103
310016	Diamantina	3103
310017	Araçuaí	3103
310018	Pedra Azul	3103
310019	Águas Formosas	3103
310020	Governador Valadares	3104
310021	Guanhães	3104
310022	Mantena	3104
310023	Aimorés - Resplendor	3104
310024	Ipatinga	3105
310025	Caratinga	3105
310026	João Monlevade	3105
310027	Juiz de Fora	3106
310028	Manhuaçu	3106
310029	Ubá	3106
310030	Ponte Nova	3106
310031	Muriaé	3106
310032	Cataguases	3106
310033	Viçosa	3106
310034	Carangola	3106
310035	São João Nepomuceno - Bicas	3106
310036	Além Paraíba	3106
310037	Barbacena	3107
310038	Conselheiro Lafaiete	3107
310039	São João del Rei	3107
310040	Varginha	3108
310041	Passos	3108
310042	Alfenas	3108
310043	Lavras	3108
310044	Guaxupé	3108
310045	Três Corações	3108
310046	Três Pontas - Boa Esperança	3108
310047	São Sebastião do Paraíso	3108
310048	Campo Belo	3108
310049	Piumhi	3108
310050	Pouso Alegre	3109
310051	Poços de Caldas	3109
310052	Itajubá	3109
310053	São Lourenço	3109
310054	Caxambu - Baependi	3109
310055	Uberaba	3110
310056	Araxá	3110
310057	Frutal	3110
310058	Iturama	3110
310059	Uberlândia	3111
310060	Ituiutaba	3111
310061	Monte Carmelo	3111
310062	Patos de Minas	3112
310063	Unaí	3112
310064	Patrocínio	3112
310065	Divinópolis	3113
310066	Formiga	3113
310067	Dores do Indaiá	3113
310068	Pará de Minas	3113
310069	Oliveira	3113
310070	Abaeté	3113
320001	Vitória	3201
320002	Afonso Cláudio - Venda Nova do Imigrante - Santa Maria de Jetibá	3201
320003	São Mateus	3202
320004	Linhares	3202
320005	Colatina	3203
320006	Nova Venécia	3203
320007	Cachoeiro de Itapemirim	3204
320008	Alegre	3204
330001	Rio de Janeiro	3301
330002	Angra dos Reis	3301
330003	Rio Bonito	3301
330004	Volta Redonda - Barra Mansa	3302
330005	Resende	3302
330006	Valença	3302
330007	Petrópolis	3303
330008	Nova Friburgo	3303
330009	Três Rios - Paraíba do Sul	3303
330010	Campos dos Goytacazes	3304
330011	Itaperuna	3304
330012	Santo Antônio de Pádua	3304
330013	Cabo Frio	3305
330014	Macaé - Rio das Ostras	3305
350001	São Paulo	3501
350002	Santos	3501
350003	Sorocaba	3502
350004	Itapeva	3502
350005	Registro	3502
350006	Itapetininga	3502
350007	Avaré	3502
350008	Tatuí	3502
350009	Bauru	3503
350010	Jaú	3503
350011	Botucatu	3503
350012	Lins	3503
350013	Marília	3504
350014	Assis	3504
350015	Ourinhos	3504
350016	Tupã	3504
350017	Piraju	3504
350018	Presidente Prudente	3505
350019	Adamantina - Lucélia	3505
350020	Dracena	3505
350021	Presidente Epitácio-Presidente Venceslau	3505
350022	Araçatuba	3506
350023	Birigui - Penápolis	3506
350024	Andradina	3506
350025	São José do Rio Preto	3507
350026	Catanduva	3507
350027	Votuporanga	3507
350028	Jales	3507
350029	Fernandópolis	3507
350030	Santa Fé do Sul	3507
350031	Ribeirão Preto	3508
350032	Barretos	3508
350033	Franca	3508
350034	São Joaquim da Barra – Orlândia	3508
350035	Ituverava	3508
350036	Araraquara	3509
350037	São Carlos	3509
350038	Campinas	3510
350039	Jundiaí	3510
350040	Piracicaba	3510
350041	Bragança Paulista	3510
350042	Limeira	3510
350043	Mogi Guaçu	3510
350044	São João da Boa Vista	3510
350045	Araras	3510
350046	Rio Claro	3510
350047	São José do Rio Pardo - Mococa	3510
350048	Amparo	3510
350049	São José dos Campos	3511
350050	Taubaté - Pindamonhangaba	3511
350051	Caraguatatuba - Ubatuba - São Sebastião	3511
350052	Guaratinguetá	3511
350053	Cruzeiro	3511
410001	Curitiba	4101
410002	Paranaguá	4101
410003	União da Vitória	4101
410004	Guarapuava	4102
410005	Pitanga	4102
410006	Cascavel	4103
410007	Foz do Iguaçu	4103
410008	Toledo	4103
410009	Francisco Beltrão	4103
410010	Pato Branco	4103
410011	Laranjeiras do Sul - Quedas do Iguaçu	4103
410012	Dois Vizinhos	4103
410013	Marechal Cândido Rondon	4103
410014	Maringá	4104
410015	Campo Mourão	4104
410016	Umuarama	4104
410017	Paranavaí	4104
410018	Cianorte	4104
410019	Paranacity - Colorado	4104
410020	Loanda	4104
410021	Londrina	4105
410022	Santo Antônio da Platina	4105
410023	Apucarana	4105
410024	Cornélio Procópio – Bandeirantes	4105
410025	Ivaiporã	4105
410026	Ibaiti	4105
410027	Ponta Grossa	4106
410028	Telêmaco Borba	4106
410029	Irati	4106
420001	Florianópolis	4201
420002	Criciúma	4202
420003	Tubarão	4202
420004	Araranguá	4202
420005	Lages	4203
420006	Curitibanos	4203
420007	Chapecó	4204
420008	Joaçaba - Herval d'Oeste	4204
420009	São Miguel do Oeste	4204
420010	Concórdia	4204
420011	Xanxerê	4204
420012	Maravilha	4204
420013	São Lourenço do Oeste	4204
420014	Caçador	4205
420015	Videira	4205
420016	Joinville	4206
420017	Mafra	4206
420018	São Bento do Sul - Rio Negrinho	4206
420019	Blumenau	4207
420020	Itajaí	4207
420021	Brusque	4207
420022	Rio do Sul	4207
420023	Ibirama - Presidente Getúlio	4207
420024	Ituporanga	4207
430001	Porto Alegre	4301
430002	Novo Hamburgo - São Leopoldo	4301
430003	Tramandaí - Osório	4301
430004	Taquara - Parobé - Igrejinha	4301
430005	Camaquã	4301
430006	Charqueadas - Triunfo - São Jerônimo	4301
430007	Montenegro	4301
430008	Torres	4301
430009	Pelotas	4302
430010	Bagé	4302
430011	Santa Maria	4303
430012	São Gabriel - Caçapava do Sul	4303
430013	Cachoeira do Sul	4303
430014	Santiago	4303
430015	Uruguaiana	4304
430016	Santana do Livramento	4304
430017	São Borja	4304
430018	Ijuí	4305
430019	Santa Rosa	4305
430020	Santo Ângelo	4305
430021	Três Passos	4305
430022	São Luiz Gonzaga	4305
430023	Três de Maio	4305
430024	Cerro Largo	4305
430025	Passo Fundo	4306
430026	Erechim	4306
430027	Cruz Alta	4306
430028	Carazinho	4306
430029	Frederico Westphalen	4306
430030	Marau	4306
430031	Soledade	4306
430032	Tapejara - Sananduva	4306
430033	Lagoa Vermelha	4306
430034	Palmeira das Missões	4306
430035	Nonoai	4306
430036	Caxias do Sul	4307
430037	Bento Gonçalves	4307
430038	Nova Prata - Guaporé	4307
430039	Vacaria	4307
430040	Santa Cruz do Sul	4308
430041	Lajeado	4308
430042	Sobradinho	4308
430043	Encantado	4308
500001	Campo Grande	5001
500002	Três Lagoas	5001
500003	Paranaíba - Chapadão do Sul - Cassilândia	5001
500004	Coxim	5001
500005	Dourados	5002
500006	Naviraí - Mundo Novo	5002
500007	Nova Andradina	5002
500008	Ponta Porã	5002
500009	Amambai	5002
500010	Corumbá	5003
500011	Jardim	5003
500012	Aquidauana - Anastácio	5003
510001	Cuiabá	5101
510002	Tangará da Serra	5101
510003	Diamantino	5101
510004	Cáceres	5102
510005	Pontes e Lacerda - Comodoro	5102
510006	Mirassol D'oeste	5102
510007	Sinop	5103
510008	Sorriso	5103
510009	Juína	5103
510010	Alta Floresta	5103
510011	Peixoto de Azevedo - Guarantã do Norte	5103
510012	Juara	5103
510013	Barra do Garças	5104
510014	Confresa - Vila Rica	5104
510015	Água Boa	5104
510016	Rondonópolis	5105
510017	Primavera do Leste	5105
510018	Jaciara	5105
520001	Goiânia	5201
520002	Anápolis	5201
520003	Inhumas - Itaberaí - Anicuns	5201
520004	Catalão	5201
520005	Goiás - Itapuranga	5201
520006	Pires do Rio	5201
520007	Itumbiara	5202
520008	Caldas Novas-Morrinhos	5202
520009	Piracanjuba	5202
520010	Rio Verde	5203
520011	Jataí-Mineiros	5203
520012	Quirinópolis	5203
520013	São Luís de Montes Belos	5204
520014	Iporá	5204
520015	Palmeiras de Goiás	5204
520016	Porangatu	5205
520017	Uruaçu - Niquelândia	5205
520018	Ceres - Rialma - Goianésia	5205
520019	Luziânia	5206
520020	Águas Lindas de Goiás	5206
520021	Posse-Campos Belos	5206
520022	Flores de Goiás	5206
530001	Distrito Federal	5301
\.


--
-- Data for Name: municipios_regiaointermediaria; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.municipios_regiaointermediaria (id, nome, uf_id) FROM stdin;
1101	Porto Velho	11
1102	Ji-Paraná	11
1201	Rio Branco	12
1202	Cruzeiro do Sul	12
1301	Manaus	13
1302	Tefé	13
1303	Lábrea	13
1304	Parintins	13
1401	Boa Vista	14
1402	Rorainópolis - Caracaraí	14
1501	Belém	15
1502	Castanhal	15
1503	Marabá	15
1504	Redenção	15
1505	Santarém	15
1506	Altamira	15
1507	Breves	15
1601	Macapá	16
1602	Oiapoque - Porto Grande	16
1701	Palmas	17
1702	Araguaína	17
1703	Gurupi	17
2101	São Luís	21
2102	Santa Inês - Bacabal	21
2103	Caxias	21
2104	Presidente Dutra	21
2105	Imperatriz	21
2201	Teresina	22
2202	Parnaíba	22
2203	Picos	22
2204	São Raimundo Nonato	22
2205	Corrente - Bom Jesus	22
2206	Floriano	22
2301	Fortaleza	23
2302	Quixadá	23
2303	Iguatu	23
2304	Juazeiro do Norte	23
2305	Crateús	23
2306	Sobral	23
2401	Natal	24
2402	Caicó	24
2403	Mossoró	24
2501	João Pessoa	25
2502	Campina Grande	25
2503	Patos	25
2504	Sousa - Cajazeiras	25
2601	Recife	26
2602	Caruaru	26
2603	Serra Talhada	26
2604	Petrolina	26
2701	Maceió	27
2702	Arapiraca	27
2801	Aracaju	28
2802	Itabaiana	28
2901	Salvador	29
2902	Santo Antônio de Jesus	29
2903	Ilhéus – Itabuna	29
2904	Vitória da Conquista	29
2905	Guanambi	29
2906	Barreiras	29
2907	Irecê	29
2908	Juazeiro	29
2909	Paulo Afonso	29
2910	Feira de Santana	29
3101	Belo Horizonte	31
3102	Montes Claros	31
3103	Teófilo Otoni	31
3104	Governador Valadares	31
3105	Ipatinga	31
3106	Juíz de Fora	31
3107	Barbacena	31
3108	Varginha	31
3109	Pouso Alegre	31
3110	Uberaba	31
3111	Uberlândia	31
3112	Patos de Minas	31
3113	Divinópolis	31
3201	Vitória	32
3202	São Mateus	32
3203	Colatina	32
3204	Cachoeiro do Itapemirim	32
3301	Rio de Janeiro	33
3302	Volta Redonda - Barra Mansa	33
3303	Petrópolis	33
3304	Campos dos Goytacazes	33
3305	Macaé - Rio das Ostras - Cabo Frio	33
3501	São Paulo	35
3502	Sorocaba	35
3503	Bauru	35
3504	Marília	35
3505	Presidente Prudente	35
3506	Araçatuba	35
3507	São José do Rio Preto	35
3508	Ribeirão Preto	35
3509	Araraquara	35
3510	Campinas	35
3511	São José dos Campos	35
4101	Curitiba	41
4102	Guarapuava	41
4103	Cascavel	41
4104	Maringá	41
4105	Londrina	41
4106	Ponta Grossa	41
4201	Florianópolis	42
4202	Criciúma	42
4203	Lages	42
4204	Chapecó	42
4205	Caçador	42
4206	Joinville	42
4207	Blumenau	42
4301	Porto Alegre	43
4302	Pelotas	43
4303	Santa Maria	43
4304	Uruguaiana	43
4305	Ijuí	43
4306	Passo Fundo	43
4307	Caxias do Sul	43
4308	Santa Cruz do Sul - Lajeado	43
5001	Campo Grande	50
5002	Dourados	50
5003	Corumbá	50
5101	Cuiabá	51
5102	Cáceres	51
5103	Sinop	51
5104	Barra do Garças	51
5105	Rondonópolis	51
5201	Goiânia	52
5202	Itumbiara	52
5203	Rio Verde	52
5204	São Luís de Montes Belos - Iporá	52
5205	Porangatu - Uruaçu	52
5206	Luziânia - Águas Lindas de Goiás	52
5301	Distrito Federal	53
\.


--
-- Data for Name: municipios_uf; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.municipios_uf (id, nome, sigla, regiao_id) FROM stdin;
11	Rondônia	RO	1
12	Acre	AC	1
13	Amazonas	AM	1
14	Roraima	RR	1
15	Pará	PA	1
16	Amapá	AP	1
17	Tocantins	TO	1
21	Maranhão	MA	2
22	Piauí	PI	2
23	Ceará	CE	2
24	Rio Grande do Norte	RN	2
25	Paraíba	PB	2
26	Pernambuco	PE	2
27	Alagoas	AL	2
28	Sergipe	SE	2
29	Bahia	BA	2
31	Minas Gerais	MG	3
32	Espírito Santo	ES	3
33	Rio de Janeiro	RJ	3
35	São Paulo	SP	3
41	Paraná	PR	4
42	Santa Catarina	SC	4
43	Rio Grande do Sul	RS	4
50	Mato Grosso do Sul	MS	5
51	Mato Grosso	MT	5
52	Goiás	GO	5
53	Distrito Federal	DF	5
\.


--
-- Data for Name: organizacao; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.organizacao (id, nome, criacao, ultima_alteracao, deleted) FROM stdin;
1	EITA	2019-04-05 09:46:22.988147-03	2019-04-05 09:46:22.988176-03	\N
2	ASPTA	2019-04-09 16:03:01.1703-03	2019-04-09 16:03:01.17033-03	\N
\.


--
-- Data for Name: qualitativa_analise; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.qualitativa_analise (id, ano, ano_referencia, criacao, ultima_alteracao, agroecossistema_id, deleted) FROM stdin;
2	2019	2011	2019-04-08 10:16:08.846887-03	2019-04-08 10:16:08.846923-03	1	\N
1	2019	2009	2019-04-08 10:04:19.759196-03	2019-04-08 10:04:19.759305-03	1	2019-04-17 16:39:33.973205-03
\.


--
-- Data for Name: qualitativa_atributo; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.qualitativa_atributo (id, nome, deleted) FROM stdin;
1	Autonomia	\N
2	Responsividade	\N
3	Integração Social	\N
4	Equidade Gênero	\N
5	Protagonismo da Juventude	\N
\.


--
-- Data for Name: qualitativa_avaliacaoparametro; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.qualitativa_avaliacaoparametro (id, mudancas, justificativa, ultima_alteracao, analise_id, parametro_id, criacao, deleted) FROM stdin;
7	What is Lorem Ipsum?\n\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.	What is Lorem Ipsum?\n\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.	2019-04-18 11:31:52.564322-03	2	1	2019-04-18 11:31:49.468074-03	\N
8	lkjfasdçklfjasldkjfaslkçd		2019-04-18 11:39:43.757647-03	2	14	2019-04-18 11:39:43.712721-03	\N
6		çlkjlk	2019-04-12 07:32:18.470116-03	1	6	2019-04-12 07:32:17.643534-03	2019-04-17 16:39:33.973205-03
1	lkçkçlk	ljlkjklj	2019-04-12 07:48:58.775152-03	1	1	2019-04-10 11:57:27.785329-03	2019-04-17 16:39:33.973205-03
2	lkjlkj	lkjlkj	2019-04-12 07:49:02.699905-03	1	2	2019-04-10 11:58:25.701211-03	2019-04-17 16:39:33.973205-03
3	lkjlkj	lkjklj	2019-04-12 07:49:05.727368-03	1	3	2019-04-10 11:58:42.860515-03	2019-04-17 16:39:33.973205-03
4	lkjkljlkj	lkjkljlkj	2019-04-12 07:49:09.716017-03	1	4	2019-04-12 07:32:13.399904-03	2019-04-17 16:39:33.973205-03
5	lkjlkjlkj	çlkjlkj	2019-04-12 07:49:11.362769-03	1	5	2019-04-12 07:32:15.455112-03	2019-04-17 16:39:33.973205-03
\.


--
-- Data for Name: qualitativa_parametro; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.qualitativa_parametro (id, nome, atributo_id, subsection, agroecossistema_id, deleted, ordem) FROM stdin;
1	Terra de Terceiros	1	Recursos Produtivos Mercantis	\N	\N	10
2	Sementes, mudas, mat. propag., crias	1	Recursos Produtivos Mercantis	\N	\N	20
3	Água	1	Recursos Produtivos Mercantis	\N	\N	30
4	Fertilizantes	1	Recursos Produtivos Mercantis	\N	\N	40
5	Forragem/ração	1	Recursos Produtivos Mercantis	\N	\N	50
6	Trabalho de Terceiros	1	Recursos Produtivos Mercantis	\N	\N	60
7	Autoabastecimento Alimentar (quantidade, diversidade e qualidade)	1	Base de Recursos Autocontrolada	\N	\N	1
8	Equipamentos/Infraestrutura	1	Base de Recursos Autocontrolada	\N	\N	2
9	Força de Trabalho	1	Base de Recursos Autocontrolada	\N	\N	3
10	Disponibilidade de Forragem/Ração	1	Base de Recursos Autocontrolada	\N	\N	4
11	Fertilidade do Solo	1	Base de Recursos Autocontrolada	\N	\N	5
12	Disponibilidade de Água	1	Base de Recursos Autocontrolada	\N	\N	6
13	Biodiversidade (inter e intraespecífica)	1	Base de Recursos Autocontrolada	\N	\N	7
14	Disponibilidade de Terra	1	Base de Recursos Autocontrolada	\N	\N	8
15	Biodiversidade (planejada ou associada)	2		\N	\N	10
16	Diversidade de Mercados Acessados	2		\N	\N	20
17	Diversidade de Rendas (agrícolas e não-agrícolas)	2		\N	\N	30
18	Estoques de Insumos	2		\N	\N	40
19	Estoques Vivos	2		\N	\N	50
22	Participação em redes sociotécnicas de aprendizagem	3		\N	\N	30
23	Apropriação da riqueza produzida no agroecossistema pelo NSGA	3		\N	\N	40
24	Participação em espaços de gestão de bens comuns	3		\N	\N	50
25	Divisão sexual do trabalho doméstico e de cuidados (adultos)	4		\N	\N	10
26	Divisão sexual do trabalho doméstico e de cuidados (jovens)	4		\N	\N	20
28	Participação em espaços sócio-organizativos	4		\N	\N	40
29	Apropriação da riqueza gerada no agroecossistema	4		\N	\N	50
30	Acesso a políticas públicas	4		\N	\N	60
31	Participação em espaços de aprendizagem	5		\N	\N	10
27	Participação nas decisões de gestão do agroecossistema	4		\N	\N	30
32	Participação nas decisões de gestão do agroecossistema	5		\N	\N	20
20	Participação em espaços político-organizativos	3		\N	\N	10
33	Participação em espaços político-organizativos	5		\N	\N	30
21	Acesso a políticas públicas	3		\N	\N	20
34	Acesso a políticas públicas	5		\N	\N	40
35	Autonomia econômica	5		\N	\N	50
\.


--
-- Data for Name: qualitativa_qualificacao; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.qualitativa_qualificacao (id, ano, qualificacao, ultima_alteracao, agroecossistema_id, parametro_id, criacao, deleted) FROM stdin;
1	2009	2	2019-04-09 15:17:57.909216-03	1	3	2019-04-09 15:17:57.850601-03	\N
2	2019	2	2019-04-09 15:18:00.053895-03	1	3	2019-04-09 15:18:00.01578-03	\N
3	2009	1	2019-04-09 15:18:01.46962-03	1	4	2019-04-09 15:18:01.439252-03	\N
5	2009	3	2019-04-09 16:48:53.293262-03	1	5	2019-04-09 16:48:53.248955-03	\N
7	2009	2	2019-04-09 16:49:01.90244-03	1	20	2019-04-09 16:49:01.860142-03	\N
8	2019	4	2019-04-09 16:49:03.04006-03	1	20	2019-04-09 16:49:03.001318-03	\N
9	2009	1	2019-04-09 16:49:05.517898-03	1	23	2019-04-09 16:49:05.481963-03	\N
10	2019	3	2019-04-09 16:49:06.59109-03	1	23	2019-04-09 16:49:06.54629-03	\N
11	2009	3	2019-04-09 16:49:15.891149-03	1	26	2019-04-09 16:49:15.853238-03	\N
12	2019	1	2019-04-09 16:49:16.619087-03	1	26	2019-04-09 16:49:16.584696-03	\N
13	2009	1	2019-04-09 16:49:19.20921-03	1	30	2019-04-09 16:49:19.16415-03	\N
14	2019	3	2019-04-09 16:49:20.145483-03	1	30	2019-04-09 16:49:20.073709-03	\N
36	2019	3	2019-04-24 11:33:44.591549-03	1	14	2019-04-10 12:22:14.647959-03	\N
35	2009	3	2019-04-10 12:39:01.151155-03	1	14	2019-04-10 12:22:13.801731-03	\N
51	2011	1	2019-05-11 21:40:59.637803-03	1	20	2019-05-11 21:40:59.582438-03	\N
17	2009	2	2019-04-10 11:59:01.072981-03	1	6	2019-04-10 11:59:01.036098-03	\N
6	2019	1	2019-04-10 12:03:14.321387-03	1	5	2019-04-09 16:48:54.39438-03	\N
19	2009	2	2019-04-10 12:20:29.74208-03	1	7	2019-04-10 12:20:29.696651-03	\N
20	2019	1	2019-04-10 12:20:31.306258-03	1	7	2019-04-10 12:20:31.26089-03	\N
21	2009	1	2019-04-10 12:20:33.518031-03	1	8	2019-04-10 12:20:33.472815-03	\N
22	2019	2	2019-04-10 12:20:34.428102-03	1	8	2019-04-10 12:20:34.381825-03	\N
23	2009	5	2019-04-10 12:20:37.079322-03	1	9	2019-04-10 12:20:37.027022-03	\N
24	2019	1	2019-04-10 12:20:37.801652-03	1	9	2019-04-10 12:20:37.744216-03	\N
25	2009	1	2019-04-10 12:20:40.502295-03	1	10	2019-04-10 12:20:40.305949-03	\N
26	2019	8	2019-04-10 12:20:42.188861-03	1	10	2019-04-10 12:20:42.104208-03	\N
27	2009	5	2019-04-10 12:21:27.57911-03	1	2	2019-04-10 12:21:27.539816-03	\N
28	2019	4	2019-04-10 12:21:28.531342-03	1	2	2019-04-10 12:21:28.488198-03	\N
29	2009	2	2019-04-10 12:22:00.670168-03	1	11	2019-04-10 12:22:00.62837-03	\N
30	2019	5	2019-04-10 12:22:02.047119-03	1	11	2019-04-10 12:22:02.002266-03	\N
31	2009	1	2019-04-10 12:22:04.556849-03	1	12	2019-04-10 12:22:04.506394-03	\N
32	2019	2	2019-04-10 12:22:06.350766-03	1	12	2019-04-10 12:22:06.306774-03	\N
52	2011	2	2019-05-11 21:41:02.322116-03	1	23	2019-05-11 21:41:02.224887-03	\N
4	2019	3	2019-04-10 12:46:28.369058-03	1	4	2019-04-09 15:18:02.814644-03	\N
53	2011	2	2019-05-11 21:41:05.078488-03	1	26	2019-05-11 21:41:05.045454-03	\N
54	2011	3	2019-05-11 21:41:09.471322-03	1	30	2019-05-11 21:41:09.432345-03	\N
18	2019	4	2019-05-13 13:48:59.13762-03	1	6	2019-04-10 11:59:07.288444-03	\N
33	2009	0	2019-04-10 12:25:31.87777-03	1	13	2019-04-10 12:22:08.716916-03	\N
34	2019	1	2019-04-10 12:25:33.019731-03	1	13	2019-04-10 12:22:10.068095-03	\N
16	2009	3	2019-04-12 07:28:24.808619-03	1	1	2019-04-10 11:49:46.501635-03	\N
37	2011	2	2019-04-17 16:40:15.892002-03	1	1	2019-04-17 16:40:15.823221-03	\N
38	2011	3	2019-04-17 16:40:18.403788-03	1	2	2019-04-17 16:40:18.366053-03	\N
39	2011	4	2019-04-17 16:40:19.779952-03	1	3	2019-04-17 16:40:19.745338-03	\N
40	2011	5	2019-04-17 16:40:21.764585-03	1	4	2019-04-17 16:40:21.713433-03	\N
41	2011	5	2019-04-17 16:40:25.103126-03	1	5	2019-04-17 16:40:25.038527-03	\N
42	2011	2	2019-04-17 16:40:27.558651-03	1	6	2019-04-17 16:40:27.499082-03	\N
43	2011	1	2019-04-17 16:40:28.920886-03	1	7	2019-04-17 16:40:28.882132-03	\N
44	2011	2	2019-04-17 16:40:31.307962-03	1	8	2019-04-17 16:40:31.260292-03	\N
45	2011	3	2019-04-17 16:40:32.66825-03	1	9	2019-04-17 16:40:32.629409-03	\N
46	2011	4	2019-04-17 16:40:34.653046-03	1	10	2019-04-17 16:40:34.564283-03	\N
47	2011	5	2019-04-17 16:40:36.087652-03	1	11	2019-04-17 16:40:36.033846-03	\N
48	2011	2	2019-04-17 16:40:39.324861-03	1	12	2019-04-17 16:40:39.282733-03	\N
49	2011	1	2019-04-17 16:40:42.290715-03	1	13	2019-04-17 16:40:42.247624-03	\N
50	2011	1	2019-04-18 11:55:12.369896-03	1	14	2019-04-17 16:40:48.908262-03	\N
15	2019	1	2019-04-24 11:33:23.475408-03	1	1	2019-04-10 11:48:50.770742-03	\N
\.


--
-- Data for Name: sitetree_tree; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.sitetree_tree (id, title, alias) FROM stdin;
1	LUME	maintree
\.


--
-- Data for Name: sitetree_treeitem; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.sitetree_treeitem (id, title, hint, url, urlaspattern, hidden, alias, description, inmenu, inbreadcrumbs, insitetree, access_loggedin, access_guest, access_restricted, access_perm_type, sort_order, parent_id, tree_id) FROM stdin;
1	Agroecossistemas		agroecossistema_list	t	f	\N		t	t	t	f	f	f	1	1	\N	1
2	{{agroecossistema}}		agroecossistema_detail agroecossistema.id	t	f	\N		t	t	t	f	f	f	1	2	1	1
3	Linha do Tempo		linhadotempo:evento_list agroecossistema.id	t	f	\N		t	t	t	f	f	f	1	3	2	1
4	Novo Evento		linhadotempo:evento_create agroecossistema.id	t	f	\N		t	t	t	f	f	f	1	4	3	1
5	Editar evento <strong>{{object.titulo}}</strong>		linhadotempo:evento_update agroecossistema.id object.id	t	f	\N		t	t	t	f	f	f	1	5	3	1
6	Análises Qualitativas		qualitativa:analise_list agroecossistema.id	t	f	\N		t	t	t	f	f	f	1	6	2	1
7	{{analise}}		qualitativa:comparacao analise.id	t	f	\N		t	t	t	f	f	f	1	7	6	1
8	Cadastrar Agroecossistema		agroecossistema_create	t	f	\N		t	t	t	f	f	f	1	8	1	1
10	Cadastrar Comunidade		comunidade_create	t	f	\N		t	t	t	f	f	f	1	10	11	1
11	Comunidades		comunidade_list	t	f	\N		t	t	t	f	f	f	1	11	\N	1
13	Editar		agroecossistema_update agroecossistema.id	t	f	\N		t	t	t	f	f	f	1	13	2	1
14	{{comunidade}}		comunidade_detail comunidade.id	t	f	\N		t	t	t	f	f	f	1	14	11	1
15	Editar		comunidade_update comunidade.id	t	f	\N		t	t	t	f	f	f	1	15	14	1
18	Organizações		organizacao_list	t	f	\N		t	t	t	f	f	f	1	18	\N	1
19	Cadastrar Organização		organizacao_create	t	f	\N		t	t	t	f	f	f	1	19	18	1
20	{{organizacao}}		organizacao_detail organizacao.id	t	f	\N		t	t	t	f	f	f	1	20	18	1
21	Editar		organizacao_update organizacao.id	t	f	\N		t	t	t	f	f	f	1	21	20	1
22	Cadastrar Usuário/a		usuaria_create organizacao.id	t	f	\N		t	t	t	f	f	f	1	22	20	1
23	Territórios		territorio_list	t	f	\N		t	t	t	f	f	f	1	23	\N	1
24	Cadastrar Território		territorio_create	t	f	\N		t	t	t	f	f	f	1	24	23	1
25	{{territorio}}		territorio_detail territorio.id	t	f	\N		t	t	t	f	f	f	1	25	23	1
26	Editar		territorio_update territorio.id	t	f	\N		t	t	t	f	f	f	1	26	25	1
27	Anexos		anexos:anexorecipiente_list related_type related_id	t	f	\N		t	t	t	f	f	f	1	27	\N	1
28	Anexar arquivo em <i>{{related_object}}</i>		anexos:anexorecipiente_create related_type related_id	t	f	\N		t	t	t	f	f	f	1	28	27	1
\.


--
-- Data for Name: sitetree_treeitem_access_permissions; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.sitetree_treeitem_access_permissions (id, treeitem_id, permission_id) FROM stdin;
\.


--
-- Data for Name: socialaccount_socialaccount; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.socialaccount_socialaccount (id, provider, uid, last_login, date_joined, extra_data, user_id) FROM stdin;
\.


--
-- Data for Name: socialaccount_socialapp; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.socialaccount_socialapp (id, provider, name, client_id, secret, key) FROM stdin;
\.


--
-- Data for Name: socialaccount_socialapp_sites; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.socialaccount_socialapp_sites (id, socialapp_id, site_id) FROM stdin;
\.


--
-- Data for Name: socialaccount_socialtoken; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.socialaccount_socialtoken (id, token, token_secret, expires_at, account_id, app_id) FROM stdin;
\.


--
-- Data for Name: spatial_ref_sys; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.spatial_ref_sys (srid, auth_name, auth_srid, srtext, proj4text) FROM stdin;
\.


--
-- Data for Name: usuaria; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.usuaria (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
3	pbkdf2_sha256$120000$deeUQkFCtmew$GUI0CzBztUW1NpZa0101sSx/pHEXb3SQL3bDzJaAtwU=	\N	f	mocodostestes	Moço	dos testes		f	t	2019-04-12 09:58:39.485342-03
2	pbkdf2_sha256$120000$T9TrCmzH2giF$Ecx1eJZcHdGBYUBO+ARbXxjF8mzyG5CrflqesdVipVE=	2019-05-11 20:49:35.652701-03	f	dtygel	Daniel	Tygel	dtygel@eita.org.br	f	t	2019-04-09 14:57:13.517572-03
1	pbkdf2_sha256$120000$KxqXYAtzhgW2$h5kZqwesQNvABB42H6ixPdgBk4dBFDAQzYm7Ddpc+Vc=	2019-05-12 22:24:18.187113-03	t	admin			admin@eita.org.br	t	t	2019-04-04 12:17:12.643904-03
\.


--
-- Data for Name: usuaria_groups; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.usuaria_groups (id, usuaria_id, group_id) FROM stdin;
1	3	1
2	2	1
3	1	1
\.


--
-- Data for Name: usuaria_user_permissions; Type: TABLE DATA; Schema: public; Owner: dtygel
--

COPY public.usuaria_user_permissions (id, usuaria_id, permission_id) FROM stdin;
\.


--
-- Name: account_emailaddress_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.account_emailaddress_id_seq', 1, false);


--
-- Name: account_emailconfirmation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.account_emailconfirmation_id_seq', 1, false);


--
-- Name: agroecossistema_cicloanualreferencia_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.agroecossistema_cicloanualreferencia_id_seq', 16, true);


--
-- Name: agroecossistema_composicaonsga_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.agroecossistema_composicaonsga_id_seq', 11, true);


--
-- Name: agroecossistema_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.agroecossistema_id_seq', 4, true);


--
-- Name: agroecossistema_pessoa_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.agroecossistema_pessoa_id_seq', 14, true);


--
-- Name: agroecossistema_terra_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.agroecossistema_terra_id_seq', 4, true);


--
-- Name: anexos_anexo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.anexos_anexo_id_seq', 5, true);


--
-- Name: anexos_anexorecipiente_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.anexos_anexorecipiente_id_seq', 7, true);


--
-- Name: anexos_tipoanexo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.anexos_tipoanexo_id_seq', 1, false);


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, true);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 3, true);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 260, true);


--
-- Name: cadernodecampo_anotacao_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.cadernodecampo_anotacao_id_seq', 2, true);


--
-- Name: comunidade_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.comunidade_id_seq', 3, true);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 39, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 62, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 172, true);


--
-- Name: django_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.django_site_id_seq', 1, true);


--
-- Name: django_summernote_attachment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.django_summernote_attachment_id_seq', 1, true);


--
-- Name: economica_analise_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.economica_analise_id_seq', 3, true);


--
-- Name: economica_estoqueinsumos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.economica_estoqueinsumos_id_seq', 1, true);


--
-- Name: economica_pagamentoterceiros_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.economica_pagamentoterceiros_id_seq', 2, true);


--
-- Name: economica_patrimonio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.economica_patrimonio_id_seq', 30, true);


--
-- Name: economica_rendanaoagricola_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.economica_rendanaoagricola_id_seq', 7, true);


--
-- Name: economica_subsistema_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.economica_subsistema_id_seq', 12, true);


--
-- Name: economica_subsistemaaquisicao_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.economica_subsistemaaquisicao_id_seq', 26, true);


--
-- Name: economica_subsistemaaquisicaoreciprocidade_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.economica_subsistemaaquisicaoreciprocidade_id_seq', 6, true);


--
-- Name: economica_subsistemapagamentoterceiros_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.economica_subsistemapagamentoterceiros_id_seq', 5, true);


--
-- Name: economica_subsistemaproducao_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.economica_subsistemaproducao_id_seq', 55, true);


--
-- Name: economica_subsistematempotrabalho_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.economica_subsistematempotrabalho_id_seq', 37, true);


--
-- Name: economica_tempotrabalho_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.economica_tempotrabalho_id_seq', 9, true);


--
-- Name: linhadotempo_dimensao_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.linhadotempo_dimensao_id_seq', 11, true);


--
-- Name: linhadotempo_evento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.linhadotempo_evento_id_seq', 3, true);


--
-- Name: linhadotempo_eventogrupo_eventos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.linhadotempo_eventogrupo_eventos_id_seq', 1, false);


--
-- Name: linhadotempo_eventogrupo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.linhadotempo_eventogrupo_id_seq', 1, false);


--
-- Name: linhadotempo_tipodimensao_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.linhadotempo_tipodimensao_id_seq', 2, true);


--
-- Name: main_organizacaousuaria_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.main_organizacaousuaria_id_seq', 3, true);


--
-- Name: main_produto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.main_produto_id_seq', 80, true);


--
-- Name: main_servico_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.main_servico_id_seq', 9, true);


--
-- Name: main_territorio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.main_territorio_id_seq', 1, false);


--
-- Name: main_unidade_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.main_unidade_id_seq', 23, true);


--
-- Name: municipios_diocese_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.municipios_diocese_id_seq', 793, true);


--
-- Name: municipios_municipio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.municipios_municipio_id_seq', 5300108, true);


--
-- Name: municipios_regiao_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.municipios_regiao_id_seq', 5, true);


--
-- Name: municipios_regiaoimediata_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.municipios_regiaoimediata_id_seq', 530001, true);


--
-- Name: municipios_regiaointermediaria_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.municipios_regiaointermediaria_id_seq', 5301, true);


--
-- Name: municipios_uf_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.municipios_uf_id_seq', 53, true);


--
-- Name: organizacao_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.organizacao_id_seq', 2, true);


--
-- Name: qualitativa_analise_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.qualitativa_analise_id_seq', 2, true);


--
-- Name: qualitativa_atributo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.qualitativa_atributo_id_seq', 5, true);


--
-- Name: qualitativa_avaliacaocriterio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.qualitativa_avaliacaocriterio_id_seq', 8, true);


--
-- Name: qualitativa_criterio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.qualitativa_criterio_id_seq', 35, true);


--
-- Name: qualitativa_qualificacao_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.qualitativa_qualificacao_id_seq', 54, true);


--
-- Name: sitetree_tree_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.sitetree_tree_id_seq', 1, true);


--
-- Name: sitetree_treeitem_access_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.sitetree_treeitem_access_permissions_id_seq', 1, false);


--
-- Name: sitetree_treeitem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.sitetree_treeitem_id_seq', 28, true);


--
-- Name: socialaccount_socialaccount_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.socialaccount_socialaccount_id_seq', 1, false);


--
-- Name: socialaccount_socialapp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.socialaccount_socialapp_id_seq', 1, false);


--
-- Name: socialaccount_socialapp_sites_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.socialaccount_socialapp_sites_id_seq', 1, false);


--
-- Name: socialaccount_socialtoken_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.socialaccount_socialtoken_id_seq', 1, false);


--
-- Name: usuaria_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.usuaria_groups_id_seq', 3, true);


--
-- Name: usuaria_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.usuaria_id_seq', 3, true);


--
-- Name: usuaria_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dtygel
--

SELECT pg_catalog.setval('public.usuaria_user_permissions_id_seq', 1, false);


--
-- Name: account_emailaddress account_emailaddress_email_key; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.account_emailaddress
    ADD CONSTRAINT account_emailaddress_email_key UNIQUE (email);


--
-- Name: account_emailaddress account_emailaddress_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.account_emailaddress
    ADD CONSTRAINT account_emailaddress_pkey PRIMARY KEY (id);


--
-- Name: account_emailconfirmation account_emailconfirmation_key_key; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.account_emailconfirmation
    ADD CONSTRAINT account_emailconfirmation_key_key UNIQUE (key);


--
-- Name: account_emailconfirmation account_emailconfirmation_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.account_emailconfirmation
    ADD CONSTRAINT account_emailconfirmation_pkey PRIMARY KEY (id);


--
-- Name: agroecossistema_cicloanualreferencia agroecossistema_cicloanu_agroecossistema_id_ano_70693e54_uniq; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.agroecossistema_cicloanualreferencia
    ADD CONSTRAINT agroecossistema_cicloanu_agroecossistema_id_ano_70693e54_uniq UNIQUE (agroecossistema_id, ano);


--
-- Name: agroecossistema_cicloanualreferencia agroecossistema_cicloanualreferencia_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.agroecossistema_cicloanualreferencia
    ADD CONSTRAINT agroecossistema_cicloanualreferencia_pkey PRIMARY KEY (id);


--
-- Name: agroecossistema_composicaonsga agroecossistema_composic_ciclo_anual_referencia_i_9347a618_uniq; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.agroecossistema_composicaonsga
    ADD CONSTRAINT agroecossistema_composic_ciclo_anual_referencia_i_9347a618_uniq UNIQUE (ciclo_anual_referencia_id, pessoa_id);


--
-- Name: agroecossistema_composicaonsga agroecossistema_composicaonsga_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.agroecossistema_composicaonsga
    ADD CONSTRAINT agroecossistema_composicaonsga_pkey PRIMARY KEY (id);


--
-- Name: agroecossistema_pessoa agroecossistema_pessoa_nome_agroecossistema_id_69b5e2db_uniq; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.agroecossistema_pessoa
    ADD CONSTRAINT agroecossistema_pessoa_nome_agroecossistema_id_69b5e2db_uniq UNIQUE (nome, agroecossistema_id);


--
-- Name: agroecossistema_pessoa agroecossistema_pessoa_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.agroecossistema_pessoa
    ADD CONSTRAINT agroecossistema_pessoa_pkey PRIMARY KEY (id);


--
-- Name: agroecossistema agroecossistema_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.agroecossistema
    ADD CONSTRAINT agroecossistema_pkey PRIMARY KEY (id);


--
-- Name: agroecossistema_terra agroecossistema_terra_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.agroecossistema_terra
    ADD CONSTRAINT agroecossistema_terra_pkey PRIMARY KEY (id);


--
-- Name: anexos_anexo anexos_anexo_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.anexos_anexo
    ADD CONSTRAINT anexos_anexo_pkey PRIMARY KEY (id);


--
-- Name: anexos_anexorecipiente anexos_anexorecipiente_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.anexos_anexorecipiente
    ADD CONSTRAINT anexos_anexorecipiente_pkey PRIMARY KEY (id);


--
-- Name: anexos_tipoanexo anexos_tipoanexo_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.anexos_tipoanexo
    ADD CONSTRAINT anexos_tipoanexo_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: cadernodecampo_anotacao cadernodecampo_anotacao_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.cadernodecampo_anotacao
    ADD CONSTRAINT cadernodecampo_anotacao_pkey PRIMARY KEY (id);


--
-- Name: comunidade comunidade_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.comunidade
    ADD CONSTRAINT comunidade_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: django_site django_site_domain_a2e37b91_uniq; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.django_site
    ADD CONSTRAINT django_site_domain_a2e37b91_uniq UNIQUE (domain);


--
-- Name: django_site django_site_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.django_site
    ADD CONSTRAINT django_site_pkey PRIMARY KEY (id);


--
-- Name: django_summernote_attachment django_summernote_attachment_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.django_summernote_attachment
    ADD CONSTRAINT django_summernote_attachment_pkey PRIMARY KEY (id);


--
-- Name: economica_analise economica_analise_ciclo_anual_referencia_id_acf92bda_uniq; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_analise
    ADD CONSTRAINT economica_analise_ciclo_anual_referencia_id_acf92bda_uniq UNIQUE (ciclo_anual_referencia_id);


--
-- Name: economica_analise economica_analise_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_analise
    ADD CONSTRAINT economica_analise_pkey PRIMARY KEY (id);


--
-- Name: economica_estoqueinsumos economica_estoqueinsumos_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_estoqueinsumos
    ADD CONSTRAINT economica_estoqueinsumos_pkey PRIMARY KEY (id);


--
-- Name: economica_pagamentoterceiros economica_pagamentoterceiros_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_pagamentoterceiros
    ADD CONSTRAINT economica_pagamentoterceiros_pkey PRIMARY KEY (id);


--
-- Name: economica_patrimonio economica_patrimonio_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_patrimonio
    ADD CONSTRAINT economica_patrimonio_pkey PRIMARY KEY (id);


--
-- Name: economica_rendanaoagricola economica_rendanaoagricola_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_rendanaoagricola
    ADD CONSTRAINT economica_rendanaoagricola_pkey PRIMARY KEY (id);


--
-- Name: economica_subsistema economica_subsistema_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_subsistema
    ADD CONSTRAINT economica_subsistema_pkey PRIMARY KEY (id);


--
-- Name: economica_subsistemainsumo economica_subsistemaaquisicao_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_subsistemainsumo
    ADD CONSTRAINT economica_subsistemaaquisicao_pkey PRIMARY KEY (id);


--
-- Name: economica_subsistemaaquisicaoreciprocidade economica_subsistemaaquisicaoreciprocidade_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_subsistemaaquisicaoreciprocidade
    ADD CONSTRAINT economica_subsistemaaquisicaoreciprocidade_pkey PRIMARY KEY (id);


--
-- Name: economica_subsistemapagamentoterceiros economica_subsistemapagamentoterceiros_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_subsistemapagamentoterceiros
    ADD CONSTRAINT economica_subsistemapagamentoterceiros_pkey PRIMARY KEY (id);


--
-- Name: economica_subsistemaproducao economica_subsistemaproducao_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_subsistemaproducao
    ADD CONSTRAINT economica_subsistemaproducao_pkey PRIMARY KEY (id);


--
-- Name: economica_subsistematempotrabalho economica_subsistematempotrabalho_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_subsistematempotrabalho
    ADD CONSTRAINT economica_subsistematempotrabalho_pkey PRIMARY KEY (id);


--
-- Name: economica_tempotrabalho economica_tempotrabalho_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_tempotrabalho
    ADD CONSTRAINT economica_tempotrabalho_pkey PRIMARY KEY (id);


--
-- Name: linhadotempo_dimensao linhadotempo_dimensao_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.linhadotempo_dimensao
    ADD CONSTRAINT linhadotempo_dimensao_pkey PRIMARY KEY (id);


--
-- Name: linhadotempo_evento linhadotempo_evento_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.linhadotempo_evento
    ADD CONSTRAINT linhadotempo_evento_pkey PRIMARY KEY (id);


--
-- Name: linhadotempo_eventogrupo_eventos linhadotempo_eventogrupo_eventogrupo_id_evento_id_ebff5fc3_uniq; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.linhadotempo_eventogrupo_eventos
    ADD CONSTRAINT linhadotempo_eventogrupo_eventogrupo_id_evento_id_ebff5fc3_uniq UNIQUE (eventogrupo_id, evento_id);


--
-- Name: linhadotempo_eventogrupo_eventos linhadotempo_eventogrupo_eventos_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.linhadotempo_eventogrupo_eventos
    ADD CONSTRAINT linhadotempo_eventogrupo_eventos_pkey PRIMARY KEY (id);


--
-- Name: linhadotempo_eventogrupo linhadotempo_eventogrupo_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.linhadotempo_eventogrupo
    ADD CONSTRAINT linhadotempo_eventogrupo_pkey PRIMARY KEY (id);


--
-- Name: linhadotempo_tipodimensao linhadotempo_tipodimensao_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.linhadotempo_tipodimensao
    ADD CONSTRAINT linhadotempo_tipodimensao_pkey PRIMARY KEY (id);


--
-- Name: main_organizacaousuaria main_organizacaousuaria_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.main_organizacaousuaria
    ADD CONSTRAINT main_organizacaousuaria_pkey PRIMARY KEY (id);


--
-- Name: main_produto main_produto_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.main_produto
    ADD CONSTRAINT main_produto_pkey PRIMARY KEY (id);


--
-- Name: main_servico main_servico_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.main_servico
    ADD CONSTRAINT main_servico_pkey PRIMARY KEY (id);


--
-- Name: main_territorio main_territorio_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.main_territorio
    ADD CONSTRAINT main_territorio_pkey PRIMARY KEY (id);


--
-- Name: main_unidade main_unidade_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.main_unidade
    ADD CONSTRAINT main_unidade_pkey PRIMARY KEY (id);


--
-- Name: municipios_diocese municipios_diocese_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.municipios_diocese
    ADD CONSTRAINT municipios_diocese_pkey PRIMARY KEY (id);


--
-- Name: municipios_municipio municipios_municipio_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.municipios_municipio
    ADD CONSTRAINT municipios_municipio_pkey PRIMARY KEY (id);


--
-- Name: municipios_regiao municipios_regiao_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.municipios_regiao
    ADD CONSTRAINT municipios_regiao_pkey PRIMARY KEY (id);


--
-- Name: municipios_regiaoimediata municipios_regiaoimediata_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.municipios_regiaoimediata
    ADD CONSTRAINT municipios_regiaoimediata_pkey PRIMARY KEY (id);


--
-- Name: municipios_regiaointermediaria municipios_regiaointermediaria_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.municipios_regiaointermediaria
    ADD CONSTRAINT municipios_regiaointermediaria_pkey PRIMARY KEY (id);


--
-- Name: municipios_uf municipios_uf_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.municipios_uf
    ADD CONSTRAINT municipios_uf_pkey PRIMARY KEY (id);


--
-- Name: organizacao organizacao_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.organizacao
    ADD CONSTRAINT organizacao_pkey PRIMARY KEY (id);


--
-- Name: qualitativa_analise qualitativa_analise_agroecossistema_id_ano_a_2366a2da_uniq; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.qualitativa_analise
    ADD CONSTRAINT qualitativa_analise_agroecossistema_id_ano_a_2366a2da_uniq UNIQUE (agroecossistema_id, ano, ano_referencia);


--
-- Name: qualitativa_analise qualitativa_analise_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.qualitativa_analise
    ADD CONSTRAINT qualitativa_analise_pkey PRIMARY KEY (id);


--
-- Name: qualitativa_atributo qualitativa_atributo_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.qualitativa_atributo
    ADD CONSTRAINT qualitativa_atributo_pkey PRIMARY KEY (id);


--
-- Name: qualitativa_avaliacaoparametro qualitativa_avaliacaocri_analise_id_criterio_id_c76d6dd7_uniq; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.qualitativa_avaliacaoparametro
    ADD CONSTRAINT qualitativa_avaliacaocri_analise_id_criterio_id_c76d6dd7_uniq UNIQUE (analise_id, parametro_id);


--
-- Name: qualitativa_avaliacaoparametro qualitativa_avaliacaocriterio_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.qualitativa_avaliacaoparametro
    ADD CONSTRAINT qualitativa_avaliacaocriterio_pkey PRIMARY KEY (id);


--
-- Name: qualitativa_parametro qualitativa_criterio_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.qualitativa_parametro
    ADD CONSTRAINT qualitativa_criterio_pkey PRIMARY KEY (id);


--
-- Name: qualitativa_qualificacao qualitativa_qualificacao_agroecossistema_id_crite_fbadab23_uniq; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.qualitativa_qualificacao
    ADD CONSTRAINT qualitativa_qualificacao_agroecossistema_id_crite_fbadab23_uniq UNIQUE (agroecossistema_id, parametro_id, ano);


--
-- Name: qualitativa_qualificacao qualitativa_qualificacao_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.qualitativa_qualificacao
    ADD CONSTRAINT qualitativa_qualificacao_pkey PRIMARY KEY (id);


--
-- Name: sitetree_tree sitetree_tree_alias_key; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.sitetree_tree
    ADD CONSTRAINT sitetree_tree_alias_key UNIQUE (alias);


--
-- Name: sitetree_tree sitetree_tree_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.sitetree_tree
    ADD CONSTRAINT sitetree_tree_pkey PRIMARY KEY (id);


--
-- Name: sitetree_treeitem_access_permissions sitetree_treeitem_access_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.sitetree_treeitem_access_permissions
    ADD CONSTRAINT sitetree_treeitem_access_permissions_pkey PRIMARY KEY (id);


--
-- Name: sitetree_treeitem_access_permissions sitetree_treeitem_access_treeitem_id_permission_i_a3224a96_uniq; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.sitetree_treeitem_access_permissions
    ADD CONSTRAINT sitetree_treeitem_access_treeitem_id_permission_i_a3224a96_uniq UNIQUE (treeitem_id, permission_id);


--
-- Name: sitetree_treeitem sitetree_treeitem_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.sitetree_treeitem
    ADD CONSTRAINT sitetree_treeitem_pkey PRIMARY KEY (id);


--
-- Name: sitetree_treeitem sitetree_treeitem_tree_id_alias_f597fbd9_uniq; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.sitetree_treeitem
    ADD CONSTRAINT sitetree_treeitem_tree_id_alias_f597fbd9_uniq UNIQUE (tree_id, alias);


--
-- Name: socialaccount_socialaccount socialaccount_socialaccount_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.socialaccount_socialaccount
    ADD CONSTRAINT socialaccount_socialaccount_pkey PRIMARY KEY (id);


--
-- Name: socialaccount_socialaccount socialaccount_socialaccount_provider_uid_fc810c6e_uniq; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.socialaccount_socialaccount
    ADD CONSTRAINT socialaccount_socialaccount_provider_uid_fc810c6e_uniq UNIQUE (provider, uid);


--
-- Name: socialaccount_socialapp_sites socialaccount_socialapp__socialapp_id_site_id_71a9a768_uniq; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.socialaccount_socialapp_sites
    ADD CONSTRAINT socialaccount_socialapp__socialapp_id_site_id_71a9a768_uniq UNIQUE (socialapp_id, site_id);


--
-- Name: socialaccount_socialapp socialaccount_socialapp_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.socialaccount_socialapp
    ADD CONSTRAINT socialaccount_socialapp_pkey PRIMARY KEY (id);


--
-- Name: socialaccount_socialapp_sites socialaccount_socialapp_sites_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.socialaccount_socialapp_sites
    ADD CONSTRAINT socialaccount_socialapp_sites_pkey PRIMARY KEY (id);


--
-- Name: socialaccount_socialtoken socialaccount_socialtoken_app_id_account_id_fca4e0ac_uniq; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.socialaccount_socialtoken
    ADD CONSTRAINT socialaccount_socialtoken_app_id_account_id_fca4e0ac_uniq UNIQUE (app_id, account_id);


--
-- Name: socialaccount_socialtoken socialaccount_socialtoken_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.socialaccount_socialtoken
    ADD CONSTRAINT socialaccount_socialtoken_pkey PRIMARY KEY (id);


--
-- Name: usuaria_groups usuaria_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.usuaria_groups
    ADD CONSTRAINT usuaria_groups_pkey PRIMARY KEY (id);


--
-- Name: usuaria_groups usuaria_groups_usuaria_id_group_id_dd466dfc_uniq; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.usuaria_groups
    ADD CONSTRAINT usuaria_groups_usuaria_id_group_id_dd466dfc_uniq UNIQUE (usuaria_id, group_id);


--
-- Name: usuaria usuaria_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.usuaria
    ADD CONSTRAINT usuaria_pkey PRIMARY KEY (id);


--
-- Name: usuaria_user_permissions usuaria_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.usuaria_user_permissions
    ADD CONSTRAINT usuaria_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: usuaria_user_permissions usuaria_user_permissions_usuaria_id_permission_id_1b162b3d_uniq; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.usuaria_user_permissions
    ADD CONSTRAINT usuaria_user_permissions_usuaria_id_permission_id_1b162b3d_uniq UNIQUE (usuaria_id, permission_id);


--
-- Name: usuaria usuaria_username_key; Type: CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.usuaria
    ADD CONSTRAINT usuaria_username_key UNIQUE (username);


--
-- Name: account_emailaddress_email_03be32b2_like; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX account_emailaddress_email_03be32b2_like ON public.account_emailaddress USING btree (email varchar_pattern_ops);


--
-- Name: account_emailaddress_user_id_2c513194; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX account_emailaddress_user_id_2c513194 ON public.account_emailaddress USING btree (user_id);


--
-- Name: account_emailconfirmation_email_address_id_5b7f8c58; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX account_emailconfirmation_email_address_id_5b7f8c58 ON public.account_emailconfirmation USING btree (email_address_id);


--
-- Name: account_emailconfirmation_key_f43612bd_like; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX account_emailconfirmation_key_f43612bd_like ON public.account_emailconfirmation USING btree (key varchar_pattern_ops);


--
-- Name: agroecossistema_autor_id_49d476a5; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX agroecossistema_autor_id_49d476a5 ON public.agroecossistema USING btree (autor_id);


--
-- Name: agroecossistema_cicloanual_agroecossistema_id_f9933a3e; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX agroecossistema_cicloanual_agroecossistema_id_f9933a3e ON public.agroecossistema_cicloanualreferencia USING btree (agroecossistema_id);


--
-- Name: agroecossistema_composicao_ciclo_anual_referencia_id_fce774b1; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX agroecossistema_composicao_ciclo_anual_referencia_id_fce774b1 ON public.agroecossistema_composicaonsga USING btree (ciclo_anual_referencia_id);


--
-- Name: agroecossistema_composicaonsga_pessoa_id_9a2bf840; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX agroecossistema_composicaonsga_pessoa_id_9a2bf840 ON public.agroecossistema_composicaonsga USING btree (pessoa_id);


--
-- Name: agroecossistema_comunidade_id_5a260e51; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX agroecossistema_comunidade_id_5a260e51 ON public.agroecossistema USING btree (comunidade_id);


--
-- Name: agroecossistema_geoponto_id; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX agroecossistema_geoponto_id ON public.agroecossistema USING gist (geoponto);


--
-- Name: agroecossistema_organizacao_id_b868ea36; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX agroecossistema_organizacao_id_b868ea36 ON public.agroecossistema USING btree (organizacao_id);


--
-- Name: agroecossistema_pessoa_agroecossistema_id_f7e10a4c; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX agroecossistema_pessoa_agroecossistema_id_f7e10a4c ON public.agroecossistema_pessoa USING btree (agroecossistema_id);


--
-- Name: agroecossistema_poligono_id; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX agroecossistema_poligono_id ON public.agroecossistema USING gist (poligono);


--
-- Name: agroecossistema_terra_ciclo_anual_referencia_id_c231f679; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX agroecossistema_terra_ciclo_anual_referencia_id_c231f679 ON public.agroecossistema_terra USING btree (ciclo_anual_referencia_id);


--
-- Name: agroecossistema_terra_poligono_id; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX agroecossistema_terra_poligono_id ON public.agroecossistema_terra USING gist (poligono);


--
-- Name: agroecossistema_territorio_id_ff933ec2; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX agroecossistema_territorio_id_ff933ec2 ON public.agroecossistema USING btree (territorio_id);


--
-- Name: anexos_anexo_tipo_id_b76b0eca; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX anexos_anexo_tipo_id_b76b0eca ON public.anexos_anexo USING btree (tipo_id);


--
-- Name: anexos_anexorecipiente_anexo_id_ca915db2; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX anexos_anexorecipiente_anexo_id_ca915db2 ON public.anexos_anexorecipiente USING btree (anexo_id);


--
-- Name: anexos_anexorecipiente_recipiente_content_type_id_63a46e84; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX anexos_anexorecipiente_recipiente_content_type_id_63a46e84 ON public.anexos_anexorecipiente USING btree (recipiente_content_type_id);


--
-- Name: anexos_tipoanexo_mae_id_5a628c98; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX anexos_tipoanexo_mae_id_5a628c98 ON public.anexos_tipoanexo USING btree (mae_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: cadernodecampo_anotacao_agroecossistema_id_8c12681e; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX cadernodecampo_anotacao_agroecossistema_id_8c12681e ON public.cadernodecampo_anotacao USING btree (agroecossistema_id);


--
-- Name: cadernodecampo_anotacao_autor_id_ad0a9b9a; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX cadernodecampo_anotacao_autor_id_ad0a9b9a ON public.cadernodecampo_anotacao USING btree (autor_id);


--
-- Name: comunidade_geoponto_id; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX comunidade_geoponto_id ON public.comunidade USING gist (geoponto);


--
-- Name: comunidade_municipio_id_46341eb3; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX comunidade_municipio_id_46341eb3 ON public.comunidade USING btree (municipio_id);


--
-- Name: comunidade_poligono_id; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX comunidade_poligono_id ON public.comunidade USING gist (poligono);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: django_site_domain_a2e37b91_like; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX django_site_domain_a2e37b91_like ON public.django_site USING btree (domain varchar_pattern_ops);


--
-- Name: economica_analise_ciclo_anual_referencia_id_acf92bda; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX economica_analise_ciclo_anual_referencia_id_acf92bda ON public.economica_analise USING btree (ciclo_anual_referencia_id);


--
-- Name: economica_estoqueinsumos_analise_id_3e420d14; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX economica_estoqueinsumos_analise_id_3e420d14 ON public.economica_estoqueinsumos USING btree (analise_id);


--
-- Name: economica_estoqueinsumos_produto_id_6d0649aa; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX economica_estoqueinsumos_produto_id_6d0649aa ON public.economica_estoqueinsumos USING btree (produto_id);


--
-- Name: economica_estoqueinsumos_unidade_id_40c83a4d; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX economica_estoqueinsumos_unidade_id_40c83a4d ON public.economica_estoqueinsumos USING btree (unidade_id);


--
-- Name: economica_pagamentoterceiros_analise_id_299816b8; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX economica_pagamentoterceiros_analise_id_299816b8 ON public.economica_pagamentoterceiros USING btree (analise_id);


--
-- Name: economica_pagamentoterceiros_servico_id_310af121; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX economica_pagamentoterceiros_servico_id_310af121 ON public.economica_pagamentoterceiros USING btree (servico_id);


--
-- Name: economica_pagamentoterceiros_unidade_id_ace7a7db; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX economica_pagamentoterceiros_unidade_id_ace7a7db ON public.economica_pagamentoterceiros USING btree (unidade_id);


--
-- Name: economica_patrimonio_analise_id_5046837b; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX economica_patrimonio_analise_id_5046837b ON public.economica_patrimonio USING btree (analise_id);


--
-- Name: economica_patrimonio_unidade_id_d46316a8; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX economica_patrimonio_unidade_id_d46316a8 ON public.economica_patrimonio USING btree (unidade_id);


--
-- Name: economica_rendanaoagricola_analise_id_d254cfbf; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX economica_rendanaoagricola_analise_id_d254cfbf ON public.economica_rendanaoagricola USING btree (analise_id);


--
-- Name: economica_rendanaoagricola_unidade_id_162294dd; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX economica_rendanaoagricola_unidade_id_162294dd ON public.economica_rendanaoagricola USING btree (unidade_id);


--
-- Name: economica_subsistema_analise_id_3e1b5199; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX economica_subsistema_analise_id_3e1b5199 ON public.economica_subsistema USING btree (analise_id);


--
-- Name: economica_subsistemaaquisi_subsistema_id_29f62c8d; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX economica_subsistemaaquisi_subsistema_id_29f62c8d ON public.economica_subsistemaaquisicaoreciprocidade USING btree (subsistema_id);


--
-- Name: economica_subsistemaaquisicao_produto_id_b5e6e5a2; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX economica_subsistemaaquisicao_produto_id_b5e6e5a2 ON public.economica_subsistemainsumo USING btree (produto_id);


--
-- Name: economica_subsistemaaquisicao_subsistema_id_ea1a3063; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX economica_subsistemaaquisicao_subsistema_id_ea1a3063 ON public.economica_subsistemainsumo USING btree (subsistema_id);


--
-- Name: economica_subsistemaaquisicao_unidade_id_18d27e63; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX economica_subsistemaaquisicao_unidade_id_18d27e63 ON public.economica_subsistemainsumo USING btree (unidade_id);


--
-- Name: economica_subsistemaaquisicaoreciprocidade_produto_id_d2f78a71; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX economica_subsistemaaquisicaoreciprocidade_produto_id_d2f78a71 ON public.economica_subsistemaaquisicaoreciprocidade USING btree (produto_id);


--
-- Name: economica_subsistemaaquisicaoreciprocidade_servico_id_8d25466e; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX economica_subsistemaaquisicaoreciprocidade_servico_id_8d25466e ON public.economica_subsistemaaquisicaoreciprocidade USING btree (servico_id);


--
-- Name: economica_subsistemaaquisicaoreciprocidade_unidade_id_3057b659; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX economica_subsistemaaquisicaoreciprocidade_unidade_id_3057b659 ON public.economica_subsistemaaquisicaoreciprocidade USING btree (unidade_id);


--
-- Name: economica_subsistemapagamentoterceiros_servico_id_6960909e; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX economica_subsistemapagamentoterceiros_servico_id_6960909e ON public.economica_subsistemapagamentoterceiros USING btree (servico_id);


--
-- Name: economica_subsistemapagamentoterceiros_subsistema_id_6ada1432; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX economica_subsistemapagamentoterceiros_subsistema_id_6ada1432 ON public.economica_subsistemapagamentoterceiros USING btree (subsistema_id);


--
-- Name: economica_subsistemapagamentoterceiros_unidade_id_8f528dd0; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX economica_subsistemapagamentoterceiros_unidade_id_8f528dd0 ON public.economica_subsistemapagamentoterceiros USING btree (unidade_id);


--
-- Name: economica_subsistemaproducao_produto_id_475e8983; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX economica_subsistemaproducao_produto_id_475e8983 ON public.economica_subsistemaproducao USING btree (produto_id);


--
-- Name: economica_subsistemaproducao_subsistema_id_fb22da39; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX economica_subsistemaproducao_subsistema_id_fb22da39 ON public.economica_subsistemaproducao USING btree (subsistema_id);


--
-- Name: economica_subsistemaproducao_unidade_id_de8407ab; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX economica_subsistemaproducao_unidade_id_de8407ab ON public.economica_subsistemaproducao USING btree (unidade_id);


--
-- Name: economica_subsistematempotrabalho_composicao_nsga_id_be7d9465; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX economica_subsistematempotrabalho_composicao_nsga_id_be7d9465 ON public.economica_subsistematempotrabalho USING btree (composicao_nsga_id);


--
-- Name: economica_subsistematempotrabalho_subsistema_id_5c6c907e; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX economica_subsistematempotrabalho_subsistema_id_5c6c907e ON public.economica_subsistematempotrabalho USING btree (subsistema_id);


--
-- Name: economica_tempotrabalho_analise_id_3d3a659f; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX economica_tempotrabalho_analise_id_3d3a659f ON public.economica_tempotrabalho USING btree (analise_id);


--
-- Name: economica_tempotrabalho_composicao_nsga_id_71670b3f; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX economica_tempotrabalho_composicao_nsga_id_71670b3f ON public.economica_tempotrabalho USING btree (composicao_nsga_id);


--
-- Name: linhadotempo_dimensao_tipo_dimensao_id_2f7c0a92; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX linhadotempo_dimensao_tipo_dimensao_id_2f7c0a92 ON public.linhadotempo_dimensao USING btree (tipo_dimensao_id);


--
-- Name: linhadotempo_evento_agroecossistema_id_3dd6efe8; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX linhadotempo_evento_agroecossistema_id_3dd6efe8 ON public.linhadotempo_evento USING btree (agroecossistema_id);


--
-- Name: linhadotempo_evento_criado_por_id_b8923b5b; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX linhadotempo_evento_criado_por_id_b8923b5b ON public.linhadotempo_evento USING btree (criado_por_id);


--
-- Name: linhadotempo_evento_dimensao_id_902e8d17; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX linhadotempo_evento_dimensao_id_902e8d17 ON public.linhadotempo_evento USING btree (dimensao_id);


--
-- Name: linhadotempo_eventogrupo_agroecossistema_id_ec94d243; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX linhadotempo_eventogrupo_agroecossistema_id_ec94d243 ON public.linhadotempo_eventogrupo USING btree (agroecossistema_id);


--
-- Name: linhadotempo_eventogrupo_eventos_evento_id_c95bad9a; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX linhadotempo_eventogrupo_eventos_evento_id_c95bad9a ON public.linhadotempo_eventogrupo_eventos USING btree (evento_id);


--
-- Name: linhadotempo_eventogrupo_eventos_eventogrupo_id_ea2f8c2d; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX linhadotempo_eventogrupo_eventos_eventogrupo_id_ea2f8c2d ON public.linhadotempo_eventogrupo_eventos USING btree (eventogrupo_id);


--
-- Name: main_organizacaousuaria_organizacao_id_20671b76; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX main_organizacaousuaria_organizacao_id_20671b76 ON public.main_organizacaousuaria USING btree (organizacao_id);


--
-- Name: main_organizacaousuaria_usuaria_id_f618d019; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX main_organizacaousuaria_usuaria_id_f618d019 ON public.main_organizacaousuaria USING btree (usuaria_id);


--
-- Name: municipios_municipio_diocese_id_bd117b70; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX municipios_municipio_diocese_id_bd117b70 ON public.municipios_municipio USING btree (diocese_id);


--
-- Name: municipios_municipio_regiao_imediata_id_cbad6130; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX municipios_municipio_regiao_imediata_id_cbad6130 ON public.municipios_municipio USING btree (regiao_imediata_id);


--
-- Name: municipios_municipio_uf_id_0ca32a2d; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX municipios_municipio_uf_id_0ca32a2d ON public.municipios_municipio USING btree (uf_id);


--
-- Name: municipios_regiaoimediata_regiao_intermediaria_id_c372499c; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX municipios_regiaoimediata_regiao_intermediaria_id_c372499c ON public.municipios_regiaoimediata USING btree (regiao_intermediaria_id);


--
-- Name: municipios_regiaointermediaria_uf_id_e4427eba; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX municipios_regiaointermediaria_uf_id_e4427eba ON public.municipios_regiaointermediaria USING btree (uf_id);


--
-- Name: municipios_uf_regiao_id_77e9c7d4; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX municipios_uf_regiao_id_77e9c7d4 ON public.municipios_uf USING btree (regiao_id);


--
-- Name: qualitativa_analise_agroecossistema_id_77a1fa3d; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX qualitativa_analise_agroecossistema_id_77a1fa3d ON public.qualitativa_analise USING btree (agroecossistema_id);


--
-- Name: qualitativa_avaliacaocriterio_analise_id_824b864e; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX qualitativa_avaliacaocriterio_analise_id_824b864e ON public.qualitativa_avaliacaoparametro USING btree (analise_id);


--
-- Name: qualitativa_avaliacaocriterio_criterio_id_8470bb62; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX qualitativa_avaliacaocriterio_criterio_id_8470bb62 ON public.qualitativa_avaliacaoparametro USING btree (parametro_id);


--
-- Name: qualitativa_criterio_agroecossistema_id_3e24c437; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX qualitativa_criterio_agroecossistema_id_3e24c437 ON public.qualitativa_parametro USING btree (agroecossistema_id);


--
-- Name: qualitativa_criterio_atributo_id_ebd05d45; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX qualitativa_criterio_atributo_id_ebd05d45 ON public.qualitativa_parametro USING btree (atributo_id);


--
-- Name: qualitativa_qualificacao_agroecossistema_id_aba1fc14; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX qualitativa_qualificacao_agroecossistema_id_aba1fc14 ON public.qualitativa_qualificacao USING btree (agroecossistema_id);


--
-- Name: qualitativa_qualificacao_criterio_id_27fe50af; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX qualitativa_qualificacao_criterio_id_27fe50af ON public.qualitativa_qualificacao USING btree (parametro_id);


--
-- Name: sitetree_tree_alias_c897c375_like; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX sitetree_tree_alias_c897c375_like ON public.sitetree_tree USING btree (alias varchar_pattern_ops);


--
-- Name: sitetree_treeitem_access_guest_09916132; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX sitetree_treeitem_access_guest_09916132 ON public.sitetree_treeitem USING btree (access_guest);


--
-- Name: sitetree_treeitem_access_loggedin_8a523197; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX sitetree_treeitem_access_loggedin_8a523197 ON public.sitetree_treeitem USING btree (access_loggedin);


--
-- Name: sitetree_treeitem_access_permissions_permission_id_c6d1d87a; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX sitetree_treeitem_access_permissions_permission_id_c6d1d87a ON public.sitetree_treeitem_access_permissions USING btree (permission_id);


--
-- Name: sitetree_treeitem_access_permissions_treeitem_id_aedb7367; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX sitetree_treeitem_access_permissions_treeitem_id_aedb7367 ON public.sitetree_treeitem_access_permissions USING btree (treeitem_id);


--
-- Name: sitetree_treeitem_access_restricted_e9c87676; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX sitetree_treeitem_access_restricted_e9c87676 ON public.sitetree_treeitem USING btree (access_restricted);


--
-- Name: sitetree_treeitem_alias_33dc5690; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX sitetree_treeitem_alias_33dc5690 ON public.sitetree_treeitem USING btree (alias);


--
-- Name: sitetree_treeitem_alias_33dc5690_like; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX sitetree_treeitem_alias_33dc5690_like ON public.sitetree_treeitem USING btree (alias varchar_pattern_ops);


--
-- Name: sitetree_treeitem_hidden_5de28c6e; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX sitetree_treeitem_hidden_5de28c6e ON public.sitetree_treeitem USING btree (hidden);


--
-- Name: sitetree_treeitem_inbreadcrumbs_ebb24448; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX sitetree_treeitem_inbreadcrumbs_ebb24448 ON public.sitetree_treeitem USING btree (inbreadcrumbs);


--
-- Name: sitetree_treeitem_inmenu_ccabc0b0; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX sitetree_treeitem_inmenu_ccabc0b0 ON public.sitetree_treeitem USING btree (inmenu);


--
-- Name: sitetree_treeitem_insitetree_60c593a5; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX sitetree_treeitem_insitetree_60c593a5 ON public.sitetree_treeitem USING btree (insitetree);


--
-- Name: sitetree_treeitem_parent_id_88f6f9a4; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX sitetree_treeitem_parent_id_88f6f9a4 ON public.sitetree_treeitem USING btree (parent_id);


--
-- Name: sitetree_treeitem_sort_order_93fd716c; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX sitetree_treeitem_sort_order_93fd716c ON public.sitetree_treeitem USING btree (sort_order);


--
-- Name: sitetree_treeitem_tree_id_038a4bc7; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX sitetree_treeitem_tree_id_038a4bc7 ON public.sitetree_treeitem USING btree (tree_id);


--
-- Name: sitetree_treeitem_url_b91ef35a; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX sitetree_treeitem_url_b91ef35a ON public.sitetree_treeitem USING btree (url);


--
-- Name: sitetree_treeitem_url_b91ef35a_like; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX sitetree_treeitem_url_b91ef35a_like ON public.sitetree_treeitem USING btree (url varchar_pattern_ops);


--
-- Name: sitetree_treeitem_urlaspattern_ff432a51; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX sitetree_treeitem_urlaspattern_ff432a51 ON public.sitetree_treeitem USING btree (urlaspattern);


--
-- Name: socialaccount_socialaccount_user_id_8146e70c; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX socialaccount_socialaccount_user_id_8146e70c ON public.socialaccount_socialaccount USING btree (user_id);


--
-- Name: socialaccount_socialapp_sites_site_id_2579dee5; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX socialaccount_socialapp_sites_site_id_2579dee5 ON public.socialaccount_socialapp_sites USING btree (site_id);


--
-- Name: socialaccount_socialapp_sites_socialapp_id_97fb6e7d; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX socialaccount_socialapp_sites_socialapp_id_97fb6e7d ON public.socialaccount_socialapp_sites USING btree (socialapp_id);


--
-- Name: socialaccount_socialtoken_account_id_951f210e; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX socialaccount_socialtoken_account_id_951f210e ON public.socialaccount_socialtoken USING btree (account_id);


--
-- Name: socialaccount_socialtoken_app_id_636a42d7; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX socialaccount_socialtoken_app_id_636a42d7 ON public.socialaccount_socialtoken USING btree (app_id);


--
-- Name: usuaria_groups_group_id_1db07811; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX usuaria_groups_group_id_1db07811 ON public.usuaria_groups USING btree (group_id);


--
-- Name: usuaria_groups_usuaria_id_9f4c23a9; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX usuaria_groups_usuaria_id_9f4c23a9 ON public.usuaria_groups USING btree (usuaria_id);


--
-- Name: usuaria_user_permissions_permission_id_33142d74; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX usuaria_user_permissions_permission_id_33142d74 ON public.usuaria_user_permissions USING btree (permission_id);


--
-- Name: usuaria_user_permissions_usuaria_id_88e8cd2b; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX usuaria_user_permissions_usuaria_id_88e8cd2b ON public.usuaria_user_permissions USING btree (usuaria_id);


--
-- Name: usuaria_username_508e034c_like; Type: INDEX; Schema: public; Owner: dtygel
--

CREATE INDEX usuaria_username_508e034c_like ON public.usuaria USING btree (username varchar_pattern_ops);


--
-- Name: account_emailaddress account_emailaddress_user_id_2c513194_fk_usuaria_id; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.account_emailaddress
    ADD CONSTRAINT account_emailaddress_user_id_2c513194_fk_usuaria_id FOREIGN KEY (user_id) REFERENCES public.usuaria(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_emailconfirmation account_emailconfirm_email_address_id_5b7f8c58_fk_account_e; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.account_emailconfirmation
    ADD CONSTRAINT account_emailconfirm_email_address_id_5b7f8c58_fk_account_e FOREIGN KEY (email_address_id) REFERENCES public.account_emailaddress(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agroecossistema agroecossistema_autor_id_49d476a5_fk_usuaria_id; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.agroecossistema
    ADD CONSTRAINT agroecossistema_autor_id_49d476a5_fk_usuaria_id FOREIGN KEY (autor_id) REFERENCES public.usuaria(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agroecossistema_cicloanualreferencia agroecossistema_cicl_agroecossistema_id_f9933a3e_fk_agroecoss; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.agroecossistema_cicloanualreferencia
    ADD CONSTRAINT agroecossistema_cicl_agroecossistema_id_f9933a3e_fk_agroecoss FOREIGN KEY (agroecossistema_id) REFERENCES public.agroecossistema(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agroecossistema_composicaonsga agroecossistema_comp_ciclo_anual_referenc_fce774b1_fk_agroecoss; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.agroecossistema_composicaonsga
    ADD CONSTRAINT agroecossistema_comp_ciclo_anual_referenc_fce774b1_fk_agroecoss FOREIGN KEY (ciclo_anual_referencia_id) REFERENCES public.agroecossistema_cicloanualreferencia(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agroecossistema_composicaonsga agroecossistema_comp_pessoa_id_9a2bf840_fk_agroecoss; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.agroecossistema_composicaonsga
    ADD CONSTRAINT agroecossistema_comp_pessoa_id_9a2bf840_fk_agroecoss FOREIGN KEY (pessoa_id) REFERENCES public.agroecossistema_pessoa(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agroecossistema agroecossistema_comunidade_id_5a260e51_fk_comunidade_id; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.agroecossistema
    ADD CONSTRAINT agroecossistema_comunidade_id_5a260e51_fk_comunidade_id FOREIGN KEY (comunidade_id) REFERENCES public.comunidade(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agroecossistema agroecossistema_organizacao_id_b868ea36_fk_organizacao_id; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.agroecossistema
    ADD CONSTRAINT agroecossistema_organizacao_id_b868ea36_fk_organizacao_id FOREIGN KEY (organizacao_id) REFERENCES public.organizacao(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agroecossistema_pessoa agroecossistema_pess_agroecossistema_id_f7e10a4c_fk_agroecoss; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.agroecossistema_pessoa
    ADD CONSTRAINT agroecossistema_pess_agroecossistema_id_f7e10a4c_fk_agroecoss FOREIGN KEY (agroecossistema_id) REFERENCES public.agroecossistema(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agroecossistema_terra agroecossistema_terr_ciclo_anual_referenc_c231f679_fk_agroecoss; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.agroecossistema_terra
    ADD CONSTRAINT agroecossistema_terr_ciclo_anual_referenc_c231f679_fk_agroecoss FOREIGN KEY (ciclo_anual_referencia_id) REFERENCES public.agroecossistema_cicloanualreferencia(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agroecossistema agroecossistema_territorio_id_ff933ec2_fk_main_territorio_id; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.agroecossistema
    ADD CONSTRAINT agroecossistema_territorio_id_ff933ec2_fk_main_territorio_id FOREIGN KEY (territorio_id) REFERENCES public.main_territorio(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: anexos_anexo anexos_anexo_tipo_id_b76b0eca_fk_anexos_tipoanexo_id; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.anexos_anexo
    ADD CONSTRAINT anexos_anexo_tipo_id_b76b0eca_fk_anexos_tipoanexo_id FOREIGN KEY (tipo_id) REFERENCES public.anexos_tipoanexo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: anexos_anexorecipiente anexos_anexorecipien_recipiente_content_t_63a46e84_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.anexos_anexorecipiente
    ADD CONSTRAINT anexos_anexorecipien_recipiente_content_t_63a46e84_fk_django_co FOREIGN KEY (recipiente_content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: anexos_anexorecipiente anexos_anexorecipiente_anexo_id_ca915db2_fk_anexos_anexo_id; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.anexos_anexorecipiente
    ADD CONSTRAINT anexos_anexorecipiente_anexo_id_ca915db2_fk_anexos_anexo_id FOREIGN KEY (anexo_id) REFERENCES public.anexos_anexo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: anexos_tipoanexo anexos_tipoanexo_mae_id_5a628c98_fk_anexos_tipoanexo_id; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.anexos_tipoanexo
    ADD CONSTRAINT anexos_tipoanexo_mae_id_5a628c98_fk_anexos_tipoanexo_id FOREIGN KEY (mae_id) REFERENCES public.anexos_tipoanexo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cadernodecampo_anotacao cadernodecampo_anota_agroecossistema_id_8c12681e_fk_agroecoss; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.cadernodecampo_anotacao
    ADD CONSTRAINT cadernodecampo_anota_agroecossistema_id_8c12681e_fk_agroecoss FOREIGN KEY (agroecossistema_id) REFERENCES public.agroecossistema(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cadernodecampo_anotacao cadernodecampo_anotacao_autor_id_ad0a9b9a_fk_usuaria_id; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.cadernodecampo_anotacao
    ADD CONSTRAINT cadernodecampo_anotacao_autor_id_ad0a9b9a_fk_usuaria_id FOREIGN KEY (autor_id) REFERENCES public.usuaria(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: comunidade comunidade_municipio_id_46341eb3_fk_municipios_municipio_id; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.comunidade
    ADD CONSTRAINT comunidade_municipio_id_46341eb3_fk_municipios_municipio_id FOREIGN KEY (municipio_id) REFERENCES public.municipios_municipio(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_usuaria_id; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_usuaria_id FOREIGN KEY (user_id) REFERENCES public.usuaria(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: economica_analise economica_analise_ciclo_anual_referenc_acf92bda_fk_agroecoss; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_analise
    ADD CONSTRAINT economica_analise_ciclo_anual_referenc_acf92bda_fk_agroecoss FOREIGN KEY (ciclo_anual_referencia_id) REFERENCES public.agroecossistema_cicloanualreferencia(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: economica_estoqueinsumos economica_estoqueins_analise_id_3e420d14_fk_economica; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_estoqueinsumos
    ADD CONSTRAINT economica_estoqueins_analise_id_3e420d14_fk_economica FOREIGN KEY (analise_id) REFERENCES public.economica_analise(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: economica_estoqueinsumos economica_estoqueinsumos_produto_id_6d0649aa_fk_main_produto_id; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_estoqueinsumos
    ADD CONSTRAINT economica_estoqueinsumos_produto_id_6d0649aa_fk_main_produto_id FOREIGN KEY (produto_id) REFERENCES public.main_produto(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: economica_estoqueinsumos economica_estoqueinsumos_unidade_id_40c83a4d_fk_main_unidade_id; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_estoqueinsumos
    ADD CONSTRAINT economica_estoqueinsumos_unidade_id_40c83a4d_fk_main_unidade_id FOREIGN KEY (unidade_id) REFERENCES public.main_unidade(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: economica_pagamentoterceiros economica_pagamentot_analise_id_299816b8_fk_economica; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_pagamentoterceiros
    ADD CONSTRAINT economica_pagamentot_analise_id_299816b8_fk_economica FOREIGN KEY (analise_id) REFERENCES public.economica_analise(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: economica_pagamentoterceiros economica_pagamentot_servico_id_310af121_fk_main_serv; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_pagamentoterceiros
    ADD CONSTRAINT economica_pagamentot_servico_id_310af121_fk_main_serv FOREIGN KEY (servico_id) REFERENCES public.main_servico(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: economica_pagamentoterceiros economica_pagamentot_unidade_id_ace7a7db_fk_main_unid; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_pagamentoterceiros
    ADD CONSTRAINT economica_pagamentot_unidade_id_ace7a7db_fk_main_unid FOREIGN KEY (unidade_id) REFERENCES public.main_unidade(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: economica_patrimonio economica_patrimonio_analise_id_5046837b_fk_economica; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_patrimonio
    ADD CONSTRAINT economica_patrimonio_analise_id_5046837b_fk_economica FOREIGN KEY (analise_id) REFERENCES public.economica_analise(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: economica_patrimonio economica_patrimonio_unidade_id_d46316a8_fk_main_unidade_id; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_patrimonio
    ADD CONSTRAINT economica_patrimonio_unidade_id_d46316a8_fk_main_unidade_id FOREIGN KEY (unidade_id) REFERENCES public.main_unidade(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: economica_rendanaoagricola economica_rendanaoag_analise_id_d254cfbf_fk_economica; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_rendanaoagricola
    ADD CONSTRAINT economica_rendanaoag_analise_id_d254cfbf_fk_economica FOREIGN KEY (analise_id) REFERENCES public.economica_analise(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: economica_rendanaoagricola economica_rendanaoag_unidade_id_162294dd_fk_main_unid; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_rendanaoagricola
    ADD CONSTRAINT economica_rendanaoag_unidade_id_162294dd_fk_main_unid FOREIGN KEY (unidade_id) REFERENCES public.main_unidade(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: economica_subsistema economica_subsistema_analise_id_3e1b5199_fk_economica; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_subsistema
    ADD CONSTRAINT economica_subsistema_analise_id_3e1b5199_fk_economica FOREIGN KEY (analise_id) REFERENCES public.economica_analise(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: economica_subsistematempotrabalho economica_subsistema_composicao_nsga_id_be7d9465_fk_agroecoss; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_subsistematempotrabalho
    ADD CONSTRAINT economica_subsistema_composicao_nsga_id_be7d9465_fk_agroecoss FOREIGN KEY (composicao_nsga_id) REFERENCES public.agroecossistema_composicaonsga(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: economica_subsistemainsumo economica_subsistema_produto_id_029e72ca_fk_main_prod; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_subsistemainsumo
    ADD CONSTRAINT economica_subsistema_produto_id_029e72ca_fk_main_prod FOREIGN KEY (produto_id) REFERENCES public.main_produto(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: economica_subsistemaproducao economica_subsistema_produto_id_475e8983_fk_main_prod; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_subsistemaproducao
    ADD CONSTRAINT economica_subsistema_produto_id_475e8983_fk_main_prod FOREIGN KEY (produto_id) REFERENCES public.main_produto(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: economica_subsistemaaquisicaoreciprocidade economica_subsistema_produto_id_d2f78a71_fk_main_prod; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_subsistemaaquisicaoreciprocidade
    ADD CONSTRAINT economica_subsistema_produto_id_d2f78a71_fk_main_prod FOREIGN KEY (produto_id) REFERENCES public.main_produto(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: economica_subsistemapagamentoterceiros economica_subsistema_servico_id_6960909e_fk_main_serv; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_subsistemapagamentoterceiros
    ADD CONSTRAINT economica_subsistema_servico_id_6960909e_fk_main_serv FOREIGN KEY (servico_id) REFERENCES public.main_servico(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: economica_subsistemaaquisicaoreciprocidade economica_subsistema_servico_id_8d25466e_fk_main_serv; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_subsistemaaquisicaoreciprocidade
    ADD CONSTRAINT economica_subsistema_servico_id_8d25466e_fk_main_serv FOREIGN KEY (servico_id) REFERENCES public.main_servico(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: economica_subsistemaaquisicaoreciprocidade economica_subsistema_subsistema_id_29f62c8d_fk_economica; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_subsistemaaquisicaoreciprocidade
    ADD CONSTRAINT economica_subsistema_subsistema_id_29f62c8d_fk_economica FOREIGN KEY (subsistema_id) REFERENCES public.economica_subsistema(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: economica_subsistematempotrabalho economica_subsistema_subsistema_id_5c6c907e_fk_economica; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_subsistematempotrabalho
    ADD CONSTRAINT economica_subsistema_subsistema_id_5c6c907e_fk_economica FOREIGN KEY (subsistema_id) REFERENCES public.economica_subsistema(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: economica_subsistemapagamentoterceiros economica_subsistema_subsistema_id_6ada1432_fk_economica; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_subsistemapagamentoterceiros
    ADD CONSTRAINT economica_subsistema_subsistema_id_6ada1432_fk_economica FOREIGN KEY (subsistema_id) REFERENCES public.economica_subsistema(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: economica_subsistemainsumo economica_subsistema_subsistema_id_d17b8d68_fk_economica; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_subsistemainsumo
    ADD CONSTRAINT economica_subsistema_subsistema_id_d17b8d68_fk_economica FOREIGN KEY (subsistema_id) REFERENCES public.economica_subsistema(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: economica_subsistemaproducao economica_subsistema_subsistema_id_fb22da39_fk_economica; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_subsistemaproducao
    ADD CONSTRAINT economica_subsistema_subsistema_id_fb22da39_fk_economica FOREIGN KEY (subsistema_id) REFERENCES public.economica_subsistema(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: economica_subsistemaaquisicaoreciprocidade economica_subsistema_unidade_id_3057b659_fk_main_unid; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_subsistemaaquisicaoreciprocidade
    ADD CONSTRAINT economica_subsistema_unidade_id_3057b659_fk_main_unid FOREIGN KEY (unidade_id) REFERENCES public.main_unidade(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: economica_subsistemapagamentoterceiros economica_subsistema_unidade_id_8f528dd0_fk_main_unid; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_subsistemapagamentoterceiros
    ADD CONSTRAINT economica_subsistema_unidade_id_8f528dd0_fk_main_unid FOREIGN KEY (unidade_id) REFERENCES public.main_unidade(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: economica_subsistemainsumo economica_subsistema_unidade_id_93ea6dc1_fk_main_unid; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_subsistemainsumo
    ADD CONSTRAINT economica_subsistema_unidade_id_93ea6dc1_fk_main_unid FOREIGN KEY (unidade_id) REFERENCES public.main_unidade(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: economica_subsistemaproducao economica_subsistema_unidade_id_de8407ab_fk_main_unid; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_subsistemaproducao
    ADD CONSTRAINT economica_subsistema_unidade_id_de8407ab_fk_main_unid FOREIGN KEY (unidade_id) REFERENCES public.main_unidade(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: economica_tempotrabalho economica_tempotraba_analise_id_3d3a659f_fk_economica; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_tempotrabalho
    ADD CONSTRAINT economica_tempotraba_analise_id_3d3a659f_fk_economica FOREIGN KEY (analise_id) REFERENCES public.economica_analise(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: economica_tempotrabalho economica_tempotraba_composicao_nsga_id_71670b3f_fk_agroecoss; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.economica_tempotrabalho
    ADD CONSTRAINT economica_tempotraba_composicao_nsga_id_71670b3f_fk_agroecoss FOREIGN KEY (composicao_nsga_id) REFERENCES public.agroecossistema_composicaonsga(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: linhadotempo_dimensao linhadotempo_dimensa_tipo_dimensao_id_2f7c0a92_fk_linhadote; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.linhadotempo_dimensao
    ADD CONSTRAINT linhadotempo_dimensa_tipo_dimensao_id_2f7c0a92_fk_linhadote FOREIGN KEY (tipo_dimensao_id) REFERENCES public.linhadotempo_tipodimensao(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: linhadotempo_evento linhadotempo_evento_agroecossistema_id_3dd6efe8_fk_agroecoss; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.linhadotempo_evento
    ADD CONSTRAINT linhadotempo_evento_agroecossistema_id_3dd6efe8_fk_agroecoss FOREIGN KEY (agroecossistema_id) REFERENCES public.agroecossistema(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: linhadotempo_evento linhadotempo_evento_criado_por_id_b8923b5b_fk_usuaria_id; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.linhadotempo_evento
    ADD CONSTRAINT linhadotempo_evento_criado_por_id_b8923b5b_fk_usuaria_id FOREIGN KEY (criado_por_id) REFERENCES public.usuaria(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: linhadotempo_evento linhadotempo_evento_dimensao_id_902e8d17_fk_linhadote; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.linhadotempo_evento
    ADD CONSTRAINT linhadotempo_evento_dimensao_id_902e8d17_fk_linhadote FOREIGN KEY (dimensao_id) REFERENCES public.linhadotempo_dimensao(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: linhadotempo_eventogrupo linhadotempo_eventog_agroecossistema_id_ec94d243_fk_agroecoss; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.linhadotempo_eventogrupo
    ADD CONSTRAINT linhadotempo_eventog_agroecossistema_id_ec94d243_fk_agroecoss FOREIGN KEY (agroecossistema_id) REFERENCES public.agroecossistema(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: linhadotempo_eventogrupo_eventos linhadotempo_eventog_evento_id_c95bad9a_fk_linhadote; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.linhadotempo_eventogrupo_eventos
    ADD CONSTRAINT linhadotempo_eventog_evento_id_c95bad9a_fk_linhadote FOREIGN KEY (evento_id) REFERENCES public.linhadotempo_evento(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: linhadotempo_eventogrupo_eventos linhadotempo_eventog_eventogrupo_id_ea2f8c2d_fk_linhadote; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.linhadotempo_eventogrupo_eventos
    ADD CONSTRAINT linhadotempo_eventog_eventogrupo_id_ea2f8c2d_fk_linhadote FOREIGN KEY (eventogrupo_id) REFERENCES public.linhadotempo_eventogrupo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_organizacaousuaria main_organizacaousua_organizacao_id_20671b76_fk_organizac; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.main_organizacaousuaria
    ADD CONSTRAINT main_organizacaousua_organizacao_id_20671b76_fk_organizac FOREIGN KEY (organizacao_id) REFERENCES public.organizacao(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_organizacaousuaria main_organizacaousuaria_usuaria_id_f618d019_fk_usuaria_id; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.main_organizacaousuaria
    ADD CONSTRAINT main_organizacaousuaria_usuaria_id_f618d019_fk_usuaria_id FOREIGN KEY (usuaria_id) REFERENCES public.usuaria(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: municipios_municipio municipios_municipio_diocese_id_bd117b70_fk_municipio; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.municipios_municipio
    ADD CONSTRAINT municipios_municipio_diocese_id_bd117b70_fk_municipio FOREIGN KEY (diocese_id) REFERENCES public.municipios_diocese(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: municipios_municipio municipios_municipio_regiao_imediata_id_cbad6130_fk_municipio; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.municipios_municipio
    ADD CONSTRAINT municipios_municipio_regiao_imediata_id_cbad6130_fk_municipio FOREIGN KEY (regiao_imediata_id) REFERENCES public.municipios_regiaoimediata(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: municipios_municipio municipios_municipio_uf_id_0ca32a2d_fk_municipios_uf_id; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.municipios_municipio
    ADD CONSTRAINT municipios_municipio_uf_id_0ca32a2d_fk_municipios_uf_id FOREIGN KEY (uf_id) REFERENCES public.municipios_uf(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: municipios_regiaoimediata municipios_regiaoime_regiao_intermediaria_c372499c_fk_municipio; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.municipios_regiaoimediata
    ADD CONSTRAINT municipios_regiaoime_regiao_intermediaria_c372499c_fk_municipio FOREIGN KEY (regiao_intermediaria_id) REFERENCES public.municipios_regiaointermediaria(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: municipios_regiaointermediaria municipios_regiaoint_uf_id_e4427eba_fk_municipio; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.municipios_regiaointermediaria
    ADD CONSTRAINT municipios_regiaoint_uf_id_e4427eba_fk_municipio FOREIGN KEY (uf_id) REFERENCES public.municipios_uf(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: municipios_uf municipios_uf_regiao_id_77e9c7d4_fk_municipios_regiao_id; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.municipios_uf
    ADD CONSTRAINT municipios_uf_regiao_id_77e9c7d4_fk_municipios_regiao_id FOREIGN KEY (regiao_id) REFERENCES public.municipios_regiao(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: qualitativa_analise qualitativa_analise_agroecossistema_id_77a1fa3d_fk_agroecoss; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.qualitativa_analise
    ADD CONSTRAINT qualitativa_analise_agroecossistema_id_77a1fa3d_fk_agroecoss FOREIGN KEY (agroecossistema_id) REFERENCES public.agroecossistema(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: qualitativa_avaliacaoparametro qualitativa_avaliaca_analise_id_824b864e_fk_qualitati; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.qualitativa_avaliacaoparametro
    ADD CONSTRAINT qualitativa_avaliaca_analise_id_824b864e_fk_qualitati FOREIGN KEY (analise_id) REFERENCES public.qualitativa_analise(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: qualitativa_avaliacaoparametro qualitativa_avaliaca_parametro_id_f393ae26_fk_qualitati; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.qualitativa_avaliacaoparametro
    ADD CONSTRAINT qualitativa_avaliaca_parametro_id_f393ae26_fk_qualitati FOREIGN KEY (parametro_id) REFERENCES public.qualitativa_parametro(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: qualitativa_parametro qualitativa_criterio_agroecossistema_id_3e24c437_fk_agroecoss; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.qualitativa_parametro
    ADD CONSTRAINT qualitativa_criterio_agroecossistema_id_3e24c437_fk_agroecoss FOREIGN KEY (agroecossistema_id) REFERENCES public.agroecossistema(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: qualitativa_parametro qualitativa_criterio_atributo_id_ebd05d45_fk_qualitati; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.qualitativa_parametro
    ADD CONSTRAINT qualitativa_criterio_atributo_id_ebd05d45_fk_qualitati FOREIGN KEY (atributo_id) REFERENCES public.qualitativa_atributo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: qualitativa_qualificacao qualitativa_qualific_agroecossistema_id_aba1fc14_fk_agroecoss; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.qualitativa_qualificacao
    ADD CONSTRAINT qualitativa_qualific_agroecossistema_id_aba1fc14_fk_agroecoss FOREIGN KEY (agroecossistema_id) REFERENCES public.agroecossistema(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: qualitativa_qualificacao qualitativa_qualific_parametro_id_26ea810a_fk_qualitati; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.qualitativa_qualificacao
    ADD CONSTRAINT qualitativa_qualific_parametro_id_26ea810a_fk_qualitati FOREIGN KEY (parametro_id) REFERENCES public.qualitativa_parametro(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sitetree_treeitem_access_permissions sitetree_treeitem_ac_permission_id_c6d1d87a_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.sitetree_treeitem_access_permissions
    ADD CONSTRAINT sitetree_treeitem_ac_permission_id_c6d1d87a_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sitetree_treeitem_access_permissions sitetree_treeitem_ac_treeitem_id_aedb7367_fk_sitetree_; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.sitetree_treeitem_access_permissions
    ADD CONSTRAINT sitetree_treeitem_ac_treeitem_id_aedb7367_fk_sitetree_ FOREIGN KEY (treeitem_id) REFERENCES public.sitetree_treeitem(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sitetree_treeitem sitetree_treeitem_parent_id_88f6f9a4_fk_sitetree_treeitem_id; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.sitetree_treeitem
    ADD CONSTRAINT sitetree_treeitem_parent_id_88f6f9a4_fk_sitetree_treeitem_id FOREIGN KEY (parent_id) REFERENCES public.sitetree_treeitem(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sitetree_treeitem sitetree_treeitem_tree_id_038a4bc7_fk_sitetree_tree_id; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.sitetree_treeitem
    ADD CONSTRAINT sitetree_treeitem_tree_id_038a4bc7_fk_sitetree_tree_id FOREIGN KEY (tree_id) REFERENCES public.sitetree_tree(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: socialaccount_socialtoken socialaccount_social_account_id_951f210e_fk_socialacc; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.socialaccount_socialtoken
    ADD CONSTRAINT socialaccount_social_account_id_951f210e_fk_socialacc FOREIGN KEY (account_id) REFERENCES public.socialaccount_socialaccount(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: socialaccount_socialtoken socialaccount_social_app_id_636a42d7_fk_socialacc; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.socialaccount_socialtoken
    ADD CONSTRAINT socialaccount_social_app_id_636a42d7_fk_socialacc FOREIGN KEY (app_id) REFERENCES public.socialaccount_socialapp(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: socialaccount_socialapp_sites socialaccount_social_site_id_2579dee5_fk_django_si; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.socialaccount_socialapp_sites
    ADD CONSTRAINT socialaccount_social_site_id_2579dee5_fk_django_si FOREIGN KEY (site_id) REFERENCES public.django_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: socialaccount_socialapp_sites socialaccount_social_socialapp_id_97fb6e7d_fk_socialacc; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.socialaccount_socialapp_sites
    ADD CONSTRAINT socialaccount_social_socialapp_id_97fb6e7d_fk_socialacc FOREIGN KEY (socialapp_id) REFERENCES public.socialaccount_socialapp(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: socialaccount_socialaccount socialaccount_socialaccount_user_id_8146e70c_fk_usuaria_id; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.socialaccount_socialaccount
    ADD CONSTRAINT socialaccount_socialaccount_user_id_8146e70c_fk_usuaria_id FOREIGN KEY (user_id) REFERENCES public.usuaria(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: usuaria_groups usuaria_groups_group_id_1db07811_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.usuaria_groups
    ADD CONSTRAINT usuaria_groups_group_id_1db07811_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: usuaria_groups usuaria_groups_usuaria_id_9f4c23a9_fk_usuaria_id; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.usuaria_groups
    ADD CONSTRAINT usuaria_groups_usuaria_id_9f4c23a9_fk_usuaria_id FOREIGN KEY (usuaria_id) REFERENCES public.usuaria(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: usuaria_user_permissions usuaria_user_permiss_permission_id_33142d74_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.usuaria_user_permissions
    ADD CONSTRAINT usuaria_user_permiss_permission_id_33142d74_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: usuaria_user_permissions usuaria_user_permissions_usuaria_id_88e8cd2b_fk_usuaria_id; Type: FK CONSTRAINT; Schema: public; Owner: dtygel
--

ALTER TABLE ONLY public.usuaria_user_permissions
    ADD CONSTRAINT usuaria_user_permissions_usuaria_id_88e8cd2b_fk_usuaria_id FOREIGN KEY (usuaria_id) REFERENCES public.usuaria(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

