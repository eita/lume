from django.urls import reverse
import unicodedata
from django.db import transaction
from django.db.models import Q, Case, When, Value, IntegerField
from django.apps import apps
from django.contrib.contenttypes.fields import GenericForeignKey
from django.utils.text import slugify
from django.utils.encoding import smart_str

from main import messages_from_db


def get_name_initials(name):
    words = name.upper().split(' ')

    if len(words) == 1:
        return words[0][0:2]

    if len(words) <= 2 and len(words[1]) <= 2:
        return words[0][0:2]

    if len(words[1]) <= 2 and len(words) >= 3:
        return words[0][0:1] + words[2][0:1]

    return words[0][0:1] + words[1][0:1]


def build_navbar_tab(args):
    target = args['target']
    sim_slug = args['sim_slug']
    submodulo = args['submodulo']
    agroecossistema_id = args['agroecossistema_id']
    ano_car = args['ano_car']
    subsistema_id = args['subsistema_id']

    extra_class = " aba_subsistema" if target['submodulo'][:10] == "subsistema" else ""
    if sim_slug:
        action = "ecosim:"
    else:
        action = "economica:"

    if "sub_abas" in target:
        first_sub_aba = target['sub_abas'][0]
        action += first_sub_aba['target_id']
        if 'tipo' in first_sub_aba:
            action += "_" + first_sub_aba['tipo']
    elif "sub_abas_automaticas" in target:
        action += target['submodulo'] + "_" + \
            target['sub_abas_automaticas'][0][0]
    else:
        action += target['submodulo']

    action += "_update"

    active = " active" if target['submodulo'] == submodulo else ""
    if active:
        url = "#"
    else:
        url_args = {
            'agroecossistema_id': agroecossistema_id,
            'ano_car': ano_car
        }
        if sim_slug:
            url_args['sim_slug'] = sim_slug
        if target['submodulo'][:10] == 'subsistema':
            if subsistema_id:
                url_args['pk'] = subsistema_id
            else:
                action = 'economica:subsistema_vazio'

        try:
            url = reverse(action, kwargs=url_args)
        except:
            url = None

    title = target['title'] if not 'title_short' in target else target['title_short']

    return {
        'title': title,
        'extra_class': extra_class,
        'active': active,
        'url': url
    }


def fazer_buscavel(text):
    try:
        text = unicode(text, 'utf-8')
    except NameError:
        pass
    text = unicodedata.normalize('NFD', text).encode(
        'ascii', 'ignore').decode("utf-8")
    return str(text).lower().strip()


def busca_inteligente(qs, text, field='nome'):
    if text:
        buscavel = fazer_buscavel(text)
        limit = 3

        filter_istartwith = field + '__unaccent__lower__istartswith'
        q1 = Q(**{filter_istartwith: buscavel})

        filter_trigram = field + '__unaccent__lower__trigram_similar'
        q2 = Q(**{filter_trigram: buscavel})

        qs_final = (
            qs
            .filter(q1 | q2)
            .annotate(
                search_type_ordering=Case(
                    When(q1, then=Value(2)),
                    When(q2, then=Value(1)),
                    default=Value(-1),
                    output_field=IntegerField(),
                )
            )
            .order_by('-search_type_ordering')
        )

        return qs_final

    return qs


def merge_model_objects(primary_object, alias_objects):
    """
    Use this function to merge model objects (i.e. Users, Organizations, Polls, Etc.) and migrate all of the related fields from the alias objects the primary object.

    Usage:
    from django.contrib.auth.models import User
    primary_user = User.objects.get(email='good_email@example.com')
    duplicate_user = User.objects.get(email='good_email+duplicate@example.com')
    merge_model_objects(primary_user, duplicate_user)
    """
    # Get a list of all GenericForeignKeys in all models
    # TODO: this is a bit of a hack, since the generics framework should provide a similar
    # method to the ForeignKey field for accessing the generic related fields.
    generic_fields = []
    for model in apps.get_models():
        for field_name, field in filter(lambda x: isinstance(x[1], GenericForeignKey), model.__dict__.items()):
            generic_fields.append(field)

    # Loop through all alias objects and migrate their data to the primary object.
    for alias_object in alias_objects:
        try:
            # Migrate all foreign key references from alias object to primary object.
            for related_object in alias_object._meta.get_fields():
                if type(related_object).__name__ == 'ManyToOneRel':
                    # The variable name on the alias_object model.
                    alias_varname = related_object.get_accessor_name()
                    # The variable name on the related model.
                    obj_varname = related_object.field.name
                    related_objects = getattr(alias_object, alias_varname)
                    # breakpoint()
                    filterStr = obj_varname + '_id'
                    for obj in related_objects.deleted_qs.filter(**{filterStr: alias_object.id}):
                        try:
                            # prodserv = 'servico' if hasattr(obj, 'servico') else 'produto'
                            # print(str(getattr(obj, prodserv).nome) + ' - ' + getattr(obj,obj_varname).nome + ':' + str(getattr(obj,obj_varname).id))
                            # print(primary_object.nome + ':' + str(primary_object.id))
                            # print(alias_object.nome + ':' + str(alias_object.id) + "\n")
                            setattr(obj, obj_varname, primary_object)
                            obj.save()
                        except Exception as e:
                            print('Exception: ' + str(e))
                elif type(related_object).__name__ == 'ManyToManyRel':
                    alias_varname = related_object.get_accessor_name()
                    obj_varname = related_object.field.name
                    related_objects = getattr(alias_object, alias_varname)
                    for obj in related_objects.deleted_qs:
                        try:
                            getattr(obj, obj_varname).remove(alias_object)
                            getattr(obj, obj_varname).add(primary_object)
                        except:
                            print('Exception: ' + str(e))
                else:
                    continue

            # Migrate all generic foreign key references from alias object to primary object.
            for field in generic_fields:
                filter_kwargs = {}
                filter_kwargs[field.fk_field] = alias_object._get_pk_val()
                filter_kwargs[field.ct_field] = field.get_content_type(
                    alias_object)
                for generic_related_object in field.model.objects.filter(**filter_kwargs):
                    setattr(generic_related_object, field.name, primary_object)
                    generic_related_object.save()
            # Apaga o objeto "alias"
            alias_object.delete()
        except Exception as e:
            print("A operação falhou! Erro: " + str(e))
            transaction.rollback()
        else:
            transaction.commit()
    return primary_object


def getHelpersI18n(helpersMethod):
    return getattr(messages_from_db, helpersMethod)


def getHelperI18n(helpersMethod, obj, keyRaw):
    if keyRaw == "":
        return ""
    key = slugify(keyRaw.lower())
    helpers = getHelpersI18n(helpersMethod)
    return helpers[key] if key in helpers else obj.__dict__


def get_qualitativa_helper_i18n(parametro, atributo):
    helpers = getHelpersI18n('qualitativa_helpers')
    atributo_key = slugify(atributo.nome.lower())
    parametro_key = slugify(parametro.nome.lower())
    parametros = helpers.get(atributo_key, None)
    if parametros is not None:
        result = parametros.get(parametro_key, None)
        if result is not None:
            return result
    return {
        'atributo': atributo.nome,
        'parametro': parametro.nome,
        'descricao': parametro.descricao,
        'subsection': parametro.subsection,
    }


def lista_agroecossistemas_values(queryset):
    if not queryset:
        return queryset

    return queryset.values(
        'id',
        'nsga',
        'organizacao__nome',
        'comunidade__nome',
        'comunidade__municipio__nome',
        'comunidade__municipio__uf',
        'comunidade__pais__nome',
        'territorio__nome',
        'criacao',
        'autor__first_name'
    )


def lista_comunidades_values(queryset):
    if not queryset:
        return queryset

    return queryset.values(
        'id',
        'nome',
        'criacao',
        'pais__nome',
        'municipio__nome',
        'municipio__uf__nome'
    )


def lista_territorios_values(queryset):
    if not queryset:
        return queryset

    return queryset.values(
        'id',
        'nome',
        'criacao',
        'descricao'
    )


def lista_analisesagregadas_values(queryset):
    if not queryset:
        return queryset

    return queryset.values(
        'id',
        'nome',
        'descricao',
        'evento_gatilho__titulo',
        'ano',
        'organizacao__nome',
        'autor__first_name'
    )


def parse_args_kwargs_and_as_var(parser, bits):
    args = []
    kwargs = {}
    as_var = None

    bits = iter(bits)
    for bit in bits:
        if bit == 'as':
            as_var = bits.next()
            break
        else:
            for arg in bit.split(","):
                if '=' in arg:
                    k, v = arg.split('=', 1)
                    k = k.strip()
                    kwargs[k] = parser.compile_filter(v)
                elif arg:
                    args.append(parser.compile_filter(arg))
    return args, kwargs, as_var


def get_args_and_kwargs(args, kwargs, context):
    out_args = [arg.resolve(context) for arg in args]
    out_kwargs = dict([(smart_str(k, 'ascii'), v.resolve(context))
                      for k, v in kwargs.items()])
    return out_args, out_kwargs
