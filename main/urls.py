from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from django.views.generic import RedirectView

from main.models import Unidade, Servico, Produto
from . import views
from linhadotempo import views as linhadotempo_views
from linhaterritorial import views as linhaterritorial_views
from qualitativa import views as qualitativa_views

urlpatterns = [
    path('', views.Index.as_view(), name='MainIndex'),
    # path('', RedirectView.as_view(url='/agroecossistema/'), name="MainIndex"),
]

urlpatterns += (
    # urls for Comunidade
    path('comunidade/', views.ComunidadeListView.as_view(), name='comunidade_list'),
    path('comunidade/create/', views.ComunidadeCreateView.as_view(),
         name='comunidade_create'),
    path('comunidade/<int:pk>/detail/',
         views.ComunidadeDetailView.as_view(), name='comunidade_detail'),
    path('comunidade/<int:pk>/detail/json/',
         views.ComunidadeDetailJSONView.as_view(), name='comunidade_detail_json'),
    path('comunidade/<int:pk>/update/',
         views.ComunidadeUpdateView.as_view(), name='comunidade_update'),
    path('comunidade/<int:pk>/delete/',
         views.ComunidadeDeleteView.as_view(), name='comunidade_delete'),
    path('comunidade/<int:pk>/linhaterritorial',
         linhaterritorial_views.EventoTerritorialListView.as_view(),
         {'related_model': 'Comunidade'},
         name='comunidade_linhaterritorial'
         ),
    path('comunidade/<int:pk>/linhaterritorial/periodos/',
         linhaterritorial_views.PeriodosView.as_view(),
         {'related_model': 'Comunidade'},
         name='comunidade_linhaterritorial_periodos'
         ),
    path('comunidade/<int:pk>/linhaterritorial/api/dimensoes/<int:tipo_dimensao_id>/',
         linhaterritorial_views.ApiDimensaoView.as_view(),
         {'related_model': 'Comunidade'},
         name='comunidade_linhaterritorial_api_dimensoes'
         ),
    path('comunidade/<int:pk>/linhaterritorial/api/dimensoes/',
         linhaterritorial_views.ApiDimensaoView.as_view(),
         {'related_model': 'Comunidade', 'empty': True},
         name='comunidade_linhaterritorial_api_dimensoes'
         ),
    path('comunidade/<int:pk>/linhaterritorial/api/tipos_dimensao/',
         linhaterritorial_views.ApiDimensaoView.as_view(),
         {'related_model': 'Comunidade'},
         name='comunidade_linhaterritorial_api_tipos_dimensao'
         ),
    path('comunidade/<int:pk>/agroecossistemas',
         views.ComunidadeAgroecossistemasListView.as_view(), name='comunidade_agroecossistemas'),
)

urlpatterns += (
    # urls for Territorio
    path('territorio/', views.TerritorioListView.as_view(), name='territorio_list'),
    path('territorio/create/', views.TerritorioCreateView.as_view(),
         name='territorio_create'),
    path('territorio/<int:pk>/detail/',
         views.TerritorioDetailView.as_view(), name='territorio_detail'),
    path('territorio/<int:pk>/detail/json/',
         views.TerritorioDetailJSONView.as_view(), name='territorio_detail_json'),
    path('territorio/<int:pk>/update/',
         views.TerritorioUpdateView.as_view(), name='territorio_update'),
    path('territorio/<int:pk>/delete/',
         views.TerritorioDeleteView.as_view(), name='territorio_delete'),
    path('territorio/<int:pk>/linhaterritorial/',
         linhaterritorial_views.EventoTerritorialListView.as_view(),
         {'related_model': 'Territorio'},
         name='territorio_linhaterritorial'
         ),
    path('territorio/<int:pk>/linhaterritorial/periodos/',
         linhaterritorial_views.PeriodosView.as_view(),
         {'related_model': 'Territorio'},
         name='territorio_linhaterritorial_periodos'
         ),
    path('territorio/<int:pk>/linhaterritorial/api/dimensoes/<int:tipo_dimensao_id>/',
         linhaterritorial_views.ApiDimensaoView.as_view(),
         {'related_model': 'Territorio'},
         name='territorio_linhaterritorial_api_dimensoes'
         ),
    path('territorio/<int:pk>/linhaterritorial/api/dimensoes/',
         linhaterritorial_views.ApiDimensaoView.as_view(),
         {'related_model': 'Territorio', 'empty': True},
         name='territorio_linhaterritorial_api_dimensoes'
         ),
    path('comunidade/<int:pk>/linhaterritorial/api/tipos_dimensao/',
         linhaterritorial_views.ApiDimensaoView.as_view(),
         {'related_model': 'Territorio'},
         name='territorio_linhaterritorial_api_tipos_dimensao'
         ),
    path('territorio/<int:pk>/agroecossistemas',
         views.TerritorioAgroecossistemasListView.as_view(), name='territorio_agroecossistemas'),
)

urlpatterns += (
    # urls for Organizacao
    path('organizacao/', views.OrganizacaoListView.as_view(),
         name='organizacao_list'),
    path('organizacao/create/', views.OrganizacaoCreateView.as_view(),
         name='organizacao_create'),
    path('organizacao/detail/<int:pk>/',
         views.OrganizacaoDetailView.as_view(), name='organizacao_detail'),
    path('organizacao/update/<int:pk>/',
         views.OrganizacaoUpdateView.as_view(), name='organizacao_update'),
    path('organizacao/delete/<int:pk>/',
         views.OrganizacaoDeleteView.as_view(), name='organizacao_delete'),
    path('organizacao-autocomplete/', views.OrganizacaoAutocomplete.as_view(),
         name='organizacao_autocomplete'),
)

urlpatterns += (
    # urls for OrganizacaoUsuaria
    path('organizacaousuaria/update', views.OrganizacaoUsuariaUpdateView.as_view(),
         name='organizacaousuaria_update'),
    path('organizacaousuaria/delete/<int:pk>',
         views.OrganizacaoUsuariaDeleteView.as_view(), name='organizacaousuaria_delete')
)

urlpatterns += (
    path('organizacao/<int:organizacao_id>/create_usuaria/',
         views.UsuariaCreateView.as_view(), name='usuaria_create'),
)

urlpatterns += (
    # urls for Unidade
    path('unidade/', views.UnidadeListView.as_view(), name='unidade_list'),
    path('unidade/create/', views.UnidadeCreateView.as_view(), name='unidade_create'),
    path('unidade/detail/<int:pk>/',
         views.UnidadeDetailView.as_view(), name='unidade_detail'),
    path('unidade/update/<int:pk>/',
         views.UnidadeUpdateView.as_view(), name='unidade_update'),
    path('unidade/delete/<int:pk>/',
         views.UnidadeDeleteView.as_view(), name='unidade_delete'),
    path('unidade-autocomplete/', views.UnidadeAutocomplete.as_view(
        create_field='nome', model=Unidade), name='unidade_autocomplete'),
)

urlpatterns += (
    # urls for Servico
    path('servico/', views.ServicoListView.as_view(), name='servico_list'),
    path('servico/create/', views.ServicoCreateView.as_view(), name='servico_create'),
    path('servico/detail/<int:pk>/',
         views.ServicoDetailView.as_view(), name='servico_detail'),
    path('servico/update/<int:pk>/',
         views.ServicoUpdateView.as_view(), name='servico_update'),
    path('servico/delete/<int:pk>/',
         views.ServicoDeleteView.as_view(), name='servico_delete'),
    path('servico-autocomplete/', views.ServicoAutocomplete.as_view(
        create_field='nome', model=Servico), name='servico_autocomplete'),
)

urlpatterns += (
    # urls for Produto
    path('produto/', views.ProdutoListView.as_view(), name='produto_list'),
    path('produto/create/', views.ProdutoCreateView.as_view(), name='produto_create'),
    path('produto/detail/<int:pk>/',
         views.ProdutoDetailView.as_view(), name='produto_detail'),
    path('produto/update/<int:pk>/',
         views.ProdutoUpdateView.as_view(), name='produto_update'),
    path('produto/delete/<int:pk>/',
         views.ProdutoDeleteView.as_view(), name='produto_delete'),
    path('produto-autocomplete/', views.ProdutoAutocomplete.as_view(
        create_field='nome', model=Produto), name='produto_autocomplete'),
)

urlpatterns += (
    path('ajuda/', views.AjudaView.as_view(), name='ajuda'),
    path('ajuda_indicador/', views.AjudaIndicadorView.as_view(),
         name='ajuda_indicador'),
)

urlpatterns += (
    # urls for EventoGatilho
    path('eventogatilho/', linhadotempo_views.EventoGatilhoListView.as_view(),
         name='eventogatilho_list'),
    path('eventogatilho/create/', linhadotempo_views.EventoGatilhoCreateView.as_view(),
         name='eventogatilho_create'),
    path('eventogatilho/detail/<int:pk>/',
         linhadotempo_views.EventoGatilhoDetailView.as_view(), name='eventogatilho_detail'),
    path('eventogatilho/update/<int:pk>/',
         linhadotempo_views.EventoGatilhoUpdateView.as_view(), name='eventogatilho_update'),
    path('eventogatilho/delete/<int:pk>/',
         linhadotempo_views.EventoGatilhoDeleteView.as_view(), name='eventogatilho_delete'),
    path('eventogatilho-autocomplete/', views.EventoGatilhoAutocomplete.as_view(),
         name='eventogatilho_autocomplete'),
)

urlpatterns += (
    # urls for CicloAnualReferencia - autocomplete
    path('car-autocomplete/', views.CicloAnualReferenciaAutocomplete.as_view(),
         name='car_autocomplete'),
)

urlpatterns += (
    # urls for Qualitativa - autocomplete
    path('atributo-autocomplete/', qualitativa_views.AtributoAutocomplete.as_view(),
         name='atributo_autocomplete'),
    path('parametro-autocomplete/', qualitativa_views.ParametroAutocomplete.as_view(),
         name='parametro_autocomplete'),
)

urlpatterns += (
    # MODELOS DE ANALISE QUALITATIVA
    path('analise-template/',
         qualitativa_views.AnaliseTemplateListView.as_view(),
         name='analise_template_list'),
    # View para criação de análise atual e seleção de Referência
    path('analise-template/create/',
         qualitativa_views.AnaliseTemplateCreateView.as_view(),
         name='analise_template_create'),
    path('analise-template/<int:pk>/update',
         qualitativa_views.AnaliseTemplateUpdateView.as_view(),
         name='analise_template_update'),
    path('analise-template/<int:pk>/delete',
         qualitativa_views.AnaliseTemplateDeleteView.as_view(),
         name='analise_template_delete'),

    path('analise-template/atributo/<int:pk>/update',
         qualitativa_views.AnaliseTemplateAtributoUpdateView.as_view(),
         name='analise_template_atributo_update'),
    path('analise-template/atributo/create',
         qualitativa_views.AnaliseTemplateAtributoCreateView.as_view(),
         name='analise_template_atributo_create'),

    path('analise-template/atributo/<int:pk>/delete',
         qualitativa_views.AnaliseTemplateAtributoDeleteView.as_view(),
         name='analise_template_atributo_delete'),
)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
