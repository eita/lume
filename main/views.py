from django.contrib.auth.models import Group
from django.db import transaction
from django.db.models import CharField
from django.db.models.functions import Lower
from django.db.models.deletion import ProtectedError
from django.db.utils import IntegrityError
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.utils.translation import gettext as _
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import (
    View,
    DetailView,
    FormView,
    ListView,
    UpdateView,
    CreateView,
    DeleteView,
    RedirectView,
)
from django.views.generic.edit import FormMixin
from dal import autocomplete
from rules.contrib.views import PermissionRequiredMixin

from main.mixins import AjaxableResponseMixin
from .forms import (
    ComunidadeForm,
    OrganizacaoForm,
    OrganizacaoUsuariaForm,
    UsuariaCreationForm,
    TerritorioForm,
    UnidadeForm,
    ServicoForm,
    ProdutoForm,
    AjudaForm,
    AjudaFormSet,
    AjudaIndicadorForm,
    AjudaIndicadorFormSet,
)
from .models import (
    Comunidade,
    Organizacao,
    OrganizacaoUsuaria,
    Usuaria,
    Territorio,
    Unidade,
    Servico,
    Produto,
    Ajuda,
    AjudaIndicador,
)
from agroecossistema.models import CicloAnualReferencia
from linhadotempo.models import EventoGatilho
from agroecossistema.models import Agroecossistema
from main.utils import (
    busca_inteligente,
    merge_model_objects,
    lista_agroecossistemas_values,
)

CharField.register_lookup(Lower)


class Index(View):
    def get(self, request, *args, **kwargs):
        if request.user.is_anonymous:
            url = reverse("account_login")
        else:
            url = reverse("agroecossistema_index")
        return RedirectView.as_view(url=url)(request, *args, **kwargs)


class LumeDeleteView(DeleteView):
    def delete(self, request, *args, **kwargs):
        status = 200

        try:
            self.get_object().delete()
            payload = {"delete": "ok"}
        except (ProtectedError, IntegrityError) as e:
            payload = {"error": _("Erro ao apagar"), "message": "{0}".format(e)}
            status = 405  # Method not allowed

        return JsonResponse(payload, status=status)


class ComunidadeListView(PermissionRequiredMixin, ListView):
    model = Comunidade
    permission_required = "permissao_padrao"

    def get_queryset(self, **kwargs):
        return Comunidade.objects.all().select_related(
            "municipio", "municipio__uf", "pais"
        )
        # if self.request.user.has_perm('eh_admin_geral'):
        #     return Comunidade.objects.all().select_related('municipio', 'municipio__uf', 'pais')

        # agroecossistemas = Agroecossistema.objects.filter(organizacao_id__in=self.request.user.organizacoes.values_list('id'))
        # return Comunidade.objects.filter(agroecossistemas__in=agroecossistemas).distinct().select_related('municipio', 'municipio__uf', 'pais')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["enable_datatable_export"] = True
        context["select_options"] = list(
            Comunidade.objects.values("id", "nome").order_by("nome")
        )
        context["modulo"] = "main"
        return context

    def post(self, request, *args, **kwargs):
        primary_objects = {}
        for item_id in request.POST:
            if item_id != "csrfmiddlewaretoken" and request.POST.get(item_id) != "":
                primary_id = request.POST.get(item_id)
                if not primary_id in primary_objects:
                    primary_objects[primary_id] = []

                primary_objects[primary_id].append(Comunidade.objects.get(id=item_id))

        for primary_id, alias_objects in primary_objects.items():
            primary_object = Comunidade.objects.get(id=primary_id)
            merge_model_objects(primary_object, alias_objects)

        return HttpResponseRedirect(reverse("analise_list"))


class ComunidadeAgroecossistemasListView(PermissionRequiredMixin, ListView):
    model = Agroecossistema
    permission_required = "comunidade_ou_territorio.alterar"

    def get_queryset(self, **kwargs):
        if self.request.user.has_perm("eh_admin_geral"):
            return Agroecossistema.objects.filter(comunidade_id=self.kwargs["pk"])

        return Agroecossistema.objects.filter(
            comunidade_id=self.kwargs["pk"],
            organizacao_id__in=self.request.user.organizacoes.values_list("id"),
        )

    def get_permission_object(self):
        return Agroecossistema.objects.filter(comunidade_id=self.kwargs["pk"])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["enable_datatable_export"] = True
        context["comunidadeOuTerritorio"] = Comunidade.objects.get(pk=self.kwargs["pk"])
        context["agroecossistemas"] = self.get_queryset().select_related(
            "organizacao",
            "comunidade",
            "comunidade__municipio",
            "comunidade__municipio__uf",
            "comunidade__pais",
            "territorio",
            "autor",
        )
        context["own_agroecossistemas"] = (
            self.get_queryset()
            .filter(autor=self.request.user)
            .select_related(
                "organizacao",
                "comunidade",
                "comunidade__municipio",
                "comunidade__municipio__uf",
                "comunidade__pais",
                "territorio",
                "autor",
            )
        )
        context["titulo"] = (
            _("Agroecossistemas na comunidade ")
            + context["comunidadeOuTerritorio"].nome
        )
        context["modulo"] = "comunidade"
        context["submodulo"] = "agroecossistemas_list"
        return context


class ComunidadeCreateView(PermissionRequiredMixin, AjaxableResponseMixin, CreateView):
    model = Comunidade
    form_class = ComunidadeForm
    titulo = _("Cadastrar Comunidade")
    permission_required = "comunidade.cadastrar"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["titulo"] = self.titulo
        context["modulo"] = "comunidade"
        return context

    def get_prefix(self):
        """
        usando esse metodo como apoio para
        mandar o pais sugerido para o form.
        """
        try:
            pais_sugerido = self.request.user.agroecossistemas.last().comunidade.pais.id
        except:
            pais_sugerido = None

        return {"pais_sugerido": pais_sugerido}


class ComunidadeDetailView(DetailView):
    model = Comunidade

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["modulo"] = "comunidade"
        context["submodulo"] = "inicio"
        context["comunidadeOuTerritorio"] = self.object
        return context


class ComunidadeDetailJSONView(ComunidadeDetailView):

    def get(self, request, *args, **kwargs):
        obj = self.get_object()
        data = {
            "id": obj.id,
            "nome": obj.nome,
            "pais": {"id": obj.pais.id, "label": obj.pais.nome},
            "uf": {"id": obj.municipio.uf.id, "label": obj.municipio.uf.nome},
            "municipio": {"id": obj.municipio.id, "label": obj.municipio.nome},
        }
        return JsonResponse(data)


class ComunidadeUpdateView(PermissionRequiredMixin, AjaxableResponseMixin, UpdateView):
    model = Comunidade
    form_class = ComunidadeForm
    titulo = _("Editar Comunidade")
    # permission_required = "comunidade_ou_territorio.alterar"
    permission_required = "permissao_padrao"

    # def get_permission_object(self):
    #     return Agroecossistema.objects.filter(comunidade_id=self.kwargs["pk"])

    # Sending user object to the form, to verify which fields to display/remove (depending on group)
    def get_form_kwargs(self):
        kwargs = super(ComunidadeUpdateView, self).get_form_kwargs()
        kwargs.update(
            {
                "can_delete": self.request.user.has_perm(
                    "comunidade.apagar", self.get_object()
                )
            }
        )
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["titulo"] = self.titulo
        context["comunidadeOuTerritorio"] = self.object
        context["modulo"] = "comunidade"
        context["submodulo"] = "update"
        return context


class ComunidadeDeleteView(PermissionRequiredMixin, LumeDeleteView):
    model = Comunidade
    permission_required = "comunidade.apagar"

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ComunidadeDeleteView, self).dispatch(*args, **kwargs)


class OrganizacaoListView(ListView):
    model = Organizacao

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["titulo"] = "Organizações"
        context["modulo"] = "main"
        return context

    def get_queryset(self):
        if self.request.user.has_perm("organizacao.ver_todos"):
            return Organizacao.objects.all()
        elif self.request.user.is_anonymous:
            return Organizacao.objects.none()
        else:
            return Organizacao.objects.filter(usuarias=self.request.user)


class OrganizacaoCreateView(PermissionRequiredMixin, CreateView):
    model = Organizacao
    form_class = OrganizacaoForm
    permission_required = "organizacao.cadastrar"
    titulo = _("Nova Organização")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["titulo"] = self.titulo
        context["modulo"] = "main"
        return context


class OrganizacaoDetailView(PermissionRequiredMixin, DetailView):
    model = Organizacao
    permission_required = "organizacao.visualizar"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        usuarias_do_sistema = Usuaria.objects.exclude(
            is_superuser=True, id__in=self.object.usuarias.values_list("id")
        )
        context["usuarias_do_sistema"] = usuarias_do_sistema
        context["modulo"] = "main"
        context["org_usuarias"] = (
            self.get_object()
            .org_usuarias.select_related("usuaria")
            .order_by("usuaria__first_name")
        )
        return context


class OrganizacaoUpdateView(PermissionRequiredMixin, UpdateView):
    model = Organizacao
    form_class = OrganizacaoForm
    permission_required = "organizacao.alterar"
    titulo = _("Editar Organização")

    def get_form_kwargs(self):
        kwargs = super(OrganizacaoUpdateView, self).get_form_kwargs()
        kwargs.update(
            {
                "can_delete": self.request.user.has_perm(
                    "organizacao.apagar", self.get_object()
                )
            }
        )
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["titulo"] = self.titulo
        context["modulo"] = "main"
        context["org_usuarias"] = self.get_object().org_usuarias.order_by(
            "usuaria__username"
        )
        return context


class OrganizacaoDeleteView(PermissionRequiredMixin, LumeDeleteView):
    model = Organizacao
    permission_required = "organizacao.apagar"

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(OrganizacaoDeleteView, self).dispatch(*args, **kwargs)


class OrganizacaoUsuariaListView(ListView):
    model = OrganizacaoUsuaria


class OrganizacaoUsuariaUpdateView(PermissionRequiredMixin, UpdateView):
    model = OrganizacaoUsuaria
    form_class = OrganizacaoUsuariaForm
    permission_required = "organizacao.alterar"

    def get_permission_object(self):
        return self.get_object().organizacao

    def dispatch(self, *args, **kwargs):
        return super(OrganizacaoUsuariaUpdateView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        # get the existing object or created a new one
        data = self.request.POST

        with transaction.atomic():
            obj, created = OrganizacaoUsuaria.objects.get_or_create(
                usuaria_id=data.get("usuaria", None),
                organizacao_id=data.get("organizacao", None),
            )

            is_admin = data.get("is_admin", False)
            if is_admin == "true":
                obj.is_admin = True
                obj.save()

            return obj

    def get_success_url(self):
        # get the existing object or created a new one
        data = self.request.POST

        return reverse(
            "organizacao_detail", kwargs={"pk": int(data.get("organizacao", None))}
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["titulo"] = self.titulo
        context["modulo"] = "main"
        return context


class OrganizacaoUsuariaDeleteView(
    PermissionRequiredMixin, AjaxableResponseMixin, DeleteView
):
    model = OrganizacaoUsuaria
    organizacao = None
    permission_required = "organizacao.alterar"

    def get_permission_object(self):
        return self.get_object().organizacao

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.organizacao = self.get_object().organizacao
        self.object.delete()

        data = {
            "status": "success",
            "message": _("Apagado com sucesso!"),
            "redirect": self.get_success_url(),
        }
        return JsonResponse(data)

    def get_success_url(self):
        return reverse("organizacao_detail", kwargs={"pk": self.organizacao.pk})


class UsuariaCreateView(PermissionRequiredMixin, CreateView):
    model = Usuaria
    form_class = UsuariaCreationForm
    permission_required = "organizacao.alterar"

    def get_permission_object(self):
        return Organizacao.objects.get(pk=self.kwargs["organizacao_id"])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["organizacao"] = Organizacao.objects.get(
            pk=self.kwargs["organizacao_id"]
        )
        context["modulo"] = "main"
        return context

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.save()

        organizacao = Organizacao.objects.get(pk=self.kwargs["organizacao_id"])

        OrganizacaoUsuaria.objects.create(
            organizacao_id=organizacao.id,
            usuaria_id=obj.id,
            is_admin=form.cleaned_data["is_organization_admin"],
        )

        gr_usuarios = Group.objects.get(name="usuarios")
        gr_usuarios.user_set.add(obj)

        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse(
            "organizacao_detail", kwargs={"pk": self.kwargs["organizacao_id"]}
        )


class TerritorioListView(PermissionRequiredMixin, ListView):
    model = Territorio
    permission_required = "permissao_padrao"

    def get_queryset(self, **kwargs):
        return Territorio.objects.all()
        # if self.request.user.has_perm("eh_admin_geral"):
        #     return Territorio.objects.all()

        # agroecossistemas = Agroecossistema.objects.filter(
        #     organizacao_id__in=self.request.user.organizacoes.values_list("id")
        # )
        # return Territorio.objects.filter(agroecossistemas__in=agroecossistemas).distinct()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["enable_datatable_export"] = True
        context["select_options"] = list(
            Territorio.objects.values("id", "nome").order_by("nome")
        )
        context["modulo"] = "main"
        return context

    def post(self, request, *args, **kwargs):
        primary_objects = {}
        for item_id in request.POST:
            if item_id != "csrfmiddlewaretoken" and request.POST.get(item_id) != "":
                primary_id = request.POST.get(item_id)
                if not primary_id in primary_objects:
                    primary_objects[primary_id] = []

                primary_objects[primary_id].append(Territorio.objects.get(id=item_id))

        for primary_id, alias_objects in primary_objects.items():
            primary_object = Territorio.objects.get(id=primary_id)
            merge_model_objects(primary_object, alias_objects)

        return HttpResponseRedirect(reverse("territorio_list"))


class TerritorioAgroecossistemasListView(PermissionRequiredMixin, ListView):
    model = Agroecossistema
    permission_required = "comunidade_ou_territorio.alterar"

    def get_queryset(self, **kwargs):
        if self.request.user.has_perm("eh_admin_geral"):
            return Agroecossistema.objects.filter(territorio_id=self.kwargs["pk"])

        return Agroecossistema.objects.filter(
            territorio_id=self.kwargs["pk"],
            organizacao_id__in=self.request.user.organizacoes.values_list("id"),
        )

    def get_permission_object(self):
        return Agroecossistema.objects.filter(territorio_id=self.kwargs["pk"])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["enable_datatable_export"] = True
        context["comunidadeOuTerritorio"] = Territorio.objects.get(pk=self.kwargs["pk"])
        context["agroecossistemas"] = lista_agroecossistemas_values(self.get_queryset())
        context["own_agroecossistemas"] = lista_agroecossistemas_values(
            self.get_queryset().filter(autor=self.request.user)
        )
        context["titulo"] = (
            _("Agroecossistemas no território ")
            + context["comunidadeOuTerritorio"].nome
        )
        context["modulo"] = "territorio"
        context["submodulo"] = "agroecossistemas_list"
        return context


class TerritorioCreateView(PermissionRequiredMixin, AjaxableResponseMixin, CreateView):
    model = Territorio
    form_class = TerritorioForm
    titulo = _("Cadastrar Território")
    permission_required = "territorio.cadastrar"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["titulo"] = self.titulo
        context["modulo"] = "territorio"
        return context


class TerritorioDetailView(DetailView):
    model = Territorio

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["modulo"] = "territorio"
        context["submodulo"] = "inicio"
        context["comunidadeOuTerritorio"] = self.object
        return context


class TerritorioDetailJSONView(TerritorioDetailView):

    def get(self, request, *args, **kwargs):
        obj = self.get_object()
        data = {
            "id": obj.id,
            "nome": obj.nome,
            "descricao": obj.descricao,
        }
        return JsonResponse(data)


class TerritorioUpdateView(PermissionRequiredMixin, AjaxableResponseMixin, UpdateView):
    model = Territorio
    form_class = TerritorioForm
    titulo = _("Editar Território")
    # permission_required = "comunidade_ou_territorio.alterar"
    permission_required = "permissao_padrao"

    # def get_permission_object(self):
    #     return Agroecossistema.objects.filter(territorio_id=self.kwargs["pk"])

    # Sending user object to the form, to verify which fields to display/remove (depending on group)
    def get_form_kwargs(self):
        kwargs = super(TerritorioUpdateView, self).get_form_kwargs()
        kwargs.update(
            {
                "can_delete": self.request.user.has_perm(
                    "territorio.apagar", self.get_object()
                )
            }
        )
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["titulo"] = self.titulo
        context["comunidadeOuTerritorio"] = self.object
        context["modulo"] = "territorio"
        context["submodulo"] = "update"
        return context


class TerritorioDeleteView(
    PermissionRequiredMixin, AjaxableResponseMixin, LumeDeleteView
):
    model = Territorio
    permission_required = "territorio.apagar"

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(TerritorioDeleteView, self).dispatch(*args, **kwargs)


class UnidadeListView(PermissionRequiredMixin, ListView):
    model = Unidade
    permission_required = "permissao_padrao"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["select_options"] = list(Unidade.objects.all().order_by("nome"))
        context["modulo"] = "main"
        return context

    def post(self, request, *args, **kwargs):
        primary_objects = {}
        for item_id in request.POST:
            if item_id != "csrfmiddlewaretoken" and request.POST.get(item_id) != "":
                primary_id = request.POST.get(item_id)
                if not primary_id in primary_objects:
                    primary_objects[primary_id] = []

                primary_objects[primary_id].append(Unidade.objects.get(id=item_id))

        for primary_id, alias_objects in primary_objects.items():
            primary_object = Unidade.objects.get(id=primary_id)
            merge_model_objects(primary_object, alias_objects)

        return HttpResponseRedirect(reverse("unidade_list"))


class UnidadeCreateView(PermissionRequiredMixin, CreateView):
    model = Unidade
    form_class = UnidadeForm
    permission_required = "permissao_padrao"
    titulo = _("Novo Unidade")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["titulo"] = self.titulo
        context["modulo"] = "main"
        return context


class UnidadeDetailView(PermissionRequiredMixin, DetailView):
    model = Unidade
    permission_required = "permissao_padrao"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["modulo"] = "main"
        return context


class UnidadeUpdateView(PermissionRequiredMixin, UpdateView):
    model = Unidade
    form_class = UnidadeForm
    permission_required = "permissao_padrao"
    titulo = _("Editar Unidade")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["titulo"] = self.titulo
        context["modulo"] = "main"
        return context

    def get_form_kwargs(self):
        kwargs = super(UnidadeUpdateView, self).get_form_kwargs()
        kwargs.update({"can_delete": self.request.user.has_perm("permissao_padrao")})
        return kwargs


class UnidadeDeleteView(PermissionRequiredMixin, LumeDeleteView):
    model = Unidade
    permission_required = "permissao_padrao"

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(UnidadeDeleteView, self).dispatch(*args, **kwargs)


class ServicoListView(PermissionRequiredMixin, ListView):
    model = Servico
    permission_required = "permissao_padrao"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["modulo"] = "main"
        context["select_options"] = list(Servico.objects.all().order_by("nome"))
        context["enable_datatable_export"] = True
        return context

    def post(self, request, *args, **kwargs):
        primary_objects = {}
        for item_id in request.POST:
            if item_id != "csrfmiddlewaretoken" and request.POST.get(item_id) != "":
                primary_id = request.POST.get(item_id)
                if not primary_id in primary_objects:
                    primary_objects[primary_id] = []

                primary_objects[primary_id].append(Servico.objects.get(id=item_id))

        for primary_id, alias_objects in primary_objects.items():
            primary_object = Servico.objects.get(id=primary_id)
            merge_model_objects(primary_object, alias_objects)

        return HttpResponseRedirect(reverse("servico_list"))


class ServicoCreateView(PermissionRequiredMixin, CreateView):
    model = Servico
    form_class = ServicoForm
    permission_required = "permissao_padrao"
    titulo = _("Novo Servico")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["titulo"] = self.titulo
        context["modulo"] = "main"
        return context


class ServicoDetailView(PermissionRequiredMixin, DetailView):
    model = Servico
    permission_required = "permissao_padrao"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["modulo"] = "main"
        return context


class ServicoUpdateView(PermissionRequiredMixin, UpdateView):
    model = Servico
    form_class = ServicoForm
    permission_required = "permissao_padrao"
    titulo = _("Editar Servico")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["titulo"] = self.titulo
        context["modulo"] = "main"
        return context

    def get_form_kwargs(self):
        kwargs = super(ServicoUpdateView, self).get_form_kwargs()
        kwargs.update({"can_delete": self.request.user.has_perm("permissao_padrao")})
        return kwargs


class ServicoDeleteView(PermissionRequiredMixin, LumeDeleteView):
    model = Servico
    permission_required = "permissao_padrao"

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ServicoDeleteView, self).dispatch(*args, **kwargs)


class ProdutoListView(PermissionRequiredMixin, ListView):
    model = Produto
    permission_required = "permissao_padrao"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["modulo"] = "main"
        context["select_options"] = list(
            Produto.objects.values("id", "nome").order_by("nome")
        )
        context["enable_datatable_export"] = True
        return context

    def post(self, request, *args, **kwargs):
        primary_objects = {}
        for item_id in request.POST:
            if item_id != "csrfmiddlewaretoken" and request.POST.get(item_id) != "":
                primary_id = request.POST.get(item_id)
                if not primary_id in primary_objects:
                    primary_objects[primary_id] = []

                primary_objects[primary_id].append(Produto.objects.get(id=item_id))

        for primary_id, alias_objects in primary_objects.items():
            primary_object = Produto.objects.get(id=primary_id)
            merge_model_objects(primary_object, alias_objects)

        return HttpResponseRedirect(reverse("produto_list"))


class ProdutoCreateView(PermissionRequiredMixin, CreateView):
    model = Produto
    form_class = ProdutoForm
    permission_required = "permissao_padrao"
    titulo = _("Novo Produto")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["titulo"] = self.titulo
        context["modulo"] = "main"

        return context


class ProdutoDetailView(PermissionRequiredMixin, DetailView):
    model = Produto
    permission_required = "permissao_padrao"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["modulo"] = "main"

        return context


class ProdutoUpdateView(PermissionRequiredMixin, UpdateView):
    model = Produto
    form_class = ProdutoForm
    permission_required = "permissao_padrao"
    titulo = _("Editar Produto")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["titulo"] = self.titulo
        context["modulo"] = "main"

        return context

    def get_form_kwargs(self):
        kwargs = super(ProdutoUpdateView, self).get_form_kwargs()
        kwargs.update({"can_delete": self.request.user.has_perm("permissao_padrao")})
        return kwargs


class ProdutoDeleteView(PermissionRequiredMixin, LumeDeleteView):
    model = Produto
    permission_required = "permissao_padrao"

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ProdutoDeleteView, self).dispatch(*args, **kwargs)


class AjudaView(PermissionRequiredMixin, FormView):
    model = Ajuda
    permission_required = "eh_admin_geral"
    form_class = AjudaForm
    template_name = "main/ajuda.html"

    def get_permission_object(self):
        return self.request.user

    def post(self, request, *args, **kwargs):
        formset = AjudaFormSet(request.POST)
        if formset.is_valid():
            return self.form_valid(formset)
        else:
            return self.form_invalid(formset)

    def form_invalid(self, formset):
        return self.render_to_response(self.get_context_data(form=form))

    def form_valid(self, formset):
        instances = []
        hasErrors = False
        for form in formset:
            if form.is_valid():
                if form.cleaned_data["DELETE"] and form.cleaned_data["id"] is not None:
                    form.instance.delete()
                else:
                    instances.append(form.save())
            else:
                hasErrors = True

        if hasErrors:
            return self.render_to_response(self.get_context_data(form=form))

        return HttpResponseRedirect(reverse("ajuda"))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if self.request.GET.get("force_edit"):
            context["force_edit"] = True

        context["titulo"] = "Ver/editar Ajudas do sistema"
        context["modulo"] = "main"
        context["submodulo"] = "ajuda"

        if self.request.POST:
            context["formset"] = AjudaFormSet(self.request.POST)
        else:
            context["formset"] = AjudaFormSet()

        return context


class AjudaIndicadorView(PermissionRequiredMixin, FormView):
    model = AjudaIndicador
    permission_required = "eh_admin_geral"
    form_class = AjudaIndicadorForm
    template_name = "main/ajuda_indicador.html"

    def get_permission_object(self):
        return self.request.user

    def post(self, request, *args, **kwargs):
        formset = AjudaIndicadorFormSet(request.POST)
        if formset.is_valid():
            return self.form_valid(formset)
        else:
            return self.form_invalid(formset)

    def form_invalid(self, formset):
        return self.render_to_response(self.get_context_data(form=form))

    def form_valid(self, formset):
        instances = []
        hasErrors = False
        for form in formset:
            if form.is_valid():
                if form.cleaned_data["DELETE"] and form.cleaned_data["id"] is not None:
                    form.instance.delete()
                else:
                    instances.append(form.save())
            else:
                hasErrors = True

        if hasErrors:
            return self.render_to_response(self.get_context_data(form=form))

        return HttpResponseRedirect(reverse("ajuda_indicador"))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if self.request.GET.get("force_edit"):
            context["force_edit"] = True

        context["titulo"] = "Ver/editar Ajudas dos indicadores Lume"
        context["modulo"] = "main"
        context["submodulo"] = "ajuda_indicador"

        if self.request.POST:
            context["formset"] = AjudaIndicadorFormSet(self.request.POST)
        else:
            context["formset"] = AjudaIndicadorFormSet()

        return context


class GeneralFormSetView(PermissionRequiredMixin, FormView):
    model = AjudaIndicador
    permission_required = "eh_admin_geral"
    template_name = "general_formset.html"

    def get_permission_object(self):
        return self.request.user

    def post(self, request, *args, **kwargs):
        formset = AjudaIndicadorFormSet(request.POST)
        if formset.is_valid():
            return self.form_valid(formset)
        else:
            return self.form_invalid(formset)

    def form_invalid(self, formset):
        return self.render_to_response(self.get_context_data(form=form))

    def form_valid(self, formset):
        instances = []
        hasErrors = False
        for form in formset:
            if form.is_valid():
                if form.cleaned_data["DELETE"] and form.cleaned_data["id"] is not None:
                    form.instance.delete()
                else:
                    instances.append(form.save())
            else:
                hasErrors = True

        if hasErrors:
            return self.render_to_response(self.get_context_data(form=form))

        return HttpResponseRedirect(reverse("ajuda_indicador"))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if self.request.GET.get("force_edit"):
            context["force_edit"] = True

        context["titulo"] = "Ver/editar Ajudas dos indicadores Lume"
        context["modulo"] = "main"
        context["submodulo"] = "ajuda_indicador"

        if self.request.POST:
            context["formset"] = AjudaIndicadorFormSet(self.request.POST)
        else:
            context["formset"] = AjudaIndicadorFormSet()

        return context


class UnidadeAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        return busca_inteligente(Unidade.objects.all(), self.q)


class ProdutoAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        return busca_inteligente(Produto.objects.all(), self.q)


class ServicoAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        return busca_inteligente(Servico.objects.all(), self.q)


class OrganizacaoAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        return busca_inteligente(Organizacao.objects.all(), self.q)


class CicloAnualReferenciaAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # TODO considerar user permission
        qs = CicloAnualReferencia.objects.all()
        organizacao = self.forwarded.get("organizacao", None)

        if organizacao:
            qs = qs.filter(agroecossistema__organizacao=organizacao)

        if self.q:
            qs = qs.filter(ano__istartswith=self.q)

        return qs


class EventoGatilhoAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # TODO considerar user permission
        qs = EventoGatilho.objects.all()
        organizacao = self.forwarded.get("organizacao", None)

        if organizacao:
            qs = qs.filter(organizacao=organizacao)

        return busca_inteligente(qs, self.q, "titulo")
