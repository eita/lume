import re
import json
from django import template
from django.conf import settings
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.template.defaultfilters import floatformat
from django.template.loader import get_template
from django.utils.safestring import mark_safe, SafeString
from django.apps import apps
from django.utils.translation import gettext as _

from agroecossistema.models import Agroecossistema
from main.utils import (
    lista_agroecossistemas_values,
    lista_comunidades_values,
    lista_territorios_values,
    lista_analisesagregadas_values,
    parse_args_kwargs_and_as_var,
    get_args_and_kwargs,
)
from economica.models import Analise
from analiseagregada.models import Analiseagregada
from relatorios.models import TotaisAnaliseEconomica
from main.models import Ajuda, Comunidade, Territorio

from economica.utils import LDecimal

register = template.Library()


@register.simple_tag
def vstatic(path):
    url = static(path)
    if settings.STATIC_VERSION:
        url += "?v=" + settings.STATIC_VERSION
    return url


@register.simple_tag(takes_context=True)
def lista_agroecossistemas(context, meus=False):
    user = context["request"].user

    if user.is_anonymous:
        return None

    if meus:
        return Agroecossistema.objects.filter(autor=user)

    if user.has_perm("organizacao.ver_todos"):
        return Agroecossistema.objects.all()
    elif user.is_anonymous:
        return Agroecossistema.objects.none()
    else:
        return Agroecossistema.objects.filter(organizacao__usuarias=user)

    return Agroecossistema.objects.all()


@register.simple_tag
def lista_agroecossistemas_publicos():
    return lista_agroecossistemas_values(
        Agroecossistema.objects.filter(visualizacao_publica=True)
    )


@register.simple_tag(takes_context=True)
def lista_comunidades(context):
    usuaria = context["request"].user

    if usuaria.is_anonymous:
        return None

    return Comunidade.objects.all().select_related("municipio", "municipio__uf", "pais")
    # if usuaria.has_perm('eh_admin_geral'):
    #     return Comunidade.objects.all()

    # agroecossistemas = Agroecossistema.objects.filter(organizacao_id__in=usuaria.organizacoes.values_list('id'))
    # return Comunidade.objects.filter(agroecossistemas__in=agroecossistemas).distinct()


@register.simple_tag(takes_context=True)
def lista_territorios(context):
    usuaria = context["request"].user

    if usuaria.is_anonymous:
        return None

    return Territorio.objects.all()

    # if usuaria.has_perm("eh_admin_geral"):
    #     return Territorio.objects.all()

    # agroecossistemas = Agroecossistema.objects.filter(organizacao_id__in=usuaria.organizacoes.values_list("id"))
    # return Territorio.objects.filter(agroecossistemas__in=agroecossistemas).distinct()


@register.simple_tag(takes_context=True)
def lista_analisesagregadas(context, meus=False):
    user = context["request"].user

    if user.is_anonymous:
        return None

    if meus:
        return Analiseagregada.objects.filter(autor=user)

    if user.has_perm("organizacao.ver_todos"):
        return Analiseagregada.objects.all()
    elif user.is_anonymous:
        return Analiseagregada.objects.filter(is_analise_modelo=True)
    else:
        return Analiseagregada.objects.filter(organizacao__usuarias=user)

    return Analiseagregada.objects.all()


@register.filter(name="currency")
def currency(value, include_symbol=False):
    str = ""
    if isinstance(value, LDecimal) and value.messages:
        res = currency(value.value)
        return mark_safe(value.get_span_tag(res, "teste"))

    if not value:
        str = "-"
    else:
        if include_symbol:
            str = settings.CURRENCY_SYMBOL + " "
        str += floatformat(value, 2)
    return str


@register.filter(name="percent")
def percent(value, decimal_places=0):
    if not value:
        return ""
    return floatformat(value * 100, decimal_places) + "%"


@register.filter(name="number")
def number(value, decimal_places=0):
    str = "" if not value else floatformat(value, decimal_places)
    return str


@register.filter(name="concat")
def concat(value, concat_string):
    return str(value) + str(concat_string)


@register.filter
def get_list_item(list, i):
    try:
        return list[i]
    except:
        return None


@register.filter
def get_obj_attr(obj, attr):
    try:
        return getattr(obj, attr)
    except:
        return None


@register.filter
def get_dict_attr(dict, attr):
    try:
        return dict.get(attr)
    except:
        return None


@register.filter
def get_form_field(form_fields, field_name):
    for field in form_fields:
        if field.name == field_name:
            return field

    return None


@register.filter
def trim(text):
    return txt.strip()


@register.inclusion_tag("main/inclusiontags/tabela_cabecalho.html", takes_context=True)
def tabela_cabecalho(
    context,
    colspan,
    tableTitle,
    targetId,
    editavel=False,
    positionAbsolute=False,
    hasSubsistemaSelector=False,
    ajudaDaTelaBtn=True,
):
    return {
        "colspan": colspan,
        "tableTitle": tableTitle,
        "targetId": targetId,
        "editavel": editavel,
        "positionAbsolute": positionAbsolute,
        "hasSubsistemaSelector": hasSubsistemaSelector,
        "parentContext": context,
        "ajudaDaTelaBtn": ajudaDaTelaBtn,
        "pode_alterar_agroecossistema": (
            False
            if not "pode_alterar_agroecossistema" in context
            else context["pode_alterar_agroecossistema"]
        ),
    }


@register.inclusion_tag("main/inclusiontags/box_de_ajuda.html", takes_context=True)
def box_de_ajuda(context, extra_class=""):
    if extra_class:
        context["extra_class"] = extra_class
    return context


@register.inclusion_tag(
    "main/inclusiontags/subsistema_selector.html", takes_context=True
)
def subsistema_selector(context):
    cont = context["parentContext"]
    submodulo_tipo = ""
    if "submodulo_tipo" in cont:
        submodulo_tipo = cont["submodulo_tipo"]
    return {
        "analise_id": cont["car"].analise_economica.id,
        "submodulo": cont["submodulo"],
        "submodulo_tipo": submodulo_tipo,
        "subsistema_id": cont["subsistema_id"],
        "subsistemas": cont["car"].analise_economica.subsistemas.all(),
    }


def generic_content_tabs(context, model):
    if model == "agroecossistema":
        tab_items = [
            {
                "id": "meus",
                "title": _("Criados por mim"),
                "items": lista_agroecossistemas_values(
                    lista_agroecossistemas(context, "meus")
                ),
                "subs": [{"field": "criacao", "label": "Criação", "isDate": True}],
            },
            {
                "id": "todos",
                "title": _("Todos"),
                "items": lista_agroecossistemas_values(lista_agroecossistemas(context)),
                "subs": [
                    {"field": "autor__first_name", "label": _("Por")},
                    {"field": "organizacao__nome", "label": ""},
                ],
            },
        ]

        return {
            "id": model,
            "title": _("Agroecossistemas"),
            "title_create": _("Novo agroecossistema"),
            "create_url": "agroecossistema_create",
            "list_url": "agroecossistema_list",
            "single_url": "agroecossistema_detail",
            "tab_items": tab_items,
        }

    if model == "comunidade":
        tab_items = [
            {
                "id": "meus",
                "title": _("Comunidades"),
                "items": lista_comunidades_values(lista_comunidades(context)),
                "subs": [
                    {"field": "criacao", "label": "Criação", "isDate": True},
                    {"field": "municipio__nome", "label": _("Município")},
                    {"field": "municipio__uf__nome", "label": _("Estado")},
                    {"field": "pais__nome", "label": _("País")},
                ],
            }
        ]

        return {
            "id": model,
            "title": _("Comunidades"),
            "title_create": _("Nova comunidade"),
            "create_url": "comunidade_create",
            "list_url": "comunidade_list",
            "single_url": "comunidade_detail",
            "tab_items": tab_items,
        }

    if model == "territorio":
        tab_items = [
            {
                "id": "meus",
                "title": _("Territórios"),
                "items": lista_territorios_values(lista_territorios(context)),
                "subs": [
                    {"field": "criacao", "label": "Criação", "isDate": True},
                    {"field": "descricao", "label": _("Descrição")},
                ],
            }
        ]

        return {
            "id": model,
            "title": _("Territórios"),
            "title_create": _("Novo território"),
            "create_url": "territorio_create",
            "list_url": "territorio_list",
            "single_url": "territorio_detail",
            "tab_items": tab_items,
        }

    if model == "analiseagregada":
        tab_items = [
            {
                "id": "meus",
                "title": _("Criadas por mim"),
                "items": lista_analisesagregadas_values(
                    lista_analisesagregadas(context, "meus")
                ),
                "subs": [
                    {"field": "ano", "label": _("Ano")},
                    {"field": "evento_gatilho__titulo", "label": _("Evento gatilho")},
                ],
            },
            {
                "id": "todas",
                "title": _("Todas"),
                "items": lista_analisesagregadas_values(
                    lista_analisesagregadas(context)
                ),
                "subs": [
                    {"field": "autor__first_name", "label": _("Por")},
                    {"field": "ano", "label": _("Ano")},
                    {"field": "evento_gatilho__titulo", "label": _("Evento gatilho")},
                ],
            },
        ]
        return {
            "id": model,
            "title": _("Análises agregadas"),
            "title_create": _("Criar análise"),
            "create_url": "analiseagregada:form",
            "list_url": "analiseagregada:list",
            "single_url": "analiseagregada:detail",
            "tab_items": tab_items,
        }

    return None


@register.inclusion_tag("main/inclusiontags/nav_dropdown_tabs.html", takes_context=True)
def nav_dropdown_tabs(context, model):
    return generic_content_tabs(context, model)


@register.inclusion_tag("main/inclusiontags/content_index_tabs.html", takes_context=True)
def content_index_tabs(context, model):
    return generic_content_tabs(context, model)


@register.simple_tag(takes_context=True)
def tabela_class(
    context,
    tableId,
    extra="",
    editavel=False,
    sticky=True,
    fitToPage=False,
    portrait=False,
    searchable=False,
    orderable=False,
    searchIsTitle=False,
    editDisabled="0",
):
    classes = "datatable table w-100 no-paging " + tableId
    if searchIsTitle:
        classes += " search-is-title"
    if not searchable:
        classes += " no-searching"
    if not orderable:
        classes += " no-ordering"
    if editavel:
        classes += " tabelaEditavel modoEdicao"
    if editDisabled == "1":
        classes += " edit-disabled"
    if extra:
        classes += " " + extra

    if portrait:
        classes += " add-export-buttons-collapsed"
    else:
        classes += " add-export-buttons-collapsed-landscape"

    if fitToPage:
        classes += " fit-to-page"
    elif sticky:
        classes += " fixed-headers"

    if "form" in context and context["form"].errors:
        classes += " has-errors"

    html = mark_safe('class="' + classes + '"')

    return html


@register.tag(name="tabela2")
def do_tabela2(parser, token):
    PATTERN = re.compile(r"""((?:[^ "']|"[^"]*"|'[^']*')+)""")
    bits = PATTERN.split(token.contents)[1::2]
    if len(bits) < 1:
        raise TemplateSyntaxError("'%s' takes at least one argument" % bits[0])

    if len(bits) > 1:
        args, kwargs, as_var = parse_args_kwargs_and_as_var(parser, bits[1:])

    nodelist = parser.parse("endtabela2")
    parser.delete_first_token()
    kwargs["nodelist"] = nodelist
    return TabelaNode(args, kwargs, as_var)


class TabelaNode(template.Node):
    def __init__(self, args, kwargs, as_var):
        self.nodelist = kwargs.pop("nodelist")
        self.args = args
        self.kwargs = kwargs
        self.as_var = as_var

    def render(self, context):
        args, kwargs = get_args_and_kwargs(self.args, self.kwargs, context)
        content = self.nodelist.render(context)
        table_id = kwargs.get("tableId", "tabela")

        classes = "datatable table w-100 no-paging " + table_id
        if kwargs.get("searchIsTitle", False):
            classes += " search-is-title"
        if not kwargs.get("searchable", False):
            classes += " no-searching"
        if not kwargs.get("orderable", False):
            classes += " no-ordering"
        if kwargs.get("noSortOnStart", False):
            classes += " no-sort-on-start"
        if kwargs.get("editavel", False):
            classes += " tabelaEditavel modoEdicao"
            if kwargs.get("keyTable", True):
                classes += " keyTable"
        if kwargs.get("forceEdit", False):
            classes += " force_edit"
        if kwargs.get("extra_class", None):
            classes += " " + extra_class
        if kwargs.get("portrait", False):
            classes += " add-export-buttons-collapsed"
        else:
            classes += " add-export-buttons-collapsed-landscape"
        if kwargs.get("fitToPage", False):
            classes += " fit-to-page"
        elif kwargs.get("sticky", True):
            classes += " fixed-headers"
        if kwargs.get("hasMarkItUp", False):
            classes += " has-markitup"

        if "form" in context and context["form"].errors:
            classes += " has-errors"

        tabela_template = get_template("main/inclusiontags/tabela2.html")
        template_context = {
            "tableClasses": classes,
            "colspan": kwargs.get("colspan", 0),
            "tableTitle": kwargs.get("tableTitle", "Tabela"),
            "tableId": table_id,
            "editavel": kwargs.get("editavel", False),
            "positionAbsolute": kwargs.get("positionAbsolute", False),
            "hasSubsistemaSelector": kwargs.get("hasSubsistemaSelector", False),
            "parentContext": context,
            "ajudaDaTelaBtn": kwargs.get("ajudaDaTelaBtn", True),
            "pode_alterar_agroecossistema": (
                False
                if not "pode_alterar_agroecossistema" in context
                else context["pode_alterar_agroecossistema"]
            ),
            "content": content,
        }
        return tabela_template.render(template_context)


@register.inclusion_tag("main/inclusiontags/tabela.html", takes_context=True)
def tabela(
    context,
    colspan,
    tableTitle,
    tableId,
    extra_class="",
    editavel=False,
    keyTable=True,
    sticky=True,
    fitToPage=False,
    portrait=False,
    searchable=False,
    orderable=False,
    searchIsTitle=False,
    positionAbsolute=False,
    hasSubsistemaSelector=False,
    ajudaDaTelaBtn=True,
    forceEdit=False,
):
    classes = "datatable table w-100 no-paging " + tableId
    if searchIsTitle:
        classes += " search-is-title"
    if not searchable:
        classes += " no-searching"
    if not orderable:
        classes += " no-ordering"
    if editavel:
        classes += " tabelaEditavel modoEdicao"
        if keyTable:
            classes += " keyTable"
    if forceEdit:
        classes += " force_edit"
    if extra_class:
        classes += " " + extra_class

    if portrait:
        classes += " add-export-buttons-collapsed"
    else:
        classes += " add-export-buttons-collapsed-landscape"

    if fitToPage:
        classes += " fit-to-page"
    elif sticky:
        classes += " fixed-headers"

    if "form" in context and context["form"].errors:
        classes += " has-errors"

    return {
        "tableClasses": classes,
        "colspan": colspan,
        "tableTitle": tableTitle,
        "tableId": tableId,
        "editavel": editavel,
        "positionAbsolute": positionAbsolute,
        "hasSubsistemaSelector": hasSubsistemaSelector,
        "parentContext": context,
        "ajudaDaTelaBtn": ajudaDaTelaBtn,
        "pode_alterar_agroecossistema": (
            False
            if not "pode_alterar_agroecossistema" in context
            else context["pode_alterar_agroecossistema"]
        ),
    }


@register.inclusion_tag("qualitativa/inclusiontags/select_qualificacao.html")
def select_qualificacao(parametro_id, analise, qualificacao, dataTipo):
    return {
        "parametro_id": parametro_id,
        "analise": analise,
        "qualificacao": qualificacao,
        "dataTipo": dataTipo,
        "ano": analise.ano_referencia if dataTipo == "referencia" else analise.ano,
    }


@register.simple_tag(takes_context=True)
def is_agroecossistema_menu(context):
    if not "modulo" in context:
        return False
    modulo = context["modulo"]
    submodulo = "" if not "submodulo" in context else context["submodulo"]
    return modulo == "main" or modulo == "agroecossistema" or submodulo == "car"


@register.filter
def replace(value, arg):
    """
    Replacing filter
    Use `{{ "aaa"|replace:"a|b" }}`
    """
    if len(arg.split("|")) != 2:
        return value

    what, to = arg.split("|")
    return value.replace(what, to)


@register.simple_tag(takes_context=True)
def get_ajuda(context):
    if not "modulo" in context:
        return ""

    modulo = context["modulo"]
    submodulo = "" if not "submodulo" in context else context["submodulo"]

    try:
        return Ajuda.objects.get(modulo=modulo, submodulo=submodulo)
    except:
        return ""


@register.simple_tag(takes_context=True)
def build_ajuda_storage_key(context):
    if not "modulo" in context:
        return ""

    key = context["modulo"]

    if "submodulo" in context:
        key += "_" + context["submodulo"]

    if "submodulo_tipo" in context:
        key += "_" + context["submodulo_tipo"]

    return key


@register.simple_tag()
def get_update_submodulo_url(analise_id, subsistema_id, submodulo, submodulo_tipo):
    analise = Analise.objects.get(pk=analise_id)

    subsistema_id = subsistema_id if submodulo[:10] == "subsistema" else ""
    url = ""

    for target in Analise.ABAS:
        if target["submodulo"] == submodulo:
            if "sub_abas_automaticas" in target:
                url = analise.get_update_submodulo_url(
                    submodulo, submodulo_tipo, subsistema_id
                )
            elif "sub_abas" not in target:
                url = analise.get_update_submodulo_url(submodulo, "", subsistema_id)

        if "sub_abas" in target:
            for sub_aba in target["sub_abas"]:
                if sub_aba["target_id"] == submodulo:
                    tipo = "" if not "tipo" in sub_aba else sub_aba["tipo"]
                    url = analise.get_update_submodulo_url(
                        submodulo, tipo, subsistema_id
                    )
                    break

        if url != "":
            break

    return url


@register.simple_tag()
def get_terras(car, tipo):
    return {"qtde": car.get_qtde(tipo), "area": car.get_area(tipo)}


@register.simple_tag()
def get_terras_list(car, tipo):
    terras_list = []
    for tipo_sigla, tipo_nome in car.terras.first().TIPOS_TERRA:
        qtde = car.get_qtde(tipo_sigla)
        if qtde:
            terras_list.append(
                {"tipo": tipo_nome, "qtde": qtde, "area": car.get_area(tipo_sigla)}
            )
    return terras_list


@register.simple_tag()
def td_qtde(field, ref="quantidade"):
    return "" if ref not in field else " td_qtde"


@register.simple_tag(takes_context=True)
def ajudaIndicador(context, slug, estilo="indicador"):
    if "totais" not in context or not context["totais"]:
        if estilo != 'painel_agroecossistema':
            return slug
        else:
            modelo = TotaisAnaliseEconomica()
            indicador = modelo.get_ajuda_indicador(slug)
    else:
        indicador = context["totais"].get_ajuda_indicador(slug)

    if not indicador:
        return slug

    tag_class = "indicador"
    if estilo == "indicador":
        nome = indicador["slug"]
    if estilo == "nome":
        nome = indicador["nome"]
    if estilo == "ambos":
        nome = f"{indicador['nome']} ({indicador['slug']})"
    if estilo == "painel_agroecossistema":
        nome = "&nbsp;"
        tag_class = "indicador icon-panel-info indicador-painel-agroecossistema"

    return mark_safe(
        f"<span class='{tag_class}' data-id='{indicador['id']}'>{nome}</span>"
    )


@register.simple_tag(takes_context=True)
def ajudaIndicadorPainelAgroecossistema(context, slug):
    return ajudaIndicador(context, slug, "painel_agroecossistema")


@register.simple_tag(takes_context=True)
def ajudaIndicadorNome(context, slug):
    return ajudaIndicador(context, slug, "nome")


@register.simple_tag(takes_context=True)
def ajudaIndicadorAmbos(context, slug):
    return ajudaIndicador(context, slug, "ambos")


@register.filter(is_safe=True)
def formula(indicador):
    return indicador.getFormula()


@register.filter(is_safe=True)
def sum(a, b):
    return a + b


@register.inclusion_tag("main/inclusiontags/glossario.html")
def glossario(ajuda_indicadores):
    return {"ajuda_indicadores": ajuda_indicadores}


@register.inclusion_tag("main/inclusiontags/select_simples.html")
def select_simples(id, options_var="select_options", options=None):
    return {"options_var": options_var, "options": options, "id": id, "name": str(id)}


@register.filter("escapedict")
def escapedict(data):
    if not isinstance(data, dict):
        return data
    for key in data:
        if isinstance(data[key], int) and not isinstance(data[key], bool):
            data[key] = int(SafeString(data[key]))
        else:
            data[key] = SafeString(data[key])
    return json.dumps(data)
