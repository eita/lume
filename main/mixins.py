from django.http import JsonResponse
from django.utils.translation import gettext as _


class AjaxableResponseMixin:
    """
    Mixin to add AJAX support to a form.
    Must be used with an object-based FormView (e.g. CreateView)
    """

    def form_invalid(self, form):
        response = super().form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse({'status': 'error', 'message': form.errors.as_text()}, status=400)
        else:
            return response

    def form_valid(self, form):
        # We make sure to call the parent's form_valid() method because
        # it might do some processing (in the case of CreateView, it will
        # call form.save() for example).
        response = super().form_valid(form)

        try:
            msg = self.success_message
        except AttributeError:
            msg = _('%(objeto)s gravado com sucesso!') % {
                'objeto': self.object.__class__.__name__}

        if self.request.is_ajax():
            data = {
                'status': 'success',
                'message': msg,
                'pk': self.object.id,
                'str': str(self.object)
            }
            return JsonResponse(data)
        else:
            return response
