from django.core.management.base import BaseCommand, CommandError
from qualitativa.models import Analise


class Command(BaseCommand):

    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS("Migrando dados de parâmetros inativos para Análise Qualitativa..."))

        for analise in Analise.objects.all():
            parametros_inativos_origem = analise.agroecossistema.parametros_inativos
            analise.parametros_inativos = parametros_inativos_origem
            analise.save()

        self.stdout.write(self.style.SUCCESS("\nParâmetros inativos migrados com sucesso!"))
