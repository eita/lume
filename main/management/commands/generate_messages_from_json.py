from django.core.management.base import BaseCommand, CommandError
from django.utils.text import slugify
from qualitativa.models import Parametro


class Command(BaseCommand):

    def handle(self, *args, **options):
        self.set_qualitativa_descricao()

        self.stdout.write(self.style.SUCCESS("\nMensagens gravadas na base a partir do JSON!\n"))

    def set_qualitativa_descricao(self):
        parametros = Parametro.objects.all()
        for param in parametros:
            atributo = slugify(param.atributo)
            parametro = slugify(param.nome)
            param.descricao=Parametro.HELPERS[atributo][parametro]
            param.save()

