from django.core.management.base import BaseCommand, CommandError
from main import models as main
from django.db.models.functions import Trim
from django.apps import apps
from agroecossistema import models as agroecossistema
from economica import models as economica
from main.utils import fazer_buscavel, merge_model_objects


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument(
            '--primary',
        )
        parser.add_argument(
            '--alias',
            nargs='+'
        )
        parser.add_argument('model', type=str)

    def handle(self, *args, **options):
        try:
            model = apps.get_model('main', options['model'])
        except Exception as e:
            self.stdout.write(self.style.ERROR("Seu modelo não existe! " + str(e)))
            return

        model.objects.update(nome=Trim('nome'))

        if options['primary'] and options['alias']:
            try:
                primary = model.objects.get(nome=options['primary'])
                alias_objects = []
                for obj in options['alias']:
                    alias_objects.append(model.objects.get(nome=obj))

                merge_model_objects(primary, alias_objects)
                return
            except Exception as e:
                self.stdout.write(self.style.ERROR("Não há estes itens no modelo. " + str(e)))
                return
        elif options['primary'] or options['alias']:
            self.stdout.write(self.style.ERROR("Ou tudo, ou nada! Erro!"))
            return


        # Este é o caso de navegar pelo model e identificar todos os que são idênticos, com diferença só de acentos e maiúsculas:
        self.stdout.write(self.style.SUCCESS("Script de eliminação automática de duplicatas do model " + options['model'] + "."))
        duplicatas_removidas = 0
        for primary_object in model.objects.all().order_by('id'):
            buscavel = fazer_buscavel(primary_object.nome)
            alias_objects_qs = model.objects.filter(nome__unaccent__lower=buscavel, id__gt=primary_object.id).order_by('id')
            # self.stdout.write(self.style.SUCCESS(buscavel))
            alias_objects = []
            for alias_object in alias_objects_qs:
                alias_objects.append(alias_object)

            if len(alias_objects) > 0:
                self.stdout.write(self.style.ERROR("Encontrei " + str(len(alias_objects)) + " duplicatas para o termo '" + primary_object.nome + "'..."))
                merge_model_objects(primary_object, alias_objects)
                self.stdout.write(self.style.SUCCESS("Duplicatas eliminadas!\n"))
                duplicatas_removidas += len(alias_objects)

        if duplicatas_removidas > 0:
            self.stdout.write(self.style.SUCCESS("Missão cumprida. No model " + options['model'] + " foram eliminadas " + str(duplicatas_removidas) + " duplicatas"))
        else:
            self.stdout.write(self.style.SUCCESS("Missão cumprida. Não foram encontradas duplicatas no model " + options['model'] + "."))
