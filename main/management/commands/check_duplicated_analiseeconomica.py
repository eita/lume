from django.core.management.base import BaseCommand, CommandError
from agroecossistema.models import CicloAnualReferencia
from main.utils import fazer_buscavel
from datetime import datetime


class Command(BaseCommand):

    # def add_arguments(self, parser):
        # parser.add_argument(
        #     '--primary',
        # )
        # parser.add_argument(
        #     '--alias',
        #     nargs='+'
        # )
        # parser.add_argument('model', type=str)

    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS("Verificando CARs repetidos..."))
        cars = CicloAnualReferencia.objects.filter(sim_slug__isnull=True).order_by('agroecossistema_id', 'criacao')

        nsga = ''
        anos = []
        dados = []
        repetidos = False
        for car in cars:
            if car.agroecossistema.nsga != nsga:
                if repetidos:
                    self.stdout.write(self.style.SUCCESS("\nAgroecossistema: " + nsga))
                    for dado in dados:
                        self.stdout.write(self.style.ERROR("Ano: " + str(dado['ano']) + ". Criado em " + dado['criacao']))
                nsga = car.agroecossistema.nsga
                anos = []
                dados = []
                repetidos = False

            if car.ano in anos:
                repetidos = True

            anos.append(car.ano)
            dados.append({
                'ano': car.ano,
                'criacao': car.criacao.strftime("%d/%m/%Y")
            })

        self.stdout.write(self.style.SUCCESS("\nFIM!"))
