from django.core.management.base import BaseCommand, CommandError
from django.utils.text import slugify
from qualitativa.models import Parametro
from linhadotempo.models import Dimensao
from linhaterritorial.models import DimensaoTerritorial
from main.models import AjudaIndicador


class Command(BaseCommand):

    def handle(self, *args, **options):
        qtde = self.generate_qualitativa_helpers()
        self.stdout.write(self.style.SUCCESS(f"\nMódulo Qualitativa: {qtde} mensagens geradas a partir da base para tradução!\n"))

        qtde = self.generate_linhadotempo_helpers()
        self.stdout.write(self.style.SUCCESS(f"\nMódulo Linha do Tempo: {qtde} mensagens geradas a partir da base para tradução!\n"))

        qtde = self.generate_ajuda_indicador_helpers()
        self.stdout.write(self.style.SUCCESS(f"\nAjudas de indicadores: {qtde} mensagens geradas a partir da base para tradução!\n"))

        qtde = self.generate_linhaterritorial_helpers()
        self.stdout.write(self.style.SUCCESS(f"\nMódulo Linha Territorial: {qtde} mensagens geradas a partir da base para tradução!\n"))

        self.stdout.write(self.style.SUCCESS("\nFinalizada a geração de strings traduzidas! Estão disponíveis!\n"))


    def generate_linhadotempo_helpers(self):
        nomeHelper = 'linhadotempo_helpers'
        dimensoes = Dimensao.objects.select_related('tipo_dimensao').all()
        ret = self.generate_add_simple_helpers(nomeHelper, dimensoes, 'descricao', 'tipo_dimensao')

        file = r'./linhadotempo/messages_from_db_generated.py'
        self.save_messages(file, ret['code'])
        return ret['counter']

    def generate_ajuda_indicador_helpers(self):
        nomeHelper = 'ajuda_indicador'
        ajuda_indicadores = AjudaIndicador.objects.all()
        code = self.get_import_commons()
        code += f"{nomeHelper}_helpers = {'{}'}\n\n"
        for item in ajuda_indicadores:
            key = slugify(str(item.nome))
            nome = item.nome
            slug = item.slug
            ajuda = item.ajuda
            ajuda_curta = item.ajuda_curta
            dict_value = f"{{'nome': _('''{nome}'''), 'slug': _('''{slug}'''), 'ajuda': _('''{ajuda}'''), 'ajuda_curta': _('''{ajuda_curta}'''), }}"
            code += f"{nomeHelper}_helpers['{key}'] = {dict_value}\n\n"

        file = r'./main/messages_from_db_generated_' + nomeHelper + '.py'
        self.save_messages(file, code)
        return ajuda_indicadores.count()

    def generate_qualitativa_helpers(self):
        parametros = Parametro.objects.all().order_by('atributo__nome', 'nome')
        code = self.get_import_commons()
        code += "qualitativa_helpers = {}\n\n"
        for parametro in parametros:
            atributo = parametro.atributo
            attr = slugify(str(atributo))
            param = slugify(str(parametro))
            descricao = parametro.descricao
            dict_value = f"{{'atributo': _('''{str(atributo)}'''), 'parametro': _('''{str(parametro)}'''), 'descricao': _('''{descricao}'''), 'subsection': _('''{parametro.subsection}'''), }}"
            code += f"qualitativa_helpers = add_helper(qualitativa_helpers, '{attr}', '{param}', {dict_value})\n\n"

        file = r'./qualitativa/messages_from_db_generated.py'
        self.save_messages(file, code)
        return parametros.count()

    def generate_linhaterritorial_helpers(self):
        nomeHelper = 'linhaterritorial_helpers'
        dimensoes = DimensaoTerritorial.objects.select_related('tipo_dimensao').all()
        ret = self.generate_add_simple_helpers(nomeHelper, dimensoes, 'descricao', 'tipo_dimensao', 'descricao')

        file = r'./linhaterritorial/messages_from_db_generated.py'
        self.save_messages(file, ret['code'])
        return ret['counter']

    def save_messages(self, file, content):
        pyfile = open(file,'w')
        pyfile.write(content)
        pyfile.close()

    def get_import_commons(self):
        code = "from main.messages_from_db import add_helper, add_simple_helper\n"
        code += "from django.utils.translation import gettext_lazy as _\n\n"
        return code

    def generate_add_simple_helpers(self, nomeHelper, items, ajudaField, parentField = None, parentAjudaField = None):
        parents = []
        counter = 0
        code = self.get_import_commons()
        code += f"{nomeHelper} = {'{}'}\n\n"
        for item in items:
            slug = slugify(item.nome)
            nomeI18n = f"_(\"{item.nome}\")"
            ajudaI18n = f"_(\"\"\"{getattr(item, ajudaField)}\"\"\")" if getattr(item, ajudaField) else '""'
            code += f"{nomeHelper} = add_simple_helper({nomeHelper}, \"{slug}\", {nomeI18n}, {ajudaI18n})\n\n"
            counter += 1

            if parentField and getattr(item, parentField):
                parentItem = getattr(item, parentField)
                slug = slugify(parentItem.nome)
                if slug not in parents:
                    parents.append(slug)
                    nomeI18n = f"_(\"{parentItem.nome}\")"
                    if parentAjudaField:
                        ajudaI18n = f"_(\"\"\"{getattr(parentItem, parentAjudaField)}\"\"\")" if getattr(parentItem, parentAjudaField) else '""'
                        code += f"{nomeHelper} = add_simple_helper({nomeHelper}, \"{slug}\", {nomeI18n}, {ajudaI18n})\n\n"
                    else:
                        code += f"{nomeHelper} = add_simple_helper({nomeHelper}, \"{slug}\", {nomeI18n})\n\n"
                    counter += 1

        return {'code': code, 'counter': counter}
