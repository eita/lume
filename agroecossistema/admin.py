from django.contrib import admin

# Register your models here.
from agroecossistema.models import *

class CicloAnualReferenciaAdmin(admin.ModelAdmin):
    list_display = ("agroecossistema", "ano", "sim_slug",)

admin.site.register(Agroecossistema)
admin.site.register(CicloAnualReferencia, CicloAnualReferenciaAdmin)
admin.site.register(Pessoa)
admin.site.register(ComposicaoNsga)
admin.site.register(Terra)
admin.site.register(ProjecaoDeFuturo)
admin.site.register(DiagramasDeFluxo)
