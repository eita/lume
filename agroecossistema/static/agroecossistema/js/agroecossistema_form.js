function formatState(state) {
  if (!state.id) {
    return state.text;
  }

  const title = `Editar ${$.trim(state.text)}`;
  const obj = /comunidade/i.test(state._resultId) ? "comunidade" : "territorio";
  const stateEl = $(
    `<span><button type="button" class="mudancas_eventos-editar-wrapper" title="${title}" data-object="${obj}" aria-label="${title}" id="${state.id}"><i class="fas fa-pen mudancas_eventos-editar" aria-hidden="true"></i></button><span>${state.text}</span></span>`
  );
  stateEl.find("span").text(state.text);

  setTimeout(
    function () {
      $(".mudancas_eventos-editar-wrapper")
        .off("click")
        .on("click", function () {
          const obj = $(this);
          if (obj) {
            openEditaForm(obj);
            setTimeout(function () {
              $('select[name="comunidade"], select[name="territorio"]').select2(
                "close"
              );
            }, 10);
          }
        });
    },
    [200]
  );
  return stateEl;
}

function openCriaForm(item) {
  item.novo = true;

  setTimeout(function () {
    $("#modal-right-wrapper").scrollTop(0);
    $(".modal-right-body").scrollTop(0);
  }, 600);
  $(".modalFormDiv").hide();

  $(".modal-right-footer").show();
  $("#modal-right-wrapper").fadeIn();

  obj = item.element_id;
  if (/comunidade/i.test(obj)) {
    $(".modal-right-title").html(`Criar comunidade`);
    $(".comunidadeFormDiv").show();
    comunidadeCriaForm(item);
  } else {
    $(".modal-right-title").html(`Criar território`);
    $(".territorioFormDiv").show();
    territorioCriaForm(item);
  }
}

function openEditaForm(item) {
  $(".modal-right-title").html(item.attr("title"));
  $(".modal-right-footer .btn-apagar").data("id", item.attr("id"));
  item.novo = false;

  setTimeout(function () {
    $("#modal-right-wrapper").scrollTop(0);
    $(".modal-right-body").scrollTop(0);
  }, 600);
  $(".modalFormDiv").hide();

  obj = item.data("object");
  if (obj == "comunidade") {
    comunidadeEditaForm(item);
    $(".comunidadeFormDiv").show();
  } else {
    territorioEditaForm(item);
    $(".territorioFormDiv").show();
  }
  $(".modal-right-footer").show();
  $("#modal-right-wrapper").fadeIn();
}

function territorioCriaForm(item) {
  // Definir contexto
  ctx = $(".modal-right");
  form = $("form:visible", ctx);

  // Limpar form
  form[0].reset();
  $("select option", ctx).remove();

  // Atribuir nome
  $('input[name="nome"]', ctx).val(item.titulo);

  // Atribuir action para adicionar
  form.attr("action", FORM_ACTION_ADD_TERRITORIO);
}

function comunidadeCriaForm(item) {
  // Definir contexto
  ctx = $(".modal-right");
  form = $("form:visible", ctx);

  // Limpar form
  form[0].reset();
  $("select option", ctx).remove();

  // Atribuir nome
  $('input[name="nome"]', ctx).val(item.titulo);

  // Atribuir action para adicionar
  form.attr("action", FORM_ACTION_ADD_COMUNIDADE);
}

function territorioEditaForm(item) {
  id = item.attr("id");
  url_ajax = URL_TERRITORIO.replace(ID_TO_REPLACE, id);
  form_action_territorio = FORM_ACTION_TERRITORIO.replace(ID_TO_REPLACE, id);

  $.ajax({
    url: url_ajax,
    success: function (result) {
      ctx = $(".modal-right");

      form = $("form", ctx);
      form.attr("action", form_action_territorio);

      nome = $('input[name="nome"]', ctx);
      nome.val(result.nome);

      descricao = $('textarea[name="descricao"]', ctx);
      descricao.val(result.descricao);
    },
  });
}

function comunidadeEditaForm(item) {
  id = item.attr("id");
  url_ajax = URL_COMUNIDADE.replace(ID_TO_REPLACE, id);
  form_action_comunidade = FORM_ACTION_COMUNIDADE.replace(ID_TO_REPLACE, id);

  $.ajax({
    url: url_ajax,
    success: function (result) {
      ctx = $(".modal-right");

      form = $("form", ctx);
      form.attr("action", form_action_comunidade);

      nome = $('input[name="nome"]', ctx);
      nome.val(result.nome);

      pais = $('select[name="pais"]', ctx);
      if (!$("option[value=" + result.pais.id + "]", pais).length) {
        const optionPais = new Option(
          result.pais.label,
          result.pais.id,
          true,
          true
        );
        pais.append(optionPais).trigger("change");
      } else {
        pais.val(result.pais.id).trigger("change");
      }

      uf = $('select[name="uf"]', ctx);
      if (!$("option[value=" + result.uf.id + "]", pais).length) {
        const optionUF = new Option(result.uf.label, result.uf.id, true, true);
        uf.append(optionUF).trigger("change");
      } else {
        uf.val(result.uf.id).trigger("change");
      }

      municipio = $('select[name="municipio"]', ctx);
      if (!$("option[value=" + result.municipio.id + "]", pais).length) {
        const optionMunicipio = new Option(
          result.municipio.label,
          result.municipio.id,
          true,
          true
        );
        municipio.append(optionMunicipio).trigger("change");
      } else {
        municipio.val(result.municipio.id).trigger("change");
      }
    },
  });
}

function initSelect2(isFirstTime = true) {
  const elements = $('select[name="comunidade"], select[name="territorio"]');
  if (!isFirstTime) {
    elements.select2("destroy");
  }
  elements.select2({
    tags: true,
    templateSelection: formatState,
    insertTag: function (data, tag) {
      data.push({ id: tag.id, text: `Criar "${tag.text}"...` });
    },
  });
  if (isFirstTime) {
    elements.on("select2:select", function (e) {
      if (!e.params.data.element) {
        $('option[data-select2-tag="true"]').remove();
        newEventoInputId = e.target.id;
        openCriaForm({
          titulo: e.params.data.text.replace('Criar "', "").replace('"...', ""),
          element_id: newEventoInputId,
          // start: `${anoReferencia}-01-03T00:00:00`,
        });
        return;
      }
      // salvaParametro($(e.target));
    });
    elements.on("select2:unselect", function (e) {
      if (!e.params.originalEvent) {
        return;
      }

      e.params.originalEvent.stopPropagation();
    });

    $(document).on("mouseenter", ".select2-selection__rendered", function () {
      $(".select2-selection__choice").removeAttr("title");
    });
  }
}

modal_right_form_submit = function (evt) {
  evt.preventDefault();

  const target = evt.target;
  const $target = $(target);
  const formId = target.id;

  const url = target.action;
  const formData = $target.serialize();

  $.ajax({
    method: "POST",
    url,
    data: formData,
    statusCode: {
      400: function (data) {
        console.log("error", data);
        json = data.responseJSON;
        msg = json.message;
        $("#alert-feedback-danger-title").html("Erro ao gravar!");
        $("#alert-feedback-danger-message").html(msg);
        $("#alert-feedback-danger").css("display", "block");
      },
      200: function (data) {
        if (/comunidade/i.test(url)) {
          ctx = $('select[name="comunidade"]');
        } else {
          ctx = $('select[name="territorio"]');
        }
        if (ctx.val()) {
          $("option:selected", ctx).html(data.str);
        } else {
          var option = new Option(data.str, data.pk, true, true);
          ctx.append(option).trigger("change");
        }
        initSelect2(false);
        $("#modal-right-wrapper").fadeOut();
      },
    },
  });
};

modal_right_btn_apagar_click = function (evt) {
  if (!confirm("Deseja realmente apagar?")) return;

  id = $(this).data("id");
  form_action = $("#modal-right-wrapper form:visible").attr("action");
  if (/comunidade/i.test(form_action)) {
    url_delete = URL_DELETE_COMUNIDADE.replace(ID_TO_REPLACE, id);
    ctx = $('select[name="comunidade"]');
  } else {
    url_delete = URL_DELETE_TERRITORIO.replace(ID_TO_REPLACE, id);
    ctx = $('select[name="territorio"]');
  }

  $.ajax({
    url: url_delete,
    method: "POST",
    statusCode: {
      405: function (data) {
        console.log("error", data);
        json = data.responseJSON;
        msg = json.message;
        $("#alert-feedback-danger-title").html("Erro ao apagar!");
        $("#alert-feedback-danger-message").html(msg);
        $("#alert-feedback-danger").css("display", "block");
      },
      200: function (data) {
        $("option:selected", ctx).remove();
        initSelect2(false);
        $("#modal-right-wrapper").fadeOut();
      },
    },
  });
};

$(document).ready(function () {
  initSelect2(true);

  $(".btn-salvar-form-modal").on("click", function (evt) {
    $("#modal-right-wrapper form:visible").trigger("submit");
  });

  $(".modal-right form").on("submit", modal_right_form_submit);

  $(".modal-right .btn-apagar").on("click", modal_right_btn_apagar_click);
});
