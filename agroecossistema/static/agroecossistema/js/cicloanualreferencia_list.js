$(document).ready(function () {
    $('.car-apagar').click(function (evt) {
        var elm = $(this);
        // if (confirm('Deseja realmente apagar o Ciclo Anual de Referẽncia "' + elm.data('carname') + '"?')) {
        if (confirm(gettext('Deseja realmente apagar este ciclo de análise econômica?'))) {
            if (confirm(gettext('ATENÇÃO!') + '\n' + gettext('Esta operação não poderá ser desfeita!') + '\n' + gettext('Isto irá apagar a análise econômica e os dados de composição e terras deste ciclo!') + '\n' + gettext('Continuar?'))) {
                $.ajax(elm.data('url'), {
                    'method': 'POST',
                    'success': function () {
                        elm.closest('.car-line').remove();
                    },
                    'error': function () {
                        alert(gettext('Não foi possível apagar.'));
                    }
                });
            }
        }
        return false;
    });
});
