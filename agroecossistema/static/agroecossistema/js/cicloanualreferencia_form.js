$(document).ready(function () {
    $('#id_ano').on("dp.change", function (e) {
        var anoInicial = $(this).val();
        var anoFinal = (mesInicioCiclo < mesFimCiclo) ? anoInicial : parseInt(anoInicial)+1;
        $("#anoHelp").remove();
        const target = $('#id_ano').parent();
        $("<small id='anoHelp' class='form-text text-muted'>" + gettext("Ciclo produtivo") + " " + gettext("de") + " " + mesInicioCicloNome + " " + gettext("de") + " " + anoInicial + " " + gettext("até") + " " + mesFimCicloNome+" " + gettext("de") + " " + anoFinal + ".</small>").insertAfter(target);
     });
});
