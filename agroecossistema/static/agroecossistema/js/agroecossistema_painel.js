/********************************************/
/********** LOAD CONTENT ACCORDION **********/
/********************************************/
function get_content_url(elem, force_reload=false) {
  $('.icon-panel-accordion').removeClass('icon-panel-chevron-lg-up');
  $('.icon-panel-accordion').addClass('icon-panel-chevron-lg-down');
  elem_btn = $(elem).prev().find('.icon-panel-accordion');
  elem_btn.removeClass('icon-panel-chevron-lg-down');
  elem_btn.addClass('icon-panel-chevron-lg-up');

  if ($.trim($('div', elem).html()) != '' && ! force_reload)
    return

  $('div', elem).html( 'carregando ...' );

  $.get($(elem).data('contentUrl'))
    .done(function(data){
      $('div', elem).html( data );

      $('#analises-qualitativas table.datatable')
        .parent('div')
        .parent('div')
        .css('display', 'none');
    })
    .fail(function(){
      $('div', elem).html('');
      console.log('falha no carregamento do conteúdo!');
    })
}

$('.item-accordion').on('show.bs.collapse', function () {
  get_content_url(this);
});

$('.item-accordion').on('hide.bs.collapse', function () {
  elem_btn = $(this).prev().find('.icon-panel-accordion');
  elem_btn.removeClass('icon-panel-chevron-lg-up');
  elem_btn.addClass('icon-panel-chevron-lg-down');
});

get_content_url(
  $('#painel_agroecossistema_linhadotempo')
);

get_content_url(
  $('#painel_agroecossistema_imagem')
);

$(document).on('click', '.btn-qualitativa-analise-change', function (evt) {
  evt.preventDefault();

  item_active = $('.carousel-item:visible');
  atributo = item_active.data('tblAtributo');

  url = $(this).attr('href');
  $('#analises-qualitativas').data('contentUrl', url+'?param_atributo_id='+atributo);
  get_content_url($('#analises-qualitativas'), force_reload=true);
});

/**********************************************/
/************ LOAD CONTENT ON MODAL ***********/
/**********************************************/
function modal_open(btn_action) {
  elem_modal = $('#modal-right-wrapper');
  btn_action = $(btn_action);
  $('.modal-right-title', elem_modal).html(btn_action.data('title'));

  // $('.modal-body', elem_modal).html($(`#${elem_content}`).val());
  $('.modal-right-body', elem_modal).html('carregando ...');

  $.get($(btn_action).data('contentUrl'))
    .done(function(data){
      $('.modal-right-body', elem_modal).html( data );
      $('.btn-apagar', '.modal-right-footer').css('display', 'none');
    })
    .fail(function(){
      $('.modal-right-body', elem_modal).html('');
      console.log('falha no carregamento do conteúdo!');
    })

  // elem_modal.modal('show');
  elem_modal.fadeIn();
}

$(document).on('click', '.action-modal', function(){
  $('#modal-ameba-right-wrapper').hide();
  modal_open(this);
});

/*************************************/
/************ SUBMIT MODAL ***********/
/*************************************/
modal_right_form_submit = function (evt) {
  evt.preventDefault();

  const target = evt.target;
  const $target = $(target);

  const url = target.action;
  // const formData = $target.serialize();
  var formData = new FormData(target);

  $.ajax({
    method: "POST",
    url,
    data: formData,
    statusCode: {
      400: function (data) {
        console.log("error", data);
        json = data.responseJSON;
        msg = json.message;
        $("#alert-feedback-danger-title").html("Erro ao gravar!");
        $("#alert-feedback-danger-message").html(msg);
        $("#alert-feedback-danger").css("display", "block");
      },
      200: function (data) {
        $("#modal-right-wrapper").fadeOut();
        if (data.target=='evento') {
          get_content_url(
            $('#painel_agroecossistema_linhadotempo'),
            force_reload=true
          );
        }
        if ($('.item-accordion.collapse.show').length){
          get_content_url(
            $('.item-accordion.collapse.show').first(),
            force_reload=true
          );
        }
      },
    },
    cache: false,
    contentType: false,
    processData: false
  });
};

$(document).on("submit", ".modal-right form", modal_right_form_submit);

$(".btn-salvar-form-modal").on("click", function (evt) {
  $("#modal-right-wrapper form:visible").first().trigger("submit");
});

/********************************************/
/********** FIX ALERT ON DISMISS ************/
/********************************************/
$("#alert-feedback-danger").on('close.bs.alert', function (evt) {
  evt.preventDefault();
  $("#alert-feedback-danger").css("display", "none");
});

/****************************************************/
/********** AGROECOSSISTEMA UPLOAD IMAGE ************/
/****************************************************/
$(document).on('change', "#formAgroecossistemaImage input[type='file']", function(){
  $('#formAgroecossistemaImage').trigger("submit");
});
$(document).on("submit", '#formAgroecossistemaImage', function(evt){
  evt.preventDefault();
  var url = $(this).attr("action");
  var formData = new FormData(this);
  $.ajax({
    url: url,
    type: 'POST',
    data: formData,
    success: function (data) {
      console.log('Upload completo!');
      console.log(data);
      get_content_url(
        $('#painel_agroecossistema_imagem'),
        force_reload=true
      );
    },
    cache: false,
    contentType: false,
    processData: false
  });
});

/****************************************************/
/********** AGROECOSSISTEMA DELETE IMAGE ************/
/****************************************************/
$(document).on("click", "#agroecossistema_delete_image", function (evt) {

  if (! confirm('Deseja realmente apagar esta imagem do agroecossistema?'))
    return false

  $('#imagem-clear_id').prop( "checked", true );
  $('#formAgroecossistemaImage').trigger("submit");
});

/********************************************/
/********** CAROUSEL QUALITATIVA ************/
/********************************************/
function setTitleQualitativaCarousel(elem) {
  item_active = $('.carousel-item:visible', elem);
  title = item_active.data('atributoTitleHeader');
  $('#title_qualitativa_carousel').text(title);
}
$(document).on('slid.bs.carousel', '#carouselQualitativaGraficos', function () {
  setTitleQualitativaCarousel($(this));
})

/******************************************/
/********** CAROUSEL ECONOMICA ************/
/******************************************/
function setTitleEconomicaCarousel(elem) {
  item_active = $('.carousel-item:visible', elem);
  title = item_active.data('atributoTitleHeader');
  $('#title_economica_carousel').text(title);
}
$(document).on('slid.bs.carousel', '#carouselEconomicaGraficos', function () {
  setTitleEconomicaCarousel($(this));
})

/***********************************************/
/********** CAROUSEL FLUXO DE CAIXA ************/
/***********************************************/
function setTitleDiagramaFluxoCarousel(elem) {
  item_active = $('.carousel-item:visible', elem);
  title = item_active.data('atributoTitleHeader');
  $('#title_diagrama_fluxo_carousel').text(title);
  edit_url = item_active.data('editUrl');
  $('#diagrama_fluxo_btn_edit').data('contentUrl', edit_url);
}
$(document).on('slid.bs.carousel', '#carouselDiagramaFluxo', function () {
  setTitleDiagramaFluxoCarousel($(this));
})
