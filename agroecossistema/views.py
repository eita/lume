import re
import datetime

from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import NON_FIELD_ERRORS
from django.db import transaction
from django.db.models.deletion import ProtectedError
from django.db.utils import IntegrityError
from django.forms.forms import BaseForm
from django.http import HttpResponseRedirect
from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse
from django.core import serializers
from django.utils.decorators import method_decorator
from django.utils.functional import cached_property
from django.utils.text import slugify
from django.utils.translation import gettext as _
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import DetailView, ListView, UpdateView, CreateView
from django.views.generic.base import TemplateView, RedirectView
from django.views.generic.edit import DeleteView, FormView, FormMixin
from rules.contrib.views import PermissionRequiredMixin

from anexos.utils import organiza_anexos
from economica.models import Analise as AnaliseEconomica
from qualitativa.models import Analise
from qualitativa.models import (
    Atributo, Parametro, Analise as AnaliseQualitativa,
    AnaliseTemplate, Qualificacao, AvaliacaoParametro
)
from linhadotempo.models import EventoGatilho, Evento
from main.utils import lista_agroecossistemas_values, get_qualitativa_helper_i18n
from economica.utils import LJSONRenderer
from main.views import LumeDeleteView
from relatorios.models import TotaisAnaliseEconomica
from relatorios.serializers import (
    TotaisAnaliseEconomicaSerializer,
    TotaisPorProdutoSerializer,
)
from main.mixins import AjaxableResponseMixin
from .forms import (
    AgroecossistemaForm,
    AgroecossistemaImagemForm,
    CicloAnualReferenciaForm,
    PessoaForm,
    PessoaFormSet,
    ComposicaoNsgaForm,
    TerraForm,
    CicloAnualReferenciaEditForm,
    TerraFormSet,
    ComposicaoNsgaFormSet,
    ComparacaoAnaliseForm,
    SimulacaoForm,
    SimulacaoUpdateForm,
    DiagramaDeFluxoForm,
    ProjecaoDeFuturoForm,
)
from .models import (
    Agroecossistema, CicloAnualReferencia, Pessoa,
    ComposicaoNsga, Terra, DiagramasDeFluxo,
    ProjecaoDeFuturo)
from main.forms import ComunidadeForm, TerritorioForm


class AgroecossistemaIndexView(PermissionRequiredMixin, TemplateView):
    template_name = "agroecossistema/agroecossistema_index.html"
    permission_required = "permissao_padrao"


class AgroecossistemaListView(PermissionRequiredMixin, ListView):
    model = Agroecossistema
    permission_required = "permissao_padrao"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["titulo"] = _("Agroecossistemas")
        context["modulo"] = "main"
        context["enable_datatable_export"] = True
        context["agroecossistemas"] = self.get_agroecossistemas().select_related(
            "organizacao",
            "comunidade",
            "comunidade__municipio",
            "comunidade__municipio__uf",
            "comunidade__pais",
            "territorio",
            "autor",
        )
        context[
            "own_agroecossistemas"
        ] = self.get_own_agroecossistemas().select_related(
            "organizacao",
            "comunidade",
            "comunidade__municipio",
            "comunidade__municipio__uf",
            "comunidade__pais",
            "territorio",
            "autor",
        )
        return context

    def get_agroecossistemas(self):
        if self.request.user.has_perm("organizacao.ver_todos"):
            return Agroecossistema.objects.all()
        elif self.request.user.is_anonymous:
            return Agroecossistema.objects.none()
        else:
            return Agroecossistema.objects.filter(
                organizacao__usuarias=self.request.user
            )

    def get_own_agroecossistemas(self):
        if self.request.user.is_anonymous:
            return None
        return Agroecossistema.objects.filter(autor=self.request.user)


class AgroecossistemaCreateView(PermissionRequiredMixin, CreateView):
    model = Agroecossistema
    form_class = AgroecossistemaForm
    titulo = _("Cadastrar Agroecossistema")
    permission_required = "agroecossistema.cadastrar"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["titulo"] = self.titulo
        context["modulo"] = "main"
        context["choices_help_text"] = CicloAnualReferencia.CHOICES_HELP_TEXTS
        try:
            pais_sugerido = self.request.user.agroecossistemas.last().comunidade.pais.id
        except:
            pais_sugerido = None
        context["form_comunidade"] = ComunidadeForm(
            prefix={"pais_sugerido": pais_sugerido}, instance=None
        )
        context["form_territorio"] = TerritorioForm(instance=None)
        context["form_modal_not_define"] = True
        return context

    # Sending user object to the form, to verify which fields to display/remove (depending on group)
    def get_form_kwargs(self):
        kwargs = super(AgroecossistemaCreateView, self).get_form_kwargs()
        kwargs.update({"user": self.request.user})
        return kwargs

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.autor = self.request.user
        obj.save()

        ano = datetime.date.today().year
        novo_ciclo = CicloAnualReferencia.objects.create(
            ano=ano, agroecossistema_id=obj.pk
        )
        nova_analise = Analise.objects.create(ciclo_anual_referencia_id=novo_ciclo.id)

        return HttpResponseRedirect(obj.get_absolute_url())


class AgroecossistemaDetailView(PermissionRequiredMixin, DetailView):
    model = Agroecossistema
    permission_required = "agroecossistema.visualizar"

    def dispatch(self, request, *args, **kwargs):
        ano_car = self.kwargs.get("ano_car", None)
        if ano_car:
            return super().dispatch(
                request, *args, **kwargs
            )
        agroecossistema_id = self.kwargs.get("pk", None)
        last_car = (
            CicloAnualReferencia.objects.filter(
                agroecossistema_id=agroecossistema_id, sim_slug__isnull=True
            )
            .order_by("-ano")
            .first()
        )
        if last_car:
            return redirect(
                "agroecossistema_detail_car",
                pk=agroecossistema_id,
                ano_car=last_car.ano,
            )
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        agroecossistema = self.get_object()
        context["agroecossistema"] = agroecossistema
        context["modulo"] = "main"
        context["submodulo"] = "painel"
        context["description"] = agroecossistema.get_description()
        context["years"] = agroecossistema.getYearsSpan()

        sim_slug = None if "sim_slug" not in self.kwargs else self.kwargs["sim_slug"]
        ano_car = None if "ano_car" not in self.kwargs else self.kwargs["ano_car"]
        context['ano_car'] = ano_car

        built_session = Agroecossistema.build_session(self.object.id, ano_car, sim_slug)
        for key, val in built_session["session"].items():
            self.request.session[key] = val

        if built_session["session"]["ano_car"]:
            context["car"] = built_session["car"]
            context["analise_economica"] = built_session[
                "car"
            ].analises_economicas.first()
            context["totais"] = getattr(context["analise_economica"], "totais", None)

        return context


class AgroecossistemaDetailNewView(AgroecossistemaDetailView):
    template_name = "agroecossistema/agroecossistema_detail_new.html"

    def get_analise(self):
        return AnaliseQualitativa.objects\
            .filter(agroecossistema=self.get_object())\
            .order_by('-ano_referencia')\
            .first()

    def get_parametros_list(self):
        qs = Parametro.objects.filter(atributo__in=self.atributos_list)
        # Deprecated. TODO: remove this check on parametros_inativos_agroecossistema, which
        # became deprecated after the introduction of analise_templates.
        parametros_inativos_agroecossistema = self.agroecossistema.parametros_inativos.get(
            'inativos', None)
        parametros_inativos_analise = self.analise.parametros_inativos.get(
            'inativos', None)
        parametros_inativos = parametros_inativos_analise if parametros_inativos_analise is not None else parametros_inativos_agroecossistema

        if parametros_inativos:
            try:
                qs = qs.exclude(id__in=parametros_inativos)
            except:
                pass

        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        self.agroecossistema = self.get_object()
        self.analise = self.get_analise()
        if self.analise:
            self.atributos_list = Atributo.objects.filter(
                analise_template=self.analise.analise_template)

            ano_referencia = Qualificacao.objects.filter(
                ano=self.analise.ano_referencia)
            analises_list = Analise.objects.filter(
                agroecossistema_id=self.agroecossistema.id)
            tem_mudancas_antigo = False

            avaliacoes_parametro = AvaliacaoParametro.objects.filter(
                analise_id=self.analise.id)
            qualificacoes_atuais = Qualificacao.objects.filter(
                agroecossistema_id=self.agroecossistema.id, ano=self.analise.ano)
            qualificacoes_referencia = Qualificacao.objects.filter(agroecossistema_id=self.agroecossistema.id,
                                                                   ano=self.analise.ano_referencia)
            # parametros_list = self.get_queryset()
            parametros_list = self.get_parametros_list()
            atributos_list = self.atributos_list

            atributosVazios = []
            atributos = {}
            for atributo in self.atributos_list:
                count = 0
                for parametro in parametros_list:
                    if atributo.id == parametro.atributo_id:
                        count += 1
                if count == 0:
                    atributosVazios.append(atributo.id)
                else:
                    atributos[atributo.id] = {}

            if len(atributosVazios) > 0:
                atributos_list = self.atributos_list.exclude(
                    id__in=atributosVazios)

            subsections = {}

            for parametro in parametros_list:
                try:
                    avaliacao_parametro = avaliacoes_parametro.get(
                        parametro_id=parametro.id)
                except AvaliacaoParametro.DoesNotExist:
                    avaliacao_parametro = None

                if avaliacao_parametro is not None and avaliacao_parametro.mudancas and tem_mudancas_antigo == False:
                    tem_mudancas_antigo = True

                try:
                    qualificacao_atual = qualificacoes_atuais.get(
                        parametro_id=parametro.id).qualificacao
                except Qualificacao.DoesNotExist:
                    qualificacao_atual = ''

                try:
                    qualificacao_referencia = qualificacoes_referencia.get(
                        parametro_id=parametro.id).qualificacao
                except Qualificacao.DoesNotExist:
                    qualificacao_referencia = ''

                mudancas_eventos = [i['id'] for i in avaliacao_parametro.mudancas_eventos.values(
                    'id')] if avaliacao_parametro else []

                parametro_i18n = get_qualitativa_helper_i18n(
                    parametro, parametro.atributo)
                atributo_i18n = parametro_i18n['atributo']
                subsection = parametro_i18n['subsection']

                atributos[parametro.atributo_id][parametro.id] = {
                    'parametro': parametro,
                    'parametro_i18n': parametro_i18n['parametro'],
                    'atributo_i18n': atributo_i18n,
                    'subsection': subsection,
                    'qualificacao_referencia': qualificacao_referencia,
                    'qualificacao_atual': qualificacao_atual,
                    'mudancas': avaliacao_parametro.mudancas if avaliacao_parametro else '',
                    'mudancas_eventos': mudancas_eventos,
                    'justificativa': avaliacao_parametro.justificativa if avaliacao_parametro else '',
                    'ajuda': parametro_i18n['descricao'],
                }

                if parametro.atributo_id not in subsections:
                    subsections[parametro.atributo_id] = {}

                if subsection != "" and subsection not in subsections[parametro.atributo_id]:
                    subsections[parametro.atributo_id][subsection] = subsection
                elif subsection == "" and parametro.atributo_id not in subsections[parametro.atributo_id]:
                    subsections[parametro.atributo_id][parametro.atributo_id] = atributo_i18n

            eventos_linhadotempo = Evento.objects.filter(
                agroecossistema=self.get_object(),
                data_inicio__gte=self.analise.ano_referencia,
                data_inicio__lte=self.analise.ano
            ).order_by('data_inicio', 'titulo')

            context['eventos_linhadotempo'] = eventos_linhadotempo
            context['atributos_list'] = atributos_list
            context['analises_list'] = analises_list
            context['atributos'] = atributos
            context['subsections'] = subsections
            context['tem_mudancas_antigo'] = tem_mudancas_antigo

        context['analise'] = self.analise
        context['agroecossistema'] = self.agroecossistema
        # context['modulo'] = 'qualitativa'
        context["modulo"] = "agroecossistema"
        context["submodulo"] = "agroecossistema_painel"
        context['hide_sidebar'] = False
        context['enable_datatable_export'] = True
        context['glossario_model'] = TotaisAnaliseEconomica()

        context['form_agroecossistema_imagem_update'] = AgroecossistemaImagemForm(
            prefix=None, instance=self.agroecossistema, user=self.request.user)

        context['accordion_items'] = {
            'Caracterização': {
                'title': _('Caracterização'),
                'url': reverse('agroecossistema_detail_caracterizacao',
                               args=(self.agroecossistema.id, context['car'].ano)),
            },
            'Análises Qualitativas': {
                'title': _('Análises Qualitativas'),
                'url':  reverse('qualitativa:painel-graficos',
                                args=(self.agroecossistema.id, context['car'].ano)),
            },
            'Relatórios Econômicos': {
                'title': _('Relatórios Econômicos'),
                'url': reverse('economica:painel-graficos',
                               args=(self.agroecossistema.id, context['car'].ano)),
            },
            'Diagramas de Fluxo': {
                'title': _('Diagramas de Fluxo'),
                'url': reverse('painel_diagrama_fluxo',
                               args=(self.agroecossistema.id,)),
            },
            'Fragilidades, potencialidade e projeção de futuro': {
                'title': _('Fragilidades, potencialidade e projeção de futuro'),
                'url': reverse('painel_projecao_futuro',
                               args=(self.agroecossistema.id, context['car'].ano)),
            }
        }

        return context


class AgroecossistemaDetailCaracterizacaoView(AgroecossistemaDetailView):
    template_name = "agroecossistema/inclusiontags/painel_caracterizacao.html"

    def get_subsistema_list(self):
        get_args = {
            'ciclo_anual_referencia__agroecossistema_id': self.kwargs['pk'],
            'ciclo_anual_referencia__ano': self.kwargs['ano_car']
        }

        if 'sim_slug' in self.kwargs:
            get_args['ciclo_anual_referencia__sim_slug'] = self.kwargs['sim_slug']
        else:
            get_args['ciclo_anual_referencia__sim_slug__isnull'] = True

            analise = AnaliseEconomica.objects.get(**get_args)
            return analise.subsistemas.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['subsistemas_object_list'] = self.get_subsistema_list()

        return context


class AgroecossistemaUpdateView(PermissionRequiredMixin, UpdateView):
    model = Agroecossistema
    form_class = AgroecossistemaForm
    titulo = _("Editar Agroecossistema")
    permission_required = "agroecossistema.alterar"

    # Sending user object to the form, to verify which fields to display/remove (depending on group)
    def get_form_kwargs(self):
        kwargs = super(AgroecossistemaUpdateView, self).get_form_kwargs()
        kwargs.update({"user": self.request.user})
        kwargs.update(
            {
                "can_delete": self.request.user.has_perm(
                    "agroecossistema.apagar", self.get_object()
                )
            }
        )
        return kwargs

    def get_form(self):
        form = super(AgroecossistemaUpdateView, self).get_form(AgroecossistemaForm)
        if not self.request.user.is_superuser:
            form.fields.pop("visualizacao_publica")
        return form

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["titulo"] = self.titulo
        context["modulo"] = "main"

        try:
            pais_sugerido = self.request.user.agroecossistemas.last().comunidade.pais.id
        except:
            pais_sugerido = None
        context["form_comunidade"] = ComunidadeForm(
            prefix={"pais_sugerido": pais_sugerido}, instance=None
        )
        context["form_territorio"] = TerritorioForm(instance=None)
        context["form_modal_not_define"] = True

        return context


class AgroecossistemaImagemUpdateView(PermissionRequiredMixin, AjaxableResponseMixin, UpdateView):
    model = Agroecossistema
    template_name = "agroecossistema/painel_agroecossistema_form_image.html"
    form_class = AgroecossistemaImagemForm
    titulo = _("Editar imagem do Agroecossistema")
    permission_required = "agroecossistema.alterar"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"user": self.request.user})
        return kwargs


class AgroecossistemaDeleteView(PermissionRequiredMixin, LumeDeleteView):
    model = Agroecossistema
    permission_required = "agroecossistema.apagar"

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(AgroecossistemaDeleteView, self).dispatch(*args, **kwargs)


class CicloAnualReferenciaListView(PermissionRequiredMixin, ListView):
    model = CicloAnualReferencia
    permission_required = "agroecossistema.visualizar"

    def get_permission_object(self):
        agroecossistema = get_object_or_404(
            Agroecossistema, id=self.kwargs["agroecossistema_id"]
        )
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["modulo"] = "economica"
        context["submodulo"] = "lista"
        context["agroecossistema"] = Agroecossistema.objects.get(
            id=self.kwargs["agroecossistema_id"]
        )
        # context['car'] = _('Ciclos Anuais de Referência')  # para o breadcrumb
        context["car_label"] = _("Ciclos Anuais de Referência")  # para o breadcrumb
        return context

    def get_queryset(self):
        return CicloAnualReferencia.objects.filter(
            agroecossistema_id=self.kwargs["agroecossistema_id"], sim_slug__isnull=True
        )


class CicloAnualReferenciaCreateView(PermissionRequiredMixin, CreateView):
    model = CicloAnualReferencia
    form_class = CicloAnualReferenciaForm
    permission_required = "permissao_padrao"
    titulo = _("Criar Ciclo Anual de Referencia")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["titulo"] = self.titulo
        context["modulo"] = "economica"
        context["submodulo"] = "editar_dados"
        context["agroecossistema"] = Agroecossistema.objects.get(
            id=self.kwargs["agroecossistema_id"]
        )
        context["CHOICES_HELP_TEXTS"] = CicloAnualReferencia.CHOICES_HELP_TEXTS
        return context

    def form_valid(self, form):
        if not form.is_valid():
            return super().form_invalid(form)
        tipo_criacao_escolhido = None
        if form.data is not None:
            tipo_criacao_escolhido = form.data.get("tipo_criacao", None)

        try:
            if tipo_criacao_escolhido == CicloAnualReferenciaForm.CRIACAO_LIMPA:
                obj = form.save()
                # obj.agroecossistema_id = self.kwargs['agroecossistema_id']
                # obj.save()
                return HttpResponseRedirect(obj.get_absolute_url())

            else:
                ciclo_base_id = form.data.get("ciclo_base", None)
                if ciclo_base_id is not None:
                    ciclo_base = CicloAnualReferencia.objects.get(pk=int(ciclo_base_id))
                    novo_ciclo = ciclo_base.copia_profunda(
                        ano=form.data.get("ano"), tipo_copia=tipo_criacao_escolhido
                    )
                    return HttpResponseRedirect(novo_ciclo.get_absolute_url())

        except IntegrityError:
            form.full_clean()
            form._errors[NON_FIELD_ERRORS] = form.error_class(
                [
                    _("Ciclo anual de referência já cadastrado para o ano de %(ano)s")
                    % {"ano": str(obj.ano)}
                ]
            )
            return self.render_to_response(self.get_context_data(form=form))

    def get_form_kwargs(self):
        kwargs = super(CicloAnualReferenciaCreateView, self).get_form_kwargs()
        kwargs.update({"agroecossistema_id": self.kwargs["agroecossistema_id"]})
        return kwargs

    def form_invalid(self, form: BaseForm) -> HttpResponse:
        return super().form_invalid(form)


class CicloAnualReferenciaDetailView(PermissionRequiredMixin, DetailView):
    model = CicloAnualReferencia
    permission_required = "agroecossistema.visualizar"

    def get_permission_object(self):
        agroecossistema = get_object_or_404(
            Agroecossistema, id=self.kwargs["agroecossistema_id"]
        )
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["modulo"] = "economica"
        context["submodulo"] = "editar_dados"
        context["agroecossistema"] = Agroecossistema.objects.get(
            id=self.kwargs["agroecossistema_id"]
        )
        context["car"] = self.get_object()
        return context

    def get_object(self, queryset=None):
        return CicloAnualReferencia.objects.get(
            agroecossistema_id=self.kwargs["agroecossistema_id"],
            ano=self.kwargs["ano_car"],
            sim_slug__isnull=True,
        )


class CARTerrasUpdateView(PermissionRequiredMixin, UpdateView):
    model = CicloAnualReferencia
    form_class = CicloAnualReferenciaEditForm
    titulo = _("Editar ciclo anual de referência")
    template_name = "agroecossistema/cicloanualreferencia_terras_form.html"
    permission_required = "agroecossistema.visualizar"

    def get_permission_object(self):
        agroecossistema = get_object_or_404(
            Agroecossistema, id=self.kwargs["agroecossistema_id"]
        )
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        sim_slug = None if "sim_slug" not in self.kwargs else self.kwargs["sim_slug"]
        ano_car = None if "ano_car" not in self.kwargs else self.kwargs["ano_car"]

        built_session = Agroecossistema.build_session(
            self.kwargs["agroecossistema_id"], ano_car, sim_slug
        )
        for key, val in built_session["session"].items():
            self.request.session[key] = val

        context["titulo"] = self.titulo
        context["modulo"] = "agroecossistema"
        context["submodulo"] = "terras"
        context["agroecossistema"] = Agroecossistema.objects.get(
            id=self.kwargs["agroecossistema_id"]
        )
        context["car"] = self.get_object()

        if self.request.POST:
            context["terras_formset"] = TerraFormSet(
                self.request.POST, instance=self.get_object()
            )
        else:
            context["terras_formset"] = TerraFormSet(instance=self.get_object())

        return context

    def form_valid(self, form):
        context = self.get_context_data()
        terras_formset = context["terras_formset"]

        try:
            with transaction.atomic():
                self.object = form.save()

                if terras_formset.is_valid():
                    terras_formset.instance = self.object
                    terras_formset.save()
                else:
                    tmp = form.full_clean()
                    form._errors[NON_FIELD_ERRORS] = form.error_class(
                        [
                            _(
                                "Foram encontrados erros no formulário e os dados NÃO foram salvos. Favor fazer as correções indicadas."
                            )
                        ]
                    )
                    return self.render_to_response(self.get_context_data(form=form))

        except ValueError:
            tmp = form.full_clean()
            form._errors[NON_FIELD_ERRORS] = form.error_class(
                [
                    _(
                        "Foram encontrados erros no formulário e os dados NÃO foram salvos. Favor fazer as correções indicadas."
                    )
                ]
            )
            return self.render_to_response(self.get_context_data(form=form))

        return super(CARTerrasUpdateView, self).form_valid(form)

    def get_object(self, queryset=None):
        get_args = {
            "agroecossistema_id": self.kwargs["agroecossistema_id"],
            "ano": self.kwargs["ano_car"],
        }

        if "sim_slug" in self.kwargs:
            get_args["sim_slug"] = self.kwargs["sim_slug"]
        else:
            get_args["sim_slug__isnull"] = True

        return CicloAnualReferencia.objects.get(**get_args)

    def get_success_url(self):
        return self.request.path


class CARComposicaoUpdateView(PermissionRequiredMixin, UpdateView):
    model = CicloAnualReferencia
    form_class = CicloAnualReferenciaEditForm
    titulo = _("Editar ciclo anual de referência")
    template_name = "agroecossistema/cicloanualreferencia_composicao_form.html"
    permission_required = "agroecossistema.visualizar"

    def get_permission_object(self):
        agroecossistema = get_object_or_404(
            Agroecossistema, id=self.kwargs["agroecossistema_id"]
        )
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        agroecossistema_id = self.kwargs["agroecossistema_id"]
        sim_slug = None if "sim_slug" not in self.kwargs else self.kwargs["sim_slug"]
        ano_car = None if "ano_car" not in self.kwargs else self.kwargs["ano_car"]

        built_session = Agroecossistema.build_session(
            agroecossistema_id, ano_car, sim_slug
        )
        for key, val in built_session["session"].items():
            self.request.session[key] = val

        pessoas_list = Pessoa.objects.filter(agroecossistema_id=agroecossistema_id)

        context["titulo"] = self.titulo
        context["modulo"] = "agroecossistema"
        context["submodulo"] = "composicaonsga"
        context["agroecossistema"] = Agroecossistema.objects.get(id=agroecossistema_id)
        context["car"] = self.get_object()
        context["queryset"] = pessoas_list
        context["queryset_json"] = serializers.serialize("json", pessoas_list)

        if self.request.POST:
            context["composicao_nsga_formset"] = ComposicaoNsgaFormSet(
                self.request.POST,
                instance=self.get_object(),
                form_kwargs={"agroecossistema_id": self.kwargs["agroecossistema_id"]},
            )
        else:
            context["composicao_nsga_formset"] = ComposicaoNsgaFormSet(
                instance=self.get_object(),
                form_kwargs={"agroecossistema_id": self.kwargs["agroecossistema_id"]},
            )
        return context

    def form_valid(self, form):
        context = self.get_context_data()

        form.save()

        composicao_nsga_formset = context["composicao_nsga_formset"]

        try:
            with transaction.atomic():
                if composicao_nsga_formset.is_valid():
                    composicao_nsga_formset.instance = self.object
                    composicao_nsga_formset.save()
                    self.object = form.save()
                else:
                    tmp = form.full_clean()
                    form._errors[NON_FIELD_ERRORS] = form.error_class(
                        [
                            _(
                                "Foram encontrados erros no formulário e os dados NÃO foram salvos. Favor fazer as correções indicadas."
                            )
                        ]
                    )
                    return self.render_to_response(self.get_context_data(form=form))

        except (ValueError, ProtectedError) as e:
            tmp = form.full_clean()
            form._errors[NON_FIELD_ERRORS] = form.error_class(
                [
                    _(
                        "Foram encontrados erros no formulário e os dados NÃO foram salvos. Favor fazer as correções indicadas."
                    ),
                    self.error_message_friendly(e),
                ]
            )
            return self.render_to_response(self.get_context_data(form=form))

        return super(CARComposicaoUpdateView, self).form_valid(form)

    def get_object(self, queryset=None):
        get_args = {
            "agroecossistema_id": self.kwargs["agroecossistema_id"],
            "ano": self.kwargs["ano_car"],
        }

        if "sim_slug" in self.kwargs:
            get_args["sim_slug"] = self.kwargs["sim_slug"]
        else:
            get_args["sim_slug__isnull"] = True

        return CicloAnualReferencia.objects.get(**get_args)

    def get_success_url(self):
        return self.request.path

    def error_message_friendly(self, e):
        if isinstance(e, ProtectedError):
            if re.match("^Cannot delete", e.args[0]):
                return _(
                    _(
                        "Não foi possível retirar pessoa da composição, pois há tempo de trabalho registrado para ela na análise econômica."
                    )
                )
        return None


class CicloAnualReferenciaDeleteView(PermissionRequiredMixin, LumeDeleteView):
    model = CicloAnualReferencia
    permission_required = "agroecossistema.alterar"

    def get_permission_object(self):
        agroecossistema = get_object_or_404(
            Agroecossistema, id=self.kwargs["agroecossistema_id"]
        )
        return agroecossistema

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(CicloAnualReferenciaDeleteView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        get_args = {
            "agroecossistema_id": self.kwargs["agroecossistema_id"],
            "ano": self.kwargs["ano_car"],
        }

        if "sim_slug" in self.kwargs:
            get_args["sim_slug"] = self.kwargs["sim_slug"]
            self.request.session["sim_slug"] = self.kwargs["sim_slug"]
        else:
            get_args["sim_slug__isnull"] = True
            self.request.session["sim_slug"] = None

        return CicloAnualReferencia.objects.get(**get_args)

    def delete(self, request, *args, **kwargs):
        status = 200

        try:
            self.get_object().really_delete()
            payload = {"delete": "ok"}
        except (ProtectedError, IntegrityError) as e:
            payload = {"error": _("Erro ao apagar"), "message": "{0}".format(e)}
            status = 405  # Method not allowed

        return JsonResponse(payload, status=status)


class PessoaListView(PermissionRequiredMixin, UpdateView):
    model = Agroecossistema
    permission_required = "agroecossistema.visualizar"
    form_class = AgroecossistemaForm
    template_name = "agroecossistema/pessoa_list.html"

    def get_permission_object(self):
        agroecossistema = get_object_or_404(
            Agroecossistema, id=self.kwargs["agroecossistema_id"]
        )
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if self.request.GET.get("force_edit"):
            context["force_edit"] = True

        context["titulo"] = "Ver/editar Pessoas cadastradas"
        context["modulo"] = "agroecossistema"
        context["submodulo"] = "pessoas"
        context["agroecossistema"] = self.get_object()

        if self.request.POST:
            context["pessoas_formset"] = PessoaFormSet(
                self.request.POST, instance=self.get_object()
            )
        else:
            context["pessoas_formset"] = PessoaFormSet(instance=self.get_object())

        return context

    def get_form_kwargs(self):
        kwargs = super(PessoaListView, self).get_form_kwargs()
        kwargs.update({"user": self.request.user})
        return kwargs

    def form_valid(self, form):
        context = self.get_context_data()
        pessoas_formset = context["pessoas_formset"]

        try:
            with transaction.atomic():
                self.object = form.save()

                if pessoas_formset.is_valid():
                    pessoas_formset.instance = self.object
                    pessoas_formset.save()
                else:
                    tmp = form.full_clean()
                    form._errors[NON_FIELD_ERRORS] = form.error_class(
                        [
                            _(
                                "Foram encontrados erros no formulário e os dados NÃO foram salvos. Favor fazer as correções indicadas."
                            )
                        ]
                    )
                    return self.render_to_response(self.get_context_data(form=form))

        except ValueError:
            tmp = form.full_clean()
            form._errors[NON_FIELD_ERRORS] = form.error_class(
                [
                    _(
                        "Foram encontrados erros no formulário e os dados NÃO foram salvos. Favor fazer as correções indicadas."
                    )
                ]
            )
            return self.render_to_response(self.get_context_data(form=form))

        return super(PessoaListView, self).form_valid(form)

    def get_object(self, queryset=None):
        return Agroecossistema.objects.get(id=self.kwargs["agroecossistema_id"])

    def get_success_url(self):
        return reverse(
            "agroecossistema:car_update_composicao",
            kwargs={
                "agroecossistema_id": self.kwargs["agroecossistema_id"],
                "ano_car": self.request.session["ano_car"],
            },
        )


class PessoaCreateView(PermissionRequiredMixin, CreateView):
    model = Pessoa
    form_class = PessoaForm
    permission_required = "permissao_padrao"
    titulo = _("Adicionar pessoa à base")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["titulo"] = self.titulo
        context["modulo"] = "agroecossistema"
        context["submodulo"] = "pessoas"
        context["agroecossistema"] = Agroecossistema.objects.get(
            id=self.kwargs["agroecossistema_id"]
        )
        return context

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.agroecossistema = Agroecossistema.objects.get(
            id=self.kwargs["agroecossistema_id"]
        )
        try:
            obj.save()
            return HttpResponseRedirect(self.get_success_url(obj.id))
        except IntegrityError:
            form.full_clean()
            form._errors[NON_FIELD_ERRORS] = form.error_class(
                [_("Pessoa já cadastrada")]
            )
            return self.render_to_response(self.get_context_data(form=form))

    def get_success_url(self, object_id=None):
        next = self.request.GET.get("next")
        return (
            next
            if next
            else reverse(
                "agroecossistema:car_update_composicao",
                kwargs={
                    "agroecossistema_id": self.kwargs["agroecossistema_id"],
                    "ano_car": self.request.session["ano_car"],
                },
            )
        )


class PessoaDetailView(PermissionRequiredMixin, DetailView):
    model = Pessoa
    permission_required = "agroecossistema.visualizar"

    def get_permission_object(self):
        agroecossistema = get_object_or_404(
            Agroecossistema, id=self.kwargs["agroecossistema_id"]
        )
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["modulo"] = "agroecossistema"
        context["submodulo"] = "pessoas"
        context["agroecossistema"] = Agroecossistema.objects.get(
            id=self.kwargs["agroecossistema_id"]
        )
        return context


class PessoaUpdateView(PermissionRequiredMixin, UpdateView):
    model = Pessoa
    form_class = PessoaForm
    permission_required = "permissao_padrao"
    titulo = _("Editar Pessoa")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["titulo"] = self.titulo
        context["modulo"] = "agroecossistema"
        context["submodulo"] = "pessoas"
        context["agroecossistema"] = Agroecossistema.objects.get(
            id=self.kwargs["agroecossistema_id"]
        )
        return context

    def get_form_kwargs(self):
        kwargs = super(PessoaUpdateView, self).get_form_kwargs()
        kwargs.update(
            {
                "can_delete": self.request.user.has_perm(
                    "agroecossistema.alterar",
                    Agroecossistema.objects.get(id=self.kwargs["agroecossistema_id"]),
                )
            }
        )
        kwargs.update({"agroecossistema_id": self.kwargs["agroecossistema_id"]})
        return kwargs

    def get_success_url(self, object_id=None):
        return reverse(
            "agroecossistema:pessoa_list",
            kwargs={"agroecossistema_id": self.kwargs["agroecossistema_id"]},
        )


class PessoaDeleteView(PermissionRequiredMixin, LumeDeleteView):
    model = Pessoa
    permission_required = "agroecossistema.alterar"

    def get_permission_object(self):
        agroecossistema = get_object_or_404(
            Agroecossistema, id=self.kwargs["agroecossistema_id"]
        )
        return agroecossistema

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(PessoaDeleteView, self).dispatch(*args, **kwargs)


class AnaliseCompareFormView(PermissionRequiredMixin, FormView):
    form_class = ComparacaoAnaliseForm
    titulo = _("Comparar análises econômicas")
    template_name = "agroecossistema/analise_comparacao_form.html"
    permission_required = "agroecossistema.visualizar"

    def get_permission_object(self):
        agroecossistema = get_object_or_404(
            Agroecossistema, id=self.kwargs["agroecossistema_id"]
        )
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["titulo"] = _("Comparar análises econômicas")
        context["modulo"] = "economica"
        context["submodulo"] = "comparacao_economica"
        context["agroecossistema"] = Agroecossistema.objects.get(
            id=self.kwargs["agroecossistema_id"]
        )
        return context

    def get_form_kwargs(self):
        kwargs = super(AnaliseCompareFormView, self).get_form_kwargs()
        kwargs.update({"agroecossistema_id": self.kwargs["agroecossistema_id"]})
        return kwargs

    def get_success_url(self):
        # get the existing object or created a new one
        data = self.request.POST

        return reverse(
            "agroecossistema:analise_compara",
            kwargs={
                "agroecossistema_id": self.kwargs["agroecossistema_id"],
                "analise_id_1": data["analise1"],
                "analise_id_2": data["analise2"],
            },
        )


class AnaliseCompareView(PermissionRequiredMixin, TemplateView):
    template_name = "agroecossistema/analise_comparacao.html"
    analise1 = None
    analise2 = None
    permission_required = "agroecossistema.visualizar"

    def get_permission_object(self):
        agroecossistema = get_object_or_404(
            Agroecossistema, id=self.kwargs["agroecossistema_id"]
        )
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["modulo"] = "economica"
        context["submodulo"] = "comparacao_economica"
        context["agroecossistema"] = Agroecossistema.objects.get(
            id=self.kwargs["agroecossistema_id"]
        )
        context["base"] = "base.html"
        context["car1"] = self.analise1.ciclo_anual_referencia
        context["totais1"] = self.totais1
        context["totais1_json"] = self.totais1_json
        context["totais1_por_produto_json"] = self.totais1_por_produto_json
        context["car2"] = self.analise2.ciclo_anual_referencia
        context["totais2"] = self.totais2
        context["totais2_json"] = self.totais2_json
        context["totais2_por_produto_json"] = self.totais2_por_produto_json
        context["RELATORIOS"] = TotaisAnaliseEconomica.RELATORIOS
        context["COMPARACOES"] = TotaisAnaliseEconomica.COMPARACOES
        context["grafico_composicao_defs"] = self.grafico_composicao_defs
        context["arquivo_pre"] = slugify(
            context["agroecossistema"].nsga + "_" + context["car1"].__str__()
        )
        context["titulo_geral"] = _("Comparação de análises econômicas")
        return context

    @cached_property
    def grafico_composicao_defs(self):
        return list(
            filter(
                lambda x: x["id"] == "composicao_rendas",
                TotaisAnaliseEconomica.RELATORIOS["graficosRenda"]["itens"],
            )
        )[0]

    @cached_property
    def analise1(self):
        return Analise.objects.get(pk=self.kwargs["analise_id_1"])

    @cached_property
    def analise2(self):
        return Analise.objects.get(pk=self.kwargs["analise_id_2"])

    @cached_property
    def totais1(self):
        return getattr(self.analise1, "totais", None)

    @cached_property
    def totais1_json(self):
        r = (
            LJSONRenderer()
            .render(TotaisAnaliseEconomicaSerializer(self.totais1).data)
            .decode("utf-8")
        )
        return r

    @cached_property
    def totais1_por_produto_json(self):
        r = (
            LJSONRenderer()
            .render(
                TotaisPorProdutoSerializer(
                    self.analise1.totais_por_produto.all(), many=True
                ).data
            )
            .decode("utf-8")
        )
        return r

    @cached_property
    def totais2(self):
        return getattr(self.analise2, "totais", None)

    @cached_property
    def totais2_por_produto_json(self):
        r = (
            LJSONRenderer()
            .render(
                TotaisPorProdutoSerializer(
                    self.analise2.totais_por_produto.all(), many=True
                ).data
            )
            .decode("utf-8")
        )
        return r

    @cached_property
    def totais2_json(self):
        r = (
            LJSONRenderer()
            .render(TotaisAnaliseEconomicaSerializer(self.totais2).data)
            .decode("utf-8")
        )
        return r


class AnaliseCompareSimView(RedirectView):

    def get_redirect_url(self, *args, **kwargs):
        analise1 = Analise.objects.get(
            ciclo_anual_referencia__agroecossistema_id=self.kwargs[
                "agroecossistema_id"
            ],
            ciclo_anual_referencia__ano=self.kwargs["ano_car"],
            ciclo_anual_referencia__sim_slug=self.kwargs["sim_slug"],
        )

        analise2 = Analise.objects.get(
            ciclo_anual_referencia__agroecossistema_id=self.kwargs[
                "agroecossistema_id"
            ],
            ciclo_anual_referencia__ano=self.kwargs["ano_car"],
            ciclo_anual_referencia__sim_slug__isnull=True,
        )

        return reverse(
            "agroecossistema:analise_compara",
            args=(
                self.kwargs["agroecossistema_id"],
                analise1.id,
                analise2.id,
            ),
        )


# Simulações
class SimulacaoListView(PermissionRequiredMixin, ListView):
    template_name = "agroecossistema/simulacao_list.html"
    model = CicloAnualReferencia
    permission_required = "agroecossistema.visualizar"

    def get_permission_object(self):
        agroecossistema = get_object_or_404(
            Agroecossistema, id=self.kwargs["agroecossistema_id"]
        )
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["modulo"] = "economica"
        context["submodulo"] = "simulacoes"
        context["agroecossistema"] = Agroecossistema.objects.get(
            id=self.kwargs["agroecossistema_id"]
        )
        context["car_label"] = _("Ciclos Anuais de Referência")  # para o breadcrumb
        context["ano_car"] = self.kwargs["ano_car"]
        context["car"] = CicloAnualReferencia.objects.filter(
            agroecossistema_id=self.kwargs["agroecossistema_id"],
            ano=self.kwargs["ano_car"],
            sim_slug__isnull=True,
        ).first()

        return context

    def get_queryset(self):
        return CicloAnualReferencia.objects.filter(
            agroecossistema_id=self.kwargs["agroecossistema_id"],
            ano=self.kwargs["ano_car"],
            sim_slug__isnull=False,
        )


class SimulacaoFormView:
    template_name = "agroecossistema/simulacao_form.html"
    model = CicloAnualReferencia
    permission_required = "permissao_padrao"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["titulo"] = self.titulo
        context["modulo"] = "economica"
        context["submodulo"] = "simulacoes"
        context["ano_car"] = self.kwargs["ano_car"]
        context["agroecossistema"] = Agroecossistema.objects.get(
            id=self.kwargs["agroecossistema_id"]
        )
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"agroecossistema_id": self.kwargs["agroecossistema_id"]})
        kwargs.update({"ano_car": self.kwargs["ano_car"]})
        return kwargs

    def form_invalid(self, form: BaseForm) -> HttpResponse:
        return super().form_invalid(form)


class SimulacaoCreateView(SimulacaoFormView, PermissionRequiredMixin, CreateView):
    titulo = _("Criar Simulação")
    form_class = SimulacaoForm

    def form_valid(self, form):
        if form.data is not None:
            pass

        try:
            ciclo_base = CicloAnualReferencia.objects.get(
                ano=self.kwargs["ano_car"],
                agroecossistema_id=self.kwargs["agroecossistema_id"],
                sim_slug__isnull=True,
            )
            novo_ciclo = ciclo_base.copia_profunda(
                tipo_copia=CicloAnualReferencia.COPIA_AE_COMPLETA,
                sim_nome=form.data.get("sim_nome"),
                sim_descricao=form.data.get("sim_descricao"),
                sim_subsistemas=form.cleaned_data.get("subsistemas"),
                sim_evento_gatilho=form.cleaned_data.get("sim_evento_gatilho"),
            )
            return HttpResponseRedirect(novo_ciclo.get_economica_analise_detail_url())

        except IntegrityError:
            form.full_clean()
            form._errors[NON_FIELD_ERRORS] = form.error_class(
                [
                    _(
                        "Não foi possível gravar esta simulação. O nome ou o evento gatilho podem estar sendo utilizados por outra simulação deste ciclo."
                    )
                ]
            )
            return self.render_to_response(self.get_context_data(form=form))


class SimulacaoUpdateView(SimulacaoFormView, PermissionRequiredMixin, UpdateView):
    titulo = _("Atualizar Simulação")
    form_class = SimulacaoUpdateForm

    def get_success_url(self):
        return CicloAnualReferencia.objects.get(pk=self.kwargs["pk"]).get_absolute_url()


class DiagramaDeFluxoListView(FormMixin, ListView):
    model = DiagramasDeFluxo
    template_name = 'agroecossistema/diagrama_de_fluxo_list.html'
    form_class = DiagramaDeFluxoForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'can_delete': self.request.user.has_perm('permissao_padrao')
        })
        if 'agroecossistema_id' in self.kwargs:
            kwargs['agroecossistema_id'] = self.kwargs['agroecossistema_id']
        if 'data' in kwargs and 'update_pk' in kwargs['data'] and kwargs['data']['update_pk'] != '':
            kwargs['instance'] = Anexo.objects.get(pk=int(kwargs['data']['update_pk']))

        return kwargs

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        anexo = form.save(commit=False)

        anexo.agroecossistema=Agroecossistema.objects.get(pk=self.kwargs['agroecossistema_id'])

        arquivo = form.cleaned_data['arquivo']
        if arquivo.__class__.__name__ == 'InMemoryUploadedFile':
            anexo.mimetype = arquivo.content_type
            if anexo.nome is None or anexo.nome == "":
                anexo.nome = os.path.splitext(arquivo.name)[0]

        anexo.save()

        if 'related_type' in self.kwargs:
            related_type = ContentType.objects.get(app_label=self.kwargs['related_type'], model=self.kwargs['related_model'].lower())
            anexo_recipiente, created = AnexoRecipiente.objects.get_or_create(
                recipiente_object_id=self.kwargs['pk'],
                anexo_id=anexo.id,
                recipiente_content_type_id=related_type.id
            )
            return HttpResponseRedirect(reverse(
                f"{self.kwargs['related_type']}:{self.kwargs['related_model'].lower()}_anexos",
                args=(self.kwargs['agroecossistema_id'],self.kwargs['pk'],)
            ))

        return HttpResponseRedirect(
            reverse('anexos:anexo_list', args=(self.kwargs['agroecossistema_id'],))
        )

    def get_queryset(self):
        relatedType = None
        if 'related_type' in self.kwargs:
            relatedType = self.kwargs['related_type']

        if not relatedType:
            contentType = ContentType.objects.get(app_label='agroecossistema', model='agroecossistema')
            if 'filter' in self.kwargs:
                filter = self.kwargs['filter']
                anexosAll = Anexo.objects.filter(agroecossistema_id=self.kwargs['agroecossistema_id']).order_by('-ultima_alteracao')
                anexos = []
                for anexo in anexosAll:
                    if filter == 'imagens':
                        if anexo.is_image():
                            anexos.append(anexo)
                        continue

                    if filter == 'videos':
                        if anexo.is_video():
                            anexos.append(anexo)
                        continue

                    if not anexo.is_video() and not anexo.is_image():
                        anexos.append(anexo)

            elif 'pastaId' in self.kwargs:
                pastaId = self.kwargs['pastaId']
                anexos = DiagramasDeFluxo.objects.filter(agroecossistema_id=self.kwargs['agroecossistema_id'], pasta_id=pastaId).order_by('-id')

            else:
                anexos = DiagramasDeFluxo.objects.filter(agroecossistema_id=self.kwargs['agroecossistema_id']).order_by('-id')

            return anexos

        # is related content!
        anexosRecipientesList = AnexoRecipiente.objects.filter(recipiente_object_id=self.kwargs['pk']).order_by('-ultima_alteracao').values_list('anexo_id')
        return Anexo.objects.filter(pk__in=anexosRecipientesList)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        relatedType = None
        if 'related_type' in self.kwargs:
            relatedType = self.kwargs['related_type']
        comPastas = True

        if 'agroecossistema_id' in self.kwargs:
            context['agroecossistema'] = Agroecossistema.objects.get(id=self.kwargs['agroecossistema_id'])
        context['modo'] = 'grade' if not ('modo' in self.request.GET) else self.request.GET['modo']
        context['modulo'] = relatedType if relatedType else 'anexos'
        context['isPasta'] = False
        context['titulo'] = _('Anexos')
        context['anexosPlural'] = _('arquivos')
        context['anexosSingular'] = _('arquivo')

        if not relatedType:
            context['submodulo'] = 'todos'
            context['anexosUrlList'] = reverse("anexos:anexo_list", args=(self.kwargs['agroecossistema_id'],))
            context['anexosUrlCreate'] = reverse("anexos:anexo_create", args=(self.kwargs['agroecossistema_id'],))
            if 'filter' in self.kwargs:
                filter = self.kwargs['filter']
                context['submodulo'] = filter
                if filter == 'imagens':
                    context['titulo'] = _("Imagens")
                    context['anexosPlural'] = _("imagens")
                    context['anexosSingular'] = _("imagem")
                elif filter == 'videos':
                    context['titulo'] = _("Vídeos")
                    context['anexosPlural'] = _("vídeos")
                    context['anexosSingular'] = _("vídeo")
                elif filter == 'documentos':
                    context['titulo'] = _("Documentos")
                    context['anexosPlural'] = _("documentos")
                    context['anexosSingular'] = _("documento")

            elif 'pastaId' in self.kwargs:
                pasta = PastaAnexo.objects.get(pk=self.kwargs['pastaId'])
                context['isPasta'] = True
                context['pasta'] = pasta
                context['submodulo'] = pasta.nome
                context['titulo'] = pasta.nome

        else:
            module = __import__(relatedType)
            class_ = getattr(module.models, self.kwargs['related_model'])
            related_object = class_.objects.get(pk=self.kwargs['pk'])
            content_type = ContentType.objects.get(app_label=relatedType, model=self.kwargs['related_model'].lower())

            context['titulo'] = f"{self.kwargs['related_model']}: {related_object.__str__()}"
            context['submodulo'] = 'anexosTodos'
            context['related_id'] = related_object.id
            context['related_type'] = content_type.id
            context['relatedUrl'] = reverse(
                f"{relatedType}:{self.kwargs['related_model'].lower()}_list",
                args=(self.kwargs['agroecossistema_id'],)
            ) if 'related_list_url' not in self.kwargs else reverse(
                self.kwargs['related_list_url'],
                args=(self.kwargs['pk'],)
            )
            # context['anexosUrlCreate'] = reverse(
            #     f"{relatedType}:{self.kwargs['related_model'].lower()}_anexos_create",
            #     args=(
            #         self.kwargs['agroecossistema_id'],
            #         self.kwargs['pk'],
            #     )
            # )

        agroecossistema_id = self.kwargs['agroecossistema_id'] if 'agroecossistema_id' in self.kwargs else None
        # context['anexos'] = organiza_anexos(self.object_list, agroecossistema_id, context['isPasta'])
        context['anexos'] = organiza_anexos(self.object_list, agroecossistema_id, True)

        return context


class DiagramaDeFluxoCreateView(PermissionRequiredMixin, AjaxableResponseMixin, CreateView):
    model = DiagramasDeFluxo
    template_name = "agroecossistema/painel_diagramadefluxo_form.html"
    form_class = DiagramaDeFluxoForm
    titulo = _("Criar diagrama de fluxo")
    permission_required = "agroecossistema.alterar"

    def get_initial(self):
        initial = self.initial.copy()
        initial['agroecossistema'] = self.kwargs["agroecossistema_id"]
        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["agroecossistema_id"] = self.kwargs["agroecossistema_id"]
        return context


class DiagramaDeFluxoUpdateView(PermissionRequiredMixin, AjaxableResponseMixin, UpdateView):
    model = DiagramasDeFluxo
    template_name = "agroecossistema/painel_diagramadefluxo_form.html"
    form_class = DiagramaDeFluxoForm
    titulo = _("Editar diagrama de fluxo")
    permission_required = "agroecossistema.alterar"


class ProjecaoDeFuturoListView(PermissionRequiredMixin, ListView):
    model = ProjecaoDeFuturo
    permission_required = "agroecossistema.visualizar"

    def get_permission_object(self):
        agroecossistema = get_object_or_404(
            Agroecossistema, id=self.kwargs["agroecossistema_id"]
        )
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["modulo"] = "main"
        context["submodulo"] = "lista"
        context["agroecossistema"] = Agroecossistema.objects.get(
            id=self.kwargs["agroecossistema_id"]
        )
        context["ano_car"] = self.kwargs["ano_car"]
        return context

    def get_queryset(self):
        car = CicloAnualReferencia.objects.get(
            agroecossistema_id=self.kwargs["agroecossistema_id"],
            ano=self.kwargs["ano_car"],
            sim_slug__isnull=True,
        )
        return ProjecaoDeFuturo.objects.filter(
            ciclo_anual_referencia=car
        ).order_by('-id')


class ProjecaoDeFuturoUpdateView(PermissionRequiredMixin, AjaxableResponseMixin, UpdateView):
    model = ProjecaoDeFuturo
    template_name = "agroecossistema/painel_projecaodefuturo_form.html"
    form_class = ProjecaoDeFuturoForm
    titulo = _("Editar projeção de futuro")
    permission_required = "agroecossistema.alterar"


class ProjecaoDeFuturoCreateView(PermissionRequiredMixin, AjaxableResponseMixin, CreateView):
    model = ProjecaoDeFuturo
    template_name = "agroecossistema/painel_projecaodefuturo_form.html"
    form_class = ProjecaoDeFuturoForm
    titulo = _("Criar projeção de futuro")
    permission_required = "agroecossistema.alterar"

    def get_initial(self):
        initial = self.initial.copy()
        car = CicloAnualReferencia.objects.get(
            agroecossistema_id=self.kwargs["agroecossistema_id"],
            ano=self.kwargs["ano_car"],
            sim_slug__isnull=True,
        )
        initial['ciclo_anual_referencia'] = car.id
        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["agroecossistema_id"] = self.kwargs["agroecossistema_id"]
        context["ano_car"] = self.kwargs["ano_car"]
        return context
