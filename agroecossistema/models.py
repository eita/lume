from copy import deepcopy
from datetime import date

from django.conf import settings
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.contrib.gis.db import models
from django.db import transaction
from django.contrib.postgres.fields import JSONField
from django.db.models import Sum, Q
from django.db.models.constraints import UniqueConstraint
from django.db.models.fields.related import ForeignKey
from django.db.models import Min, Max
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.text import slugify

from django.utils.translation import gettext as _
from django.utils.translation import gettext_lazy as _lazy
from main.utils import get_name_initials

from taggit.managers import TaggableManager

from agroecossistema.utils import nome_car
from lume.models import LumeModel
from main.models import Organizacao, Territorio, Comunidade


class Agroecossistema(LumeModel):
    TIPOS_GESTAO_NSGA = (("F", _("Gestão familiar")), ("C", _("Gestão comunitária")))
    MESES_ANO = (
        (1, _("Janeiro")),
        (2, _("Fevereiro")),
        (3, _("Março")),
        (4, _("Abril")),
        (5, _("Maio")),
        (6, _("Junho")),
        (7, _("Julho")),
        (8, _("Agosto")),
        (9, _("Setembro")),
        (10, _("Outubro")),
        (11, _("Novembro")),
        (12, _("Dezembro")),
    )
    def agroecossistema_upload_path(instance, filename):
        return 'agroecossistema_{0}/{1}'.format(instance.id, filename)
    imagem = models.ImageField(_lazy("imagem"), blank=True, null=True,
                               upload_to=agroecossistema_upload_path)
    autor = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="agroecossistemas",
        verbose_name=_lazy("autor"),
    )
    comunidade = models.ForeignKey(
        Comunidade,
        models.PROTECT,
        related_name="agroecossistemas",
        verbose_name=_lazy("comunidade"),
    )
    geoponto = models.PointField(blank=True, null=True, verbose_name=_lazy("geoponto"))
    poligono = models.PolygonField(
        blank=True, null=True, verbose_name=_lazy("poligono")
    )
    nsga = models.CharField(max_length=80, null=True, verbose_name=_lazy("nsga"))
    tipo_gestao_nsga = models.CharField(
        max_length=1,
        choices=TIPOS_GESTAO_NSGA,
        default="F",
        verbose_name=_lazy("tipo de gestão nsga"),
    )
    organizacao = models.ForeignKey(
        Organizacao,
        models.CASCADE,
        null=True,
        related_name="agroecossistemas",
        verbose_name=_lazy("organização"),
    )  # TODO notnull
    territorio = models.ForeignKey(
        Territorio,
        models.PROTECT,
        null=True,
        blank=True,
        related_name="agroecossistemas",
        verbose_name=_lazy("territorio"),
    )  # TODO notnull
    # TODO desconsinuar este campo abaixo pois passou para Analise (qualitativa)
    parametros_inativos = JSONField(
        default=dict, blank=True, verbose_name=_lazy("parâmetros inativos")
    )
    mes_inicio_ciclo = models.IntegerField(
        choices=MESES_ANO, default=1, verbose_name=_lazy("mês inicial do ciclo")
    )
    mes_fim_ciclo = models.IntegerField(
        choices=MESES_ANO, default=12, verbose_name=_lazy("mês final do ciclo")
    )
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("última alteração")
    )
    visualizacao_publica = models.BooleanField(
        blank=True, null=True, default=False, verbose_name=_lazy("visualização pública")
    )

    tags = TaggableManager(
        verbose_name=_lazy("marcadores"),
        help_text=_lazy("Uma lista de marcadores separados por vírgula."),
        blank=True,
    )

    latest_car = None
    description = None

    class Meta:
        db_table = "agroecossistema"
        ordering = ("nsga",)

    def __str__(self):
        return self.nsga

    def get_initials(self):
        return get_name_initials(self.nsga)

    def get_absolute_url(self):
        return reverse("agroecossistema_detail", args=(self.pk,))

    def get_update_url(self):
        return reverse("agroecossistema_update", args=(self.pk,))

    def get_anexos_url(self):
        return reverse("anexos:anexo_list", args=(self.pk,))

    def get_latest_car(self):
        if self.latest_car is None:
            self.latest_car = self.cars.order_by("-ano").first()
        return self.latest_car

    def get_car_list_url(self, ano_car=None, sim_slug=None):
        if sim_slug:
            return reverse(
                "agroecossistema:sim_list",
                args=(
                    self.pk,
                    ano_car,
                ),
            )
        else:
            return reverse("agroecossistema:car_list", args=(self.pk,))

    def get_description(self):
        if self.description is None:
            self.description = _(
                "O agroecossistema %(nsga)s está localizado na comunidade %(comunidade)s"
            ) % {"nsga": self.nsga, "comunidade": self.comunidade.nome}
            if self.territorio is not None:
                self.description += " (do território %(territorio)s)" % {
                    "territorio": self.territorio.nome
                }
            self.description += (
                ". A organização responsável pelo cadastro é a %(organizacao)s."
                % {"organizacao": self.organizacao.nome}
            )

        return self.description

    def getYearsSpan(self):
        min_year = self.eventos.aggregate(min_year=Min("data_inicio"))
        max_year = self.eventos.aggregate(max_year=Max("data_inicio"))
        max_year_2 = self.eventos.aggregate(max_year=Max("data_fim"))

        min_year = min_year["min_year"]
        if max_year["max_year"] is not None:
            if max_year_2["max_year"] is not None:
                max_year = (
                    max_year_2["max_year"]
                    if max_year_2["max_year"] > max_year["max_year"]
                    else max_year["max_year"]
                )
            else:
                max_year = max_year["max_year"]

        years = None
        if min_year is not None and max_year is not None:
            if min_year == max_year:
                years = str(min_year)
            else:
                years = str(min_year) + "-" + str(max_year)

        return years

    def tem_multiplas_analises_economicas(self):
        from economica.models import Analise

        return (
            Analise.objects.filter(
                ciclo_anual_referencia__agroecossistema_id=self.id,
                ciclo_anual_referencia__sim_slug__isnull=True,
            ).count()
            > 1
        )

    def build_session(agroecossistema_id, ano_car, sim_slug=None, subsistema_id=None):
        built_session = {
            "session": {
                "ano_car": None,
                "nome_car": None,
                "subsistema_id": None,
                "sim_nome": None,
                "sim_slug": None,
            },
            "car": None,
        }
        if not agroecossistema_id or not ano_car:
            return built_session

        try:
            get_args = {
                "agroecossistema_id": agroecossistema_id,
                "ano": ano_car,
            }

            if sim_slug:
                get_args["sim_slug"] = sim_slug
                built_session["session"]["sim_slug"] = sim_slug
            else:
                get_args["sim_slug__isnull"] = True

            car = CicloAnualReferencia.objects.get(**get_args)

            built_session["car"] = car
            built_session["session"]["ano_car"] = ano_car
            built_session["session"]["nome_car"] = car.__str__()
            built_session["session"]["sim_nome"] = car.sim_nome
            built_session["session"]["subsistema_id"] = subsistema_id
        except (KeyError, CicloAnualReferencia.DoesNotExist):
            return built_session

        return built_session


class CicloAnualReferencia(LumeModel):
    # copia terras, composicao (valores UTF zeram), subsistemas (vazios) e itens de patrimonio (com valores 0)
    COPIA_AE_ESTRUTURA = "ae_estrutura"

    # cria um novo CAR em sequencia a um antigo: copia terras, composicao (valores UTF zeram), analise economica:
    # subsistemas (vazios), estoques e patrimonios (valores e quantidades finais do antigo se tornam iniciais no novo)
    COPIA_CAR_EM_SEQUENCIA = "car_em_sequencia"

    # faz uma copia total de tudo abaixo deste CAR
    COPIA_AE_COMPLETA = "ae_completa"

    # copia terras, composicao (valores UTF zeram), não copia análise econômica
    COPIA_APENAS_TERRAS_E_COMPOSICAO = "apenas_terras_e_composicao"

    CHOICES_HELP_TEXTS = {
        COPIA_AE_ESTRUTURA: "Terras, composição do NSGA (sem UTF) e itens de estoque e patrimônio da análise econômica serão copiados de outro ano, com valores zerados",
        COPIA_CAR_EM_SEQUENCIA: _(
            "Terras, composição do NSGA (sem UTF) e itens de estoques e patrimônio da análise econômica serão copiados de outro ano, com valor e quantidade iniciais iguais às quantidades e valores finais do ano escolhido"
        ),
        COPIA_AE_COMPLETA: _("Copia todas as informações de um outro ano"),
        COPIA_APENAS_TERRAS_E_COMPOSICAO: _(
            "Apenas as terras e composição do NSGA (sem UTF) serão copiadas de um outro ano. Não copia a análise econômica"
        ),
    }

    # Fields
    ano = models.IntegerField(verbose_name=_lazy("ano"))
    data_coleta = models.DateTimeField(
        blank=True, null=True, verbose_name=_lazy("data da coleta")
    )
    sim_slug = models.SlugField(blank=True, null=True, max_length=100)
    sim_nome = models.CharField(
        blank=True, null=True, max_length=120, verbose_name=_lazy("nome da simulação")
    )
    sim_descricao = models.TextField(
        blank=True, null=True, verbose_name=_lazy("descrição da simulação")
    )
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("última alteração")
    )

    # Relationship Fields
    agroecossistema = ForeignKey(
        Agroecossistema,
        on_delete=models.CASCADE,
        related_name="cars",
        verbose_name=_lazy("agroecossistema"),
    )

    sim_evento_gatilho = ForeignKey(
        "linhadotempo.EventoGatilho",
        on_delete=models.CASCADE,
        related_name="simulacoes",
        null=True,
        blank=True,
        verbose_name=_lazy("evento gatilho da simulação"),
    )

    area_total = None
    area_terras_proprias = None
    qtde_terras_proprias = None
    area_terras_outros = None
    qtde_terras_outros = None
    area_terras_comunitarias = None
    qtde_terras_comunitarias = None
    area_terras_posse = None
    qtde_terras_posse = None
    area_terras_arrendamento = None
    qtde_terras_arrendamento = None
    area_terras_meacao = None
    qtde_terras_meacao = None
    area_terras_parceria = None
    qtde_terras_parceria = None
    area_terras_cessao = None
    qtde_terras_cessao = None
    area_terras_comodato = None
    qtde_terras_comodato = None
    area_terras_direitodeuso = None
    qtde_terras_direitodeuso = None
    area_terras_usocomunitario = None
    qtde_terras_usocomunitario = None

    class Meta:
        ordering = ("ano",)
        unique_together = (
            ("agroecossistema", "ano", "sim_slug"),
            ("agroecossistema", "ano", "sim_evento_gatilho"),
        )
        constraints = [
            UniqueConstraint(
                fields=["agroecossistema", "ano", "sim_slug"],
                name="unique_com_sim_slug",
            ),
            UniqueConstraint(
                fields=["agroecossistema", "ano", "deleted"],
                condition=Q(sim_slug__isnull=True),
                name="unique_sem_sim_slug",
            ),
        ]

    def __str__(self):
        agroecossistema = self.agroecossistema
        nc = nome_car(
            self.ano, agroecossistema.mes_inicio_ciclo, agroecossistema.mes_fim_ciclo
        )
        if self.sim_slug is not None:
            nc = _("Simulação “%(sim_nome)s”, do Ciclo %(nc)s") % {
                "sim_nome": self.sim_nome,
                "nc": nc,
            }
        return nc

    @property
    def titulo_base(self):
        if self.sim_slug is not None:
            agroecossistema = self.agroecossistema
            return nome_car(
                self.ano,
                agroecossistema.mes_inicio_ciclo,
                agroecossistema.mes_fim_ciclo,
            )
        else:
            return self.__str__()

    def __unicode__(self):
        return "%s" % self.pk

    @cached_property
    def analise_economica(self):
        return self.analises_economicas.first()

    def get_absolute_url(self):
        if self.sim_slug is None:
            return reverse(
                "agroecossistema_detail_car", args=(self.agroecossistema_id, self.ano)
            )
        else:
            return reverse(
                "agroecossistema_detail_sim",
                args=(self.agroecossistema_id, self.ano, self.sim_slug),
            )

    def get_update_url(self):
        if self.sim_slug is None:
            return reverse(
                "agroecossistema_update_car", args=(self.agroecossistema_id, self.ano)
            )
        else:
            return reverse(
                "agroecossistema:sim_update",
                args=(self.agroecossistema_id, self.ano, self.sim_slug),
            )

    def get_subsistema_list_url(self):
        if self.sim_slug is None:
            return reverse(
                "economica:subsistema_list", args=(self.agroecossistema_id, self.ano)
            )
        else:
            return reverse(
                "ecosim:subsistema_list",
                args=(self.agroecossistema_id, self.ano, self.sim_slug),
            )

    def get_subsistema_vazio_url(self):
        if self.sim_slug is None:
            return reverse(
                "economica:subsistema_vazio", args=(self.agroecossistema_id, self.ano)
            )
        else:
            return reverse(
                "ecosim:subsistema_vazio",
                args=(self.agroecossistema_id, self.ano, self.sim_slug),
            )

    def get_economica_analise_detail_url(self):
        if self.sim_slug is None:
            return reverse(
                "economica:analise_detail", args=(self.agroecossistema_id, self.ano)
            )
        else:
            return reverse(
                "ecosim:analise_detail",
                args=(self.agroecossistema_id, self.ano, self.sim_slug),
            )

    def get_area_total(self):
        if self.area_total is None:
            area_total = self.terras.all().aggregate(Sum("area_territorio"))
            self.area_total = area_total["area_territorio__sum"]
        return self.area_total

    def get_area(self, tipo):
        area = self.terras.filter(tipo=tipo).aggregate(Sum("area_territorio"))
        return area["area_territorio__sum"]

    def get_qtde(self, tipo):
        qtde = self.terras.filter(tipo=tipo).count()
        return qtde

    def get_area_terras_proprias(self):
        if self.area_terras_proprias is None:
            proprias = self.terras.filter(tipo="P").aggregate(Sum("area_territorio"))
            self.area_terras_proprias = proprias["area_territorio__sum"]
        return self.area_terras_proprias

    def get_qtde_terras_proprias(self):
        if self.qtde_terras_proprias is None:
            self.qtde_terras_proprias = self.terras.filter(tipo="P").count()
        return self.qtde_terras_proprias

    def get_area_terras_outros(self):
        if self.area_terras_outros is None:
            terceiros = self.terras.filter(tipo="T").aggregate(Sum("area_territorio"))
            self.area_terras_outros = terceiros["area_territorio__sum"]
        return self.area_terras_outros

    def get_qtde_terras_outros(self):
        if self.qtde_terras_outros is None:
            self.qtde_terras_outros = self.terras.filter(tipo="T").count()
        return self.qtde_terras_outros

    def get_area_terras_comunitarias(self):
        if self.area_terras_comunitarias is None:
            comunitarias = self.terras.filter(tipo="C").aggregate(
                Sum("area_territorio")
            )
            self.area_terras_comunitarias = comunitarias["area_territorio__sum"]
        return self.area_terras_comunitarias

    def get_qtde_terras_comunitarias(self):
        if self.qtde_terras_comunitarias is None:
            self.qtde_terras_comunitarias = self.terras.filter(tipo="C").count()
        return self.qtde_terras_comunitarias

    def get_area_terras_posse(self):
        if self.area_terras_posse is None:
            posse = self.terras.filter(tipo="Po").aggregate(Sum("area_territorio"))
            self.area_terras_posse = posse["area_territorio__sum"]
        return self.area_terras_posse

    def get_qtde_terras_posse(self):
        if self.qtde_terras_posse is None:
            self.qtde_terras_posse = self.terras.filter(tipo="Po").count()
        return self.qtde_terras_posse

    def get_area_terras_arrendamento(self):
        if self.area_terras_arrendamento is None:
            arrendamento = self.terras.filter(tipo="A").aggregate(
                Sum("area_territorio")
            )
            self.area_terras_arrendamento = arrendamento["area_territorio__sum"]
        return self.area_terras_arrendamento

    def get_qtde_terras_arrendamento(self):
        if self.qtde_terras_arrendamento is None:
            self.qtde_terras_arrendamento = self.terras.filter(tipo="A").count()
        return self.qtde_terras_arrendamento

    def get_area_terras_meacao(self):
        if self.area_terras_meacao is None:
            meacao = self.terras.filter(tipo="M").aggregate(Sum("area_territorio"))
            self.area_terras_meacao = meacao["area_territorio__sum"]
        return self.area_terras_meacao

    def get_qtde_terras_meacao(self):
        if self.qtde_terras_meacao is None:
            self.qtde_terras_meacao = self.terras.filter(tipo="M").count()
        return self.qtde_terras_meacao

    def get_area_terras_parceria(self):
        if self.area_terras_parceria is None:
            parceria = self.terras.filter(tipo="Pa").aggregate(Sum("area_territorio"))
            self.area_terras_parceria = parceria["area_territorio__sum"]
        return self.area_terras_parceria

    def get_qtde_terras_parceria(self):
        if self.qtde_terras_parceria is None:
            self.qtde_terras_parceria = self.terras.filter(tipo="Pa").count()
        return self.qtde_terras_parceria

    def get_area_terras_cessao(self):
        if self.area_terras_cessao is None:
            cessao = self.terras.filter(tipo="Ce").aggregate(Sum("area_territorio"))
            self.area_terras_cessao = cessao["area_territorio__sum"]
        return self.area_terras_cessao

    def get_qtde_terras_cessao(self):
        if self.qtde_terras_cessao is None:
            self.qtde_terras_cessao = self.terras.filter(tipo="Ce").count()
        return self.qtde_terras_cessao

    def get_area_terras_comodato(self):
        if self.area_terras_comodato is None:
            comodato = self.terras.filter(tipo="Co").aggregate(Sum("area_territorio"))
            self.area_terras_comodato = comodato["area_territorio__sum"]
        return self.area_terras_comodato

    def get_qtde_terras_comodato(self):
        if self.qtde_terras_comodato is None:
            self.qtde_terras_comodato = self.terras.filter(tipo="Co").count()
        return self.qtde_terras_comodato

    def get_area_terras_direitodeuso(self):
        if self.area_terras_direitodeuso is None:
            direitodeuso = self.terras.filter(tipo="D").aggregate(
                Sum("area_territorio")
            )
            self.area_terras_direitodeuso = direitodeuso["area_territorio__sum"]
        return self.area_terras_direitodeuso

    def get_qtde_terras_direitodeuso(self):
        if self.qtde_terras_direitodeuso is None:
            self.qtde_terras_direitodeuso = self.terras.filter(tipo="D").count()
        return self.qtde_terras_direitodeuso

    def get_area_terras_usocomunitario(self):
        if self.area_terras_usocomunitario is None:
            usocomunitario = self.terras.filter(tipo="U").aggregate(
                Sum("area_territorio")
            )
            self.area_terras_usocomunitario = usocomunitario["area_territorio__sum"]
        return self.area_terras_usocomunitario

    def get_qtde_terras_usocomunitario(self):
        if self.qtde_terras_usocomunitario is None:
            self.qtde_terras_usocomunitario = self.terras.filter(tipo="U").count()
        return self.qtde_terras_usocomunitario

    # https://books.agiliq.com/projects/django-orm-cookbook/en/latest/copy.html
    # https://stackoverflow.com/a/8924679/1216027
    @transaction.atomic
    def copia_profunda(
        self,
        ano=None,
        tipo_copia=COPIA_AE_ESTRUTURA,
        sim_nome=None,
        sim_descricao=None,
        sim_subsistemas=None,
        sim_evento_gatilho=None,
    ):
        novo_car = deepcopy(self)
        novo_car.id = None
        if ano:
            novo_car.ano = ano
        if sim_nome:
            novo_car.sim_nome = sim_nome
        if sim_descricao:
            novo_car.sim_descricao = sim_descricao
        if sim_evento_gatilho:
            novo_car.sim_evento_gatilho = sim_evento_gatilho
        novo_car.save()

        # composicoes
        composicoes = self.composicoes_nsga.all()
        for composicao in composicoes:
            composicao.copia_profunda(novo_car, tipo_copia)

        # terras
        terras = self.terras.all()
        for terra in terras:
            terra.copia_profunda(novo_car)

        # analise economica
        if tipo_copia != self.COPIA_APENAS_TERRAS_E_COMPOSICAO:
            analises = self.analises_economicas.all()
            for analise in analises:
                analise.copia_profunda(
                    novo_car, tipo_copia, sim_subsistemas=sim_subsistemas
                )

        return novo_car

    def get_delete_ajax_url(self):
        if self.sim_slug is not None:
            url = reverse(
                "agroecossistema:sim_delete",
                kwargs={
                    "agroecossistema_id": self.agroecossistema_id,
                    "ano_car": self.ano,
                    "sim_slug": self.sim_slug,
                },
            )
        else:
            url = reverse(
                "agroecossistema:car_delete",
                kwargs={
                    "agroecossistema_id": self.agroecossistema_id,
                    "ano_car": self.ano,
                },
            )

        return url

    def save(self, *args, **kwargs):
        if self.sim_nome is not None:
            self.sim_slug = "simulacao_" + slugify(self.sim_nome)

        # update ultima_alteracao of agroecossistema:
        self.agroecossistema.save()

        super(CicloAnualReferencia, self).save(*args, **kwargs)


class Pessoa(models.Model):
    idade_minima_ECA = 14
    norma_ECA = _lazy(
        f"O estatuto da criança e do adolescente veda a participação de menores de {idade_minima_ECA} anos em qualquer tipo de trabalho"
    )

    SEXO_OPCOES = (("M", _("Homem")), ("F", _("Mulher")))
    # Fields
    nome = models.CharField(max_length=100, verbose_name=_lazy("nome"))
    sexo = models.CharField(
        max_length=1,
        choices=SEXO_OPCOES,
        default=None,
        blank=False,
        verbose_name=_lazy("sexo"),
    )
    data_nascimento = models.DateField(verbose_name=_lazy("data de nascimento"))
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("última alteração")
    )

    # Relationship Fields
    agroecossistema = ForeignKey(
        Agroecossistema,
        on_delete=models.CASCADE,
        related_name="pessoas",
        verbose_name=_lazy("agroecossistema"),
    )

    class Meta:
        ordering = ("-criacao",)
        unique_together = (("nome", "agroecossistema"),)

    def __str__(self):
        return self.nome

    def __unicode__(self):
        return "%s" % self.pk

    def get_absolute_url(self):
        return reverse(
            "agroecossistema:pessoa_detail", args=(self.agroecossistema_id, self.pk)
        )

    def get_update_url(self):
        return reverse(
            "agroecossistema:pessoa_update", args=(self.agroecossistema_id, self.pk)
        )

    def get_idade(self):
        today = date.today()
        return (
            today.year
            - self.data_nascimento.year
            - (
                (today.month, today.day)
                < (self.data_nascimento.month, self.data_nascimento.day)
            )
        )

    def get_sexo(self):
        for opcao in self.SEXO_OPCOES:
            if opcao[0] == self.sexo:
                opcaoEscolhida = opcao
                break

        return opcaoEscolhida[1]

    def permitida_pelo_ECA(self):
        return self.get_idade() >= self.idade_minima_ECA

    def save(self, *args, **kwargs):
        # update ultima_alteracao of agroecossistema:
        self.agroecossistema.save()
        super(Pessoa, self).save(*args, **kwargs)


class ComposicaoNsga(models.Model):
    GRAU_DE_PARENTESCO = (
        ("E", _("Esposa/o")),
        ("F", _("Filha/o")),
        ("Pr", _("Prima/o")),
        ("I", _("Irmã/ão")),
        ("M", _("Mãe")),
        ("P", _("Pai")),
        ("A", _("Avó/ô")),
        ("T", _("Tia/o")),
        ("G", _("Agregada/o")),
        ("N", _("Neta/o")),
        ("O", _("Outros")),
    )

    # Fields
    responsavel = models.BooleanField(default=False, verbose_name=_lazy("responsavel"))
    mora_no_agroecossistema = models.BooleanField(
        default=True, verbose_name=_lazy("mora no agroecossistema")
    )
    grau_de_parentesco = models.CharField(
        max_length=2,
        choices=GRAU_DE_PARENTESCO,
        default="O",
        verbose_name=_lazy("grau de parentesco"),
    )
    observacoes = models.TextField(
        null=True, blank=True, verbose_name=_lazy("observações")
    )
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("última alteração")
    )

    # Relationship Fields
    ciclo_anual_referencia = ForeignKey(
        CicloAnualReferencia,
        verbose_name=_lazy("ciclo anual de referência"),
        on_delete=models.CASCADE,
        related_name="composicoes_nsga",
    )
    pessoa = ForeignKey(
        Pessoa,
        verbose_name=_lazy("pessoa"),
        on_delete=models.CASCADE,
        related_name="composicoes_nsga",
    )

    class Meta:
        ordering = ("-responsavel", "pessoa__nome")
        unique_together = (("ciclo_anual_referencia", "pessoa"),)

    def __unicode__(self):
        return "%s" % self.pk

    def __str__(self):
        str = self.pessoa.nome
        if self.responsavel:
            str += "*"
        return str

    def get_absolute_url(self):
        return reverse(
            "agroecossistema:composicaonsga_detail",
            args=(self.ciclo_anual_referencia.agroecossistema_id, self.pk),
        )

    def get_update_url(self):
        return reverse(
            "agroecossistema:composicaonsga_update",
            args=(self.ciclo_anual_referencia.agroecossistema_id, self.pk),
        )

    def copia_profunda(self, novo_car, tipo_copia):
        nova_composicao = deepcopy(self)
        nova_composicao.id = None
        nova_composicao.ciclo_anual_referencia = novo_car
        nova_composicao.save()

    def get_parentesco(self):
        for opcao in self.GRAU_DE_PARENTESCO:
            if opcao[0] == self.grau_de_parentesco:
                opcaoEscolhida = opcao
                break

        return opcaoEscolhida[1]

    def save(self, *args, **kwargs):
        # update ultima_alteracao of agroecossistema:
        self.ciclo_anual_referencia.agroecossistema.save()
        super(ComposicaoNsga, self).save(*args, **kwargs)


class Terra(models.Model):
    TIPOS_TERRA = (
        ("P", _("Própria")),
        ("T", _("Outros")),
        ("C", _("Comunitária")),
        ("Po", _("Posse")),
        ("A", _("Arrendamento")),
        ("M", _("Meação")),
        ("Pa", _("Parceria")),
        ("Ce", _("Cessão")),
        ("Co", _("Comodato")),
        ("D", _("Direito de Uso")),
        ("U", _("Uso Comunitário")),
    )
    # Fields
    poligono = models.PolygonField(
        blank=True, null=True, verbose_name=_lazy("poligono")
    )
    area_territorio = models.DecimalField(
        max_digits=12, decimal_places=4, verbose_name=_lazy("área do território")
    )
    tipo = models.CharField(
        max_length=2, choices=TIPOS_TERRA, default="P", verbose_name=_lazy("tipo")
    )
    observacoes = models.TextField(
        null=True, blank=True, verbose_name=_lazy("observações")
    )
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("última alteração")
    )

    # Relationship Fields
    ciclo_anual_referencia = ForeignKey(
        CicloAnualReferencia,
        verbose_name=_lazy("ciclo anual de referência"),
        on_delete=models.CASCADE,
        related_name="terras",
    )

    class Meta:
        ordering = (
            "tipo",
            "-area_territorio",
        )

    def __unicode__(self):
        return "%s" % self.pk

    def get_absolute_url(self):
        return reverse(
            "agroecossistema:terra_detail",
            args=(self.ciclo_anual_referencia.agroecossistema_id, self.pk),
        )

    def get_update_url(self):
        return reverse(
            "agroecossistema:terra_update",
            args=(self.ciclo_anual_referencia.agroecossistema_id, self.pk),
        )

    def copia_profunda(self, novo_car):
        nova_terra = deepcopy(self)
        nova_terra.id = None
        nova_terra.ciclo_anual_referencia = novo_car
        nova_terra.save()

    def save(self, *args, **kwargs):
        # update ultima_alteracao of agroecossistema:
        self.ciclo_anual_referencia.agroecossistema.save()
        super(Terra, self).save(*args, **kwargs)


class ProjecaoDeFuturo(models.Model):
    fragilidades = models.TextField(
        null=True, blank=True, verbose_name=_lazy("Fragilidades")
    )
    potencialidades = models.TextField(
        null=True, blank=True, verbose_name=_lazy("Potencialidades")
    )
    projecao_de_futuro = models.TextField(
        null=True, blank=True, verbose_name=_lazy("Projeção de Futuro")
    )
    # Relationship Fields
    ciclo_anual_referencia = ForeignKey(
        CicloAnualReferencia,
        verbose_name=_lazy("ciclo anual de referência"),
        on_delete=models.CASCADE,
        related_name="projecoes_de_futuro",
    )

    def get_absolute_url(self):
        return reverse("painel_projecao_futuro",
                       args=(self.ciclo_anual_referencia.agroecossistema.id,
                             self.ciclo_anual_referencia.ano))

    def get_update_url(self):
        return reverse("painel_projecao_futuro_update", args=(self.pk,))


class DiagramasDeFluxo(models.Model):
    nome = models.CharField(max_length=100, verbose_name=_lazy("Nome"))
    descricao = models.TextField(null=True, blank=True, verbose_name=_lazy("Descrição"))
    arquivo = models.FileField(_lazy("arquivo"), upload_to="diagramas/%Y/%m/%d/")
    agroecossistema = ForeignKey(
        Agroecossistema,
        verbose_name=_lazy("Agroecossistema"),
        on_delete=models.CASCADE,
        related_name="diagramas",
    )

    def get_absolute_url(self):
        return reverse("painel_diagrama_fluxo",
                       args=(self.agroecossistema.id,))
