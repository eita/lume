from django_summernote.widgets import SummernoteInplaceWidget
from django.forms.widgets import CheckboxSelectMultiple

from bootstrap_datepicker_plus import DatePickerInput, YearPickerInput
from django import forms
from django.forms.models import inlineformset_factory

from django.utils.translation import gettext as _

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Button

from economica.models import Analise, Subsistema
from qualitativa.models import Analise as AnaliseQualitativa
from linhadotempo.models import EventoGatilho
from main.forms import LumeModelForm, LumeForm
from main.models import *
from .models import (
    Agroecossistema, CicloAnualReferencia, Pessoa,
    ComposicaoNsga, Terra, DiagramasDeFluxo, ProjecaoDeFuturo,
)

class AgroecossistemaForm(LumeModelForm):
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')  # To get request.user. Do not use kwargs.pop('user', None) due to potential security hole
        self.can_delete = kwargs.pop('can_delete', False)
        if self.can_delete:
            self.delete_confirm_message = _('Deseja realmente apagar este agroecossistema?')
            self.delete_url = reverse('agroecossistema_delete', kwargs={'pk': kwargs.get('instance').id})
            self.delete_success_url = reverse('agroecossistema_list')

        if self.user.has_perm('organizacao.ver_todos'):
            organizacoes_visiveis = Organizacao.objects.all()
        else:
            organizacoes_visiveis = Organizacao.objects.filter(usuarias=self.user)

        super(AgroecossistemaForm, self).__init__(*args, **kwargs)
        self.fields['organizacao'].queryset = organizacoes_visiveis

        # help_text_comunidade = _('Nova')
        # self.fields['comunidade'].help_text = "<button class='btn btn-primary modal-right-open' type='button'><i class='fa fa-plus mr-2'></i>" + help_text_comunidade + "</button>"

        # help_text_territorio = _('Novo')
        # self.fields['territorio'].help_text = "<a href=\"" + reverse(
        #     "territorio_create") + "\">" + help_text_territorio + "</a>"

    class Meta:
        model = Agroecossistema
        fields = [
            'nsga',
            'tipo_gestao_nsga',
            'comunidade',
            'organizacao',
            'territorio',
            'mes_inicio_ciclo',
            'mes_fim_ciclo',
            'visualizacao_publica'
        ]
        exclude = ['anexos', 'geoponto', 'poligono']
        labels = {
            'nsga': _('NSGA'),
            'tipo_gestao_nsga': _('Tipo de gestão do NSGA'),
            'organizacao': _('Organização'),
            'mes_inicio_ciclo': _('Mês de Início do Ciclo Produtivo'),
            'mes_fim_ciclo': _('Mês de Fim do Ciclo Produtivo'),
            'visualizacao_publica': _('É modelo de acesso público?')
        }


class AgroecossistemaImagemForm(LumeModelForm):
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')  # To get request.user. Do not use kwargs.pop('user', None) due to potential security hole
        super().__init__(*args, **kwargs)

    class Meta:
        model = Agroecossistema
        fields = [
            'imagem',
        ]


class CicloAnualReferenciaForm(LumeModelForm):
    CRIACAO_LIMPA = 'limpa'

    CHOICES_CRIACAO = (
        (CRIACAO_LIMPA, _('Inicializar análise com dados em branco')),
        (CicloAnualReferencia.COPIA_AE_ESTRUTURA,
         _('Copiar estrutura')),
        (CicloAnualReferencia.COPIA_CAR_EM_SEQUENCIA,
         _('Copiar estrutura com valores sequenciais')),
        (CicloAnualReferencia.COPIA_APENAS_TERRAS_E_COMPOSICAO,
         _('Copiar estrutura sem análise econômica')),
         (CicloAnualReferencia.COPIA_AE_COMPLETA, _('Copiar todas as informações de um outro ano'))
    )

    tipo_criacao = forms.ChoiceField(choices=CHOICES_CRIACAO, widget=forms.RadioSelect, label=_('Opções'),
                                     initial=CicloAnualReferencia.COPIA_AE_ESTRUTURA)

    ciclo_base = forms.ModelChoiceField(queryset=None, label=_("Análise de base para a cópia"), required=False)

    class Meta:
        model = CicloAnualReferencia
        fields = ['agroecossistema', 'ano', 'tipo_criacao', 'ciclo_base']
        widgets = {
            'ano': YearPickerInput(
                options={
                    "format": "YYYY",
                    "locale": "pt-br",
                },
                required=True
            ),
            'agroecossistema': forms.HiddenInput()
        }

    def __init__(self, *args, **kwargs):
        agroecossistema_id = kwargs.pop('agroecossistema_id')
        super(CicloAnualReferenciaForm, self).__init__(*args, **kwargs)
        self.fields['agroecossistema'].initial = Agroecossistema.objects.get(id=agroecossistema_id)
        self.fields['agroecossistema'].is_hidden = True
        self.fields['ciclo_base'].queryset = CicloAnualReferencia.objects.filter(agroecossistema_id=agroecossistema_id)

        # If 'later' is chosen, set send_date as required
        if self.data and self.data.get('tipo_criacao', None) != str(self.CRIACAO_LIMPA):
            self.fields['ciclo_base'].required = True

    def clean(self):
        data = super().clean()

        ano = data.get('ano')
        agroecossistema = data.get('agroecossistema')
        sim_slug = data.get('sim_slug')
        if not sim_slug:
            car = CicloAnualReferencia.objects.filter(ano=ano, agroecossistema_id=agroecossistema.id, sim_slug__isnull=True)
            if len(car) > 0:
                self.add_error('ano', _('Você não pode ter dois ciclos do mesmo ano!'))

        return data

class SimulacaoForm(LumeModelForm):
    subsistemas = forms.ModelMultipleChoiceField(
        queryset=None,
        widget=CheckboxSelectMultiple,
        required=False)

    class Meta:
        model = CicloAnualReferencia
        fields = ['sim_nome', 'sim_descricao', 'subsistemas', 'sim_evento_gatilho']
        labels = {
            'sim_nome': _('Nome da Simulação'),
            'sim_descricao': _('Descrição'),
            'subsistemas': _('Subsistemas da Simulação'),
            'sim_evento_gatilho': _('Evento Gatilho')
        }

    def __init__(self, *args, **kwargs):
        agroecossistema_id = kwargs.pop('agroecossistema_id')
        ano_car = kwargs.pop('ano_car')
        super(SimulacaoForm, self).__init__(*args, **kwargs)

        agroecossistema = Agroecossistema.objects.get(pk=agroecossistema_id)

        self.fields['sim_evento_gatilho'].queryset = EventoGatilho.objects.filter(
            organizacao_id=agroecossistema.organizacao_id)

        ss_values = Subsistema.objects.filter(analise__ciclo_anual_referencia__ano=ano_car,
                                              analise__ciclo_anual_referencia__agroecossistema_id=agroecossistema_id,
                                              analise__ciclo_anual_referencia__sim_slug__isnull=True)
        self.fields['subsistemas'].queryset = ss_values
        self.fields['subsistemas'].initial = ss_values
        self.fields['sim_nome'].required = True

        # self.fields['ciclo_base'].queryset = CicloAnualReferencia.objects.filter(agroecossistema_id=agroecossistema_id)

        # If 'later' is chosen, set send_date as required
        # if self.data and self.data.get('tipo_criacao', None) != str(self.CRIACAO_LIMPA):
        #    self.fields['ciclo_base'].required = True


class SimulacaoUpdateForm(LumeModelForm):
    class Meta:
        model = CicloAnualReferencia
        fields = ['sim_nome', 'sim_descricao', 'sim_evento_gatilho']
        labels = {
            'sim_nome': _('Nome da Simulação'),
            'sim_descricao': _('Descrição'),
            'sim_evento_gatilho': _('Evento Gatilho')
        }

    def __init__(self, *args, **kwargs):
        agroecossistema_id = kwargs.pop('agroecossistema_id')
        ano_car = kwargs.pop('ano_car')
        super().__init__(*args, **kwargs)

        self.fields['sim_nome'].required = True

        agroecossistema = Agroecossistema.objects.get(pk=agroecossistema_id)
        self.fields['sim_evento_gatilho'].queryset = EventoGatilho.objects.filter(
            organizacao_id=agroecossistema.organizacao_id)


class CicloAnualReferenciaEditForm(LumeModelForm):
    class Meta:
        model = CicloAnualReferencia
        fields = []

    def clean(self):
        self._validate_unique = True

        qtd_responsaveis = len([valor for campo, valor in self.data.items()
                                if 'responsavel' in campo and valor=='on'])
        if qtd_responsaveis > 4:
            raise forms.ValidationError(_('É permitida atribuição de no máximo quatro (04) responsáveis por um agroecossistema'))

        return self.cleaned_data


class PessoaForm(LumeModelForm):

    def __init__(self, *args, **kwargs):
        self.can_delete = kwargs.pop('can_delete', False)

        if self.can_delete:
            agroecossistema_id = kwargs.pop('agroecossistema_id')
            self.delete_confirm_message = _('Deseja realmente apagar esta pessoa?')
            self.delete_url = reverse('agroecossistema:pessoa_delete', kwargs={'agroecossistema_id': agroecossistema_id, 'pk': kwargs.get('instance').id})
            self.delete_success_url = reverse('agroecossistema:pessoa_list',
                                              kwargs={'agroecossistema_id': agroecossistema_id})
        super(PessoaForm, self).__init__(*args, **kwargs)
        self.fields['data_nascimento'].input_formats = ['%Y-%m-%d', '%d/%m/%Y', '%d/%m/%y']

    class Meta:
        model = Pessoa
        fields = ['nome', 'sexo', 'data_nascimento']
        widgets = {
            'data_nascimento': DatePickerInput(
                options={
                    "format": "DD/MM/YYYY",
                    "locale": "pt-br",
                },
                required=True
            ),
            'sexo': forms.RadioSelect(
            )
        }
        labels = {
            'data_nascimento': _('Data de Nascimento')
        }

PessoaFormSet = inlineformset_factory(Agroecossistema, Pessoa, form=PessoaForm, can_delete=True, extra=1, min_num=0)

class ComposicaoNsgaForm(LumeModelForm):

    def __init__(self, *args, **kwargs):
        self.can_delete = kwargs.pop('can_delete', False)
        agroecossistema_id = kwargs.pop('agroecossistema_id')
        if self.can_delete:
            self.delete_confirm_message = _('REVER Deseja realmente apagar esta composicaonsga?')
            self.delete_url = reverse('agroecossistema:composicaonsga_delete',
                                      kwargs={'agroecossistema_id': agroecossistema_id,
                                              'pk': kwargs.get('instance').id})
            self.delete_success_url = reverse('agroecossistema:composicaonsga_list',
                                              kwargs={'agroecossistema_id': agroecossistema_id})
        super(ComposicaoNsgaForm, self).__init__(*args, **kwargs)
        self.fields['pessoa'].queryset = Pessoa.objects.filter(agroecossistema_id=agroecossistema_id)

    class Meta:
        model = ComposicaoNsga
        fields = [
            'pessoa',
            'responsavel',
            'mora_no_agroecossistema',
            'grau_de_parentesco',
            'observacoes',
            'ciclo_anual_referencia'
        ]
        labels = {
            'responsavel': _('Responsável pelo NSGA?'),
            'mora_no_agroecossistema': _('Mora no agroecossistema?'),
            'grau_de_parentesco': _('Grau de parentesco com relação às/aos reponsáveis')
        }


class TerraForm(LumeModelForm):

    def __init__(self, *args, **kwargs):
        self.can_delete = kwargs.pop('can_delete', False)
        if self.can_delete:
            agroecossistema_id = kwargs.pop('agroecossistema_id')
            self.delete_confirm_message = _('Deseja realmente apagar esta terra?')
            self.delete_url = reverse('agroecossistema:terra_delete', kwargs={'agroecossistema_id': agroecossistema_id, 'pk': kwargs.get('instance').id})
            self.delete_success_url = reverse('agroecossistema:terra_list', kwargs={'agroecossistema_id': agroecossistema_id})
        super(TerraForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Terra
        fields = ['tipo', 'area_territorio', 'observacoes']
        exclude = ['ciclo_anual_referencia']
        labels = {
            'area_territorio': _('Área (ha)'),
            'tipo': _('Classificação'),
            'observacoes': _('Observações')
        }
        widgets = {
            'observacoes': forms.Textarea(attrs={'rows': 3}),
            'area_territorio': forms.TextInput(attrs={'style': 'width:8ch'})
        }


TerraFormSet = inlineformset_factory(CicloAnualReferencia, Terra, form=TerraForm, can_delete=True, extra=1, min_num=0)

ComposicaoNsgaFormSet = inlineformset_factory(CicloAnualReferencia, ComposicaoNsga, form=ComposicaoNsgaForm, extra=1, min_num=0)


class ComparacaoAnaliseForm(LumeForm):
    analise1 = forms.ModelChoiceField(empty_label=_("Escolha a primeira análise econômica para comparação"), queryset=None)
    analise2 = forms.ModelChoiceField(empty_label=_("Escolha a segunda análise econômica"), queryset=None)

    def __init__(self, *args, **kwargs):
        agroecossistema_id = kwargs.pop('agroecossistema_id')
        super(ComparacaoAnaliseForm, self).__init__(*args, **kwargs)
        qset = Analise.objects.filter(
            ciclo_anual_referencia__agroecossistema_id=agroecossistema_id,
            ciclo_anual_referencia__sim_slug__isnull=True).order_by('ciclo_anual_referencia__ano')
        self.fields['analise1'].queryset = qset
        self.fields['analise2'].queryset = qset


class DiagramaDeFluxoForm(LumeModelForm):

    def __init__(self, agroecossistema_id = None, *args, **kwargs):
        self.can_delete = kwargs.pop('can_delete', False)
        if self.can_delete:
            self.delete_confirm_message = _('Deseja realmente apagar este arquivo?')
            # self.delete_url = reverse('anexos:anexo_delete_ajax', kwargs={'agroecossistema_id': agroecossistema.id, 'pk': kwargs.get('instance').id})
            # self.delete_success_url = reverse('anexos:anexo_list', kwargs={'agroecossistema_id': agroecossistema.id})

        super().__init__(*args, **kwargs)

        if agroecossistema_id:
            # self.fields['pasta'].queryset = PastaAnexo.objects.filter(agroecossistema_id=agroecossistema_id)
            self.fields['agroecossistema'].initial = agroecossistema_id

    class Meta:
        model = DiagramasDeFluxo
        fields = ['nome', 'arquivo', 'descricao', 'agroecossistema']
        widgets = {
            'agroecossistema': forms.HiddenInput(),
        }


class ProjecaoDeFuturoForm(LumeModelForm):
    class Meta:
        model = ProjecaoDeFuturo
        fields = ['projecao_de_futuro', 'fragilidades', 'potencialidades', 'ciclo_anual_referencia']
        widgets = {
            'fragilidades': SummernoteInplaceWidget(),
            'potencialidades': SummernoteInplaceWidget(),
            'projecao_de_futuro': SummernoteInplaceWidget(),
            'ciclo_anual_referencia': forms.HiddenInput(),
        }
