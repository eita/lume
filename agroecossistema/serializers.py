from . import models
from main import models as main_models
from municipios import models as municipio_models
from paises_municipios.models import Pais
from economica import models as economica_models
from relatorios import serializers as relatorios_serializers

from rest_framework import serializers


class UsuariaSerializer(serializers.ModelSerializer):
    class Meta:
        model = main_models.Usuaria
        fields = ("id", "first_name", "last_name")


class OrganizacaoSerializer(serializers.ModelSerializer):
    # usuarias = UsuariaSerializer(many=True)

    class Meta:
        model = main_models.Organizacao
        fields = ("id", "nome", "criacao", "ultima_alteracao")


class UfSerializer(serializers.ModelSerializer):
    class Meta:
        model = municipio_models.UF
        fields = ("id", "nome", "sigla")


class MunicipioSerializer(serializers.ModelSerializer):
    uf = UfSerializer()

    class Meta:
        model = municipio_models.Municipio
        fields = ("id", "uf", "nome")


class PaisSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pais
        fields = ("id", "nome", "sigla")


class ComunidadeSerializer(serializers.ModelSerializer):
    municipio = MunicipioSerializer()
    pais = PaisSerializer()

    class Meta:
        model = main_models.Comunidade
        fields = "__all__"


class TerritorioSerializer(serializers.ModelSerializer):
    class Meta:
        model = main_models.Territorio
        fields = "__all__"


class EstoqueInsumosSerializer(serializers.ModelSerializer):
    class Meta:
        model = economica_models.EstoqueInsumos
        fields = "__all__"


class SubsistemaSerializer(serializers.ModelSerializer):
    class Meta:
        model = economica_models.Subsistema
        fields = "__all__"


class AnaliseEconomicaSerializer(serializers.ModelSerializer):
    # estoques_insumos = EstoqueInsumosSerializer(many=True)
    # subsistemas = SubsistemaSerializer(many=True)
    totais = relatorios_serializers.TotaisAnaliseEconomicaShortSerializer()
    totais_por_produto = relatorios_serializers.TotaisPorProdutoSerializer(many=True)

    class Meta:
        model = economica_models.Analise
        fields = "__all__"


class CicloAnualReferenciaSerializer(serializers.ModelSerializer):
    analises_economicas = AnaliseEconomicaSerializer(many=True)

    class Meta:
        model = models.CicloAnualReferencia
        fields = "__all__"


class AgroecossistemaSerializer(serializers.ModelSerializer):
    autor = UsuariaSerializer()
    comunidade = ComunidadeSerializer()
    organizacao = OrganizacaoSerializer()
    territorio = TerritorioSerializer()
    tipo_gestao_nsga = serializers.CharField(source="get_tipo_gestao_nsga_display")
    cars = CicloAnualReferenciaSerializer(many=True)

    class Meta:
        model = models.Agroecossistema
        fields = "__all__"
