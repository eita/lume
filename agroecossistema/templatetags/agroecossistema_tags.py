from django import template
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from django.template import Context
from django.utils.translation import gettext as _
import calendar

from agroecossistema.models import Agroecossistema

register = template.Library()


@register.filter(name='month_name')
def month_name(month_number):
    return _(calendar.month_name[month_number])


@register.inclusion_tag("agroecossistema/inclusiontags/painel_caractericacao.html", takes_context=True)
def painel_agroecossistema_caracterizacao(context, car, object, analise_economica):
    return {
        "car": car,
        "object": object,
        "analise_economica": analise_economica,
    }


@register.inclusion_tag("agroecossistema/inclusiontags/painel_cabecalho.html", takes_context=True)
def painel_agroecossistema_cabecalho(context, car, agroecossistema, sim_slug, tipo_sigla):
    return {
        "car": car,
        "agroecossistema": agroecossistema,
        "sim_slug": sim_slug,
        "tipo_sigla": tipo_sigla,
        "submodulo": context['submodulo']
    }
