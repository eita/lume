# Generated by Django 2.1.5 on 2019-09-26 16:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agroecossistema', '0023_composicaonsga_grau_de_parentesco'),
    ]

    operations = [
        migrations.AddField(
            model_name='composicaonsga',
            name='deleted',
            field=models.DateTimeField(blank=True, editable=False, null=True),
        ),
    ]
