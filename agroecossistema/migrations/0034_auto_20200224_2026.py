# Generated by Django 2.1.5 on 2020-02-24 23:26

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('agroecossistema', '0033_agregacaoagroecossistemas'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agregacaoagroecossistemas',
            name='ciclo_anual_referencia',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='agregacao', to='agroecossistema.CicloAnualReferencia'),
        ),
        migrations.AlterField(
            model_name='agregacaoagroecossistemas',
            name='descricao',
            field=models.TextField(blank=True, null=True, verbose_name='Descrição'),
        ),
        migrations.AlterField(
            model_name='agregacaoagroecossistemas',
            name='evento_gatilho',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='agregacao', to='linhadotempo.EventoGatilho'),
        ),
        migrations.AlterField(
            model_name='agregacaoagroecossistemas',
            name='json_criterios_selecao',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='agregacaoagroecossistemas',
            name='nome',
            field=models.CharField(blank=True, max_length=80, null=True),
        ),
        migrations.AlterField(
            model_name='agregacaoagroecossistemas',
            name='organizacao',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='agregacao', to='main.Organizacao'),
        ),
    ]
