# Generated by Django 2.1.5 on 2019-09-24 16:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agroecossistema', '0021_composicaonsga_mora_no_agroecossistema'),
    ]

    operations = [
        migrations.AlterField(
            model_name='composicaonsga',
            name='mora_no_agroecossistema',
            field=models.BooleanField(default=True),
        ),
    ]
