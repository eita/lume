from django.apps import AppConfig


class AgroecossistemaConfig(AppConfig):
    name = 'agroecossistema'
