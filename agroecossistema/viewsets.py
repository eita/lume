from . import models
from . import serializers
from rest_framework import viewsets, permissions


class AgroecossistemaViewSet(viewsets.ModelViewSet):
    queryset = (
        models.Agroecossistema.objects.select_related(
            "organizacao",
            "comunidade__municipio",
            "comunidade__municipio__uf",
            "comunidade__pais",
            "comunidade",
            "autor",
        )
        .prefetch_related("organizacao__usuarias")
        .all()
    )
    serializer_class = serializers.AgroecossistemaSerializer
    permission_classes = [permissions.IsAuthenticated]
