from django.urls import path, include
from rest_framework import routers
from . import views, viewsets
from anexos import views as anexo_views

app_name = "agroecossistema"
router = routers.DefaultRouter()
router.register(r"agroecossistema", viewsets.AgroecossistemaViewSet)

urlpatterns = []

urlpatterns += [
    # DRF
    path("api/v1/", include(router.urls)),
]


urlpatterns += (
    # urls for CicloAnualReferencia
    path("car/", views.CicloAnualReferenciaListView.as_view(), name="car_list"),
    # path('car/<int:ano_car>/detail', views.CicloAnualReferenciaDetailView.as_view(),
    path(
        "car/create/", views.CicloAnualReferenciaCreateView.as_view(), name="car_create"
    ),
    path(
        "car/<int:ano_car>/terras/update",
        views.CARTerrasUpdateView.as_view(),
        name="car_update_terras",
    ),
    path(
        "car/<int:ano_car>/composicaonsga",
        views.CARComposicaoUpdateView.as_view(),
        name="car_update_composicao",
    ),
    path(
        "car/<int:ano_car>/delete",
        views.CicloAnualReferenciaDeleteView.as_view(),
        name="car_delete",
    ),
)

urlpatterns += (
    # urls para Simulação
    path(
        "car/<int:ano_car>/simulacao",
        views.SimulacaoListView.as_view(),
        name="sim_list",
    ),
    path(
        "car/<int:ano_car>/create_simulacao",
        views.SimulacaoCreateView.as_view(),
        name="sim_create",
    ),
    path(
        "car/<int:ano_car>/simulacao/<str:sim_slug>/delete",
        views.CicloAnualReferenciaDeleteView.as_view(),
        name="sim_delete",
    ),
    path(
        "car/<int:ano_car>/simulacao/<str:sim_slug>",
        views.CicloAnualReferenciaDetailView.as_view(),
        name="sim_detail",
    ),
    path(
        "car/<int:ano_car>/simulacao/<str:sim_slug>/update",
        views.SimulacaoUpdateView.as_view(),
        name="sim_update",
    ),
    path(
        "car/<int:ano_car>/simulacao/<str:sim_slug>/terras/update",
        views.CARTerrasUpdateView.as_view(),
        name="sim_update_terras",
    ),
    path(
        "car/<int:ano_car>/simulacao/<str:sim_slug>/composicaonsga",
        views.CARComposicaoUpdateView.as_view(),
        name="sim_update_composicao",
    ),
)

urlpatterns += (
    # urls for Pessoa
    path("pessoa/", views.PessoaListView.as_view(), name="pessoa_list"),
    path("pessoa/create/", views.PessoaCreateView.as_view(), name="pessoa_create"),
    # path('pessoa/update/<int:pk>/', views.PessoaUpdateView.as_view(), name='pessoa_update'),
    path(
        "pessoa/delete/<int:pk>/",
        views.PessoaDeleteView.as_view(),
        name="pessoa_delete",
    ),
)

urlpatterns += (
    path(
        "comparar",
        views.AnaliseCompareFormView.as_view(),
        name="analise_compara_escolha",
    ),
    path(
        "comparar_sim/<int:ano_car>/simulacao/<str:sim_slug>",
        views.AnaliseCompareSimView.as_view(),
        name="analise_compara_escolha_simulacao",
    ),
    path(
        "comparar/<int:analise_id_1>/<int:analise_id_2>",
        views.AnaliseCompareView.as_view(),
        name="analise_compara",
    ),
)
