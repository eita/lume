import calendar

from django.utils.translation import gettext as _

def nome_car(ano, mes_inicio_ciclo, mes_fim_ciclo):
    ano_final = ano if mes_inicio_ciclo < mes_fim_ciclo else ano+1
    return _(calendar.month_abbr[mes_inicio_ciclo]) + str(ano) + ' - ' + _(
        calendar.month_abbr[mes_fim_ciclo]) + str(ano_final)
