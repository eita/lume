from . import models
from . import serializers
from rest_framework import viewsets, permissions


class AnaliseViewSet(viewsets.ModelViewSet):
    """ViewSet for the Analise class"""

    queryset = models.Analise.objects.all()
    serializer_class = serializers.AnaliseSerializer
    permission_classes = [permissions.IsAuthenticated]


class AtributoViewSet(viewsets.ModelViewSet):
    """ViewSet for the Atributo class"""

    queryset = models.Atributo.objects.all()
    serializer_class = serializers.AtributoSerializer
    permission_classes = [permissions.IsAuthenticated]


class ParametroViewSet(viewsets.ModelViewSet):
    """ViewSet for the Parametro class"""

    queryset = models.Parametro.objects.all()
    serializer_class = serializers.ParametroSerializer
    permission_classes = [permissions.IsAuthenticated]


class QualificacaoViewSet(viewsets.ModelViewSet):
    """ViewSet for the Qualificacao class"""

    queryset = models.Qualificacao.objects.all()
    serializer_class = serializers.QualificacaoSerializer
    permission_classes = [permissions.IsAuthenticated]


class AvaliacaoParametroViewSet(viewsets.ModelViewSet):
    """ViewSet for the AvaliacaoParametro class"""

    queryset = models.AvaliacaoParametro.objects.all()
    serializer_class = serializers.AvaliacaoParametroSerializer
    permission_classes = [permissions.IsAuthenticated]


