from django.db.models import Q
from django.core.exceptions import ValidationError
from django.forms import ModelForm, forms
from django.utils.translation import gettext as _
from django.urls import reverse
from dal import autocomplete

from main.models import Organizacao, OrganizacaoUsuaria
from linhadotempo.models import EventoGatilho
from qualitativa.models import (Analise, AnaliseTemplate, Atributo,
                                Parametro, Qualificacao, AvaliacaoParametro)
from django import forms

"""
Classes para a entrada de forma individual de dados por formulário
Não se aplicam regras da planilha nessas Classes
Forms úteis para testes e para funções avançadas de administrador
"""


class insertAnaliseForm(ModelForm):
    class Meta:
        model = Analise
        fields = ['agroecossistema', 'ano']


class insertAtributoForm(ModelForm):
    class Meta:
        model = Atributo
        fields = ['nome']


class insertParametroForm(ModelForm):
    class Meta:
        model = Parametro
        fields = ['nome', 'atributo']


class QualificacaoForm(ModelForm):
    class Meta:
        model = Qualificacao
        fields = ['parametro', 'qualificacao', 'ano', 'agroecossistema']


class AvaliacaoParametroForm(ModelForm):
    class Meta:
        model = AvaliacaoParametro
        fields = ['analise', 'parametro', 'mudancas',
                  'mudancas_eventos', 'justificativa']


class AnaliseQualitativaModelForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(AnaliseQualitativaModelForm, self).__init__(*args, **kwargs)
        usuaria = kwargs['initial']['usuaria']
        self.fields['evento_gatilho'].queryset = self.get_evento_gatilho_queryset(
            usuaria)
        self.fields['analise_template'].queryset = self.get_analise_template_queryset(
            usuaria)

    def get_analise_template_queryset(self, usuaria):
        eh_admin_geral = usuaria.groups.filter(name='admin')
        if eh_admin_geral or usuaria.is_superuser:
            return AnaliseTemplate.objects.all()

        user_organizacoes = OrganizacaoUsuaria.objects.filter(
            usuaria_id=usuaria.id).values_list('organizacao__id', flat=True)

        return AnaliseTemplate.objects.filter(
            Q(organizacoes__in=user_organizacoes) | Q(eh_template_global=True))

    def get_evento_gatilho_queryset(self, user):
        eh_admin_geral = user.groups.filter(name='admin')
        if eh_admin_geral or user.is_superuser:
            return EventoGatilho.objects.all()

        user_organizacoes = OrganizacaoUsuaria.objects.filter(
            usuaria_id=user.id).values_list('organizacao__id', flat=True)

        return EventoGatilho.objects.filter(organizacao__in=user_organizacoes)

    class Meta:
        model = Analise
        exclude = ['parametros_inativos',]
        widgets = {
            'agroecossistema': forms.HiddenInput(),
        }


class AnaliseTemplateForm(ModelForm):
    def __init__(self, *args, **kwargs):
        if kwargs.get('instance'):
            self.can_delete = kwargs.pop('can_delete', True)
            if self.can_delete:
                self.delete_confirm_message = _(
                    'Deseja realmente apagar este registro?')
                self.delete_url = reverse('analise_template_delete', kwargs={
                                          'pk': kwargs.get('instance').id})
                self.delete_success_url = reverse('analise_template_list')
        super(AnaliseTemplateForm, self).__init__(*args, **kwargs)

    class Meta:
        model = AnaliseTemplate
        fields = ['nome', 'eh_template_global',
                  'organizacoes', 'descricao']
        widgets = {
            'descricao': forms.Textarea(attrs={'cols': 80, 'rows': 2}),
            'organizacoes': autocomplete.ModelSelect2Multiple(url='organizacao_autocomplete'),
        }

    def clean_organizacoes(self, *args, **kwargs):
        data = self.cleaned_data
        organizacoes = data['organizacoes']

        if not self.instance.id:
            return organizacoes

        if 'organizacoes' not in self.changed_data:
            return organizacoes

        organizacoes_iniciais = self.get_initial_for_field(
            self.fields['organizacoes'], 'organizacoes')
        for org in organizacoes_iniciais:
            if org in organizacoes:
                continue

            # caso exista uma análise qualitativa criada por um membro da organização que está sendo retirada, ele não sendo admin, retorne erro.
            analise_utilizando_template = Analise.objects\
                .filter(
                    agroecossistema__organizacao=org,
                    analise_template=self.instance
                )
            if analise_utilizando_template:
                raise ValidationError(
                    f"Este modelo de análise já é utilizada pela organização '{ org }'!")

        return organizacoes


class AnaliseTemplateAtributoModelForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(AnaliseTemplateAtributoModelForm, self).__init__(*args, **kwargs)
        if kwargs.get('instance'):
            self.fields['nome'].widget = forms.HiddenInput()
            self.fields['ordem'].widget = forms.HiddenInput()
        else:
            self.fields['nome'].label = _('Novo Atributo')

    class Meta:
        model = Atributo
        fields = ('nome', 'ordem', 'analise_template')
        labels = {
            'nome': _('Atributo'),
            'ordem': _('Posição'),
        }
        widgets = {
            'analise_template': forms.HiddenInput(),
        }


class AnaliseTemplateParametroModelForm(forms.ModelForm):
    class Meta:
        model = Parametro
        fields = ('ordem', 'nome', 'descricao')
        labels = {
            'nome': _('Parâmetro'),
        }


def analise_template_parametros_form_set(model):

    return forms.inlineformset_factory(
        model,
        Parametro,
        form=AnaliseTemplateParametroModelForm, extra=1)
