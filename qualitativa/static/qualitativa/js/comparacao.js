function alerta(message, error = false) {
  var notifyMessage = $(".notify");
  var notifyWrapper = $(".notify-wrapper");
  notifyMessage.html(message);
  notifyWrapper.toggleClass("alert-danger", error);
  notifyWrapper.fadeIn(1500);
  setTimeout(function () {
    notifyWrapper.fadeOut(1500);
  }, 1500);
}

fnSoma = function (elm1, elm2) {
  return elm1 + elm2;
};

// Variável Global que recebe valores totais_analise_qualitativa de sintese
var totais_analise_qualitativa = {};

var sintese_title = "";

function alternatePointStyles(ctx) {
  var index = ctx.dataIndex;
  return index % 2 === 0 ? "circle" : "rect";
}

var chart = {};
function atualizaGraficosESintese() {
  var sinteseTotal = true;
  var presets = window.chartColors;
  var qtde_atributos = 0;

  var ano = $("#analise-info").data("ano");
  var ano_referencia = $("#analise-info").data("ano-referencia");

  let colors = {};

  $("[data-tbl-atributo]").each(function (index, tbl_atributo) {
    var atributo_id = $(tbl_atributo).data("tbl-atributo");
    var atributo_title = $(tbl_atributo).data("atributo-title").split("|");
    if (sintese_title == "") {
      sintese_title = atributo_title[0];
    }

    var inputs_ano_atual = $(tbl_atributo).find("[data-tipo='atual']");
    var inputs_ano_referencia = $(tbl_atributo).find(
      "[data-tipo='referencia']"
    );
    var parametro_labels = $(tbl_atributo).find(
      "[data-tipo='parametro_label']"
    );

    var dados_atuais = [];
    var dados_referencia = [];
    var close = false;

    totais_analise_qualitativa[atributo_id] = {};
    let sintese_atual = 0;
    let sintese_referencia = 0;

    inputs_ano_atual.each(function (j, input_ano_atual) {
      if (!close) {
        let valor = parseInt(input_ano_atual.value);
        if (isNaN(valor)) {
          close = true;
          return;
        } else {
          dados_atuais.push(valor);
          sintese_atual += (valor - 1) / 4;
        }
      }
    });

    inputs_ano_referencia.each(function (j, input_ano_referencia) {
      if (!close) {
        var valor = parseInt(input_ano_referencia.value);
        if (isNaN(valor)) {
          close = true;
          return;
        } else {
          dados_referencia.push(valor);
          sintese_referencia += (valor - 1) / 4;
        }
      }
    });

    var crl = [];
    colors[atributo_id] = [];
    var crids = [];

    parametro_labels.each(function (lbl_key, lbl_val) {
      crl.push($(lbl_val).text().trim());
      crids.push($(lbl_val).data("parametro-id"));
    });

    if (atributo_id == "1") {
      const trs = $(tbl_atributo).find('tr[id^="row-parametro-"]');
      trs.each(function (i, tr) {
        const subsecao = $(tr).parents("table").data("subsecao");
        let color = "#000";
        if (subsecao == "base-de-recursos-autocontrolada") {
          color = "#00C";
        } else {
          color = "#C00";
        }
        colors[atributo_id].push(color);
      });
    }

    var totalsElements = $(
      `#totais-${atributo_id}, .download-chart-${atributo_id}, #aranha-${atributo_id}`
    );
    if (!close) {
      //Calcula Síntese para aquele atributo
      totais_analise_qualitativa[atributo_id] = {
        atual: sintese_atual / dados_atuais.length,
        referencia: sintese_referencia / dados_referencia.length,
      };

      var chart_data = {
        labels: crl,
        datasets: [
          {
            backgroundColor: presets.qualitativaAnoReferencia,
            borderColor: presets.qualitativaAnoReferenciaLinha,
            fill: true,
            data: dados_referencia,
            label: `Ano Referência (${ano_referencia})`,
          },
          {
            backgroundColor: presets.qualitativaAnoAtual,
            borderColor: presets.qualitativaAnoAtualLinha,
            fill: true,
            data: dados_atuais,
            label: `Ano Atual (${ano})`,
          },
        ],
      };

      var chart_options = {
        spanGaps: false,
        elements: {
          line: {
            tension: 0.000001,
            fill: "top",
          },
        },
        plugins: {
          title: {
            display: true,
            text: atributo_title,
            font: {
              size: 18,
            },
            padding: {
              bottom: 30,
            },
          },
          legend: {
            position: "bottom",
            align: "center",
          },
          filler: {
            propagate: false,
          },
          "samples-filler-analyser": {
            target: "chart-analyser",
          },
          afterDraw: function () {
            if (this.data.datasets.length === 0) {
              // No data is present
              this.clear();
              this.save();
              this.textAlign = "center";
              this.textBaseline = "middle";
              this.fillText("No data to display", width / 2, height / 2);
              this.restore();
            }
          },
        },
        scales: {
          r: {
            max: 5,
            min: 0,
            beginAtZero: true,
            stepSize: 0.1,
            pointLabels: {
              font: {
                size: 14,
              },
              color: function (e) {
                if (atributo_id == 1) {
                  return colors[atributo_id][e.index]
                    ? colors[atributo_id][e.index]
                    : "#C00";
                }
                return "#000";
              },
            },
          },
        },
        animation: {
          onComplete: function () {
            const link = $(`#link-${atributo_id}`);
            var url_base64 = this.toBase64Image();
            link
              .attr("href", url_base64)
              .attr("download", getSlug(atributo_title.join(" ") + ".png"));
          },
        },
        onClick: function (e, activeElems) {
          if (activeElems && activeElems.length) {
            $("#modal-ameba-right-wrapper").fadeOut();
            return;
          }
          var helpers = Chart.helpers;
          var eventPosition = helpers.getRelativePosition(e, e.chart);
          var mouseX = eventPosition.x;
          var mouseY = eventPosition.y;

          var activePoints = [];
          helpers.each(
            e.chart.scales.r.ticks,
            function (label, index) {
              for (var i = this._pointLabels.length - 1; i >= 0; i--) {
                var pointLabelPosition = this.getPointPosition(
                  i,
                  this.getDistanceFromCenterForValue(
                    this.options.reverse ? this.min : this.max
                  ) + 5
                );

                var pointLabelFontSize = this.options.pointLabels.font.size;
                var pointLabelFontStyle = "normal";
                var pointLabelFontFamily = "Open Sans";
                var pointLabelFont = helpers.fontString(
                  pointLabelFontSize,
                  pointLabelFontStyle,
                  pointLabelFontFamily
                );
                this.ctx.font = pointLabelFont;

                var labelsCount = this._pointLabels.length,
                  halfLabelsCount = this._pointLabels.length / 2,
                  quarterLabelsCount = halfLabelsCount / 2,
                  upperHalf =
                    i < quarterLabelsCount ||
                    i > labelsCount - quarterLabelsCount,
                  exactQuarter =
                    i === quarterLabelsCount ||
                    i === labelsCount - quarterLabelsCount;
                var width = this.ctx.measureText(this._pointLabels[i]).width;
                var height = pointLabelFontSize;

                var x, y;

                if (i === 0 || i === halfLabelsCount)
                  x = pointLabelPosition.x - width / 2;
                else if (i < halfLabelsCount) x = pointLabelPosition.x;
                else x = pointLabelPosition.x - width;

                if (exactQuarter) y = pointLabelPosition.y - height / 2;
                else if (upperHalf) y = pointLabelPosition.y - height;
                else y = pointLabelPosition.y;

                if (
                  mouseY >= y &&
                  mouseY <= y + height &&
                  mouseX >= x &&
                  mouseX <= x + width
                )
                  activePoints.push({
                    index: i,
                    label: this._pointLabels[i],
                  });
              }
            },
            e.chart.scales.r
          );

          var firstPoint = activePoints[0];
          if (firstPoint !== undefined) {
            const atributo_id = e.native.target.id.split("-")[1];
            const parametro_id = crids[firstPoint.index];
            const title = crl[firstPoint.index];

            const qualificacao_referencia_val = parseInt(
              $(`#qualificacao-referencia-${parametro_id}`).val()
            );
            const qualificacao_val = parseInt(
              $(`#qualificacao-atual-${parametro_id}`).val()
            );

            let avaliacaoStr = "";
            if (qualificacao_referencia_val == qualificacao_val) {
              const avaliacao_unica = $(
                `#qualificacao-referencia-${parametro_id} option:selected`
              )
                .text()
                .trim();
              avaliacaoStr =
                '<i class="fa fa-stop-circle mr-2 text-secondary"></i>' +
                gettext("Manteve-se") +
                ` <strong>${avaliacao_unica}</strong>.`;
            } else {
              const qualificacao_referencia = $(
                `#qualificacao-referencia-${parametro_id} option:selected`
              )
                .text()
                .trim();
              const qualificacao = $(
                `#qualificacao-atual-${parametro_id} option:selected`
              )
                .text()
                .trim();
              const str = ` de <strong>${qualificacao_referencia}</strong> para <strong>${qualificacao}</strong>.`;
              avaliacaoStr =
                qualificacao_referencia_val > qualificacao_val
                  ? '<i class="fa fa-arrow-circle-down mr-2 text-secondary"></i>' +
                    gettext("Baixou") +
                    str
                  : '<i class="fa fa-arrow-circle-up mr-2 text-secondary"></i>' +
                    gettext("Aumentou") +
                    str;
            }

            const mudancas = $("#mudancas-" + parametro_id).val()
              ? $(`#mudancas-${parametro_id}`)
                  .html()
                  .replace(/\r\n|\r|\n/g, "<br />")
              : gettext("(sem texto)");
            const justificativa = $("#justificativa-" + parametro_id).val()
              ? $(`#justificativa-${parametro_id}`)
                  .html()
                  .replace(/\r\n|\r|\n/g, "<br />")
              : gettext("(sem texto)");

            $('#glossario').fadeOut();
            $('#modal-right-wrapper').fadeOut();
            

            $("#modal-ameba-right-wrapper .modal-right-title").html(title);
            $("#card-avaliacao").html(avaliacaoStr);
            $("#card-mudancas").html(mudancas);
            $("#card-justificativa").html(justificativa);
            $("#modal-ameba-right-wrapper").fadeIn();
          } else {
            $("#modal-ameba-right-wrapper").fadeOut();
          }
        },
      };

      if (
        Object.prototype.hasOwnProperty.call(chart, `${chart}-${atributo_id}`)
      ) {
        chart["chart-" + atributo_id].data = chart_data;
        chart["chart-" + atributo_id].update();
      } else {
        chart["chart-" + atributo_id] = new Chart("aranha-" + atributo_id, {
          type: "radar",
          data: chart_data,
          options: chart_options,
        });
      }

      //Show graph and indicators
      totalsElements.fadeIn();
      $("#aranhaEmpty-" + atributo_id).fadeOut();
      let indiceAtual = formatNumber(
        parseFloat(totais_analise_qualitativa[atributo_id]["atual"].toFixed(2))
      );
      if (!indiceAtual) {
        indiceRef = "0,00";
      }
      let indiceRef = formatNumber(
        parseFloat(totais_analise_qualitativa[atributo_id]["referencia"].toFixed(2))
      );
      if (!indiceRef) {
        indiceRef = "0,00";
      }
      $("#atual-" + atributo_id).html(indiceAtual);
      $("#referencia-" + atributo_id).html(indiceRef);
    } else {
      // Hide graph and indicators
      totalsElements.fadeOut();
      $("#aranhaEmpty-" + atributo_id).fadeIn();
      sinteseTotal = false;
    }
    $("#modal-ameba-right-wrapper").fadeOut();
  });
  qtde_atributos = Object.keys(totais_analise_qualitativa).length;

  if (sinteseTotal) {
    //se síntese total for true
    var bodyHtml = ""; //inicializa variável para inserção de HTML
    var totaisSintese = { atual: 0, referencia: 0 }; //variável para total
    var atributoslbl = [];
    var sintese_dados_referencia = [];
    var sintese_dados_atuais = [];
    // Inicia loop para acessar os atributo de data-tbl-atributo
    $("[data-tbl-atributo]").each(function (index, tbl_atributo) {
      // Declara Variável para armazenar o dado (id do atributo) em tbl-atributo
      var atributo_id = $(tbl_atributo).data("tbl-atributo");
      atributoslbl.push(
        $("#nav_" + atributo_id)
          .text()
          .trim()
      );
      sintese_dados_referencia.push(
        totais_analise_qualitativa[atributo_id]["referencia"].toFixed(2)
      );
      sintese_dados_atuais.push(totais_analise_qualitativa[atributo_id]["atual"].toFixed(2));

      $("#sintese-body-ano-ref-" + atributo_id).text(
        totais_analise_qualitativa[atributo_id]["referencia"].toLocaleString("pt-BR", {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2,
        })
      );
      $("#sintese-body-ano-atual-" + atributo_id).text(
        totais_analise_qualitativa[atributo_id]["atual"].toLocaleString("pt-BR", {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2,
        })
      );

      //variável para cálculo de síntese total
      totaisSintese["atual"] += totais_analise_qualitativa[atributo_id]["atual"];
      totaisSintese["referencia"] += totais_analise_qualitativa[atributo_id]["referencia"];
    });

    $("#mensagem-sintese").addClass("d-sm-none");

    var sintese_chart_data = {
      labels: atributoslbl,
      datasets: [
        {
          backgroundColor: presets.qualitativaAnoReferencia,
          borderColor: presets.qualitativaAnoReferenciaLinha,
          fill: true,
          data: sintese_dados_referencia,
          label: gettext("Ano de referência") + " (" + ano_referencia + ")",
        },
        {
          backgroundColor: presets.qualitativaAnoAtual,
          borderColor: presets.qualitativaAnoAtualLinha,
          fill: true,
          data: sintese_dados_atuais,
          label: gettext("Ano atual") + " (" + ano + ")",
        },
      ],
    };

    var sintese_chart_options = {
      spanGaps: false,
      elements: {
        line: {
          tension: 0.000001,
          fill: "top",
        },
      },
      plugins: {
        title: {
          display: true,
          text: [sintese_title, gettext("Gráfico síntese")],
          font: {
            size: 18,
          },
          padding: {
            bottom: 30,
          },
        },
        legend: {
          position: "bottom",
          align: "center",
        },
        filler: {
          propagate: false,
        },
        "samples-filler-analyser": {
          target: "chart-analyser",
        },
        afterDraw: function () {
          if (this.data.datasets.length === 0) {
            // No data is present
            this.clear();
            this.save();
            this.textAlign = "center";
            this.textBaseline = "middle";
            this.fillText("No data to display", width / 2, height / 2);
            this.restore();
          }
        },
      },
      scales: {
        r: {
          max: 1,
          min: -0.1,
          stepSize: 0.1,
          pointLabels: {
            font: {
              size: 14,
            },
          },
        },
      },
      animation: {
        onComplete: function () {
          var url_base64 = this.toBase64Image();
          $("#link-sintese").attr("href", url_base64);
          $("#link-sintese")
            .attr("href", url_base64)
            .attr("download", getSlug(sintese_title) + "_sintese.png");
        },
      },
    };

    if (Object.prototype.hasOwnProperty.call(chart, "chart-sintese")) {
      chart["chart-sintese"].data = sintese_chart_data;
      chart["chart-sintese"].update();
    } else {
      chart["chart-sintese"] = new Chart("aranha-sintese", {
        type: "radar",
        data: sintese_chart_data,
        options: sintese_chart_options,
      });
    }

    // calcula e insere sintese total no HTML da página
    $("#referencia-sintese").text(
      (totaisSintese["referencia"] / qtde_atributos).toLocaleString(
        gettext("pt-BR"),
        { minimumFractionDigits: 2, maximumFractionDigits: 2 }
      )
    );
    $("#atual-sintese").text(
      (totaisSintese["atual"] / qtde_atributos).toLocaleString(
        gettext("pt-BR"),
        { minimumFractionDigits: 2, maximumFractionDigits: 2 }
      )
    );
    $(".sintese-geral, .download-chart-sintese").fadeIn();
  } else {
    $(".sintese-geral, .download-chart-sintese").fadeOut();
  }
}

function atualizaEvento(params) {
  jQuery(".notify-loading").show(500);

  const onSuccess = function (data, newEvento, apagar, toastBody) {
    if (!apagar) {
      newEvento.id = data.pk;
    }
    const item = eventoToItem(newEvento, newEvento, false);
    let eventCreated = true;
    if (apagar) {
      for (element of $(".select_mudancas_eventos")) {
        const values = $(element).val();
        const newValues = values.filter((id) => parseInt(id) !== item.id);
        $(element).val(newValues).trigger("change");
      }
      eventos = eventos.filter((evento) => evento.id !== item.id);
      $(`option[value=${item.id}]`).remove();
      eventCreated = false;
    } else {
      for ([idx, evento] of eventos.entries()) {
        if (evento.id === parseInt(item.id)) {
          eventos[idx] = item;
          $(`option[value=${item.id}]`).html(
            `${moment(item.start).year()} - ${item.titulo}`
          );
          eventCreated = false;
          break;
        }
      }
      if (eventCreated) {
        eventos.push(item);
        $(".select_mudancas_eventos")
          .append(
            $("<option>", {
              value: parseInt(item.id),
              text: `${moment(item.start).year()} - ${item.titulo}`,
            })
          )
          .trigger("change");
        const values = $(`#${newEventoInputId}`).val();
        values.push(item.id);
        $(`#${newEventoInputId}`).val(values).trigger("change");
        salvaParametro($(`#${newEventoInputId}`));
        newEventoInputId = null;
      }
    }

    initSelect2(false);
    if (!eventCreated) {
      const msg = apagar
        ? "Evento apagado com sucesso!"
        : `O evento <i>${newEvento.titulo}</i> foi ${toastBody} com sucesso!`;
      alerta(msg);
    }
    jQuery(".notify-loading").hide();

    // $("#toastTitle").html(toastTitle);
    // $(".toast-body").html(msg);
    // $(".toast").toast("show");
  };

  const onError = function (eventoAntigo, evento) {
    const msg = `Falha ao atualizar ou criar o evento <i>${evento.titulo}</i>. Operação falhou!`;
    jQuery(".notify-loading").hide();
    alerta(msg, true);

    // $("#toastTitle").html("Erro!");
    // $(".toast-body").html(`<div class="alert alert-danger">${msg}</div>`);
    // $(".toast").toast("show");
  };

  doAtualizaEvento(
    params,
    agroecossistemaId,
    null,
    onSuccess,
    onError,
    restDeleteUrl,
    restUpdateUrl,
    false
  );
}

function formatState(state) {
  if (!state.id) {
    return state.text;
  }

  // Use .text() instead of HTML string concatenation to avoid script injection issues
  const title = `Editar ${$.trim(state.text)}`;
  const stateEl = $(
    `<span><button type="button" class="mudancas_eventos-editar-wrapper" title="${title}" aria-label="${title}" id="${state.id}"><i class="fas fa-pen mudancas_eventos-editar" aria-hidden="true"></i></button><span>${state.text}</span></span>`
  );
  // var $state = $(
  //   `<span><i class="fas fa-pen mr-2 mudancas_eventos-editar" title="Editar ${$.trim(
  //     state.text
  //   )}" id="${
  //     state.id
  //   }" style="cursor: pointer;" aria-hidden="true"></i><span></span></span>`
  // );
  stateEl.find("span").text(state.text);
  // const evento = eventos.find((item) => parseInt(state.id) === item.id);
  // const stateText = evento
  //   ? `${moment(evento.start).year()} - ${state.text}`
  //   : state.text;
  // $state.find("span").text(stateText);

  setTimeout(
    function () {
      $(".mudancas_eventos-editar-wrapper")
        .off("click")
        .on("click", function () {
          const id = parseInt($(this).attr("id"));
          const evento = eventos.find((evento) => evento.id === id);
          if (evento) {
            openEditaEventoForm(evento, false);
            setTimeout(function () {
              $(".select_mudancas_eventos").select2("close");
            }, 10);
          }
        });
    },
    [200]
  );
  return stateEl;
}

function initSelect2(isFirstTime = true) {
  const elements = $(".select_mudancas_eventos");
  if (!isFirstTime) {
    elements.select2("destroy");
  }
  elements.select2({
    tags: true,
    templateSelection: formatState,
  });
  if (isFirstTime) {
    elements.on("select2:select", function (e) {
      if (!e.params.data.element) {
        $('option[data-select2-tag="true"]').remove();
        newEventoInputId = e.target.id;
        openCriaEventoForm({
          titulo: e.params.data.text,
          start: `${anoReferencia}-01-03T00:00:00`,
        });
        return;
      }
      salvaParametro($(e.target));
    });
    elements.on("select2:unselect", function (e) {
      if (!e.params.originalEvent) {
        return;
      }

      e.params.originalEvent.stopPropagation();

      if (
        !isNaN(parseInt(e.params.data.id)) &&
        e.params.data.id !== e.params.data.text
      ) {
        salvaParametro($(e.target));
      }
    });

    $(document).on("mouseenter", ".select2-selection__rendered", function () {
      $(".select2-selection__choice").removeAttr("title");
    });
  }
}

function salvaParametro(input) {
  const analiseId = input.data("analise-id");
  const parametroId = input.data("parametro-id");

  var data = {
    analise: analiseId,
    parametro: parametroId,
  };

  data["mudancas"] = $(`#mudancas-${parametroId}`).val();
  data["justificativa"] = $(`#justificativa-${parametroId}`).val();
  data["mudancas_eventos"] = $(`#mudancas_eventos-${parametroId}`).val();

  jQuery.ajax("save_avaliacao_parametro", {
    method: "POST",
    data,
    dataType: "json",
    success: function (data) {
      alerta(data.message);
    },
  });
}

$(document).ready(function () {
  atualizaGraficosESintese();

  $("#comparacao_nav .nav-link").on("click", function (e) {
    if (!$(this).hasClass("active")) {
      const href = $(this).attr("href");
      $(`${href} table.datatable.fixed-headers`)
        .DataTable()
        .fixedHeader.enable(false);
      setTimeout(function () {
        $("#comparacao_nav .nav-link").each(function (i, el) {
          const id = $(el).attr("href");
          $(`${id} table.datatable.fixed-headers`)
            .DataTable()
            .fixedHeader.enable(id == href);
        });
      }, 400);
    }
  });

  $(".botaoConcluir").on("click", function () {
    toggleEditar($(this).parents("table"), false);
  });

  $(".comparacao-table input").on("keypress", function (evt) {
    if (evt.which < 48 || evt.which > 53) {
      evt.preventDefault();
      alerta(gettext("A qualificação deve ser um valor entre 1 e 5!"), true);
    }
  });
  $('.comparacao-table select[multiple!="multiple"]').on(
    "change",
    function (evt) {
      var select = $(this);
      const ano = select.data("ano");
      const agroecossistema_id = select.data("agroecossistema-id");
      const parametro_id = select.data("parametro-id");
      const qualificacao = select.val();

      jQuery(".notify-loading").show(500);
      jQuery.ajax("save_qualificacao", {
        method: "POST",
        data: {
          ano: ano,
          agroecossistema: agroecossistema_id,
          parametro: parametro_id,
          qualificacao: qualificacao,
        },
        dataType: "json",
        success: function (data) {
          jQuery(".notify-loading").hide();
          alerta(data.message);
          atualizaGraficosESintese();
          select.closest("span").text(select.val());
        },
      });
    }
  );

  $(
    ".comparacao-table textarea, .comparacao-table select.select_mudancas_eventos"
  ).on("change", function (evt) {
    var input = $(evt.target);
    if (input.hasClass("select_mudancas_eventos")) {
      return;
    }
    salvaParametro(input);
  });

  initEventosFormHelpers(atualizaEvento, false);
  initSelect2(true);
});
