let atributoDragEl = null;

function atributoOnDragStart(evt) {
  atributoDragEl = $(evt.target);
}

function atributoOnDragEnd(evt) {
  // $(".atributo-drag-icon").remove();
}

function atributoOnDragOver(evt) {
  evt.preventDefault();
}

function atributoOnDragEnter(evt) {
  evt.preventDefault();
  const target = $(evt.target);
  if (
    target.attr("id") &&
    atributoDragEl &&
    atributoDragEl.hasClass("active") &&
    target.attr("id") !== atributoDragEl.attr("id")
  ) {
    target.addClass("atributo-drag-over");
    $(`<i class="atributo-drag-icon fa fa-arrow-down"></i>`).prependTo(target);
  }
}

function atributoOnDragLeave(evt) {
  evt.preventDefault();
  $(evt.target).find(".atributo-drag-icon").remove();
  $(evt.target).removeClass("atributo-drag-over");
}

function updateAtributoOrdens() {
  $("#comparacao_nav .nav-link").each((i, el) => {
    const id = el.id.split("_")[1];
    const form = $(`#FormAtributo${id}`);
    form.find("#id_ordem").val(i + 1);
    form.submit();
  });
}

function atributoOnDrop(evt) {
  if (atributoDragEl && atributoDragEl.hasClass("active")) {
    const target = $(evt.target);
    $(".atributo-drag-icon").remove();
    $(".atributo-drag-over").removeClass("atributo-drag-over");
    atributoDragEl.parent().insertBefore(target.parent());
    updateAtributoOrdens();
  }
}

$(document).ready(function () {
  // show checkbox "apagar"
  $('*[id$="DELETE"]').css("display", "block");
  $('div[id$="DELETE"].form-check').css("margin-top", "2rem");
  $(".nav-atributo-input-wrapper").hide();
  $("#FormAtributoEmpty #id_ordem").val($("#comparacao_nav .nav-link").length);

  $("#comparacao_nav .nav-link").on("click", (evt) => {
    if (evt.target.id === "nav_empty") {
      return;
    }
    const tableId =
      "table_atributo_" + $(evt.target).parents("a")[0].id.split("_")[1];
    setTimeout(() => {
      datatablesManager[tableId].columns.adjust();
    }, 200);
  });

  $("#comparacao_nav .atributo-edit").on("click", (evt) => {
    target = $(evt.target).parents("a");
    target.hide();
    const li = target.parents("li");
    li.find(".nav-atributo-input-wrapper").show().find("input").focus();
    evt.preventDefault();
  });

  $("#comparacao_nav .atributo-delete").on("click", (evt) => {
    const url =
      window.location.origin +
      $(evt.target).parent().parent().data("atributo-delete-url");
    window.location.href = url;
    evt.preventDefault();
    // target.hide();
    // target.parent().find("input").show().focus();
  });

  function saveAtributo(evt) {
    const id = evt.target.id.split("-")[2];
    const target = $(evt.target);
    const form = $(`#FormAtributo${id}`);
    form.find("#id_nome").val(target.val());
    form.submit();
    target.parents(".nav-atributo-input-wrapper").hide();
    $(`#nav_${id} span.nav-item-label`).text(target.val());
    $(`#nav_${id}`).show();

    alerta($(`#table_atributo_${id}`), "Nome atualizado com sucesso!", false);
  }

  $(".nav-atributo-input")
    .on("blur", saveAtributo)
    .on("keypress", (evt) => {
      if (evt.which === 13) {
        evt.preventDefault();
        $(evt.target).blur();
      }
    });

  $("#id_eh_template_global").on("click", (evt) => {
    const isChecked = $(evt.target).is(":checked");
    $("#id_organizacoes").attr("disabled", isChecked);
  });

  $("#comparacao_nav .nav-item").each((i, el) => {
    $(el)
      .on("dragenter", atributoOnDragEnter)
      .on("dragleave", atributoOnDragLeave)
      .on("dragover", atributoOnDragOver)
      .on("drop dragdrop", atributoOnDrop)
      .on("dragstart", atributoOnDragStart);

    // el.addEventListener("dragend", atributoDragEnd);
  });

  // button submit action multiple forms
  let form_error = 0;
  $("#submit-id-btn_submit").on("click", function (evt) {
    // init msg feedback
    $("#alert-feedback-danger").css("display", "none");
    $("#alert-feedback-success").css("display", "none");

    form_error = 0;

    // main form analise_template
    $("#form-analise_template").submit();

    // each form atributos
    $(".form-atributo_parametros").each(function (i, elem) {
      $(elem).submit();
    });

    // form new atributo
    const newAtributoVal = $("#FormAtributoEmpty #id_nome").val().trim();
    if (newAtributoVal) {
      id_analise_template = $("#form_main_analise_template_id").val();
      $("#FormAtributoEmpty #id_analise_template").val(id_analise_template);
      $("#FormAtributoEmpty").submit();
    }
  });

  // callback to submit form via ajax
  const submit_function = function (evt) {
    evt.preventDefault();
    const target = evt.target;
    const $target = $(target);
    const formId = target.id;
    const $table = $(`#table_atributo_${formId.replace("FormAtributo", "")}`);
    if ($table.length > 0 && !$table.hasClass("modoEdicao")) {
      const tables = $(`.${$table.attr("id")}`);
      toggleEditar(tables, true);
    }

    const url = target.action;
    const formData = $target.serialize();

    $.ajax({
      method: "POST",
      url,
      data: formData,
      async: false,
      statusCode: {
        400: function (data) {
          console.log("error", data);
          json = data.responseJSON;
          msg = json.message;
          form_error = form_error + 1;
          $("#alert-feedback-danger-title").html("Erro ao gravar!");
          $("#alert-feedback-danger-message").html(msg);
          $("#alert-feedback-danger").css("display", "block");
        },
        200: function (data) {
          if ($table.length > 0) {
            $(`#${$table.attr("id")}`)
              .find("tbody")
              .find("select, textarea, input")
              .each(function (j, el) {
                const val = $(el).val();
                $(el)
                  .parents("td")
                  .attr(
                    "data-order",
                    isNaN(parseFloat(val)) ? val : val.padStart(3, "0")
                  );
              });
            datatablesManager[$table.attr("id")]
              .rows("tbody tr")
              .invalidate()
              .order([0, "asc"])
              .draw()
              .order([]);
            toggleEditar($(`.${$table.attr("id")}`), false);
            alerta($table, "Salvo com sucesso!", false);
          }
          if (
            $("#nav_empty").hasClass("active") &&
            formId === "FormAtributoEmpty" &&
            $("#FormAtributoEmpty #id_nome").val().trim()
          ) {
            document.location.reload(true);
            return;
          }
        },
      },
    });
  };

  // submit main forms (analise_template)
  $("#form-analise_template").on("submit", submit_function);
  $(".form-atributo_parametros").each(function (i, elem) {
    $(elem).on("submit", submit_function);
  });
  $("#FormAtributoEmpty").on("submit", submit_function);

  $(".form-row label").css("cursor", "move");
  $(".form-row label").attr("title", "Clique e arraste para mudar a ordenação");
});
