$(document).ready(function () {
    jQuery.each($(".atributo_select_all"), function (i, atributo) {
        var parametros = $(".parametro_do_"+atributo.id);
        var n = 0;
        jQuery.each(parametros, function (j,parametro) {
            if (parametro.checked) {
                n ++;
            }
        });
        if (n > 0 && n < parametros.length) {
            atributo.indeterminate = true;
        }
    });
    $(".atributo_select_all").on("change", function (e) {
        $('.parametro_do_'+e.target.id).attr('checked', e.target.checked);
    });
});
