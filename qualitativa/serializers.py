from . import models

from rest_framework import serializers


class AnaliseSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Analise
        fields = (
            'pk', 
            'ano', 
            'ano_referencia', 
            'criacao', 
            'ultima_alteracao', 
        )


class AtributoSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Atributo
        fields = (
            'pk', 
            'nome', 
        )


class ParametroSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Parametro
        fields = (
            'pk', 
            'nome', 
        )


class QualificacaoSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Qualificacao
        fields = (
            'pk', 
            'ano', 
            'qualificacao', 
            'ultima_alteracao', 
        )


class AvaliacaoParametroSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.AvaliacaoParametro
        fields = (
            'pk', 
            'mudancas', 
            'justificativa', 
            'ultima_alteracao', 
        )


