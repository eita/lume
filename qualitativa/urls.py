from django.urls import path, include
from rest_framework import routers

from qualitativa.views import SaveQualificacaoView, SaveAvaliacaoParametroView
from . import api
from . import views

app_name = 'qualitativa'


router = routers.DefaultRouter()
router.register(r'analise', api.AnaliseViewSet)
router.register(r'atributo', api.AtributoViewSet)
router.register(r'parametro', api.ParametroViewSet)
router.register(r'qualificacao', api.QualificacaoViewSet)
router.register(r'avaliacaoparametro', api.AvaliacaoParametroViewSet)


urlpatterns = (
    # urls for Django Rest Framework API
    path('api/v1/', include(router.urls)),
)

urlpatterns += (
    path('',
         views.AnaliseListView.as_view(),
         name='analise_list'),
    # View para criação de análise atual e seleção de Referência
    path('create/',
         views.AnaliseQualitativaCreate.as_view(),
         name='analise_create'),
    path('<int:pk>/update',
         views.AnaliseQualitativaUpdate.as_view(),
         name='analise_update'),

    # View para Criação de qualificações e avaliações
    path('<int:pk>',
         views.ComparacaoView.as_view(),
         name='comparacao'),
    path('car/<int:ano_car>/graficos',
         views.QualitativasPainelCarouselGraficosView.as_view(),
         name='painel-graficos'),
    path('<int:pk>/qualitativa-graficos',
         views.QualitativasPainelCarouselGraficosItemView.as_view(),
         name='painel-graficos-item'),
    # View para realizar a deleção de uma análise.
    path('<int:pk>/delete',
         views.analise_delete,
         name='analise_delete'),

    path('save_qualificacao',
         SaveQualificacaoView.as_view(),
         name="saveQualificacao"
         ),
    path('save_avaliacao_parametro',
         SaveAvaliacaoParametroView.as_view(),
         name="saveAvaliacaoParametro"),

    # View para Desabilitar e Habilitar parâmetros
    path('<int:pk>/config', views.parametrosView, name="parametros_select"),
)
