from django.apps import AppConfig


class QualitativaConfig(AppConfig):
    name = 'qualitativa'
