from django.urls import reverse_lazy
from django.utils.text import slugify
from django.forms import modelform_factory, inlineformset_factory
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import (UpdateView,
                                  ListView,
                                  CreateView,
                                  DetailView)
from django.views.generic.edit import DeleteView
from django.utils.translation import gettext as _

from rules.contrib.views import permission_required, PermissionRequiredMixin
from dal import autocomplete
from formsetview.views.mixins import FormSetViewMixin

from agroecossistema.models import Agroecossistema
from main.models import Organizacao, OrganizacaoUsuaria
from main.utils import get_qualitativa_helper_i18n
from linhadotempo.forms import EventoForm
from qualitativa.mixins import AjaxableResponseMixin
from linhadotempo.models import EventoGatilho, Evento
from .models import (Atributo, Parametro, Analise, AnaliseTemplate,
                     Qualificacao, AvaliacaoParametro)
from .forms import (QualificacaoForm, AvaliacaoParametroForm,
                    AnaliseQualitativaModelForm, AnaliseTemplateForm,
                    AnaliseTemplateAtributoModelForm, AnaliseTemplateParametroModelForm,
                    analise_template_parametros_form_set)


def get_agroecossistema_by_analise_id(request, agroecossistema_id, pk):
    return Agroecossistema.objects.get(analise=pk)


class AnaliseQualitativaCreate(PermissionRequiredMixin,
                               CreateView):
    model = Analise
    permission_required = 'permissao_padrao'
    form_class = AnaliseQualitativaModelForm
    template_name = 'qualitativa/aplAnalise.html'

    def get_success_url(self):
        success_url = reverse_lazy('qualitativa:comparacao',
                                   args=(self.object.agroecossistema.id, self.object.id))
        return success_url

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        kwargs['analiseCreateForm'] = context['form']
        kwargs['agroecossistema'] = Agroecossistema.objects.get(
            pk=self.kwargs['agroecossistema_id'])
        kwargs['modulo'] = 'qualitativa'

        return super().get_context_data(**kwargs)

    def get_initial(self):
        initial = super().get_initial()
        initial['usuaria'] = self.request.user
        initial['agroecossistema'] = self.kwargs['agroecossistema_id']
        return initial


class AnaliseQualitativaUpdate(PermissionRequiredMixin,
                               UpdateView):
    model = Analise
    permission_required = 'permissao_padrao'
    form_class = AnaliseQualitativaModelForm
    success_url = reverse_lazy('qualitativa:analise_list')

    def get_success_url(self):
        success_url = reverse_lazy('qualitativa:analise_list',
                                   args=(self.object.agroecossistema.id,))
        return success_url

    def get_context_data(self, **kwargs):
        kwargs['agroecossistema'] = self.object.agroecossistema
        return super().get_context_data(**kwargs)

    def get_initial(self):
        initial = super().get_initial()
        initial['usuaria'] = self.request.user
        return initial


class ComparacaoView(PermissionRequiredMixin, ListView):
    model = Parametro
    form_class = EventoForm
    template_name = 'qualitativa/comparacao.html'
    agroecossistema = None
    analise = None
    atributos_list = None
    permission_required = 'agroecossistema.visualizar'

    def get_analise(self):
        analise_id = self.kwargs['pk']
        return Analise.objects.get(id=analise_id)

    def dispatch(self, request, *args, **kwargs):
        self.agroecossistema = get_object_or_404(
            Agroecossistema, id=kwargs['agroecossistema_id'])
        self.analise = self.get_analise()
        self.atributos_list = Atributo.objects.filter(
            analise_template=self.analise.analise_template)
        return super(ComparacaoView, self).dispatch(request, *args, **kwargs)

    def get_permission_object(self):
        return self.agroecossistema

    def get_queryset(self):
        qs = Parametro.objects.filter(atributo__in=self.atributos_list)
        # Deprecated. TODO: remove this check on parametros_inativos_agroecossistema, which
        # became deprecated after the introduction of analise_templates.
        parametros_inativos_agroecossistema = self.agroecossistema.parametros_inativos.get(
            'inativos', None)
        parametros_inativos_analise = self.analise.parametros_inativos.get(
            'inativos', None)
        parametros_inativos = parametros_inativos_analise if parametros_inativos_analise is not None else parametros_inativos_agroecossistema

        if parametros_inativos:
            try:
                qs = qs.exclude(id__in=parametros_inativos)
            except:
                pass

        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        ano_referencia = Qualificacao.objects.filter(
            ano=self.analise.ano_referencia)
        analises_list = Analise.objects.filter(
            agroecossistema_id=self.agroecossistema.id)
        tem_mudancas_antigo = False

        avaliacoes_parametro = AvaliacaoParametro.objects.filter(
            analise_id=self.analise.id)
        qualificacoes_atuais = Qualificacao.objects.filter(
            agroecossistema_id=self.agroecossistema.id, ano=self.analise.ano)
        qualificacoes_referencia = Qualificacao.objects.filter(agroecossistema_id=self.agroecossistema.id,
                                                               ano=self.analise.ano_referencia)
        parametros_list = self.get_queryset()
        atributos_list = self.atributos_list

        atributosVazios = []
        atributos = {}
        for atributo in self.atributos_list:
            count = 0
            for parametro in parametros_list:
                if atributo.id == parametro.atributo_id:
                    count += 1
            if count == 0:
                atributosVazios.append(atributo.id)
            else:
                atributos[atributo.id] = {}

        if len(atributosVazios) > 0:
            atributos_list = self.atributos_list.exclude(
                id__in=atributosVazios)

        subsections = {}

        for parametro in parametros_list:
            try:
                avaliacao_parametro = avaliacoes_parametro.get(
                    parametro_id=parametro.id)
            except AvaliacaoParametro.DoesNotExist:
                avaliacao_parametro = None

            if avaliacao_parametro is not None and avaliacao_parametro.mudancas and tem_mudancas_antigo == False:
                tem_mudancas_antigo = True

            try:
                qualificacao_atual = qualificacoes_atuais.get(
                    parametro_id=parametro.id).qualificacao
            except Qualificacao.DoesNotExist:
                qualificacao_atual = ''

            try:
                qualificacao_referencia = qualificacoes_referencia.get(
                    parametro_id=parametro.id).qualificacao
            except Qualificacao.DoesNotExist:
                qualificacao_referencia = ''

            mudancas_eventos = [i['id'] for i in avaliacao_parametro.mudancas_eventos.values(
                'id')] if avaliacao_parametro else []

            parametro_i18n = get_qualitativa_helper_i18n(
                parametro, parametro.atributo)
            atributo_i18n = parametro_i18n['atributo']
            subsection = parametro_i18n['subsection']

            atributos[parametro.atributo_id][parametro.id] = {
                'parametro': parametro,
                'parametro_i18n': parametro_i18n['parametro'],
                'atributo_i18n': atributo_i18n,
                'subsection': subsection,
                'qualificacao_referencia': qualificacao_referencia,
                'qualificacao_atual': qualificacao_atual,
                'mudancas': avaliacao_parametro.mudancas if avaliacao_parametro else '',
                'mudancas_eventos': mudancas_eventos,
                'justificativa': avaliacao_parametro.justificativa if avaliacao_parametro else '',
                'ajuda': parametro_i18n['descricao'],
            }

            if parametro.atributo_id not in subsections:
                subsections[parametro.atributo_id] = {}

            if subsection != "" and subsection not in subsections[parametro.atributo_id]:
                subsections[parametro.atributo_id][subsection] = subsection
            elif subsection == "" and parametro.atributo_id not in subsections[parametro.atributo_id]:
                subsections[parametro.atributo_id][parametro.atributo_id] = atributo_i18n

        eventos_linhadotempo = Evento.objects.filter(
            agroecossistema=self.agroecossistema,
            data_inicio__gte=self.analise.ano_referencia,
            data_inicio__lte=self.analise.ano
        ).order_by('data_inicio', 'titulo')

        context['analise'] = self.analise
        context['atributos_list'] = atributos_list
        context['analises_list'] = analises_list
        context['tem_mudancas_antigo'] = tem_mudancas_antigo
        context['atributos'] = atributos
        context['subsections'] = subsections
        context['agroecossistema'] = self.agroecossistema
        context['modulo'] = 'qualitativa'
        context['hide_sidebar'] = False
        context['enable_datatable_export'] = True
        context['eventos_linhadotempo'] = eventos_linhadotempo
        context['form'] = EventoForm(
            prefix=self.agroecossistema.id, instance=None)
        return context


class QualitativasPainelCarouselGraficosView(ComparacaoView):
    template_name = 'qualitativa/painel_carousel_graficos.html'

    def get_analise(self):
        return Analise.objects\
            .filter(agroecossistema=self.agroecossistema)\
            .order_by('-ano_referencia')\
            .first()


class QualitativasPainelCarouselGraficosItemView(ComparacaoView):
    template_name = 'qualitativa/painel_carousel_graficos.html'

    def get_analise(self):
        return Analise.objects.get(pk=self.kwargs['pk'])


@permission_required('agroecossistema.alterar', fn=get_agroecossistema_by_analise_id)
def analise_delete(request, agroecossistema_id, pk):
    analise = Analise.objects.get(id=pk)

    if request.method == "POST":
        ano = analise.ano
        ano_referencia = analise.ano_referencia

        AvaliacaoParametro.objects.filter(analise=analise).delete()
        analise.delete()

        deve_apagar_ano = True
        deve_apagar_ano_referencia = True

        replicas = Analise.objects.filter(
            ano=ano, agroecossistema_id=agroecossistema_id).count()
        if replicas > 0:
            deve_apagar_ano = False

        replicas = Analise.objects.filter(
            ano_referencia=ano, agroecossistema_id=agroecossistema_id).count()
        if replicas > 0:
            deve_apagar_ano = False

        if deve_apagar_ano:
            Qualificacao.objects.filter(
                ano=ano, agroecossistema_id=agroecossistema_id).delete()

        replicas = Analise.objects.filter(
            ano=ano_referencia, agroecossistema_id=agroecossistema_id).count()
        if replicas > 0:
            deve_apagar_ano_referencia = False

        replicas = Analise.objects.filter(
            ano_referencia=ano_referencia, agroecossistema_id=agroecossistema_id).count()
        if replicas > 0:
            deve_apagar_ano_referencia = False

        if deve_apagar_ano:
            Qualificacao.objects.filter(
                ano=ano_referencia, agroecossistema_id=agroecossistema_id).delete()

        return HttpResponseRedirect(reverse_lazy('qualitativa:analise_list', args=(agroecossistema_id,)))
    else:
        context = {
            "analise": analise,
            'modulo': 'qualitativa'
        }
        return render(request, "qualitativa/deletaAnalise.html", context)


class SaveQualificacaoView(PermissionRequiredMixin, AjaxableResponseMixin, UpdateView):
    form_class = QualificacaoForm
    success_message = _("Atualizado!")
    permission_required = 'agroecossistema.alterar'

    def get_permission_object(self):
        data = self.request.POST
        return Agroecossistema.objects.get(pk=data.get('agroecossistema'))

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(SaveQualificacaoView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        # get the existing object or created a new one
        data = self.request.POST

        obj, created = Qualificacao.objects.get_or_create(
            ano=data.get('ano'),
            agroecossistema_id=data.get('agroecossistema'),
            parametro_id=data.get('parametro')
        )

        return obj


class SaveAvaliacaoParametroView(PermissionRequiredMixin, AjaxableResponseMixin, UpdateView):
    form_class = AvaliacaoParametroForm
    permission_required = 'agroecossistema.alterar'

    def get_permission_object(self):
        data = self.request.POST
        return Agroecossistema.objects.get(analise=data.get('analise'))

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(SaveAvaliacaoParametroView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        # get the existing object or created a new one
        data = self.request.POST

        obj, created = AvaliacaoParametro.objects.get_or_create(analise_id=data.get('analise', None),
                                                                parametro_id=data.get('parametro', None))

        return obj

    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        response = super().form_valid(form)
        self.object = form.save()
        mudancas_eventos_list = self.request.POST.getlist('mudancas_eventos[]')
        mudancas_eventos_list = [int(i.replace('.', ''))
                                 for i in mudancas_eventos_list]
        self.object.mudancas_eventos.add(*mudancas_eventos_list)
        return response


class AnaliseListView(PermissionRequiredMixin, ListView):
    model = Analise
    agroecossistema = None
    permission_required = 'agroecossistema.visualizar'

    def get_permission_object(self):
        agroecossistema = get_object_or_404(
            Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    def get_queryset(self):
        return Analise.objects.filter(agroecossistema_id=self.kwargs['agroecossistema_id'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['agroecossistema'] = Agroecossistema.objects.get(
            id=self.kwargs['agroecossistema_id'])
        context['modulo'] = 'qualitativa'
        return context


# @permission_required('agroecossistema.visualizar')
def parametrosView(request, agroecossistema_id, pk):
    agroecossistema = Agroecossistema.objects.get(id=agroecossistema_id)
    analise = Analise.objects.get(id=pk)
    parametros_list_raw = Parametro.objects.filter(
        atributo__analise_template=analise.analise_template)

    if request.method == "POST":
        parametros_inativos = []
        for parametro in parametros_list_raw:
            if str('parametro-' + str(parametro.id)) not in request.POST.keys():
                parametros_inativos.append(parametro.id)

        analise.parametros_inativos.update({'inativos': parametros_inativos})
        analise.save()
        return redirect('qualitativa:analise_list', agroecossistema_id=agroecossistema_id)
    else:
        atributos_list = Atributo.objects.filter(
            analise_template=analise.analise_template)
        parametros_inativos_list = analise.parametros_inativos['inativos'] if 'inativos' in analise.parametros_inativos else [
        ]

        atributos_ativos_list = []
        parametros_list = {}
        for parametro in parametros_list_raw:
            if parametro.atributo_id not in parametros_list:
                parametros_list.update({parametro.atributo_id: []})
            parametros_list[parametro.atributo_id].append(parametro)
            if parametro.id not in parametros_inativos_list and parametro.atributo_id not in atributos_ativos_list:
                atributos_ativos_list.append(parametro.atributo_id)

        context = {
            "analise": analise,
            "atributos_list": atributos_list,
            "parametros_list": parametros_list,
            "atributos_ativos_list": atributos_ativos_list,
            "inativos_list": (analise.parametros_inativos['inativos'] if 'inativos' in analise.parametros_inativos else []),
            'agroecossistema': agroecossistema,
            'modulo': 'qualitativa'
        }

        return render(request, "qualitativa/parametro_list.html", context)


class AnaliseDeleteView(PermissionRequiredMixin, DeleteView):
    model = Analise
    permission_required = 'agroecossistema.apagar'

    def get_permission_object(self):
        return self.get_object().agroecossistema

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(AnaliseDeleteView, self).dispatch(*args, **kwargs)

    def delete(self, request, *args, **kwargs):
        self.get_object().delete()
        payload = {'delete': 'ok'}
        return JsonResponse(payload)


class AtributoAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):

        # TODO considerar user permission
        qs = Atributo.objects.all()

        if self.q:
            qs = qs.filter(nome__istartswith=self.q)

        return qs


class ParametroAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):

        # TODO considerar user permission
        qs = Parametro.objects.all()
        atributo = self.forwarded.get('atributo', None)

        if atributo:
            qs = qs.filter(atributo=atributo)

        if self.q:
            qs = qs.filter(nome__istartswith=self.q)

        return qs


#########################
# ANALISE TEMPLATE VIEWS
#########################

class AnaliseTemplateListView(PermissionRequiredMixin, ListView):
    model = AnaliseTemplate
    permission_required = 'eh_admin_geral'

    def get_permission_object(self):
        return self.request.user

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['select_options'] = list(AnaliseTemplate.objects.all().order_by('nome'))
        context['modulo'] = 'qualitativa'
        return context


class AnaliseTemplateCreateView(PermissionRequiredMixin, CreateView):
    model = AnaliseTemplate
    form_class = AnaliseTemplateForm
    permission_required = 'eh_admin_geral'
    titulo = _('Novo Modelo de análise qualitativa')
    template_name_suffix = '_form_create'

    def get_permission_object(self):
        return self.request.user

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = self.titulo
        context['modulo'] = 'qualitativa'
        return context


class AnaliseTemplateDetailView(PermissionRequiredMixin, DetailView):
    model = AnaliseTemplate
    permission_required = 'eh_admin_geral'

    def get_permission_object(self):
        return self.request.user

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['modulo'] = 'qualitativa'
        return context


class AnaliseTemplateUpdateView(PermissionRequiredMixin,
                                AjaxableResponseMixin,
                                UpdateView):
    model = AnaliseTemplate
    form_class = AnaliseTemplateForm
    permission_required = 'eh_admin_geral'
    titulo = _('Editar Modelo de análise qualitativa')

    def get_permission_object(self):
        return self.request.user

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = self.titulo
        context['modulo'] = 'qualitativa'
        context['forms_atributos'] = []

        for atributo in self.get_object().atributos.all():
            form = AnaliseTemplateAtributoModelForm(instance=atributo)
            # formset = inlineformset_factory(
            #     Atributo, Parametro, extra=1,
            #     form=AnaliseTemplateParametroModelForm
            # )
            formset = inlineformset_factory(
                Atributo,
                Parametro,
                form=AnaliseTemplateParametroModelForm,
                can_delete=True,
                extra=1,
                min_num=0
            )
            context['forms_atributos'].append({
                'obj': atributo,
                'form': form,
                'formset': [{
                    'obj_instance': formset(
                        instance=atributo,
                        prefix=f'parametros_atributo_{atributo.id}'
                    ),
                    'options': {'title': ' ', 'is_modal': False}
                }],
            })

        context['form_atributo_novo'] = AnaliseTemplateAtributoModelForm(
            instance=None)
        context['pode_alterar_agroecossistema'] = True

        return context

    def get_form_kwargs(self):
        kwargs = super(AnaliseTemplateUpdateView, self).get_form_kwargs()
        kwargs.update(
            {'can_delete': self.request.user.has_perm('eh_admin_geral')})
        return kwargs


class AnaliseTemplateAtributoUpdateView(PermissionRequiredMixin,
                                        FormSetViewMixin,
                                        AjaxableResponseMixin,
                                        UpdateView):
    model = Atributo
    form_class = AnaliseTemplateAtributoModelForm
    permission_required = 'eh_admin_geral'
    titulo = _('Editar Modelo de análise qualitativa')
    formsets = [
        (analise_template_parametros_form_set,
         {'title': ' ', 'is_modal': False}),
    ]

    def get_permission_object(self):
        return self.request.user

    def get_formset_prefix(self):
        return f'parametros_atributo_{self.get_object().id}'


class AnaliseTemplateAtributoCreateView(PermissionRequiredMixin,
                                        AjaxableResponseMixin,
                                        CreateView):
    model = Atributo
    form_class = AnaliseTemplateAtributoModelForm
    permission_required = 'eh_admin_geral'
    titulo = _('Adicionar Modelo de análise qualitativa')

    def get_permission_object(self):
        return self.request.user


class DeleteCascadeConfirmViewBase(DeleteView):
    template_name = 'qualitativa/delete_cascade_confirm.html'

    def get_url_admin(self):
        info = self.object._meta.app_label, self.object._meta.model_name

        # admin - delete item - url
        return reverse_lazy('admin:%s_%s_delete' % info, args=(self.object.id,))

    def get_url_form_submit(self):
        info = self.object._meta.app_label, self.object._meta.model_name

        # submit url
        return reverse_lazy('%s:%s-delete' % info, args=(self.object.id,))

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['url_delete_cascade_confirm'] = self.get_url_admin()
        ctx['form_delete_cascade_confirm'] = self.get_url_form_submit()

        return ctx


class AnaliseTemplateAtributoDeleteView(PermissionRequiredMixin,
                                        DeleteCascadeConfirmViewBase):
    model = Atributo
    permission_required = 'eh_admin_geral'

    def get_permission_object(self):
        return self.request.user

    def get_url_form_submit(self):
        return reverse_lazy('analise_template_atributo_delete',
                            args=(self.object.id,))

    def get_success_url(self):

        url_name = 'analise_template_update'
        url_args = self.get_object().analise_template.id

        return reverse_lazy(url_name, args=(url_args,))


class AnaliseTemplateDeleteView(PermissionRequiredMixin,
                                DeleteCascadeConfirmViewBase):
    model = AnaliseTemplate
    permission_required = 'eh_admin_geral'

    def get_permission_object(self):
        return self.request.user

    def get_url_form_submit(self):

        return reverse_lazy('analise_template_delete',
                            args=(self.object.id,))

    def get_success_url(self):

        url_name = 'analise_template_list'

        return reverse_lazy(url_name)

    def dispatch(self, *args, **kwargs):
        if self.get_object().id == 1:
            raise Exception(_('Não é permitido deletar o modelo "Geral"'))
        return super().dispatch(*args, **kwargs)
