from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.postgres.fields import JSONField
from django.core.exceptions import ValidationError
from django.db import models
from django.urls import reverse
from django.utils.text import slugify
from django.utils.translation import gettext as _
from django.utils.translation import gettext_lazy as _lazy
from django_markdown.models import MarkdownField
from django_archive_mixin.managers import ArchiveManager

from anexos.models import AnexoRecipiente
from lume.models import LumeModel
from agroecossistema.models import Agroecossistema
from linhadotempo.models import EventoGatilho
from main.models import Organizacao
from main.messages_from_db import qualitativa_helpers


class AnaliseTemplate(models.Model):
    nome = models.CharField(max_length=120)
    descricao = models.TextField(_("descrição"), blank=True, null=True)
    eh_template_global = models.BooleanField(
        _lazy("É um modelo global"),
        default=True,
        help_text=_lazy("Um modelo global está relacionado a todas às organizações"),
    )
    organizacoes = models.ManyToManyField(
        Organizacao,
        blank=True,
        related_name="analise_templates",
        verbose_name=_("organizações"),
    )
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("última alteração")
    )

    def __str__(self):
        return self.nome

    def get_update_url(self):
        return reverse("analise_template_update", args=(self.pk,))

    def get_absolute_url(self):
        return reverse("analise_template_update", args=(self.pk,))


# Fazer migrate na mão criando o template com nome "Geral", com id 1, e eh_template_global=True.
# OK!
# O campo organizacoes só é usado se o eh_template_global for False
# Quando não tem organizacoes, é pra todo mundo


class Analise(models.Model):
    agroecossistema = models.ForeignKey(
        Agroecossistema, models.CASCADE, verbose_name=_lazy("Agroecossistema")
    )
    ano = models.SmallIntegerField(blank=False, verbose_name=_lazy("Ano"))
    ano_referencia = models.SmallIntegerField(
        blank=False, verbose_name=_lazy("Ano de referência")
    )
    evento_gatilho = models.ForeignKey(
        EventoGatilho,
        models.PROTECT,
        null=True,
        blank=True,
        verbose_name=_lazy("Evento-gatilho"),
    )
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("Criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("Última alteração")
    )
    analise_template = models.ForeignKey(
        AnaliseTemplate,
        models.CASCADE,
        verbose_name=_lazy("Modelo de análise"),
        default=1,
    )
    parametros_inativos = JSONField(
        default=dict, blank=True, verbose_name=_lazy("Parâmetros inativos")
    )

    class Meta:
        unique_together = (("agroecossistema", "ano", "ano_referencia"),)

    def __str__(self):
        aux = str(self.ano_referencia) + " x " + str(self.ano)
        return aux

    def get_update_url(self):
        return reverse(
            "qualitativa:comparacao",
            args=(
                self.agroecossistema_id,
                self.pk,
            ),
        )

    def clean(self):
        if self.ano == None:
            raise ValidationError((_("Ano não pode ser Vazio")))
        else:
            if self.ano_referencia == None:
                raise ValidationError((_("Ano Referência não pode ser Vazio")))
            else:
                if not (self.ano >= self.ano_referencia):
                    raise ValidationError(
                        (_("Ano deve ser maior que Ano de Referência"))
                    )

    def save(self, *args, **kwargs):
        # update ultima_alteracao of agroecossistema:
        self.agroecossistema.save()
        super(Analise, self).save(*args, **kwargs)


class AtributoManager(ArchiveManager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .select_related("analise_template")
            .prefetch_related("parametros_atributo")
        )


class Atributo(LumeModel):
    nome = models.CharField(max_length=90, blank=False, verbose_name=_lazy("nome"))
    analise_template = models.ForeignKey(
        AnaliseTemplate, on_delete=models.CASCADE, default=1, related_name="atributos"
    )
    ordem = models.IntegerField(verbose_name=_lazy("ordem"), default=0)

    objects = AtributoManager()

    class Meta:
        ordering = (
            "analise_template__nome",
            "ordem",
        )

    def __str__(self):
        return self.nome

    def get_absolute_url(self):
        return ""


class ParametroManager(models.Manager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .select_related("atributo", "atributo__analise_template")
        )


class Parametro(models.Model):
    nome = models.CharField(max_length=90, blank=False, verbose_name=_lazy("Nome"))
    atributo = models.ForeignKey(
        Atributo,
        models.CASCADE,
        verbose_name=_lazy("Atributo"),
        related_name="parametros_atributo",
    )
    subsection = models.CharField(
        max_length=200, blank=True, default="", verbose_name=_lazy("Subseção")
    )
    # opcional para parametros personalizados
    agroecossistema = models.ForeignKey(
        Agroecossistema,
        models.CASCADE,
        null=True,
        blank=True,
        verbose_name=_lazy("Agroecossistema"),
    )
    ordem = models.IntegerField(verbose_name=_lazy("Ordem"))
    descricao = MarkdownField(null=True, blank=True, verbose_name=_lazy("Ajuda"))

    objects = ParametroManager()

    class Meta:
        ordering = (
            "atributo__ordem",
            "ordem",
        )

    def __str__(self):
        return self.nome

    def css_class(self):
        if self.subsection != "":
            return "qualitativa-" + slugify(self.subsection)
        return ""

    @property
    def analise_template(self):
        return self.atributo.analise_template

    HELPERS = qualitativa_helpers


class Qualificacao(models.Model):
    agroecossistema = models.ForeignKey(
        Agroecossistema, models.CASCADE, verbose_name=_lazy("agroecossistema")
    )
    parametro = models.ForeignKey(
        Parametro, models.CASCADE, verbose_name=_lazy("parâmetro")
    )
    ano = models.SmallIntegerField(blank=False, verbose_name=_lazy("ano"))
    qualificacao = models.IntegerField(
        blank=True, null=True, verbose_name=_lazy("qualificação")
    )
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("última alteração")
    )

    class Meta:
        unique_together = (("agroecossistema", "parametro", "ano"),)

    def __str__(self):
        aux = str(self.parametro) + " " + str(self.ano)
        return aux

    def get_absolute_url(self):
        return ""

    def save(self, *args, **kwargs):
        # update ultima_alteracao of agroecossistema:
        self.agroecossistema.save()
        super(Qualificacao, self).save(*args, **kwargs)


class AvaliacaoParametro(models.Model):
    analise = models.ForeignKey(
        Analise,
        models.CASCADE,
        related_name="avaliacoes",
        verbose_name=_lazy("análise"),
    )
    parametro = models.ForeignKey(
        Parametro, models.CASCADE, verbose_name=_lazy("parâmetro")
    )
    mudancas = models.TextField(
        null=True, blank=True, verbose_name=_lazy("mudanças (antigo)")
    )
    mudancas_eventos = models.ManyToManyField(
        "linhadotempo.Evento",
        verbose_name=_lazy("mudanças"),
        related_name="parametros_qualitativos_relacionados",
        blank=True,
    )
    justificativa = models.TextField(
        null=True, blank=True, verbose_name=_lazy("justificativa")
    )
    anexos = GenericRelation(
        AnexoRecipiente,
        verbose_name=_lazy("anexos"),
        content_type_field="recipiente_content_type",
        object_id_field="recipiente_object_id",
    )
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("última alteração")
    )

    class Meta:
        unique_together = (("analise", "parametro"),)

    def get_absolute_url(self):
        return ""

    def save(self, *args, **kwargs):
        # update ultima_alteracao of agroecossistema:
        self.analise.agroecossistema.save()
        super(AvaliacaoParametro, self).save(*args, **kwargs)
