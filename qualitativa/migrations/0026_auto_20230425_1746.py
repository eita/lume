# Generated by Django 2.2 on 2023-04-25 20:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0039_auto_20230425_1543'),
        ('qualitativa', '0025_auto_20211029_1116'),
    ]

    operations = [
        migrations.CreateModel(
            name='AnaliseTemplate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=120)),
                ('descricao', models.TextField(blank=True, null=True)),
                ('eh_template_global', models.BooleanField(default=True)),
                ('organizacoes', models.ManyToManyField(blank=True, related_name='analise_templates', to='main.Organizacao')),
            ],
        ),
        migrations.AddField(
            model_name='analise',
            name='analise_template',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='qualitativa.AnaliseTemplate'),
        ),
        migrations.AddField(
            model_name='atributo',
            name='analise_template',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='qualitativa.AnaliseTemplate'),
        ),
    ]
