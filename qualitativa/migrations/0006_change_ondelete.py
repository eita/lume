# Generated by Django 2.1.5 on 2019-04-10 16:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0006_change_ondelete'),
        ('qualitativa', '0005_remove_criterio_css_classes'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='atributo',
            options={'ordering': ('pk',)},
        ),
        migrations.AddField(
            model_name='criterio',
            name='agroecossistema',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.Agroecossistema'),
        ),
    ]
