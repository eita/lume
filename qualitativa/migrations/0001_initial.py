# Generated by Django 2.1.5 on 2019-03-28 20:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Analise',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ano', models.SmallIntegerField()),
                ('ano_referencia', models.SmallIntegerField()),
                ('criacao', models.DateTimeField(auto_now_add=True)),
                ('ultima_alteracao', models.DateTimeField(auto_now=True)),
                ('agroecossistema', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Agroecossistema')),
            ],
        ),
        migrations.CreateModel(
            name='Atributo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=90)),
            ],
        ),
        migrations.CreateModel(
            name='AvaliacaoCriterio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mudancas', models.TextField(blank=True, null=True)),
                ('justificativa', models.TextField(blank=True, null=True)),
                ('ultima_alteracao', models.DateTimeField(auto_now=True)),
                ('analise', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='qualitativa.Analise')),
            ],
        ),
        migrations.CreateModel(
            name='Criterio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=90)),
                ('atributo', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='qualitativa.Atributo')),
            ],
        ),
        migrations.CreateModel(
            name='Qualificacao',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ano', models.SmallIntegerField()),
                ('qualificacao', models.IntegerField(blank=True, null=True)),
                ('ultima_alteracao', models.DateTimeField(auto_now=True)),
                ('agroecossistema', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Agroecossistema')),
                ('criterio', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='qualitativa.Criterio')),
            ],
        ),
        migrations.AddField(
            model_name='avaliacaocriterio',
            name='criterio',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='qualitativa.Criterio'),
        ),
        migrations.AlterUniqueTogether(
            name='qualificacao',
            unique_together={('agroecossistema', 'criterio', 'ano')},
        ),
        migrations.AlterUniqueTogether(
            name='avaliacaocriterio',
            unique_together={('analise', 'criterio')},
        ),
        migrations.AlterUniqueTogether(
            name='analise',
            unique_together={('agroecossistema', 'ano', 'ano_referencia')},
        ),
    ]
