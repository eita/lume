# Generated by Django 2.1.5 on 2019-05-11 17:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('qualitativa', '0017_auto_20190510_1011'),
    ]

    operations = [
        migrations.AlterField(
            model_name='analise',
            name='deleted',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='atributo',
            name='deleted',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='avaliacaoparametro',
            name='deleted',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='parametro',
            name='deleted',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='qualificacao',
            name='deleted',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
