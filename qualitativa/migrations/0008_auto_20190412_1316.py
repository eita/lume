# Generated by Django 2.1.5 on 2019-04-12 13:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('qualitativa', '0007_merge_20190410_1655'),
    ]

    operations = [
        migrations.AddField(
            model_name='analise',
            name='deleted',
            field=models.DateTimeField(editable=False, null=True),
        ),
        migrations.AddField(
            model_name='atributo',
            name='deleted',
            field=models.DateTimeField(editable=False, null=True),
        ),
        migrations.AddField(
            model_name='avaliacaocriterio',
            name='deleted',
            field=models.DateTimeField(editable=False, null=True),
        ),
        migrations.AddField(
            model_name='criterio',
            name='deleted',
            field=models.DateTimeField(editable=False, null=True),
        ),
        migrations.AddField(
            model_name='qualificacao',
            name='deleted',
            field=models.DateTimeField(editable=False, null=True),
        ),
    ]
