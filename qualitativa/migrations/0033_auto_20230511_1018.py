# Generated by Django 2.2 on 2023-05-11 13:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('qualitativa', '0032_auto_20230511_1017'),
    ]

    operations = [
        migrations.AlterField(
            model_name='atributo',
            name='analise_template',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='atributos', to='qualitativa.AnaliseTemplate'),
        ),
    ]
