# Generated by Django 2.1.5 on 2019-04-09 22:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('qualitativa', '0003_criterio_css_classes'),
    ]

    operations = [
        migrations.AddField(
            model_name='criterio',
            name='subsection',
            field=models.CharField(blank=True, default='', max_length=200),
        ),
    ]
