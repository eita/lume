from django import template
from django.utils.text import slugify

from qualitativa.models import Parametro
from main.utils import get_qualitativa_helper_i18n

register = template.Library()


@register.filter(name='get_by_key')
def get_by_key(objectList, key):
    return objectList.get(pk=key)


@register.filter(name='get_atributo_by_key')
def get_atributo_by_key(objectList, key):
    obj = objectList.get(pk=key)
    atributo = slugify(str(obj))
    parametro = Parametro.objects.filter(atributo=obj).first()
    # param = slugify(str(Parametro.objects.filter(atributo=obj).first()))
    parametro_i18n = get_qualitativa_helper_i18n(parametro, obj)
    return parametro_i18n['atributo']


@register.filter(name='get_item')
def get_item(dictionary, key):
    return dictionary.get(str(key))


@register.filter(name='get_obj_attr')
def get_obj_attr(obj, attr):
    return obj[attr]


@register.filter(name='get_dict_by_key')
def get_dict_by_key(obj, attr):
    item = obj.__getattribute__(attr)
    if callable(item):
        return item()
    else:
        return item


# atribuição
# uso: {% define "valor" as var_name %}
# https://stackoverflow.com/a/37755722
@register.simple_tag
def define(val=None):
    return val
