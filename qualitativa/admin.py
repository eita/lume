from django.contrib import admin
from .models import (Analise, Atributo, Parametro, Qualificacao,
                     AvaliacaoParametro, AnaliseTemplate)


class ParametroAdmin(admin.ModelAdmin):
    list_display = ("analise_template",
                    "atributo", "ordem", "nome", )


class AtributoAdmin(admin.ModelAdmin):
    list_display = ("analise_template", "ordem", "nome", )


admin.site.register(Analise)
admin.site.register(Atributo, AtributoAdmin)
admin.site.register(Parametro, ParametroAdmin)
admin.site.register(Qualificacao)
admin.site.register(AvaliacaoParametro)
admin.site.register(AnaliseTemplate)
