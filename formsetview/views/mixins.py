from django.http import JsonResponse
from django.db import transaction
from django.utils.html import strip_tags


class FormSetViewMixin():

    def get_formset_prefix(self):
        return ''

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)

        try:
            data['form_action'] = self.get_form().helper.form_action
        except:
            data['form_action'] = None

        for fs in self.formsets:
            formset_options = ()
            if type(fs) is tuple:

                fs, formset_options = fs

            object_atributo = self.get_object()
            formset = fs(self.model)
            formset_prefix_param = {}
            formset_prefix = self.get_formset_prefix()
            if formset_prefix:
                formset_prefix_param = {'prefix': formset_prefix}

            if self.request.POST:
                formset_params_post = [self.request.POST, self.request.FILES]
                try:
                    formset_instance = formset(
                        *formset_params_post, **formset_prefix_param, instance=object_atributo)
                except:
                    formset_instance = formset(
                        *formset_params_post, **formset_prefix_param)
            else:
                try:
                    formset_instance = formset(
                        **formset_prefix_param, instance=object_atributo)
                except:
                    formset_instance = formset(**formset_prefix_param)

            formset_item = {
                'obj_instance': formset_instance,
                'options': formset_options}

            try:
                data['formsets'].append(formset_item)
            except:
                data['formsets'] = [formset_item]

        return data

    def form_valid(self, form):

        formset_errors = []
        context = self.get_context_data()

        with transaction.atomic():
            self.object = form.save()

            for formset in context['formsets']:
                if formset['obj_instance'].is_valid():
                    formset['obj_instance'].instance = self.object
                    formset['obj_instance'].save()
                else:
                    formset_errors.append(formset['obj_instance'].errors)

        for err_list in formset_errors:
            for err in err_list:
                if not err:
                    continue

                keys = err.keys()
                key = list(keys)[0]
                values = err.values()
                value = list(values)[0]
                value = strip_tags(value)
                value = f"*{key}:* {value}"

                form.add_error(None, value)

        if form.is_valid():
            return super().form_valid(form)
        else:
            return super().form_invalid(form)
