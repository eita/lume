# FormSetView

Aplicação que facilita a inserção de formsets em formulários.

## Requerimentos

* Django >= 2.0

## Utilização

* Adicione em `setting.py`

  ```python
  INSTALLED_APPS = [
    ...

    'formsetview.apps.FormSetViewConfig',

    ...
  ]
  ```

* Faça a configuração em `views.py`

  ```python
  from formsetview.views.mixins import FormSetViewMixin
  ...

  class MyViewUpdateView(FormSetViewMixin, UpdateView):
      model = MyModel
      ...
      formsets = [my_form_set1,
                  (my_form_set2, {'title': 'Titulo verboso', 'is_modal': True, }),]
  ```

* Em `templates` utilize os `templatetags`. Neste exemplo utilizamos `crispy-forms`
  ```html
  {% load formset_tags %}
  ...
  {% if form_action %}
  <form action="{{ form_action }}" method="post">
    {% csrf_token %}
    {{ form|crispy }}

    {% formsets_render formsets %}

    <div class="block-extra-forms">
      {% block extra_form %}
      {% endblock extra_form %}
    </div>
  </form>
  {% else %}
    {% crispy form %}
  {% endif %}
  ...
  {% formsets_scripts formsets %}
  ```

