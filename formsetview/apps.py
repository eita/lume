from django.apps import AppConfig


class FormSetViewConfig(AppConfig):
    name = 'formsetview'

