from django import template

register = template.Library()

@register.inclusion_tag('formsetview.html')
def formsets_render(formsets):
    return {'formsets': formsets}

@register.inclusion_tag('formset_scripts.html')
def formsets_scripts(formsets):
    return {'formsets': formsets}

