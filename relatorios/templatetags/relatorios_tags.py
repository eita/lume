from django import template
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from django.template import Library, loader, Context
from django.utils.translation import gettext as _
from django.utils.html import format_html
from django.urls import reverse
from django.db.models.query import QuerySet
from django.conf import settings
from django.utils.translation import gettext as _
import calendar

from economica.models import Analise
from economica.utils import oo

register = template.Library()


@register.simple_tag()
def tabela_relatorio(
    totais,
    titulo,
    template_name,
    targetId,
    agroecossistema_nome=None,
    car=None,
    addButtonToAnalise=False,
):
    if totais:
        t = loader.get_template(
            "relatorios/inclusiontags/tabela_" + template_name + ".html"
        )
        # if titulo:
        #    titulo += ' do agroecossistema ' + agroecossistema_nome + ' (ciclo ' + car.__str__() + ')'

        if (
            template_name in totais.RELATORIOS
            and "vars" in totais.RELATORIOS[template_name]
        ):
            vars = totais.RELATORIOS[template_name]["vars"]
            status = get_status(vars, totais)
        else:
            status = None
            vars = None

        return t.render(
            {
                "titulo": titulo,
                "totais": totais,
                "status": status,
                "vars": vars,
                "targetId": targetId,
                "agroecossistema": agroecossistema_nome,
                "car": car,
                "composicoes_nsga": (
                    car.composicoes_nsga.all().select_related("pessoa") if car else None
                ),
                "DEBUG_VARS": settings.DEBUG_VARS,
            }
        )
    else:
        return _("Por favor preencha os dados da análise.")


@register.simple_tag()
def mostra_vars(totais, vars):
    html = ""
    for var in vars:
        value = (
            totais.get(var, None)
            if isinstance(totais, dict)
            else getattr(totais, var, None)
        )
        # totais.__getattribute__(var)
        if not isinstance(value, QuerySet):
            html += format_html('<span class="badge badge-dark mr-2">{}</span>', var)
    return format_html(html)


@register.inclusion_tag("relatorios/inclusiontags/tabela_relatorio_wrapper.html")
def tabela_relatorio_wrapper(
    totais, car, titulo, template_name, vars, titulo_tab, arquivo_pre
):
    return {
        "totais": totais,
        "car": car,
        "titulo": titulo,
        "template_name": template_name,
        "filename": arquivo_pre + "_" + template_name,
        "status": get_status(vars, totais),
        "vars": vars,
        "DEBUG_VARS": settings.DEBUG_VARS,
    }


@register.inclusion_tag("economica/inclusiontags/grafico_analise_detail.html")
def grafico_analise_detail(
    totais,
    titulo,
    id,
    vars,
    indice=None,
    funcao_js=None,
    cols=1,
    totais_varname="totais",
    subtitulo_varname="subtitulo",
    comparacao=False,
    ordem=1,
    legend_id="",
):
    grafico_titulo = titulo
    grafico_id = "grafico_" + id + "_" + str(ordem) if comparacao else "grafico_" + id
    grafico_funcao_js = "inicializa_grafico_" + id if funcao_js is None else funcao_js
    grafico_class = "col-md-12 col-lg-10 col-xl-9" if cols == 1 else "col-md-6"
    grafico_class = "col-12" if cols == 1.5 else grafico_class

    return {
        "grafico_titulo": grafico_titulo,
        "grafico_id": grafico_id,
        "grafico_funcao_js": grafico_funcao_js,
        "grafico_class": grafico_class,
        "status": get_status(vars, totais),
        "vars": vars,
        "totais": totais,
        "DEBUG_VARS": settings.DEBUG_VARS,
        "totais_varname": totais_varname,
        "subtitulo_varname": subtitulo_varname,
        "comparacao": comparacao,
        "ordem": ordem,
        "legend_id": legend_id,
        "indice": indice,
    }


@register.inclusion_tag("economica/inclusiontags/item_lista_graficos.html")
def item_lista_graficos(totais, i, itens, cols=1):
    item = itens[i - 1]
    abrediv = True
    fechadiv = True

    padding_top = ""
    if item.get("cols") == 2 and i == 1:
        abrediv = True
        fechadiv = False
        padding_top = " pt-5"
    elif item.get("cols") == 2:
        item_anterior = itens[i - 2]
        n_single_cols = 0
        for j in range(i):
            if itens[j].get("cols") == 1:
                n_single_cols += 1

        if n_single_cols % 2 == 0:
            abrediv = i % 2 != 0
            fechadiv = i == len(itens) or i % 2 == 0
        else:
            abrediv = i % 2 == 0
            fechadiv = i == len(itens) or i % 2 != 0

    return {
        "abrediv": abrediv,
        "fechadiv": fechadiv,
        "item": item,
        "cols": cols,
        "padding_top": padding_top,
        "totais": totais,
        "indice": i,
    }


def get_status(vars, totais):
    status = _("Completo")
    if len(vars) > 0:
        n = 0
        z = 0
        for var in vars:
            value = (
                totais.get(var, None)
                if isinstance(totais, dict)
                else getattr(totais, var, None)
            )
            if isinstance(value, QuerySet):
                if value.count() == 0:
                    n += 1
            else:
                v = value() if callable(value) else value
                if v is None:
                    n += 1
                elif v == 0:
                    z += 1

        t = n + z
        if t == len(vars):
            status = _("Vazio")
        elif n > 0:
            status = _("Incompleto")
    return status


@register.filter(name="vezes")
def vezes(a, b):
    return oo([a, b], "*")


@register.filter(name="dividido")
def dividido(a, b):
    return oo([a, b])
