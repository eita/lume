from datetime import date

from django.db import models
from django.db.models.base import Model
from django.utils.functional import cached_property
from django.utils.translation import gettext as _
from django.utils.translation import gettext_lazy as _lazy
from django.utils.text import slugify

from main.messages_from_db import ajuda_indicador_helpers
from main.models import AjudaIndicador
from agroecossistema.models import Pessoa, ComposicaoNsga
from economica.models import Subsistema, Analise
from economica.utils import *
from main.utils import getHelperI18n


# Database View
class TotaisAnaliseEconomica(Model):
    area = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('área'))
    area_propria = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('área própria'))
    area_outros = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('área de terceiros'))
    area_comunitaria = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('área comunitária'))
    area_posse = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('área de posse'))
    area_arrendamento = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('área de arrendamento'))
    area_meacao = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('área de meação'))
    area_parceria = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('área de parceria'))
    area_cessao = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('área em cessão'))
    area_comodato = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('área em comodato'))
    area_direitodeuso = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('área de direito de uso'))
    area_usocomunitario = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('área de uso comunitário'))
    patrimonio_vivo_inicio = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('patrimônio vivo inicial'))
    patrimonio_vivo_final = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('patrimônio vivo final'))
    patrimonio_vivo_variacao = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('patrimônio vivo - variação'))
    patrimonio_fundiario_inicio = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('patrimônio fundiário inicial'))
    patrimonio_fundiario_final = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('patrimônio fundiário final'))
    patrimonio_fundiario_variacao = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('patrimônio fundiário - variação'))
    patrimonio_equipamentos_inicio = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('patrimônio equipamentos inicial'))
    patrimonio_equipamentos_final = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('patrimônio equipamentos final'))
    patrimonio_equipamentos_variacao = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('patrimônio equipamentos - variação'))
    rna_pluriatividade = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('rna - pluriatividade'))
    rna_transferencia = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('rna - transferência'))
    rna_valor_total = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('rna - valor total'))
    estoqueinsumos_inicio = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('estoque insumos inicial'))
    estoqueinsumos_final = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('estoque insumos final'))
    estoqueinsumos_variacao = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('estoque insumos - variação'))
    pt = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('pt'))

    RELATORIOS = {
        'indicadores1': {
            'id': 'indicadores1',
            'titulo_tab': _('Indicadores I'),
            'titulo': _('Indicadores Econômicos'),
            'tipo_tab': 'tabelas',
            'itens': [
                {
                    'titulo': _('Tabela 01: Composição da renda do NSGA em valores absolutos e relativos'),
                    'template': 'indicadores1_01',
                    'vars': ['total_ra', 'total_ra_nao_extrativistas', 'total_ra_extrativistas', 'rna_valor_total', 'rna_pluriatividade', 'rna_transferencia', 'renda_familiar_total']
                },
                {
                    'titulo': _('Tabela 02: Síntese do inventário patrimonial'), 'template': 'indicadores1_02',
                    'vars': ['patrimonio_fundiario_inicio', 'patrimonio_fundiario_final', 'patrimonio_equipamentos_inicio', 'patrimonio_equipamentos_final', 'patrimonio_vivo_inicio', 'patrimonio_vivo_final']
                },
                {
                    'titulo': _('Tabela 03: Produto Bruto e Custos de Produção do Agroecossistema e de seus Subsistemas'),
                    'template': 'indicadores1_03',
                    'vars': ['pt', 'area', 'total_pb_venda', 'total_pb_autoconsumo', 'total_pb_doacoes_trocas', 'total_pb_estoque', 'total_pb', 'total_ci_t', 'total_ci_f', 'total_ci', 'total_pt_t', 'total_pt_f', 'total_pt', 'total_cp']},
                {
                    'titulo': _('Tabela 04: Indicadores econômicos do Agroecossistema e de seus Subsistemas'),
                    'template': 'indicadores1_04',
                    'vars': ['pt', 'total_rb', 'total_va', 'total_vat', 'total_ra', 'total_valor_agregado', 'total_produtividade_terra', 'total_ra_area', 'total_ram', 'total_rentabilidade_monetaria_liquida', 'total_rentabilidade_total', 'total_endogeneidade', 'total_apropriacao_valor_agregado', 'total_mercantilizacao']
                },
                {
                    'titulo': _('Tabela 05: Número de Horas Trabalhadas no Agroecossistema e nos seus Subsistemas segundo as/os integrantes do NSGA'),
                    'template': 'indicadores1_05',
                    'vars': ['totais_ss_tempostrabalho', 'horas_mercantil_total']
                },
                {
                    'titulo': _('Tabela 06: Indicadores de Produtividade do Trabalho no Agroecossistema e nos seus Subsistemas'),
                    'template': 'indicadores1_06',
                    'vars': ['total_va', 'total_produtividade_do_trabalho_1', 'total_produtividade_do_trabalho_2', 'total_intensidade_ocupacao_trabalho_porarea', 'total_ra_por_ht']
                }
            ]
        },
        'indicadores2': {
            'id': 'indicadores2',
            'titulo_tab': _('Indicadores II'),
            'titulo': _('Indicadores de ocupação econômica'),
            'tipo_tab': 'tabelas',
            'itens': [
                {
                    'titulo': _('Tabela 07: Composição do NSGA (sexo, parentesco, idade e UTF)'),
                    'template': 'indicadores2_07',
                    'vars': []
                },
                {
                    'titulo': _('Tabela 08: Repartição do tempo e da renda por esfera de ocupação econômica por integrante do NSGA'),
                    'template': 'indicadores2_08',
                    'vars': []
                },
                {
                    'titulo': _('Tabela 09: Equivalência do trabalho anual do NSGA, por pessoa, em jornadas de 8 horas trabalhadas e em Unidade de Trabalho Contratada (UTC)'),
                    'template': 'indicadores2_09',
                    'vars': []
                },
                {
                    'titulo': _('Tabela 10 : Equivalência do trabalho anual do NSGA, por gênero e geração, em horas trabalhadas, por Unidade de Trabalho Contratada (UTC) e Renda ($)'),
                    'template': 'indicadores2_10',
                    'vars': []
                },
                {
                    'titulo': _('Tabela 11: Repartição da renda por esfera de trabalho por gênero e UTF'),
                    'template': 'indicadores2_11',
                    'vars': []
                }
            ]
        },
        'quadroSintese': {
            'id': 'quadroSintese',
            'titulo_tab': _('Quadro Síntese'),
            'titulo': _('Quadro Síntese'),
            'vars': ['total_cp', 'total_rentabilidade_monetaria_bruta', 'total_pb_venda', 'total_pb_insumos_produzidos', 'total_mercantilizacao', 'total_pb_autoconsumo', 'total_reciprocidade', 'total_pp', 'total_pb_doacoes_trocas', 'total_recursos_reproduzidos']
        },
        'graficosRenda': {
            'id': 'graficosRenda',
            'titulo_tab': _('Gráficos Renda'),
            'titulo': _('Gráficos de renda'),
            'tipo_tab': 'graficos',
            'itens': [
                {
                    'titulo': _('Origem das Rendas'),
                    'id': 'origem_rendas',
                    'vars': ['total_ra', 'rna_pluriatividade', 'rna_transferencia']
                },
                {
                    'titulo': _('Composição das Rendas'),
                    'titulo_agregada': _('Composição Média das Rendas'),
                    'id': 'composicao_rendas',
                    'vars': ['total_pb', 'total_pb_estoque', 'total_pb_doacoes_trocas', 'total_pb_autoconsumo', 'total_pb_venda', 'total_ci', 'total_va', 'total_ci_f', 'total_vat', 'total_cp', 'total_ra', 'total_ram']
                },
                {
                    'titulo': _('Composição das Rendas por Hectare'),
                    'id': 'composicao_rendas_por_area',
                    'vars': ['total_pb', 'area', 'total_pb_estoque', 'total_pb_doacoes_trocas', 'total_pb_autoconsumo', 'total_pb_venda', 'total_ci', 'total_va', 'total_ci_f', 'total_vat', 'total_cp', 'total_ra', 'total_ram']
                },
                {
                    'titulo': _('Composição do Produto Bruto'),
                    'id': 'composicao_produto_bruto',
                    'vars': ['total_pb_estoque', 'total_pb_doacoes_trocas', 'total_pb_autoconsumo', 'total_pb_venda']
                },
                {
                    'titulo': _('Composição da Renda Bruta I'),
                    'precisa_subsistemas': True,
                    'id': 'composicao_renda_bruta_1',
                    'vars': ['total_ci', 'total_va']
                },
                {
                    'titulo': _('Composição da Renda Bruta II'),
                    'id': 'composicao_renda_bruta_2',
                    'vars': ['total_ci_f', 'total_vat']
                },
                {
                    'titulo': _('Composição da Renda Bruta Monetária'),
                    'id': 'composicao_renda_bruta_monetaria',
                    'vars': ['total_ram', 'total_cp']
                },
                {
                    'titulo': _('Conjunto das Vendas por Tipo de Mercado'),
                    'id': 'producoes_por_venda',
                    'precisa_totais_por_produto': True,
                    'vars': ['total_pb_venda']
                },
                {
                    'titulo': _('Conjunto das Produções I (venda, autoconsumo, doações, estoque)'),
                    'id': 'producoes1',
                    'precisa_totais_por_produto': True,
                    'vars': ['total_pb', 'total_pb_venda', 'total_pb_autoconsumo', 'total_pb_doacoes_trocas', 'total_pb_estoque']
                },
                {
                    'titulo': _('Conjunto das Produções II (venda, autoconsumo, doações, estoque)'),
                    'id': 'producoes2',
                    'precisa_totais_por_produto': True,
                    'vars': ['total_pb']
                },
                {
                    'titulo': _('Conjunto das Produções Vendidas'),
                    'id': 'producoes_venda',
                    'precisa_totais_por_produto': True,
                    'vars': ['total_pb_venda']
                },
                {
                    'titulo': _('Conjunto das Produções Autoconsumidas'),
                    'id': 'producoes_autoconsumo',
                    'precisa_totais_por_produto': True,
                    'vars': ['total_pb_autoconsumo']
                },
                {
                    'titulo': _('Intensidade do Uso Efetivo da Terra - VA (R$) /  Área (ha)'),
                    'id': 'efetivo_va_area',
                    'precisa_subsistemas': True,
                    'vars': ['total_va']
                },
                {
                    'titulo': _('Intensidade do Uso Proporcional da Terra - VA (R$) /  Área (ha)'),
                    'id': 'proporcional_va_area',
                    'vars': ['total_va']
                },
                {
                    'titulo': _('Intensidade de Trabalho - Valor Agregado por Hora de Trabalho nos Subsistemas'),
                    'id': 'intensidade_trabalho',
                    'vars': ['total_va', 'horas_total_total']
                },
                {
                    'titulo': _('Intensidade do Uso da Terra por Unidade de Trabalho VA/ha/UTF'),
                    'id': 'intensidade_uso_terra',
                    'vars': ['total_intensidade_ocupacao_trabalho_porarea', 'total_produtividade_do_trabalho_2']
                }
            ]
        },
        'graficosTrabalho': {
            'id': 'graficosTrabalho',
            'titulo_tab': _('Gráficos Trabalho'),
            'titulo': _('Gráficos de trabalho'),
            'tipo_tab': 'graficos',
            'itens': [
                {
                    'titulo': _('Repartição da Renda por Pessoa e por Esfera de Trabalho'),
                    'id': 'rendas_totais_pessoa_et',
                    'vars': ['horas_total_total', 'renda_familiar_total']
                },
                {
                    'titulo': _('Repartição da Renda por Esfera de Trabalho entre responsáveis do NSGA'),
                    'id': 'rendas_et_pessoa_responsavel',
                    'vars': ['horas_total_total', 'renda_familiar_total']
                },
                {
                    'titulo': _('Repartição da Renda por Gênero e Geração e por Esfera de Trabalho'),
                    'id': 'rendas_genero_geracao_et',
                    'vars': ['horas_total_total']
                },
                {
                    'titulo': _('Repartição da Renda por Gênero e por Esfera de Trabalho'),
                    'id': 'rendas_totais_genero_et',
                    'vars': ['horas_total_total']
                },
                {
                    'titulo': _('Repartição da Renda por Esfera de Trabalho e por Gênero'),
                    'id': 'rendas_totais_et_genero',
                    'vars': ['horas_total_total']
                },
                {
                    'titulo': _('Repartição da Renda por Esfera de Ocupação por Gênero e por UTF'),
                    'id': 'rendas_totais_genero_et_UTF',
                    'vars': ['horasporutf_total_total']
                },
                {
                    'titulo': _('Repartição do Tempo de Trabalho por Pessoa e por Esfera de Trabalho (UTC)'),
                    'id': 'utc_pessoa_et',
                    'vars': ['horas_total_total']
                },
                {
                    'titulo': _('Repartição Proporcional do Tempo de Trabalho por Gênero e por Esfera de Trabalho (UTC)'),
                    'titulo_agregada': _('Repartição Proporcional do Tempo Médio de Trabalho por Gênero e por Esfera de Trabalho (UTC)'),
                    'id': 'utc_genero_et',
                    'vars': ['utc_total_total']
                },
                {
                    'titulo': _('Repartição do Tempo de Trabalho por Gênero e Geração e por Esfera de Trabalho (Tempo equivalente a 8h/dia)'),
                    'id': 'dias_genero_et',
                    'vars': ['dias_mercantil_total', 'dias_domestico_cuidados_total', 'dias_participacao_social_total', 'dias_pluriatividade_total']
                },
                {
                    'titulo': _('Repartição do Tempo de Trabalho por Pessoa e por subsistema (horas)'),
                    'id': 'utc_pessoa_subsistema',
                    'vars': ['totais_ss_tempostrabalho', 'horas_mercantil_total']
                },
                {
                    'titulo': _('Repartição do Valor Agregado do Autoconsumo por UTF e por Subsistema'),
                    'id': 'va_autoconsumo_por_utf',
                    'vars': ['total_va_autoconsumo_por_utf_total', 'utf_total_total']
                },
                {
                    'titulo': _('Repartição do Valor Agregado Mercantil por UTF e por Subsistema'),
                    'id': 'va_mercantil_por_utf',
                    'vars': ['total_va_mercantil_por_utf_total', 'utf_total_total']
                },
                {
                    'titulo': _('Repartição do Valor Agregado do Autoconsumo e Mercantil por UTF e por Subsistema'),
                    'id': 'va_aem_por_utf',
                    'vars': ['total_va_autoconsumo_por_utf_total', 'total_va_mercantil_por_utf_total']
                },
                # {
                #     'titulo': _('Repartição da Renda por Gênero e por Esfera de Trabalho entre responsáveis do NSGA'),
                #     'titulo_agregada': _('Repartição Proporcional da Renda Média por Gênero e por Esfera de Trabalho entre responsáveis dos NSGAs'),
                #     'id': 'rendas_responsaveis_et',
                #     'vars': ['horas_total_total']
                # },
                # {
                #     'titulo': _('Repartição da Renda por Gênero e por Esfera de Trabalho entre integrantes do NSGA'),
                #     'id': 'rendas_genero_et',
                #     'vars': ['horas_total_total']
                # },
            ]
        },
        'graficosReciprocidade': {
            'id': 'graficosReciprocidade',
            'titulo_tab': _('Gráficos Reciprocidade'),
            'titulo': _('Reciprocidade Ecológica e Social'),
            'tipo_tab': 'graficos',
            'itens': [
                {
                    'titulo': _('Reciprocidade Ecológica (insumos)'),
                    'id': 'reciprocidade_ecologica',
                    'vars': ['estoqueinsumos_inicio', 'estoqueinsumos_final', 'total_pp', 'total_pb_insumos_produzidos']
                },
                {
                    'titulo': _('Reciprocidade Social - Entrada e saída de produtos e serviços por reciprocidade'),
                    'id': 'reciprocidade_social',
                    'vars': ['total_reciprocidade', 'total_pb_doacoes_trocas']
                }
            ]
        },
        'diagramaSintese': {
            'id': 'diagramaSintese',
            'titulo_tab': _('Diagrama Síntese'),
            'titulo': _('Diagrama Síntese'),
            'vars': ['total_cp', 'total_rentabilidade_monetaria_bruta', 'total_pb_venda', 'total_pb_insumos_produzidos', 'total_mercantilizacao', 'total_pb_autoconsumo', 'total_reciprocidade', 'total_pp', 'total_pb_doacoes_trocas', 'total_recursos_reproduzidos']
        }
    }

    COMPARACOES = [
        {
            'key_tab': 'graficosRenda',
            'id': 'composicao_rendas',
            'titulo': _('Comparação da composição das rendas do agroecossistema')
        },
        {
            'key_tab': 'graficosRenda',
            'id': 'composicao_renda_bruta_1',
            'titulo': _('Comparação da composição  da renda bruta (VA/CI) do agroecossistema e dos subsistemas')
        },
        {
            'key_tab': 'graficosTrabalho',
            'id': 'rendas_responsaveis_et',
            'titulo': _('Comparação da repartição da renda por gênero e esfera de trabalho')
        },
        {
            'key_tab': 'graficosTrabalho',
            'id': 'utc_genero_et',
            'titulo': _('Comparação da repartição proporcional do tempo de trabalho por gênero e por esfera de trabalho (UTC)')
        },
        {
            'key_tab': 'graficosRenda',
            'id': 'producoes2',
            'titulo': _('Comparação do conjunto das produções')
        },
        {
            'key_tab': 'graficosRenda',
            'id': 'producoes_venda',
            'titulo': _('Comparação das produções vendidas')
        },
        {
            'key_tab': 'graficosRenda',
            'id': 'producoes_autoconsumo',
            'titulo': _('Comparação das produções autoconsumidas')
        },
        {
            'key_tab': 'graficosRenda',
            'id': 'producoes1',
            'titulo': _('Comparação do conjunto das produções (total e por destinação)')
        },
        {
            'key_tab': 'graficosRenda',
            'id': 'efetivo_va_area',
            'titulo': _('Comparação da intensidade do uso efetivo da terra (VA[R$]/Área[ha])')
        }
    ]

    # Relationship Fields
    analise = models.OneToOneField(
        Analise, verbose_name=_lazy('análise'),
        related_name="totais",
        on_delete=models.DO_NOTHING
    )


    class Meta:
        managed = False

    # ==================================================================================
    # metodos de tempo - getters são criados dinamicamente na inicialização do objeto

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # inicialização de alguns métodos dinamicos para pegar tempo de trabalho
        # principio: cria um metodo e faz bind
        # a.__dict__['blabla'] = blabla.__get__(a)
        # https://stackoverflow.com/a/28060251/1216027
        self.cached_tempotrabalho = {}
        self.cached_ajuda_indicadores = self.get_all_ajuda_indicadores()

    def get_glossario(self):
        ajudaIndicadores = []
        for indicador in AjudaIndicador.objects.all():
            helperI18n = getHelperI18n('ajuda_indicador_helpers', indicador, indicador.nome)
            ajudaIndicadores.append({
                'id': indicador.id,
                'slug': indicador.slug,
                'nome': helperI18n['nome'],
                'formula': indicador.getFormula(),
                'ajuda': helperI18n['ajuda'],
                'ajuda_curta': helperI18n['ajuda_curta'],
            })
        return ajudaIndicadores

    def get_all_ajuda_indicadores(self):
        ajuda_indicadores = AjudaIndicador.objects.all()
        cached_ajuda_indicadores = {}
        for indicador in ajuda_indicadores:
            helperI18n = getHelperI18n('ajuda_indicador_helpers', indicador, indicador.nome)
            key = slugify(indicador.slug.lower()) if indicador.slug else slugify(indicador.nome.lower())
            cached_ajuda_indicadores[key] = {
                'id': indicador.id,
                'slug': indicador.slug,
                'nome': helperI18n['nome'],
                'formula': indicador.getFormula(),
                'ajuda': helperI18n['ajuda'],
                'ajuda_curta': helperI18n['ajuda_curta'],
            }
        return cached_ajuda_indicadores

    def get_ajuda_indicador(self, slug):
        slugLower = slugify(slug.lower())
        if slugLower in self.cached_ajuda_indicadores:
            return self.cached_ajuda_indicadores[slugLower]
        return None

    # categoria: horas_pluriatividade, horas_participacao_social, horas_domestico_cuidados, horas_mercantil,
    #           horas_total, utf_pluriatividade, utf_agroecossistema, utf_total
    # categoria_pessoa: af, am, jf, jm, of, om, c, f, m, total
    def tempotrabalho(self, tipo, categoria, pessoa):
        cached = self.cached_tempotrabalho.get(tipo + '_' + categoria + '_' + pessoa)
        if cached:
            return cached

        cat = tipo + '_' + categoria if tipo != '' else categoria
        vals = []
        if cat in ['horas_mercantil']:
            vals.append(osum('horas_' + pessoa, self.totais_ss_tempostrabalho))
        tot = oo(vals, '+')

        if cat in ['horas_pluriatividade', 'horas_participacao_social', 'horas_domestico_cuidados',
                   'utf_pluriatividade', 'utf_agroecossistema', 'utf_domestico_cuidados', 'utf_participacao_social',
                   'utf_mercantil', 'utf_total', 'horas_total', 'pessoas']:
            for tt in self.totais_tempostrabalho:
                if len(pessoa) == 2:
                    tt_categoria = pessoa[0].upper()
                    sexo = pessoa[1].upper()
                    if tt.categoria == tt_categoria and tt.sexo == sexo:
                        tot = oo([tot, tt.__dict__[cat]], '+')
                elif len(pessoa) == 1:
                    if pessoa == 'c' and tt.categoria == 'C':
                        tot = oo([tot, tt.__dict__[cat]], '+')
                    elif tt.sexo == pessoa.upper():
                        tot = oo([tot, tt.__dict__[cat]], '+')
                elif pessoa == 'total':
                    tot = oo([tot, tt.__dict__[cat]], '+')

        if tot is not None:
            tot = Decimal(tot)
        self.cached_tempotrabalho[cat + '_' + pessoa] = tot
        return tot

    def tempo_dias(self, categoria, pessoa):
        horas = self.tempotrabalho('horas', categoria, pessoa)
        return oo([horas, 8])

    def tempo_reparticao(self, categoria, pessoa):
        horas_total = self.tempotrabalho('horas', 'total', 'total')
        horas_categoria = self.tempotrabalho('horas', categoria, pessoa)
        a = oo([self.total_ra, self.rna_pluriatividade], '+')
        num = oo([a, horas_categoria], '*')
        tempo_reparticao = oo([num, horas_total])
        return tempo_reparticao

    def tempo_totalutc(self, categoria, pessoa):
        num = oo([self.tempotrabalho('horas', 'total', pessoa), self.tempotrabalho('horas', categoria, pessoa)], '*')
        den = oo([self.tempo_utc('total', pessoa), self.tempotrabalho('horas', 'total', pessoa), 2420], '*')
        return oo([num, den])

    def tempo_totalutf(self, categoria, pessoa):
        tempo_dias = self.tempo_dias(categoria, pessoa)
        utf = self.tempotrabalho('utf', 'agroecossistema', pessoa)
        return oo([tempo_dias, utf])

    def tempo_percentutf(self, categoria, pessoa):
        num = self.tempo_totalutf(categoria, pessoa)
        den = oo([self.tempo_totalutf('total', 'm'), self.tempo_totalutf('total', 'f')], '+')
        return oo([num, den])

    def tempo_utc(self, categoria, pessoa):
        total_horas = self.tempotrabalho('horas', categoria, pessoa)
        return oo([total_horas, 2420])

    def renda(self, categoria, pessoa):
        total_horas = self.tempotrabalho('horas', categoria, pessoa)
        return oo([total_horas, self.valor_hora], '*')

    def rendaporutf(self, categoria, pessoa):
        return oo([self.renda(categoria, pessoa), self.tempotrabalho('utf', 'total', pessoa)])

    def tempo_htdia(self, pessoa):
        total_horas = self.tempotrabalho('horas', 'total', pessoa)
        return oo([total_horas, 365])

    # ====================================================================
    # Referências para os outros objetos totalizadores - busco eles por uma relação de foreign key

    @cached_property
    def totais_tempostrabalho(self):
        try:
            return self.totais_tempostrabalho_f.all()
        except TotaisSubsistema.totais_tempostrabalho_f.RelatedObjectDoesNotExist:
            return None

    @cached_property
    def totais_subsistemas(self):
        return self.totais_subsistemas_f.all().prefetch_related('totais_tempostrabalho_f', 'totais_analise_f')

    @cached_property
    def totais_tempo_trabalho_subsistemas(self):
        try:
            return self.analise.get_totais_tempostrabalho()
        except TotaisSubsistema.analise.RelatedObjectDoesNotExist:
            return None

    @cached_property
    def totais_subsistemas_extrativistas(self):
        ne = []
        for ss in self.totais_subsistemas:
            if ss.extrativismo:
                ne.append(ss)
        return ne

    @cached_property
    def totais_subsistemas_nao_extrativistas(self):
        ne = []
        for ss in self.totais_subsistemas:
            if not ss.extrativismo:
                ne.append(ss)
        return ne

    @cached_property
    def totais_ss_tempostrabalho(self):
        return self.totais_ss_tempostrabalho_f.all()

    @cached_property
    def horas_trabalho_por_pessoa(self):
        return self.totais_horastrabalhoporpessoa_f.all()

    # ============================================================================
    # Cálculos dos valores totais para o Agroecossistema

    @cached_property
    def total_ci(self):
        return osum('ci', self.totais_subsistemas)

    @cached_property
    def total_ci_f(self):
        return osum('ci_f', self.totais_subsistemas)

    @cached_property
    def total_ci_t(self):
        return osum('ci_t', self.totais_subsistemas)

    @cached_property
    def total_pp(self):
        return osum('pp', self.totais_subsistemas)

    @cached_property
    def total_pt_f(self):
        return osum('pt_f', self.totais_subsistemas)

    @cached_property
    def total_pt_t(self):
        return osum('pt_t', self.totais_subsistemas)

    @cached_property
    def total_pt(self):
        a = osum('pt', self.totais_subsistemas)
        return oo([a, self.pt], '+')

    @cached_property
    def total_pb_venda_convencional(self):
        return osum('pb_venda_convencional', self.totais_subsistemas)

    @cached_property
    def total_pb_venda_territorial(self):
        return osum('pb_venda_territorial', self.totais_subsistemas)

    @cached_property
    def total_pb_venda_institucional(self):
        return osum('pb_venda_institucional', self.totais_subsistemas)

    @cached_property
    def total_pb_venda(self):
        return oo([self.total_pb_venda_convencional, self.total_pb_venda_territorial, self.total_pb_venda_institucional], '+')

    @cached_property
    def total_pb_autoconsumo(self):
        return osum('pb_autoconsumo', self.totais_subsistemas)

    @cached_property
    def total_pb_doacoes_trocas(self):
        return osum('pb_doacoes_trocas', self.totais_subsistemas)

    @cached_property
    def total_pb_estoque(self):
        return osum('pb_estoque', self.totais_subsistemas)

    @cached_property
    def total_pb_insumos_produzidos(self):
        return osum('pb_insumos_produzidos', self.totais_subsistemas)

    @cached_property
    def total_reciprocidade(self):
        return osum('reciprocidade', self.totais_subsistemas)

    # not done
    @cached_property
    def total_pb(self):
        return osum('pb', self.totais_subsistemas)

    @cached_property
    def total_rb(self):
        return oo([self.total_pb_venda, self.total_pb_autoconsumo, self.total_pb_doacoes_trocas], '+')

    @cached_property
    def total_cp(self):
        a = osum('cp', self.totais_subsistemas)
        return oo([a, self.pt], '+')

    @cached_property
    def total_va(self):
        return oo([self.total_rb, self.total_ci], '-')

    @cached_property
    def total_vat(self):
        return oo([self.total_rb, self.total_ci_f], '-')

    @cached_property
    def total_ra(self):
        a = osum('ra', self.totais_subsistemas)
        return oo([a, self.pt], '-')

    @cached_property
    def total_valor_agregado(self):
        return oo([self.total_ra, self.total_va])

    @cached_property
    def total_produtividade_terra(self):
        va_extrativistas = osum('va', self.totais_subsistemas_extrativistas)
        return oo([oo([self.total_va, va_extrativistas], '-'), self.area])

    @cached_property
    def total_ra_area(self):
        ra_extrativistas = osum('ra', self.totais_subsistemas_extrativistas)
        return oo([oo([self.total_ra, ra_extrativistas], '-'), self.area])

    @cached_property
    def total_ram(self):
        a = osum('ram', self.totais_subsistemas)
        return oo([a, self.pt], '-')

    @cached_property
    def total_ranm(self):
        return oo([self.total_ra, self.total_ram], '-')

    @cached_property
    def total_rentabilidade_monetaria_liquida(self):
        return oo([oo([self.total_ram, self.total_cp]), 1], '-')

    @cached_property
    def total_rentabilidade_monetaria_bruta(self):
        return oo([oo([self.total_pb_venda, self.total_cp]), 1], '-')

    @cached_property
    def total_rentabilidade_total(self):
        return oo([self.total_va, self.total_cp])

    @cached_property
    def total_endogeneidade(self):
        return oo([self.total_va, self.total_rb])

    @cached_property
    def total_apropriacao_valor_agregado(self):
        return oo([self.total_ra, self.total_rb])

    @cached_property
    def total_mercantilizacao(self):
        num = oo([self.total_ci, self.total_pt], '+')
        den = oo([self.total_ci, self.total_pt, self.total_pp, self.total_reciprocidade], '+')
        return oo([num, den])

    # ===================================================================================
    # Indicadores I -> Tabela 01

    # IndicadoresI.C8
    @cached_property
    def total_ra_extrativistas(self):
        return oo([self.total_ra, self.percent_ra_extrativistas], '*')

    # IndicadoresI.C7
    @cached_property
    def total_ra_nao_extrativistas(self):
        return oo([self.total_ra, self.percent_ra_nao_extrativistas], '*')

    # IndicadoresI.C12
    @cached_property
    def renda_familiar_total(self):
        return oo([self.total_ra, self.rna_valor_total], '+')

    # IndicadoresI.D8
    @cached_property
    def percent_ra_extrativistas(self):
        total_ra_extrativistas = osum('ra', self.totais_subsistemas_extrativistas)
        total_ra_subsistemas = osum('ra', self.totais_subsistemas)
        ret = oo([total_ra_extrativistas, total_ra_subsistemas])
        return 0 if ret is None else ret

    # IndicadoresI.D7
    @cached_property
    def percent_ra_nao_extrativistas(self):
        return oo([1, self.percent_ra_extrativistas], '-')

    # Indicadores.D10
    @cached_property
    def percent_rna_pluriatividade(self):
        return oo([self.rna_pluriatividade, self.rna_valor_total])

    # Indicadores.D11
    @cached_property
    def percent_rna_transferencia(self):
        return oo([self.rna_transferencia, self.rna_valor_total])

    # Indicadores.D9
    @cached_property
    def percent_rna(self):
        return oo([self.rna_valor_total, self.renda_familiar_total])

    # Indicadores.D6
    @cached_property
    def percent_ra(self):
        return oo([self.total_ra, self.renda_familiar_total])

    @cached_property
    def percent_ram(self):
        return oo([self.total_ram, self.renda_familiar_total])

    # ===============================================================================================
    # Indicadores I -> Tabela 02

    @cached_property
    def valor_hora(self):
        return oo([oo([self.renda_familiar_total, self.rna_transferencia], '-'), self.horas_total_total()]);

    # Indicadores.K9
    @cached_property
    def patrimonio_total_inicio(self):
        return oo([self.patrimonio_equipamentos_inicio, self.patrimonio_fundiario_inicio, self.patrimonio_vivo_inicio], '+')

    # Indicadores.M9
    @cached_property
    def patrimonio_total_final(self):
        return oo([self.patrimonio_equipamentos_final, self.patrimonio_fundiario_final, self.patrimonio_vivo_final], '+')

    # Indicadores.O9
    @cached_property
    def patrimonio_total_variacao(self):
        return oo(
            [self.patrimonio_equipamentos_variacao, self.patrimonio_fundiario_variacao, self.patrimonio_vivo_variacao],
            '+')

    # ===================================================================================
    # Indicadores I -> Tabela 06

    @cached_property
    def total_va_af(self):
        return osum('va_af', self.totais_subsistemas)

    @cached_property
    def total_va_am(self):
        return osum('va_am', self.totais_subsistemas)

    @cached_property
    def total_va_j(self):
        return osum('va_j', self.totais_subsistemas)

    @cached_property
    def total_va_f(self):
        return osum('va_f', self.totais_subsistemas)

    @cached_property
    def total_va_m(self):
        return osum('va_m', self.totais_subsistemas)

    @cached_property
    def total_va_autoconsumo_por_utf_f(self):
        return osum('va_autoconsumo_por_utf_f', self.totais_subsistemas)

    @cached_property
    def total_va_autoconsumo_por_utf_m(self):
        return osum('va_autoconsumo_por_utf_m', self.totais_subsistemas)

    @cached_property
    def total_va_autoconsumo_por_utf_total(self):
        return oo([self.total_va_autoconsumo_por_utf_f, self.total_va_autoconsumo_por_utf_m], '+')

    @cached_property
    def total_percent_va_autoconsumo_por_utf_f(self):
        return oo([self.total_va_autoconsumo_por_utf_f, self.total_va_autoconsumo_por_utf_total])

    @cached_property
    def total_percent_va_autoconsumo_por_utf_m(self):
        return oo([self.total_va_autoconsumo_por_utf_m, self.total_va_autoconsumo_por_utf_total])

    @cached_property
    def total_va_mercantil_por_utf_f(self):
        return osum('va_mercantil_por_utf_f', self.totais_subsistemas)

    @cached_property
    def total_va_mercantil_por_utf_m(self):
        return osum('va_mercantil_por_utf_m', self.totais_subsistemas)

    @cached_property
    def total_va_mercantil_por_utf_total(self):
        return oo([self.total_va_mercantil_por_utf_f, self.total_va_mercantil_por_utf_m], '+')

    @cached_property
    def total_percent_va_mercantil_por_utf_f(self):
        return oo([self.total_va_mercantil_por_utf_f, self.total_va_mercantil_por_utf_total])

    @cached_property
    def total_percent_va_mercantil_por_utf_m(self):
        return oo([self.total_va_mercantil_por_utf_m, self.total_va_mercantil_por_utf_total])

    @cached_property
    def total_produtividade_do_trabalho_1(self):
        # total_horas = self.horas_total_total()
        # return oo([self.total_va, total_horas])
        return oo([self.total_va, self.horas_mercantil_total()])

    @cached_property
    def total_produtividade_do_trabalho_2(self):
        utf_total = self.utf_agroecossistema_total()
        return oo([self.total_va, utf_total])

    @cached_property
    def total_intensidade_ocupacao_trabalho_porarea(self):
        utf_total = self.utf_agroecossistema_total()
        return oo([self.area, utf_total])

    @cached_property
    def total_ra_por_ht(self):
        # horas_total = self.horas_total_total()
        # return oo([self.total_ra, horas_total])
        return oo([self.total_ra, self.horas_mercantil_total()])

    @cached_property
    def total_recursos_reproduzidos(self):
        return oo([self.total_pp, self.total_pb_autoconsumo, self.total_reciprocidade, self.total_pb_doacoes_trocas],
                  '+')


# https://artandlogic.com/2015/01/dynamic-python-method/
# adiciona um metodo neste objeto para cada categoria/pessoa
def add_metodo_analise_economica(tipo, categoria, pessoa):
    getter_name = tipo + '_' + categoria + '_' + pessoa if tipo != '' else categoria + '_' + pessoa
    if hasattr(TotaisAnaliseEconomica, getter_name):
        return None

    # print(getter_name)

    if tipo in ['horas', 'utf'] or categoria == 'pessoas':
        def tempotrabalho_fn(self):
            return self.tempotrabalho(tipo, categoria, pessoa)

        setattr(TotaisAnaliseEconomica, getter_name, tempotrabalho_fn)

    if tipo == 'dias':
        def dias_fn(self):
            return self.tempo_dias(categoria, pessoa)

        setattr(TotaisAnaliseEconomica, getter_name, dias_fn)

    if tipo == 'reparticao':
        def reparticao_fn(self):
            return self.tempo_reparticao(categoria, pessoa)

        setattr(TotaisAnaliseEconomica, getter_name, reparticao_fn)

    if tipo == 'totalutc':
        def totalutc_fn(self):
            return self.tempo_totalutc(categoria, pessoa)

        setattr(TotaisAnaliseEconomica, getter_name, totalutc_fn)

    if tipo == 'totalutf':
        def totalutf_fn(self):
            return self.tempo_totalutf(categoria, pessoa)

        setattr(TotaisAnaliseEconomica, getter_name, totalutf_fn)

    if tipo == 'percentutf':
        def percentutf_fn(self):
            return self.tempo_percentutf(categoria, pessoa)

        setattr(TotaisAnaliseEconomica, getter_name, percentutf_fn)

    if tipo == 'utc':
        def utc_fn(self):
            return self.tempo_utc(categoria, pessoa)

        setattr(TotaisAnaliseEconomica, getter_name, utc_fn)

    if tipo == 'renda':
        def renda_fn(self):
            return self.renda(categoria, pessoa)

        setattr(TotaisAnaliseEconomica, getter_name, renda_fn)

    if tipo == 'rendaporutf':
        def rendaporutf_fn(self):
            return self.rendaporutf(categoria, pessoa)

        setattr(TotaisAnaliseEconomica, getter_name, rendaporutf_fn)

    if categoria == 'htdia':
        def htdia_fn(self):
            return self.tempo_htdia(pessoa)

        setattr(TotaisAnaliseEconomica, getter_name, htdia_fn)

    if tipo == 'horasporutf':
        def horasporutf_fn(self):
            tempo_reparticao_total = self.tempo_reparticao('total', pessoa)
            tempotrabalho_utf_pessoa = self.tempotrabalho('utf', 'total', pessoa)
            if tempotrabalho_utf_pessoa:
                tempotrabalho_utf_pessoa_ajuste = 1 if tempotrabalho_utf_pessoa < 1 else tempotrabalho_utf_pessoa
            else:
                tempotrabalho_utf_pessoa_ajuste = None

            ret = oo([self.tempo_reparticao(categoria, pessoa), tempotrabalho_utf_pessoa_ajuste])

            return ret

        setattr(TotaisAnaliseEconomica, getter_name, horasporutf_fn)


"""
 tipo                         categoria                        pessoa
________________________________________________________________________________
|horas             |          mercantil                   |    af, am,         |
|dias        (C)   |          domestico_cuidados          |    jf, jm,         |
|reparticao  (C)   |          participacao_social         |    of, om,         |
|utc (C)           |          pluriatividade              |    c, f, m, total  |
|                  |          total                       |                    |
--------------------------------------------------------------------------------

________________________________________________________________________________
|utf               |          pluriatividade              |    af, am,         |
|                  |          agroecossistema             |    jf, jm,         |
|                  |          domestico_cuidados          |    of, om,         |
|                  |          participacao_social         |    c, f, m, total  |
|                  |          mercantil                   |                    |
|                  |          total                       |                    |
--------------------------------------------------------------------------------

________________________________________________________________________________
|                  |          pessoas                     |    af, am,         |
|                  |          htdia  (C)                  |    jf, jm,         |
|                  |                                      |    of, om,         |
|                  |                                      |    c, f, m, total  |
--------------------------------------------------------------------------------

________________________________________________________________________________
|totalutc       (C)|          mercantil                   |    f, m, total     |
|totalutf       (C)|          domestico_cuidados          |                    |
|percentutf     (C)|          participacao_social         |                    |
|                  |          pluriatividade              |                    |
|                  |          total                       |                    |
--------------------------------------------------------------------------------

 * (C) - calculado

"""
tipos = [
    ['horas', 'dias', 'reparticao', 'utc', 'renda', 'rendaporutf', 'horasporutf'],
    ['utf'],
    [''],
    ['totalutc', 'totalutf', 'percentutf'],
]

categorias = [
    ['mercantil', 'domestico_cuidados', 'participacao_social', 'pluriatividade', 'total'],
    ['pluriatividade', 'agroecossistema', 'domestico_cuidados', 'participacao_social', 'mercantil', 'total'],
    ['pessoas', 'htdia'],
    ['mercantil', 'domestico_cuidados', 'participacao_social', 'pluriatividade', 'total'],
]

pessoas = [
    ['af', 'am', 'jf', 'jm', 'of', 'om', 'c', 'f', 'm', 'total'],
    ['af', 'am', 'jf', 'jm', 'of', 'om', 'c', 'f', 'm', 'total'],
    ['af', 'am', 'jf', 'jm', 'of', 'om', 'c', 'f', 'm', 'total'],
    ['f', 'm', 'total']
]
# print("==========================")
for i in range(4):
    for tipo in tipos[i]:
        for cat in categorias[i]:
            for p in pessoas[i]:
                    add_metodo_analise_economica(tipo, cat, p)


# Database View
class TotaisSubsistema(Model):
    area = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True, verbose_name=_lazy('área'))
    nome = models.CharField(max_length=80, verbose_name=_lazy('nome'))
    extrativismo = models.BooleanField(default=False, verbose_name=_lazy('extrativismo'))
    ci = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('ci'))
    ci_f = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('ci f'))
    ci_t = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('ci t'))
    pp = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('pp'))  # insumos - produção própria
    pt = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('pt'))
    pt_f = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('pt f'))
    pt_t = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('pt t'))
    pb_venda_convencional = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('pb venda_convencional'))
    pb_venda_territorial = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('pb venda_territorial'))
    pb_venda_institucional = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('pb venda_institucional'))
    pb_autoconsumo = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('pb autoconsumo'))
    pb_doacoes_trocas = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('pb doações e trocas'))
    pb_estoque = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('pb estoque'))
    pb_insumos_produzidos = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('pb insumos produzidos'))
    reciprocidade = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('reciprocidade'))

    # Relationship Fields
    subsistema = models.OneToOneField(
        Subsistema, verbose_name=_lazy('subsistema'),
        related_name="totais", on_delete=models.DO_NOTHING
    )

    # Relationship Fields
    analise = models.ForeignKey(
        Analise, verbose_name=_lazy('análise'),
        related_name="totais_subsistemas", on_delete=models.DO_NOTHING
    )

    # Relationship Fields
    totais_analise_f = models.ForeignKey(
        TotaisAnaliseEconomica, verbose_name=_lazy('totais análise econômica'),
        db_column='analise_id_2',
        related_name="totais_subsistemas_f",
        to_field='analise_id',
        on_delete=models.DO_NOTHING
    )

    class Meta:
        managed = False

    @cached_property
    def totais_tempostrabalho(self):
        try:
            return self.totais_tempostrabalho_f
        except TotaisSubsistema.totais_tempostrabalho_f.RelatedObjectDoesNotExist:
            return None

    @cached_property
    def totais_analise(self):
        return self.totais_analise_f

    # Indicadores
    @cached_property
    def pb_venda(self):
        return oo([self.pb_venda_convencional, self.pb_venda_territorial, self.pb_venda_institucional], '+')

    @cached_property
    def pb(self):
        return oo([self.pb_venda, self.pb_autoconsumo, self.pb_doacoes_trocas, self.pb_estoque], '+')

    @cached_property
    def rb(self):
        return oo([self.pb_venda, self.pb_autoconsumo, self.pb_doacoes_trocas], '+')

    @cached_property
    def cp(self):
        return oo([self.ci, self.pt], '+')

    @cached_property
    def va(self):
        return oo([self.rb, self.ci], '-')

    @cached_property
    def vat(self):
        return oo([self.rb, self.ci_f], '-')

    @cached_property
    def ra(self):
        return oo([self.va, self.pt], '-')

    @cached_property
    def valor_agregado(self):
        val = oo([self.ra, self.va])
        return val

    @cached_property
    def produtividade_terra(self):
        if self.extrativismo:
            return None
        else:
            val = oo([self.va, self.area])
            return val

    @cached_property
    def ra_area(self):
        val = oo([self.ra, self.area])
        return val

    @cached_property
    def ram(self):
        return oo([self.pb_venda, self.cp], '-')

    @cached_property
    def rentabilidade_monetaria_liquida(self):
        val = oo([oo([self.ram, self.cp]), 1], '-')
        return val

    @cached_property
    def rentabilidade_monetaria_bruta(self):
        return oo([oo([self.pb_venda, self.cp]), 1], '-')

    @cached_property
    def rentabilidade_total(self):
        val = oo([self.va, self.cp])
        return val

    @cached_property
    def endogeneidade(self):
        val = oo([self.va, self.rb])
        return val

    @cached_property
    def apropriacao_valor_agregado(self):
        val = oo([self.ra, self.rb])
        return val

    @cached_property
    def mercantilizacao(self):
        num = oo([self.ci, self.pt], '+')
        den = oo([self.ci, self.pt, self.pp, self.reciprocidade], '+')
        val = oo([num, den])
        return val

    # ===================================================================================
    # Indicadores I -> Tabela 06

    # VA Mulher
    @cached_property
    def va_af(self):
        tt = self.totais_tempostrabalho
        if tt:
            val = oo([oo([tt.horas_af, tt.horas_total]), self.va], '*')
            return val

    # VA Homem
    @cached_property
    def va_am(self):
        tt = self.totais_tempostrabalho
        if tt:
            val = oo([oo([tt.horas_am, tt.horas_total]), self.va], '*')
            return val

    # VA Jovens
    @cached_property
    def va_j(self):
        tt = self.totais_tempostrabalho
        if tt:
            val = oo([oo([oo([tt.horas_jf, tt.horas_jm], '+'), tt.horas_total]), self.va], '*')
            return val

    # VA Total Mulheres
    @cached_property
    def va_f(self):
        tt = self.totais_tempostrabalho
        if tt:
            val = oo([oo([tt.horas_f, tt.horas_total]), self.va], '*')
            return val

    # VA Total Homens
    @cached_property
    def va_m(self):
        tt = self.totais_tempostrabalho
        if tt:
            val = oo([oo([tt.horas_m, tt.horas_total]), self.va], '*')
            return val

    @cached_property
    def va_autoconsumo_por_utf_f(self):
        tt = self.totais_tempostrabalho
        if tt:
            tot_analise = self.totais_analise

            a = oo([self.pb_autoconsumo, oo([self.pb_autoconsumo, self.pb_venda], '+')])
            b = oo([tt.horas_f, tt.horas_total])
            c = oo([self.va, tot_analise.utf_agroecossistema_f()])

            return oo([a, b, c], '*')

    @cached_property
    def percent_va_autoconsumo_por_utf_f(self):
        tot_analise = self.totais_analise
        den = oo([tot_analise.total_va_autoconsumo_por_utf_f, tot_analise.total_va_autoconsumo_por_utf_m], '+')
        return oo([self.va_autoconsumo_por_utf_f, den])

    @cached_property
    def va_autoconsumo_por_utf_m(self):
        tt = self.totais_tempostrabalho
        if tt:
            tot_analise = self.totais_analise

            a = oo([self.pb_autoconsumo, oo([self.pb_autoconsumo, self.pb_venda], '+')])
            b = oo([tt.horas_m, tt.horas_total])
            c = oo([self.va, tot_analise.utf_agroecossistema_m()])

            return oo([a, b, c], '*')

    @cached_property
    def percent_va_autoconsumo_por_utf_m(self):
        tot_analise = self.totais_analise
        den = oo([tot_analise.total_va_autoconsumo_por_utf_f, tot_analise.total_va_autoconsumo_por_utf_m], '+')
        return oo([self.va_autoconsumo_por_utf_m, den])

    @cached_property
    def va_mercantil_por_utf_f(self):
        tt = self.totais_tempostrabalho

        if tt:
            tot_analise = self.totais_analise

            a = oo([self.pb_venda, oo([self.pb_autoconsumo, self.pb_venda], '+')])
            b = oo([tt.horas_f, tt.horas_total])
            c = oo([self.va, tot_analise.utf_agroecossistema_f()])

            return oo([a, b, c], '*')

    @cached_property
    def percent_va_mercantil_por_utf_f(self):
        tot_analise = self.totais_analise
        den = oo([tot_analise.total_va_mercantil_por_utf_f, tot_analise.total_va_mercantil_por_utf_m], '+')
        return oo([self.va_mercantil_por_utf_f, den])

    @cached_property
    def va_mercantil_por_utf_m(self):
        tt = self.totais_tempostrabalho

        if tt:
            tot_analise = self.totais_analise

            a = oo([self.pb_venda, oo([self.pb_autoconsumo, self.pb_venda], '+')])
            b = oo([tt.horas_m, tt.horas_total])
            c = oo([self.va, tot_analise.utf_agroecossistema_m()])

            return oo([a, b, c], '*')

    @cached_property
    def percent_va_mercantil_por_utf_m(self):
        tot_analise = self.totais_analise
        den = oo([tot_analise.total_va_mercantil_por_utf_f, tot_analise.total_va_mercantil_por_utf_m], '+')
        return oo([self.va_mercantil_por_utf_m, den])

    def produtividade_do_trabalho_1(self):
        tt = self.totais_tempostrabalho
        if tt:
            return oo([self.va, tt.horas_total])

    def produtividade_do_trabalho_2(self):
        tot_analise = self.totais_analise
        utf_total = tot_analise.utf_agroecossistema_total()
        return oo([self.va, utf_total])

    def intensidade_ocupacao_trabalho_porarea(self):
        tot_analise = self.totais_analise
        utf_total = tot_analise.utf_agroecossistema_total()
        return oo([self.area, utf_total])

    def ra_por_ht(self):
        tt = self.totais_tempostrabalho
        if tt:
            return oo([self.ra, tt.horas_total])


# Database View
class TotaisTempoTrabalhoUTF(Model):
    CATEGORIA_OPCOES = (
        ('O', _('Outros')),
        ('C', _('Criança')),
        ('J', _('Jovem')),
        ('A', _('Adulto'))
    )
    sexo = models.CharField(max_length=1, choices=Pessoa.SEXO_OPCOES, default=None, blank=False, verbose_name=_lazy('sexo'))
    categoria = models.CharField(max_length=1, default=None, blank=False, verbose_name=_lazy('categoria'))
    pessoas = models.IntegerField(verbose_name=_lazy('pessoa'))

    horas_domestico_cuidados = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('horas de cuidados domésticos'))
    horas_participacao_social = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('horas de participação social'))
    horas_pluriatividade = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('horas de pluriatividade'))
    horas_total = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('horas total'))

    # este utf é aquele que é informado na composição do NSGA
    utf_pluriatividade = models.DecimalField(max_digits=4, decimal_places=3, verbose_name=_lazy('utf pluriatividade'))
    utf_agroecossistema = models.DecimalField(max_digits=4, decimal_places=3, verbose_name=_lazy('utf agroecossistema'))
    utf_domestico_cuidados = models.DecimalField(max_digits=4, decimal_places=3, verbose_name=_lazy('UTF de cuidados domésticos'))
    utf_participacao_social = models.DecimalField(max_digits=4, decimal_places=3, verbose_name=_lazy('UTF de participação social'))
    utf_mercantil = models.DecimalField(max_digits=4, decimal_places=3, verbose_name=_lazy('UTF mercantil'))

    utf_total = models.DecimalField(max_digits=4, decimal_places=3, verbose_name=_lazy('utf total'))

    # Relationship Fields
    analise = models.ForeignKey(
        Analise, verbose_name=_lazy('análise'),
        related_name="totais_tempostrabalho", on_delete=models.DO_NOTHING
    )

    # Relationship Fields
    totais_analise_f = models.ForeignKey(
        TotaisAnaliseEconomica, verbose_name=_lazy('totais análise econômica'),
        db_column='analise_id_2',
        to_field='analise_id',
        related_name="totais_tempostrabalho_f",
        on_delete=models.DO_NOTHING
    )

    @cached_property
    def totais_analise(self):
        return self.totais_analise_f

    class Meta:
        managed = False


class TotaisSubsistemaTempoTrabalho(Model):
    nome = models.CharField(max_length=80, verbose_name=_lazy('nome'))
    horas_am = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('horas am'))
    horas_af = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('horas af'))
    horas_jm = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('horas jm'))
    horas_jf = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('horas jf'))
    horas_om = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('horas om'))
    horas_of = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('horas of'))
    horas_c = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('horas c'))
    horas_m = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('horas m'))
    horas_f = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('horas f'))
    horas_total = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('horas total'))

    # Relationship Fields
    subsistema = models.OneToOneField(
        Subsistema, verbose_name=_lazy('subsistema'),
        related_name="totais_tt", on_delete=models.DO_NOTHING
    )

    # Relationship Fields
    analise = models.ForeignKey(
        Analise, verbose_name=_lazy('análise'),
        related_name="totais_ss_tempostrabalho", on_delete=models.DO_NOTHING
    )

    # Relationship Fields
    totais_subsistema_f = models.OneToOneField(
        TotaisSubsistema, verbose_name=_lazy('totais do subsistema'),
        db_column='subsistema_id_2',
        to_field='subsistema_id',
        related_name="totais_tempostrabalho_f",
        on_delete=models.DO_NOTHING
    )

    # Relationship Fields
    totais_analise_f = models.ForeignKey(
        TotaisAnaliseEconomica, verbose_name=_lazy('totais da análise econômica'),
        db_column='analise_id_2',
        to_field='analise_id',
        related_name="totais_ss_tempostrabalho_f",
        on_delete=models.DO_NOTHING
    )

    class Meta:
        managed = False

    @cached_property
    def totais_analise(self):
        return self.totais_analise_f

    @cached_property
    def totais_subsistema(self):
        return self.totais_subsistema_f


class TotaisPorProduto(Model):
    nome = models.CharField(max_length=80, verbose_name=_lazy('nome'))
    produto_id = models.CharField(max_length=80, verbose_name=_lazy('produto id'))
    servico_id = models.CharField(max_length=80, verbose_name=_lazy('servico id'))
    pp = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('pp'))
    pb_venda_convencional = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('pb venda_convencional'))
    pb_venda_territorial = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('pb venda_territorial'))
    pb_venda_institucional = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('pb venda_institucional'))
    pb_autoconsumo = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('pb autoconsumo'))
    pb_doacoes_trocas = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('pb doações / trocas'))
    pb_estoque = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('pb estoque'))
    pb_insumos_produzidos = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('pb insumos produzidos'))
    reciprocidade = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('reciprocidade'))
    estoqueinsumos_inicio = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('estoque de insumos inicial'))
    estoqueinsumos_final = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('estoque de insumos final'))

    # Relationship Fields
    analise = models.ForeignKey(
        Analise, verbose_name=_lazy('análise'),
        related_name="totais_por_produto", on_delete=models.DO_NOTHING
    )

    # Relationship Fields
    totais_analise_f = models.ForeignKey(
        TotaisAnaliseEconomica, verbose_name=_lazy('totais análise econômica'),
        db_column='analise_id_2',
        to_field='analise_id',
        related_name="totais_por_produto_f",
        on_delete=models.DO_NOTHING
    )

    class Meta:
        managed = False

    @cached_property
    def totais_analise(self):
        return self.totais_analise_f

    @cached_property
    def pb_venda(self):
        return oo([self.pb_venda_convencional, self.pb_venda_territorial, self.pb_venda_institucional], '+')

    @cached_property
    def pb(self):
        return oo([self.pb_autoconsumo, self.pb_venda, self.pb_doacoes_trocas, self.pb_estoque], '+')


class TotaisQtdePorProdutoPorUnidade(Model):
    nome = models.CharField(max_length=80, verbose_name=_lazy('nome'))
    produto_id = models.CharField(max_length=80, verbose_name=_lazy('produto id'))
    unidade_id = models.CharField(max_length=80, verbose_name=_lazy('unidade id'))
    qtde_insumos_consumidos_pp = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('qtde insumos consumidos pp'))
    qtde_insumos_produzidos = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('qtde insumos produzidos'))

    # Relationship Fields
    analise = models.ForeignKey(
        Analise,
        verbose_name=_lazy('análise'),
        related_name="totais_qtde_por_produto_por_unidade",
        on_delete=models.DO_NOTHING
    )

    # Relationship Fields
    totais_analise_f = models.ForeignKey(
        TotaisAnaliseEconomica,
        verbose_name=_lazy('totais análise econômica'),
        db_column='analise_id_2',
        to_field='analise_id',
        related_name="totais_qtde_por_produto_por_unidade_f",
        on_delete=models.DO_NOTHING
    )

    class Meta:
        managed = False

    @cached_property
    def totais_analise(self):
        return self.totais_analise_f


class HorasTrabalhoPorPessoa(Model):
    pessoa_nome = models.CharField(max_length=80, verbose_name=_lazy('nome da pessoa'))
    pessoa_sexo = models.CharField(max_length=1, choices=Pessoa.SEXO_OPCOES, default=None, blank=False, verbose_name=_lazy('sexo'))
    pessoa_data_nascimento = models.DateField(verbose_name=_lazy('data de nascimento'))

    # campos da composicao_nsga
    responsavel = models.BooleanField(default=False, verbose_name=_lazy('responsavel'))
    mora_no_agroecossistema = models.BooleanField(default=True, verbose_name=_lazy('mora no agroecossistema'))
    grau_de_parentesco = models.CharField(max_length=2, choices=ComposicaoNsga.GRAU_DE_PARENTESCO, default='O', verbose_name=_lazy('grau de parentesco'))
    observacoes = models.TextField(null=True, blank=True, verbose_name=_lazy('observações'))

    # Horas: campos agregadores das informações da análise e dos subsistemas
    horas_pluriatividade = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('horas pluriatividade'))
    horas_participacao_social = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('horas de participação social'))
    horas_domestico_cuidados = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('horas de cuidados domésticos'))
    horas_mercantil = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('horas mercantil'))
    horas_total = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('horas total'))

    # UTF: campos calculados com base nos agregadores das informações da análise e dos subsistemas
    utf_agroecossistema = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('UTF no agroecossistema'))
    utf_pluriatividade = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('UTF em pluriatividades'))
    utf_domestico_cuidados = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('UTF de cuidados domésticos'))
    utf_participacao_social = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('UTF de participação social'))
    utf_mercantil = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('UTF mercantil'))
    utf_total = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_lazy('UTF total'))

    # Relationship Fields
    analise = models.ForeignKey(
        Analise, verbose_name=_lazy('análise'),
        related_name="totais_horastrabalhoporpessoa", on_delete=models.DO_NOTHING
    )

    # Relationship Fields
    totais_analise_f = models.ForeignKey(
        TotaisAnaliseEconomica, verbose_name=_lazy('totais da análise econômica'),
        db_column='analise_id_2',
        to_field='analise_id',
        related_name="totais_horastrabalhoporpessoa_f",
        on_delete=models.DO_NOTHING
    )

    pessoa = models.ForeignKey(
        Pessoa, verbose_name=_lazy('pessoa'),
        related_name="totais_horastrabalhoporpessoa", on_delete=models.DO_NOTHING
    )

    def get_idade(self):
        today = date.today()
        return today.year - self.pessoa_data_nascimento.year - (
                (today.month, today.day) < (self.pessoa_data_nascimento.month, self.pessoa_data_nascimento.day))

    def get_parentesco(self):
        for opcao in ComposicaoNsga.GRAU_DE_PARENTESCO:
            if opcao[0] == self.grau_de_parentesco:
                opcaoEscolhida = opcao
                break;

        return opcaoEscolhida[1]

    def get_sexo(self):
        for opcao in Pessoa.SEXO_OPCOES:
            if opcao[0] == self.pessoa_sexo:
                opcaoEscolhida = opcao
                break;

        return opcaoEscolhida[1]

    @property
    def horas_media_por_dia(self):
        return oo([self.horas_total, 365])

    @property
    def jornadas(self):
        return oo([self.horas_total, 8])

    @property
    def utc(self):
        return oo([self.horas_total, 2420])

    class Meta:
        managed = False
