# Generated by Django 2.2 on 2021-08-27 21:30

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('relatorios', '0021_totaisqtdeporprodutoporunidade'),
    ]

    operations = [
        migrations.DeleteModel(
            name='ProducaoPorProduto',
        ),
    ]
