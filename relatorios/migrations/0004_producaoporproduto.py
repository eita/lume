# Generated by Django 2.1.5 on 2019-05-25 00:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('relatorios', '0003_add_totaisporproduto_view'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProducaoPorProduto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('produto', models.CharField(max_length=80)),
                ('pb_venda', models.DecimalField(decimal_places=2, max_digits=10)),
                ('pb_autoconsumo', models.DecimalField(decimal_places=2, max_digits=10)),
                ('pb_doacoes_trocas', models.DecimalField(decimal_places=2, max_digits=10)),
                ('pb_estoque', models.DecimalField(decimal_places=2, max_digits=10)),
                ('pb_insumos_produzidos', models.DecimalField(decimal_places=2, max_digits=10)),
                ('pb', models.DecimalField(decimal_places=2, max_digits=10)),
            ],
            options={
                'managed': False,
            },
        ),
    ]
