# Generated by Django 2.1.5 on 2019-04-24 17:31

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('main', '0012_produto_servico_unidade'),
        ('agroecossistema', '0002_auto_20190424_1613'),
    ]

    operations = [
        migrations.CreateModel(
            name='Analise',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(blank=True, editable=False, null=True)),
                ('data_coleta', models.DateTimeField()),
                ('criacao', models.DateTimeField(auto_now_add=True)),
                ('ultima_alteracao', models.DateTimeField(auto_now=True)),
                ('ciclo_anual_referencia', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='analises_economicas', to='agroecossistema.CicloAnualReferencia')),
            ],
            options={
                'ordering': ('-criacao',),
            },
        ),
        migrations.CreateModel(
            name='EstoqueInsumos',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(blank=True, editable=False, null=True)),
                ('valor_unitario_inicio', models.DecimalField(decimal_places=2, max_digits=10)),
                ('quantidade_inicio', models.DecimalField(decimal_places=2, max_digits=10)),
                ('valor_unitario_final', models.DecimalField(decimal_places=2, max_digits=10)),
                ('quantidade_final', models.DecimalField(decimal_places=2, max_digits=10)),
                ('criacao', models.DateTimeField(auto_now_add=True)),
                ('ultima_alteracao', models.DateTimeField(auto_now=True)),
                ('analise', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='estoques_insumos', to='economica.Analise')),
                ('produto', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='estoques_insumos', to='main.Produto')),
                ('unidade', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='estoques_insumos', to='main.Unidade')),
            ],
            options={
                'ordering': ('-criacao',),
            },
        ),
        migrations.CreateModel(
            name='PagamentoTerceiros',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(blank=True, editable=False, null=True)),
                ('valor_unitario', models.DecimalField(decimal_places=2, max_digits=10)),
                ('quantidade', models.DecimalField(decimal_places=2, max_digits=10)),
                ('criacao', models.DateTimeField(auto_now_add=True)),
                ('ultima_alteracao', models.DateTimeField(auto_now=True)),
                ('analise', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='pagamentos_terceiros', to='economica.Analise')),
                ('servico', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='pagamentos_terceiros', to='main.Servico')),
                ('unidade', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='pagamentos_terceiros', to='main.Unidade')),
            ],
            options={
                'ordering': ('-criacao',),
            },
        ),
        migrations.CreateModel(
            name='Patrimonio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(blank=True, editable=False, null=True)),
                ('item', models.CharField(max_length=255)),
                ('valor_unitario_inicio', models.DecimalField(decimal_places=2, max_digits=10)),
                ('quantidade_inicio', models.DecimalField(decimal_places=2, max_digits=10)),
                ('valor_unitario_final', models.DecimalField(decimal_places=2, max_digits=10)),
                ('tipo', models.CharField(max_length=1)),
                ('observacao', models.TextField(blank=True, null=True)),
                ('criacao', models.DateTimeField(auto_now_add=True)),
                ('ultima_alteracao', models.DateTimeField(auto_now=True)),
                ('analise', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='patrimonios', to='economica.Analise')),
                ('unidade', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='patrimonios', to='main.Unidade')),
            ],
            options={
                'ordering': ('-criacao',),
            },
        ),
        migrations.CreateModel(
            name='RendaNaoAgricola',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(blank=True, editable=False, null=True)),
                ('tipo', models.CharField(max_length=30)),
                ('item', models.CharField(max_length=60)),
                ('valor_unitario', models.DecimalField(decimal_places=2, max_digits=10)),
                ('quantidade', models.DecimalField(decimal_places=2, max_digits=10)),
                ('observacao', models.TextField(blank=True, null=True)),
                ('criacao', models.DateTimeField(auto_now_add=True)),
                ('ultima_alteracao', models.DateTimeField(auto_now=True)),
                ('analise', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='rendas_nao_agricolas', to='economica.Analise')),
            ],
            options={
                'ordering': ('-criacao',),
            },
        ),
        migrations.CreateModel(
            name='Subsistema',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(blank=True, editable=False, null=True)),
                ('area', models.DecimalField(decimal_places=2, max_digits=10)),
                ('nome', models.CharField(max_length=80)),
                ('observacao', models.TextField(blank=True, null=True)),
                ('criacao', models.DateTimeField(auto_now_add=True)),
                ('ultima_alteracao', models.DateTimeField(auto_now=True)),
                ('analise', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='subsistemas', to='economica.Analise')),
            ],
            options={
                'ordering': ('-criacao',),
            },
        ),
        migrations.CreateModel(
            name='SubsistemaAquisicao',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(blank=True, editable=False, null=True)),
                ('valor_unitario', models.DecimalField(decimal_places=2, max_digits=10)),
                ('quantidade', models.DecimalField(decimal_places=2, max_digits=10)),
                ('origem', models.CharField(blank=True, max_length=1, null=True)),
                ('observacoes', models.TextField(blank=True, null=True)),
                ('tipo', models.CharField(max_length=1)),
                ('criacao', models.DateTimeField(auto_now_add=True)),
                ('ultima_alteracao', models.DateTimeField(auto_now=True)),
                ('produto', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='subsistema_aquisicoes', to='main.Produto')),
                ('servico', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='subsistema_aquisicoes', to='main.Servico')),
                ('subsistema', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='subsistema_aquisicoes', to='economica.Subsistema')),
                ('unidade', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='subsistema_aquisicoes', to='main.Unidade')),
            ],
            options={
                'ordering': ('-criacao',),
            },
        ),
        migrations.CreateModel(
            name='SubsistemaProducao',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(blank=True, editable=False, null=True)),
                ('observacao', models.TextField(blank=True, null=True)),
                ('valor_unitario', models.DecimalField(decimal_places=2, max_digits=10)),
                ('quantidade_venda', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('quantidade_autoconsumo', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('quantidade_doacoes_trocas', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('quantidade_estoque', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('quantidade_insumos_produzidos', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('criacao', models.DateTimeField(auto_now_add=True)),
                ('ultima_alteracao', models.DateTimeField(auto_now=True)),
                ('produto', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='subsistema_producoes', to='main.Produto')),
                ('subsistema', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='subsistema_producoes', to='economica.Subsistema')),
                ('unidade', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='subsistema_producoes', to='main.Unidade')),
            ],
            options={
                'ordering': ('-criacao',),
            },
        ),
        migrations.CreateModel(
            name='SubsistemaTempoTrabalho',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(blank=True, editable=False, null=True)),
                ('horas', models.DecimalField(decimal_places=2, max_digits=10)),
                ('criacao', models.DateTimeField(auto_now_add=True)),
                ('ultima_alteracao', models.DateTimeField(auto_now=True)),
                ('subsistema', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='subsistema_tempos_de_trabalhos', to='economica.Subsistema')),
            ],
            options={
                'ordering': ('-criacao',),
            },
        ),
        migrations.CreateModel(
            name='TempoTrabalho',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(blank=True, editable=False, null=True)),
                ('horas', models.DecimalField(decimal_places=2, max_digits=10)),
                ('esfera_ocupacao', models.CharField(blank=True, max_length=1, null=True)),
                ('criacao', models.DateTimeField(auto_now_add=True)),
                ('ultima_alteracao', models.DateTimeField(auto_now=True)),
                ('analise', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='tempos_de_trabalho', to='economica.Analise')),
            ],
            options={
                'ordering': ('-criacao',),
            },
        ),
    ]
