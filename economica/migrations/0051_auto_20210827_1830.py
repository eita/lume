# Generated by Django 2.2 on 2021-08-27 21:30

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('economica', '0050_auto_20210818_1219'),
    ]

    operations = [
        migrations.DeleteModel(
            name='AnaliseTemposTrabalho',
        ),
        migrations.DeleteModel(
            name='AnaliseTotais',
        ),
        migrations.DeleteModel(
            name='SubsistemaTotais',
        ),
    ]
