# Generated by Django 2.1.5 on 2019-05-13 15:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('economica', '0024_auto_20190513_0819'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='rendanaoagricola',
            options={'ordering': ('criacao',)},
        ),
        migrations.AlterModelOptions(
            name='tempotrabalho',
            options={'ordering': ('-composicao_nsga__responsavel',)},
        ),
        migrations.AlterField(
            model_name='subsistematempotrabalho',
            name='subsistema',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='subsistema_tempos_de_trabalho', to='economica.Subsistema'),
        ),
    ]
