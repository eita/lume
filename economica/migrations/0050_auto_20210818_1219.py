# Generated by Django 2.1.5 on 2021-08-18 15:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('economica', '0049_auto_20210813_1049'),
    ]

    operations = [
        migrations.AlterField(
            model_name='estoqueinsumos',
            name='quantidade_final',
            field=models.DecimalField(decimal_places=2, default=0.0, max_digits=10, verbose_name='quantidade final'),
        ),
        migrations.AlterField(
            model_name='estoqueinsumos',
            name='quantidade_inicio',
            field=models.DecimalField(decimal_places=2, default=0.0, max_digits=10, verbose_name='quantidade inicial'),
        ),
        migrations.AlterField(
            model_name='estoqueinsumos',
            name='valor_unitario_final',
            field=models.DecimalField(decimal_places=2, default=0.0, max_digits=10, verbose_name='valor unitário final'),
        ),
        migrations.AlterField(
            model_name='estoqueinsumos',
            name='valor_unitario_inicio',
            field=models.DecimalField(decimal_places=2, default=0.0, max_digits=10, verbose_name='valor unitário inicial'),
        ),
    ]
