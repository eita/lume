# Generated by Django 2.1.5 on 2019-05-10 10:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('agroecossistema', '0012_auto_20190510_1011'),
        ('economica', '0009_merge_20190509_1943'),
    ]

    operations = [
        migrations.AddField(
            model_name='subsistema',
            name='extrativismo',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='subsistematempotrabalho',
            name='composicao_nsga',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='subsistema_composicao_tempos_trabalhos', to='agroecossistema.ComposicaoNsga'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='analise',
            name='deleted',
            field=models.DateTimeField(blank=True, editable=False, null=True),
        ),
        migrations.AlterField(
            model_name='estoqueinsumos',
            name='deleted',
            field=models.DateTimeField(blank=True, editable=False, null=True),
        ),
        migrations.AlterField(
            model_name='estoqueinsumos',
            name='quantidade_final',
            field=models.DecimalField(decimal_places=2, default=1, max_digits=10),
        ),
        migrations.AlterField(
            model_name='estoqueinsumos',
            name='quantidade_inicio',
            field=models.DecimalField(decimal_places=2, default=1, max_digits=10),
        ),
        migrations.AlterField(
            model_name='pagamentoterceiros',
            name='deleted',
            field=models.DateTimeField(blank=True, editable=False, null=True),
        ),
        migrations.AlterField(
            model_name='pagamentoterceiros',
            name='quantidade',
            field=models.DecimalField(decimal_places=2, default=1, max_digits=10),
        ),
        migrations.AlterField(
            model_name='patrimonio',
            name='deleted',
            field=models.DateTimeField(blank=True, editable=False, null=True),
        ),
        migrations.AlterField(
            model_name='patrimonio',
            name='quantidade_final',
            field=models.DecimalField(decimal_places=2, default=1, max_digits=10),
        ),
        migrations.AlterField(
            model_name='patrimonio',
            name='quantidade_inicio',
            field=models.DecimalField(decimal_places=2, default=1, max_digits=10),
        ),
        migrations.AlterField(
            model_name='rendanaoagricola',
            name='deleted',
            field=models.DateTimeField(blank=True, editable=False, null=True),
        ),
        migrations.AlterField(
            model_name='rendanaoagricola',
            name='quantidade',
            field=models.DecimalField(decimal_places=2, default=1, max_digits=10),
        ),
        migrations.AlterField(
            model_name='subsistema',
            name='deleted',
            field=models.DateTimeField(blank=True, editable=False, null=True),
        ),
        migrations.AlterField(
            model_name='subsistemaaquisicao',
            name='deleted',
            field=models.DateTimeField(blank=True, editable=False, null=True),
        ),
        migrations.AlterField(
            model_name='subsistemaaquisicao',
            name='quantidade',
            field=models.DecimalField(decimal_places=2, default=1, max_digits=10),
        ),
        migrations.AlterField(
            model_name='subsistemaproducao',
            name='deleted',
            field=models.DateTimeField(blank=True, editable=False, null=True),
        ),
        migrations.AlterField(
            model_name='subsistematempotrabalho',
            name='deleted',
            field=models.DateTimeField(blank=True, editable=False, null=True),
        ),
        migrations.AlterField(
            model_name='tempotrabalho',
            name='deleted',
            field=models.DateTimeField(blank=True, editable=False, null=True),
        ),
    ]
