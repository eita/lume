# Generated by Django 2.1.5 on 2021-05-28 17:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('economica', '0044_auto_20210319_1204'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subsistemaproducao',
            name='valor_unitario',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True, verbose_name='valor unitário'),
        ),
    ]
