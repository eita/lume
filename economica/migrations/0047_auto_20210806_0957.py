# Generated by Django 2.1.5 on 2021-08-06 12:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('economica', '0046_auto_20210719_0110'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subsistemainsumo',
            name='origem',
            field=models.CharField(blank=True, choices=[('F', 'Mercado Convencional'), ('T', 'Mercado Territorial')], max_length=1, null=True, verbose_name='origem'),
        ),
    ]
