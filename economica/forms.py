from dal import autocomplete
from django import forms
from django.forms.fields import ChoiceField
from django.forms.models import inlineformset_factory
from django.forms.widgets import HiddenInput
from django.urls import reverse
from django.utils.translation import gettext as _

from agroecossistema.models import ComposicaoNsga
from bootstrap_datepicker_plus import DatePickerInput
from main.forms import LumeModelForm, LumeForm
from .models import Analise, EstoqueInsumos, PagamentoTerceiros, Patrimonio, RendaNaoAgricola, TempoTrabalho, \
    Subsistema, SubsistemaInsumo, SubsistemaTempoTrabalho, SubsistemaProducao, SubsistemaPagamentoTerceiros, \
    SubsistemaAquisicaoReciprocidade


class AnaliseForm(LumeModelForm):
    class Meta:
        model = Analise
        fields = []
        # labels = {'data_coleta': _('Data da coleta')}
        # widgets = {'data_coleta': DatePickerInput(options={"locale": "pt-br", "format": "DD/MM/YYYY"})}


class AnaliseCreateForm(LumeForm):
    data_coleta = forms.DateField(
        input_formats=['%Y-%m-%d', '%d/%m/%Y', '%d/%m/%y'],
        widget=DatePickerInput(
            options={
                "format": "DD/MM/YYYY",
                "locale": "pt-br",
            },
            required=False
        )
    )

    def __init__(self, *args, **kwargs):
        ano_choices = kwargs.pop('ano_choices')

        super().__init__(*args, **kwargs)
        self.fields['ano'] = ChoiceField(
            # FIXME não consegui colocar option selected
            choices=ano_choices
        )


class EstoqueInsumosForm(LumeModelForm):

    def __init__(self, *args, **kwargs):
        self.can_delete = kwargs.pop('can_delete', False)
        if self.can_delete:
            agroecossistema_id = kwargs.pop('agroecossistema_id')
            # self.delete_confirm_message = _('REVER Deseja realmente apagar esta estoqueinsumos?')
            self.delete_confirm_message = _('Deseja realmente apagar este registro?')
            self.delete_url = reverse('economica:estoqueinsumos_delete',
                                      kwargs={'agroecossistema_id': agroecossistema_id,
                                              'pk': kwargs.get('instance').id})
            self.delete_success_url = reverse('economica:estoqueinsumos_list',
                                              kwargs={'agroecossistema_id': agroecossistema_id})
        super(EstoqueInsumosForm, self).__init__(*args, **kwargs)

    class Meta:
        model = EstoqueInsumos
        fields = ['produto', 'unidade', 'quantidade_inicio', 'valor_unitario_inicio', 'quantidade_final', 'valor_unitario_final', 'analise',]
        labels = {
            'produto': _('Item'),
            'unidade': _('Unidade'),
            'quantidade_inicio': _('Qtde'),
            'valor_unitario_inicio': _('Valor Un.'),
            'quantidade_final': _('Qtde'),
            'valor_unitario_final': _('Valor Un.')
        }
        widgets = {
            'produto': autocomplete.ModelSelect2(url='produto_autocomplete', attrs={'data-placeholder': _('Produto...')}),
            'unidade': autocomplete.ModelSelect2(url='unidade_autocomplete', attrs={'data-placeholder': _('Unidade...')}),
            'quantidade_inicio': forms.TextInput(attrs={'style': 'width:6ch'}),
            # 'valor_unitario_inicio': forms.TextInput(attrs={'style': 'width:8ch'}),
            'quantidade_final': forms.TextInput(attrs={'style': 'width:6ch'}),
            'valor_unitario_final': forms.TextInput(attrs={'style': 'width:8ch'})
        }

EstoqueInsumosFormSet = inlineformset_factory(
    Analise,
    EstoqueInsumos,
    form=EstoqueInsumosForm,
    can_delete=True,
    extra=1,
    min_num=0
)


class PagamentoTerceirosForm(LumeModelForm):

    def __init__(self, *args, **kwargs):
        self.can_delete = kwargs.pop('can_delete', False)
        if self.can_delete:
            agroecossistema_id = kwargs.pop('agroecossistema_id')
            # self.delete_confirm_message = _('REVER Deseja realmente apagar esta pagamentoterceiros?')
            self.delete_confirm_message = _('Deseja realmente apagar este registro?')
            self.delete_url = reverse('economica:pagamentoterceiros_delete',
                                      kwargs={'agroecossistema_id': agroecossistema_id,
                                              'pk': kwargs.get('instance').id})
            self.delete_success_url = reverse('economica:pagamentoterceiros_list',
                                              kwargs={'agroecossistema_id': agroecossistema_id})
        super(PagamentoTerceirosForm, self).__init__(*args, **kwargs)

    class Meta:
        model = PagamentoTerceiros
        fields = [
            'servico',
            'unidade',
            'quantidade',
            'valor_unitario',
            'analise'
        ]
        labels = {
            'servico': _('Item'),
            'unidade': _('Unidade'),
            'quantidade': _('Qtde'),
            'valor_unitario': _('Valor Un.')
        }
        widgets = {
            'servico': autocomplete.ModelSelect2(url='servico_autocomplete', attrs={'data-placeholder': _('Serviço...')}),
            'unidade': autocomplete.ModelSelect2(url='unidade_autocomplete', attrs={'data-placeholder': _('Unidade...')}),
            'quantidade': forms.TextInput(attrs={'style': 'width:6ch'}),
            'valor_unitario': forms.TextInput(attrs={'style': 'width:8ch'})
        }

PagamentoTerceirosFormSet = inlineformset_factory(
    Analise,
    PagamentoTerceiros,
    form=PagamentoTerceirosForm,
    can_delete=True,
    extra=1,
    min_num=0
)


class PatrimonioForm(LumeModelForm):
    def __init__(self, *args, **kwargs):
        self.can_delete = kwargs.pop('can_delete', False)
        self.tp = kwargs.pop('tipo')
        if self.can_delete:
            agroecossistema_id = kwargs.pop('agroecossistema_id')
            # self.delete_confirm_message = _('REVER Deseja realmente apagar esta patrimonio?')
            self.delete_confirm_message = _('Deseja realmente apagar este registro?')
            self.delete_url = reverse('economica:patrimonio_delete', kwargs={'agroecossistema_id': agroecossistema_id, 'pk': kwargs.get('instance').id})
            self.delete_success_url = reverse('economica:patrimonio_list', kwargs={'agroecossistema_id': agroecossistema_id})
        super(PatrimonioForm, self).__init__(*args, **kwargs)

    def clean(self):
        self.changed_data.append('tipo')
        cleaned_data = self.cleaned_data
        cleaned_data['tipo'] = self.tp
        return cleaned_data

    class Meta:
        model = Patrimonio
        fields = [
            'tipo',
            'item',
            'unidade',
            'quantidade_inicio',
            'valor_unitario_inicio',
            'quantidade_final',
            'valor_unitario_final',
            'observacoes',
            'analise'
        ]
        labels = {
            'unidade': _('Unidade'),
            'observacoes': _('Observações'),
            'quantidade_inicio': _('Qtde'),
            'valor_unitario_inicio': _('Valor un.'),
            'quantidade_final': _('Qtde'),
            'valor_unitario_final': _('Valor un.')
        }
        widgets = {
            'item': forms.TextInput(attrs={'style': 'width:300px; padding-left:.5rem;'}),
            'unidade': autocomplete.ModelSelect2(url='unidade_autocomplete', attrs={'style': 'width:300px', 'data-placeholder': _('Unidade...')}),
            'observacoes': forms.Textarea(attrs={'rows': 3, 'cols': 25}),
            'quantidade_inicio': forms.TextInput(attrs={'style': 'width:6ch'}),
            'valor_unitario_inicio': forms.TextInput(attrs={'style': 'width:8ch'}),
            'quantidade_final': forms.TextInput(attrs={'style': 'width:6ch'}),
            'valor_unitario_final': forms.TextInput(attrs={'style': 'width:8ch'}),
            'tipo': HiddenInput()
        }

PatrimonioFormSet = inlineformset_factory(
    Analise,
    Patrimonio,
    form=PatrimonioForm,
    can_delete=True,
    extra=1,
    min_num=0
)


class RendaNaoAgricolaForm(LumeModelForm):
    def __init__(self, *args, **kwargs):
        self.can_delete = kwargs.pop('can_delete', False)
        if self.can_delete:
            agroecossistema_id = kwargs.pop('agroecossistema_id')
            # self.delete_confirm_message = _('REVER Deseja realmente apagar esta rendanaoagricola?')
            self.delete_confirm_message = _('Deseja realmente apagar este registro?')
            self.delete_url = reverse('economica:rendanaoagricola_delete',
                                      kwargs={'agroecossistema_id': agroecossistema_id,
                                              'pk': kwargs.get('instance').id})
            self.delete_success_url = reverse('economica:rendanaoagricola_list',
                                              kwargs={'agroecossistema_id': agroecossistema_id})
        super(RendaNaoAgricolaForm, self).__init__(*args, **kwargs)

    class Meta:
        model = RendaNaoAgricola
        fields = [
            'tipo',
            'item',
            'unidade',
            'quantidade',
            'valor_unitario',
            'observacoes',
            'analise'
        ]
        labels = {
            'unidade': _('Unidade'),
            'quantidade': _('Qtde'),
            'valor_unitario': _('Valor Un.'),
            'observacoes': _('Observações')
        }
        widgets = {
            'tipo': forms.HiddenInput(),
            'item': forms.TextInput(attrs={'style': 'width:300px; padding-left:.5rem;'}),
            'unidade': autocomplete.ModelSelect2(url='unidade_autocomplete', attrs={'style': 'width:300px', 'data-placeholder': _('Unidade...')}),
            'quantidade': forms.TextInput(attrs={'style': 'width:6ch'}),
            'valor_unitario': forms.TextInput(attrs={'style': 'width:8ch'}),
            'observacoes': forms.Textarea(attrs={'rows': 3, 'cols': 25})
        }

RendaNaoAgricolaFormSet = inlineformset_factory(
    Analise,
    RendaNaoAgricola,
    form=RendaNaoAgricolaForm,
    can_delete=True,
    extra=1,
    min_num=0
)


class TempoTrabalhoForm(LumeModelForm):
    def __init__(self, *args, **kwargs):
        agroecossistema_id = kwargs.pop('agroecossistema_id')
        ano_car = kwargs.pop('ano_car')
        sim_slug = kwargs.pop('sim_slug')

        super(TempoTrabalhoForm, self).__init__(*args, **kwargs)

        get_args = {
            'ciclo_anual_referencia__agroecossistema_id': agroecossistema_id,
            'ciclo_anual_referencia__ano': ano_car
        }

        if sim_slug is not None:
            get_args['ciclo_anual_referencia__sim_slug'] = sim_slug
        else:
            get_args['ciclo_anual_referencia__sim_slug__isnull'] = True

        self.fields['composicao_nsga'].queryset = ComposicaoNsga.objects.filter(**get_args)

    class Meta:
        model = TempoTrabalho
        fields = [
            'composicao_nsga',
            'horas_domestico_cuidados',
            'horas_participacao_social',
            'horas_pluriatividade',
            'analise'
        ]
        labels = {
            'horas_domestico_cuidados': _('Trabalho doméstico e de cuidados'),
            'horas_participacao_social': _('Participação social'),
            'horas_pluriatividade': _('Pluriatividade'),
            'composicao_nsga': _('Pessoa')
        }


TempoTrabalhoFormSet = inlineformset_factory(
    Analise,
    TempoTrabalho,
    form=TempoTrabalhoForm,
    can_delete=True,
    extra=1,
    min_num=0
)

class SubsistemaForm(LumeModelForm):
    def __init__(self, *args, **kwargs):
        self.can_delete = kwargs.pop('can_delete', True)
        agroecossistema_id = kwargs.pop('agroecossistema_id', None)
        ano_car = kwargs.pop('ano_car', None)
        sim_slug = kwargs.pop('sim_slug', None)
        super(SubsistemaForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Subsistema
        fields = ['nome', 'area', 'extrativismo',
                  'gerido_por_mulheres', 'gerido_por_jovens',
                  'quintal', 'observacoes']
        labels = {
            'extrativismo': _('Extrativismo'),
            'area': _('Área (ha)'),
            'observacoes': _('Observações')
        }
        widgets = {
            'area': forms.TextInput(attrs={'style': 'width:8ch'}),
            'observacoes': forms.Textarea(attrs={'rows': 3, 'cols': 25})
        }

SubsistemaFormSet = inlineformset_factory(
    Analise,
    Subsistema,
    form=SubsistemaForm,
    can_delete=True,
    extra=1,
    min_num=0
)


class SubsistemaInsumoForm(LumeModelForm):
    def __init__(self, *args, **kwargs):
        tipo = kwargs.pop('tipo')
        super(SubsistemaInsumoForm, self).__init__(*args, **kwargs)
        if tipo == 'P':
            self.fields['origem'].widget = HiddenInput()

    class Meta:
        model = SubsistemaInsumo
        fields = [
            'tipo',
            'produto',
            'unidade',
            'origem',
            'quantidade',
            'valor_unitario',
            'observacoes'
        ]
        labels = {
            'produto': _('Insumo'),
            'unidade': _('Unidade'),
            'origem': _('Origem'),
            'quantidade': _('Qtde'),
            'valor_unitario': _('Valor Un.'),
            'observacoes': _('Observações')
        }
        widgets = {
            'tipo': HiddenInput(),
            'produto': autocomplete.ModelSelect2(url='produto_autocomplete', attrs={'data-placeholder': _('Produto...')}),
            'unidade': autocomplete.ModelSelect2(url='unidade_autocomplete', attrs={'data-placeholder': _('Unidade...')}),
            'quantidade': forms.TextInput(attrs={'style': 'width:6ch'}),
            'valor_unitario': forms.TextInput(attrs={'style': 'width:8ch'}),
            'observacoes': forms.Textarea(attrs={'rows': 3, 'cols': 25}),
        }

SubsistemaInsumoFormSet = inlineformset_factory(
    Subsistema,
    SubsistemaInsumo,
    form=SubsistemaInsumoForm,
    can_delete=True,
    extra=1,
    min_num=0
)


class SubsistemaPagamentoTerceirosForm(LumeModelForm):
    class Meta:
        model = SubsistemaPagamentoTerceiros
        fields = [
            'servico',
            'unidade',
            'origem',
            'quantidade',
            'valor_unitario',
            'observacoes'
        ]
        labels = {
            'servico': _('Serviços'),
            'unidade': _('Unidade'),
            'quantidade': _('Qtde'),
            'valor_unitario': _('Valor Un.'),
            'origem': _('Origem'),
            'observacoes': _('Observações')
        }
        widgets = {
            'servico': autocomplete.ModelSelect2(url='servico_autocomplete', attrs={'data-placeholder': _('Serviço...')}),
            'unidade': autocomplete.ModelSelect2(url='unidade_autocomplete', attrs={'data-placeholder': _('Unidade...')}),
            'quantidade': forms.TextInput(attrs={'style': 'width:6ch'}),
            'valor_unitario': forms.TextInput(attrs={'style': 'width:8ch'}),
            'observacoes': forms.Textarea(attrs={'rows': 3, 'cols': 25})
        }

SubsistemaPagamentoTerceirosFormSet = inlineformset_factory(
    Subsistema,
    SubsistemaPagamentoTerceiros,
    form=SubsistemaPagamentoTerceirosForm,
    can_delete=True,
    extra=1,
    min_num=0
)


class SubsistemaAquisicaoReciprocidadeForm(LumeModelForm):
    def __init__(self, *args, **kwargs):
        self.show = kwargs.pop('show', 'I')
        super(SubsistemaAquisicaoReciprocidadeForm, self).__init__(*args, **kwargs)
        tipo = 'produto' if self.show == 'S' else 'servico'
        self.fields[tipo].widget = HiddenInput()

    def clean(self):
        try:
            self.cleaned_data['tipo'] = self.show

            item_field = 'produto' if self.show == 'I' else 'servico'
            item = self.cleaned_data.get(item_field)

            if item is None:
                self._errors[item_field] = self.error_class([_('Este campo é obrigatório.')])
                del self.cleaned_data[item_field]
        except KeyError:
            pass

    class Meta:
        model = SubsistemaAquisicaoReciprocidade
        fields = [
            'tipo',
            'produto',
            'servico',
            'unidade',
            'quantidade',
            'valor_unitario',
            'observacoes'
        ]
        labels = {
            'produto': _('Insumos'),
            'servico': _('Serviços'),
            'unidade': _('Unidade'),
            'quantidade': _('Qtde'),
            'valor_unitario': _('Valor Un.'),
            'observacoes': _('Observações')
        }
        widgets = {
            'tipo': HiddenInput(),
            'produto': autocomplete.ModelSelect2(url='produto_autocomplete', attrs={'data-placeholder': _('Produto...')}),
            'servico': autocomplete.ModelSelect2(url='servico_autocomplete', attrs={'data-placeholder': _('Serviço...')}),
            'unidade': autocomplete.ModelSelect2(url='unidade_autocomplete', attrs={'data-placeholder': _('Unidade...')}),
            'quantidade': forms.TextInput(attrs={'style': 'width:6ch'}),
            'valor_unitario': forms.TextInput(attrs={'style': 'width:8ch'}),
            'observacoes': forms.Textarea(attrs={'rows': 3, 'cols': 25})
        }

SubsistemaAquisicaoReciprocidadeFormSet = inlineformset_factory(
    Subsistema,
    SubsistemaAquisicaoReciprocidade,
    form=SubsistemaAquisicaoReciprocidadeForm,
    can_delete=True,
    extra=1,
    min_num=0
)


class SubsistemaTempoTrabalhoForm(LumeModelForm):
    def __init__(self, *args, **kwargs):
        agroecossistema_id = kwargs.pop('agroecossistema_id')
        ano_car = kwargs.pop('ano_car')
        sim_slug = kwargs.pop('sim_slug')

        super(SubsistemaTempoTrabalhoForm, self).__init__(*args, **kwargs)

        get_args = {
            'ciclo_anual_referencia__agroecossistema_id': agroecossistema_id,
            'ciclo_anual_referencia__ano': ano_car
        }

        if sim_slug is not None:
            get_args['ciclo_anual_referencia__sim_slug'] = sim_slug
        else:
            get_args['ciclo_anual_referencia__sim_slug__isnull'] = True

        self.fields['composicao_nsga'].queryset = ComposicaoNsga.objects.filter(**get_args)

    def clean(self):
        cleaned_data = self.cleaned_data

        if 'composicao_nsga' in cleaned_data:
            pessoa = cleaned_data['composicao_nsga'].pessoa
            horas = cleaned_data['horas']
            if horas and not pessoa.permitida_pelo_ECA():
                self._errors['horas'] = self.error_class([pessoa.norma_ECA])
                del cleaned_data['horas']

        return cleaned_data

    class Meta:
        model = SubsistemaTempoTrabalho
        fields = [
            'composicao_nsga',
            'horas'
        ]
        labels = {
            'composicao_nsga': _('Pessoa')
        }
        widgets = {
            'horas': forms.TextInput(attrs={'style': 'width:8ch'}),
        }

SubsistemaTempoTrabalhoFormSet = inlineformset_factory(
    Subsistema,
    SubsistemaTempoTrabalho,
    form=SubsistemaTempoTrabalhoForm,
    can_delete=True,
    extra=1,
    min_num=0
)


class SubsistemaProducaoForm(LumeModelForm):
    class Meta:
        model = SubsistemaProducao
        fields = [
            'produto',
            'unidade',
            'quantidade_venda_convencional',
            'valor_venda_convencional',
            'quantidade_venda_territorial',
            'valor_venda_territorial',
            'quantidade_venda_institucional',
            'valor_venda_institucional',
            'quantidade_autoconsumo',
            'valor_autoconsumo',
            'quantidade_doacoes_trocas',
            'valor_doacoes_trocas',
            'quantidade_estoque',
            'valor_estoque',
            'quantidade_insumos_produzidos',
            'valor_insumos_produzidos',
            'subsistema',
            'observacoes'
        ]
        labels = {
            'produto': _('Produto ou insumo gerado'),
            'unidade': _('Unidade'),
            'quantidade_venda_convencional': _('Qtde'),
            'valor_venda_convencional': _('Valor un.'),
            'quantidade_venda_territorial': _('Qtde'),
            'valor_venda_territorial': _('Valor un.'),
            'quantidade_venda_institucional': _('Qtde'),
            'valor_venda_institucional': _('Valor un.'),
            'quantidade_autoconsumo': _('Qtde'),
            'valor_autoconsumo': _('Valor un.'),
            'quantidade_doacoes_trocas': _('Qtde'),
            'valor_doacoes_trocas': _('Valor un.'),
            'quantidade_estoque': _('Qtde'),
            'valor_estoque': _('Valor un.'),
            'quantidade_insumos_produzidos': _('Qtde'),
            'valor_insumos_produzidos': _('Valor un.'),
            'observacoes': _('Observações')
        }
        widgets = {
            'produto': autocomplete.ModelSelect2(url='produto_autocomplete', attrs={'data-width': '80%', 'data-placeholder': _('Produto...')}),
            'unidade': autocomplete.ModelSelect2(url='unidade_autocomplete', attrs={'data-placeholder': _('Unidade...'), 'data-width': '80%'}),
            'quantidade_venda_convencional': forms.TextInput(attrs={'style': 'width:6ch'}),
            'valor_venda_convencional': forms.TextInput(attrs={'style': 'width:8ch'}),
            'quantidade_venda_institucional': forms.TextInput(attrs={'style': 'width:6ch'}),
            'valor_venda_institucional': forms.TextInput(attrs={'style': 'width:8ch'}),
            'quantidade_venda_territorial': forms.TextInput(attrs={'style': 'width:6ch'}),
            'valor_venda_territorial': forms.TextInput(attrs={'style': 'width:8ch'}),
            'quantidade_autoconsumo': forms.TextInput(attrs={'style': 'width:6ch'}),
            'valor_autoconsumo': forms.TextInput(attrs={'style': 'width:8ch'}),
            'quantidade_doacoes_trocas': forms.TextInput(attrs={'style': 'width:6ch'}),
            'valor_doacoes_trocas': forms.TextInput(attrs={'style': 'width:8ch'}),
            'quantidade_insumos_produzidos': forms.TextInput(attrs={'style': 'width:6ch'}),
            'valor_insumos_produzidos': forms.TextInput(attrs={'style': 'width:8ch'}),
            'quantidade_estoque': forms.TextInput(attrs={'style': 'width:6ch'}),
            'valor_estoque': forms.TextInput(attrs={'style': 'width:8ch'}),
            'observacoes': forms.Textarea(attrs={'rows': 3, 'cols': 8})
        }

SubsistemaProducaoFormSet = inlineformset_factory(
    Subsistema,
    SubsistemaProducao,
    form=SubsistemaProducaoForm,
    can_delete=True,
    extra=1,
    min_num=0
)
