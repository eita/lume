import copy
import decimal
from decimal import Decimal

from rest_framework.renderers import JSONRenderer
from rest_framework.utils.encoders import JSONEncoder


class LDecimal(object):
    value = None
    messages = None

    def check(self, variable, varname, message):
        if self.messages is None:
            self.messages = {}
        if isinstance(variable, LDecimal):
            variable = variable.value
        if variable is None or variable is False:
            if not varname in self.messages:
                self.messages[varname] = []
            if not message in self.messages[varname]:
                self.messages[varname].append(message)

    def feed_messages(self, messages):
        if messages:
            if self.messages is None:
                self.messages = {}
            for i in messages:
                if not i in self.messages:
                    self.messages[i] = []
                for message in messages[i]:
                    if not message in self.messages[i]:
                        self.messages[i].append(message)

    def messages_as_ul(self):
        if self.messages:
            str = '<ul>'
            for i in self.messages:
                str += '<li><strong>' + i + ':</strong><ul>'
                for msg in self.messages[i]:
                    str += '<li>' + msg + '</li>'
                str += '</ul></li>'
            str += '</ul>'
            return str

    def get_span_tag(self, content, title=""):
        #if self.messages:
        #    str = '<span class="clickable" data-toggle="popover" data-html="true" data-content="' + self.messages_as_ul() + '" '
        #    if title:
        #        str += 'data-title="'+title+'" '
        #    str += '>' + content + '</span>'
        #    return str
        #else:
            return content

    def __init__(self, value):
        if isinstance(value, LDecimal):
            self.value = value.value
            self.messages = value.messages
        else:
            self.value = value

    def __eq__(self, o):
        if isinstance(o, LDecimal):
            return self.value == o.value
        else:
            return self.value == o

    def __ne__(self, o):
        if isinstance(o, LDecimal):
            return self.value != o.value
        else:
            return self.value != o

    def __str__(self) -> str:
        return str(self.value)

    def __repr__(self):
        res = "LDecimal('%s')" % str(self)
        if self.messages:
            res += '*'
        return res

    def __bool__(self):
        return bool(self.value)

    def __add__(self, other):
        a = copy.deepcopy(self)
        a += other
        return a

    def __sub__(self, other):
        a = copy.deepcopy(self)
        a -= other
        return a

    def __mul__(self, other):
        a = copy.deepcopy(self)
        a *= other
        return a

    def __truediv__(self, other):
        a = copy.deepcopy(self)
        a /= other
        return a

    def __floordiv__(self, other):
        a = copy.deepcopy(self)
        a //= other
        return a

    def __mod__(self, other):
        a = copy.deepcopy(self)
        a %= other
        return a

    def __pow__(self, other, modulo):
        a = copy.deepcopy(self)
        try:
            if isinstance(other, LDecimal):
                a.feed_messages(other.messages)
                a.value = pow(a.value, other.value, modulo)
            else:
                a.value = pow(a.value, other, modulo)
        except TypeError:
            a.value = None
        return a

    def __radd__(self, other):
        return self.__add__(other)

    def __rsub__(self, other):
        return self.__sub__(other)

    def __rmul__(self, other):
        return self.__mul__(other)

    def __rtruediv__(self, other):
        return self.__truediv__(other)

    def __rfloordiv__(self, other):
        return self.__floordiv__(other)

    def __rmod__(self, other):
        return self.__mod__(other)

    def __rpow__(self, other):
        return self.__pow__(other)

    def __iadd__(self, other):
        try:
            if isinstance(other, LDecimal):
                self.feed_messages(other.messages)
                self.value += other.value
            else:
                self.value += other
        except TypeError:
            self.value = None
        return self

    def __isub__(self, other):
        try:
            if isinstance(other, LDecimal):
                self.feed_messages(other.messages)
                self.value -= other.value
            else:
                self.value -= other
        except TypeError:
            self.value = None
        return self

    def __imul__(self, other):
        try:
            if isinstance(other, LDecimal):
                self.feed_messages(other.messages)
                self.value *= other.value
            else:
                self.value *= other
        except TypeError:
            self.value = None
        return self

    def __itruediv__(self, other):
        try:
            if isinstance(other, LDecimal):
                self.feed_messages(other.messages)
                self.value /= other.value
            else:
                self.value /= other
        except (TypeError, ZeroDivisionError, decimal.InvalidOperation):
            self.value = None
        return self

    def __ifloordiv__(self, other):
        try:
            if isinstance(other, LDecimal):
                self.feed_messages(other.messages)
                self.value //= other.value
            else:
                self.value //= other
        except (TypeError, ZeroDivisionError):
            self.value = None
        return self

    def __imod__(self, other):
        try:
            if isinstance(other, LDecimal):
                self.feed_messages(other.messages)
                self.value %= other.value
            else:
                self.value %= other
        except (TypeError, ZeroDivisionError):
            self.value = None
        return self

    def __ipow__(self, other, modulo):
        try:
            if isinstance(other, LDecimal):
                self.feed_messages(other.messages)
                self.value = pow(self.value, other.value, modulo)
            else:
                self.value = pow(self.value, other, modulo)
        except TypeError:
            self.value = None
        return self

    def __neg__(self):
        a = copy.deepcopy(self)
        try:
            a.value = - a.value
        except TypeError:
            a.value = None
        return a

    def __pos__(self):
        a = copy.deepcopy(self)
        try:
            a.value = + a.value
        except TypeError:
            a.value = None
        return a

    def __abs__(self):
        a = copy.deepcopy(self)
        try:
            a.value = abs(a.value)
        except TypeError:
            a.value = None
        return a

    def __complex__(self):
        a = copy.deepcopy(self)
        try:
            a.value = complex(a.value)
        except TypeError:
            a.value = None
        return a

    def __int__(self):
        return int(self.value)

    def __float__(self):
        return float(self.value)

    def __round__(self, ndigits):
        a = copy.deepcopy(self)
        try:
            a.value = round(a.value, ndigits)
        except TypeError:
            a.value = None
        return a

    def __trunc__(self):
        a = copy.deepcopy(self)
        try:
            a.value = a.value.__trunc__()
        except TypeError:
            a.value = None
        return a

    def __floor__(self):
        a = copy.deepcopy(self)
        try:
            a.value = a.value.__floor__()
        except TypeError:
            a.value = None
        return a

    def __ceil__(self):
        a = copy.deepcopy(self)
        try:
            a.value = a.value.__ceil__()
        except TypeError:
            a.value = None
        return a


class LJSONEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, LDecimal):
            if o.value is None:
                return 'null'
            elif isinstance(o.value, (int, float)):
                return super().default(Decimal(o.value))
            return super().default(o.value)
        return super().default(o)


class LJSONRenderer(JSONRenderer):
    encoder_class = LJSONEncoder

def oo(vals, op='/'):
    if op == '/':
        if vals[1] == 0 or vals[0] is None or vals[1] is None:
            ret = None
        else:
            ret = vals[0] / vals[1]
        return ret
    elif op == '*':
        allValid = True
        for val in vals:
            if val is None:
                allValid = False
        if not allValid:
            return None

        ret = vals[0]
        for val in vals[1:]:
            ret *= val
        return ret
    elif op == '+':
        ret = 0
        allNone = True
        for val in vals:
            if val is not None:
                ret += val
                allNone = False
        return None if allNone else ret
    elif op == '-':
        ret = 0 if vals[0] is None else vals[0]
        allNone = vals[0] is None
        for val in vals[1:]:
            if val is not None:
                ret -= val
                allNone = False
        return None if allNone else ret

def osum(key, vals):
    ret = 0
    allNone = True
    for val in vals:
        valKey = val.__getattribute__(key)
        ret += valKey or 0
        if valKey is not None:
            allNone = False
    return None if allNone else ret
