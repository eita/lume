var graficos = {};
var graficosCompletos = {};

function getStackedBarValueLineHeight(ctx) {
    var scale = ctx.chart.scales['y-axis-0'];
    var value = ctx.dataset.data[ctx.dataIndex];
    var range = Math.max(scale.max - scale.min, 1);
    return (ctx.chart.height / range) * value;
}

function getPieValuePercentage(ctx) {
    var value = parseFloat(ctx.dataset.data[ctx.dataIndex]);
    var sum = 0;
    for (var i = 0; i < ctx.chart.data.datasets[0].data.length; i++) {
        sum += parseFloat(ctx.chart.data.datasets[0].data[i]);
    }
    ;
    return (value / sum);
}

var chartJsDataLabelsOptions = function (size = 'm', type = 'stackedbars', prependBRL = true, rather = '') {
    var opts = {};
    var minHeight = 32;
    if (size == 'm') {
        opts['borderRadius'] = 4;
        opts['formatter'] = function (value, ctx) {
            return valueToBRLCurrency(value, prependBRL);
        };
    } else if (size == 'sm') {
        opts['borderRadius'] = 2;
        opts['padding'] = 2;
        opts['font'] = {size: 10};
        opts['formatter'] = function (value, ctx) {
            return valueToBRLCurrency(value, false);
        };
        minHeight = 24;
    }
    if (type == 'stackedbars') {
        opts['backgroundColor'] = '#fff';
        opts['borderColor'] = '#eee';
        opts['borderWidth'] = 1;
        opts['display'] = function (ctx) {
            return getStackedBarValueLineHeight(ctx) > minHeight;
        };
        opts['align'] = 'center';
        opts['anchor'] = 'center';
        opts['textAlign'] = 'center';
        opts['formatter'] = function (value, ctx) {
            if (rather == 'labels') {
                return (getStackedBarValueLineHeight(ctx) > 80)
                    ? ctx.dataset.label + '\n' + valueToBRLCurrency(value, false)
                    : ctx.dataset.label;
            } else if (rather == 'values') {
                return valueToBRLCurrency(value, prependBRL);
            } else if  (ctx.chart.data.labels.length <= 6) {
                if (ctx.chart.data.datasets.length <= 4) {
                    return valueToBRLCurrency(value, prependBRL);
                } else {
                    return (getStackedBarValueLineHeight(ctx) > 80)
                        ? ctx.dataset.label + '\n' + valueToBRLCurrency(value, prependBRL)
                        : valueToBRLCurrency(value, prependBRL);
                }
            } else {
                return valueToBRLCurrency(value, prependBRL && ctx.chart.data.labels.length <= 10);
            }
        };
    } else if (type == 'pie') {
        opts['backgroundColor'] = '#fff';
        opts['borderColor'] = '#eee';
        opts['borderWidth'] = 1;
        opts['display'] = function (ctx) {
            return (getPieValuePercentage(ctx) >= 0.02);
        };
        opts['align'] = function (ctx) {
            return (getPieValuePercentage(ctx) >= 0.06) ? 'center' : 'end';
        };
        opts['anchor'] = function (ctx) {
            return (getPieValuePercentage(ctx) >= 0.06) ? 'center' : 'end';
        };
        opts['textAlign'] = 'center';
        opts['formatter'] = function (value, ctx) {
            var pct = getPieValuePercentage(ctx);
            return (pct >= 0.1 && ctx.dataset.data.length > 4)
                ? ctx.chart.legend.legendItems[ctx.dataIndex].text + '\n' + valueToBRLCurrency(value, prependBRL)
                : valueToBRLCurrency(value, pct >= 0.1 && ctx.dataset.data.length <= 4)
        };
    } else if (type == 'scatter') {
        opts['align'] = 'right';
        opts['anchor'] = 'center';
        opts['offset'] = 10;
        opts['formatter'] = function (value, ctx) {
            return ctx.dataset.data[ctx.dataIndex].label;
        };
    } else if (type == 'scatterSemLabel') {
        opts['formatter'] = function (value, ctx) {
            return ''
        };
    } else if (type == 'line'){
        opts['align'] = 'top';
        opts['anchor'] = 'center';
        opts['formatter'] = function (value, ctx) {
            return valueToBRLCurrency(ctx.dataset.data[ctx.dataIndex].x, false);
        };
        opts['display'] = false;
    } else {
        opts['display'] = false;
    }

    return opts;
};

function valueToBRLCurrency(value, withCurrency = true, decimals = 2) {
    var prepend = (withCurrency) ? gettext('R$ ') : '';
    if (!value) {
        value = 0;
    }
    return prepend + parseFloat(value).toLocaleString(gettext("pt-BR"), {
        minimumFractionDigits: decimals,
        maximumFractionDigits: decimals
    });
}

function copyFormValuesFromInitialToFinal(el) {
    var elementIdPieces = el.id.split('-');
    if (elementIdPieces.length == 3) {
        var submodulo = elementIdPieces[0];
        var id = elementIdPieces[1];
        var field = elementIdPieces.pop();

        var fieldPieces = field.split('_');
        var last = fieldPieces.pop();
        if (last == 'inicio') {
            targetElementId = elementIdPieces.join('-') + '-' + fieldPieces.join('_') + '_final';
            $('#' + targetElementId).val($(el).val()).change();
        }
    }
}

function acoes_pos_carregamento(chart, id, legend_id = null, ordemComparacao = null, mostraLegenda = true) {
    if (graficosCompletos[id]<=1) {
        var url_base64 = chart.toBase64Image();
        $('#botao-' + id).attr('href', url_base64).attr('download', arquivo_pre + '_' + id + '.png').show();
        $('.exportar_todos_graficos').show();
        if (legend_id && $('#' + legend_id)) {
            var htmlLegend = chart.generateLegend();
            if (mostraLegenda) {
                $('#' + legend_id).html(htmlLegend);
            } else {
                $('#' + legend_id).hide();
            }
        }
        if (ordemComparacao) {
            var idCorrelato = ordemComparacao == 1
                ? id.substr(0,id.length - 1) + '2'
                : id.substr(0,id.length - 1) + '1';
            if (graficos[id].scales && graficos[id].scales["y-axis-0"] && graficosCompletos[id]==0 && graficosCompletos[idCorrelato]>0) {
                if (graficos[id].scales["y-axis-0"].max > graficos[idCorrelato].scales["y-axis-0"].max) {
                    graficos[idCorrelato].options.scales.yAxes[0].ticks.max = graficos[id].scales["y-axis-0"].max;
                } else {
                    graficos[id].options.scales.yAxes[0].ticks.max = graficos[idCorrelato].scales["y-axis-0"].max;
                }
                if (graficos[id].scales["y-axis-0"].min < graficos[idCorrelato].scales["y-axis-0"].min) {
                    graficos[idCorrelato].options.scales.yAxes[0].ticks.min = graficos[id].scales["y-axis-0"].min;
                } else {
                    graficos[id].options.scales.yAxes[0].ticks.min = graficos[idCorrelato].scales["y-axis-0"].min;
                }
                graficos[id].update();
                graficos[idCorrelato].update();
                setTimeout(function() {
                    graficos[id].update();
                    graficos[idCorrelato].update();
                }, 10);
            }
        }
        graficosCompletos[id]++;
    }
}

function draw_legend(chart, id) {
    var text = [];
    text.push('<ul class="list-inline inner-legend-' + id + '">');
    if (chart.config.type == 'pie') {
        for (var i = 0; i < chart.data.labels.length; i++) {
            text.push('<li class="list-inline-item small mr-4"><span class="d-inline-block" style="height: 0.7rem; width:40px; background-color:' + chart.data.datasets[0].backgroundColor[i] + '"></span>');
            if (chart.data.labels[i]) {
                text.push('<span class="ml-2">' + chart.data.labels[i] + '</span>');
            }
            text.push('</li>');
        }
    } else {
        for (var i = 0; i < chart.data.datasets.length; i++) {
            text.push('<li class="list-inline-item small mr-4"><span class="d-inline-block" style="height: 0.7rem; width:40px; background-color:' + chart.data.datasets[i].backgroundColor + '"></span>');
            if (chart.data.datasets[i].label) {
                text.push('<span class="ml-2">' + chart.data.datasets[i].label + '</span>');
            }
            text.push('</li>');
        }
    }
    text.push('</ul>');
    return text.join('');
}

function pegaOpcoesGraficoLume(id) {
    var $canvas = $('#' + id);
    if (!$canvas || !$canvas[0]) {
        return {};
    }
    var opcoesGraficoLume = {};
    $.each($canvas[0].attributes, function (i, attr) {
        var nome = attr.name.split('-');
        if (nome[0] == 'chart') {
            opcoesGraficoLume[nome[1]] = attr.value;
        }
    });
    var coresProdutos = {};
    if (opcoesGraficoLume.comparacao) {
        if (!opcoesGraficoLume.ordem) {
            opcoesGraficoLume.ordem = parseInt(id.substr(-1));
        }
        if (opcoesGraficoLume.ordem == 1) {
            opcoesGraficoLume.totais = totais1;
            if (totais1_por_produto) {
                opcoesGraficoLume.totais_por_produto = totais1_por_produto;
            }
        } else {
            opcoesGraficoLume.totais = totais2;
            if (totais2_por_produto) {
                opcoesGraficoLume.totais_por_produto = totais2_por_produto;
            }
        }
        if (totais1_por_produto) {
            for (var c = 0; c < totais1_por_produto.length; c++) {
                coresProdutos[totais1_por_produto[c].nome] = window.coresProdutos[c % window.coresProdutos.length];
            }
            var l = c;
            if (totais2_por_produto) {
                for (var cc = 0; cc < totais2_por_produto.length; cc++) {
                    if (!coresProdutos[totais2_por_produto[cc].nome]) {
                        coresProdutos[totais2_por_produto[cc].nome] = window.coresProdutos[l % window.coresProdutos.length];
                        l++;
                    }
                }
            }
        }
    } else {
        opcoesGraficoLume.totais = totais;
        if (typeof totais_por_produto != 'undefined') {
            opcoesGraficoLume.totais_por_produto = totais_por_produto;
            for (var c = 0; c < totais_por_produto.length; c++) {
                coresProdutos[totais_por_produto[c].nome] = window.coresProdutos[c % window.coresProdutos.length];
            }
        }
    }
    opcoesGraficoLume.coresProdutos = coresProdutos;
    return opcoesGraficoLume;
}

function makeCustomTooltipStackedBars(tooltipModel, chart) {
    // Tooltip Element
    var tooltipEl = document.getElementById('chartjs-tooltip');

    // Create element on first render
    if (!tooltipEl) {
        tooltipEl = document.createElement('div');
        tooltipEl.id = 'chartjs-tooltip';
        tooltipEl.innerHTML = '<table></table>';
        document.body.appendChild(tooltipEl);
    }

    // Hide if no tooltip
    if (tooltipModel.opacity === 0) {
        tooltipEl.style.opacity = 0;
        return;
    }

    // Set caret Position
    tooltipEl.classList.remove('above', 'below', 'no-transform');
    if (tooltipModel.yAlign) {
        tooltipEl.classList.add(tooltipModel.yAlign);
    } else {
        tooltipEl.classList.add('no-transform');
    }

    function getBody(bodyItem) {
        return bodyItem.lines;
    }
    // Set Text
    if (tooltipModel.body) {
        var titleLines = tooltipModel.title || [];
        var bodyLines = tooltipModel.body.map(getBody);
        var innerHtml = '<thead>';

        titleLines.forEach(function(title) {
            innerHtml += '<tr><th>' + title + '</th></tr>';
        });
        innerHtml += '</thead><tbody>';

        bodyLines.forEach(function(body, i) {
            var colors = tooltipModel.labelColors[i];
            var style = 'background:' + colors.backgroundColor;
            style += '; border-color:' + colors.borderColor;
            style += '; border-width: 2px; height:20px; width:20px';
            //var span = '<span style="' + style + '"> </span>';
            var span = '<span class="d-inline-block mr-1" style="height: 12px; width:12px; background-color:' + colors.backgroundColor + '; border: solid ' + '#fff' + ' 1px"></span>';
            var bodyParts = body[0].split(': ');
            if (parseFloat(bodyParts[1])!=0) {
                innerHtml += '<tr><td>' + span + bodyParts[0] + ': ' + valueToBRLCurrency(bodyParts[1]) + '</td></tr>';
            }
        });
        innerHtml += '</tbody>';

        var tableRoot = tooltipEl.querySelector('table');
        tableRoot.innerHTML = innerHtml;
    }

    // `this` will be the overall tooltip
    var position = chart.canvas.getBoundingClientRect();

    // Display, position, and set styles for font
    tooltipEl.style.opacity = 0.9;
    tooltipEl.style.left = position.left + window.pageXOffset + tooltipModel.caretX + 'px';
    tooltipEl.style.top = position.top + window.pageYOffset + tooltipModel.caretY + 'px';
    tooltipEl.style.fontFamily = tooltipModel._bodyFontFamily;
    tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
    tooltipEl.style.fontStyle = tooltipModel._bodyFontStyle;
    tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
    tooltipEl.style.pointerEvents = 'none';
}

function inicializa_grafico_origem_rendas(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }
    var pieChartData = {
        labels: [gettext('Rendas Agrícolas'), gettext('Pluriatividade'), gettext('Transferência de Renda')],
        datasets: [{
            data: [opcoesGraficoLume.totais.total_ra, opcoesGraficoLume.totais.rna_pluriatividade, opcoesGraficoLume.totais.rna_transferencia,],
            backgroundColor: [window.chartColors.ra, window.chartColors.rna_pluriatividade, window.chartColors.rna_transferencia],
            label: gettext('Dataset 1')
        }]
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'pie',
        data: pieChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? subtitulo : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.labels[tooltipItem.index];
                        return label + ":" + valueToBRLCurrency(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]);
                    }
                }
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'bottom'
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions('sm', 'pie')
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            }
        }
    });
}

/**
 *
 * @param id
 * @param totais
 * @param subtitulo
 */
function inicializa_grafico_composicao_rendas(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }
    var barChartData = {
        labels: ['', '', '', '', '', ''],
        datasets: [{
            label: gettext('PB'),
            backgroundColor: window.chartColors.pb,
            data: [
                opcoesGraficoLume.totais.total_pb, 0, 0, 0, 0, 0
            ]
        }, {
            label: gettext('Estoque'),
            backgroundColor: window.chartColors.estoque,
            data: [
                0, opcoesGraficoLume.totais.total_pb_estoque, 0, 0, 0, 0
            ]
        }, {
            label: gettext('Doações/Trocas'),
            backgroundColor: window.chartColors.doacoes_trocas,
            data: [
                0, opcoesGraficoLume.totais.total_pb_doacoes_trocas, 0, 0, 0, 0
            ]
        }, {
            label: gettext('Autoconsumo'),
            backgroundColor: window.chartColors.autoconsumo,
            data: [
                0, opcoesGraficoLume.totais.total_pb_autoconsumo, 0, 0, 0, 0
            ]
        }, {
            label: gettext('Venda'),
            backgroundColor: window.chartColors.venda,
            data: [
                0, opcoesGraficoLume.totais.total_pb_venda, 0, 0, 0, 0
            ]
        }, {
            label: gettext('CP'),
            backgroundColor: window.chartColors.cp,
            data: [
                0, 0, 0, 0, 0, opcoesGraficoLume.totais.total_cp
            ]
        }, {
            label: gettext('CI'),
            backgroundColor: window.chartColors.ci,
            data: [
                0, 0, opcoesGraficoLume.totais.total_ci, 0, opcoesGraficoLume.totais.total_ci, 0
            ]
        }, {
            label: gettext('CIFT'),
            backgroundColor: window.chartColors.cift,
            data: [
                0, 0, 0, opcoesGraficoLume.totais.total_ci_f, 0, 0
            ]
        }, {
            label: gettext('VAT'),
            backgroundColor: window.chartColors.vat,
            data: [
                0, 0, 0, opcoesGraficoLume.totais.total_vat, 0, 0
            ]
        }, {
            label: gettext('VA'),
            backgroundColor: window.chartColors.va,
            data: [
                0, 0, opcoesGraficoLume.totais.total_va, 0, 0, 0
            ]
        }, {
            label: gettext('PT'),
            backgroundColor: window.chartColors.pt,
            data: [
                0, 0, 0, 0, opcoesGraficoLume.totais.total_pt, 0
            ]
        }, {
            label: gettext('RA'),
            backgroundColor: window.chartColors.ra,
            data: [
                0, 0, 0, 0, opcoesGraficoLume.totais.total_ra, 0
            ]
        }, {
            label: gettext('RAM'),
            backgroundColor: window.chartColors.ram,
            data: [
                0, 0, 0, 0, 0, opcoesGraficoLume.totais.total_ram
            ]
        }        ]
    };

    var filterTooltips = function (item, chart_data) {
        return item.yLabel != 0;
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? subtitulo : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                filter: filterTooltips,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';
                        if (label) {
                            label += ': ';
                        }
                        return label + valueToBRLCurrency(tooltipItem.yLabel);
                    }
                }
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'right'
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? chartJsDataLabelsOptions('sm', 'stackedbars', false, 'labels') : chartJsDataLabelsOptions()
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    ticks: {
                        stepSize: 1,
                        min: 0,
                        autoSkip: false
                    }
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels);
                        }
                    }
                }]
            }
        }
    });
}

function inicializa_grafico_composicao_rendas_por_area(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais || !opcoesGraficoLume.totais.area) {
        return;
    }
    var barChartData = {
        labels: ['', '', '', '', '', ''],
        datasets: [{
            label: gettext('PB'),
            backgroundColor: window.chartColors.pb,
            data: [
                opcoesGraficoLume.totais.total_pb / opcoesGraficoLume.totais.area, 0, 0, 0, 0, 0
            ]
        }, {
            label: gettext('Estoque'),
            backgroundColor: window.chartColors.estoque,
            data: [
                0, opcoesGraficoLume.totais.total_pb_estoque / opcoesGraficoLume.totais.area, 0, 0, 0, 0
            ]
        }, {
            label: gettext('Doaçoes/Trocas'),
            backgroundColor: window.chartColors.doacoes_trocas,
            data: [
                0, opcoesGraficoLume.totais.total_pb_doacoes_trocas / opcoesGraficoLume.totais.area, 0, 0, 0, 0
            ]
        }, {
            label: gettext('Autoconsumo'),
            backgroundColor: window.chartColors.autoconsumo,
            data: [
                0, opcoesGraficoLume.totais.total_pb_autoconsumo / opcoesGraficoLume.totais.area, 0, 0, 0, 0
            ]
        }, {
            label: gettext('Venda'),
            backgroundColor: window.chartColors.venda,
            data: [
                0, opcoesGraficoLume.totais.total_pb_venda / opcoesGraficoLume.totais.area, 0, 0, 0, 0
            ]
        }, {
            label: gettext('CI'),
            backgroundColor: window.chartColors.ci,
            data: [
                0, 0, opcoesGraficoLume.totais.total_ci / opcoesGraficoLume.totais.area, 0, 0, 0
            ]
        }, {
            label: gettext('VA'),
            backgroundColor: window.chartColors.va,
            data: [
                0, 0, opcoesGraficoLume.totais.total_va / opcoesGraficoLume.totais.area, 0, 0, 0
            ]
        }, {
            label: gettext('CIFT'),
            backgroundColor: window.chartColors.cift,
            data: [
                0, 0, 0, opcoesGraficoLume.totais.total_ci_f / opcoesGraficoLume.totais.area, 0, 0
            ]
        }, {
            label: gettext('VAT'),
            backgroundColor: window.chartColors.vat,
            data: [
                0, 0, 0, opcoesGraficoLume.totais.total_vat / opcoesGraficoLume.totais.area, 0, 0
            ]
        }, {
            label: gettext('CP'),
            backgroundColor: window.chartColors.cp,
            data: [
                0, 0, 0, 0, opcoesGraficoLume.totais.total_cp / opcoesGraficoLume.totais.area, opcoesGraficoLume.totais.total_cp / opcoesGraficoLume.totais.area
            ]
        }, {
            label: gettext('RA'),
            backgroundColor: window.chartColors.ra,
            data: [
                0, 0, 0, 0, opcoesGraficoLume.totais.total_ra / opcoesGraficoLume.totais.area, 0
            ]
        }, {
            label: gettext('RAM'),
            backgroundColor: window.chartColors.ram,
            data: [
                0, 0, 0, 0, 0, opcoesGraficoLume.totais.total_ram / opcoesGraficoLume.totais.area
            ]
        }
        ]
    };

    var filterTooltips = function (item, chart_data) {
        return item.yLabel != 0;
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? subtitulo : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                filter: filterTooltips,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';

                        if (label) {
                            label += ': ';
                        }
                        return label + valueToBRLCurrency(tooltipItem.yLabel);
                    }
                }
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'right'
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions()
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    ticks: {
                        stepSize: 1,
                        min: 0,
                        autoSkip: false
                    }
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels);
                        }
                    }
                }]
            }
        }
    });
}

function inicializa_grafico_composicao_produto_bruto(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }
    /*
    var labels = [gettext('Agroecossistema')];
    var dados_pb_estoque = [opcoesGraficoLume.totais.total_pb_estoque];
    var dados_pb_doacoes_trocas = [opcoesGraficoLume.totais.total_pb_doacoes_trocas];
    var dados_pb_autoconsumo = [opcoesGraficoLume.totais.total_pb_autoconsumo];
    var dados_pb_venda = [opcoesGraficoLume.totais.total_pb_venda];
    */

    var labels = [];
    var dados_pb_estoque = [];
    var dados_pb_doacoes_trocas = [];
    var dados_pb_autoconsumo = [];
    var dados_pb_venda = [];
    var subsistema;

    for (var i = 0; i < opcoesGraficoLume.totais.totais_subsistemas.length; i++) {
        subsistema = opcoesGraficoLume.totais.totais_subsistemas[i];
        labels.push(subsistema.nome);
        dados_pb_estoque.push(subsistema.pb_estoque);
        dados_pb_doacoes_trocas.push(subsistema.pb_doacoes_trocas);
        dados_pb_autoconsumo.push(subsistema.pb_autoconsumo);
        dados_pb_venda.push(subsistema.pb_venda);
    }

    var barChartData = {
        labels: labels,
        datasets: [{
            label: gettext('Estoque'),
            backgroundColor: window.chartColors.estoque,
            data: dados_pb_estoque
        }, {
            label: gettext('Trocas e doações'),
            backgroundColor: window.chartColors.doacoes_trocas,
            data: dados_pb_doacoes_trocas
        }, {
            label: gettext('Autoconsumo'),
            backgroundColor: window.chartColors.autoconsumo,
            data: dados_pb_autoconsumo
        }, {
            label: gettext('Venda'),
            backgroundColor: window.chartColors.venda,
            data: dados_pb_venda
        }]
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? subtitulo : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';

                        if (label) {
                            label += ': ';
                        }
                        return label + valueToBRLCurrency(tooltipItem.yLabel);
                    }
                }
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'bottom'
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions('sm')
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    ticks: {
                        stepSize: 1,
                        min: 0,
                        autoSkip: false
                    }
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels);
                        }
                    },
                }]
            }
        }
    });
}

function inicializa_grafico_composicao_renda_bruta_1(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }
    // var labels = [gettext('Agroecossistema')];
    // var dados_ci = [opcoesGraficoLume.totais.total_ci];
    // var dados_va = [opcoesGraficoLume.totais.total_va];
    var labels = [];
    var dados_ci = [];
    var dados_va = [];

    var subsistema;
    for (var i = 0; i < opcoesGraficoLume.totais.totais_subsistemas.length; i++) {
        subsistema = opcoesGraficoLume.totais.totais_subsistemas[i];
        labels.push(subsistema.nome);
        dados_ci.push(subsistema.ci);
        dados_va.push(subsistema.va);
    }

    var barChartData = {
        labels: labels,
        datasets: [{
            label: gettext('Consumos Intermediários'),
            backgroundColor: window.chartColors.ci,
            data: dados_ci
        }, {
            label: gettext('Valor Agregado'),
            backgroundColor: window.chartColors.va,
            data: dados_va
        }]
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? subtitulo : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';

                        if (label) {
                            label += ': ';
                        }
                        return label + valueToBRLCurrency(tooltipItem.yLabel);
                    }
                }
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'bottom'
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions('sm')
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    ticks: {
                        stepSize: 1,
                        min: 0,
                        autoSkip: false
                    }
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels);
                        }
                    }
                }]
            }
        }
    });

}

function inicializa_grafico_composicao_renda_bruta_2(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }
    // var labels = [gettext('Agroecossistema')];
    // var dados_ci_f = [opcoesGraficoLume.totais.total_ci_f];
    // var dados_vat = [opcoesGraficoLume.totais.total_vat];
    var labels = [];
    var dados_ci_f = [];
    var dados_vat = [];

    var subsistema;
    for (var i = 0; i < opcoesGraficoLume.totais.totais_subsistemas.length; i++) {
        subsistema = opcoesGraficoLume.totais.totais_subsistemas[i];
        labels.push(subsistema.nome);
        dados_ci_f.push(subsistema.ci_f);
        dados_vat.push(subsistema.vat);
    }

    var barChartData = {
        labels: labels,
        datasets: [{
            label: gettext('Consumos Intermediários - Fora do Território'),
            backgroundColor: window.chartColors.cift,
            data: dados_ci_f
        }, {
            label: gettext('Valor Agregado Territorial'),
            backgroundColor: window.chartColors.vat,
            data: dados_vat
        }]
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? subtitulo : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';

                        if (label) {
                            label += ': ';
                        }
                        return label + valueToBRLCurrency(tooltipItem.yLabel);
                    }
                }
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'bottom'
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions('sm')
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    ticks: {
                        stepSize: 1,
                        min: 0,
                        autoSkip: false
                    }
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels);
                        }
                    }
                }]
            }
        }
    });

}

function inicializa_grafico_composicao_renda_bruta_monetaria(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }
    // var labels = [gettext('Agroecossistema')];
    // var dados_ram = [opcoesGraficoLume.totais.total_ram];
    // var dados_cp = [opcoesGraficoLume.totais.total_cp];
    var labels = [];
    var dados_ram = [];
    var dados_cp = [];

    var subsistema;
    for (var i = 0; i < opcoesGraficoLume.totais.totais_subsistemas.length; i++) {
        subsistema = opcoesGraficoLume.totais.totais_subsistemas[i];
        labels.push(subsistema.nome);
        dados_ram.push(subsistema.ram);
        dados_cp.push(subsistema.cp);
    }

    var barChartData = {
        labels: labels,
        datasets: [{
            label: gettext('Renda Agrícola Monetária'),
            backgroundColor: window.chartColors.ram,
            data: dados_ram
        }, {
            label: gettext('Custos Produtivos'),
            backgroundColor: window.chartColors.cp,
            data: dados_cp
        }]
    };
    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? subtitulo : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';

                        if (label) {
                            label += ': ';
                        }
                        return label + valueToBRLCurrency(tooltipItem.yLabel);
                    }
                }
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'right'
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions('sm')
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    ticks: {
                        stepSize: 1,
                        min: 0,
                        autoSkip: false
                    }
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels);
                        }
                    }
                }]
            }
        }
    });

}

function inicializa_grafico_producoes_por_venda(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais_por_produto) {
        return;
    }
    var labels = [gettext('Mercado Territorial'), gettext('Mercado Convencional'), gettext('Mercado Institucional')];

    var datasets = [];
    //copia array p/ novo
    var pp2 = opcoesGraficoLume.totais_por_produto.filter(function (obj) {
        return parseFloat(obj.pb_venda_territorial) >= 0.01 || parseFloat(obj.pb_venda_convencional) >= 0.01 || parseFloat(obj.pb_venda_institucional) >= 0.01
    });

    pp2.sort(function (a, b) {
        vala = parseFloat(a.pb);
        valb = parseFloat(b.pb);
        return vala > valb ? -1 : vala < valb ? 1 : 0;
    });

    for (var i = 0; i < pp2.length; i++) {
        datasets.push({
            label: pp2[i].nome,
            backgroundColor: opcoesGraficoLume.coresProdutos[pp2[i].nome],
            data: [pp2[i].pb_venda_territorial, pp2[i].pb_venda_convencional, pp2[i].pb_venda_institucional]
        })
    }


    var barChartData = {
        labels: labels,
        datasets: datasets
    };

    var filterTooltips = function (item, chart_data) {
        return item.yLabel != 0 && !isNaN(item.yLabel);
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? [subtitulo, '(' + pp2.length.toString() + ' ' + gettext('produtos') + ')'] : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            tooltips: {
                // Disable the on-canvas tooltip
                enabled: false,
                mode: 'index',
                custom: function(tooltipModel) {
                    makeCustomTooltipStackedBars(tooltipModel, this._chart);
                }
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'bottom'
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions('sm')
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    autoSkip: false,
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels);
                        },
                        autoSkip: false
                    }
                }]
            }
        }
    });
}

function inicializa_grafico_producoes1(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais_por_produto) {
        return;
    }
    var labels = [gettext('Produções'), gettext('Venda'), gettext('Autoconsumo'), gettext('Doações e Trocas'), gettext('Estoque')];

    var datasets = [];

    //copia array p/ novo
    var pp2 = opcoesGraficoLume.totais_por_produto.filter(function (obj) {
        return parseFloat(obj.pb_venda) >= 0.01 || parseFloat(obj.pb_autoconsumo) >= 0.01 || parseFloat(obj.pb_doacoes_trocas) >= 0.01 || parseFloat(obj.pb_estoque) >= 0.01
    });

    pp2.sort(function (a, b) {
        vala = parseFloat(a.pb);
        valb = parseFloat(b.pb);
        return vala > valb ? -1 : vala < valb ? 1 : 0;
    });

    for (var i = 0; i < pp2.length; i++) {
        datasets.push({
            label: pp2[i].nome,
            backgroundColor: opcoesGraficoLume.coresProdutos[pp2[i].nome],
            data: [pp2[i].pb, pp2[i].pb_venda, pp2[i].pb_autoconsumo, pp2[i].pb_doacoes_trocas, pp2[i].pb_estoque]
        })
    }


    var barChartData = {
        labels: labels,
        datasets: datasets
    };

    var filterTooltips = function (item, chart_data) {
        return item.yLabel != 0 && !isNaN(item.yLabel);
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? [subtitulo, '(' + pp2.length.toString() + ' ' + gettext('produtos') + ')'] : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            tooltips: {
                // Disable the on-canvas tooltip
                enabled: false,
                mode: 'index',
                custom: function(tooltipModel) {
                    makeCustomTooltipStackedBars(tooltipModel, this._chart);
                }
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'bottom'
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions('sm')
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    autoSkip: false,
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels);
                        },
                        autoSkip: false
                    }
                }]
            }
        }
    });
}

function inicializa_grafico_producoes2(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais_por_produto) {
        return;
    }

    var backgroundColors = [];
    var data = [];
    var labels = [];

    var datasets = [];

    //copia array p/ novo
    var pp2 = opcoesGraficoLume.totais_por_produto.filter(function (obj) {
        return parseFloat(obj.pb) >= 0.01
    });

    pp2.sort(function (a, b) {
        vala = parseFloat(a.pb);
        valb = parseFloat(b.pb);
        return vala > valb ? -1 : vala < valb ? 1 : 0;
    });

    for (var i = 0; i < pp2.length; i++) {
        data.push(pp2[i].pb);
        backgroundColors.push(opcoesGraficoLume.coresProdutos[pp2[i].nome]);
        labels.push(pp2[i].nome);
    }


    var pieChartData = {
        labels: labels,
        datasets: [{
            data: data,
            backgroundColor: backgroundColors,
            label: gettext('Dataset 1')
        }]
    };

    var filterTooltips = function (item, chart_data) {
        return item.yLabel != 0 && !isNaN(item.yLabel);
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'pie',
        data: pieChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? [subtitulo, '(' + pp2.length.toString() + ' ' + gettext('produtos') + ')'] : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'bottom'
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions('sm', 'pie')
            },
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.labels[tooltipItem.index];
                        return label + ":" + valueToBRLCurrency(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]);
                    }
                }
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
        }
    });
}

function inicializa_grafico_producoes_venda(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais_por_produto) {
        return;
    }

    var backgroundColors = [];
    var data = [];
    var labels = [];

    //copia array p/ novo
    var pp2 = opcoesGraficoLume.totais_por_produto.filter(function (obj) {
        return parseFloat(obj.pb_venda) >= 0.01
    });

    pp2.sort(function (a, b) {
        vala = parseFloat(a.pb_venda);
        valb = parseFloat(b.pb_venda);
        return vala > valb ? -1 : vala < valb ? 1 : 0;
    });

    for (var i = 0; i < pp2.length; i++) {
        data.push(pp2[i].pb_venda);
        backgroundColors.push(opcoesGraficoLume.coresProdutos[pp2[i].nome]);
        labels.push(pp2[i].nome);
    }


    var pieChartData = {
        labels: labels,
        datasets: [{
            data: data,
            backgroundColor: backgroundColors,
            label: gettext('Dataset 1')
        }]
    };

    var filterTooltips = function (item, chart_data) {
        return item.yLabel != 0 && !isNaN(item.yLabel);
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'pie',
        data: pieChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? [subtitulo, '(' + pp2.length.toString() + ' ' + gettext('produtos') + ')'] : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'bottom'
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions('sm', 'pie')
            },
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.labels[tooltipItem.index];
                        return label + ":" + valueToBRLCurrency(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]);
                    }
                }
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
        }
    });
}

function inicializa_grafico_producoes_autoconsumo(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais_por_produto) {
        return;
    }

    var backgroundColors = [];
    var data = [];
    var labels = [];

    //copia array p/ novo
    var pp2 = opcoesGraficoLume.totais_por_produto.filter(function (obj) {
        return parseFloat(obj.pb_autoconsumo) >= 0.01
    });

    pp2.sort(function (a, b) {
        vala = parseFloat(a.pb_autoconsumo);
        valb = parseFloat(b.pb_autoconsumo);
        return vala > valb ? -1 : vala < valb ? 1 : 0;
    });

    for (var i = 0; i < pp2.length; i++) {
        data.push(pp2[i].pb_autoconsumo);
        backgroundColors.push(opcoesGraficoLume.coresProdutos[pp2[i].nome]);
        labels.push(pp2[i].nome);
    }


    var pieChartData = {
        labels: labels,
        datasets: [{
            data: data,
            backgroundColor: backgroundColors,
            label: gettext('Dataset 1')
        }]
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'pie',
        data: pieChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? [subtitulo, '(' + pp2.length.toString() + ' ' + gettext('produtos') + ')'] : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'bottom'
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions('sm', 'pie')
            },
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.labels[tooltipItem.index];
                        return label + ":" + valueToBRLCurrency(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]);
                    }
                }
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
        }
    });
}


function inicializa_grafico_efetivo_va_area(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }
    var data = [];
    var labels = [];
    var totalParcial = {x: 0, y: 0, label: "", ownX: 0, ownY: 0};
    data.push(Object.assign({}, totalParcial));
    var j = 0;
    for (var i = 0; i < opcoesGraficoLume.totais.totais_subsistemas.length; i++) {
        if (opcoesGraficoLume.totais.totais_subsistemas[i].area && opcoesGraficoLume.totais.totais_subsistemas[i].va) {
            totalParcial.ownX = parseFloat(opcoesGraficoLume.totais.totais_subsistemas[i].area);
            totalParcial.ownY = parseFloat(opcoesGraficoLume.totais.totais_subsistemas[i].va);
            totalParcial.x += parseFloat(opcoesGraficoLume.totais.totais_subsistemas[i].area);
            totalParcial.y += parseFloat(opcoesGraficoLume.totais.totais_subsistemas[i].va);
            totalParcial.label = opcoesGraficoLume.totais.totais_subsistemas[i].nome;
            data.push(Object.assign({}, totalParcial));
        }
    }

    var lineChartData = {
        labels: labels,
        datasets: [{
            data: data,
            pointBackgroundColor: window.chartColors.blue,
            pointBorderWidth: 0,
            pointBorderColor: window.chartColors.blue,
            showLine: true,
            borderColor: 'rgb(150,150,150)',
            borderWidth: 2,
            label: gettext('Dataset 1'),
            fill: false
        }]
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'scatter',
        data: lineChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? subtitulo : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            legend: {display: false},
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions('sm', 'scatter')
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem, false);
                }
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            elements: {
                line: {
                    tension: 0
                }
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: gettext('Área (ha)')
                    }
                }],
                yAxes: [{
                    id: 'y-axis-0',
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: gettext('Valor Agregado (R$)'),
                    },
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels, false);
                        }
                    }
                }]
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].label;
                        var area_label = gettext('Área') + ': ' + valueToBRLCurrency(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].ownX, false) + ' ' + gettext('ha');
                        var va_label = gettext('VA') + ': ' + valueToBRLCurrency(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].ownY);
                        return label + ' (' + area_label + ', ' + va_label + ')'
                    }
                },
                filter: function (tooltipItem, data) {
                    return (tooltipItem.index > 0);
                }
            }
        }
    });
}

function inicializa_grafico_proporcional_va_area(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }
    var data = [];
    var labels = [];
    var totalParcial = {x: 0, y: 0, label: "", ownX: 0, ownY: 0};
    data.push(Object.assign({}, totalParcial));
    var j = 0;
    var areaTotalSubsistemas = 0;
    for (var i = 0; i < opcoesGraficoLume.totais.totais_subsistemas.length; i++) {
        if (opcoesGraficoLume.totais.totais_subsistemas[i].area) {
            areaTotalSubsistemas += parseFloat(opcoesGraficoLume.totais.totais_subsistemas[i].area);
        }
    }
    for (var i = 0; i < opcoesGraficoLume.totais.totais_subsistemas.length; i++) {
        if (opcoesGraficoLume.totais.totais_subsistemas[i].area && opcoesGraficoLume.totais.totais_subsistemas[i].va) {
            totalParcial.ownX = parseFloat(opcoesGraficoLume.totais.totais_subsistemas[i].area) / areaTotalSubsistemas;
            totalParcial.ownY = parseFloat(opcoesGraficoLume.totais.totais_subsistemas[i].va) / areaTotalSubsistemas;
            totalParcial.x += parseFloat(opcoesGraficoLume.totais.totais_subsistemas[i].area) / areaTotalSubsistemas;
            totalParcial.y += parseFloat(opcoesGraficoLume.totais.totais_subsistemas[i].va) / areaTotalSubsistemas;
            totalParcial.label = opcoesGraficoLume.totais.totais_subsistemas[i].nome;
            data.push(Object.assign({}, totalParcial));
        }
    }
    var lineChartData = {
        labels: labels,
        datasets: [{
            data: data,
            pointBackgroundColor: window.chartColors.blue,
            pointBorderWidth: 0,
            pointBorderColor: window.chartColors.blue,
            showLine: true,
            borderColor: 'rgb(150,150,150)',
            borderWidth: 2,
            label: gettext('Dataset 1'),
            fill: false
        }]
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'scatter',
        data: lineChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? subtitulo : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            legend: {display: false},
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions('sm', 'scatter')
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem, false);
                }
            },
            hover: {
                mode: 'point',
                intersect: true
            },
            elements: {
                line: {
                    tension: 0
                }
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: gettext('Área (ha)')
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: gettext('Valor Agregado (R$)')
                    },
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels, false);
                        }
                    }
                }]
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].label;
                        var area_label = gettext('Área') + ': ' + valueToBRLCurrency(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].ownX, false) + ' ' + gettext('ha');
                        var va_label = gettext('VA') + ': ' + valueToBRLCurrency(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].ownY);
                        return label + ' (' + area_label + ', ' + va_label + ')'
                    }
                },
                filter: function (tooltipItem, data) {
                    return (tooltipItem.index > 0);
                }
            }
        }
    });
}


function inicializa_grafico_intensidade_trabalho(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }
    var data = [];
    var labels = [];
    var totalParcial = {x: 0, y: 0, label: "", ownX: 0, ownY: 0};
    data.push(Object.assign({}, totalParcial));
    var j = 0;
    for (var i = 0; i < opcoesGraficoLume.totais.totais_subsistemas.length; i++) {
        if (opcoesGraficoLume.totais.totais_subsistemas[i].totais_tempostrabalho && opcoesGraficoLume.totais.totais_subsistemas[i].totais_tempostrabalho.horas_total && opcoesGraficoLume.totais.totais_subsistemas[i].va) {
            totalParcial.ownX = parseFloat(opcoesGraficoLume.totais.totais_subsistemas[i].totais_tempostrabalho.horas_total);
            totalParcial.ownY = parseFloat(opcoesGraficoLume.totais.totais_subsistemas[i].va);
            totalParcial.x += parseFloat(opcoesGraficoLume.totais.totais_subsistemas[i].totais_tempostrabalho.horas_total);
            totalParcial.y += parseFloat(opcoesGraficoLume.totais.totais_subsistemas[i].va);
            totalParcial.label = opcoesGraficoLume.totais.totais_subsistemas[i].nome;
            data.push(Object.assign({}, totalParcial));
        }
    }

    var lineChartData = {
        labels: labels,
        datasets: [{
            data: data,
            pointBackgroundColor: window.chartColors.blue,
            pointBorderWidth: 0,
            pointBorderColor: window.chartColors.blue,
            showLine: true,
            borderColor: 'rgb(150,150,150)',
            borderWidth: 2,
            label: gettext('Dataset 1'),
            fill: false
        }]
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'scatter',
        data: lineChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? subtitulo : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            legend: {display: false},
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions('sm', 'scatter')
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem, false);
                }
            },
            hover: {
                mode: 'point',
                intersect: true
            },
            elements: {
                line: {
                    tension: 0
                }
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: gettext('Trabalho (horas)')
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: gettext('Valor Agregado (R$)')
                    },
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels, false);
                        }
                    }
                }]
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].label;
                        var area_label = gettext('Trabalho') + ': ' + valueToBRLCurrency(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].ownX, false) + ' ' + gettext('horas');
                        var va_label = gettext('VA') + ': ' + valueToBRLCurrency(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].ownY);
                        return label + ' (' + area_label + ', ' + va_label + ')'
                    }
                },
                filter: function (tooltipItem, data) {
                    return (tooltipItem.index > 0);
                }
            },
        }
    });
}

function inicializa_grafico_intensidade_uso_terra(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }
    var data = [];
    var labels = [];
    var j = 0;
    var totalParcial = {x: 0, y: 0, label: "", ownX: 0, ownY: 0};
    data.push(Object.assign({}, totalParcial));
    for (var i = 0; i < opcoesGraficoLume.totais.totais_subsistemas.length; i++) {
        if (opcoesGraficoLume.totais.totais_subsistemas[i].intensidade_ocupacao_trabalho_porarea && opcoesGraficoLume.totais.totais_subsistemas[i].produtividade_do_trabalho_2) {
            totalParcial.ownX = parseFloat(opcoesGraficoLume.totais.totais_subsistemas[i].intensidade_ocupacao_trabalho_porarea);
            totalParcial.ownY = parseFloat(opcoesGraficoLume.totais.totais_subsistemas[i].produtividade_do_trabalho_2);
            totalParcial.x += parseFloat(opcoesGraficoLume.totais.totais_subsistemas[i].intensidade_ocupacao_trabalho_porarea);
            totalParcial.y += parseFloat(opcoesGraficoLume.totais.totais_subsistemas[i].produtividade_do_trabalho_2);
            totalParcial.label = opcoesGraficoLume.totais.totais_subsistemas[i].nome;
            data.push(Object.assign({}, totalParcial));
        }
    }

    var lineChartData = {
        labels: labels,
        datasets: [{
            data: data,
            pointBackgroundColor: window.chartColors.blue,
            pointBorderWidth: 0,
            pointBorderColor: window.chartColors.blue,
            showLine: true,
            borderColor: 'rgb(150,150,150)',
            borderWidth: 2,
            label: gettext('Dataset 1'),
            fill: false
        }]
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'scatter',
        data: lineChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? subtitulo : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            legend: {display: false},
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions('sm', 'scatter')
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem, false);
                }
            },
            hover: {
                mode: 'point',
                intersect: true
            },
            elements: {
                line: {
                    tension: 0
                }
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: gettext('ha/UTF')
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: gettext('Valor Agregado/UTF (R$)')
                    },
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels, false);
                        }
                    }
                }]
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].label;
                        var area_label = gettext('ha/UTF') + ': ' + valueToBRLCurrency(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].ownX, false);
                        var va_label = gettext('VA/UTF') + ': ' + valueToBRLCurrency(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].ownY);
                        return label + ' (' + area_label + ', ' + va_label + ')'
                    }
                },
                filter: function (tooltipItem, data) {
                    return (tooltipItem.index > 0);
                }
            },
        }
    });
}

// GRÁFICOS TRABALHO:

function inicializa_grafico_rendas_responsaveis_et(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }
    var labels = [gettext('Mulher'), gettext('Homem')];

    var mercantil = [opcoesGraficoLume.totais.reparticao_mercantil_af, opcoesGraficoLume.totais.reparticao_mercantil_am];
    var domestico_cuidados = [opcoesGraficoLume.totais.reparticao_domestico_cuidados_af, opcoesGraficoLume.totais.reparticao_domestico_cuidados_am];
    var participacao_social = [opcoesGraficoLume.totais.reparticao_participacao_social_af, opcoesGraficoLume.totais.reparticao_participacao_social_am];
    var pluriatividade = [opcoesGraficoLume.totais.reparticao_pluriatividade_af, opcoesGraficoLume.totais.reparticao_pluriatividade_am];

    var filterTooltips = function (item, chart_data) {
        return item.yLabel != 0;
    };

    var barChartData = {
        labels: labels,
        datasets: [{
            label: gettext('Mercantil e Autoconsumo'),
            backgroundColor: window.chartColors.mercantil,
            data: mercantil
        }, {
            label: gettext('Doméstico e de Cuidados'),
            backgroundColor: window.chartColors.domestico_cuidados,
            data: domestico_cuidados
        }, {
            label: gettext('Participação Social'),
            backgroundColor: window.chartColors.participacao_social,
            data: participacao_social
        }, {
            label: gettext('Pluriatividade'),
            backgroundColor: window.chartColors.pluriatividade,
            data: pluriatividade
        }]
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? subtitulo : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            tooltips: {
                mode: 'index',
                filter: filterTooltips,
                intersect: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';

                        if (label) {
                            label += ': ';
                        }
                        return label + valueToBRLCurrency(tooltipItem.yLabel);
                    }
                }
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'right'
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions('m', 'stackedbars', true, 'values')
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    autoSkip: false
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels);
                        }
                    }
                }]
            }
        }
    });

}


function inicializa_grafico_rendas_genero_et(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }
    var labels = [gettext('Mulher'), gettext('Homem'), gettext('Jovens Mulheres'), gettext('Jovens Homens'), gettext('Outras'), gettext('Outros')];

    var mercantil = [opcoesGraficoLume.totais.reparticao_mercantil_af, opcoesGraficoLume.totais.reparticao_mercantil_am, opcoesGraficoLume.totais.reparticao_mercantil_jf, opcoesGraficoLume.totais.reparticao_mercantil_jm, opcoesGraficoLume.totais.reparticao_mercantil_of, opcoesGraficoLume.totais.reparticao_mercantil_om];
    var domestico_cuidados = [opcoesGraficoLume.totais.reparticao_domestico_cuidados_af, opcoesGraficoLume.totais.reparticao_domestico_cuidados_am, opcoesGraficoLume.totais.reparticao_domestico_cuidados_jf, opcoesGraficoLume.totais.reparticao_domestico_cuidados_jm, opcoesGraficoLume.totais.reparticao_domestico_cuidados_of, opcoesGraficoLume.totais.reparticao_domestico_cuidados_om];
    var participacao_social = [opcoesGraficoLume.totais.reparticao_participacao_social_af, opcoesGraficoLume.totais.reparticao_participacao_social_am, opcoesGraficoLume.totais.reparticao_participacao_social_jf, opcoesGraficoLume.totais.reparticao_participacao_social_jm, opcoesGraficoLume.totais.reparticao_participacao_social_of, opcoesGraficoLume.totais.reparticao_participacao_social_om];
    var pluriatividade = [opcoesGraficoLume.totais.reparticao_pluriatividade_af, opcoesGraficoLume.totais.reparticao_pluriatividade_am, opcoesGraficoLume.totais.reparticao_pluriatividade_jf, opcoesGraficoLume.totais.reparticao_pluriatividade_jm, opcoesGraficoLume.totais.reparticao_pluriatividade_of, opcoesGraficoLume.totais.reparticao_pluriatividade_om];


    var barChartData = {
        labels: labels,
        datasets: [{
            label: gettext('Mercantil e Autoconsumo'),
            backgroundColor: window.chartColors.mercantil,
            data: mercantil
        }, {
            label: gettext('Doméstico e de Cuidados'),
            backgroundColor: window.chartColors.domestico_cuidados,
            data: domestico_cuidados
        }, {
            label: gettext('Participação Social'),
            backgroundColor: window.chartColors.participacao_social,
            data: participacao_social
        }, {
            label: gettext('Pluriatividade'),
            backgroundColor: window.chartColors.pluriatividade,
            data: pluriatividade
        }]
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? subtitulo : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';

                        if (label) {
                            label += ': ';
                        }
                        return label + valueToBRLCurrency(tooltipItem.yLabel);
                    }
                }
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'right'
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions('m', 'stackedbars', true, 'values')
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    autoSkip: false
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels);
                        }
                    }
                }]
            }
        }
    });

}

function inicializa_grafico_rendas_genero_geracao_et(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }
    var labels = [gettext('Mulher'), gettext('Homem'), gettext('Jovens Mulheres'), gettext('Jovens Homens')];

    var mercantil = [opcoesGraficoLume.totais.renda_mercantil_af, opcoesGraficoLume.totais.renda_mercantil_am, opcoesGraficoLume.totais.renda_mercantil_jf, opcoesGraficoLume.totais.renda_mercantil_jm];
    var domestico_cuidados = [opcoesGraficoLume.totais.renda_domestico_cuidados_af, opcoesGraficoLume.totais.renda_domestico_cuidados_am, opcoesGraficoLume.totais.renda_domestico_cuidados_jf, opcoesGraficoLume.totais.renda_domestico_cuidados_jm];
    var participacao_social = [opcoesGraficoLume.totais.renda_participacao_social_af, opcoesGraficoLume.totais.renda_participacao_social_am, opcoesGraficoLume.totais.renda_participacao_social_jf, opcoesGraficoLume.totais.renda_participacao_social_jm];
    var pluriatividade = [opcoesGraficoLume.totais.renda_pluriatividade_af, opcoesGraficoLume.totais.renda_pluriatividade_am, opcoesGraficoLume.totais.renda_pluriatividade_jf, opcoesGraficoLume.totais.renda_pluriatividade_jm];


    var barChartData = {
        labels: labels,
        datasets: [{
            label: gettext('Mercantil e Autoconsumo'),
            backgroundColor: window.chartColors.mercantil,
            data: mercantil
        }, {
            label: gettext('Doméstico e de Cuidados'),
            backgroundColor: window.chartColors.domestico_cuidados,
            data: domestico_cuidados
        }, {
            label: gettext('Participação Social'),
            backgroundColor: window.chartColors.participacao_social,
            data: participacao_social
        }, {
            label: gettext('Pluriatividade'),
            backgroundColor: window.chartColors.pluriatividade,
            data: pluriatividade
        }]
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? subtitulo : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';

                        if (label) {
                            label += ': ';
                        }
                        return label + valueToBRLCurrency(tooltipItem.yLabel);
                    }
                }
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'right'
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions('m', 'stackedbars', true, 'values')
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    autoSkip: false
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels);
                        }
                    }
                }]
            }
        }
    });

}

function inicializa_grafico_rendas_totais_pessoa_et(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }

    var labels = [];
    var mercantil = [];
    var domestico_cuidados = [];
    var participacao_social = [];
    var pluriatividade = [];

    var valor_hora = (opcoesGraficoLume.totais.renda_familiar_total - parseFloat(opcoesGraficoLume.totais.rna_transferencia)) / opcoesGraficoLume.totais.horas_total_total;

    for (var i in opcoesGraficoLume.totais.horas_trabalho_por_pessoa) {
        if (totais.horas_trabalho_por_pessoa[i].horas_total > 0) {
            labels.push(opcoesGraficoLume.totais.horas_trabalho_por_pessoa[i].pessoa_nome);
            mercantil.push(opcoesGraficoLume.totais.horas_trabalho_por_pessoa[i].horas_mercantil * valor_hora);
            domestico_cuidados.push(opcoesGraficoLume.totais.horas_trabalho_por_pessoa[i].horas_domestico_cuidados * valor_hora);
            participacao_social.push(opcoesGraficoLume.totais.horas_trabalho_por_pessoa[i].horas_participacao_social * valor_hora);
            pluriatividade.push(opcoesGraficoLume.totais.horas_trabalho_por_pessoa[i].horas_pluriatividade * valor_hora);
        }
    }

    var barChartData = {
        labels: labels,
        datasets: [{
            label: gettext('Mercantil e Autoconsumo'),
            backgroundColor: window.chartColors.mercantil,
            data: mercantil
        }, {
            label: gettext('Doméstico e de Cuidados'),
            backgroundColor: window.chartColors.domestico_cuidados,
            data: domestico_cuidados
        }, {
            label: gettext('Participação Social'),
            backgroundColor: window.chartColors.participacao_social,
            data: participacao_social
        }, {
            label: gettext('Pluriatividade'),
            backgroundColor: window.chartColors.pluriatividade,
            data: pluriatividade
        }]
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? subtitulo : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';

                        if (label) {
                            label += ': ';
                        }
                        return label + valueToBRLCurrency(tooltipItem.yLabel);
                    }
                }
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'right'
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions('m', 'stackedbars', false, 'values')
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    autoSkip: false
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels);
                        }
                    }
                }]
            }
        }
    });
}


function inicializa_grafico_utc_pessoa_subsistema(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }

    var labels = [];
    var datasets = [];
    var colors = window.chartColorsOnly;

    for (var i in totais_tempostrabalho.totais_tempostrabalho_totais_por_subsistema) {
        var tt_subsistema = totais_tempostrabalho.totais_tempostrabalho_totais_por_subsistema[i];
        var ss_data = [];

        for (var j in tt_subsistema.totais_tempostrabalhoporpessoa) {
            var total_horas_pessoa = Object.values(totais_tempostrabalho.totais_tempostrabalho_totais_por_pessoa).filter(function (el) {
                return el.nome == tt_subsistema.totais_tempostrabalhoporpessoa[j].nome ;
            });
            if (total_horas_pessoa.length > 0 && total_horas_pessoa[0].horas > 0){
                if (labels.indexOf(tt_subsistema.totais_tempostrabalhoporpessoa[j].nome) < 0) {
                    labels.push(tt_subsistema.totais_tempostrabalhoporpessoa[j].nome);
                }
                ss_data.push(tt_subsistema.totais_tempostrabalhoporpessoa[j].horas);
            }
        }
        datasets.push({
            label: totais_tempostrabalho.totais_tempostrabalho_totais_por_subsistema[i].nome,
            backgroundColor: colors[i % colors.length],
            data: ss_data
        });
    }

    var barChartData = {
        labels: labels,
        datasets: datasets
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? subtitulo : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';

                        if (label) {
                            label += ': ';
                        }
                        return label + valueToBRLCurrency(tooltipItem.yLabel, false);
                    }
                }
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'right'
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions('m', 'stackedbars', false, 'values')
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    autoSkip: false
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels, false);
                        }
                    }
                }]
            }
        }
    });
}


function inicializa_grafico_rendas_totais_et_genero(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }
    var labels = [gettext('Mercantil e Autoconsumo'), gettext('Doméstico e de Cuidados'), gettext('Participação Social'), gettext('Pluriatividade')];

    var total_m = [opcoesGraficoLume.totais.reparticao_mercantil_m, opcoesGraficoLume.totais.reparticao_domestico_cuidados_m, opcoesGraficoLume.totais.reparticao_participacao_social_m, opcoesGraficoLume.totais.reparticao_pluriatividade_m];
    var total_f = [opcoesGraficoLume.totais.reparticao_mercantil_f, opcoesGraficoLume.totais.reparticao_domestico_cuidados_f, opcoesGraficoLume.totais.reparticao_participacao_social_f, opcoesGraficoLume.totais.reparticao_pluriatividade_f];


    var barChartData = {
        labels: labels,
        datasets: [{
            label: gettext('Total Homens'),
            backgroundColor: window.chartColors.homens,
            data: total_m
        }, {
            label: gettext('Total Mulheres'),
            backgroundColor: window.chartColors.mulheres,
            data: total_f
        }]
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? subtitulo : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';

                        if (label) {
                            label += ': ';
                        }
                        return label + valueToBRLCurrency(tooltipItem.yLabel);
                    }
                }
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'right'
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions('m', 'stackedbars', true, 'values')
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    autoSkip: false
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels);
                        }
                    }
                }]
            }
        }
    });

}

function inicializa_grafico_rendas_totais_genero_et(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }
    var labels = [gettext('Total Mulheres'), gettext('Total Homens')];

    var mercantil = [opcoesGraficoLume.totais.reparticao_mercantil_f, opcoesGraficoLume.totais.reparticao_mercantil_m];
    var domestico_cuidados = [opcoesGraficoLume.totais.reparticao_domestico_cuidados_f, opcoesGraficoLume.totais.reparticao_domestico_cuidados_m];
    var participacao_social = [opcoesGraficoLume.totais.reparticao_participacao_social_f, opcoesGraficoLume.totais.reparticao_participacao_social_m];
    var pluriatividade = [opcoesGraficoLume.totais.reparticao_pluriatividade_f, opcoesGraficoLume.totais.reparticao_pluriatividade_m];


    var barChartData = {
        labels: labels,
        datasets: [{
            label: gettext('Mercantil e Autoconsumo'),
            backgroundColor: window.chartColors.mercantil,
            data: mercantil
        }, {
            label: gettext('Doméstico e de Cuidados'),
            backgroundColor: window.chartColors.domestico_cuidados,
            data: domestico_cuidados
        }, {
            label: gettext('Participação Social'),
            backgroundColor: window.chartColors.participacao_social,
            data: participacao_social
        }, {
            label: gettext('Pluriatividade'),
            backgroundColor: window.chartColors.pluriatividade,
            data: pluriatividade
        }]
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? subtitulo : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';

                        if (label) {
                            label += ': ';
                        }
                        return label + valueToBRLCurrency(tooltipItem.yLabel);
                    }
                }
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'right'
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions('m', 'stackedbars', true, 'values')
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    autoSkip: false
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels);
                        }
                    }
                }]
            }
        }
    });

}

function inicializa_grafico_rendas_totais_genero_et_UTF(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }
    var labels = [gettext('Total Mulheres'), gettext('Total Homens')];

    var mercantil = [opcoesGraficoLume.totais.horasporutf_mercantil_f, opcoesGraficoLume.totais.horasporutf_mercantil_m];
    var domestico_cuidados = [opcoesGraficoLume.totais.horasporutf_domestico_cuidados_f, opcoesGraficoLume.totais.horasporutf_domestico_cuidados_m];
    var participacao_social = [opcoesGraficoLume.totais.horasporutf_participacao_social_f, opcoesGraficoLume.totais.horasporutf_participacao_social_m];
    var pluriatividade = [opcoesGraficoLume.totais.horasporutf_pluriatividade_f, opcoesGraficoLume.totais.horasporutf_pluriatividade_m];

    var barChartData = {
        labels: labels,
        datasets: [{
            label: gettext('Mercantil e Autoconsumo'),
            backgroundColor: window.chartColors.mercantil,
            data: mercantil
        }, {
            label: gettext('Doméstico e de Cuidados'),
            backgroundColor: window.chartColors.domestico_cuidados,
            data: domestico_cuidados
        }, {
            label: gettext('Participação Social'),
            backgroundColor: window.chartColors.participacao_social,
            data: participacao_social
        }, {
            label: gettext('Pluriatividade'),
            backgroundColor: window.chartColors.pluriatividade,
            data: pluriatividade
        }]
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? subtitulo : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';

                        if (label) {
                            label += ': ';
                        }
                        return label + valueToBRLCurrency(tooltipItem.yLabel);
                    }
                }
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'right'
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions('m', 'stackedbars', true, 'values')
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    autoSkip: false
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels);
                        }
                    }
                }]
            }
        }
    });

}

function inicializa_grafico_rendas_et_pessoa_responsavel(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }

    var labels = [];
    var datasets = [];
    var colors = window.chartColorsOnly;

    var valor_hora = (opcoesGraficoLume.totais.renda_familiar_total - parseFloat(opcoesGraficoLume.totais.rna_transferencia)) / opcoesGraficoLume.totais.horas_total_total;
    let mercantil = [];
    let domestico_cuidados = [];
    let participacao_social = [];
    let pluriatividade = [];

    for (var i in opcoesGraficoLume.totais.horas_trabalho_por_pessoa) {
        if (totais.horas_trabalho_por_pessoa[i].responsavel) {
            labels.push(opcoesGraficoLume.totais.horas_trabalho_por_pessoa[i].pessoa_nome);
            mercantil.push(opcoesGraficoLume.totais.horas_trabalho_por_pessoa[i].horas_mercantil * valor_hora);
            domestico_cuidados.push(opcoesGraficoLume.totais.horas_trabalho_por_pessoa[i].horas_domestico_cuidados * valor_hora);
            participacao_social.push(opcoesGraficoLume.totais.horas_trabalho_por_pessoa[i].horas_participacao_social * valor_hora);
            pluriatividade.push(opcoesGraficoLume.totais.horas_trabalho_por_pessoa[i].horas_pluriatividade * valor_hora);
        }
    }

    var barChartData = {
        labels: labels,
        datasets: [{
            label: gettext('Mercantil e Autoconsumo'),
            backgroundColor: window.chartColors.mercantil,
            data: mercantil
        }, {
            label: gettext('Doméstico e de Cuidados'),
            backgroundColor: window.chartColors.domestico_cuidados,
            data: domestico_cuidados
        }, {
            label: gettext('Participação Social'),
            backgroundColor: window.chartColors.participacao_social,
            data: participacao_social
        }, {
            label: gettext('Pluriatividade'),
            backgroundColor: window.chartColors.pluriatividade,
            data: pluriatividade
        }]
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? subtitulo : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';

                        if (label) {
                            label += ': ';
                        }
                        return label + valueToBRLCurrency(tooltipItem.yLabel);
                    }
                }
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'right'
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions('m', 'stackedbars', false, 'values')
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    autoSkip: false
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels);
                        }
                    }
                }]
            }
        }
    });
}


function inicializa_grafico_utc_pessoa_et(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }

    var labels = [];
    var mercantil = [];
    var domestico_cuidados = [];
    var participacao_social = [];
    var pluriatividade = [];

    for (var i in opcoesGraficoLume.totais.horas_trabalho_por_pessoa) {
        if (totais.horas_trabalho_por_pessoa[i].horas_total > 0) {
            labels.push(opcoesGraficoLume.totais.horas_trabalho_por_pessoa[i].pessoa_nome);
            mercantil.push(opcoesGraficoLume.totais.horas_trabalho_por_pessoa[i].horas_mercantil / 2420);
            domestico_cuidados.push(opcoesGraficoLume.totais.horas_trabalho_por_pessoa[i].horas_domestico_cuidados / 2420);
            participacao_social.push(opcoesGraficoLume.totais.horas_trabalho_por_pessoa[i].horas_participacao_social / 2420);
            pluriatividade.push(opcoesGraficoLume.totais.horas_trabalho_por_pessoa[i].horas_pluriatividade / 2420);
        }
    }

    var barChartData = {
        labels: labels,
        datasets: [{
            label: gettext('Mercantil e Autoconsumo'),
            backgroundColor: window.chartColors.mercantil,
            data: mercantil
        }, {
            label: gettext('Doméstico e de Cuidados'),
            backgroundColor: window.chartColors.domestico_cuidados,
            data: domestico_cuidados
        }, {
            label: gettext('Participação Social'),
            backgroundColor: window.chartColors.participacao_social,
            data: participacao_social
        }, {
            label: gettext('Pluriatividade'),
            backgroundColor: window.chartColors.pluriatividade,
            data: pluriatividade
        }]
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? subtitulo : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';

                        if (label) {
                            label += ': ';
                        }
                        return label + valueToBRLCurrency(tooltipItem.yLabel, false);
                    }
                }
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'right'
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions('m', 'stackedbars', false, 'values')
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    autoSkip: false
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels, false);
                        }
                    }
                }]
            }
        }
    });
}


function inicializa_grafico_rendas_totais_et_genero(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }
    var labels = [gettext('Mercantil e Autoconsumo'), gettext('Doméstico e de Cuidados'), gettext('Participação Social'), gettext('Pluriatividade')];

    var total_m = [opcoesGraficoLume.totais.reparticao_mercantil_m, opcoesGraficoLume.totais.reparticao_domestico_cuidados_m, opcoesGraficoLume.totais.reparticao_participacao_social_m, opcoesGraficoLume.totais.reparticao_pluriatividade_m];
    var total_f = [opcoesGraficoLume.totais.reparticao_mercantil_f, opcoesGraficoLume.totais.reparticao_domestico_cuidados_f, opcoesGraficoLume.totais.reparticao_participacao_social_f, opcoesGraficoLume.totais.reparticao_pluriatividade_f];


    var barChartData = {
        labels: labels,
        datasets: [{
            label: gettext('Total Homens'),
            backgroundColor: window.chartColors.homens,
            data: total_m
        }, {
            label: gettext('Total Mulheres'),
            backgroundColor: window.chartColors.mulheres,
            data: total_f
        }]
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? subtitulo : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';

                        if (label) {
                            label += ': ';
                        }
                        return label + valueToBRLCurrency(tooltipItem.yLabel);
                    }
                }
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'right'
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions('m', 'stackedbars', true, 'values')
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    autoSkip: false
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels);
                        }
                    }
                }]
            }
        }
    });

}

function inicializa_grafico_utc_genero_et(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }
    var labels = [gettext('Total Mulheres'), gettext('Total Homens')];

    var mercantil = [opcoesGraficoLume.totais.utc_mercantil_f, opcoesGraficoLume.totais.utc_mercantil_m];
    var domestico_cuidados = [opcoesGraficoLume.totais.utc_domestico_cuidados_f, opcoesGraficoLume.totais.utc_domestico_cuidados_m];
    var participacao_social = [opcoesGraficoLume.totais.utc_participacao_social_f, opcoesGraficoLume.totais.utc_participacao_social_m];
    var pluriatividade = [opcoesGraficoLume.totais.utc_pluriatividade_f, opcoesGraficoLume.totais.utc_pluriatividade_m];


    var barChartData = {
        labels: labels,
        datasets: [{
            label: gettext('Mercantil e Autoconsumo'),
            backgroundColor: window.chartColors.mercantil,
            data: mercantil
        }, {
            label: gettext('Doméstico e de Cuidados'),
            backgroundColor: window.chartColors.domestico_cuidados,
            data: domestico_cuidados
        }, {
            label: gettext('Participação Social'),
            backgroundColor: window.chartColors.participacao_social,
            data: participacao_social
        }, {
            label: gettext('Pluriatividade'),
            backgroundColor: window.chartColors.pluriatividade,
            data: pluriatividade
        }]
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? subtitulo : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';

                        if (label) {
                            label += ': ';
                        }
                        return label + valueToBRLCurrency(tooltipItem.yLabel, false) + ' ' + gettext('UTCs');
                    }
                }
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'right'
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions('m', 'stackedbars', false, 'values')
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    autoSkip: false
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels, false);
                        }
                    }
                }]
            }
        }
    });

}


function inicializa_grafico_dias_genero_et(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }
    var labels = [gettext('Mulheres'), gettext('Homens'), gettext('Jovens Mulheres'), gettext('Jovens Homens')];

    var mercantil = [opcoesGraficoLume.totais.dias_mercantil_af, opcoesGraficoLume.totais.dias_mercantil_am, opcoesGraficoLume.totais.dias_mercantil_jf, opcoesGraficoLume.totais.dias_mercantil_jm];
    var domestico_cuidados = [opcoesGraficoLume.totais.dias_domestico_cuidados_af, opcoesGraficoLume.totais.dias_domestico_cuidados_am, opcoesGraficoLume.totais.dias_domestico_cuidados_jf, opcoesGraficoLume.totais.dias_domestico_cuidados_jm];
    var participacao_social = [opcoesGraficoLume.totais.dias_participacao_social_af, opcoesGraficoLume.totais.dias_participacao_social_am, opcoesGraficoLume.totais.dias_participacao_social_jf, opcoesGraficoLume.totais.dias_participacao_social_jm];
    var pluriatividade = [opcoesGraficoLume.totais.dias_pluriatividade_af, opcoesGraficoLume.totais.dias_pluriatividade_am, opcoesGraficoLume.totais.dias_pluriatividade_jf, opcoesGraficoLume.totais.dias_pluriatividade_jm];


    var barChartData = {
        labels: labels,
        datasets: [{
            label: gettext('Mercantil e Autoconsumo'),
            backgroundColor: window.chartColors.mercantil,
            data: mercantil
        }, {
            label: gettext('Doméstico e de Cuidados'),
            backgroundColor: window.chartColors.domestico_cuidados,
            data: domestico_cuidados
        }, {
            label: gettext('Participação Social'),
            backgroundColor: window.chartColors.participacao_social,
            data: participacao_social
        }, {
            label: gettext('Pluriatividade'),
            backgroundColor: window.chartColors.pluriatividade,
            data: pluriatividade
        }]
    };

    var filterTooltips = function (item, chart_data) {
        return item.yLabel != 0 && !isNaN(item.yLabel);
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? subtitulo : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                filter: filterTooltips,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';

                        if (label) {
                            label += ': ';
                        }
                        return label + valueToBRLCurrency(tooltipItem.yLabel, false) + ' ' + gettext('dias');
                    }
                }
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'right'
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions('m', 'stackedbars', false, 'values')
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    autoSkip: false,
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels, false);
                        }
                    }
                }]
            }
        }
    });

}

function inicializa_grafico_va_autoconsumo_por_utf(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }
    var labels = [gettext('VA Autoconsumo Mulheres / UTF'), gettext('VA Autoconsumo Homens / UTF')];

    var colors = window.chartColorsOnly;

    var datasets = [];

    for (var i = 0; i < opcoesGraficoLume.totais.totais_subsistemas.length; i++) {
        datasets.push({
            label: opcoesGraficoLume.totais.totais_subsistemas[i].nome,
            backgroundColor: colors[i % colors.length],
            data: [opcoesGraficoLume.totais.totais_subsistemas[i].va_autoconsumo_por_utf_f, opcoesGraficoLume.totais.totais_subsistemas[i].va_autoconsumo_por_utf_m]
        })
    }


    var barChartData = {
        labels: labels,
        datasets: datasets
    };

    var filterTooltips = function (item, chart_data) {
        return item.yLabel != 0 && !isNaN(item.yLabel);
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? subtitulo : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                filter: filterTooltips,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';

                        if (label) {
                            label += ': ';
                        }
                        return label + valueToBRLCurrency(tooltipItem.yLabel);
                    }
                }
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'right'
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions()
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    autoSkip: false,
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels);
                        }
                    }
                }]
            }
        }
    });

}

function inicializa_grafico_va_mercantil_por_utf(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }
    var labels = [gettext('VA Mercantil Mulheres / UTF'), gettext('VA Mercantil Homens / UTF')];

    var colors = window.chartColorsOnly;

    var datasets = [];

    for (var i = 0; i < opcoesGraficoLume.totais.totais_subsistemas.length; i++) {
        datasets.push({
            label: opcoesGraficoLume.totais.totais_subsistemas[i].nome,
            backgroundColor: colors[i % colors.length],
            data: [opcoesGraficoLume.totais.totais_subsistemas[i].va_mercantil_por_utf_f, opcoesGraficoLume.totais.totais_subsistemas[i].va_mercantil_por_utf_m]
        })
    }


    var barChartData = {
        labels: labels,
        datasets: datasets
    };

    var filterTooltips = function (item, chart_data) {
        return item.yLabel != 0 && !isNaN(item.yLabel);
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? subtitulo : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                filter: filterTooltips,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';

                        if (label) {
                            label += ': ';
                        }
                        return label + valueToBRLCurrency(tooltipItem.yLabel);
                    }
                }
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'right'
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions()
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    autoSkip: false,
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels);
                        }
                    }
                }]
            }
        }
    });
}


function inicializa_grafico_va_aem_por_utf(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }
    var labels = [gettext('VA Autoconsumo Mulheres / UTF'), gettext('VA Autoconsumo Homens / UTF'), gettext('VA Mercantil Mulheres / UTF'), gettext('VA Mercantil Homens / UTF')];

    var colors = window.chartColorsOnly;

    var datasets = [];

    for (var i = 0; i < opcoesGraficoLume.totais.totais_subsistemas.length; i++) {
        datasets.push({
            label: opcoesGraficoLume.totais.totais_subsistemas[i].nome,
            backgroundColor: colors[i % colors.length],
            data: [opcoesGraficoLume.totais.totais_subsistemas[i].va_autoconsumo_por_utf_f, opcoesGraficoLume.totais.totais_subsistemas[i].va_autoconsumo_por_utf_m, opcoesGraficoLume.totais.totais_subsistemas[i].va_mercantil_por_utf_f, opcoesGraficoLume.totais.totais_subsistemas[i].va_mercantil_por_utf_m]
        })
    }

    var barChartData = {
        labels: labels,
        datasets: datasets
    };

    var filterTooltips = function (item, chart_data) {
        return item.yLabel != 0 && !isNaN(item.yLabel);
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? subtitulo : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                filter: filterTooltips,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';

                        if (label) {
                            label += ': ';
                        }
                        return label + valueToBRLCurrency(tooltipItem.yLabel);
                    }
                }
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'right'
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions()
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    ticks: {
                        autoSkip: false
                    }
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels);
                        }
                    }
                }]
            }
        }
    });

}

// GRÁFICOS RECIPROCIDADE

function inicializa_grafico_reciprocidade_ecologica(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais_por_produto) {
        return;
    }
    var labels = [gettext('Estoque período anterior'), gettext('Produzido'), gettext('Consumidos'), gettext('Estoque final período')];

    //copia array p/ novo
    var pp2 = opcoesGraficoLume.totais_por_produto.filter(function (obj) {
        return parseFloat(obj.estoqueinsumos_inicio) >= 0.01 || parseFloat(obj.estoqueinsumos_final) >= 0.01 || parseFloat(obj.pp) >= 0.01 || parseFloat(obj.pb_insumos_produzidos) >= 0.01;
    });

    pp2.sort(function (a, b) {
        vala = parseFloat(a.pp);
        valb = parseFloat(b.pp);
        return vala > valb ? -1 : vala < valb ? 1 : 0;
    });


    var datasets = [];

    for (var i = 0; i < pp2.length; i++) {
        datasets.push({
            label: pp2[i].nome,
            backgroundColor: opcoesGraficoLume.coresProdutos[pp2[i].nome],
            data: [pp2[i].estoqueinsumos_inicio, pp2[i].pb_insumos_produzidos, pp2[i].pp, pp2[i].estoqueinsumos_final]
        })
    }

    var barChartData = {
        labels: labels,
        datasets: datasets
    };

    var filterTooltips = function (item, chart_data) {
        return item.yLabel != 0 && !isNaN(item.yLabel);
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? [subtitulo, '(' + pp2.length.toString() + ')'] : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            tooltips: {
                mode: 'index',
                enabled: false,
                custom: function(tooltipModel) {
                    makeCustomTooltipStackedBars(tooltipModel, this._chart);
                }
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'bottom'
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions()
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    autoSkip: false,
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels);
                        }
                    }
                }]
            }
        }
    });
}

function inicializa_grafico_reciprocidade_social(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais_por_produto) {
        return;
    }
    var labels = [gettext('Entrada de serviços e produtos'), gettext('Doações')];

    //copia array p/ novo
    var pp2 = opcoesGraficoLume.totais_por_produto.filter(function (obj) {
        return parseFloat(obj.reciprocidade) >= 0.01 || parseFloat(obj.pb_doacoes_trocas) >= 0.01;
    });

    pp2.sort(function (a, b) {
        vala = parseFloat(a.reciprocidade);
        valb = parseFloat(b.reciprocidade);
        return vala > valb ? -1 : vala < valb ? 1 : 0;
    });

    var datasets = [];

    for (var i = 0; i < pp2.length; i++) {
        datasets.push({
            label: pp2[i].nome,
            backgroundColor: opcoesGraficoLume.coresProdutos[pp2[i].nome],
            data: [pp2[i].reciprocidade, pp2[i].pb_doacoes_trocas]
        })
    }

    var barChartData = {
        labels: labels,
        datasets: datasets
    };

    var filterTooltips = function (item, chart_data) {
        return item.yLabel != 0 && !isNaN(item.yLabel);
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: true,
                text: opcoesGraficoLume.comparacao ? [subtitulo, '(' + pp2.length.toString() + ')'] : [opcoesGraficoLume.titulo, '(' + subtitulo + ')']
            },
            tooltips: {
                mode: 'index',
                enabled: false,
                custom: function(tooltipModel) {
                    makeCustomTooltipStackedBars(tooltipModel, this._chart);
                }
            },
            legend: {
                display: opcoesGraficoLume.comparacao ? false : true,
                position: 'bottom'
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            plugins: {
                datalabels: opcoesGraficoLume.comparacao ? {display:false} : chartJsDataLabelsOptions()
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    autoSkip: false,
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels);
                        }
                    }
                }]
            }
        }
    });
}

/********************************
 * GRÁFICOS DO DIAGRAMA SINTESE *
 ********************************/

var sinteseEixoYMax = 0;

function inicializa_grafico_sintese_tamanhos(maxRadius, cpTotal, pbInsumosProduzidosTotal, reciprocidadeTotal, pbVendaTotal, pbAutoconsumoTotal, pbDoacoesTrocasTotal) {
    sinteseEixoYMax = Math.max(cpTotal, pbInsumosProduzidosTotal, reciprocidadeTotal, pbVendaTotal, pbAutoconsumoTotal, pbDoacoesTrocasTotal);
}

function inicializa_grafico_sintese_cp(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }
    var labels = [''];
    var colors = window.chartColorsOnly;

    var datasets = [];

    for (var i = 0; i < opcoesGraficoLume.totais.totais_subsistemas.length; i++) {
        datasets.push({
            label: opcoesGraficoLume.totais.totais_subsistemas[i].nome,
            backgroundColor: colors[i % colors.length],
            data: [opcoesGraficoLume.totais.totais_subsistemas[i].cp]
        })
    }

    datasets.push({
        label: gettext('Custos sistêmicos'),
        backgroundColor: colors[opcoesGraficoLume.totais.totais_subsistemas.length % colors.length],
        data: [parseFloat(opcoesGraficoLume.totais.pt)]
    });

    var barChartData = {
        labels: labels,
        datasets: datasets
    };

    var filterTooltips = function (item, chart_data) {
        return item.yLabel != 0 && !isNaN(item.yLabel);
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {display: false},
            legend: {display: false},
            plugins: {
                datalabels: {'display': false}
            },
            legendCallback: function (chart) {
                return draw_legend(chart, opcoesGraficoLume.legend_id);
            },
            maintainAspectRatio: false,
            tooltips: {
                mode: 'index',
                enabled: false,
                custom: function(tooltipModel) {
                    makeCustomTooltipStackedBars(tooltipModel, this._chart);
                }
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    autoSkip: false,
                    display: false
                }],
                yAxes: [{
                    stacked: true,
                    display: false,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels);
                        },
                        max: sinteseEixoYMax
                    }
                }]
            }
        }
    });
}

function inicializa_grafico_sintese_pb_insumos_produzidos(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }
    var labels = [''];
    var colors = window.chartColorsOnly;

    var datasets = [];

    for (var i = 0; i < opcoesGraficoLume.totais.totais_subsistemas.length; i++) {
        datasets.push({
            label: opcoesGraficoLume.totais.totais_subsistemas[i].nome,
            backgroundColor: colors[i % colors.length],
            data: [opcoesGraficoLume.totais.totais_subsistemas[i].pp]
        })
    }

    var barChartData = {
        labels: labels,
        datasets: datasets
    };

    var filterTooltips = function (item, chart_data) {
        return item.yLabel != 0 && !isNaN(item.yLabel);
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {display: false},
            legend: {display: false},
            plugins: {
                datalabels: {'display': false}
            },
            maintainAspectRatio: false,
            tooltips: {
                mode: 'index',
                enabled: false,
                custom: function(tooltipModel) {
                    makeCustomTooltipStackedBars(tooltipModel, this._chart);
                }
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    autoSkip: false,
                    display: false
                }],
                yAxes: [{
                    stacked: true,
                    display: false,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels);
                        },
                        max: sinteseEixoYMax
                    }
                }]
            }
        }
    });
}

function inicializa_grafico_sintese_reciprocidade(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }
    var labels = [''];
    var colors = window.chartColorsOnly;

    var datasets = [];

    for (var i = 0; i < opcoesGraficoLume.totais.totais_subsistemas.length; i++) {
        datasets.push({
            label: opcoesGraficoLume.totais.totais_subsistemas[i].nome,
            backgroundColor: colors[i % colors.length],
            data: [opcoesGraficoLume.totais.totais_subsistemas[i].reciprocidade]
        })
    }

    var barChartData = {
        labels: labels,
        datasets: datasets
    };

    var filterTooltips = function (item, chart_data) {
        return item.yLabel != 0 && !isNaN(item.yLabel);
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {display: false},
            legend: {display: false},
            plugins: {
                datalabels: {'display': false}
            },
            maintainAspectRatio: false,
            tooltips: {
                mode: 'index',
                enabled: false,
                custom: function(tooltipModel) {
                    makeCustomTooltipStackedBars(tooltipModel, this._chart);
                }
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    autoSkip: false,
                    display: false
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels);
                        },
                        max: sinteseEixoYMax
                    },
                    display: false
                }]
            }
        }
    });
}

function inicializa_grafico_sintese_pb_venda(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }
    var labels = [''];
    var colors = window.chartColorsOnly;

    var datasets = [];

    for (var i = 0; i < opcoesGraficoLume.totais.totais_subsistemas.length; i++) {
        datasets.push({
            label: opcoesGraficoLume.totais.totais_subsistemas[i].nome,
            backgroundColor: colors[i % colors.length],
            data: [opcoesGraficoLume.totais.totais_subsistemas[i].pb_venda]
        })
    }

    var barChartData = {
        labels: labels,
        datasets: datasets
    };

    var filterTooltips = function (item, chart_data) {
        return item.yLabel != 0 && !isNaN(item.yLabel);
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {display: false},
            legend: {display: false},
            plugins: {
                datalabels: {'display': false}
            },
            maintainAspectRatio: false,
            tooltips: {
                mode: 'index',
                enabled: false,
                custom: function(tooltipModel) {
                    makeCustomTooltipStackedBars(tooltipModel, this._chart);
                }
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    autoSkip: false,
                    display: false
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels);
                        },
                        max: sinteseEixoYMax
                    },
                    display: false
                }]
            }
        }
    });
}

function inicializa_grafico_sintese_pb_doacoes_trocas(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }
    var labels = [''];
    var colors = window.chartColorsOnly;

    var datasets = [];

    for (var i = 0; i < opcoesGraficoLume.totais.totais_subsistemas.length; i++) {
        datasets.push({
            label: opcoesGraficoLume.totais.totais_subsistemas[i].nome,
            backgroundColor: colors[i % colors.length],
            data: [opcoesGraficoLume.totais.totais_subsistemas[i].pb_doacoes_trocas]
        })
    }

    var barChartData = {
        labels: labels,
        datasets: datasets
    };

    var filterTooltips = function (item, chart_data) {
        return item.yLabel != 0 && !isNaN(item.yLabel);
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {display: false},
            legend: {display: false},
            plugins: {
                datalabels: {'display': false}
            },
            maintainAspectRatio: false,
            tooltips: {
                mode: 'index',
                enabled: false,
                custom: function(tooltipModel) {
                    makeCustomTooltipStackedBars(tooltipModel, this._chart);
                }
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    autoSkip: false,
                    display: false
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels);
                        },
                        max: sinteseEixoYMax
                    },
                    display: false
                }]
            }
        }
    });
}

function inicializa_grafico_sintese_pb_autoconsumo(id, subtitulo) {
    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    if (!opcoesGraficoLume.totais) {
        return;
    }
    var labels = [''];
    var colors = window.chartColorsOnly;

    var datasets = [];

    for (var i = 0; i < opcoesGraficoLume.totais.totais_subsistemas.length; i++) {
        datasets.push({
            label: opcoesGraficoLume.totais.totais_subsistemas[i].nome,
            backgroundColor: colors[i % colors.length],
            data: [opcoesGraficoLume.totais.totais_subsistemas[i].pb_autoconsumo]
        })
    }

    var barChartData = {
        labels: labels,
        datasets: datasets
    };

    var filterTooltips = function (item, chart_data) {
        return item.yLabel != 0 && !isNaN(item.yLabel);
    };

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {display: false},
            legend: {display: false},
            plugins: {
                datalabels: {'display': false}
            },
            maintainAspectRatio: false,
            tooltips: {
                mode: 'index',
                enabled: false,
                custom: function(tooltipModel) {
                    makeCustomTooltipStackedBars(tooltipModel, this._chart);
                }
            },
            responsive: true,
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    autoSkip: false,
                    display: false
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels);
                        },
                        max: sinteseEixoYMax
                    },
                    display: false
                }]
            }
        }
    });
}

//------------------------------------------------------------------------------//
// Gráficos de análises agregadas

function grafico_agregada_scatter(params) {
    const id = params.id;
    const opcoesGraficoLume = pegaOpcoesGraficoLume(id);
    const titulo = [...opcoesGraficoLume.titulo.split(': '), ...params.titulo];

    var dataCiclos = [];
    var dataSimulacoes = [];
    var dataMedia = [];

    let todos = {
        x: 0,
        y: 0,
        n: 0
    }
    let todosSimulacao = {
        x: 0,
        y: 0,
        n: 0
    }

    const color = Chart.helpers.color;
    let ciclosLabels = [];
    let simulacoesLabels = [];
    let datasetsLinhas = [];
    let newDatasetLinha = {
        label: gettext('linha'),
        data: [],
        type: 'line',
        fill: false,
        showLine: true,
        pointRadius: 0,
        borderWidth: 2,
        lineTension: 0,
        pointHoverRadius: 0,
        borderColor: '#000000',
        borderDash: [2, 2]
    };
    let mediaLabel = null;
    let simulacoesMediaLabel = null;
    const varX = params.varX.includes('/')
        ? params.varX.split('/')
        : params.varX;
    const varY = params.varY.includes('/')
        ? params.varY.split('/')
        : params.varY;

    for (let car of totais_agroecossistemas) {
        if (car.totais_economica) {
            let ciclo_x = (params.varX.includes('/'))
                ? parseFloat(car.totais_economica[varX[0]])/parseFloat(car.totais_economica[varX[1]])
                : parseFloat(car.totais_economica[varX]);
            let ciclo_y = (params.varY.includes('/'))
                ? parseFloat(car.totais_economica[varY[0]])/parseFloat(car.totais_economica[varY[1]])
                : parseFloat(car.totais_economica[varY]);

            if (ciclo_x == null || ciclo_y==null || isNaN(ciclo_x) || isNaN(ciclo_y)) {
                continue;
            }

            dataCiclos.push({
                nome: car.agroecossistema.nome,
                x: ciclo_x,
                y: ciclo_y
            })
            todos.n ++;
            todos.x += ciclo_x;
            todos.y += ciclo_y;

            ciclosLabels.push({
                label: car.agroecossistema.nome,
                afterLabel: [
                    params.labelX + ": " + valueToBRLCurrency(ciclo_x, params.typeX === 'currency'),
                    params.labelY + ": " + valueToBRLCurrency(ciclo_y, params.typeY === 'currency')
                ]
            });

            if (car.totais_simulacao) {
                let sim_x = (params.varX.includes('/'))
                    ? parseFloat(car.totais_simulacao[varX[0]])/parseFloat(car.totais_simulacao[varX[1]])
                    : parseFloat(car.totais_simulacao[varX]);
                let sim_y = (params.varY.includes('/'))
                    ? parseFloat(car.totais_simulacao[varY[0]])/parseFloat(car.totais_simulacao[varY[1]])
                    : parseFloat(car.totais_simulacao[varY]);
                dataSimulacoes.push({
                    nome: car.agroecossistema.nome,
                    x: sim_x,
                    y: sim_y
                })
                todosSimulacao.n ++;
                todosSimulacao.x += sim_x;
                todosSimulacao.y += sim_y;

                simulacoesLabels.push({
                    label: car.agroecossistema.nome + " - " + gettext("SIMULAÇÃO"),
                    afterLabel: [
                        params.labelX + ": " + valueToBRLCurrency(sim_x, params.typeX === 'currency'),
                        params.labelY + ": " + valueToBRLCurrency(sim_y, params.typeY === 'currency')
                    ]
                });

                datasetsLinhas.push({
                    ...newDatasetLinha,
                    data: [{x: ciclo_x, y: ciclo_y}, {x: sim_x, y: sim_y}]
                });
            }
        }
    }

    const media_x = (todos.n > 0) ? todos.x / todos.n : 0;
    const media_y = (todos.n > 0) ? todos.y / todos.n : 0
    mediaLabel = {
        label: gettext("MÉDIA - Ciclos"),
        afterLabel: [
            params.labelX + ": " + valueToBRLCurrency(media_x, params.typeX === 'currency'),
            params.labelY + ": " + valueToBRLCurrency(media_y, params.typeY === 'currency')
        ]
    };

    let scatterChartData = {
        datasets: [
            {
                label: gettext('Ciclos'),
                pointRadius: 4,
                pointHoverRadius: 4,
                borderColor: window.chartColors.blue,
                backgroundColor: color(window.chartColors.blue).alpha(0.6).rgbString(),
                data: dataCiclos
            },
            {
                label: gettext('Ciclos (média)'),
                borderColor: window.chartColors.blue,
                borderWidth: 4,
                backgroundColor: color(window.chartColors.black).alpha(0.6).rgbString(),
                pointRadius: 8,
                pointHoverRadius: 8,
                data: [{x: media_x, y: media_y}]
            }
        ]
    };

    if (dataSimulacoes.length) {
        const mediaSimulacao_x = (todosSimulacao.n > 0) ? todosSimulacao.x / todosSimulacao.n : 0;
        const mediaSimulacao_y = (todosSimulacao.n > 0) ? todosSimulacao.y / todosSimulacao.n : 0;
        scatterChartData.datasets.push({
            label: gettext('Simulações'),
            pointRadius: 4,
            pointHoverRadius: 4,
            borderColor: window.chartColors.red,
            backgroundColor: color(window.chartColors.red).alpha(0.6).rgbString(),
            data: dataSimulacoes
        });
        scatterChartData.datasets.push({
            label: gettext('Simulações (média)'),
            borderColor: window.chartColors.red,
            borderWidth: 4,
            backgroundColor: color(window.chartColors.black).alpha(0.6).rgbString(),
            pointRadius: 8,
            pointHoverRadius: 8,
            data: [{x: mediaSimulacao_x, y: mediaSimulacao_y}]
        });
        datasetsLinhas.push({
            ...newDatasetLinha,
            data: [{x: media_x, y: media_y}, {x: mediaSimulacao_x, y: mediaSimulacao_y}]
        });

        scatterChartData.datasets = [...scatterChartData.datasets, ...datasetsLinhas];

        simulacoesMediaLabel = {
            label: gettext("MÉDIA - Simulações"),
            afterLabel: [
                params.labelX + ": " + valueToBRLCurrency(mediaSimulacao_x, params.typeX === 'currency'),
                params.labelY + ": " + valueToBRLCurrency(mediaSimulacao_y, params.typeY === 'currency')
            ]
        };
    }

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = Chart.Scatter(ctx, {
        data: scatterChartData,
        options: {
            formatter: null,
            title: {
                display: true,
                fontSize: 14,
                text: titulo
            },
            tooltips: {
                filter: function (tooltipItem, data) {
                    if (tooltipItem.datasetIndex > 3) {
                        return false;
                    } else {
                        return true;
                    }
                },
                callbacks: {
                    label: function(tooltipItem, data) {
                        let ret = false;
                        switch (tooltipItem.datasetIndex) {
                            case 0:
                                ret = ciclosLabels[tooltipItem.index].label;
                                break;
                            case 1:
                                ret = mediaLabel.label;
                                break;
                            case 2:
                                ret = simulacoesLabels[tooltipItem.index].label;
                                break;
                            case 3:
                                ret = simulacoesMediaLabel.label;
                                break;
                        }
                        return ret;
                    },
                    afterLabel: function(tooltipItem, data) {
                        let ret = false;
                        switch (tooltipItem.datasetIndex) {
                            case 0:
                                ret = ciclosLabels[tooltipItem.index].afterLabel;
                                break;
                            case 1:
                                ret = mediaLabel.afterLabel;
                                break;
                            case 2:
                                ret = simulacoesLabels[tooltipItem.index].afterLabel;
                                break;
                            case 3:
                                ret = simulacoesMediaLabel.afterLabel;
                                break;
                        }
                        return ret;
                    }
                }
            },
            legend: {
                display: (dataSimulacoes.length) ? true : false,
                position: 'bottom',
                labels: {
                    filter: function(item, chart) {
                        return !item.text.includes('linha');
                    }
                },
                onClick: function(e, legendItem) {
                    const index = legendItem.datasetIndex;
                    let ci = this.chart;
                    if (index == 1) {
                        ci.getDatasetMeta(scatterChartData.datasets.length - 1).hidden = ci.getDatasetMeta(scatterChartData.datasets.length - 1).hidden === null
                            ? !ci.data.datasets[index].hidden
                            : null;
                    } else if (index == 3) {
                        ci.getDatasetMeta(scatterChartData.datasets.length - 1).hidden = ci.getDatasetMeta(scatterChartData.datasets.length - 1).hidden === null
                            ? !ci.data.datasets[index].hidden
                            : null;
                    } else {
                        let mediaIndex = (index == 0) ? 1 : 3;
                        ci.getDatasetMeta(mediaIndex).hidden = ci.getDatasetMeta(mediaIndex).hidden === null
                            ? !ci.data.datasets[index].hidden
                            : null;

                        // Retirada das linhas:
                        for (let i = 4; i < scatterChartData.datasets.length; i++) {
                            ci.getDatasetMeta(i).hidden = ci.getDatasetMeta(i).hidden === null
                                ? !ci.data.datasets[index].hidden
                                : null;
                        }
                    }
                    ci.getDatasetMeta(index).hidden = (ci.getDatasetMeta(index).hidden === null)
                        ? !ci.data.datasets[index].hidden
                        : null;
                    ci.update();
                }
            },
            plugins: {
                datalabels: chartJsDataLabelsOptions('sm', 'scatterSemLabel')
            },
            scales: {
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: params.labelAxisX
                    },
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels, params.typeX === 'currency');
                        }
                    }

                }],
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: params.labelAxisY
                    },
                    ticks: {
                        callback: function (label, index, labels) {
                            return customYaxesTicks(label, index, labels, params.typeY === 'currency');
                        }
                    }
                }]
            },
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            }
        }
    });
}

function inicializa_grafico_agregada_mercantilizacao_e_rentabilidade(id, subtitulo) {
    grafico_agregada_scatter({
        id: id,
        labelX: gettext('Rentabilidade Monetária'),
        labelY: gettext('Índice de Mercantilização'),
        varX: 'total_rentabilidade_monetaria_bruta',
        varY: 'total_mercantilizacao',
        typeX: 'indice',
        typeY: 'indice',
        labelAxisX: gettext('Rentabilidade Monetária'),
        labelAxisY: gettext('Índice de Mercantilização'),
        titulo: [...subtitulo]
    });
}

function inicializa_grafico_agregada_autonomia_intensidade(id, subtitulo) {
    grafico_agregada_scatter({
        id: id,
        labelX: gettext('VA/HT'),
        labelY: gettext('VA/RB'),
        varX: 'total_va/horas_mercantil_total',
        varY: 'total_va/total_rb',
        typeX: 'currency',
        typeY: 'indice',
        labelAxisX: gettext('VA/HT'),
        labelAxisY: gettext('VA/RB'),
        titulo: [...subtitulo]
    });
}

function inicializa_grafico_agregada_autonomia_RA_intensidade(id, subtitulo) {
    grafico_agregada_scatter({
        id: id,
        labelX: gettext('RA/HT'),
        labelY: gettext('RA/RB'),
        varX: 'total_ra/horas_mercantil_total',
        varY: 'total_ra/total_rb',
        typeX: 'currency',
        typeY: 'indice',
        labelAxisX: gettext('RA/HT'),
        labelAxisY: gettext('RA/RB'),
        titulo: [...subtitulo]
    });
}

function inicializa_grafico_agregada_ra_rna(id, subtitulo) {
    grafico_agregada_scatter({
        id: id,
        labelX: gettext('RNA'),
        labelY: gettext('RA'),
        varX: 'rna_valor_total',
        varY: 'total_ra',
        typeX: 'currency',
        typeY: 'currency',
        labelAxisX: gettext('Renda Não Agrícola (RNA)'),
        labelAxisY: gettext('Renda Agrícola (RA)'),
        titulo: [...subtitulo]
    });
}

function inicializa_grafico_agregada_produtividade_terra_va(id, subtitulo) {
    grafico_agregada_scatter({
        id: id,
        labelX: gettext('Área (ha)'),
        labelY: gettext('VA/ha'),
        varX: 'area',
        varY: 'total_produtividade_terra',
        typeX: 'indice',
        typeY: 'currency',
        labelAxisX: gettext('Área (ha)'),
        labelAxisY: gettext('VA/ha'),
        titulo: [...subtitulo]
    });
}

function inicializa_grafico_agregada_produtividade_terra_ra(id, subtitulo) {
    grafico_agregada_scatter({
        id: id,
        labelX: gettext('Área (ha)'),
        labelY: gettext('RA/ha'),
        varX: 'area',
        varY: 'total_ra_area',
        typeX: 'indice',
        typeY: 'currency',
        labelAxisX: gettext('Área (ha)'),
        labelAxisY: gettext('RA/ha'),
        titulo: [...subtitulo]
    });
}

function grafico_agregada_poleiro(params) {
    const {id, subtitulo} = params;
    let vars = params.vars;

    var opcoesGraficoLume = pegaOpcoesGraficoLume(id);

    const color = Chart.helpers.color;
    let axisLabelsEsquerda = [];
    let axisLabelsDireita = [];
    let datasets = [];
    let mediaDatasetData = [];
    let media = {};
    let pointLabels = {};

    if (!evento_gatilho) {
        params.simulacao = false;
    }

    if (params.simulacao) {
        let newVars = [];
        for (var v of vars) {
            newVars.push({
                ...v,
                labelA: v.labelA + ' (' + gettext('Simulação') + ')',
                labelB: v.labelB + ' (' + gettext('Simulação') + ')',
                simulacao: true
            });
            newVars.push(v);
        }
        vars = newVars;
    }

    for (var v of vars) {
        const {labelA, labelB, varA, varB} = v;
        media[labelA] = {totalA: 0, totalB: 0, totalA_perc: 0, n: 0};
        pointLabels[labelA] = [];
        axisLabelsEsquerda.push(labelA);
        axisLabelsDireita.push(labelB);

        for (let i = 0; i < totais_agroecossistemas.length; i++) {
            const car = totais_agroecossistemas[i];
            const source = (v.simulacao) ? 'totais_simulacao' : 'totais_economica';
            if (
                (v.simulacao && car.totais_simulacao && car.totais_economica) ||
                (!v.simulacao && car.totais_economica)
            ) {
                let a = 0;
                let b = 0;
                let valor_x;
                let valor_y;
                let newPointLabels = [];
                const corAgroecossistema = window.coresProdutos[i % window.coresProdutos.length];

                if (datasets.length <= i) {
                    datasets.push({
                        yAxisID: 'esquerda',
                        label: car.agroecossistema.nome,
                        data: [],
                        pointBackgroundColor: color(corAgroecossistema).alpha(0.6).rgbString(),
                        pointBorderColor: corAgroecossistema,
                        borderColor: corAgroecossistema,
                        backgroundColor: corAgroecossistema,
                        pointBorderWidth: 1,
                        pointRadius: 4,
                        pointHoverRadius: 4,
                        borderWidth: 1,
                        showLine: false,
                        fill: false
                    });
                }

                a = parseFloat(car[source][varA]);
                b = parseFloat(car[source][varB]);
                valor_x =  (b < 0)
                    ? 0
                    : (b / (b + a)) * 100;
                valor_y = labelA;

                datasets[i].data.push({x: valor_x, y: valor_y});

                media[labelA].n++;
                media[labelA].totalA += a;
                media[labelA].totalB += b;
                media[labelA].totalA_perc += valor_x;

                pointLabels[labelA].push([
                    labelA + ': ' + valueToBRLCurrency(a, true) + ' (' + valueToBRLCurrency(100 - valor_x, false, 2) + '%)',
                    labelB + ': ' + valueToBRLCurrency(b, true) + ' (' + valueToBRLCurrency(valor_x, false, 2) + '%)'
                ]);
            }
        }

        pointLabels[labelA].push([
            labelA + ': ' + valueToBRLCurrency((media[labelA].n == 0) ? 0 : media[labelA].totalA / media[labelA].n, true) + ' (' + valueToBRLCurrency((media[labelA].n == 0) ? 0 : (100 - media[labelA].totalA_perc / media[labelA].n), false, 2) + '%)',
            labelB + ': ' + valueToBRLCurrency((media[labelA].n == 0) ? 0 : media[labelA].totalB / media[labelA].n, true) + ' (' + valueToBRLCurrency((media[labelA].n == 0) ? 0 : (media[labelA].totalA_perc / media[labelA].n), false, 2) + '%)'
        ]);

        mediaDatasetData.push({
            x: (media[labelA].n == 0) ? 0 : media[labelA].totalA_perc / media[labelA].n,
            y: labelA
        });
    }

    axisLabelsDireita.push('');
    axisLabelsEsquerda.push('');

    datasets.push({
        yAxisID: 'esquerda',
        label: gettext('MÉDIA'),
        data: mediaDatasetData,
        pointBackgroundColor: color('rgb(0, 0, 0)').alpha(0.6).rgbString(),
        pointBorderColor: 'rgb(0, 0, 0)',
        borderColor: 'rgb(0, 0, 0)',
        backgroundColor: 'rgb(0, 0, 0)',
        pointBorderWidth: 2,
        pointRadius: 8,
        pointHoverRadius: 8,
        borderWidth: 1,
        showLine: false,
        fill: false
    });

    const numAgroecossistemas = datasets.length;

    if (params.simulacao) {
        let datasetsLinhas = [];
        const newDatasetLinha = {
            label: gettext('linha'),
            data: [],
            type: 'line',
            fill: false,
            showLine: true,
            pointRadius: 0,
            borderWidth: 2,
            lineTension: 0,
            pointHoverRadius: 0,
            borderColor: '#000000',
            borderDash: [2, 2]
        };
        for (var i in params.vars) {
            const index = 2*i;
            const indexSimulacao = (2*i) + 1;
            for (var dataset of datasets) {
                const data = dataset.data[index];
                const dataSimulacao = dataset.data[indexSimulacao];
                datasetsLinhas.push({
                    ...newDatasetLinha,
                    data: [
                        data,
                        dataSimulacao
                    ]
                });
            }
        }
        datasets = [...datasets, ...datasetsLinhas];
    }

    var ctx = $('#' + id);
    graficosCompletos[id] = 0;
    graficos[id] = new Chart(ctx, {
        type: 'line',
        data: {
            labels: axisLabelsEsquerda,
            datasets: datasets
        },
        options: {
            title: {
                display: true,
                padding: 20,
                text: [gettext('Comparação de rendas'), subtitulo]
            },
            scales: {
                xAxes: [{
                    type: 'linear',
                    scaleLabel: {
                        display: false,
                        labelString: gettext('Porcentagem (%)')
                    },
                    ticks: {
                        callback: function (label, index, labels) {
                            return (index == 1) ? valueToBRLCurrency(label, false, 0) + '%' : '';
                        },
                        stepSize: 50,
                        min: 0,
                        max: 100
                    }
                }],
                yAxes: [
                    {
                        id: 'esquerda',
                        type: 'category',
                        position: 'left',
                        scaleLabel: {
                            display: false,
                        },
                        ticks: {
                            callback: function (label, index, labels) {
                                return axisLabelsEsquerda[index];
                            }
                        }
                    },
                    {
                        id: 'direita',
                        type: 'category',
                        position: 'right',
                        scaleLabel: {
                            display: false,
                        },
                        ticks: {
                            callback: function (label, index, labels) {
                                return axisLabelsDireita[index];
                            }
                        }
                    }
                ]
            },
            tooltips: {
                filter: function (tooltipItem, data) {
                    if (tooltipItem.datasetIndex >= numAgroecossistemas) {
                        return false;
                    } else {
                        return true;
                    }
                },
                callbacks: {
                    title: function(tooltipItem, data) {
                        return data.datasets[tooltipItem[0].datasetIndex].label;
                    },
                    label: function(tooltipItem, data) {
                        return pointLabels[tooltipItem.yLabel][tooltipItem.datasetIndex];
                    }
                }
            },
            plugins: {
                datalabels: chartJsDataLabelsOptions('sm', 'line')
            },
            showLines: true,
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    filter: function(item, chart) {
                        return !item.text.includes('linha');
                    }
                },
                onClick: function(e, legendItem) {
                    const index = legendItem.datasetIndex;
                    let ci = this.chart;
                    if (params.simulacao) {
                        if (index < numAgroecossistemas) {
                            ci.getDatasetMeta(index + numAgroecossistemas).hidden = ci.getDatasetMeta(index + numAgroecossistemas).hidden === null
                            ? !ci.data.datasets[index].hidden
                            : null;
                        }
                    }
                    ci.getDatasetMeta(index).hidden = (ci.getDatasetMeta(index).hidden === null)
                        ? !ci.data.datasets[index].hidden
                        : null;
                    ci.update();
                }
            },
            animation: {
                onComplete: function () {
                    acoes_pos_carregamento(this, id, opcoesGraficoLume.legend_id, opcoesGraficoLume.ordem);
                }
            }
        }
    });
}

function inicializa_grafico_agregada_compara_rendas(id, subtitulo) {
    grafico_agregada_poleiro({
        id: id,
        subtitulo: subtitulo,
        simulacao: false,
        vars: [
            {
                labelA: gettext('RNA'),
                labelB: gettext('RA'),
                varA: 'rna_valor_total',
                varB: 'total_ra'
            },
            {
                labelA: gettext('RPluri'),
                labelB: gettext('RA'),
                varA: 'rna_pluriatividade',
                varB: 'total_ra'
            },
            {
                labelA: gettext('RAM'),
                labelB: gettext('RANM'),
                varA: 'total_ram',
                varB: 'total_ranm'
            }
        ]
    });
}

function inicializa_grafico_agregada_compara_rendas_RNAxRA(id, subtitulo) {
    grafico_agregada_poleiro({
        id: id,
        subtitulo: subtitulo,
        vars: [
            {
                labelA: gettext('RNA'),
                labelB: gettext('RA'),
                varA: 'rna_valor_total',
                varB: 'total_ra'
            }
        ],
        simulacao: true
    });
}

function inicializa_grafico_agregada_compara_rendas_RPlurixRA(id, subtitulo) {
    grafico_agregada_poleiro({
        id: id,
        subtitulo: subtitulo,
        vars: [
            {
                labelA: gettext('RPluri'),
                labelB: gettext('RA'),
                varA: 'rna_pluriatividade',
                varB: 'total_ra'
            }
        ],
        simulacao: true
    });
}

function inicializa_grafico_agregada_compara_rendas_RAMxRANM(id, subtitulo) {
    grafico_agregada_poleiro({
        id: id,
        subtitulo: subtitulo,
        vars: [
            {
                labelA: gettext('RAM'),
                labelB: gettext('RANM'),
                varA: 'total_ram',
                varB: 'total_ranm'
            }
        ],
        simulacao: true
    });
}

//------------------------------------------------------------------------------//

$(document).ready(function () {
    // Monitorar alterações no select2 (autocomplete light)
    var targets = $('.select2');
    var targetForm = $('.select2').first().parents('form')[0];

    function select2Listener(mutations) {
        var br = false;
        for (var i = 0; i < mutations.length; i++) {
            var mutation = mutations[i];
            if (mutation.addedNodes.length) {
                br = true;
                toggleSubmitForm(targetForm);
            }
            if (br) {
                return false;
            }
        }
    }

    const observer = new MutationObserver(select2Listener);
    targets.each(function () {
        observer.observe(this, {
            characterData: true,
            characterDataOldValue: true,
            childList: true,
            subtree: true
        });
    });

    $('.exportar_todos_graficos').hide();
    $('.exportar_todos_graficos').click(function (e) {
        var tab_id = $(this).data('tab-id');

        var zip = new JSZip();

        var folder_top = zip.folder(arquivo_pre);
        var folder = folder_top.folder(tab_id);

        $('.' + tab_id + '_tab_wrapper').find('canvas').each(function () {
            var dataStr = graficos[this.id].toBase64Image().split(',')[1];
            folder.file(this.id + '.png', dataStr, {base64: true});
        })

        zip.generateAsync({type: "blob"}).then(function (content) {
            saveAs(content, arquivo_pre + '.zip');
        });
    });
});

function customYaxesTicks(label, index, labels, prependBRL = true) {
    var prepend = (prependBRL) ? gettext('R$') + ' ' : '';
    if (label >= 1000000) {
        var fLabel = label / 1000000;
        var pos = (fLabel == 1) ? ' ' + gettext('milhão') : ' ' + gettext('milhões');
        return prepend + fLabel.toLocaleString(gettext("pt-BR")) + pos;
    } else {
        return prepend + label.toLocaleString(gettext("pt-BR"));
    }
}
