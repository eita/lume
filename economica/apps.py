from django.apps import AppConfig


class EconomicaConfig(AppConfig):
    name = 'economica'
