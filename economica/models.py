from copy import deepcopy
from datetime import date

from dateutil.relativedelta import relativedelta
from django.db import models as models
from django.db.models.base import Model
from djmoney.models.fields import MoneyField
from django.urls import reverse
from django.utils.functional import cached_property

from django.utils.translation import gettext as _
from django.utils.translation import gettext_lazy as _lazy

from taggit.managers import TaggableManager

from agroecossistema.models import CicloAnualReferencia, ComposicaoNsga, Pessoa
from main.models import LumeModel, Unidade, Produto, Servico


class Analise(LumeModel):
    data_coleta = models.DateTimeField(
        blank=True, null=True, verbose_name=_lazy("data da coleta")
    )
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("última alteração")
    )

    # Relationship Fields
    ciclo_anual_referencia = models.ForeignKey(
        CicloAnualReferencia,
        verbose_name=_lazy("ciclo anual de referência"),
        related_name="analises_economicas",
        on_delete=models.CASCADE,
    )

    # submodulos da análise econômica geral
    TIPOS_PATRIMONIO = (
        ("F", _("Capital Fundiário")),
        ("E", _("Capital Fixo - Equipamentos")),
        ("V", _("Capital Fixo - Vivo (Semoventes)")),
    )
    TIPOS_RENDA_NAO_AGRICOLA = (
        ("P", _("Pluriatividade")),
        ("T", _("Transferências de Renda")),
    )

    # submodulos dos subsistemas
    SUBSISTEMA_TIPOS_ORIGEM = (
        ("F", _("Mercado Convencional")),
        ("T", _("Mercado Territorial")),
    )
    SUBSISTEMA_TIPOS_INSUMO = (
        ("P", _("Produção Própria - Reciprocidade Ecológica")),
        ("C", _("Comprados - Consumos Intermediários")),
    )

    SUBSISTEMA_TIPOS_AQUISICAO_RECIPROCIDADE = (
        ("I", _("Insumos")),
        ("S", _("Serviços")),
    )

    ABAS = [
        {
            "title": _("Produtos e insumos gerados"),
            "title_short": _("Produções nos subsistemas"),
            "submodulo": "subsistemaproducao",
        },
        {
            "title": _("Insumos consumidos"),
            "submodulo": "subsistemainsumo",
            "sub_abas": [
                {"title": _("Comprados"), "target_id": "subsistemainsumo", "tipo": "C"},
                {
                    "title": _("Produção Própria (reciprocidade ecológica)"),
                    "target_id": "subsistemainsumo",
                    "tipo": "P",
                },
                {
                    "title": _("Recebidos (reciprocidade social)"),
                    "target_id": "subsistemaaquisicaoreciprocidade",
                    "tipo": "I",
                },
            ],
        },
        {
            "title": _("Serviços de terceiros"),
            "submodulo": "subsistemapagamentoterceiros",
            "sub_abas": [
                {
                    "title": _("Pagamento de serviços"),
                    "target_id": "subsistemapagamentoterceiros",
                },
                {
                    "title": _("Serviços por reciprocidade social"),
                    "target_id": "subsistemaaquisicaoreciprocidade",
                    "tipo": "S",
                },
            ],
        },
        {"title": _("Trabalhos no subsistema"), "submodulo": "subsistematempotrabalho"},
        {
            "title": _("Tempo estimado de ocupação por esfera de trabalho (horas/ano)"),
            "title_short": _("Tempos Estimados de ocupação"),
            "submodulo": "tempotrabalho",
        },
        {"title": _("Reserva de insumos"), "submodulo": "estoqueinsumos"},
        {"title": _("Custos Sistêmicos"), "submodulo": "pagamentoterceiros"},
        {
            "title": _("Rendas Não Agrícolas"),
            "submodulo": "rendanaoagricola",
            "sub_abas_automaticas": TIPOS_RENDA_NAO_AGRICOLA,
        },
        {
            "title": _("Inventário Patrimonial"),
            "submodulo": "patrimonio",
            "sub_abas_automaticas": TIPOS_PATRIMONIO,
        },
    ]

    INDICADORES = [
        {"def": "pb", "label": "", "help": ""},
        {"def": "estoque", "label": "", "help": ""},
        {"def": "doacoes_trocas", "label": "", "help": ""},
        {"def": "autoconsumo", "label": "", "help": ""},
        {"def": "venda", "label": "", "help": ""},
        {"def": "ci", "label": "", "help": ""},
        {"def": "va", "label": "", "help": ""},
        {"def": "cift", "label": "", "help": ""},
        {"def": "vat", "label": "", "help": ""},
        {"def": "cp", "label": "", "help": ""},
        {"def": "ra", "label": "", "help": ""},
        {"def": "ram", "label": "", "help": ""},
        {"def": "pt", "label": "", "help": ""},
        {"def": "rna_pluriatividade", "label": "", "help": ""},
        {"def": "rna_transferencia", "label": "", "help": ""},
        {"def": "mercantil", "label": "", "help": ""},
        {"def": "domestico_cuidados", "label": "", "help": ""},
        {"def": "participacao_social", "label": "", "help": ""},
        {"def": "pluriatividade", "label": "", "help": ""},
        {"label": _("Recursos Produtivos Mercantis"), "def": "total_cp", "help": ""},
        {
            "label": _("Rentabilidade Monetária Bruta"),
            "def": "total_rentabilidade_monetaria_bruta",
            "help": "",
        },
        {"label": _("Produtos Vendidos"), "def": "total_pb_venda", "help": ""},
        {"label": _("Recursos Reproduzidos (insumos)"), "def": "total_pp", "help": ""},
        {
            "label": _("Índice de Mercantilização"),
            "def": "total_mercantilizacao",
            "help": "",
        },
        {
            "label": _("Produtos Consumidos (autoconsumo)"),
            "def": "total_pb_autoconsumo",
            "help": "",
        },
        {
            "label": _("Recursos Recebidos (entrada reciprocidade)"),
            "def": "total_reciprocidade",
            "help": "",
        },
        {
            "label": _("Total recursos reproduzidos"),
            "def": "total_recursos_reproduzidos",
            "help": "",
        },
        {
            "label": _("Produtos Doados (saída reciprocidade)"),
            "def": "total_pb_doacoes_trocas",
            "help": "",
        },
    ]

    class Meta:
        ordering = ("ciclo_anual_referencia__ano",)
        unique_together = (("ciclo_anual_referencia"),)

    def __unicode__(self):
        return "%s" % self.pk

    def __str__(self):
        return self.ciclo_anual_referencia.__str__()

    @cached_property
    def ano(self):
        return self.ciclo_anual_referencia.ano

    def get_absolute_url(self):
        return reverse(
            "economica:analise_detail",
            args=(
                self.ciclo_anual_referencia.agroecossistema_id,
                self.ciclo_anual_referencia.ano,
            ),
        )

    def get_update_url(self):
        return reverse(
            "economica:analise_update",
            args=(
                self.ciclo_anual_referencia.agroecossistema_id,
                self.ciclo_anual_referencia.ano,
            ),
        )

    def get_update_submodulo_url(self, submodulo, submodulo_tipo, subsistema_id=None):
        car = self.ciclo_anual_referencia

        action = "ecosim:" if car.sim_slug else "economica:"

        action += (
            submodulo + "_update"
            if submodulo_tipo == ""
            else submodulo + "_" + submodulo_tipo + "_update"
        )
        args = {
            "agroecossistema_id": car.agroecossistema_id,
            "ano_car": car.ano,
        }
        if car.sim_slug:
            args["sim_slug"] = car.sim_slug

        if subsistema_id:
            args["pk"] = subsistema_id

        return reverse(action, kwargs=args)

    def copia_profunda(self, novo_car, tipo_copia, sim_subsistemas=None):
        nova_analise = deepcopy(self)
        nova_analise.id = None
        nova_analise.ciclo_anual_referencia = novo_car
        nova_analise.save()

        composicoes_do_novo_car = novo_car.composicoes_nsga.all()

        # Subsistemas
        if sim_subsistemas is not None:  # simulação
            subs = sim_subsistemas
        else:
            subs = self.subsistemas.all()

        for sub in subs:
            sub.copia_profunda(nova_analise, composicoes_do_novo_car, tipo_copia)

        # Patrimônio
        patrs = self.patrimonios.all()
        for patr in patrs:
            patr.copia_profunda(nova_analise, tipo_copia)

        # Estoque Insumos
        eis = self.estoques_insumos.all()
        for ei in eis:
            ei.copia_profunda(nova_analise, tipo_copia)

        if tipo_copia == CicloAnualReferencia.COPIA_AE_COMPLETA:
            # Renda Não Agrícola
            rnas = self.rendas_nao_agricolas.all()
            for rna in rnas:
                rna.copia_profunda(nova_analise)

            # Pagamento Terceiros
            pts = self.pagamentos_terceiros.all()
            for pt in pts:
                pt.copia_profunda(nova_analise)

            # Tempo Trabalho
            tts = self.tempos_de_trabalho.all()
            for tt in tts:
                tt.copia_profunda(nova_analise, composicoes_do_novo_car)

    def get_submodulo_titulo(submodulo, submodulo_tipo=""):
        targets = Analise.ABAS

        for target in targets:
            if target["submodulo"] == submodulo:
                if "sub_abas_automaticas" in target:
                    for sub_aba in target["sub_abas_automaticas"]:
                        if sub_aba[0] == submodulo_tipo:
                            return target["title"] + " (" + sub_aba[1] + ")"

                elif "sub_abas" in target:
                    for sub_aba in target["sub_abas"]:
                        if (not "tipo" in sub_aba and not submodulo_tipo) or (
                            "tipo" in sub_aba and sub_aba["tipo"] == submodulo_tipo
                        ):
                            return target["title"] + " (" + sub_aba["title"] + ")"
                else:
                    return target["title"]

        return ""

    def get_totais_tempostrabalho(self):
        composicoes_nsga = (
            self.ciclo_anual_referencia.composicoes_nsga.filter(
                pessoa__data_nascimento__lte=date.today() - relativedelta(years=14)
            )
            .select_related("pessoa")
            .order_by("-responsavel", "pessoa__data_nascimento")
        )
        totais_tempostrabalho_totais_por_subsistema = []
        totais_tempostrabalho_total = 0
        totais_tempostrabalho_mulheres = 0
        totais_tempostrabalho_homens = 0
        totais_tempostrabalho_totais_por_pessoa = {}
        for subsistema in self.subsistemas.all():
            subsistema_totais_tempostrabalho = subsistema.get_totais_tempostrabalho(
                composicoes_nsga
            )
            totais_tempostrabalho_totais_por_subsistema.append(
                subsistema_totais_tempostrabalho
            )
            totais_tempostrabalho_total += subsistema_totais_tempostrabalho[
                "totais_tempostrabalho_total"
            ]
            totais_tempostrabalho_homens += subsistema_totais_tempostrabalho[
                "totais_tempostrabalho_homens"
            ]
            totais_tempostrabalho_mulheres += subsistema_totais_tempostrabalho[
                "totais_tempostrabalho_mulheres"
            ]
            for pessoa in subsistema_totais_tempostrabalho[
                "totais_tempostrabalhoporpessoa"
            ]:
                if pessoa["id"] not in totais_tempostrabalho_totais_por_pessoa:
                    totais_tempostrabalho_totais_por_pessoa[pessoa["id"]] = {
                        "nome": pessoa["nome"],
                        "responsavel": pessoa["responsavel"],
                        "horas": 0,
                    }
                totais_tempostrabalho_totais_por_pessoa[pessoa["id"]][
                    "horas"
                ] += pessoa["horas"]

        return {
            "totais_tempostrabalho_total": totais_tempostrabalho_total,
            "totais_tempostrabalho_mulheres": totais_tempostrabalho_mulheres,
            "totais_tempostrabalho_homens": totais_tempostrabalho_homens,
            "totais_tempostrabalho_totais_por_pessoa": totais_tempostrabalho_totais_por_pessoa,
            "totais_tempostrabalho_totais_por_subsistema": totais_tempostrabalho_totais_por_subsistema,
        }

    def save(self, *args, **kwargs):
        # update ultima_alteracao of agroecossistema:
        self.ciclo_anual_referencia.agroecossistema.save()
        super(Analise, self).save(*args, **kwargs)


class EstoqueInsumos(LumeModel):
    valor_unitario_inicio = MoneyField(
        max_digits=10,
        decimal_places=2,
        default_currency="BRL",
        verbose_name=_lazy("valor unitário inicial"),
    )
    quantidade_inicio = models.DecimalField(
        default=0.0,
        max_digits=10,
        decimal_places=2,
        verbose_name=_lazy("quantidade inicial"),
    )
    valor_unitario_final = models.DecimalField(
        default=0.0,
        max_digits=10,
        decimal_places=2,
        verbose_name=_lazy("valor unitário final"),
    )
    quantidade_final = models.DecimalField(
        default=0.0,
        max_digits=10,
        decimal_places=2,
        verbose_name=_lazy("quantidade final"),
    )
    observacoes = models.TextField(
        blank=True, null=True, verbose_name=_lazy("observações")
    )
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("última alteração")
    )

    # Relationship Fields
    unidade = models.ForeignKey(
        Unidade,
        verbose_name=_lazy("unidade"),
        related_name="estoques_insumos",
        on_delete=models.PROTECT,
    )
    analise = models.ForeignKey(
        Analise,
        verbose_name=_lazy("análise"),
        related_name="estoques_insumos",
        on_delete=models.CASCADE,
    )
    produto = models.ForeignKey(
        Produto,
        verbose_name=_lazy("produto"),
        related_name="estoques_insumos",
        on_delete=models.PROTECT,
    )

    class Meta:
        ordering = ("produto__nome",)

    def __unicode__(self):
        return "%s" % self.pk

    def get_absolute_url(self):
        return reverse("economica:estoqueinsumos_detail", args=(self.pk,))

    def get_update_url(self):
        return reverse("economica:estoqueinsumos_update", args=(self.pk,))

    def copia_profunda(self, nova_analise, tipo_copia):
        novo_item_estoque = deepcopy(self)
        novo_item_estoque.id = None
        novo_item_estoque.analise = nova_analise
        if tipo_copia == CicloAnualReferencia.COPIA_CAR_EM_SEQUENCIA:
            novo_item_estoque.quantidade_inicio = self.quantidade_final
            novo_item_estoque.valor_unitario_inicio = self.valor_unitario_final
            novo_item_estoque.quantidade_final = 0
            novo_item_estoque.valor_unitario_final = 0
        elif tipo_copia == CicloAnualReferencia.COPIA_AE_ESTRUTURA:
            novo_item_estoque.valor_unitario_inicio = 0
            novo_item_estoque.valor_unitario_final = 0
        novo_item_estoque.save()

    def save(self, *args, **kwargs):
        # update ultima_alteracao of agroecossistema:
        self.analise.ciclo_anual_referencia.agroecossistema.save()
        super(EstoqueInsumos, self).save(*args, **kwargs)


class PagamentoTerceiros(LumeModel):
    valor_unitario = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name=_lazy("valor unitário")
    )
    quantidade = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name=_lazy("quantidade")
    )
    observacoes = models.TextField(
        blank=True, null=True, verbose_name=_lazy("observações")
    )
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("última alteração")
    )

    # Relationship Fields
    analise = models.ForeignKey(
        Analise,
        verbose_name=_lazy("análise"),
        related_name="pagamentos_terceiros",
        on_delete=models.CASCADE,
    )
    unidade = models.ForeignKey(
        Unidade,
        verbose_name=_lazy("unidade"),
        related_name="pagamentos_terceiros",
        on_delete=models.PROTECT,
    )
    servico = models.ForeignKey(
        Servico,
        verbose_name=_lazy("serviço"),
        related_name="pagamentos_terceiros",
        on_delete=models.PROTECT,
    )

    class Meta:
        ordering = ("servico__nome",)

    def __unicode__(self):
        return "%s" % self.pk

    def get_absolute_url(self):
        return reverse("economica:pagamentoterceiros_detail", args=(self.pk,))

    def get_update_url(self):
        return reverse("economica:pagamentoterceiros_update", args=(self.pk,))

    def copia_profunda(self, nova_analise):
        novo_pagamento = deepcopy(self)
        novo_pagamento.id = None
        novo_pagamento.analise = nova_analise
        novo_pagamento.save()

    def save(self, *args, **kwargs):
        # update ultima_alteracao of agroecossistema:
        self.analise.ciclo_anual_referencia.agroecossistema.save()
        super(PagamentoTerceiros, self).save(*args, **kwargs)


class Patrimonio(LumeModel):
    item = models.CharField(max_length=255, verbose_name=_lazy("item"))
    valor_unitario_inicio = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name=_lazy("valor unitário inicial")
    )
    quantidade_inicio = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name=_lazy("quantidade inicial")
    )
    valor_unitario_final = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name=_lazy("valor unitário final")
    )
    quantidade_final = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name=_lazy("quantidade final")
    )
    tipo = models.CharField(
        max_length=1, choices=Analise.TIPOS_PATRIMONIO, verbose_name=_lazy("tipo")
    )
    observacoes = models.TextField(
        blank=True, null=True, verbose_name=_lazy("observações")
    )
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("última alteração")
    )

    # Relationship Fields
    unidade = models.ForeignKey(
        Unidade,
        verbose_name=_lazy("unidade"),
        related_name="patrimonios",
        on_delete=models.PROTECT,
    )
    analise = models.ForeignKey(
        Analise,
        verbose_name=_lazy("análise"),
        related_name="patrimonios",
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ("item",)

    def __unicode__(self):
        return "%s" % self.pk

    def get_absolute_url(self):
        return reverse("economica:patrimonio_detail", args=(self.pk,))

    def get_update_url(self):
        return reverse("economica:patrimonio_update", args=(self.pk,))

    def copia_profunda(self, nova_analise, tipo_copia):
        novo_patrimonio = deepcopy(self)
        novo_patrimonio.id = None
        novo_patrimonio.analise = nova_analise
        if tipo_copia == CicloAnualReferencia.COPIA_CAR_EM_SEQUENCIA:
            novo_patrimonio.quantidade_inicio = self.quantidade_final
            novo_patrimonio.valor_unitario_inicio = self.valor_unitario_final
            novo_patrimonio.quantidade_final = 0
            novo_patrimonio.valor_unitario_final = 0
        elif tipo_copia == CicloAnualReferencia.COPIA_AE_ESTRUTURA:
            novo_patrimonio.valor_unitario_inicio = 0
            novo_patrimonio.valor_unitario_final = 0
        novo_patrimonio.save()

    def save(self, *args, **kwargs):
        # update ultima_alteracao of agroecossistema:
        self.analise.ciclo_anual_referencia.agroecossistema.save()
        super(Patrimonio, self).save(*args, **kwargs)


class RendaNaoAgricola(LumeModel):
    tipo = models.CharField(
        max_length=1,
        choices=Analise.TIPOS_RENDA_NAO_AGRICOLA,
        verbose_name=_lazy("tipo"),
    )
    item = models.CharField(max_length=60, verbose_name=_lazy("item"))
    valor_unitario = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name=_lazy("valor unitário")
    )
    quantidade = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name=_lazy("quantidade")
    )
    observacoes = models.TextField(
        blank=True, null=True, verbose_name=_lazy("observações")
    )
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("última alteração")
    )

    # Relationship Fields
    analise = models.ForeignKey(
        Analise,
        verbose_name=_lazy("análise"),
        related_name="rendas_nao_agricolas",
        on_delete=models.CASCADE,
    )
    unidade = models.ForeignKey(
        Unidade,
        verbose_name=_lazy("unidade"),
        related_name="rendas_nao_agricolas",
        on_delete=models.PROTECT,
    )

    class Meta:
        ordering = ("item",)

    def __unicode__(self):
        return "%s" % self.pk

    def get_absolute_url(self):
        return reverse("economica:rendanaoagricola_detail", args=(self.pk,))

    def get_update_url(self):
        return reverse("economica:rendanaoagricola_update", args=(self.pk,))

    def copia_profunda(self, nova_analise):
        nova_renda = deepcopy(self)
        nova_renda.id = None
        nova_renda.analise = nova_analise
        nova_renda.save()

    def save(self, *args, **kwargs):
        # update ultima_alteracao of agroecossistema:
        self.analise.ciclo_anual_referencia.agroecossistema.save()
        super(RendaNaoAgricola, self).save(*args, **kwargs)


class TempoTrabalho(Model):
    horas_domestico_cuidados = models.IntegerField(
        default=0, verbose_name=_lazy("horas para cuidados domésticos")
    )
    horas_participacao_social = models.IntegerField(
        default=0, verbose_name=_lazy("horas de participação social")
    )
    horas_pluriatividade = models.IntegerField(
        default=0, verbose_name=_lazy("horas de pluriatividade")
    )
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("última alteração")
    )

    # Relationship Fields
    analise = models.ForeignKey(
        Analise,
        verbose_name=_lazy("análise"),
        related_name="tempos_de_trabalho",
        on_delete=models.CASCADE,
    )
    composicao_nsga = models.ForeignKey(
        ComposicaoNsga,
        verbose_name=_lazy("composição nsga"),
        related_name="tempos_de_trabalho",
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ("composicao_nsga__pessoa__nome",)

    def __unicode__(self):
        return "%s" % self.pk

    def get_absolute_url(self):
        return reverse("economica:tempotrabalho_detail", args=(self.pk,))

    def get_update_url(self):
        return reverse("economica:tempotrabalho_update", args=(self.pk,))

    def copia_profunda(self, nova_analise, composicoes_do_novo_car):
        novo_tempo_de_trabalho = deepcopy(self)
        novo_tempo_de_trabalho.id = None
        novo_tempo_de_trabalho.analise = nova_analise
        novo_tempo_de_trabalho.composicao_nsga = composicoes_do_novo_car.filter(
            pessoa=self.composicao_nsga.pessoa
        ).first()
        novo_tempo_de_trabalho.save()

    def save(self, *args, **kwargs):
        # update ultima_alteracao of agroecossistema:
        self.analise.ciclo_anual_referencia.agroecossistema.save()
        super(TempoTrabalho, self).save(*args, **kwargs)


class Subsistema(LumeModel):
    area = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        blank=True,
        null=True,
        verbose_name=_lazy("área"),
    )
    nome = models.CharField(max_length=80, verbose_name=_lazy("nome"))
    observacoes = models.TextField(
        blank=True, null=True, verbose_name=_lazy("observações")
    )
    extrativismo = models.BooleanField(
        default=False, verbose_name=_lazy("extrativismo")
    )
    gerido_por_mulheres = models.BooleanField(
        default=False, verbose_name=_lazy("gerido por mulheres")
    )
    gerido_por_jovens = models.BooleanField(
        default=False, verbose_name=_lazy("gerido por jovens")
    )
    quintal = models.BooleanField(default=False, verbose_name=_lazy("quintal"))
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("última alteração")
    )

    # Relationship Fields
    analise = models.ForeignKey(
        Analise,
        verbose_name=_lazy("análise"),
        related_name="subsistemas",
        on_delete=models.CASCADE,
    )

    tags = TaggableManager(
        verbose_name=_lazy("marcadores"),
        help_text=_("Uma lista de marcadores separados por vírgula."),
        blank=True,
    )

    class Meta:
        ordering = (
            "nome",
            "extrativismo",
            "id",
        )

    def __unicode__(self):
        return "%s" % self.pk

    def __str__(self):
        return self.nome

    def get_absolute_url(self):
        car = self.analise.ciclo_anual_referencia

        if car.sim_slug is None:
            return reverse(
                "economica:subsistema_list",
                args=(
                    car.agroecossistema_id,
                    car.ano,
                ),
            )
        else:
            return reverse(
                "ecosim:subsistema_list",
                args=(
                    car.agroecossistema_id,
                    car.ano,
                    car.sim_slug,
                ),
            )

    def get_update_url(self):
        car = self.analise.ciclo_anual_referencia

        if car.sim_slug is None:
            return reverse(
                "economica:subsistema_update",
                args=(car.agroecossistema_id, car.ano, self.pk),
            )
        else:
            return reverse(
                "ecosim:subsistema_update",
                args=(car.agroecossistema_id, car.ano, car.sim_slug, self.pk),
            )

    def copia_profunda(self, nova_analise, composicoes_do_novo_car, tipo_copia):
        novo_subsistema = deepcopy(self)
        novo_subsistema.id = None
        novo_subsistema.analise = nova_analise
        novo_subsistema.save()

        if tipo_copia == CicloAnualReferencia.COPIA_AE_COMPLETA:
            # SS Pagamento Terceiros
            pts = self.pagamento_terceiros.all()
            for pt in pts:
                pt.copia_profunda(novo_subsistema)

            # SS Aquisição Reciprocidade
            ars = self.aquisicoes_reciprocidade.all()
            for ar in ars:
                ar.copia_profunda(novo_subsistema)

            # SS Insumos
            inss = self.insumos.all()
            for ins in inss:
                ins.copia_profunda(novo_subsistema)

            # SS Produção
            prods = self.producoes.all()
            for prod in prods:
                prod.copia_profunda(novo_subsistema)

            # SS Tempo Trabalho
            tts = self.subsistema_tempos_de_trabalho.all()
            for tt in tts:
                tt.copia_profunda(novo_subsistema, composicoes_do_novo_car)

    def get_totais_tempostrabalho(self, composicoes_nsga):
        subsistema_tempos_de_trabalho = self.subsistema_tempos_de_trabalho.values(
            "composicao_nsga__responsavel", "composicao_nsga__pessoa_id", "horas"
        )

        totais_tempostrabalhoporpessoa = []
        totais_tempostrabalho_total = 0
        totais_tempostrabalho_mulheres = 0
        totais_tempostrabalho_homens = 0
        for nsga in composicoes_nsga:
            horas = 0
            for horasporpessoa in subsistema_tempos_de_trabalho:
                if horasporpessoa["composicao_nsga__pessoa_id"] == nsga.pessoa.id:
                    horas = horasporpessoa["horas"]
                    totais_tempostrabalho_total += horas
                    if nsga.pessoa.sexo == "F":
                        totais_tempostrabalho_mulheres += horas
                    else:
                        totais_tempostrabalho_homens += horas
                    break

            totais_tempostrabalhoporpessoa.append(
                {
                    "id": nsga.pessoa.id,
                    "responsavel": nsga.responsavel,
                    "nome": nsga.pessoa.nome,
                    "horas": horas,
                }
            )
        return {
            "nome": self.nome,
            "totais_tempostrabalho_total": totais_tempostrabalho_total,
            "totais_tempostrabalho_mulheres": totais_tempostrabalho_mulheres,
            "totais_tempostrabalho_homens": totais_tempostrabalho_homens,
            "totais_tempostrabalhoporpessoa": totais_tempostrabalhoporpessoa,
        }

    def save(self, *args, **kwargs):
        # update ultima_alteracao of agroecossistema:
        self.analise.ciclo_anual_referencia.agroecossistema.save()
        super(Subsistema, self).save(*args, **kwargs)


class SubsistemaInsumo(LumeModel):
    valor_unitario = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name=_lazy("valor unitário")
    )
    quantidade = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name=_lazy("quantidade")
    )
    origem = models.CharField(
        max_length=1,
        blank=True,
        null=True,
        verbose_name=_lazy("origem"),
        choices=Analise.SUBSISTEMA_TIPOS_ORIGEM,
    )
    observacoes = models.TextField(
        blank=True, null=True, verbose_name=_lazy("observações")
    )
    tipo = models.CharField(
        max_length=1,
        choices=Analise.SUBSISTEMA_TIPOS_INSUMO,
        default="P",
        verbose_name=_lazy("tipo"),
    )
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("última alteração")
    )

    # Relationship Fields
    unidade = models.ForeignKey(
        Unidade,
        verbose_name=_lazy("unidade"),
        related_name="subsistema_insumos",
        on_delete=models.PROTECT,
    )
    subsistema = models.ForeignKey(
        Subsistema,
        verbose_name=_lazy("subsistema"),
        related_name="insumos",
        on_delete=models.CASCADE,
    )
    produto = models.ForeignKey(
        Produto,
        verbose_name=_lazy("produto"),
        related_name="subsistema_insumos",
        on_delete=models.PROTECT,
    )

    class Meta:
        ordering = ("produto__nome",)

    def __unicode__(self):
        return "%s" % self.pk

    def get_absolute_url(self):
        return reverse("economica:subsistemainsumo_detail", args=(self.pk,))

    def get_update_url(self):
        return reverse("economica:subsistemainsumo_update", args=(self.pk,))

    def copia_profunda(self, novo_subsistema):
        novo_insumo = deepcopy(self)
        novo_insumo.id = None
        novo_insumo.subsistema = novo_subsistema
        novo_insumo.save()

    def save(self, *args, **kwargs):
        # update ultima_alteracao of agroecossistema:
        self.subsistema.analise.ciclo_anual_referencia.agroecossistema.save()
        super(SubsistemaInsumo, self).save(*args, **kwargs)


class SubsistemaPagamentoTerceiros(LumeModel):
    valor_unitario = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name=_lazy("valor unitário")
    )
    quantidade = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name=_lazy("quantidade")
    )
    origem = models.CharField(
        max_length=1,
        blank=True,
        null=True,
        verbose_name=_lazy("origem"),
        choices=Analise.SUBSISTEMA_TIPOS_ORIGEM,
    )
    observacoes = models.TextField(
        blank=True, null=True, verbose_name=_lazy("observações")
    )
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("última alteração")
    )

    # Relationship Fields
    unidade = models.ForeignKey(
        Unidade,
        verbose_name=_lazy("unidade"),
        related_name="subsistema_pagamento_terceiros",
        on_delete=models.PROTECT,
    )
    subsistema = models.ForeignKey(
        Subsistema,
        verbose_name=_lazy("subsistema"),
        related_name="pagamento_terceiros",
        on_delete=models.CASCADE,
    )
    servico = models.ForeignKey(
        Servico,
        verbose_name=_lazy("serviço"),
        related_name="subsistema_pagamento_terceiros",
        on_delete=models.PROTECT,
    )

    class Meta:
        ordering = ("servico__nome",)

    def __unicode__(self):
        return "%s" % self.pk

    def get_absolute_url(self):
        return reverse("economica:subsistemapagamentoterceiro_detail", args=(self.pk,))

    def get_update_url(self):
        return reverse("economica:subsistemapagamentoterceiro_update", args=(self.pk,))

    def copia_profunda(self, novo_subsistema):
        novo_pagamento_terceiros = deepcopy(self)
        novo_pagamento_terceiros.id = None
        novo_pagamento_terceiros.subsistema = novo_subsistema
        novo_pagamento_terceiros.save()

    def save(self, *args, **kwargs):
        # update ultima_alteracao of agroecossistema:
        self.subsistema.analise.ciclo_anual_referencia.agroecossistema.save()
        super(SubsistemaPagamentoTerceiros, self).save(*args, **kwargs)


class SubsistemaAquisicaoReciprocidade(LumeModel):
    valor_unitario = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name=_lazy("valor unitário")
    )
    quantidade = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name=_lazy("quantidade")
    )
    observacoes = models.TextField(
        blank=True, null=True, verbose_name=_lazy("observações")
    )
    tipo = models.CharField(
        max_length=1,
        verbose_name=_lazy("tipo"),
        choices=Analise.SUBSISTEMA_TIPOS_AQUISICAO_RECIPROCIDADE,
        default="I",
    )
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("última alteração")
    )

    # Relationship Fields
    unidade = models.ForeignKey(
        Unidade,
        verbose_name=_lazy("unidade"),
        related_name="subsistema_aquisicoes_reciprocidade",
        on_delete=models.PROTECT,
    )
    subsistema = models.ForeignKey(
        Subsistema,
        verbose_name=_lazy("subsistema"),
        related_name="aquisicoes_reciprocidade",
        on_delete=models.CASCADE,
    )
    produto = models.ForeignKey(
        Produto,
        verbose_name=_lazy("produto"),
        related_name="subsistema_aquisicoes_reciprocidade",
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )
    servico = models.ForeignKey(
        Servico,
        verbose_name=_lazy("serviço"),
        related_name="subsistema_aquisicoes_reciprocidade",
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )

    class Meta:
        ordering = ("produto__nome", "servico__nome")

    def __unicode__(self):
        return "%s" % self.pk

    def get_absolute_url(self):
        return reverse(
            "economica:subsistemaaquisicaoreciprocidade_detail", args=(self.pk,)
        )

    def get_update_url(self):
        return reverse(
            "economica:subsistemaaquisicaoreciprocidade_update", args=(self.pk,)
        )

    def copia_profunda(self, novo_subsistema):
        novo_reciprocidade = deepcopy(self)
        novo_reciprocidade.id = None
        novo_reciprocidade.subsistema = novo_subsistema
        novo_reciprocidade.save()

    def save(self, *args, **kwargs):
        # update ultima_alteracao of agroecossistema:
        self.subsistema.analise.ciclo_anual_referencia.agroecossistema.save()
        super(SubsistemaAquisicaoReciprocidade, self).save(*args, **kwargs)


class SubsistemaTempoTrabalho(Model):
    horas = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name=_lazy("horas")
    )
    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("última alteração")
    )

    # Relationship Fields
    subsistema = models.ForeignKey(
        Subsistema,
        verbose_name=_lazy("subsistema"),
        related_name="subsistema_tempos_de_trabalho",
        on_delete=models.CASCADE,
    )

    composicao_nsga = models.ForeignKey(
        ComposicaoNsga,
        verbose_name=_lazy("composição nsga"),
        related_name="subsistema_tempos_de_trabalho",
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ("composicao_nsga__pessoa__nome",)

    def __unicode__(self):
        return "%s" % self.pk

    def get_absolute_url(self):
        return reverse("economica:subsistematempotrabalho_detail", args=(self.pk,))

    def get_update_url(self):
        return reverse("economica:subsistematempotrabalho_update", args=(self.pk,))

    def copia_profunda(self, novo_subsistema, composicoes_do_novo_car):
        novo_tempo_de_trabalho = deepcopy(self)
        novo_tempo_de_trabalho.id = None
        novo_tempo_de_trabalho.subsistema = novo_subsistema
        novo_tempo_de_trabalho.composicao_nsga = composicoes_do_novo_car.filter(
            pessoa=self.composicao_nsga.pessoa
        ).first()
        novo_tempo_de_trabalho.save()

    def save(self, *args, **kwargs):
        # update ultima_alteracao of agroecossistema:
        self.subsistema.analise.ciclo_anual_referencia.agroecossistema.save()
        super(SubsistemaTempoTrabalho, self).save(*args, **kwargs)


class SubsistemaProducao(LumeModel):
    observacoes = models.TextField(
        blank=True, null=True, verbose_name=_lazy("observações")
    )
    valor_unitario = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        verbose_name=_lazy("valor unitário"),
        null=True,
        blank=True,
    )

    # MERCADO CONVENCIONAL
    quantidade_venda_convencional = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        blank=True,
        null=True,
        verbose_name=_lazy("quantidade de venda"),
    )
    valor_venda_convencional = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        blank=True,
        null=True,
        verbose_name=_lazy("valor de venda convencional"),
    )

    # MERCADO INSTITUCIONAL
    quantidade_venda_institucional = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        blank=True,
        null=True,
        verbose_name=_lazy("quantidade de venda institucional"),
    )
    valor_venda_institucional = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        blank=True,
        null=True,
        verbose_name=_lazy("valor de venda institucional"),
    )

    # MERCADO TERRITORIAL
    quantidade_venda_territorial = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        blank=True,
        null=True,
        verbose_name=_lazy("quantidade de venda territorial"),
    )
    valor_venda_territorial = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        blank=True,
        null=True,
        verbose_name=_lazy("valor de venda territorial"),
    )

    quantidade_autoconsumo = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        blank=True,
        null=True,
        verbose_name=_lazy("quantidade autoconsumo"),
    )
    valor_autoconsumo = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        blank=True,
        null=True,
        verbose_name=_lazy("valor autoconsumo"),
    )

    quantidade_doacoes_trocas = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        blank=True,
        null=True,
        verbose_name=_lazy("quantidade de doações / trocas"),
    )
    valor_doacoes_trocas = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        blank=True,
        null=True,
        verbose_name=_lazy("valor de doações / trocas"),
    )

    quantidade_insumos_produzidos = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        blank=True,
        null=True,
        verbose_name=_lazy("quantidade de insumos produzidos"),
    )
    valor_insumos_produzidos = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        blank=True,
        null=True,
        verbose_name=_lazy("valor de insumos produzidos"),
    )

    quantidade_estoque = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        blank=True,
        null=True,
        verbose_name=_lazy("quantidade em estoque"),
    )
    valor_estoque = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        blank=True,
        null=True,
        verbose_name=_lazy("valor de insumos produzidos"),
    )

    criacao = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_lazy("criação")
    )
    ultima_alteracao = models.DateTimeField(
        auto_now=True, editable=False, verbose_name=_lazy("última alteração")
    )

    # Relationship Fields
    subsistema = models.ForeignKey(
        Subsistema,
        verbose_name=_lazy("subsistema"),
        related_name="producoes",
        on_delete=models.CASCADE,
    )
    unidade = models.ForeignKey(
        Unidade,
        verbose_name=_lazy("unidade"),
        related_name="subsistema_producoes",
        on_delete=models.PROTECT,
    )
    produto = models.ForeignKey(
        Produto,
        verbose_name=_lazy("produto"),
        related_name="subsistema_producoes",
        on_delete=models.PROTECT,
    )

    class Meta:
        ordering = ("produto__nome",)

    def __unicode__(self):
        return "%s" % self.pk

    def get_absolute_url(self):
        return reverse("economica:subsistemaproducao_detail", args=(self.pk,))

    def get_update_url(self):
        return reverse("economica:subsistemaproducao_update", args=(self.pk,))

    def copia_profunda(self, novo_subsistema):
        nova_producao = deepcopy(self)
        nova_producao.id = None
        nova_producao.subsistema = novo_subsistema
        nova_producao.save()

    def save(self, *args, **kwargs):
        # update ultima_alteracao of agroecossistema:
        self.subsistema.analise.ciclo_anual_referencia.agroecossistema.save()
        super(SubsistemaProducao, self).save(*args, **kwargs)
