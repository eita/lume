from django.core.management.base import BaseCommand, CommandError
from economica.models import SubsistemaTempoTrabalho


class Command(BaseCommand):

    def handle(self, *args, **options):
        msg = self.fix_horas_ECA()

        self.stdout.write(self.style.SUCCESS(f"\nComando executado com sucesso!\n{msg}\n"))

    def fix_horas_ECA(self):
        subsis_tempo_trabalho = SubsistemaTempoTrabalho.objects.filter(
            # TODO verificar criterio
            composicao_nsga__pessoa__isnull=False
        )

        horas_fora_norma_eca = [
            obj.id
            for obj in subsis_tempo_trabalho
            if (not obj.composicao_nsga.pessoa.permitida_pelo_ECA()
                and obj.horas)
        ]

        SubsistemaTempoTrabalho.objects.filter(id__in=horas_fora_norma_eca).delete()

        return f"Foi retirado {len(horas_fora_norma_eca)} registros de horas de pessoas fora da norma ECA."

