from django.core.management.base import BaseCommand, CommandError
from django.db.models import F
from economica.models import SubsistemaProducao


class Command(BaseCommand):

    def handle(self, *args, **options):
        msg = self.fix_valores()

        self.stdout.write(self.style.SUCCESS(f"\nComando executado com sucesso!\n{msg}\n"))

    def fix_valores(self):
        qs = SubsistemaProducao.objects.filter(
            valor_venda_convencional__isnull=True
        )

        count = qs.count()

        if not count:
            return 'Nenhum valor à ser corrigido!'

        qs.update(
            valor_venda_convencional=F('valor_unitario'),
            valor_venda_institucional=F('valor_unitario'),
            valor_venda_territorial=F('valor_unitario'),

            valor_autoconsumo=F('valor_unitario'),
            valor_doacoes_trocas=F('valor_unitario'),
            valor_insumos_produzidos=F('valor_unitario'),
            valor_estoque=F('valor_unitario'),
        )

        return f"{count} registro(s) corrigidos!"

