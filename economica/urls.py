from django.urls import path
from django.views.generic.base import RedirectView

from . import views

app_name = 'economica'

urlpatterns = []

urlpatterns += (
    # urls for Analise
    path('update', views.AnaliseUpdateView.as_view(), name='analise_update'),
    path('relatorios', views.AnaliseDetailView.as_view(), name='analise_detail'),
    # path('painel-graficos',
    #     views.AnalisesPainelGraficosView.as_view(),
    #     name='painel-graficos'),
    path('painel-graficos',
         views.AnalisesPainelGraficosCarouselView.as_view(),
         name='painel-graficos'),
    path('delete', views.AnaliseDeleteView.as_view(), name='analise_delete'),
    path('estoqueinsumos', views.EstoqueInsumosUpdateView.as_view(), name='estoqueinsumos_update'),
    path('pagamentoterceiros', views.PagamentoTerceirosUpdateView.as_view(),
         name='pagamentoterceiros_update'),
    path('patrimonio/F', views.PatrimonioUpdateView.as_view(tipo='F'), name='patrimonio_F_update'),
    path('patrimonio/E', views.PatrimonioUpdateView.as_view(tipo='E'), name='patrimonio_E_update'),
    path('patrimonio/V', views.PatrimonioUpdateView.as_view(tipo='V'), name='patrimonio_V_update'),
    path('rendanaoagricola/T', views.RendaNaoAgricolaUpdateView.as_view(tipo="T"),
         name='rendanaoagricola_T_update'),
    path('rendanaoagricola/P', views.RendaNaoAgricolaUpdateView.as_view(tipo="P"),
         name='rendanaoagricola_P_update'),
    path('tempotrabalho', views.TempoTrabalhoUpdateView.as_view(), name='tempotrabalho_update'),

)

# Subsistema
urlpatterns += (
    path('subsistema', views.SubsistemaListView.as_view(),
        name='subsistema_list'),
    path('subsistema/vazio', views.SubsistemaVazioView.as_view(),
        name='subsistema_vazio'),
    path('subsistema/<int:pk>/insumo/P', views.SubsistemaInsumoUpdateView.as_view(tipo='P'),
        name='subsistemainsumo_P_update'),
    path('subsistema/<int:pk>/insumo/C', views.SubsistemaInsumoUpdateView.as_view(tipo='C'),
        name='subsistemainsumo_C_update'),
    path('subsistema/<int:pk>/pagamentoterceiros',
        views.SubsistemaPagamentoTerceirosUpdateView.as_view(),
        name='subsistemapagamentoterceiros_update'),
    path('subsistema/<int:pk>/aquisicaoreciprocidade/I',
        views.SubsistemaAquisicaoReciprocidadeUpdateView.as_view(tipo='I'),
        name='subsistemaaquisicaoreciprocidade_I_update'),
    path('subsistema/<int:pk>/aquisicaoreciprocidade/S',
        views.SubsistemaAquisicaoReciprocidadeUpdateView.as_view(tipo='S'),
        name='subsistemaaquisicaoreciprocidade_S_update'),
    path('subsistema/<int:pk>/tempo_trabalho', views.SubsistemaTempoTrabalhoUpdateView.as_view(),
        name='subsistematempotrabalho_update'),
    path('subsistema/<int:pk>/producao', views.SubsistemaProducaoUpdateView.as_view(),
        name='subsistemaproducao_update'),
)
