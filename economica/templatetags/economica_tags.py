from django import template
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from django.template import Context
from django.utils.translation import gettext as _
from django.urls import reverse
from django.utils.text import slugify
from main.utils import build_navbar_tab
import calendar

from agroecossistema.models import CicloAnualReferencia
from economica.models import Analise

register = template.Library()

@register.inclusion_tag('economica/inclusiontags/nav_tab_titles.html', takes_context = True)
def nav_tab_titles(context):
    if not 'car' in context or not 'agroecossistema' in context:
        return []

    targets = Analise.ABAS
    agroecossistema_id = context['agroecossistema'].id
    car = context['car']
    ano_car = car.ano
    sim_slug = car.sim_slug
    submodulo = context['submodulo']
    subsistema_id = context['subsistema_id'] if 'subsistema_id' in context else ""
    submodulo_tipo = context['subsistema_tipo'] if 'subsistema_tipo' in context else ""
    target_id = context['target_id'] if 'target_id' in context else ""

    navbar_tabs = []
    for target in targets:
        navbar_tab = build_navbar_tab({
            'target': target,
            'submodulo': submodulo,
            'sim_slug': sim_slug,
            'agroecossistema_id': agroecossistema_id,
            'ano_car': ano_car,
            'subsistema_id': subsistema_id
        })
        navbar_tabs.append(navbar_tab)

    return {
        'car': car,
        'agroecossistema': context['agroecossistema'],
        'agroecossistema_id': agroecossistema_id,
        'navbar_tabs': navbar_tabs
    }

@register.inclusion_tag('economica/inclusiontags/sub_nav_tab_titles.html', takes_context = True)
def sub_nav_tab_titles(context):
    targets = Analise.ABAS
    colspan = context['colspan']
    agroecossistema_id = context['agroecossistema'].id
    submodulo = context['submodulo']
    submodulo_tipo = '' if 'submodulo_tipo' not in context else context['submodulo_tipo']
    subsistema_id = context['subsistema_id'] if submodulo[:10] == 'subsistema' else None
    sim_slug = context['car'].sim_slug
    ano_car = context['car'].ano
    analise = Analise.objects.get(pk=context['car'].analise_economica.id)

    sub_navbar_tabs = []

    for target in targets:
        if target['submodulo'] == submodulo:
            if 'sub_abas_automaticas' in target:
                for sub_aba in target['sub_abas_automaticas']:
                    active = " active" if sub_aba[0]==submodulo_tipo else ""
                    if active:
                        url = "#"
                    else:
                        url = analise.get_update_submodulo_url(submodulo, sub_aba[0], subsistema_id)

                    sub_navbar_tabs.append({
                        'title': sub_aba[1],
                        'active': active,
                        'url': url
                    })
            elif 'sub_abas' in target:
                for sub_aba in target['sub_abas']:
                    custom_submodulo = sub_aba['target_id'] if 'target_id' in sub_aba else submodulo
                    tipo = '' if 'tipo' not in sub_aba else sub_aba['tipo']
                    if submodulo_tipo:
                        active = " active" if 'tipo' in sub_aba and sub_aba['tipo']==submodulo_tipo else ""
                    else:
                        active = " active" if sub_aba['target_id']==submodulo else ""

                    if active:
                        url = "#"
                    else:
                        url = analise.get_update_submodulo_url(custom_submodulo, tipo, subsistema_id)

                    sub_navbar_tabs.append({
                        'title': sub_aba['title'],
                        'active': active,
                        'url': url
                    })

    return {
        'colspan': colspan,
        'sub_navbar_tabs': sub_navbar_tabs,
    }

@register.simple_tag
def first_submodule_url(agroecossistema_id, ano_car, submodulo, sim_slug=None, subsistema_id=None):
    targets = Analise.ABAS

    for target in targets:
        navbar_tab = build_navbar_tab({
            'target': target,
            'submodulo': submodulo,
            'sim_slug': sim_slug,
            'agroecossistema_id': agroecossistema_id,
            'ano_car': ano_car,
            'subsistema_id': subsistema_id
        })
        if navbar_tab['url'] is not None:
            return navbar_tab['url']

    return ''

@register.simple_tag(takes_context=True)
def get_has_sub_abas(context):
    targets = Analise.ABAS

    has_sub_abas = False
    for target in targets:
        if target['submodulo'] == context['submodulo']:
            has_sub_abas = ('sub_abas' in target) or ('sub_abas_automaticas' in target)

    return has_sub_abas

@register.simple_tag(takes_context=True)
def is_economica_editar_dados(context):
    if not 'modulo' in context or context['modulo'] != 'economica':
        return False

    if context['submodulo'] == 'editar_dados':
        return True

    for target in Analise.ABAS:
        if target['submodulo'] == context['submodulo']:
            return True

    return False

@register.simple_tag(takes_context=True)
def get_sim_slug(context):
    if 'car' in context:
        return context['car'].sim_slug

    sim_slug = context['request'].session.get('sim_slug')
    if sim_slug:
        return sim_slug

    return ''

@register.simple_tag(takes_context=True)
def get_ano_car(context):
    if 'car' in context:
        return context['car'].ano

    ano_car = context['request'].session.get('ano_car')
    if ano_car:
        return ano_car

    return ''

@register.simple_tag(takes_context=True)
def get_subsistema_id(context):
    try:
        if 'submodulo' in context and context['submodulo'][:10] == 'subsistema':
            return context['object'].pk
    except:
        subsistema_id = context['request'].session.get('subsistema_id')
        if subsistema_id:
            return subsistema_id

    try:
        return context['car'].analise_economica.subsistemas.first().id
    except:
        return ''

@register.simple_tag(takes_context = True)
def get_subsistema(context):
    if not 'subsistema_id' in context or not context['subsistema_id'] or not 'car' in context:
        return ''
    subsistema_id = context['subsistema_id']
    car = context['car']

    subsistema = car.analise_economica.subsistemas.get(pk=subsistema_id)

    return subsistema

@register.simple_tag()
def get_economica_analise_detail_url(agroecossistema_id, car = {}, ano_car = None, sim_slug = None):
    if agroecossistema_id == '' or not agroecossistema_id:
        return ''

    if 'ano' in car:
        return car.get_economica_analise_detail_url()

    if not ano_car:
        return ''

    get_args = {
        'agroecossistema_id': agroecossistema_id,
        'ano': ano_car
    }
    if sim_slug:
        get_args['sim_slug'] = sim_slug
    else:
        get_args['sim_slug__isnull'] = True

    car = CicloAnualReferencia.objects.get(**get_args)
    return car.get_economica_analise_detail_url()

@register.simple_tag(takes_context = True)
def get_car_list_url(context, force_exit_sim = False):
    if not 'agroecossistema' in context:
        return ''

    agroecossistema = context['agroecossistema']
    ano_car = context['ano_car'] if 'ano_car' in context else ''
    sim_slug = context['sim_slug'] if 'sim_slug' in context and not force_exit_sim else ''

    return agroecossistema.get_car_list_url(ano_car, sim_slug)

@register.simple_tag(takes_context = True)
def get_car_list(context, force_exit_sim = False):
    if not 'agroecossistema' in context:
        return ''

    agroecossistema = context['agroecossistema']
    ano_car = context['ano_car'] if 'ano_car' in context else ''
    exclude_sim_slug = 'sim_slug' not in context or not context['sim_slug'] or force_exit_sim
    return CicloAnualReferencia.objects.filter(agroecossistema_id=agroecossistema.id, sim_slug__isnull=exclude_sim_slug)
