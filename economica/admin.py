from django.contrib import admin

from economica.models import Analise, EstoqueInsumos, PagamentoTerceiros, Patrimonio, RendaNaoAgricola, TempoTrabalho, \
    Subsistema, SubsistemaInsumo, SubsistemaTempoTrabalho, SubsistemaProducao

admin.site.register(Analise)
admin.site.register(EstoqueInsumos)
admin.site.register(PagamentoTerceiros)
admin.site.register(Patrimonio)
admin.site.register(RendaNaoAgricola)
admin.site.register(TempoTrabalho)
admin.site.register(Subsistema)
admin.site.register(SubsistemaInsumo)
admin.site.register(SubsistemaTempoTrabalho)
admin.site.register(SubsistemaProducao)
