from django.core.exceptions import NON_FIELD_ERRORS
from django.db import transaction
from django.http.response import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.utils.functional import cached_property
from django.utils.text import slugify
from django.utils.translation import gettext as _
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, DetailView, ListView, UpdateView, CreateView
from django.core import serializers
from rules.contrib.views import PermissionRequiredMixin

from agroecossistema.models import Agroecossistema, CicloAnualReferencia
from economica.utils import LJSONRenderer
from main.views import LumeDeleteView
from relatorios.serializers import TotaisAnaliseEconomicaSerializer, TotaisPorProdutoSerializer, TotaisQtdePorProdutoPorUnidadeSerializer
from .forms import AnaliseForm, SubsistemaForm, SubsistemaFormSet, SubsistemaProducaoFormSet, TempoTrabalhoFormSet, RendaNaoAgricolaFormSet, PatrimonioFormSet, PagamentoTerceirosFormSet, EstoqueInsumosFormSet, SubsistemaInsumoFormSet, SubsistemaTempoTrabalhoFormSet, SubsistemaPagamentoTerceirosFormSet, SubsistemaAquisicaoReciprocidadeFormSet
from .models import Analise, Patrimonio, RendaNaoAgricola, Subsistema, SubsistemaInsumo, SubsistemaAquisicaoReciprocidade
from relatorios.models import TotaisAnaliseEconomica

TOTAIS_ANALISE = None


class AnaliseListView(PermissionRequiredMixin, ListView):
    model = Analise
    permission_required = 'agroecossistema.visualizar'

    def get_permission_object(self):
        agroecossistema = get_object_or_404(Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['modulo'] = 'economica'
        context['agroecossistema'] = Agroecossistema.objects.get(id=self.kwargs['agroecossistema_id'])
        return context


class AnaliseDetailView(PermissionRequiredMixin, DetailView):
    model = Analise
    obj = None

    totais_analise = None
    totais_subsistemas = None
    totais_tempostrabalho = None

    permission_required = 'agroecossistema.visualizar'

    def get_permission_object(self):
        agroecossistema = get_object_or_404(Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['modulo'] = 'economica'
        context['submodulo'] = 'relatorios'
        context['agroecossistema'] = Agroecossistema.objects.get(id=self.kwargs['agroecossistema_id'])
        context['car'] = self.get_object().ciclo_anual_referencia
        context['arquivo_pre'] = slugify(context['agroecossistema'].nsga + "_" + context['car'].__str__())
        context['totais'] = self.totais
        context['totais_json'] = self.totais_json(context['totais'])
        context['totais_por_produto_json'] = self.totais_por_produto_json
        context['totais_tempostrabalho_json'] = self.totais_tempostrabalho_json
        context['enable_datatable_export'] = True
        return context

    @cached_property
    def totais(self):
        try:
            return self.get_object().totais
        except Analise.totais.RelatedObjectDoesNotExist:
            return None

    def totais_json(self, totais):
        r = LJSONRenderer().render(
            TotaisAnaliseEconomicaSerializer(totais).data
        ).decode("utf-8")
        return r

    @cached_property
    def totais_por_produto_json(self):
        r = LJSONRenderer().render(
            TotaisPorProdutoSerializer(self.get_object().totais_por_produto.all(), many=True).data
        ).decode("utf-8")
        return r

    @cached_property
    def totais_tempostrabalho_json(self):
        r = LJSONRenderer().render(
            self.get_object().get_totais_tempostrabalho()
        ).decode("utf-8")
        return r

    def get_object(self, queryset=None):
        if self.obj:
            return self.obj

        get_args = {
            'agroecossistema_id': self.kwargs['agroecossistema_id'],
            'ano': self.kwargs['ano_car']
        }

        if 'sim_slug' in self.kwargs:
            get_args['sim_slug'] = self.kwargs['sim_slug']
        else:
            get_args['sim_slug__isnull'] = True

        car = CicloAnualReferencia.objects.get(**get_args)
        obj, created = Analise.objects.get_or_create(ciclo_anual_referencia_id=car.id)
        self.obj = obj
        return obj


class AnalisesPainelGraficosView(AnaliseDetailView):
    template_name = 'economica/painel_graficos.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        painel_graficos = [
            'quadroSintese',
            'graficosRenda',
            'graficosTrabalho',
            'graficosReciprocidade',
            'diagramaSintese'
        ]
        def filter_painel_graficos(k):
            return k in painel_graficos
        context['totais'].RELATORIOS = {
            k:v for k,v in context['totais'].RELATORIOS.items()
            if filter_painel_graficos(k)
        }
        # import pdb; pdb.set_trace()
        return context


class AnalisesPainelGraficosCarouselView(AnaliseDetailView):
    template_name = 'economica/painel_graficos_carousel.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        self.painel_graficos = {
            'quadroSintese': [
                'quadroSintese',
            ],
            'graficosRenda': [
                'origem_rendas',
                'composicao_rendas',
                'composicao_rendas_por_area',
            ],
            'graficosTrabalho': [
                'rendas_totais_pessoa_et',
                'rendas_et_pessoa_responsavel',
                'rendas_genero_geracao_et',
            ],
            'graficosReciprocidade': [
                'reciprocidade_ecologica',
                'reciprocidade_social',
            ],
            'diagramaSintese': [
                'sintese_pb_doacoes_trocas',
            ],
        }

        totais_relatorios = {}
        for k,v in context['totais'].RELATORIOS.items():
            if k not in self.painel_graficos:
                continue
            graficos={}
            try:
                graficos = v['itens']
            except KeyError:
                if v['id'] not in self.painel_graficos[k]:
                    continue
            try:
                totais_relatorios[k].update(v)
            except KeyError:
                totais_relatorios[k] = v
            if not graficos:
                continue
            totais_relatorios[k]['itens'] = [
                item
                for item in graficos
                if item['id'] in self.painel_graficos[k]
            ]
        context['totais'].RELATORIOS = totais_relatorios
        return context


class AnaliseUpdateView(PermissionRequiredMixin, UpdateView):
    model = Analise
    form_class = AnaliseForm
    permission_required = 'permissao_padrao'
    titulo = _('Análise Econômica')
    obj = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = self.titulo
        context['modulo'] = 'economica'
        context['submodulo'] = 'editar_dados'
        context['agroecossistema'] = Agroecossistema.objects.get(id=self.kwargs['agroecossistema_id'])
        context['composicao_rendas'] = self.get_composicao_rendas()
        context['car'] = self.get_object().ciclo_anual_referencia
        car = context['car']

        if self.request.POST:
            context['tempo_trabalho_formset'] = TempoTrabalhoFormSet(self.request.POST, instance=self.get_object())
            context['renda_nao_agricola_formset'] = RendaNaoAgricolaFormSet(self.request.POST,
                                                                            instance=self.get_object())
            context['patrimonio_formset'] = PatrimonioFormSet(self.request.POST, instance=self.get_object())
            context['pagamento_terceiros_formset'] = PagamentoTerceirosFormSet(self.request.POST,
                                                                               instance=self.get_object())
            context['estoque_insumos_formset'] = EstoqueInsumosFormSet(self.request.POST, instance=self.get_object())
        else:
            context['tempo_trabalho_formset'] = TempoTrabalhoFormSet(instance=self.get_object())
            context['renda_nao_agricola_formset'] = RendaNaoAgricolaFormSet(instance=self.get_object())
            context['patrimonio_formset'] = PatrimonioFormSet(instance=self.get_object())
            context['pagamento_terceiros_formset'] = PagamentoTerceirosFormSet(instance=self.get_object())
            context['estoque_insumos_formset'] = EstoqueInsumosFormSet(instance=self.get_object())
        return context

    def get_object(self, queryset=None):
        if self.obj:
            return self.obj

        car = CicloAnualReferencia.objects.get(agroecossistema_id=self.kwargs['agroecossistema_id'],
                                               ano=self.kwargs['ano_car'])
        obj, created = Analise.objects.get_or_create(ciclo_anual_referencia_id=car.id)
        self.obj = obj
        return obj

    def form_valid(self, form):
        context = self.get_context_data()
        tempo_trabalho_formset = context['tempo_trabalho_formset']
        renda_nao_agricola_formset = context['renda_nao_agricola_formset']
        patrimonio_formset = context['patrimonio_formset']
        pagamento_terceiros_formset = context['pagamento_terceiros_formset']
        estoque_insumos_formset = context['estoque_insumos_formset']

        with transaction.atomic():
            if tempo_trabalho_formset.is_valid() and renda_nao_agricola_formset.is_valid() and patrimonio_formset.is_valid() and pagamento_terceiros_formset.is_valid() and estoque_insumos_formset.is_valid():
                self.object = form.save()

                tempo_trabalho_formset.instance = self.object
                tempo_trabalho_formset.save()
                renda_nao_agricola_formset.instance = self.object
                renda_nao_agricola_formset.save()
                patrimonio_formset.instance = self.object
                patrimonio_formset.save()
                pagamento_terceiros_formset.instance = self.object
                pagamento_terceiros_formset.save()
                estoque_insumos_formset.instance = self.object
                estoque_insumos_formset.save()

            else:
                form.full_clean()
                form._errors[NON_FIELD_ERRORS] = form.error_class(
                    [
                        _("Foram encontrados erros no formulário e os dados NÃO foram salvos. Favor fazer as correções indicadas.")])
                return self.render_to_response(self.get_context_data(form=form))

        return super(AnaliseUpdateView, self).form_valid(form)

    def get_success_url(self):
        return self.request.path


class AnaliseDeleteView(PermissionRequiredMixin, LumeDeleteView):
    model = Analise
    permission_required = 'agroecossistema.alterar'
    obj = None

    def get_permission_object(self):
        agroecossistema = get_object_or_404(Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(AnaliseDeleteView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        if self.obj:
            return self.obj

        car = CicloAnualReferencia.objects.get(agroecossistema_id=self.kwargs['agroecossistema_id'],
                                               ano=self.kwargs['ano_car'])
        obj, created = Analise.objects.get_or_create(ciclo_anual_referencia_id=car.id)
        self.obj = obj
        return obj


class EstoqueInsumosUpdateView(PermissionRequiredMixin, UpdateView):
    model = Analise
    form_class = AnaliseForm
    permission_required = 'agroecossistema.visualizar'
    titulo = _('Análise Econômica')
    template_name = 'economica/estoqueinsumos_form.html'
    obj = None

    def get_permission_object(self):
        agroecossistema = get_object_or_404(Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = self.titulo
        context['modulo'] = 'economica'
        context['submodulo'] = 'estoqueinsumos'
        context['submodulo_titulo'] = Analise.get_submodulo_titulo(context['submodulo'])
        context['agroecossistema'] = Agroecossistema.objects.get(id=self.kwargs['agroecossistema_id'])
        context['car'] = self.get_object().ciclo_anual_referencia
        context['totais_qtde_por_produto_por_unidade_json'] = self.totais_qtde_por_produto_por_unidade_json
        car = context['car']

        if self.request.POST:
            context['estoque_insumos_formset'] = EstoqueInsumosFormSet(self.request.POST, instance=self.get_object())
        else:
            context['estoque_insumos_formset'] = EstoqueInsumosFormSet(instance=self.get_object())
        return context

    def get_object(self, queryset=None):
        if self.obj:
            return self.obj

        get_args = {
            'agroecossistema_id': self.kwargs['agroecossistema_id'],
            'ano': self.kwargs['ano_car']
        }

        if 'sim_slug' in self.kwargs:
            get_args['sim_slug'] = self.kwargs['sim_slug']
        else:
            get_args['sim_slug__isnull'] = True

        car = CicloAnualReferencia.objects.get(**get_args)
        obj, created = Analise.objects.get_or_create(ciclo_anual_referencia_id=car.id)
        self.obj = obj
        return obj

    def form_valid(self, form):
        context = self.get_context_data()
        estoque_insumos_formset = context['estoque_insumos_formset']

        with transaction.atomic():
            if estoque_insumos_formset.is_valid():
                self.object = form.save()

                estoque_insumos_formset.instance = self.object
                estoque_insumos_formset.save()

            else:
                form.full_clean()
                form._errors[NON_FIELD_ERRORS] = form.error_class(
                    [
                        _("Foram encontrados erros no formulário e os dados NÃO foram salvos. Favor fazer as correções indicadas.")])
                return self.render_to_response(self.get_context_data(form=form))

        return super(EstoqueInsumosUpdateView, self).form_valid(form)

    def get_success_url(self):
        return self.request.path

    @cached_property
    def totais_qtde_por_produto_por_unidade_json(self):
        r = LJSONRenderer().render(
            TotaisQtdePorProdutoPorUnidadeSerializer(self.get_object().totais_qtde_por_produto_por_unidade.all(), many=True).data).decode("utf-8")
        return r


class PagamentoTerceirosUpdateView(PermissionRequiredMixin, UpdateView):
    model = Analise
    form_class = AnaliseForm
    permission_required = 'agroecossistema.visualizar'
    titulo = _('Análise Econômica')
    template_name = 'economica/pagamentoterceiros_form.html'
    obj = None

    def get_permission_object(self):
        agroecossistema = get_object_or_404(Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = self.titulo
        context['modulo'] = 'economica'
        context['submodulo'] = 'pagamentoterceiros'
        context['submodulo_titulo'] = Analise.get_submodulo_titulo(context['submodulo'])
        context['agroecossistema'] = Agroecossistema.objects.get(id=self.kwargs['agroecossistema_id'])
        context['car'] = self.get_object().ciclo_anual_referencia
        car = context['car']

        if self.request.POST:
            context['pagamento_terceiros_formset'] = PagamentoTerceirosFormSet(self.request.POST,
                                                                               instance=self.get_object())
        else:
            context['pagamento_terceiros_formset'] = PagamentoTerceirosFormSet(instance=self.get_object())
        return context

    def get_object(self, queryset=None):
        if self.obj:
            return self.obj

        get_args = {
            'agroecossistema_id': self.kwargs['agroecossistema_id'],
            'ano': self.kwargs['ano_car']
        }

        if 'sim_slug' in self.kwargs:
            get_args['sim_slug'] = self.kwargs['sim_slug']
        else:
            get_args['sim_slug__isnull'] = True

        car = CicloAnualReferencia.objects.get(**get_args)
        obj, created = Analise.objects.get_or_create(ciclo_anual_referencia_id=car.id)
        self.obj = obj
        return obj

    def form_valid(self, form):
        context = self.get_context_data()
        pagamento_terceiros_formset = context['pagamento_terceiros_formset']

        with transaction.atomic():
            if pagamento_terceiros_formset.is_valid():
                self.object = form.save()

                pagamento_terceiros_formset.instance = self.object
                pagamento_terceiros_formset.save()

            else:
                form.full_clean()
                form._errors[NON_FIELD_ERRORS] = form.error_class(
                    [
                        _("Foram encontrados erros no formulário e os dados NÃO foram salvos. Favor fazer as correções indicadas.")])
                return self.render_to_response(self.get_context_data(form=form))

        return super(PagamentoTerceirosUpdateView, self).form_valid(form)

    def get_success_url(self):
        return self.request.path


class PatrimonioUpdateView(PermissionRequiredMixin, UpdateView):
    model = Analise
    form_class = AnaliseForm
    permission_required = 'agroecossistema.visualizar'
    titulo = _('Análise Econômica')
    template_name = 'economica/patrimonio_form.html'
    obj = None
    tipo = None

    def get_permission_object(self):
        agroecossistema = get_object_or_404(Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = self.titulo
        context['modulo'] = 'economica'
        context['submodulo'] = 'patrimonio'
        context['submodulo_tipo'] = self.tipo
        context['submodulo_titulo'] = Analise.get_submodulo_titulo(context['submodulo'], self.tipo)
        context['agroecossistema'] = Agroecossistema.objects.get(id=self.kwargs['agroecossistema_id'])
        context['car'] = self.get_object().ciclo_anual_referencia

        qset = Patrimonio.objects.filter(analise_id=self.get_object().id, tipo=self.tipo)

        if self.request.POST:
            context['patrimonio_formset'] = PatrimonioFormSet(self.request.POST,
                                                              instance=self.get_object(),
                                                              queryset=qset,
                                                              form_kwargs={'tipo': self.tipo})
        else:
            context['patrimonio_formset'] = PatrimonioFormSet(instance=self.get_object(),
                                                              queryset=qset,
                                                              form_kwargs={'tipo': self.tipo})
        return context

    def get_object(self, queryset=None):
        if self.obj:
            return self.obj

        get_args = {
            'agroecossistema_id': self.kwargs['agroecossistema_id'],
            'ano': self.kwargs['ano_car']
        }

        if 'sim_slug' in self.kwargs:
            get_args['sim_slug'] = self.kwargs['sim_slug']
        else:
            get_args['sim_slug__isnull'] = True

        car = CicloAnualReferencia.objects.get(**get_args)
        obj, created = Analise.objects.get_or_create(ciclo_anual_referencia_id=car.id)
        self.obj = obj
        return obj

    def form_valid(self, form):
        context = self.get_context_data()
        patrimonio_formset = context['patrimonio_formset']

        with transaction.atomic():
            if patrimonio_formset.is_valid():
                self.object = form.save()

                patrimonio_formset.instance = self.object
                patrimonio_formset.save()

            else:
                form.full_clean()
                form._errors[NON_FIELD_ERRORS] = form.error_class(
                    [
                        _("Foram encontrados erros no formulário e os dados NÃO foram salvos. Favor fazer as correções indicadas.")])
                return self.render_to_response(self.get_context_data(form=form))

        return super(PatrimonioUpdateView, self).form_valid(form)

    def get_success_url(self):
        return self.request.path


class RendaNaoAgricolaUpdateView(PermissionRequiredMixin, UpdateView):
    model = Analise
    form_class = AnaliseForm
    permission_required = 'agroecossistema.visualizar'
    titulo = _('Análise Econômica')
    template_name = 'economica/rendanaoagricola_form.html'
    obj = None
    tipo = None

    def get_permission_object(self):
        agroecossistema = get_object_or_404(Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = self.titulo
        context['modulo'] = 'economica'
        context['submodulo'] = 'rendanaoagricola'
        context['submodulo_tipo'] = self.tipo
        context['submodulo_titulo'] = Analise.get_submodulo_titulo(context['submodulo'], self.tipo)
        context['agroecossistema'] = Agroecossistema.objects.get(id=self.kwargs['agroecossistema_id'])
        context['car'] = self.get_object().ciclo_anual_referencia

        qset = RendaNaoAgricola.objects.filter(analise_id=self.get_object().id, tipo=self.tipo)

        if self.request.POST:
            context['renda_nao_agricola_formset'] = RendaNaoAgricolaFormSet(self.request.POST,
                                                                            instance=self.get_object(),
                                                                            queryset=qset)
        else:
            context['renda_nao_agricola_formset'] = RendaNaoAgricolaFormSet(instance=self.get_object(),
                                                                            queryset=qset)
        return context

    def get_object(self, queryset=None):
        if self.obj:
            return self.obj

        get_args = {
            'agroecossistema_id': self.kwargs['agroecossistema_id'],
            'ano': self.kwargs['ano_car']
        }

        if 'sim_slug' in self.kwargs:
            get_args['sim_slug'] = self.kwargs['sim_slug']
        else:
            get_args['sim_slug__isnull'] = True

        car = CicloAnualReferencia.objects.get(**get_args)
        obj, created = Analise.objects.get_or_create(ciclo_anual_referencia_id=car.id)
        self.obj = obj
        return obj

    def form_valid(self, form):
        context = self.get_context_data()
        renda_nao_agricola_formset = context['renda_nao_agricola_formset']

        with transaction.atomic():
            if renda_nao_agricola_formset.is_valid():
                self.object = form.save()

                renda_nao_agricola_formset.instance = self.object
                renda_nao_agricola_formset.save()

            else:
                form.full_clean()
                form._errors[NON_FIELD_ERRORS] = form.error_class(
                    [
                        _("Foram encontrados erros no formulário e os dados NÃO foram salvos. Favor fazer as correções indicadas.")])
                return self.render_to_response(self.get_context_data(form=form))

        return super(RendaNaoAgricolaUpdateView, self).form_valid(form)

    def get_success_url(self):
        return self.request.path


class TempoTrabalhoUpdateView(PermissionRequiredMixin, UpdateView):
    model = Analise
    form_class = AnaliseForm
    permission_required = 'agroecossistema.visualizar'
    titulo = _('Análise Econômica')
    template_name = 'economica/tempotrabalho_form.html'
    obj = None

    def get_permission_object(self):
        agroecossistema = get_object_or_404(Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    def get_context_data(self, **kwargs):
        car = self.get_object().ciclo_anual_referencia
        context = super().get_context_data(**kwargs)
        context['titulo'] = self.titulo
        context['modulo'] = 'economica'
        context['submodulo'] = 'tempotrabalho'
        context['submodulo_titulo'] = Analise.get_submodulo_titulo(context['submodulo'])
        context['no_convert_numbers_to_float'] = True
        context['agroecossistema'] = Agroecossistema.objects.get(id=self.kwargs['agroecossistema_id'])
        context['car'] = car
        context['composicoes_nsga'] = serializers.serialize("json", car.composicoes_nsga.all())

        try:
            totais = TotaisAnaliseEconomica.objects.get(analise_id=self.get_object().id)
            context['horas_trabalho_por_pessoa'] = serializers.serialize("json", totais.horas_trabalho_por_pessoa)
        except:
            context['horas_trabalho_por_pessoa'] = None

        if self.request.POST:
            context['tempo_trabalho_formset'] = TempoTrabalhoFormSet(
                self.request.POST, instance=self.get_object(),
                form_kwargs={'agroecossistema_id': self.kwargs['agroecossistema_id'],
                             'ano_car': self.kwargs['ano_car'],
                             'sim_slug': self.kwargs.get('sim_slug', None)})

        else:
            context['tempo_trabalho_formset'] = TempoTrabalhoFormSet(
                instance=self.get_object(),
                form_kwargs={'agroecossistema_id': self.kwargs['agroecossistema_id'],
                             'ano_car': self.kwargs['ano_car'],
                             'sim_slug': self.kwargs.get('sim_slug', None)})

        return context

    def get_object(self, queryset=None):
        if self.obj:
            return self.obj

        get_args = {
            'agroecossistema_id': self.kwargs['agroecossistema_id'],
            'ano': self.kwargs['ano_car']
        }

        if 'sim_slug' in self.kwargs:
            get_args['sim_slug'] = self.kwargs['sim_slug']
        else:
            get_args['sim_slug__isnull'] = True

        car = CicloAnualReferencia.objects.get(**get_args)
        obj, created = Analise.objects.get_or_create(ciclo_anual_referencia_id=car.id)
        self.obj = obj
        return obj

    def form_valid(self, form):
        context = self.get_context_data()
        tempo_trabalho_formset = context['tempo_trabalho_formset']

        with transaction.atomic():
            if tempo_trabalho_formset.is_valid():
                self.object = form.save()

                tempo_trabalho_formset.instance = self.object
                tempo_trabalho_formset.save()

            else:
                form.full_clean()
                form._errors[NON_FIELD_ERRORS] = form.error_class(
                    [
                        _("Foram encontrados erros no formulário e os dados NÃO foram salvos. Favor fazer as correções indicadas.")])
                return self.render_to_response(self.get_context_data(form=form))

        return super(TempoTrabalhoUpdateView, self).form_valid(form)

    def get_success_url(self):
        return self.request.path


class SubsistemaListView(PermissionRequiredMixin, UpdateView):
    model = Analise
    form_class = AnaliseForm
    permission_required = 'agroecossistema.visualizar'
    template_name = 'economica/subsistema_list.html'

    def get_permission_object(self):
        agroecossistema = get_object_or_404(Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        sim_slug = None if 'sim_slug' not in self.kwargs else self.kwargs['sim_slug']
        ano_car = None if 'ano_car' not in self.kwargs else self.kwargs['ano_car']

        built_session = Agroecossistema.build_session(self.kwargs['agroecossistema_id'], ano_car, sim_slug)
        for key, val in built_session['session'].items():
            self.request.session[key] = val

        context['modulo'] = 'agroecossistema'
        context['submodulo'] = 'subsistemas'
        context['agroecossistema'] = Agroecossistema.objects.get(id=self.kwargs['agroecossistema_id'])
        context['car'] = built_session['car']
        # context['object'] = _('Subsistemas')  # para o breadcrumb

        if self.request.POST:
            context['subsistemas_formset'] = SubsistemaFormSet(self.request.POST, instance=self.get_object())
        else:
            context['subsistemas_formset'] = SubsistemaFormSet(instance=self.get_object())

        return context

    def form_valid(self, form):
        context = self.get_context_data()
        subsistemas_formset = context['subsistemas_formset']

        try:
            with transaction.atomic():
                self.object = form.save()

                if subsistemas_formset.is_valid():
                    subsistemas_formset.instance = self.object
                    subsistemas_formset.save()
                else:
                    tmp = form.full_clean()
                    form._errors[NON_FIELD_ERRORS] = form.error_class([
                        _("Foram encontrados erros no formulário e os dados NÃO foram salvos. Favor fazer as correções indicadas.")
                    ])
                    return self.render_to_response(self.get_context_data(form=form))

        except ValueError:
            tmp = form.full_clean()
            form._errors[NON_FIELD_ERRORS] = form.error_class([
                _("Foram encontrados erros no formulário e os dados NÃO foram salvos. Favor fazer as correções indicadas.")
            ])
            return self.render_to_response(self.get_context_data(form=form))

        return super(SubsistemaListView, self).form_valid(form)

    def get_object(self, queryset=None):
        get_args = {'agroecossistema_id': self.kwargs['agroecossistema_id'],'ano': self.kwargs['ano_car']}

        if 'sim_slug' in self.kwargs:
            get_args['sim_slug'] = self.kwargs['sim_slug']
        else:
            get_args['sim_slug__isnull'] = True

        car = CicloAnualReferencia.objects.get(**get_args)

        if car.analise_economica:
            return car.analise_economica

        analise_economica, created = Analise.objects.get_or_create(ciclo_anual_referencia_id=car.id)
        return analise_economica

    def get_success_url(self):
        return self.request.path

    def get_car(self):
        get_args = {
            'agroecossistema_id': self.kwargs['agroecossistema_id'],
            'ano': self.kwargs['ano_car']
        }

        if 'sim_slug' in self.kwargs:
            get_args['sim_slug'] = self.kwargs['sim_slug']
        else:
            get_args['sim_slug__isnull'] = True

        return CicloAnualReferencia.objects.get(**get_args)

    def get_queryset(self):
        get_args = {
            'ciclo_anual_referencia__agroecossistema_id': self.kwargs['agroecossistema_id'],
            'ciclo_anual_referencia__ano': self.kwargs['ano_car']
        }

        if 'sim_slug' in self.kwargs:
            get_args['ciclo_anual_referencia__sim_slug'] = self.kwargs['sim_slug']
        else:
            get_args['ciclo_anual_referencia__sim_slug__isnull'] = True

        analise = Analise.objects.get(**get_args)
        return analise.subsistemas.all()

class SubsistemaVazioView(PermissionRequiredMixin, TemplateView):
    permission_required = 'agroecossistema.visualizar'
    template_name = 'economica/subsistema_vazio.html'

    def get_permission_object(self):
        agroecossistema = get_object_or_404(Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        sim_slug = None if 'sim_slug' not in self.kwargs else self.kwargs['sim_slug']
        ano_car = None if 'ano_car' not in self.kwargs else self.kwargs['ano_car']

        built_session = Agroecossistema.build_session(self.kwargs['agroecossistema_id'], ano_car, sim_slug)
        for key, val in built_session['session'].items():
            self.request.session[key] = val

        context['modulo'] = 'agroecossistema'
        context['submodulo'] = 'subsistemas'
        context['agroecossistema'] = Agroecossistema.objects.get(id=self.kwargs['agroecossistema_id'])
        context['car'] = built_session['car']

        return context

class SubsistemaInsumoUpdateView(PermissionRequiredMixin, UpdateView):
    model = Subsistema
    form_class = SubsistemaForm
    permission_required = 'agroecossistema.visualizar'
    titulo = _('Análise Econômica')
    template_name = 'economica/subsistemainsumo_form.html'
    obj = None
    tipo = None

    def get_permission_object(self):
        agroecossistema = get_object_or_404(Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = self.titulo
        context['modulo'] = 'economica'
        context['submodulo'] = 'subsistemainsumo'
        context['submodulo_tipo'] = self.tipo
        context['colspan'] = 5 if self.tipo == "P" else 6
        context['submodulo_titulo'] = Analise.get_submodulo_titulo(context['submodulo'], self.tipo)
        context['agroecossistema'] = Agroecossistema.objects.get(id=self.kwargs['agroecossistema_id'])
        context['car'] = self.get_object().analise.ciclo_anual_referencia
        self.request.session['subsistema_id'] = self.get_object().id

        qset = SubsistemaInsumo.objects.filter(subsistema_id=self.get_object().id, tipo=self.tipo)

        if self.request.POST:
            context['subsistema_insumo_formset'] = SubsistemaInsumoFormSet(self.request.POST,
                                                                           instance=self.get_object(),
                                                                           queryset=qset,
                                                                           form_kwargs={'tipo': self.tipo})
        else:
            context['subsistema_insumo_formset'] = SubsistemaInsumoFormSet(instance=self.get_object(),
                                                                           queryset=qset,
                                                                           form_kwargs={'tipo': self.tipo})
        return context

    def form_valid(self, form):
        context = self.get_context_data()
        subsistema_insumo_formset = context['subsistema_insumo_formset']

        with transaction.atomic():
            if subsistema_insumo_formset.is_valid():
                self.object = form.save()

                subsistema_insumo_formset.instance = self.object
                subsistema_insumo_formset.save()

            else:
                form.full_clean()
                form._errors[NON_FIELD_ERRORS] = form.error_class(
                    [
                        _("Foram encontrados erros no formulário e os dados NÃO foram salvos. Favor fazer as correções indicadas.")])
                return self.render_to_response(self.get_context_data(form=form))

        return super(SubsistemaInsumoUpdateView, self).form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(SubsistemaInsumoUpdateView, self).get_form_kwargs()
        kwargs.update({'can_delete': self.request.user.has_perm('agroecossistema.alterar', Agroecossistema.objects.get(
            id=self.kwargs['agroecossistema_id']))})
        kwargs.update({'ano_car': self.kwargs['ano_car']})
        kwargs.update({'agroecossistema_id': self.kwargs['agroecossistema_id']})
        return kwargs

    def get_success_url(self):
        return self.request.path


class SubsistemaPagamentoTerceirosUpdateView(PermissionRequiredMixin, UpdateView):
    model = Subsistema
    form_class = SubsistemaForm
    permission_required = 'agroecossistema.visualizar'
    titulo = _('Análise Econômica')
    template_name = 'economica/subsistemapagamentoterceiros_form.html'
    obj = None
    tipo = None

    def get_permission_object(self):
        agroecossistema = get_object_or_404(Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = self.titulo
        context['modulo'] = 'economica'
        context['submodulo'] = 'subsistemapagamentoterceiros'
        context['submodulo_titulo'] = Analise.get_submodulo_titulo(context['submodulo'], self.tipo)
        context['agroecossistema'] = Agroecossistema.objects.get(id=self.kwargs['agroecossistema_id'])
        context['car'] = self.get_object().analise.ciclo_anual_referencia
        self.request.session['subsistema_id'] = self.get_object().id

        if self.request.POST:
            context['subsistema_pagamentoterceiros_formset'] = SubsistemaPagamentoTerceirosFormSet(self.request.POST, instance=self.get_object())
        else:
            context['subsistema_pagamentoterceiros_formset'] = SubsistemaPagamentoTerceirosFormSet(
                instance=self.get_object())
        return context

    def form_valid(self, form):
        context = self.get_context_data()
        subsistema_pagamentoterceiros_formset = context['subsistema_pagamentoterceiros_formset']

        with transaction.atomic():
            if subsistema_pagamentoterceiros_formset.is_valid():
                self.object = form.save()

                subsistema_pagamentoterceiros_formset.instance = self.object
                subsistema_pagamentoterceiros_formset.save()

            else:
                form.full_clean()
                form._errors[NON_FIELD_ERRORS] = form.error_class(
                    [
                        _("Foram encontrados erros no formulário e os dados NÃO foram salvos. Favor fazer as correções indicadas.")])
                return self.render_to_response(self.get_context_data(form=form))

        return super(SubsistemaPagamentoTerceirosUpdateView, self).form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(SubsistemaPagamentoTerceirosUpdateView, self).get_form_kwargs()
        kwargs.update({'can_delete': self.request.user.has_perm('agroecossistema.alterar', Agroecossistema.objects.get(
            id=self.kwargs['agroecossistema_id']))})
        kwargs.update({'ano_car': self.kwargs['ano_car']})
        kwargs.update({'agroecossistema_id': self.kwargs['agroecossistema_id']})
        return kwargs

    def get_success_url(self):
        return self.request.path


class SubsistemaAquisicaoReciprocidadeUpdateView(PermissionRequiredMixin, UpdateView):
    model = Subsistema
    form_class = SubsistemaForm
    permission_required = 'agroecossistema.visualizar'
    titulo = _('Análise Econômica')
    template_name = 'economica/subsistemaaquisicaoreciprocidade_form.html'
    obj = None
    tipo = None

    def get_permission_object(self):
        agroecossistema = get_object_or_404(Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = self.titulo
        context['modulo'] = 'economica'
        context['submodulo'] = 'subsistemapagamentoterceiros' if self.tipo == 'S' else 'subsistemainsumo'
        context['submodulo_tipo'] = self.tipo
        context['submodulo_titulo'] = Analise.get_submodulo_titulo(context['submodulo'], self.tipo)
        context['target_id'] = 'subsistemaaquisicaoreciprocidade'
        context['agroecossistema'] = Agroecossistema.objects.get(id=self.kwargs['agroecossistema_id'])
        context['car'] = self.get_object().analise.ciclo_anual_referencia
        self.request.session['subsistema_id'] = self.get_object().id

        qset = SubsistemaAquisicaoReciprocidade.objects.filter(subsistema_id=self.get_object().id, tipo=self.tipo)

        if self.request.POST:
            context['subsistema_aquisicaoreciprocidade_formset'] = SubsistemaAquisicaoReciprocidadeFormSet(
                self.request.POST,
                instance=self.get_object(),
                queryset=qset,
                form_kwargs={'show': self.tipo})
        else:
            context['subsistema_aquisicaoreciprocidade_formset'] = SubsistemaAquisicaoReciprocidadeFormSet(
                instance=self.get_object(),
                queryset=qset,
                form_kwargs={'show': self.tipo})
        return context

    def form_valid(self, form):
        context = self.get_context_data()
        subsistema_aquisicaoreciprocidade_formset = context['subsistema_aquisicaoreciprocidade_formset']

        with transaction.atomic():
            if subsistema_aquisicaoreciprocidade_formset.is_valid():
                self.object = form.save()

                subsistema_aquisicaoreciprocidade_formset.instance = self.object
                subsistema_aquisicaoreciprocidade_formset.save()

            else:
                form.full_clean()
                form._errors[NON_FIELD_ERRORS] = form.error_class(
                    [
                        _("Foram encontrados erros no formulário e os dados NÃO foram salvos. Favor fazer as correções indicadas.")])
                return self.render_to_response(self.get_context_data(form=form))

        return super(SubsistemaAquisicaoReciprocidadeUpdateView, self).form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(SubsistemaAquisicaoReciprocidadeUpdateView, self).get_form_kwargs()
        kwargs.update({'can_delete': self.request.user.has_perm('agroecossistema.alterar', Agroecossistema.objects.get(
            id=self.kwargs['agroecossistema_id']))})
        kwargs.update({'ano_car': self.kwargs['ano_car']})
        kwargs.update({'agroecossistema_id': self.kwargs['agroecossistema_id']})
        return kwargs

    def get_success_url(self):
        return self.request.path


class SubsistemaTempoTrabalhoUpdateView(PermissionRequiredMixin, UpdateView):
    model = Subsistema
    form_class = SubsistemaForm
    permission_required = 'agroecossistema.visualizar'
    titulo = _('Subsistema')
    obj = None
    template_name = 'economica/subsistematempotrabalho_form.html'

    def get_permission_object(self):
        agroecossistema = get_object_or_404(Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = self.titulo
        context['modulo'] = 'economica'
        context['submodulo'] = 'subsistematempotrabalho'
        context['submodulo_titulo'] = Analise.get_submodulo_titulo(context['submodulo'])
        context['agroecossistema'] = Agroecossistema.objects.get(id=self.kwargs['agroecossistema_id'])
        context['analise'] = self.get_object().analise
        context['car'] = context['analise'].ciclo_anual_referencia
        self.request.session['subsistema_id'] = self.get_object().id

        if self.request.POST:
            context['subsistema_tempo_trabalho_formset'] = SubsistemaTempoTrabalhoFormSet(
                self.request.POST, instance=self.get_object(),
                form_kwargs={'agroecossistema_id': self.kwargs['agroecossistema_id'],
                             'ano_car': self.kwargs['ano_car'],
                             'sim_slug': self.kwargs.get('sim_slug', None)})
        else:
            context['subsistema_tempo_trabalho_formset'] = SubsistemaTempoTrabalhoFormSet(
                instance=self.get_object(),
                form_kwargs={'agroecossistema_id': self.kwargs['agroecossistema_id'],
                             'ano_car': self.kwargs['ano_car'],
                             'sim_slug': self.kwargs.get('sim_slug', None)})

        return context

    def form_valid(self, form):
        context = self.get_context_data()
        subsistema_tempo_trabalho_formset = context['subsistema_tempo_trabalho_formset']

        with transaction.atomic():
            if subsistema_tempo_trabalho_formset.is_valid():
                self.object = form.save()

                subsistema_tempo_trabalho_formset.instance = self.object
                subsistema_tempo_trabalho_formset.save()
            else:
                form.full_clean()
                form._errors[NON_FIELD_ERRORS] = form.error_class(
                    [
                        _("Foram encontrados erros no formulário e os dados NÃO foram salvos. Favor fazer as correções indicadas.")])
                return self.render_to_response(self.get_context_data(form=form))

        return super(SubsistemaTempoTrabalhoUpdateView, self).form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(SubsistemaTempoTrabalhoUpdateView, self).get_form_kwargs()
        kwargs.update({'can_delete': self.request.user.has_perm('agroecossistema.alterar', Agroecossistema.objects.get(
            id=self.kwargs['agroecossistema_id']))})
        kwargs.update({'ano_car': self.kwargs['ano_car']})
        kwargs.update({'agroecossistema_id': self.kwargs['agroecossistema_id']})
        kwargs.update({'sim_slug': self.kwargs.get('sim_slug')})
        return kwargs

    def get_success_url(self):
        return self.request.path


class SubsistemaProducaoUpdateView(PermissionRequiredMixin, UpdateView):
    model = Subsistema
    form_class = SubsistemaForm
    permission_required = 'agroecossistema.visualizar'
    titulo = _('Subsistema')
    obj = None
    template_name = 'economica/subsistemaproducao_form.html'

    def get_permission_object(self):
        agroecossistema = get_object_or_404(Agroecossistema, id=self.kwargs['agroecossistema_id'])
        return agroecossistema

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = self.titulo
        context['modulo'] = 'economica'
        context['submodulo'] = 'subsistemaproducao'
        context['submodulo_titulo'] = Analise.get_submodulo_titulo(context['submodulo'])
        context['agroecossistema'] = Agroecossistema.objects.get(id=self.kwargs['agroecossistema_id'])
        context['analise'] = self.get_object().analise
        context['car'] = context['analise'].ciclo_anual_referencia
        self.request.session['subsistema_id'] = self.get_object().id

        if self.request.POST:
            context['subsistema_producao_formset'] = SubsistemaProducaoFormSet(self.request.POST,
                                                                               instance=self.get_object())
        else:
            context['subsistema_producao_formset'] = SubsistemaProducaoFormSet(instance=self.get_object())

        return context

    def form_valid(self, form):
        context = self.get_context_data()
        subsistema_producao = context['subsistema_producao_formset']

        with transaction.atomic():
            if subsistema_producao.is_valid():
                self.object = form.save()

                subsistema_producao.instance = self.object
                subsistema_producao.save()
            else:
                form.full_clean()
                form._errors[NON_FIELD_ERRORS] = form.error_class(
                    [
                        _("Foram encontrados erros no formulário e os dados NÃO foram salvos. Favor fazer as correções indicadas.")])
                return self.render_to_response(self.get_context_data(form=form))

        return super(SubsistemaProducaoUpdateView, self).form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(SubsistemaProducaoUpdateView, self).get_form_kwargs()
        kwargs.update({'can_delete': self.request.user.has_perm('agroecossistema.alterar', Agroecossistema.objects.get(
            id=self.kwargs['agroecossistema_id']))})
        kwargs.update({'ano_car': self.kwargs['ano_car']})
        kwargs.update({'agroecossistema_id': self.kwargs['agroecossistema_id']})
        return kwargs

    def get_success_url(self):
        return self.request.path
