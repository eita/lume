# Projeto LUME - AS-PTA

## Instalação

1. Clonar o projeto
```bash
git clone git@gitlab.com:eita/lume.git
cd lume
``` 

2. Instalar e ativar o **virtualenv**
```bash
pip3 install virtualenv
python3 -m virtualenv env   #(apenas na primeira vez)
source env/bin/activate
``` 

3. Instalar os pré-requisitos
```bash
pip3 install -r requirements.txt
apt-get install postgis
```
4. **(apenas para desenvolvimento da biblioteca de municipios)** Clonar o projeto  django-municipios-br para um diretório separado da aplicação Lume
```bash
git clone git@gitlab.com:eita/django/django-municipios-br.git
``` 
5. **(apenas para desenvolvimento da biblioteca de municipios)**  Criar um link simbólico como um app no Lume para a pasta municipios dentro de django-municipios-br
```bash
ln -s caminho/para/o/projeto/django-municipios-br/municipios
``` 
6. Verificar se municipios se encontra na configuração INSTALLED_APPS
    INSTALLED_APPS = [
        ...
        'municipios',
    ]


5. Criar uma base de dados PostgreSQL. Ex: createdb <nome da base>

6. Configurar os dados de conexão no arquivo **settings.py** de acordo com a base criada 

7. Criação das tabelas

```bash
python manage.py migrate 
``` 

8. Migração dos dados iniciais

```bash
python manage.py loaddata ibge_2017
python manage.py loaddata initial_linhadotempo
python manage.py loaddata initial_qualitativa
python manage.py loaddata sitetree
``` 

9. Rodar

```bash
python manage.py runserver <porta>

``` 

10. Criar usuario admin
```bash
python manage.py createsuperuser
```

acessa a url **/admin** para a área administrativa

## I18n & Transifex Client

### Mensagens a partir do DB

Para gerar as mensagens a partir de valores contindos no DB, execute o comando:
`(venv)$ python manage.py generate_messages_from_db`

### Transifex

Para gerar os arquivos de tradução e integrar com Transifex do projeto siga os passos.

#### Requisitos

Tenha certeza que o [transifex client](https://docs.transifex.com/client/introduction) está instalado e configurado.

* Instalação: `(venv)$ pip install -r requirements.txt`
* Configuração: Você precisa configurar o API Token de acesso ao repositório `(venv)$ tx config`.

### Comandos dev

* Obter arquivos com traduções: `(venv)$ tx pull --all`
* Obter frases da base de dados: `(venv)$ python manage.py generate_messages_from_db`
* Gerar arquivos para tradução no projeto django com `(venv)$ django-admin makemessages -l es`. Para inglês utilize `en`. Portugês, `pt_BR`. Todos, `--all`
* Enviar modificações para transifex: `(venv)$ tx push -s --skip`.
    O `--skip` ignora erro de arquivos setados em `.tx/config` e não existem.
* Compilar arquivos de tradução `(venv)$ django-admin compilemessages`.

### Comandos produção

Execute/configure cron:  `sh bin/translate-update.sh`
